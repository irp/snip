FROM nginx:latest

# Install dependencies
RUN apt-get update && apt-get install -y openssl logrotate

# Copy Nginx configuration files
COPY ./apps/nginx /etc/nginx

# Copy logrotate configuration files
COPY ./docker/logrotate/service.sh /entry/logrotate/service.sh
COPY ./docker/logrotate/nginx.conf /etc/logrotate.d/nginx
RUN chmod 0644 /etc/logrotate.d/nginx

# Remove default Nginx link to stdout and stderr
RUN rm /var/log/nginx/*
RUN mkdir -p /logs/nginx

# Generate self-signed SSL certificates
RUN mkdir -p /ssl
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout /ssl/certificate.key \
    -out /ssl/certificate.crt \
    -config /etc/nginx/ssl/openssl.cnf \
    -subj "/C=DE/ST=Lower Saxony/L=Goettingen/O=Snip Lab/"

# Entrypoint
ENV DISABLE_METRICS=true
COPY ./docker/entrypoints/entrypoint.sh /entry/entrypoint.sh
COPY ./docker/entrypoints/entrypoint.nginx.sh /entry/entrypoint.nginx.sh
COPY ./docker/entrypoints/common.sh /entry/common.sh
ENTRYPOINT ["/entry/entrypoint.sh", "/entry/entrypoint.nginx.sh", "/docker-entrypoint.sh"]
# Have to reset CMD since it gets cleared when we set ENTRYPOINT
CMD ["nginx", "-g", "daemon off;"]