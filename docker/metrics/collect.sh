# Function to get metrics
get_metrics() {
    if [ -f /sys/fs/cgroup/cgroup.controllers ]; then
        # cgroup v2
        time=$(date +%s%N)

        # In microseconds (*1000 to convert to nanoseconds)
        cpu_usage=$(awk '/usage_usec/ {print $2 * 1000}' /sys/fs/cgroup/cpu.stat)
        memory_usage=$(cat /sys/fs/cgroup/memory.current)
        memory_limit=$(cat /sys/fs/cgroup/memory.max)
    else
        # cgroup v1
        time=$(date +%s%N)

        # In nanoseconds
        cpu_usage=$(cat /sys/fs/cgroup/cpu/cpuacct.usage)
        memory_usage=$(cat /sys/fs/cgroup/memory/memory.usage_in_bytes)
        memory_limit=$(cat /sys/fs/cgroup/memory/memory.limit_in_bytes)
    fi
}

# Get initial metrics
get_metrics
time_start=$time
cpu_start=$cpu_usage

# Wait for 1 second
sleep 10

# Get metrics again
get_metrics
time_end=$time
cpu_end=$cpu_usage

# Compute percentage CPU usage
cpu_usage_percent=$(awk -v cpu_start="$cpu_start" -v cpu_end="$cpu_end" -v time_start="$time_start" -v time_end="$time_end" 'BEGIN { printf "%.4f", (cpu_end - cpu_start) / (time_end - time_start) * 100}')
# Compute memory usage in MB
memory_usage_mb=$(awk -v memory_usage="$memory_usage" 'BEGIN { printf "%.4f", memory_usage / 1024 / 1024}')
# Get memory limit in MB
if [ "$memory_limit" = "max" ] || [ "$memory_limit" = "max\n" ]; then
    memory_limit=$(cat /proc/meminfo | grep MemTotal | awk '{print $2 * 1024}')
    memory_limit_mb=$(awk -v memory_limit="$memory_limit" 'BEGIN { printf "%.4f", memory_limit / 1024 / 1024}')
else
    memory_limit_mb=$(awk -v memory_limit="$memory_limit" 'BEGIN { printf "%.4f", memory_limit / 1024 / 1024}')
fi

############################

# Convert time to UTC ISO 8601 format
time_start_iso=$(date -u -d "@$(($time_start / 1000000000))" +"%Y-%m-%dT%H:%M:%SZ")
time_end_iso=$(date -u -d "@$(($time_end / 1000000000))" +"%Y-%m-%dT%H:%M:%SZ")

# Echo measurement as json
echo "{\"cpu_usage_percent\": $cpu_usage_percent, \"time_start\": \"$time_start_iso\", \"time_end\": \"$time_end_iso\", \"memory_usage_gb\": $memory_usage_mb, \"memory_limit_mb\": $memory_limit_mb}"
