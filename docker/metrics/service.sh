#!/bin/bash

## Description:
# Simple script that executes the collect script in a loop every 5 minutes

# Function to print an error and exit
function error_exit {
    echo -e "\033[0;31m[Metrics service] $1\033[0m"
    exit 1
}

function log {
    echo -e "[Metrics service] $1"
}

# Check if log folder exits
if [[ ! -d "${LOG_DIR}" ]]; then
    mkdir -p "${LOG_DIR}" || error_exit "Failed to create log folder."
fi

if [[ ! -f "${LOG_DIR}/metrics.log" ]]; then
    touch "${LOG_DIR}/metrics.log" || error_exit "Failed to create log file."
fi
# Change dir to the script path
cd "$(dirname "$0")"

# While loop (infinite) to run the script every 5 minutes
log "Running metrics service..."
while true; do
    start_time=$(date +%s)

    ./collect.sh >> "${LOG_DIR}/metrics.log" 2>&1
    
    if [[ $? -ne 0 ]]; then
        error_exit "Metrics service failed to run!"
    fi

    end_time=$(date +%s)
    elapsed_time=$((end_time - start_time))
    sleep_time=$((300 - elapsed_time))
    
    if [[ $sleep_time -gt 0 ]]; then
        sleep $sleep_time
    fi

    if [[ $sleep_time -lt 0 ]]; then
        error_exit "Metrics service took longer than 5 minutes to run! Stopping..."
    fi
done


# Test if cron is installed
if ! command -v cron &> /dev/null; then
    # While loop (infinite) to run the script every day at 1:00 AM
    log "Running metrics service..."
    while true; do
        start_time=$(date +%s)

        ./collect.sh >> "${LOG_DIR}/metrics.log" 2>&1
        
        if [[ $? -ne 0 ]]; then
            error_exit "Metrics service failed to run!"
        fi

        end_time=$(date +%s)
        elapsed_time=$((end_time - start_time))
        sleep_time=$((300 - elapsed_time))
        
        if [[ $sleep_time -gt 0 ]]; then
            sleep $sleep_time
        fi

        if [[ $sleep_time -lt 0 ]]; then
            error_exit "Metrics service took longer than 5 minutes to run! Stopping..."
        fi
    done
else
    # Install as cron
    log "Installing metrics service as cron job..."
    cron_job="*/5 * * * * /entry/metrics/collect.sh"
    # Write the cron job to the crontab
    (crontab -l 2>/dev/null; echo "$cron_job") | crontab -

    # Make sure cron is running
    cron > /dev/null 2>&1
fi