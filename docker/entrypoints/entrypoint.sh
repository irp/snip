#!/bin/bash
source /entry/common.sh

# Setup metrics logging and log rotation
if [ -z "$DISABLE_METRICS" ]; then
    /entry/metrics/service.sh &
fi
/entry/logrotate/service.sh &

# Run the secondary entrypoint (see Dockerfile)
exec "$@"
