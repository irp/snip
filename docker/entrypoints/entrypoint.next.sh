#!/bin/bash

# Source common functions
source /entry/common.sh

# Check if database and secrets config exists
create_default_config "/config_templates/database.yaml" "/config/database.yaml"
create_default_config "/config_templates/secrets.yaml" "/config/secrets.yaml"

MARIADB_USER=$(yq e '.database.user' /config/database.yaml)
MARIADB_PASSWORD=$(yq e '.database.password' /config/database.yaml)
MARIADB_ROOT_PASSWORD=$(yq e '.database.root_password' /config/database.yaml)
MARIADB_HOST=$(yq e '.database.host' /config/database.yaml)

if [ -z "$MARIADB_USER" ] || [ -z "$MARIADB_PASSWORD" ] || [ -z "$MARIADB_ROOT_PASSWORD" ] || [ -z "$MARIADB_HOST" ]; then
    log_error "Missing required values in /config/database.yaml or invalid yaml!"
    exit 1
fi

# Needed for middleware as it has no access to filesystem
SNIP_SESSION_SECRET=$(yq e '.session' /config/secrets.yaml)
if [ -z "$SNIP_SESSION_SECRET" ]; then
    log_error "Missing required session in /config/secrets.yaml or invalid yaml!"
    exit 1
fi
export SNIP_SESSION_SECRET=$SNIP_SESSION_SECRET

log "Waiting for Database ..."
wait_database_ready $MARIADB_HOST $MARIADB_PASSWORD $MARIADB_USER 
log "Database ready"

    
# Run migrations
log "Running database migrations"
# Check if script exists
if [ ! -f /database/migration_script/bundle/main.js ]; then
    # dev container 
    pnpm --filter database_migrate run --silent dev --password "$MARIADB_ROOT_PASSWORD" --host $MARIADB_HOST --user root
else
    # prod container
    if node /database/migration_script/bundle/main.js --host $MARIADB_HOST --user root --password "$MARIADB_ROOT_PASSWORD" /database/migration; then
        log "[Entrypoint] Database migrations complete";
    else
        exit 1;
    fi
fi

# Run the command
log "Running $*"
exec "$@"
