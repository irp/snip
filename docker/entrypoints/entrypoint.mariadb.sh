#!/bin/bash

# Source common functions
source /usr/local/bin/common.sh

# Check if /config is mounted or available
if [ ! -d /config ]; then
    log "No /config directory found."
    mkdir /config
    log "Created /config directory."
fi

create_default_config "/config_templates/database.yaml" "/config/database.yaml"

# Read values from database.yaml
MARIADB_ROOT_PASSWORD=$(yq e '.database.root_password' /config/database.yaml)
MARIADB_USER=$(yq e '.database.user' /config/database.yaml)
MARIADB_PASSWORD=$(yq e '.database.password' /config/database.yaml)

# Check if required values are present
if [ -z "$MARIADB_ROOT_PASSWORD" ]; then
    log_error "Missing required database.root_password in /config/database.yaml or invalid yaml!"
    exit 1
fi

# Call the original entrypoint script
export MARIADB_ROOT_PASSWORD=$MARIADB_ROOT_PASSWORD
exec /usr/local/bin/docker-entrypoint.sh mariadbd &

# Wait for MariaDB to start
wait_database_ready "localhost" "$MARIADB_ROOT_PASSWORD" "root"
sleep 2

# Create or update the snip user
log "Creating or updating db credentials according to database.yaml"
mysql -uroot -p"$MARIADB_ROOT_PASSWORD" <<EOF
CREATE USER IF NOT EXISTS 'snip'@'%' IDENTIFIED BY '$MARIADB_PASSWORD';
ALTER USER 'snip'@'%' IDENTIFIED BY '$MARIADB_PASSWORD';
FLUSH PRIVILEGES;
EOF

# Keep the container running
wait