log() {
    echo -e "[Entrypoint] $1"
}

log_error() {
    echo -e "\033[0;31m[Entrypoint] $1\033[0m"
}

# Create a default config file if it doesn't exist
create_default_config() {
    local input_file=$1
    local output_file=$2

    # Check if the input file exists
    if [ ! -f $input_file ]; then
        echo -e "\033[0;31m[Entrypoint] Template file $input_file not found!\033[0m"
        exit 1
    fi

    # Check if the output file exists
    if [ ! -f $output_file ]; then
        echo "[Entrypoint] No $output_file file found. Creating one from the template..."

        template_content=$(cat "$input_file")

        # Replace each occurrence of ${__SECRET__} with a unique random password
        while [[ $template_content =~ \$\{__SECRET__\} ]]; do
            random_password=$(pwgen -s 32 1)
            template_content=${template_content/\$\{__SECRET__\}/$random_password}
        done

        # Save the modified content to the output file
        echo "$template_content" > $output_file
        echo "[Entrypoint] Created new $output_file."
    fi
}

wait_database_ready() {
    local host=$1
    local password=$2
    local user=$3
    local timeout=30

    while true; do
        # Attempt to log in and execute a query
        # mariadb-admin ping returns 0 even if the server is not ready
        output=$(mariadb --host="$host" --user="$user" --password="$password" --skip-ssl -e "SELECT 1" 2>&1)
    
        # Check if the query was successful
        if [ $? -eq 0 ]; then
            break
        fi

        timeout=$((timeout - 1))
        if [ $timeout -eq 0 ]; then
            log_error "Could not connect to database!"
            exit 1
        fi
        sleep 1
    done
}
