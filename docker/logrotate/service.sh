#!/bin/bash

## Description:
# Simple script that executes the logrotate every day at 1:00 AM

# Function to print an error and exit
function error_exit {
    echo -e "\033[0;31m[Logrotate service] $1\033[0m"
    exit 1
}

function log {
    echo -e "[Logrotate service] $1"
}

# Change dir to the script path
cd "$(dirname "$0")"

# Test if cron is installed
if ! command -v cron &> /dev/null; then
    # While loop (infinite) to run the script every day at 1:00 AM
    log "Running logrotate service..."
    while true; do
        # Get the current time in HH:MM format
        current_time=$(date +%H:%M)

        # Check if the current time is 01:00
        if [[ "$current_time" == "01:00" ]]; then
            # Run your command
            logrotate /etc/logrotate.conf

            # Sleep for 60 seconds to avoid running the command multiple times within the same minute
            sleep 120
        fi

        # Sleep for 30 seconds to minimize CPU usage
        sleep 30
    done
else
    # Install as cron
    log "Installing logrotate service as cron job..."
    cron_job="0 1 * * * logrotate /etc/logrotate.conf"
    # Write the cron job to the crontab
    (crontab -l 2>/dev/null; echo "$cron_job") | crontab -

    # Make sure cron is running
    cron > /dev/null 2>&1
fi


