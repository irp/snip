---
label: Book/Page(s) editor
priority: 7
---

# The book/page(s) editor

The book/page(s) editor is the main interface for editing books and pages. It is a web-based interface that allows you to create, edit pages inside a selected book. One can think of it as a digital sheet of paper where you can write, draw, and add images. But with multiple people working on the same sheet of paper at the same time.

![Viewer](./images/viewer_typical.png)


## General layout

The editor is divided into two main parts. The <a href="#sidebar">
sidebar</a> which shows the pages of the book and allows to select them and the <a href="#main-editor">main editor</a> area where you can edit the content of a selected page.


![](./images/viewer_typical_general_overview.png)



Both parts are divided be a resize bar which allows the adjust the area of the sidebar and main editor respectively. The resize bar can be moved by clicking and dragging it and snaps to the left or right side of the editor. This allows you to hide the sidebar completely or show it in full width. 


## <a name="sidebar"></a>Sidebar

The sidebar consists of two tabs `Pages` and `Queue`. Further right underneath the tabs is a search field which allows to search for specific pages and navigation buttons to quickly create a new page or jump to the next or previous page. Further at the bottom of the sidebar there are a number of function buttons.



<div className="d-flex">
![Page tab](./images/viewer_sidebar_pages.png)
![Page tab](./images/viewer_sidebar_queue.png)
</div>


### Pages
The `Pages` tab shows a list of all pages in the book and allows to select a page by clicking on it. The currently selected page(s) are highlighted by a blue outline. The preview images of the pages show their page number and the users currently working on that page. You can hover over the user icons to see their full names.
### Queue
The `Queue` tab shows a list of all queued snippets. For instance these are images which are not yet placed on a page. The queue is shared between all users working on the same book. This means that if you add a snippet to the queue, all other users will see it in their queue as well.

### Search and navigation

The search field allows you to search for a specific word or phrase in the current book. At the moment this only works for computer generated text, i.e. not for free form entries. 

Additionally to this basic search functionality, we also provide a number of search modifiers. Every modifier has to be separated by a colon from the rest of the query. And gets interpreted distinctively. For example the search query `page:2` will search for the page number 2. If you want to search for the phrase `page:2`, you have to put it in quotes, i.e. `"page:2"`.

See the table below for an overview of the supported modifiers.

| Modifier | Description |
| --- | --- |
| `page:[num]` | Search for page by number |
| `pageid:[num]` | Search for page by id |
| `snip:[string]` | Search for pages which include snips with specific snip type |
| `snipid:[num]` | Search for page which include snips with specific snip id |
| `"string"` | Search for a specific phrase ignoring modifiers |


It is possible to chain modifiers. For example, `page:1,3,5-7 snip:motor,led,button` will search for pages 1, 3, 5, 6 and 7 with motor, led or button snips. The order of the modifiers does not matter. This means that the search will only return pages that match all modifiers. I.e. we perform a logical conjunction.



### Function buttons

The function buttons at the bottom of the sidebar allow you to perform a number of actions. The following table gives an overview of the buttons and their functionality.

| Button | Description |
| --- | --- |
| Bookshelf | Opens the bookshelf and allows to select a different book |
| Edit | Opens the book settings page |
| Download | Opens the download dialog |
| Fullscreen | Toggles fullscreen mode |




## <a name="main-editor"></a>Main editor

The main editor area is where you can edit the content of the selected page. A variety of tools are available on the left-hand side to help you interact with the page.

![](./images/viewer_main_editor.png)

The toolbar allows you to select various tools for interacting with the page. The currently selected tool is highlighted. Each tool in the toolbar may have a submenu, which can be accessed by right-clicking on the tool. Alternatively, you can click on the small arrow next to the tool to open the submenu.


### <InteractionTool/> Interaction tool

<Alert variant="info" className="m-2">
    The following content is slightly outdated, the tools still work as described but the images are not up to date.
</Alert>

The interaction tool is the default tool, enabling you to view a page and interact with its elements. This tool allows you to copy text, follow embedded links, and trigger other configured events.


### <MoveTool/> Move tool

![Move tool](./images/viewer_zoom_tool.png)

The move tool allows to change the view of the page. You can pan the page by clicking and dragging the mouse. You can zoom in and out by scrolling the mouse wheel or by using the slider in the tools submenu. Additionally the submenu allows to center or fit the page to the view.

### <DoodleTool/> Doodle tool

![Doodle tool](./images/viewer_doodle_tool.png)

The doodle tool allows you to draw on the page and is intended to be used with a stylus. You can select the color and the thickness of the pen in the tools submenu.

### <TextTool/> Text tool 

![Text tool](./images/viewer_text_tool.png)

The text tool allows you to add text to the page. You can select the font and configure it in the tools submenu. Further you may align the text relative to the page using the submenus alignment buttons or move the text around by dragging it.

### <EraserTool/> Eraser tool
The eraser tool enables you to delete specific elements from the page. However, it is important to note that you can only erase elements that you have placed yourself, and they must be less than five minutes old. This restriction helps maintain the integrity of the page and prevents accidental or unauthorized deletions of content.