---
label: "Coordinate systems"
author: Sebastian B. Mohr
priority: 0
---

# Coordinate Systems and Transformation Functions

Every snippet placed on the page is positioned using a coordinate system, which includes position and rotation values. The page coordinate system determines where each snippet is rendered. In our system:

- The top left corner has the coordinates $(0,0)$
- The bottom right corner has the coordinates $(1440,2000)$

To convert these page coordinates to a device-specific view, we use a homogeneous coordinate transformation denoted as $T$. This allows us to perform translation, scaling, rotation, and other affine transformations consistently using matrix multiplication i.e. the user can zoom or translate
the page with ease.


Let's consider a point on the page $X_P=(x_P,y_P,1)$, which is transformed to $X_S=(x_S,y_S,1)$ using the transformation matrix $T$ 
$$
T = \begin{pmatrix}
a & c & t_x \\
b & d & t_y \\
0 & 0 & 1 \\
\end{pmatrix}
$$


![transform](./images/transformation.png)



## Applying the Transformation

The transformed coordinates $X_S$ are obtained by multiplying the transformation matrix $T$ with the original coordinates $X_P$:

$$
\begin{pmatrix}
x_S \\
y_S \\
1
\end{pmatrix}
=
\begin{pmatrix}
a & c & t_x \\
b & d & t_y \\
0 & 0 & 1
\end{pmatrix}
\begin{pmatrix}
x_P \\
y_P \\
1
\end{pmatrix}
$$

### Types of Transformations

<div className="row">

<div className="col-sm-12 col-md-6">
1. **Translation:**
   - Moves a point from one location to another.
   - Transformation matrix for translation by $(t_x, t_y)$:

    $$
    T_{translation} = \begin{pmatrix}
    1 & 0 & t_x \\
    0 & 1 & t_y \\
    0 & 0 & 1
    \end{pmatrix}
    $$
</div>
<div className="col-sm-12 col-md-6">
2. **Scaling:**
   - Changes the size of the object.
   - Transformation matrix for scaling by factors $s_x$ and $s_y$:

  $$
   T_{scaling} = \begin{pmatrix}
   s_x & 0 & 0 \\
   0 & s_y & 0 \\
   0 & 0 & 1
   \end{pmatrix}
  $$
</div>
<div className="col-sm-12 col-md-6">
3. **Rotation:**
   - Rotates the object around the origin.
   - Transformation matrix for rotation by an angle $\theta$:
  $$
   T_{rotation} = \begin{pmatrix}
   \cos\theta & -\sin\theta & 0 \\
   \sin\theta & \cos\theta & 0 \\
   0 & 0 & 1
   \end{pmatrix}
  $$
</div>
<div className="col-sm-12 col-md-6">
4. **Shearing:**
   - Skews the object in one direction.
   - Transformation matrix for shearing along the x-axis by $sh_x$ and along the y-axis by $sh_y$:

  $$
   T_{shearing} = \begin{pmatrix}
   1 & sh_x & 0 \\
   sh_y & 1 & 0 \\
   0 & 0 & 1
   \end{pmatrix}
  $$
</div>
</div>

### Composite Transformations

Multiple transformations can be combined by multiplying their matrices. The order of multiplication is crucial as matrix multiplication is not commutative.

$$
T_{composite} = T_{n} \cdot T_{n-1} \cdot \ldots \cdot T_{1}
$$

By applying these transformations, we can accurately position and orient snippets on the page, ensuring that they appear correctly on different devices.


## Build in functions

All of the transform logic is handled in the `ViewPort` class. The built-in functions, `setZoom`, `fitPage`, `centerPage` and `translatePage`, provide essential controls for rendering and viewing the page content dynamically. Additionally the viewport holds functions to convert positions from view to page coordinates and the other way around. They ensure that users can interact with the page intuitively, whether they need to zoom in for detailed scrutiny or fit the entire page within their viewport for an overview. 

As a note the page coordinates are in theory also not fixed and changeable, this is not tested tho but could be used to create bigger pages.


### Regarding zoom

Zoom levels are not fixed globally but rather a relative value based on the current viewport height $v_h$ and the page height $p_h$. Any zoom $z$ that is applied is multiplied by the ratio between the view and page to obtain the scaling factors $a$ and $d$ of the transform $T$. 

$$
a = d = \frac{v_h}{p_h} \cdot z  
$$

Further to allow users to zoom using their current mouse position as origin point we translate to an origin point before and after applying the zoom scaling. 