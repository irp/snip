import fs from "fs";
import { defineConfig } from "tsup";

import { config } from "@snip/tsup-config/base";

import tsconfig from "./tsconfig.json";

export default defineConfig([
    {
        ...config,
        entry: {
            cluster: "src/cluster.ts",
            server: "src/server.ts",
            //localPage: "src/localPage.ts",
        },
        target: "node20",
        splitting: false,
        esbuildPlugins: [
            ...(config.esbuildPlugins || []),
            {
                name: "@assets",
                setup({ onLoad }) {
                    onLoad({ filter: /()/ }, (args) => {
                        let code = fs.readFileSync(args.path, "utf8");

                        code = code.replace(
                            /@assets\//g,
                            tsconfig.compilerOptions.paths[
                                "@assets/*"
                            ][0].replace("*", ""),
                        );
                        return { contents: code, loader: "ts" };
                    });
                },
            },
        ],
    },
]);
