import { NextFunction, Request, Response } from "express";

import { getPerms, ParsedPermissionACLData } from "@snip/auth/book_permissions";
import { getPermsPage } from "@snip/auth/page_permissions";
import { getSession } from "@snip/auth/session";
import { SnipSessionData, TokenSessionData } from "@snip/auth/types";
import service from "@snip/database";
import { ENTITY_TOKEN, RESOURCE_BOOK } from "@snip/database/types";

import { asyncHandler, AuthFailedError } from "./errorHandler";

// Auth middleware
export const authMiddleWare = asyncHandler(
    async (req: Request, res: Response, next: NextFunction) => {
        const session = await getSession(req, res);
        const bearer = req.headers.authorization?.split(" ").at(1);
        /** We only allow users to use the email endpoint
         * no tokens allowed!
         */
        if (!session.user && !session.api && !session.anonymous && !bearer) {
            throw new AuthFailedError();
        }

        req.session = {
            ...session,
            bearer: bearer,
        };

        next();
    },
);

// Auth middleware
export const adminMiddleWare = asyncHandler(
    async (req: Request, res: Response, next: NextFunction) => {
        const session = await getSession(req, res);

        if (session.api !== undefined) {
            return next();
        }

        /** We only allow users to use the email endpoint
         * no tokens allowed!
         */
        if (!session.user) {
            res.status(401).json({
                error: "Unauthorized",
            });
            return;
        }

        /** Check if user is in admin group */
        const user_id = session.user!.id;
        const groups = await service.group.getByUserId(user_id);

        const potentialAdminGroup = groups.find((g) => {
            // HARDCODED ID For now
            return g.id === 1;
        });

        if (potentialAdminGroup === undefined) {
            res.status(401).json({
                error: "Unauthorized",
            });
            return;
        }

        req.session = session;
        return next();
    },
);

export async function allowedToReadPage(
    page_id: number,
    session?: SnipSessionData & { bearer?: string },
) {
    if (!session) {
        return false;
    }

    let perms: { pRead?: boolean };
    if (session.api) {
        // Only used by own backend services
        perms = { pRead: true };
    } else {
        const ui_token_id = await getPotentialValidTokenFromAnonymous(
            session.anonymous,
            undefined,
            page_id,
        );
        //normal user
        perms = (
            await getPermsPage(page_id, {
                user_id: session.user?.id,
                ui_token_id: ui_token_id,
                bearer_token: session.bearer,
            })
        ).resolved;
    }
    if (!perms?.pRead) {
        return false;
    }
    return true;
}

export async function allowedToReadBook(
    book_id: number,
    session?: SnipSessionData & { bearer?: string },
) {
    if (!session) {
        return false;
    }

    const perms = await getPermsFromSession(book_id, session);

    if (!perms?.pRead) {
        return false;
    }
    return true;
}

export async function getPermsFromSession(
    book_id: number,
    session: SnipSessionData & { bearer?: string },
): Promise<ParsedPermissionACLData> {
    const ui_token_id = await getPotentialValidTokenFromAnonymous(
        session.anonymous,
        book_id,
    );

    if (session.api) {
        // Only used by own backend services
        return {
            id: -1,
            auth_method: "unknown",
            pRead: true,
            entity_id: -1,
            entity_type: ENTITY_TOKEN,
            resource_id: book_id,
            resource_type: RESOURCE_BOOK,
        };
    }

    //normal user
    return (
        await getPerms(book_id, {
            user_id: session.user?.id,
            ui_token_id: ui_token_id,
            bearer_token: session.bearer,
        })
    ).resolved;
}

async function getPotentialValidTokenFromAnonymous(
    anonymous?: TokenSessionData,
    book_id?: number,
    page_id?: number,
) {
    let ui_token_id;
    if (anonymous && anonymous.tokens.length > 0) {
        if (!book_id) {
            book_id = await service.page
                .getById(page_id!)
                .then((page) => page.book_id);
        }
        ui_token_id = anonymous.tokens.filter(
            (token) =>
                token.book_id === book_id && token.expires_at > Date.now(),
        )[0]?.id;
    }
    return ui_token_id;
}
