import { NextFunction, Request, Response } from "express";

import {
    DataValidationError,
    DataValidationErrorArray,
} from "@snip/snips/errors";
import { NotFoundError } from "@snip/database/errors";

export const errorHandler = (
    err: Error,
    _req: Request,
    res: Response,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _next: NextFunction,
) => {
    // Return generic error message to the client
    res.header("Content-Type", "application/json");
    if (err instanceof SyntaxError && "body" in err) {
        return res
            .status(400)
            .json({ error: "Invalid JSON", details: err.message });
    }
    if (err instanceof AuthFailedError) {
        return res.status(401).json({ error: "Unauthorized" });
    }
    if (err instanceof BadUsage) {
        return res
            .status(400)
            .json({ error: "BasUsage", details: err.message });
    }
    if (err instanceof DataValidationError) {
        return res
            .status(400)
            .json({ error: "DataValidationError", details: err.message });
    }
    if (err instanceof NotFoundError) {
        return res.status(404).json({ error: "Not Found" });
    }

    if (err instanceof DataValidationErrorArray) {
        return res.status(400).json({
            error: "DataValidationErrorArray",
            details: err.errors.map((e) => e.message),
        });
    }

    console.error(err); // Log the error for debugging purposes
    console.error(err.stack);
    return res.status(500).json({ error: "Internal Server Error" });
};
export const asyncHandler =
    (
        fn: (
            req: Request,
            res: Response,
            next: NextFunction,
        ) => Promise<unknown>,
    ) =>
        (req: Request, res: Response, next: NextFunction) =>
            Promise.resolve(fn(req, res, next)).catch(next);

/** Extend the error class to create an auth failed error */
export class AuthFailedError extends Error {
    constructor(message?: string) {
        super(message);
        this.name = "AuthFailedError";
        this.message = message || "";
    }
}

/** Extend the error class to create an auth failed error */
export class BadUsage extends Error {
    constructor(message?: string) {
        super(message);
        this.name = "BadUsage";
        this.message = message || "";
    }
}
