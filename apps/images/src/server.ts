import express, { Response } from "express";
import { createServer } from "http";

import service from "@snip/database";
import { NotFoundError } from "@snip/database/errors";

import {
    adminMiddleWare,
    allowedToReadBook,
    allowedToReadPage,
    authMiddleWare,
    getPermsFromSession,
} from "./common/auth";
import {
    asyncHandler,
    AuthFailedError,
    BadUsage,
    errorHandler,
} from "./common/errorHandler";
import {
    parseSelectedPages,
    RenderParams,
    validateRenderParams,
} from "./render/renderParams";

const app = express();
export const server = createServer(app);
import { AddressInfo } from "net";
import sharp from "sharp";
import { Readable } from "stream";
import { createGzip } from "zlib";
import { z } from "zod";

import { config } from "@snip/config/server";
import { LocalPage } from "@snip/render/page";
import { get_snip_from_data } from "@snip/snips";

import renderPage, { render } from "./render";
import { render_book_pdf } from "./render/pdf";
import { renderSnip } from "./render/snip";

const renderSchema = z.object({
    page_id: z.coerce.number(),
    format: z.string().optional(),
    width: z.coerce.number().optional(),
    encoding: z.string().optional(),
    return_stream: z.string().optional(),
    force_render: z.string().optional(),
});

app.get(
    "/render",
    authMiddleWare,
    express.json(),
    asyncHandler(async (req, res) => {
        const parsedBody = renderSchema.safeParse({
            ...req.query,
            ...req.body,
        });

        if (parsedBody.success === false) {
            throw new BadUsage(
                "Could not parse json: \n" +
                    parsedBody.error.issues.map((z) => z.message).join("\\n"),
            );
        }
        const { format, page_id, ...data } = parsedBody.data;

        const allowedToRead = await allowedToReadPage(page_id, req.session);
        if (!allowedToRead) {
            throw new AuthFailedError();
        }

        // Get render params
        const renderParams = validateRenderParams({
            format: format || "jpeg",
            ...data,
        });

        // Render page
        return renderPageAndReturnIt(res, page_id, renderParams);
    }),
);

app.get(
    "/render/:page_id(\\d+).:format((png|jpeg|webp|pdf))",
    authMiddleWare,
    asyncHandler(async (req, res) => {
        const pageId_s = req.params.page_id;
        const format = req.params.format;

        // Parse book id and check if
        // user is allowed to read the book
        if (!pageId_s || !format) {
            throw new Error("Failed to parse pageId or format from urlpath");
        }
        const pageId = parseInt(pageId_s);
        if (isNaN(pageId)) {
            throw new Error("Failed to parse pageId");
        }

        const allowedToRead = await allowedToReadPage(pageId, req.session);
        if (!allowedToRead) {
            throw new AuthFailedError();
        }

        // Get render params
        const renderParams = validateRenderParams({ format, ...req.query });

        // Render page
        return renderPageAndReturnIt(res, pageId, renderParams);
    }),
);

app.get(
    "/render/book_preview/:book_id(\\d+).:format((png|jpeg|webp|pdf))",
    authMiddleWare,
    asyncHandler(async (req, res) => {
        const bookId_s = req.params.book_id;
        const format = req.params.format;

        // Parse book id and check if
        // user is allowed to read the book
        if (!bookId_s || !format) {
            throw new Error("Failed to parse bookId or format from urlpath");
        }
        const bookId = parseInt(bookId_s);
        if (isNaN(bookId)) {
            throw new Error("Failed to parse bookId");
        }

        const allowedToRead = await allowedToReadBook(bookId, req.session);
        if (!allowedToRead) {
            throw new AuthFailedError();
        }

        // Get book cover page id
        let pageId: number;
        try {
            pageId = await service.page
                .getCoverByBookId(bookId)
                .then((page) => page.id);
        } catch (e) {
            if (e instanceof NotFoundError) {
                res.end(no_pages);
                return;
            }
            // rethrow error
            throw e;
        }

        // Get render params
        const renderParams = validateRenderParams({ format, ...req.query });

        return renderPageAndReturnIt(res, pageId, renderParams);
    }),
);

app.get(
    "/render/background/:bg_id(\\d+).:format((png|jpeg|webp|pdf))",
    authMiddleWare,
    asyncHandler(async (req, res) => {
        const bgId_s = req.params.bg_id;
        const format = req.params.format;

        // Parse background id and check if
        if (!bgId_s || !format) {
            throw new Error(
                "Failed to parse background id or format from urlpath",
            );
        }
        const bgId = parseInt(bgId_s);
        if (isNaN(bgId)) {
            throw new Error("Failed to parse background id");
        }

        // Get render params
        const renderParams = validateRenderParams({ format, ...req.query });

        const page = LocalPage.from_partial({
            background_type_id: bgId,
        });

        let transform_webp = false;
        if (renderParams.format === "webp") {
            transform_webp = true;
            renderParams.format = "jpeg";
        }

        let stream = await render(
            page,
            renderParams.width,
            renderParams.format,
        );

        // Transform to webp if requested
        if (transform_webp) {
            renderParams.format = "webp";
            const webp_transform = sharp().webp();
            stream = stream.pipe(webp_transform);
        }
        res.statusCode = 200;
        stream.pipe(res);
    }),
);

async function renderPageAndReturnIt(
    res: Response,
    pageId: number,
    renderParams: RenderParams,
) {
    // render to stream with timeout
    let stream: Readable | true;
    try {
        stream = await Promise.race([
            renderPage(pageId, renderParams),
            new Promise<true>((_, reject) =>
                setTimeout(() => reject(new Error("Render timeout")), 15000),
            ),
        ]);
    } catch (e) {
        if (e instanceof NotFoundError) {
            res.status(404).end(`Page ${pageId} not found`);
            return;
        } else if (e instanceof Error && e.message === "Render timeout") {
            res.status(504).end("Render timed out");
            return;
        }
        throw e;
    }

    // Early exit if return stream is false
    if (stream === true && renderParams.return_stream === false) {
        res.statusCode = 200;
        res.end("Done");
        return;
    } else {
        stream = stream as Readable;
    }

    // Compression stream
    if (renderParams.encoding === "gzip") {
        res.setHeader("Content-Encoding", "gzip");
        stream = stream.pipe(createGzip());
    }

    // Pipe the rendered stream to client
    switch (renderParams.format) {
        case "pdf":
            res.setHeader("Content-Type", "application/pdf");
            break;
        case "jpeg":
            res.setHeader("Content-Type", "image/jpeg");
            break;
        default:
            break;
    }
    res.statusCode = 200;
    stream.pipe(res);
}

const downloadBookJsonSchema = z.object({
    downloadAll: z.boolean().optional(),
    selectedPages: z.string().optional(),
    pageNumberLocation: z.enum(["none", "br", "tl", "tr", "bl"]).optional(),
});

app.post(
    "/render/download/:book_id(\\d+)",
    authMiddleWare,
    express.json(),
    asyncHandler(async (req, res) => {
        const bookId_s = req.params.book_id;
        // Parse book id and check if
        // user is allowed to read the book
        if (!bookId_s) {
            throw new Error("Failed to parse bookId");
        }
        const bookId = parseInt(bookId_s);
        if (isNaN(bookId)) {
            throw new Error("Failed to parse bookId");
        }

        const allowedToRead = await allowedToReadBook(bookId, req.session);
        if (!allowedToRead) {
            throw new AuthFailedError();
        }

        const resultParse = downloadBookJsonSchema.safeParse(req.body);

        if (resultParse.success === false) {
            throw new BadUsage(
                resultParse.error.issues.map((z) => z.message).join("\\n"),
            );
        }

        // Parse selectedPages
        // might throw bad usage
        const selectedPages = resultParse.data.selectedPages
            ? parseSelectedPages(resultParse.data.selectedPages)
            : undefined;

        // The following expects that the book is fully rendered
        // if they are not the endpoint might be a bit slow!
        const book_p = service.book.getById(bookId);
        const pages_p = service.page.getByBookId(bookId);

        const [book, pages] = await Promise.all([book_p, pages_p]);

        const [stream] = await render_book_pdf(
            book,
            pages,
            selectedPages,
            resultParse.data.pageNumberLocation,
        );
        res.setHeader("Content-Type", "text/pdf");
        //res.setHeader("Content-Length", size.toString());
        res.setHeader(
            "Content-Disposition",
            `attachment; filename=${book.title} - ${book.last_updated}.pdf`,
        );
        res.statusCode = 200;
        stream.pipe(res);
    }),
);

app.post(
    "/rerender/:page_id(\\d+)",
    adminMiddleWare,
    asyncHandler(async (req, res) => {
        const pageId_s = req.params.page_id;
        // Parse book id and check if
        // user is allowed to read the book
        if (!pageId_s) {
            throw new Error("Failed to parse pageId");
        }
        const pageId = parseInt(pageId_s);
        if (isNaN(pageId)) {
            throw new Error("Failed to parse pageId");
        }

        // Dispatch rerender request for each
        // format and width to the cluster adapter

        // Get params for all targets
        const targets_params = [];
        for (const target of config.render.parse().formats) {
            for (const width of target.widths) {
                targets_params.push({
                    format: target.format,
                    width,
                });
            }
        }

        // Make all requests
        const addr = server.address() as AddressInfo;
        const headers = {
            "Content-Type": "application/json",
            cookie: req.headers.cookie!,
        };

        const requests = targets_params.map(async (params) => {
            return await fetch(
                `http://localhost:${addr.port}/render/${pageId}.${params.format}?w=${params.width}&force_render=true&return_stream=false`,
                {
                    method: "GET",
                    headers,
                },
            ).then(async (res) => {
                return await res.text();
            });
        });

        await Promise.all(requests).catch((e) => {
            console.error(e);
        });
        res.statusCode = 200;
        res.end("Rerender request finished!");
    }),
);

app.post(
    "/rerender/book/:book_id(\\d+)",
    adminMiddleWare,
    asyncHandler(async (req, res) => {
        const bookId_s = req.params.book_id;
        // Parse book id and check if
        // user is allowed to read the book
        if (!bookId_s) {
            throw new Error("Failed to parse book_id");
        }
        const bookId = parseInt(bookId_s);
        if (isNaN(bookId)) {
            throw new Error("Failed to parse book_id");
        }

        // get all page ids
        const pages = await service.page.getByBookId(bookId);

        // Dispatch rerender request for each
        // format and width to the cluster adapter

        // Get params for all targets
        const targets_params: {
            format: string;
            pageId: number;
            width: number;
        }[] = [];
        for (const page of pages) {
            for (const target of config.render.parse().formats) {
                for (const width of target.widths) {
                    targets_params.push({
                        format: target.format,
                        pageId: page.id,
                        width,
                    });
                }
            }
        }

        // Make all requests
        const addr = server.address() as AddressInfo;
        const headers = {
            "Content-Type": "application/json",
            cookie: req.headers.cookie!,
        };

        const requests = targets_params.map(async (params) => {
            return await fetch(
                `http://localhost:${addr.port}/render/${params.pageId}.${params.format}?w=${params.width}&force_render=true&return_stream=false`,
                {
                    method: "GET",
                    headers,
                },
            ).then((res) => {
                return res.text();
            });
        });

        await Promise.all(requests);
        res.statusCode = 200;
        res.end("Rerender request finished!");
    }),
);

app.post(
    "/render/snip",
    //authMiddleWare,
    express.json({
        limit: "15mb",
    }),
    asyncHandler(async (req, res) => {
        const snip_data = req.body;
        // Add a book_id as this is not required
        // for the snip to be rendered
        snip_data.book_id = -1;

        const snip = get_snip_from_data(snip_data);

        const renderParams: Omit<RenderParams, "force_render"> = {
            encoding: "gzip",
            format: "png",
            width: 1400,
            return_stream: true,
        };

        const stream = await renderSnip(snip, renderParams);

        res.setHeader("Content-Type", "image/png");
        stream.pipe(res);
    }),
);

app.post(
    "/testAuth/:book_id(\\d+)",
    authMiddleWare,
    asyncHandler(async (req, res) => {
        const bookId_s = req.params.book_id;
        // Parse book id and check if
        // user is allowed to read the book
        if (!bookId_s) {
            throw new Error("Failed to parse bookId");
        }
        const bookId = parseInt(bookId_s);
        if (isNaN(bookId)) {
            throw new Error("Failed to parse bookId");
        }

        // Check if access is allowed
        if (!req.session) {
            throw new AuthFailedError();
        }

        const perms = await getPermsFromSession(bookId, req.session);

        if (!perms?.pRead) {
            res.status(403);
        }

        res.json(perms);
    }),
);

app.use(errorHandler);
app.disable("x-powered-by");

const no_pages = Buffer.from(
    "UklGRu4IAABXRUJQVlA4WAoAAAAgAAAA7wAAXQEASUNDUKACAAAAAAKgbGNtcwQwAABtbnRyUkdCIFhZWiAH5wAGAAEAEAAtAClhY3NwQVBQTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1kZXNjAAABIAAAAEBjcHJ0AAABYAAAADZ3dHB0AAABmAAAABRjaGFkAAABrAAAACxyWFlaAAAB2AAAABRiWFlaAAAB7AAAABRnWFlaAAACAAAAABRyVFJDAAACFAAAACBnVFJDAAACFAAAACBiVFJDAAACFAAAACBjaHJtAAACNAAAACRkbW5kAAACWAAAACRkbWRkAAACfAAAACRtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACQAAAAcAEcASQBNAFAAIABiAHUAaQBsAHQALQBpAG4AIABzAFIARwBCbWx1YwAAAAAAAAABAAAADGVuVVMAAAAaAAAAHABQAHUAYgBsAGkAYwAgAEQAbwBtAGEAaQBuAABYWVogAAAAAAAA9tYAAQAAAADTLXNmMzIAAAAAAAEMQgAABd7///MlAAAHkwAA/ZD///uh///9ogAAA9wAAMBuWFlaIAAAAAAAAG+gAAA49QAAA5BYWVogAAAAAAAAJJ8AAA+EAAC2xFhZWiAAAAAAAABilwAAt4cAABjZcGFyYQAAAAAAAwAAAAJmZgAA8qcAAA1ZAAAT0AAACltjaHJtAAAAAAADAAAAAKPXAABUfAAATM0AAJmaAAAmZwAAD1xtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAEcASQBNAFBtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJWUDggKAYAAPAyAJ0BKvAAXgE+MRiKQ6IhoRK0ACADBLS3cLuwjfYAfyD6APGv+Nfi75s98Pu16hfud/i9Ed9Zfo35M/uT/gPcDeEv49/S/yI/JLjCpVfxA+AL1H+X/4r8kv7N6KX7l6AfSD0O/yT+8/kh/RudhoAfxj+Zf5//Afuh/dPhY/pP77+Wftf/I/6r/yfcI/jn82/zX9g/dX/Ff//6dvYB6I37Bhgggggggggggggggpw2B818NuDO3uxDqxb0h/Dbqt7sQtb+lzXw26qi/fEOrFvddyLVi3uxDqdnb3Yh1Yt6Q/ht1W92IWt/S0Y6VHMVDGhEcFiPtG57FvSH8Nl16VFa4Ec66eqlohHg88Nme0tC/viubJ3Yha39HlUq/KVavFOheR2sik5sMyfeQnEcaA7YWWYy4PvfQWJvgzPXqtbfDaXhUuNik0tgIVPYuK+83iZBchF05vcvxLdzGHEnlWDVpzuVb+Y92IdXSTuqBgfNfDbqqL98Q6sW913ItWLe7EOp2dvdiHVi3pD+G3Vb3Yha39Lmvht1VF++IdWLe66AAP7/94EAAAAIH9udyiuhxeceuayPQh1whBmyjKdiLo38R8Rpbb4QyXTt4TDIS/02QaRA281VQbOPKqGB552gQlBvCvCAM4pwqESF3xR6Vtd1iyJhtP3EiFfjSQ8M8qHnTPBoxI+yQuViuYl8q3ujnI8dzbrLhg5qPDHoYwFyqFQbUbgeb5wIxy/vjBGc7WhX5ZsWi+CUuSG0S/iz5xB1vVLi4Lo4Q9jEEqK7B+i//6UXjlmVKuEz8AKji37ycNFpmdD/d2QgmA8NCuZUm1PylM7YRYrs2k85Olbm+ZoUO5eI9LFSUsQiJaSMhJvHgho+9GseqGyIvlHH7SPFPQXeolgkl/8a7F29uhH6g7J625hMOS3jCBFfgm3QqWfkb387wXCGbLEazI0yI78Qm2wHTs/uqvF0wYNhJfsEw0hKY/0uVSCkRuhXEnq4zkQPVyj9BaRDfOz+sgJ/E9XE48aIi3hrbAZlAoFZefRXnPGa5uESZdJyhZOlTjY24c1oS7RuP/Yllmk/vga4mOb+GfcRx0g5I6zihq61QsQ9wr1pN/wDVxp//0QrH5E63+R0YeehsFTD2NDP6sThAFbSDMei4I8iQXIt1TjV9MFeiN/aUsPvmhONW+M+HTUPHfEOPziDDXhZynjD/WbELKeXGvmzGfZr7HTjyGEvfTwEuDjMVIxOKRXj36lNvGkBdC3EtLV8q9cm3i6gA931QDawd0r5pH9DE8toBf/y7H8XnymUIjSI5JUgQzye4lvy5XDTwX6XyONz0MBohS4IrdY9Dc01WunfwJ51vxmeYbnOFeEAZt8G6ELRb/dv/5YUoY1CzTsM5JOqcWBzR6xKJEYVgBeouHVxrqDneVotScr/H5r9xf2LTCkL8PRRMm3TO8t2OGrUnXLD4cDK632pdOxibXHF8tNwHm6BT9Z3oU1OB8UYtEErQ5IT7GNpVcRH9d0aCPQLJ8iS41Y41P1zMw07uQqfnOyREEcUKXKN3+hUJex+lHy0Eqc4YEen8PPu4KrY4XWbuVu4x+y+o15nrDLDc7i5OD2EYwUACiQFnzFAhnti/avx4ATDlArd19Xxbspt4jkE7GalOS54AUcKOepM+M6AngyTHhA2qz75B8xy3YbU15+QLMnVx0K7S839sOn3AFd4bWwaC0Vbt/47E9uZcZfZ1gEAJcSGnR+73Hk1EQIxbGOVws8MB5BlGi8ts/GD5GnEHFUn9nR9xPc3eyN5AbZAdJXr7tWoaBCz0avDnLN0moQsCHyGq8W8CbLLO0B4yXJiaFA6PTEetuMxANDaRZe00xv0zSyv+f2F/CixriGIWjMAF9a6ViVIWFjPPDG8cKpGwi2xvOjs3wrn1bNGiHoVbZT/4vGoHMMJvKHjQBI6a60kONNc3bqwe1MEfTSuOdH/JU2X5VmpD2PjuBRTvL20oRzCiYkkNfAlNbc2DMOm4CYoZr/QFKcnv25Qx1L77V7avCsXgLzl7lcFDgyJWs4K2CGK6z9aZFQ0mzgnmMzgaJDGx4Yj93m344VARrxODp66qlAAAAAAAAA=",
    "base64",
);

/* c8 ignore start */
if (process.env.NODE_ENV !== "test") {
    const port = process.env.APP_PORT || "80";
    server.listen(parseInt(port));
}
/* c8 ignore end */
