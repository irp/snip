export const log = (...args: unknown[]) => {
    console.log(`[Images][PID:${process.pid}]`, ...args);
};
