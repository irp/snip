import type { SnipSessionData } from "@snip/auth/types";
declare global {
    namespace Express {
        export interface Request {
            session?: SnipSessionData & { bearer?: string };
        }
    }
}
