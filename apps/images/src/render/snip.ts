import { Readable } from "stream";

import { LocalPage } from "@snip/render/page";
import { BaseSnip } from "@snip/snips/general/base";

import { render } from ".";
import { RenderParams } from "./renderParams";

/** Render a single snip on a page
 *
 * @param snip - The snip to render
 */
export async function renderSnip(
    snip: BaseSnip,
    renderParams?: Omit<RenderParams, "force_render">,
): Promise<Readable> {
    if (renderParams === undefined) {
        renderParams = {
            encoding: "gzip",
            format: "png",
            width: 1400,
            return_stream: true,
        };
    }

    // Prepare snip images
    await snip.ready;

    const page = LocalPage.from_partial({
        snips: [snip],
    });

    // Workaround for transparent background
    //page.data.background_type_id = "none" as unknown as number;

    if (renderParams.format === "webp") {
        throw new Error(
            "WebP format is not supported for single snip rendering!",
        );
    }

    return render(page, renderParams.width, renderParams.format, false);
}
