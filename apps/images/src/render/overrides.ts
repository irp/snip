import { readFile } from "fs/promises";
import sharp from "sharp";
import { Canvas, Image, Path2D as SkiaP2D } from "skia-canvas";

import { BaseSnip } from "@snip/snips/general/base";
import { getContext } from "@snip/snips/general/code";
import { ImageSnip } from "@snip/snips/general/image";
import { ImageSnipLegacy } from "@snip/snips/general/legacy/image";
import { MarkdownSnip } from "@snip/snips/general/markdown";

BaseSnip.prototype.createCanvas = function (width: number, height: number) {
    return new Canvas(width, height);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const get_bitmap = async function (this: any) {
    this.bitmap = new Image();
    const promise = new Promise((resolve, reject) => {
        this.bitmap.onload = () => {
            resolve(true);
        };
        this.bitmap.onerror = (error: unknown) => {
            reject(error);
        };
    });

    if (this.blob.mime === "image/webp") {
        const png_buffer = await sharp(Buffer.from(this.blob.data, "base64"))
            .toFormat("png")
            .toBuffer();
        this.bitmap.src = `data:image/png;base64,${png_buffer.toString("base64")}`;
    } else {
        this.bitmap.src = `data:${this.blob.mime};base64,${this.blob.data}`;
    }
    await promise;

    this.img_width = this.bitmap.width;
    this.img_height = this.bitmap.height;
    // Overwrite default values if not set
    this._swidth = this.swidth || this.img_width;
    this._sheight = this.sheight || this.img_height;
    this._width = this.width || this.img_width;
    this._height = this.height || this.img_height;
    return true;
};

ImageSnipLegacy.prototype.get_bitmap = get_bitmap;
ImageSnip.prototype.get_bitmap = get_bitmap;
import path from "path";
async function readFromAssetsAsResponse(p: string, ...args: unknown[]) {
    const buffer = await readFile(path.join("/assets", p));
    return new Response(buffer);
}

MarkdownSnip.prototype.getContext = async function (
    lang: string | null | undefined,
) {
    return await getContext(lang, readFromAssetsAsResponse);
};

declare global {
    // eslint-disable-next-line no-var
    var Path2D: typeof SkiaP2D;
}

globalThis.Path2D = SkiaP2D;
