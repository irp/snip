import { rmSync } from "node:fs";
import renderPage from "../index";
import path from "path";

describe("renderPage", () => {
    beforeAll(() => {
        // Delete the tmp folder before running the tests
        const tmpFolderPath = import.meta.dirname + "/data/tmp";
        rmSync(tmpFolderPath, { recursive: true, force: true });
    });

    it("should render a page", async () => {
        const stream = await renderPage(1, {
            width: 1400,
            format: "pdf",
            force_render: false,
            encoding: "gzip",
            return_stream: true,
        });
        expect(stream).toBeDefined();
    });

    it("should render a page with different params", async () => {
        const widths = [200, 400, 600, 800, 1000, 1200, 1400];
        const formats = ["jpeg", "pdf"] as const;
        const encodings = ["gzip", "none"] as const;
        const force_renders = [true, false]; // See file access mock & glob mock above
        const return_streams = [true, false];

        // Run through all combinations
        for (const width of widths) {
            for (const format of formats) {
                for (const encoding of encodings) {
                    for (const force_render of force_renders) {
                        for (const return_stream of return_streams) {
                            const stream = await renderPage(1, {
                                width,
                                format,
                                force_render,
                                encoding,
                                return_stream: return_stream,
                            });
                            expect(stream).toBeDefined();
                        }
                    }
                }
            }
        }
    }, 10000);

    it("the id is invalid", async () => {
        const stream = renderPage(9999, {
            width: 9999,
            format: "pdf",
            force_render: false,
            encoding: "gzip",
            return_stream: true,
        });

        await expect(stream).rejects.toThrow(
            "Resource 'Page' with id '9999' not found in database",
        );
    });
});
