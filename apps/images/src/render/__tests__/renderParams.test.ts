import { config } from "@snip/config/server";
import {
    validateRenderParams,
    RenderParams,
    SelectedPage,
    parseSelectedPages,
} from "../renderParams";

describe("validate_renderParams", () => {
    describe("should throw an error", () => {
        it("encoding invalid", () => {
            expect(() => {
                validateRenderParams({ format: "jpeg", encoding: "invalid" });
            }).toThrow(
                "Invalid 'encoding' query parameter (not 'gzip' or 'none')!",
            );
        });

        it("width is not a number", () => {
            expect(() => {
                validateRenderParams({ format: "jpeg", width: "invalid" });
            }).toThrow("Invalid 'width' query parameter (not an int)!");
        });

        it("format is invalid", () => {
            expect(() => {
                validateRenderParams({ format: "invalid" });
            }).toThrow(
                "Invalid file format (ending)! Allowed formats are: jpeg, pdf",
            );
        });

        it("force_render is not a boolean", () => {
            expect(() => {
                validateRenderParams({
                    format: "jpeg",
                    force_render: "invalid",
                });
            }).toThrow(
                "Invalid boolean value in query parameter (not 'true' or 'false')!",
            );
        });
    });

    describe("parse the render params correctly", () => {
        it("minimum params", () => {
            const params = {
                format: "jpeg",
            };

            const expected: RenderParams = {
                encoding: "gzip",
                width: config.render.parse().formats[0]!.widths[0]!,
                format: "jpeg",
                force_render: false,
                return_stream: true,
            };

            expect(validateRenderParams(params)).toEqual(expected);
        });

        it("all params", () => {
            const params = {
                encoding: "gzip",
                width: "720",
                format: "pdf",
                force_render: "true",
            };

            const expected: RenderParams = {
                encoding: "gzip",
                width: 720,
                format: "pdf",
                force_render: true,
                return_stream: true,
            };

            expect(validateRenderParams(params)).toEqual(expected);
        });

        it("force_render different values", () => {
            for (const force_render of [true, false, "true", "false"]) {
                const params = {
                    format: "jpeg",
                    force_render,
                };

                const expected: RenderParams = {
                    encoding: "gzip",
                    width: config.render.parse().formats[0]!.widths[0]!,
                    format: "jpeg",
                    force_render:
                        force_render === "true" || force_render === true,
                    return_stream: true,
                };

                expect(validateRenderParams(params)).toEqual(expected);
            }
        });
    });

    describe("parseSelectedPages", () => {
        it("should parse selected pages correctly", () => {
            const selectedPages = "1,2,3-5,7-9";
            const expected: SelectedPage[] = [1, 2, [3, 5], [7, 9]];

            expect(parseSelectedPages(selectedPages)).toEqual(expected);
        });

        it("should throw an error for invalid range in page numbers", () => {
            const selectedPages = "1,2,3-5,7-a";

            expect(() => {
                parseSelectedPages(selectedPages);
            }).toThrow(`Invalid range in page numbers! "7-a"`);
        });

        it("should throw an error for invalid range in page numbers (start > end)", () => {
            const selectedPages = "1,2,5-3";

            expect(() => {
                parseSelectedPages(selectedPages);
            }).toThrow(`Invalid range in page numbers! "5-3"`);
        });

        it("should throw an error for first item in range smaller than 1", () => {
            const selectedPages = "1,2,0-5";

            expect(() => {
                parseSelectedPages(selectedPages);
            }).toThrow(`First item in range has to be bigger than 0! "0-5"`);
        });

        it("should throw an error for invalid page number", () => {
            const selectedPages = "1,2,3-a";

            expect(() => {
                parseSelectedPages(selectedPages);
            }).toThrow(`Invalid range in page numbers! \"3-a\"`);
        });

        it("should throw an error for page number smaller than 1", () => {
            const selectedPages = "1,2,0";

            expect(() => {
                parseSelectedPages(selectedPages);
            }).toThrow(`Page number has to be bigger than 0! "0"`);
        });
    });
});
