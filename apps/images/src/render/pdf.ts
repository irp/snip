import { PDFDocument } from "pdf-lib";
import { Readable } from "stream";

import stream2buffer from "@snip/common/stream2buffer";
import { BookDataRet, PageData } from "@snip/database/types";

import renderPage from ".";
import { PageNumberLocation, SelectedPage } from "./renderParams";

export async function render_book_pdf(
    bookData: BookDataRet,
    pages: PageData[],
    selectedPages?: SelectedPage[],
    pageNumberLocation?: PageNumberLocation,
): Promise<[Readable, number]> {
    if (pageNumberLocation === undefined) {
        pageNumberLocation = "none";
    }

    // Select pages which should be included in the pdf
    if (selectedPages && selectedPages.length > 0) {
        pages = pages.filter((page) => {
            for (const selectedPage of selectedPages) {
                // Range
                if (Array.isArray(selectedPage)) {
                    if (
                        page.page_number! + 1 >= selectedPage[0] &&
                        page.page_number! + 1 <= selectedPage[1]
                    ) {
                        return true;
                    }
                }
                // Single page
                else {
                    if (page.page_number! + 1 === selectedPage) {
                        return true;
                    }
                }
            }
            return false;
        });
    }

    const pdfsToMerge = await Promise.all(
        pages.map((page) => {
            return renderPage(page.id, {
                width: 720,
                format: "pdf",
                force_render: false,
                encoding: "gzip",
                return_stream: true,
            }) as Promise<Readable>;
        }),
    );

    const mergedPdf = await PDFDocument.create();
    for (const pdfBytes of pdfsToMerge) {
        const pdf = await PDFDocument.load(await stream2buffer(pdfBytes));
        // Get the form containing all the fields
        const form = pdf.getForm();
        form.flatten();

        const copiedPages = await mergedPdf.copyPages(
            pdf,
            pdf.getPageIndices(),
        );
        copiedPages.forEach((page) => {
            mergedPdf.addPage(page);
        });
    }
    // Set metadata
    mergedPdf.setTitle(bookData.title.toString());
    mergedPdf.setCreationDate(
        bookData.created ? new Date(bookData.created) : new Date(),
    );
    mergedPdf.setModificationDate(
        bookData.last_updated ? new Date(bookData.last_updated) : new Date(),
    );
    mergedPdf.setCreator("Snip Digital Lab-book");
    mergedPdf.setProducer("Snip Digital Lab-book");

    // Place page numbers
    if (pageNumberLocation !== "none") {
        const pagesPdf = mergedPdf.getPages();
        const font = await mergedPdf.embedFont("Helvetica");
        const fontSize = 24;

        for (let i = 0; i < pagesPdf.length; i++) {
            const { width, height } = pagesPdf[i]!.getSize();
            let pos: [number, number] = [0, 0];

            const text = `${pages[i]!.page_number! + 1}`;

            const textWidth = font.widthOfTextAtSize(text, fontSize);
            const textHeight = font.heightAtSize(fontSize);

            switch (pageNumberLocation) {
                case "tl": // top left
                    pos = [fontSize, height - fontSize - textHeight];
                    break;
                case "tr": // top right
                    pos = [
                        width - textWidth - fontSize,
                        height - fontSize - textHeight,
                    ];
                    break;
                case "bl": // bottom left
                    pos = [fontSize, fontSize];
                    break;
                case "br": // bottom right
                default:
                    pos = [width - textWidth - fontSize, fontSize];
                    break;
            }

            pagesPdf[i]!.drawText(`${pages[i]!.page_number! + 1}`, {
                x: pos[0],
                y: pos[1],
                size: fontSize,
                font: font,
            });
        }
    }

    const buf = await mergedPdf.save(); // Uint8Array
    const readable = new Readable({
        read() {
            this.push(buf);
            this.push(null);
        },
    });
    const fSize = buf.byteLength;

    return [readable, fSize];
}
