import { config } from "@snip/config/server";

import { BadUsage } from "../common/errorHandler";

export interface RenderParams {
    encoding: "gzip" | "none";
    format: "png" | "webp" | "jpeg" | "pdf";
    width: number;
    force_render: boolean;
    return_stream: boolean;
}

export function validateRenderParams(paramsRaw: {
    format: string;
    encoding?: string;
    width?: string | number;
    f?: string;
    w?: string | number;
    return_stream?: string | boolean;
    force_render?: string | boolean;
}): RenderParams {
    // Parse encoding
    const encoding = paramsRaw["encoding"] as "gzip" | "none" | undefined;
    if (encoding) {
        if (encoding !== "gzip" && encoding !== "none") {
            throw new BadUsage(
                "Invalid 'encoding' query parameter (not 'gzip' or 'none')!",
            );
        }
    }

    // Parse format
    const format = parseFormat(paramsRaw["format"]);

    // Parse width
    const width = parseWidth(paramsRaw["width"] ?? paramsRaw["w"], format);

    // Force render
    const force_render = parseBoolean(paramsRaw["force_render"], false);

    // return stream
    const return_stream = parseBoolean(paramsRaw["return_stream"], true);

    return {
        encoding: encoding ? encoding : "gzip",
        width: width,
        format: format,
        force_render,
        return_stream,
    };
}

/**
 * Parses and validates the format parameter from the input.
 *
 * This function checks if the provided format is among the allowed formats defined in the configuration.
 * If the format is valid, it returns the format. If no format is provided, it defaults to the first
 * format in the allowed formats list. If the format is invalid, it throws an error indicating the allowed formats.
 *
 * @param f - The format string to be parsed and validated.
 * @returns The validated format as specified in the RenderParams interface.
 * @throws {BadUsage} If the provided format is not among the allowed formats.
 */
function parseFormat(f: string | undefined): RenderParams["format"] {
    let format: string;
    if (f) {
        const allowed_formats = config.render
            .parse()
            .formats.map((f) => f.format);
        if (!allowed_formats.includes(f)) {
            throw new BadUsage(
                "Invalid file format (ending)! Allowed formats are: " +
                    allowed_formats.join(", "),
            );
        }
        format = f;
    } else {
        format = config.render.parse().formats[0]!.format;
    }
    return format as RenderParams["format"];
}

/**
 * Parses and validates the width parameter from the input.
 *
 * This function checks if the provided width is a valid integer and within the allowed widths for the specified format.
 * If the width is valid, it returns the closest allowed width. If no width is provided, it defaults to the first
 * width in the allowed widths list for the specified format. If the width is invalid, it throws an error indicating
 * that the width is not an integer.
 *
 * @param w - The width string to be parsed and validated.
 * @param format - The format string to determine the allowed widths.
 * @returns The validated width as a number.
 * @throws {BadUsage} If the provided width is not a valid integer.
 */
function parseWidth(w: number | string | undefined, format: string): number {
    let width: number;
    if (w) {
        width = parseInt(w as string);

        if (isNaN(width)) {
            throw new BadUsage("Invalid 'width' query parameter (not an int)!");
        }

        // Might error of no render format is set!
        const allowed_widths = config.render
            .parse()
            .formats.find((f) => f.format === format)!.widths;

        width = allowed_widths.reduce(function (prev, curr) {
            return Math.abs(curr - width) < Math.abs(prev - width)
                ? curr
                : prev;
        });
    } else {
        width = config.render.parse().formats.find((f) => f.format === format)!
            .widths[0]!;
    }
    return width;
}

/**
 * Parses and validates a boolean value from the input.
 *
 * This function checks if the provided value is a valid boolean string ("true" or "false") or a boolean type.
 * If the value is valid, it returns the corresponding boolean value. If no value is provided, it returns the default value.
 * If the value is invalid, it throws an error indicating that the value is not a valid boolean.
 *
 * @param b - The boolean value to be parsed and validated, which can be a string or a boolean.
 * @param default_v - The default boolean value to return if no value is provided. Defaults to false.
 * @returns The parsed boolean value.
 * @throws {BadUsage} If the provided value is not a valid boolean string or type.
 */
function parseBoolean(b?: string | boolean, default_v = false): boolean {
    if (b) {
        if (
            b !== "true" &&
            b !== "false" &&
            (b as boolean) !== true &&
            (b as boolean) !== false
        ) {
            throw new BadUsage(
                "Invalid boolean value in query parameter (not 'true' or 'false')!",
            );
        } else {
            return b ? b === "true" || b === true : false;
        }
    } else {
        return default_v;
    }
}

export type SelectedPage = [number, number] | number;
export type PageNumberLocation = "none" | "br" | "tl" | "tr" | "bl" | "custom";

/** Validates and parses the selectedPages string
 *
 * throws an error if the string is invalid
 *
 */
export function parseSelectedPages(selectedPages: string): SelectedPage[] {
    const pages: SelectedPage[] = [];

    // Split by comma and parse items
    const items = selectedPages.split(",");

    for (const item of items) {
        if (item === "") {
            continue;
        }

        // Check if item is a range
        if (item.includes("-")) {
            const [start, end] = item.split("-");

            if (!start || !end) {
                throw new BadUsage("Could not parse pages separated with '-'");
            }

            // check if start and end are numbers
            if (
                isNaN(parseInt(start)) ||
                isNaN(parseInt(end)) ||
                !/^\d+$/.test(start) ||
                !/^\d+$/.test(end)
            ) {
                throw new BadUsage(`Invalid range in page numbers! "${item}"`);
            }

            // Check if start is smaller than end
            if (parseInt(start) > parseInt(end)) {
                throw new BadUsage(`Invalid range in page numbers! "${item}"`);
            }

            // Start bigger than 0
            if (parseInt(start) < 1) {
                throw new BadUsage(
                    `First item in range has to be bigger than 0! "${item}"`,
                );
            }
            pages.push([parseInt(start), parseInt(end)]);
        }

        // Check if item is a single page
        else {
            // check if item is a number
            if (isNaN(parseInt(item)) || !/^\d+$/.test(item)) {
                throw new BadUsage(`Invalid page number! "${item}"`);
            }

            // Check if item is bigger than 0
            if (parseInt(item) < 1) {
                throw new BadUsage(
                    `Page number has to be bigger than 0! "${item}"`,
                );
            }
            pages.push(parseInt(item));
        }
    }

    return pages;
}
