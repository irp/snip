import "./overrides";

import glob from "fast-glob";
import { createReadStream, createWriteStream, existsSync, mkdirSync } from "fs";
import path from "path";
import { PDFDocument, PDFName, PDFString } from "pdf-lib";
import sharp from "sharp";
import skia from "skia-canvas";
const { Canvas, DOMMatrix, FontLibrary } = skia;
import { Readable } from "stream";
import { pipeline } from "stream/promises";

import { config } from "@snip/config/server";
import service from "@snip/database";
import { FONT_URLS } from "@snip/database/fonts/internal";
import { LocalPage } from "@snip/render/page";
import { get_snip_from_data } from "@snip/snips";
import {
    DataValidationErrorArray,
    UnknownNamespaceError,
    UnknownSnipTypeError,
} from "@snip/snips/errors";
import { DummySnip } from "@snip/snips/general/dummy";
import { LinkSnip } from "@snip/snips/general/link";

import { log } from "../logging";
import { type RenderParams } from "./renderParams";

// We need to load fonts for the page
// this can be problematic for tsc
// so we have to use a config path
if (process.env.NODE_ENV !== "test") {
    for (const [name, family] of FONT_URLS) {
        FontLibrary.use(family, path.join("@assets/fonts/", name));
    }
}

/** Partially public render function
 * allows to render a page by its id
 */
export default async function renderPage(
    page_id: number,
    params: RenderParams,
): Promise<Readable | true> {
    /** Check if file exists already everything with a higher format
     * than the requested one will be rendered
     *
     * /renders/{page_id}/{width}.{format}
     */
    const dir = `${config.render.parse().cache_dir}/${page_id}`;

    let transform_webp = false;
    if (params.format === "webp") {
        transform_webp = true;
        params.format = "jpeg";
    }

    let stream: Readable | undefined;
    let fs_stream: Promise<true> = Promise.resolve(true);
    if (!params.force_render) {
        stream = await pageFromFile(page_id, params.width, params.format);
    }

    if (stream === undefined) {
        stream = await pageFromDB(page_id, params.width, params.format);
        // Write the stream to the file system
        if (!existsSync(dir)) {
            mkdirSync(dir, {
                recursive: true,
            });
        }
        fs_stream = pipeline(
            stream,
            createWriteStream(`${dir}/${params.width}.${params.format}`),
            {
                end: false,
            },
        ).then(() => true);
    }

    if (!params.return_stream) {
        return await fs_stream;
    }

    // Transform to webp if requested
    if (transform_webp) {
        params.format = "webp";
        const webp_transform = sharp().webp();
        stream = stream.pipe(webp_transform);
    }

    return stream;
}

/** Helper function to find a rendered file
 * if it exists
 *
 * @param page_id the id of the page
 * @param width the width of the file
 * @param format the format of the file
 * @returns a readable stream if the file exists undefined otherwise
 */
async function pageFromFile(
    page_id: number,
    width: number,
    format: string,
): Promise<Readable | undefined> {
    const dir = `${config.render.parse().cache_dir}/${page_id}`;
    const files = await glob.glob(`${dir}/*.${format}`);
    for (const file of files) {
        const [fileWidth] = file.split("/").pop()!.split(".");
        if (width === parseInt(fileWidth!)) {
            return createReadStream(file);
        }
    }
}

/**
 * Renders a page from the database and returns the stream of the rendered page.
 * @param page_id - The ID of the page to render.
 * @param width - The width of the rendered page.
 * @param format - The format of the rendered page, which can be "pdf", "jpeg", or "png".
 * @returns A Promise that resolves to a Readable stream of the rendered page, or undefined if the page data is not found.
 */
async function pageFromDB(
    page_id: number,
    width: number,
    format: "pdf" | "jpeg" | "png",
): Promise<Readable> {
    /** Get page data
     * including all snips
     */
    const pageData = await service.page.getById(page_id);
    const page = new LocalPage(pageData);
    const snipsData = await service.snip.getByPageId(page_id);
    const snips = snipsData.map((snipData) => {
        try {
            return get_snip_from_data(snipData);
        } catch (e) {
            if (e instanceof UnknownSnipTypeError) {
                log("Unknown snip type", e.message);
            } else if (e instanceof UnknownNamespaceError) {
                log("Unknown namespace", e.message);
            } else if (e instanceof DataValidationErrorArray) {
                log("Data validation errors!");
                for (const error of e.errors) {
                    log(error.message);
                }
            } else if (e instanceof Error) {
                log("Unknown error", e.message);
            } else {
                log("Unknown error", e);
            }
        }
        return new DummySnip(snipData);
    });

    // Prepare snip images
    const promises: Promise<boolean>[] = [];
    for (const snip of snips) {
        if (Object.hasOwn(snip, "ready")) {
            promises.push(snip.ready!);
        }
    }
    await Promise.all(promises);
    page.set_snips(snips);

    /** Render the page
     * write the stream to the file system
     * and return the stream
     */
    const stream = await render(page, width, format);
    return stream;
}

/** Helper function to render a local page
 *  expects a fully initialized local page!
 */
export async function render(
    localPage: LocalPage,
    width: number,
    format: "pdf" | "jpeg" | "png",
    background = true,
): Promise<Readable> {
    const canvas = new Canvas(width, (width * 2000) / 1400);

    const ctx = canvas.getContext("2d");
    const transform = ctx.getTransform().invertSelf();
    const t_width =
        transform.a * canvas.width + transform.b * canvas.height + transform.e;
    const t_height =
        transform.c * canvas.width + transform.d * canvas.height + transform.f;
    if (background) {
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, t_width, t_height);
    }
    const scale = canvas.height / 2000;
    const t = new DOMMatrix([scale, 0, 0, scale, 0, 0]);
    ctx.setTransform(t);
    localPage.render(ctx);

    switch (format) {
        case "pdf":
            return await addHyperLinksToPdf(
                await canvas.toBuffer("pdf"),
                localPage.snips.filter((snip) => snip instanceof LinkSnip),
            );
        case "jpeg":
            return Readable.from(await canvas.toBuffer("jpeg"));
        case "png":
            return Readable.from(await canvas.toBuffer("png"));
    }
}

async function addHyperLinksToPdf(pdf: Buffer, snips: LinkSnip[]) {
    const pdfDoc = await PDFDocument.load(pdf);
    const page = pdfDoc.getPage(0);
    const w = page.getWidth();
    const h = page.getHeight();

    // Hardcoded for now
    const scale = w / 1400;

    const links = [];
    for (const snip of snips) {
        links.push(
            page.doc.context.register(
                page.doc.context.obj({
                    Type: "Annot",
                    Subtype: "Link",
                    Rect: [
                        snip.icon_x * scale,
                        h - (snip.icon_y + snip.icon_height) * scale,
                        (snip.icon_x + snip.icon_width) * scale,
                        h - snip.icon_y * scale,
                    ],
                    Border: [0, 0, 2],
                    C: [0, 0, 1],
                    A: {
                        Type: "Action",
                        S: "URI",
                        URI: PDFString.of(snip.href.href),
                    },
                }),
            ),
        );
    }
    page.node.set(PDFName.of("Annots"), pdfDoc.context.obj(links));

    const buf = await pdfDoc.save(); // Uint8Array
    const readable = new Readable({
        read() {
            this.push(buf);
            this.push(null);
        },
    });
    return readable;
}
