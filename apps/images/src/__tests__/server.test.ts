import request from "supertest";
import { server } from "../server";
import { config } from "@snip/config/server";
import { mockConfig } from "../../test_setup/mockConfig";
import { SnipSessionData } from "@snip/auth/types";

beforeEach(() => {
    mockConfig.session = {
        user: {
            id: 1,
            email_verified: true,
            created: -1,
        },
    } as SnipSessionData;
});

describe("/render (json)", () => {
    beforeAll(() => {
        server.listen(3000);
    });

    afterAll(() => {
        server.close();
    });

    it("should return 400 if no search params are provided", async () => {
        await request(server).get("/render").expect(400);
    });

    it("should return 400 if invalid search params are provided", async () => {
        await request(server)
            .get("/render")
            .send({ page_id: "invalid" })
            .expect(400);

        await request(server)
            .get("/render")
            .send({ page_id: 1, width: "invalid" })
            .expect(400);

        await request(server)
            .get("/render")
            .send({ page_id: 1, width: 200, format: "invalid" })
            .expect(400);

        await request(server)
            .get("/render")
            .send({
                page_id: 1,
                width: 200,
                format: "jpeg",
                encoding: "invalid",
            })
            .expect(400);

        await request(server)
            .get("/render")
            .send({
                page_id: 1,
                width: 200,
                format: "jpeg",
                encoding: "gzip",
                return_stream: "invalid",
            })
            .expect(400);

        await request(server)
            .get("/render")
            .send({
                page_id: 1,
                width: 200,
                format: "jpeg",
                encoding: "gzip",
                return_stream: "true",
                force_render: "invalid",
            })
            .expect(400);
    });

    it("should return 401 if no session is provided", async () => {
        mockConfig.session = {} as SnipSessionData;
        await request(server)
            .get("/render")
            .send({ page_id: 1, width: 200, format: "jpeg" })
            .expect(401);
    });

    it("should return 200 if valid", async () => {
        await request(server)
            .get("/render")
            .send({ page_id: 1, width: 200, format: "jpeg" })
            .expect(200)
            .expect("Content-Type", "image/jpeg")
            .expect("Content-Encoding", "gzip");

        await request(server)
            .get("/render")
            .send({ page_id: 1, width: 1400, format: "pdf", encoding: "none" })
            .expect("Content-Type", "application/pdf")
            .expect(200);
    });

    it("should return 200 if send via query", async () => {
        await request(server)
            .get("/render")
            .query({ page_id: 1, width: 200, format: "jpeg" })
            .expect(200)
            .expect("Content-Type", "image/jpeg")
            .expect("Content-Encoding", "gzip");

        await request(server)
            .get("/render")
            .query({ page_id: 1, width: 200, format: "jpeg", encoding: "none" })
            .expect("Content-Type", "image/jpeg")
            .expect(200);
    });
});

describe("/render/:page_id.(png|jpeg|webp)", () => {
    it("should return 404 not found if not an id", async () => {
        const endings = config.render.parse().formats.map((f) => f.format);
        for (const ending of endings) {
            const res = await request(server)
                .get("/render/9999." + ending)
                .expect(404);
        }
    });

    it("should allow for 'w' and 'width' query params", async () => {
        const endings = config.render.parse().formats.map((f) => f.format);
        const id = 1;
        for (const ending of endings) {
            await request(server)
                .get(`/render/${id}.${ending}?w=12`)
                .expect(200);

            await request(server)
                .get(`/render/${id}.${ending}?width=12`)
                .expect(200);
        }
    });
});

describe("any /", () => {
    it("should return 404 if wrong endpoint", async () => {
        await request(server).get("/").expect(404);
        await request(server).get("/wrong").expect(404);
    });
});

describe("/rerender/:page_id", () => {
    it("should return 200 on simple request", async () => {
        mockConfig.session = {
            api: {
                identifier: "test",
            },
        } as SnipSessionData;
        await request(server).post("/rerender/1").expect(200);
    });
});

describe("any /render/book_preview/:book_id", () => {
    it("should return 200 on simple request", async () => {
        const endings = config.render.parse().formats.map((f) => f.format);
        for (const ending of endings) {
            await request(server)
                .get(`/render/book_preview/1.${ending}`)
                .expect(200);
        }
    });
});

describe("testAuth /testAuth/:book_id", () => {
    it("should return user auth", async () => {
        const data = await request(server).post("/testAuth/1").expect(200);
        expect(data.body).toMatchObject({
            auth_method: "combined",
            pRead: true,
        });
    });

    it("should return api auth", async () => {
        mockConfig.session = {
            api: {
                identifier: "test",
            },
        } as SnipSessionData;
        const data = await request(server).post("/testAuth/1").expect(200);
        expect(data.body).toMatchObject({
            auth_method: "unknown",
            pRead: true,
        });
    });

    it("should return 401 if no session", async () => {
        mockConfig.session = {} as SnipSessionData;
        await request(server).post("/testAuth/1").expect(401);
    });

    it("should return api token", async () => {
        mockConfig.session = {} as SnipSessionData;
        const data = await request(server)
            .post("/testAuth/1")
            .set("Authorization", "Bearer test")
            .expect(200);

        expect(data.body).toMatchObject({
            auth_method: "bearer",
            pRead: true,
        });
    });
});

describe("/render/background/:background_id", () => {
    it("should return 200 on simple request", async () => {
        const endings = config.render.parse().formats.map((f) => f.format);
        for (const ending of endings) {
            await request(server)
                .get(`/render/background/1.${ending}`)
                .expect(200);
        }
    });
});
