import cluster, { Worker } from "node:cluster";

import { config } from "@snip/config/server";

import { log } from "./logging";

// Setup the primary cluster i.e. worker file
cluster.setupPrimary({
    exec: import.meta.dirname + "/server",
    silent: false,
});

// Create workers
const workers: { [key: string]: Worker } = {};
for (let i = 0; i < config.render.parse().num_processes; i++) {
    const worker = cluster.fork();
    workers[worker.process.pid!] = worker;
}
log(`Created ${config.render.parse().num_processes} workers`);

// Some callbacks for the cluster
cluster.on("online", function (worker) {
    log("worker pid:" + worker.process.pid + " is online");
});

cluster.on("exit", (worker, code, signal) => {
    console.log(
        `worker ${worker.process.pid} died with code: ${code}, and signal: ${signal}`,
    );
    const w = cluster.fork();
    workers[w.process.pid!] = w;
});

// Master can be terminated by either SIGTERM
// or SIGINT. The later is used by CTRL+C on console.

//Note for nodemon. Use nodemon --signal SIGTERM
const closeCluster = () => {
    log("Cluster Shutdown");
    for (const pid in workers) {
        workers[pid]!.destroy("SIGTERM");
    }
    process.exit(0);
};
process.on("SIGTERM", closeCluster);
process.on("SIGINT", closeCluster);
