# Snip image provider

The render pool is a small application that runs a server
to render the pages in the book by request. It is a simple
cluster of workers that can be scaled up or down as needed.


The worker returns streams of images or pdfs which can be ingested
by the fullstack app and returned to the client.


## Configuration

Configuration is done through the global env variables. The following
are the available options (see @snip/config/server for more details):

- `RENDER_PORT`: The port the server will listen on. Defaults to 4001 (to avoid conflicts with the main app). This is internal in the docker network.

- `RENDER_WORKERS`: The number of workers to spawn. Defaults to 4.

- `RENDER_DIR`: The directory where the workers will store the rendered files. Defaults to `/tmp/render`.


## Starting

To start the render pool, simply run `pnpm start` in this folder. The worker expects a database to be running, so make sure to start the database first. This starts a cluster of workers that will listen for requests on the configured port.


## Requesting a render

To request a render, simply send a GET request to the `/render` endpoint. You can send both a json as body or query parameters. The following are the available options:

- `page_id`: The id of the page to render. This is required.
- `format`: The format of the file to render. Can be `pdf` or `jpeg`. Defaults to `jpeg`.
- `width`: The width of the image to render. Defaults to 200.
- `mode`: The mode of the stream. Can be `compress` or `original`.


### Reading a compressed stream

The compressed stream is a stream with gzip compression. To read it, you can use the following code:

```javascript

const image = fetch('http://localhost:4001/render?page_id=1&format=jpeg&mode=compress')
  .then(res => {
    const ds = new DecompressionStream('gzip');
    const stream_ = res.body.pipeThrough(ds);
    const decompressedStream = stream_.pipeThrough(ds);
    return new Response(decompressedStream);
  }

```