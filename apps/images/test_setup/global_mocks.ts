import { resolve, join } from "path";

// Mock the loadJsonFile method for testing
vi.doMock("@snip/database", async () => {
    const { NotFoundError } = await vi.importActual<
        typeof import("@snip/database/errors")
    >("@snip/database/errors");
    const { readFileSync } = await vi.importActual<typeof import("fs")>("fs");
    const { mockConfig } = await import("./mockConfig");

    return {
        default: {
            page: {
                getById: async (id: string) => {
                    if (parseInt(id) == 9999) {
                        throw new NotFoundError("Page", 9999);
                    }
                    const data = readFileSync(
                        import.meta.dirname + "/data/pageData.json",
                        "utf8",
                    );
                    return JSON.parse(data);
                },
                getFirstByBookId: vi.fn().mockResolvedValue({ id: 123 }),
                getCoverByBookId: async (id: number) => {
                    const data = readFileSync(
                        import.meta.dirname + "/data/pageData.json",
                        "utf8",
                    );
                    return JSON.parse(data);
                },
            },
            snip: {
                getByPageId: async () => {
                    const data = readFileSync(
                        import.meta.dirname + "/data/snips.json",
                        "utf8",
                    );
                    return JSON.parse(data);
                },
            },
            permission: {
                getByUserAndPageIdReduced: async (
                    user_id: number,
                    page_id: number,
                ) => {
                    return {
                        pRead: mockConfig.pRead,
                    };
                },
                getByUserAndBookReduced: async (
                    user_id: number,
                    book_id: number,
                ) => {
                    return {
                        pRead: mockConfig.pRead,
                    };
                },
            },
            group: {
                getByUserId: async (user_id: number) => {
                    // Only needed to test admin auth
                    if (mockConfig.is_admin) {
                        return [
                            {
                                id: 1,
                            },
                        ];
                    } else {
                        return [];
                    }
                },
            },
        },
    };
});

vi.doMock("@snip/database/sql.connection", () => {
    return {
        pool_data: {
            end: vi.fn(),
        },
        pool_permissions: {
            end: vi.fn(),
        },
    };
});

vi.doMock("fast-glob", () => {
    return {
        default: {
            glob: async (query: string) => {
                // Test 2 sizes
                if (query.includes(".pdf")) {
                    return ["1400.pdf"];
                }
                if (query.includes(".jpeg")) {
                    return ["200.jpeg"];
                }
                if (query.includes(".webp")) {
                    return ["200.webp"];
                }
                return [];
            },
        },
    };
});

vi.doMock("fs", async (importOriginal) => {
    const cloneFs = await importOriginal<typeof import("fs")>();
    return {
        ...cloneFs,

        createReadStream: (path: string) => {
            if (path.includes(".pdf")) {
                return cloneFs.createReadStream(
                    import.meta.dirname + "/data/test.pdf",
                );
            }
            if (path.includes(".jpeg")) {
                return cloneFs.createReadStream(
                    import.meta.dirname + "/data/test.jpeg",
                );
            }
        },
    };
});

// Mockr readfile to read from other location
vi.mock("fs/promises", async (importOriginal) => {
    const actualModule = await importOriginal<typeof import("fs/promises")>();
    return {
        ...actualModule,
        readFile: async (p: string) => {
            if (p.startsWith("/assets")) {
                return actualModule.readFile(
                    join(import.meta.dirname, "../../../", p),
                );
            }
            return actualModule.readFile(p);
        },
    };
});

vi.mock("@snip/config/server", async (importOriginal) => {
    const actualModule =
        await importOriginal<typeof import("@snip/config/server")>();
    return {
        config: {
            render: {
                parse: () => {
                    return {
                        ...actualModule.config.render.parse(),
                        cache_dir: import.meta.dirname + "/data/tmp",
                    };
                },
            },
        },
    };
});

vi.stubEnv("SNIP_CONFIG_DIR", import.meta.dirname + "/config");

afterAll(async () => {
    const { rmSync } = await vi.importActual<typeof import("fs")>("fs");
    try {
        rmSync(import.meta.dirname + "/config", { recursive: true });
    } catch (e) {}
    try {
        rmSync(import.meta.dirname + "/data/tmp", { recursive: true });
    } catch (e) {}
    vi.resetModules();
});

// Moock session
vi.mock("@snip/auth/session", async (importOriginal) => {
    const { mockConfig } = await import("./mockConfig");
    const actualModule =
        await importOriginal<typeof import("@snip/auth/session")>();
    return {
        ...actualModule,
        getSession: async () => {
            return mockConfig.session;
        },
    };
});

vi.mock("@snip/auth/book_permissions", async (importOriginal) => {
    const { mockConfig } = await import("./mockConfig");
    const actualModule =
        await importOriginal<typeof import("@snip/auth/book_permissions")>();
    return {
        ...actualModule,
        getPerms: async (
            book_id: number,
            {
                user_id,
                ui_token_id,
                bearer_token,
            }: {
                user_id?: number;
                ui_token_id?: number;
                bearer_token?: string;
            },
        ) => {
            if (bearer_token) {
                return {
                    resolved: {
                        auth_method: "bearer",
                        pRead: mockConfig.pRead,
                    },
                };
            } else {
                return actualModule.getPerms(book_id, {
                    user_id,
                    ui_token_id,
                    bearer_token,
                });
            }
        },
    };
});

// Mock fetch
global.fetch = vi.fn(
    () =>
        Promise.resolve({
            text: () => {
                //wait 2 seconds
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve("test");
                    }, 2000);
                });
            },
        }),
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
) as any;
