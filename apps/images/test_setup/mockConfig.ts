import type { SnipSessionData } from "@snip/auth/types";

const session = {
    user: {
        id: 1,
        email_verified: true,
        created: -1,
    },
} as SnipSessionData;

export const mockConfig = {
    session: session,
    is_admin: false,
    pRead: true,
};
