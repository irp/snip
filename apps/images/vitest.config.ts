// vitest.config.ts
import fs from "fs";
import path from "path";
import wasm from "vite-plugin-wasm";
import tsconfigPaths from "vite-tsconfig-paths";
import { defineConfig } from "vitest/config";

export default defineConfig({
    test: {
        setupFiles: ["./test_setup/global_mocks.ts"],
        globals: true,
        pool: "forks",
    },

    plugins: [wasm(), tsconfigPaths()],
});
