import Link from "next/link";

import { LogoutBtn } from "./logoutBtn";

export default function Expired() {
    return (
        <>
            <h1 className="text-center">Token expired</h1>
            <hr />
            <p
                className="text-center"
                style={{
                    fontSize: "1.1rem",
                    margin: 0,
                    padding: "0.5rem",
                }}
                role="alert"
            >
                The token you are trying to use has expired. Please request a
                new token from the owner of the book.
            </p>
            <p
                className="text-center"
                style={{ fontSize: "1.1rem", margin: 0 }}
            ></p>
            <div className="d-flex flex-row align-items-center justify-content-around flex-grow-1 mt-2">
                <LogoutBtn />
                <Link href="/">Go back to the homepage</Link>
            </div>
        </>
    );
}
