"use client";
import { logout } from "lib/actions/logout";
import Button, { ButtonProps } from "react-bootstrap/Button";

export function LogoutBtn(props: ButtonProps) {
    return (
        <Button
            variant="outline-primary"
            onClick={async () => {
                await logout();
            }}
            {...props}
        >
            Logout | Delete local session
        </Button>
    );
}
