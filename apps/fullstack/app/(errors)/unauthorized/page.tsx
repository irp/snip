import Link from "next/link";

import { RetryButton } from "./retryButton";

export default function Unauthorized() {
    return (
        <>
            <h1 className="text-center">401 - Unauthorized</h1>
            <hr />
            <p
                className="text-center"
                style={{
                    fontSize: "1.1rem",
                    margin: 0,
                    padding: "0.5rem",
                }}
                role="alert"
            >
                Looks like you don&apos;t have permission to access this page!
            </p>
            <p
                className="text-center"
                style={{ fontSize: "1.1rem", margin: 0 }}
            ></p>
            <div className="d-flex flex-row align-items-center justify-content-around flex-grow-1 mt-2">
                <RetryButton />
                <Link href="/">Go back to the homepage</Link>
            </div>
        </>
    );
}
