"use client";

import { resetSession } from "app/api/(actions)/resetSession";
import { useRouter } from "next/navigation";

export function RetryButton() {
    const router = useRouter();

    return (
        <button
            style={{
                backgroundColor: "transparent",
                border: "none",
                color: "var(--bs-primary)",
                cursor: "pointer",
                fontWeight: "bold",
                textDecoration: "underline",
            }}
            onClick={async () => {
                await resetSession();
                router.back();
            }}
        >
            Back to previous page
        </button>
    );
}
