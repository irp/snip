import styles from "components/frontpage/frontpage.module.scss";

export default function Layout({ children }: { children: React.ReactNode }) {
    return (
        <div
            className={
                styles.main +
                " d-flex flex-column justify-content-center align-items-center"
            }
        >
            <div>
                <div
                    className="text-primary p-5"
                    style={{
                        backgroundColor: "#adb5bd90",
                        borderRadius: "10px",
                        fontWeight: "bold",
                    }}
                >
                    {children}
                </div>
            </div>
        </div>
    );
}
