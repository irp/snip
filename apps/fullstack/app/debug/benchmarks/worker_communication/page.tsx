"use client";

import { useEffect, useRef } from "react";
import { Button } from "react-bootstrap";

import { WorkerC } from "./workerClass";

export default function Benchmark() {
    const workerRef = useRef<WorkerC | null>(null);
    useEffect(() => {
        if (workerRef.current === null) {
            workerRef.current = new WorkerC();
        }
    }, [workerRef]);

    function setInfo(times: number[], id: string) {
        const avg = times.reduce((a, b) => a + b) / times.length;
        const variance =
            times.reduce((a, b) => a + (b - avg) ** 2) / times.length;
        document.getElementById(id)!.innerText =
            `Time: ${avg} ms, Variance: ${variance} ms, n = ${times.length}`;
    }

    async function runBenchmark() {
        const worker = workerRef.current;
        if (!worker) return;
        let times = [];
        for (let i = 0; i < 2000; i++) {
            const start = performance.now();
            await worker.taskPost({
                type: "benchmark",
                data: {
                    value: i,
                    foo: "bar",
                    baz: {
                        nested: "object",
                    },
                },
            });
            const end = performance.now();
            times.push(end - start);
        }
        setInfo(times, "resultPost");

        times = [];
        for (let i = 0; i < 2000; i++) {
            const start = performance.now();
            await worker.taskChannel({
                type: "benchmark",
                data: {
                    value: i,
                    foo: "bar",
                    baz: {
                        nested: "object",
                    },
                },
            });
            const end = performance.now();
            times.push(end - start);
        }
        setInfo(times, "resultChannel");
        times = [];
    }

    return (
        <div>
            <Button id="btn" onClick={runBenchmark}>
                Run Benchmark
            </Button>
            <div>
                Post Message: <span id="resultPost" />
            </div>
            <div>
                Channel: <span id="resultChannel" />
            </div>
        </div>
    );
}
