import { Message } from "./types";

onmessage = async (evt: MessageEvent<Message>) => {
    const port = evt.ports[0];

    const response = await handleMessageEvent(evt);

    if (port) {
        port.postMessage({ r: response });
    }
};

async function handleMessageEvent(evt: MessageEvent<Message>) {
    return {
        ...evt.data,
    };
}
