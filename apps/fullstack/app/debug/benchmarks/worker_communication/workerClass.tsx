import { Message } from "./types";

export class WorkerC {
    workerChannel: Worker;
    workerPost: Worker;
    constructor() {
        this.workerChannel = new Worker(
            new URL("./channel.ts", import.meta.url),
        );
        this.workerPost = new Worker(new URL("./post.ts", import.meta.url));
    }

    /**
     * Executes a task in the worker.
     *
     * @param message - The message to send to the worker.
     * @param transfer - An optional array of transferable objects to send to the worker.
     * @returns A promise that resolves with the result of the task.
     */
    async taskPost(message: Message, transfer?: Transferable[]) {
        const worker = this.workerPost;
        if (!transfer) {
            transfer = [];
        }

        if (!message.id) {
            message.id = Math.random().toString().slice(2, 10);
        }

        return new Promise((resolve, reject) => {
            worker.addEventListener("message", (evt) => {
                if (evt.data.id === message.id) {
                    resolve(evt.data.r);
                }
            });

            // Send the canvas to the worker
            worker.postMessage(message);

            // Timeout reject
            setTimeout(() => {
                reject(new Error("Timeout"));
            }, 2000);
        });
    }

    // Channel has no need for an id
    async taskChannel(message: Message, transfer?: Transferable[]) {
        const worker = this.workerChannel;
        if (!transfer) {
            transfer = [];
        }

        return new Promise((resolve, reject) => {
            const channel = new MessageChannel();
            channel.port1.onmessage = (evt) => {
                resolve(evt.data.r);
            };

            // Send the canvas to the worker
            worker.postMessage(message, [channel.port2, ...transfer]);

            // Timeout reject
            setTimeout(() => {
                reject(new Error("Timeout"));
            }, 2000);
        });
    }
}
