import { Message } from "./types";

onmessage = async (evt: MessageEvent<Message>) => {
    const response = await handleMessageEvent(evt);

    if (response) {
        postMessage({ r: response, id: evt.data.id });
    }
};

async function handleMessageEvent(evt: MessageEvent<Message>) {
    return {
        ...evt.data,
    };
}
