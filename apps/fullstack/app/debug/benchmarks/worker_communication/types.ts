export interface Message {
    type: string;
    id?: string;
    data?: unknown;
}
