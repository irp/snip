"use client";
import { useEffect, useRef, useState } from "react";

import { Pipeline } from "@snip/render/pipeline";
import { MinimalPage, RenderContext } from "@snip/render/types";
import { ViewPort } from "@snip/render/viewport";

import { pointerEventFactory } from "components/editor/tools/move/moveHandler";

import "./test.css";

export default function PageJsx() {
    /** Preload data */
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const viewPortRef = useRef<ViewPort>();
    const renderRef = useRef<Pipeline>();

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const page = new DummyPage();
    const evCache = useRef<Map<number, PointerEvent[]>>(new Map());

    const [allowedEvents, setAllowedEvents] = useState([
        "touch",
        "pen",
        "mouse",
    ]);

    const handleCheckboxChange = (eventType: string, isChecked: boolean) => {
        setAllowedEvents((prevEvents) => {
            if (isChecked) {
                return [...prevEvents, eventType]; // Add the event type
            } else {
                return prevEvents.filter((event) => event !== eventType); // Remove the event type
            }
        });
    };

    useEffect(() => {
        const canvas = canvasRef.current;
        if (!canvas) return;
        if (!renderRef.current) {
            const canvasSize = {
                width: canvas.width,
                height: canvas.height,
            };
            viewPortRef.current = new ViewPort(canvasSize, page.size);
            renderRef.current = new Pipeline(canvas, viewPortRef.current);
        }

        // Resizer
        const viewport = viewPortRef.current;
        const resizeObs = new ResizeObserver((entries) => {
            viewport?.resize(
                entries[0]!.contentRect.width,
                entries[0]!.contentRect.height,
            );
        });
        resizeObs.observe(canvas);

        return () => {
            resizeObs.disconnect();
        };
    }, [page]);

    useEffect(() => {
        const { registerContainer } = pointerEventFactory(
            evCache.current,
            allowedEvents,
            (scale, center) => {
                viewPortRef.current?.scale(scale, center);
                renderRef.current?.renderPage(page);
            },
            (t) => {
                viewPortRef.current?.translate(t);
                renderRef.current?.renderPage(page);
            },
            () => {
                viewPortRef.current?.fitPage(true, true, [30, 30]);
                renderRef.current?.renderPage(page);
            },
        );

        if (!canvasRef.current) return;

        const removeHandler = registerContainer(canvasRef.current);
        return removeHandler;
    }, [allowedEvents, page]);

    return (
        <>
            Playground for page resize & move events.
            <form className="d-flex gap-2 align-items-center">
                <div>
                    <button
                        type="button"
                        onClick={() => {
                            viewPortRef.current?.resetMatrix();
                            renderRef.current?.renderPage(page);
                        }}
                    >
                        Reset
                    </button>
                    <button
                        type="button"
                        onClick={() => {
                            viewPortRef.current?.centerPage(true, false);
                            renderRef.current?.renderPage(page);
                        }}
                    >
                        Center width
                    </button>
                    <button
                        type="button"
                        onClick={() => {
                            viewPortRef.current?.centerPage(false, true);
                            renderRef.current?.renderPage(page);
                        }}
                    >
                        Center height
                    </button>
                    <button
                        type="button"
                        onClick={() => {
                            viewPortRef.current?.fitPage(true, true, [30, 30]);
                            renderRef.current?.renderPage(page);
                        }}
                    >
                        Fit page
                    </button>
                </div>
                <div>
                    <label>
                        <input
                            type="checkbox"
                            checked={allowedEvents.includes("pen")}
                            onChange={(e) =>
                                handleCheckboxChange("pen", e.target.checked)
                            }
                        />
                        Allow Pen
                    </label>
                    <label>
                        <input
                            type="checkbox"
                            checked={allowedEvents.includes("touch")}
                            onChange={(e) =>
                                handleCheckboxChange("touch", e.target.checked)
                            }
                        />
                        Allow Touch
                    </label>
                    <label>
                        <input
                            type="checkbox"
                            checked={allowedEvents.includes("mouse")}
                            onChange={(e) =>
                                handleCheckboxChange("mouse", e.target.checked)
                            }
                        />
                        Allow Mouse
                    </label>
                </div>
            </form>
            <div className="d-flex h-100 w-100">
                <div
                    style={{
                        backgroundColor: "red",
                        width: "100px",
                        height: "100%",
                    }}
                ></div>
                <div
                    style={{
                        backgroundColor: "green",
                        width: "100px",
                        height: "100%",
                    }}
                ></div>
                <div className="flex-grow w-100">
                    <canvas
                        ref={canvasRef}
                        style={{
                            background: "transparent",
                            height: "100%",
                            width: "100%",
                            overscrollBehavior: "none",
                            touchAction: "none",
                            userSelect: "none",
                        }}
                        draggable={false}
                    />
                </div>
            </div>
        </>
    );
}

/** Dummy class to simulate a page
 */
class DummyPage extends MinimalPage {
    size = {
        width: 1400,
        height: 2000,
    };

    render(ctx: RenderContext) {
        // Calculate the position for the text
        const text = "Dummy PAGE";
        ctx.font = "40px Arial"; // Set font size and style
        const textMetrics = ctx.measureText(text);
        const textX = (this.size.width - textMetrics.width) / 2;
        const textY = this.size.height / 2;
        // Draw the text in the center of the page
        ctx.fillText(text, textX, textY);

        ctx.lineWidth = 30;
        ctx.strokeStyle = "green";
        ctx.strokeRect(0, 0, this.size.width, this.size.height);
    }
}
