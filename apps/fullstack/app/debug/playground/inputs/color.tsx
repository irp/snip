import React, { useState } from "react";

import { Wrapper } from "./utils";

import { ColorInput, MultiColorInput } from "components/form/inputs/color";

const ColorInputs = ({
    showValid,
    showInvalid,
}: {
    showValid: boolean;
    showInvalid: boolean;
}) => {
    const [colorList, setColorList] = useState([
        "#ff0000",
        "#00ff00",
        "#0000ff",
    ]);
    const [currentColorIdx, setCurrentColorIdx] = useState(0);
    const [color, setColor] = useState<string>(colorList[currentColorIdx]!);

    const colorInputProps = {
        color,
        setColor,
        showHex: true,
    };

    const multiColorInputProps = {
        colorList,
        setColorList,
        currentColorIdx,
        setCurrentColorIdx,
        showHex: true,
    };

    return (
        <>
            <Wrapper>
                <h3>ColorInput</h3>
                <ColorInput {...colorInputProps} />
                <ColorInput {...colorInputProps} onReset={() => {}} />
                {showValid && (
                    <>
                        <ColorInput
                            {...colorInputProps}
                            isValid
                            validFeedback="correct"
                        />
                        <ColorInput
                            {...colorInputProps}
                            isValid
                            validFeedback="correct"
                            onReset={() => {}}
                        />
                    </>
                )}
                {showInvalid && (
                    <>
                        <ColorInput
                            {...colorInputProps}
                            isInvalid
                            invalidFeedback="wrong"
                        />
                        <ColorInput
                            {...colorInputProps}
                            isInvalid
                            invalidFeedback="wrong"
                            onReset={() => {}}
                        />
                    </>
                )}
            </Wrapper>

            <Wrapper>
                <h3>MultiColorInput</h3>
                <MultiColorInput {...multiColorInputProps} />
                <MultiColorInput {...multiColorInputProps} onReset={() => {}} />
                {showValid && (
                    <>
                        <MultiColorInput
                            {...multiColorInputProps}
                            isValid
                            validFeedback="correct"
                        />
                        <MultiColorInput
                            {...multiColorInputProps}
                            isValid
                            validFeedback="correct"
                            onReset={() => {}}
                        />
                    </>
                )}
                {showInvalid && (
                    <>
                        <MultiColorInput
                            {...multiColorInputProps}
                            isInvalid
                            invalidFeedback="wrong"
                        />
                        <MultiColorInput
                            {...multiColorInputProps}
                            isInvalid
                            invalidFeedback="wrong"
                            onReset={() => {}}
                        />
                    </>
                )}
            </Wrapper>
        </>
    );
};

export default ColorInputs;
