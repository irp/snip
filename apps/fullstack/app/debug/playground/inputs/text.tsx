import React, { useState } from "react";

import { Wrapper } from "./utils";

import {
    EmailInput,
    LongTextInput,
    PasswordInput,
    TextInput,
} from "components/form/inputs/text";

const TextInputs = ({
    showValid,
    showInvalid,
}: {
    showValid: boolean;
    showInvalid: boolean;
}) => {
    const [text, setText] = useState("test value");

    const components = [
        {
            Component: TextInput,
            value: text,
            onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                setText(e.target.value),
        },
        {
            Component: LongTextInput,
            value: text,
            onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                setText(e.target.value),
        },
        {
            Component: PasswordInput,
            value: text,
            onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                setText(e.target.value),
        },
        {
            Component: EmailInput,
            value: text,
            onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                setText(e.target.value),
        },
    ];

    return components.map(({ Component, value, ...props }, index) => (
        <Wrapper key={index}>
            <h3>{Component.name}</h3>
            <Component {...props} value={value} />
            <Component {...props} value={value} onReset={() => {}} />
            {showValid && (
                <>
                    <Component
                        {...props}
                        value={value}
                        isValid
                        validFeedback="correct"
                    />
                    <Component
                        {...props}
                        value={value}
                        isValid
                        validFeedback="correct"
                        onReset={() => {}}
                    />
                </>
            )}
            {showInvalid && (
                <>
                    <Component
                        {...props}
                        value={value}
                        isInvalid
                        invalidFeedback="wrong"
                    />
                    <Component
                        {...props}
                        value={value}
                        isInvalid
                        invalidFeedback="wrong"
                        onReset={() => {}}
                    />
                </>
            )}
        </Wrapper>
    ));
};

export default TextInputs;
