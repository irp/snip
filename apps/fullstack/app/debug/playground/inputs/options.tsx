/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState } from "react";

import {
    CheckInput,
    DropdownInput,
    OptionInput,
} from "components/form/inputs/option";

const OptionInputs = ({
    showValid,
    showInvalid,
}: {
    showValid: boolean;
    showInvalid: boolean;
}) => {
    const [option, setOption] = useState("1");

    const options = [
        { label: "Option 1" },
        { label: "Option 2" },
        { label: "Option 3" },
    ];

    const optionsDropDown = [
        {
            eventKey: "1",
            label: (
                <span style={{ fontWeight: "bold", fontFamily: "Arial" }}>
                    Option 1
                </span>
            ),
        },
        {
            eventKey: "2",
            label: (
                <span style={{ fontStyle: "italic", fontFamily: "Verdana" }}>
                    Option 2
                </span>
            ),
        },
        {
            eventKey: "3",
            label: (
                <span
                    style={{
                        textDecoration: "underline",
                        fontFamily: "Courier New",
                    }}
                >
                    Option 3
                </span>
            ),
        },
    ];

    const [checked, setChecked] = useState(true);

    const components = [
        {
            Component: OptionInput,
            options,
            value: option,
            onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                setOption(e.target.value),
        },
        {
            Component: DropdownInput,
            options: optionsDropDown,
            value: option,
            onSelect: (e: string) => setOption(e),
        },
        {
            Component: CheckInput,
            checked: checked,
            onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                setChecked(e.target.checked);
            },
        },
    ];

    return components.map(({ Component, ...props }, index) => (
        <div key={index} className="mb-3 d-flex flex-column gap-2">
            <h3>{Component.name}</h3>
            <Component {...(props as any)} />
            {showValid && (
                <>
                    <Component
                        {...(props as any)}
                        isValid
                        validFeedback="correct"
                    />
                    <Component
                        {...(props as any)}
                        isValid
                        validFeedback="correct"
                        onReset={() => {}}
                    />
                </>
            )}
            {showInvalid && (
                <>
                    <Component
                        {...(props as any)}
                        isInvalid
                        invalidFeedback="wrong"
                    />
                    <Component
                        {...(props as any)}
                        isInvalid
                        invalidFeedback="wrong"
                        onReset={() => {}}
                    />
                </>
            )}
        </div>
    ));
};

export default OptionInputs;
