export function Wrapper({
    children,
    ...props
}: React.HTMLAttributes<HTMLDivElement>) {
    return (
        <div className="mb-3 d-flex flex-column gap-2" {...props}>
            {children}
        </div>
    );
}
