"use client";

import React, { useState } from "react";

import ColorInputs from "./color";
import { ContextMenu } from "./contextMenu";
import NumberInputs from "./number";
import OptionInputs from "./options";
import TextInputs from "./text";

import { ColorInput, MultiColorInput } from "components/form/inputs/color";
import {
    JoinedNumbersInput,
    JoinedRangeNumberInput,
    NumberInput,
    RangeInput,
} from "components/form/inputs/number";
import { DropdownInput, OptionInput } from "components/form/inputs/option";
import { DateTimeInput } from "components/form/inputs/other";
import {
    EmailInput,
    LongTextInput,
    PasswordInput,
    TextInput,
} from "components/form/inputs/text";

const Page = () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const components: any[] = [];

    //Text
    const [text, setText] = useState("test value");
    components.push(
        ...[
            {
                Component: TextInput,
                value: text,
                onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                    setText(e.target.value),
            },
            {
                Component: LongTextInput,
                value: text,
                onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                    setText(e.target.value),
            },
            {
                Component: PasswordInput,
                value: text,
                onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                    setText(e.target.value),
            },
            {
                Component: EmailInput,
                value: text,
                onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                    setText(e.target.value),
            },
        ],
    );

    // Colors
    const [colorList, setColorList] = useState([
        "#ff0000",
        "#00ff00",
        "#0000ff",
    ]);
    const [currentColorIdx, setCurrentColorIdx] = useState(0);
    const [color, setColor] = useState(colorList[currentColorIdx]);
    components.push(
        ...[
            {
                Component: ColorInput,
                color,
                setColor,
                showHex: true,
            },
            {
                Component: MultiColorInput,
                colorList,
                setColorList,
                currentColorIdx,
                setCurrentColorIdx,
                showHex: true,
            },
        ],
    );

    // Numbers
    const [number, setNumber] = useState(5);
    components.push(
        ...[
            {
                Component: NumberInput,
                value: number,
                onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                    setNumber(e.target.valueAsNumber),
            },
            {
                Component: RangeInput,
                value: number,
                onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                    setNumber(e.target.valueAsNumber),
            },
            {
                Component: JoinedRangeNumberInput,
                value: number,
                onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                    setNumber(e.target.valueAsNumber),
            },
            {
                Component: JoinedNumbersInput,
                values: [number, number],
                onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                    setNumber(e.target.valueAsNumber),
            },
        ],
    );

    // Options
    const [option, setOption] = useState("1");
    const options = [
        {
            label: "Option 1",
        },
        {
            label: "Option 2",
        },
        {
            label: "Option 3",
        },
    ];

    const optionsDropDown = [
        {
            eventKey: "1",
            label: (
                <span
                    style={{
                        fontWeight: "bold",
                        fontFamily: "Arial",
                    }}
                >
                    Option 1
                </span>
            ),
        },
        {
            eventKey: "2",
            label: (
                <span
                    style={{
                        fontStyle: "italic",
                        fontFamily: "Verdana",
                    }}
                >
                    Option 2
                </span>
            ),
        },
        {
            eventKey: "3",
            label: (
                <span
                    style={{
                        textDecoration: "underline",
                        fontFamily: "Courier New",
                    }}
                >
                    Option 3
                </span>
            ),
        },
    ];
    components.push(
        ...[
            {
                Component: OptionInput,
                options,
                value: option,
                onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                    setOption(e.target.value),
            },
            {
                Component: DropdownInput,
                options: optionsDropDown,
                value: option,
                onSelect: (e: string) => {
                    console.log(e);
                    setOption(e);
                },
            },
        ],
    );

    // Other
    components.push(
        ...[
            {
                Component: DateTimeInput,
                value: "2021-01-01 12:00",
            },
        ],
    );

    const [state, setState] = useState({
        showValid: false,
        showInvalid: false,
    });

    return (
        <div className="overflow-auto">
            <div className="container h-100 w-100 p-5">
                <h1>Input Component Test</h1>
                <div className="mb-3 d-flex gap-2">
                    <div className="form-check">
                        <input
                            className="form-check-input"
                            type="checkbox"
                            name="validity"
                            id="valid"
                            checked={state.showValid}
                            onChange={() =>
                                setState((prev) => ({
                                    ...prev,
                                    showValid: !prev.showValid,
                                }))
                            }
                        />
                        <label className="form-check-label" htmlFor="valid">
                            Valid
                        </label>
                    </div>
                    <div className="form-check">
                        <input
                            className="form-check-input"
                            type="checkbox"
                            name="validity"
                            id="invalid"
                            checked={state.showInvalid}
                            onChange={() =>
                                setState((prev) => ({
                                    ...prev,
                                    showInvalid: !prev.showInvalid,
                                }))
                            }
                        />
                        <label className="form-check-label" htmlFor="invalid">
                            Invalid
                        </label>
                    </div>
                </div>

                <TextInputs
                    showValid={state.showValid}
                    showInvalid={state.showInvalid}
                />
                <ColorInputs
                    showValid={state.showValid}
                    showInvalid={state.showInvalid}
                />
                <NumberInputs
                    showValid={state.showValid}
                    showInvalid={state.showInvalid}
                />
                <OptionInputs
                    showValid={state.showValid}
                    showInvalid={state.showInvalid}
                />
                <ContextMenu />
            </div>
        </div>
    );
};

export default Page;
