/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState } from "react";

import {
    JoinedNumbersInput,
    JoinedRangeNumberInput,
    LinkedNumbersInput,
    NumberInput,
    RangeInput,
} from "components/form/inputs/number";

const NumberInputs = ({
    showValid,
    showInvalid,
}: {
    showValid: boolean;
    showInvalid: boolean;
}) => {
    const [number, setNumber] = useState(5);
    const [numbers, setNumbers] = useState([5, 7]);

    const components = [
        {
            Component: NumberInput,
            value: number,
            onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                setNumber(e.target.valueAsNumber),
        },
        {
            Component: RangeInput,
            value: number,
            onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                setNumber(e.target.valueAsNumber),
        },
        {
            Component: JoinedRangeNumberInput,
            value: number,
            onInput: (e: React.ChangeEvent<HTMLInputElement>) =>
                setNumber(e.target.valueAsNumber),
        },
        {
            Component: JoinedNumbersInput,
            values: numbers,
            onChange: (...args: any[]) => {
                setNumbers(args);
            },
        },
        {
            Component: LinkedNumbersInput,
            values: numbers,
            onChange: (...args: any[]) => {
                setNumbers(args);
            },
        },
    ];

    return components.map(({ Component, ...props }, index) => (
        <div key={index} className="mb-3 d-flex flex-column gap-2">
            <h3>{Component.name}</h3>
            <Component {...(props as any)} />
            {showValid && (
                <>
                    <Component
                        {...(props as any)}
                        isValid
                        validFeedback="correct"
                    />
                    <Component
                        {...(props as any)}
                        isValid
                        validFeedback="correct"
                        onReset={() => {}}
                    />
                </>
            )}
            {showInvalid && (
                <>
                    <Component
                        {...(props as any)}
                        isInvalid
                        invalidFeedback="wrong"
                    />
                    <Component
                        {...(props as any)}
                        isInvalid
                        invalidFeedback="wrong"
                        onReset={() => {}}
                    />
                </>
            )}
        </div>
    ));
};

export default NumberInputs;
