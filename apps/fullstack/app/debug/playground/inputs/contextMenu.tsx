import useMobileSafeContextMenu from "lib/hooks/useMobileSafeContextMenu";

import { Wrapper } from "./utils";

export const ContextMenu = () => {
    const eleProps = useMobileSafeContextMenu(() => {
        alert("context menu");
    });
    return (
        <Wrapper>
            <h3>ContextMenu</h3>
            <div
                {...eleProps}
                style={{
                    width: "100px",
                    height: "100px",
                    backgroundColor: "red",
                }}
            >
                Right click me or long press me on mobile
            </div>
        </Wrapper>
    );
};
