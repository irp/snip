"use client";

import { Toolbar } from "components/editor/tools/toolbar";

import styles from "./page.module.scss";

export default function Page() {
    return (
        <div className={styles.main}>
            <Toolbar />
        </div>
    );
}
