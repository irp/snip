"use client";

import { useState } from "react";

import styles from "./page.module.scss";

export default function Page() {
    const [fullScreen, setFullScreen] = useState(false);

    return (
        <div id="fs-element" className={styles.page}>
            <p>Im {fullScreen ? "in" : "not in"} fullscreen</p>
            <button
                onClick={() => {
                    if (fullScreen) {
                        document.exitFullscreen();
                    } else {
                        document
                            .getElementById("fs-element")
                            ?.requestFullscreen();
                    }

                    setFullScreen((prev) => {
                        document.documentElement.dataset.fullscreen =
                            String(!prev);
                        return !prev;
                    });
                }}
            >
                {fullScreen ? "Exit" : "Enter"} fullscreen
            </button>
            <div className={styles.nested}>Nested element</div>
        </div>
    );
}
