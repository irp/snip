import MarkdownEditor from "components/editor/markdown/textEditor";

export default function Page() {
    return (
        <div style={{ width: "800px" }} className="container mt-3">
            <MarkdownEditor initial_text="" />
        </div>
    );
}
