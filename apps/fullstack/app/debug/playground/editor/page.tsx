"use server";
import service from "@snip/database";
import { BaseData, BaseView, SnipData } from "@snip/snips/general/base";

import { EditorSinglePage } from "components/editor";

import styles from "../toolbar/page.module.scss";

export default async function Page() {
    /** Preload data */
    const id = 127;
    const bookData = await service.book.getById(id);
    const pages = await service.page.getByBookId(id);
    const queuedSnips = await service.snip.getQueuedByBookId(id);

    return (
        <div className={styles.main}>
            <EditorSinglePage
                bookData={bookData}
                pages={pages}
                queuedSnips={queuedSnips as SnipData<BaseData, BaseView>[]}
                perms={{
                    pRead: true,
                    pWrite: true,
                    pDelete: true,
                }}
            />
        </div>
    );
}
