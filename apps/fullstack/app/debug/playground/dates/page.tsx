"use client";

import { useDate } from "lib/hooks/useDate";
import React from "react";

import { DateFormat } from "@snip/common/dates";

export default function Page() {
    // Generate some dates
    const dates = [
        {
            date: new Date(), // Now
            label: "Now",
        },
        {
            date: new Date(new Date().getTime() - 12 * 60 * 60 * 1000), // 12 hours ago
            label: "12 hours ago",
        },
        {
            date: new Date(new Date().getTime() - 24 * 60 * 60 * 1000), // 1 day ago
            label: "1 day ago",
        },
        {
            date: new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000), // 1 week ago
            label: "1 week ago",
        },
        {
            date: new Date(new Date().getTime() - 30 * 24 * 60 * 60 * 1000), // 1 month ago
            label: "1 month ago",
        },
        {
            date: new Date(new Date().getTime() + 12 * 60 * 60 * 1000), // 12 hours in the future
            label: "12 hours in the future",
        },
        {
            date: new Date(new Date().getTime() + 24 * 60 * 60 * 1000), // 1 day in the future
            label: "1 day in the future",
        },
        {
            date: new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000), // 1 week in the future
            label: "1 week in the future",
        },
        {
            date: new Date(new Date().getTime() + 30 * 24 * 60 * 60 * 1000), // 1 month in the future
            label: "1 month in the future",
        },
    ];

    // Convert all to strings
    const dateStrings = dates.map((date) => {
        return {
            date: date.date.toString(),
            label: date.label,
        };
    });

    return (
        <div className="overflow-auto h-100 w-100 p-5">
            <h1>Date utils Test</h1>
            <div className="mb-3 d-flex gap-2 flex-column">
                {dateStrings.map(({ label, date }, index) => (
                    <div key={index}>
                        <h3>{label}</h3>
                        <DateComponent key={index} date={date} format="raw" />
                        <DateComponent
                            key={index}
                            date={date}
                            format="compact"
                        />
                        <DateComponent key={index} date={date} format="full" />
                    </div>
                ))}
            </div>
        </div>
    );
}

function DateComponent({ date, format }: { date: string; format: DateFormat }) {
    const ndate = useDate(date, format);
    return (
        <div className="mb-3 d-flex flex-column gap-2">
            {format}: {ndate}
        </div>
    );
}
