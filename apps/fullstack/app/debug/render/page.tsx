import service from "@snip/database";

import { Client, RequestPage } from "./client";
import { Server } from "./server";

type SearchParams = { [key: string]: string | undefined };
const Page = async ({ searchParams }: { searchParams: SearchParams }) => {
    const page_id = searchParams.page;

    let book_data = null;
    let page = null;
    if (page_id && !isNaN(parseInt(page_id))) {
        page = await service.page.getById(parseInt(page_id));
        book_data = await service.book.getById(page.book_id);
    } else {
        return null;
    }

    return (
        <div className="h-100 p-5">
            <RequestPage />
            <div className="d-flex gap-5 mt-5 justify-content-around">
                <Client book={book_data} page={page} />
                <Server page={page} />
            </div>
        </div>
    );
};

export default Page;
