"use client";

import Image from "next/image";

import { PageDataRet } from "@snip/database/types";

import { rerenderPage } from "./action";

import { SubmitButton } from "components/form/buttons";

export function Server({ page }: { page: PageDataRet }) {
    if (!page) {
        return null;
    }

    return (
        <form
            className="d-flex flex-column gap-3"
            action={() => rerenderPage(page.id)}
        >
            <Image
                className="border border-primary rounded"
                src={page.id.toString()}
                alt="Page"
                width={720}
                height={1028}
                loader={({ src, width }) => {
                    // 200 hardcoded for now
                    return `/render/${src}.jpeg?w=${width}&date=${page.last_updated?.valueOf()}`;
                }}
            />
            <SubmitButton className="btn btn-primary">Rerender</SubmitButton>
        </form>
    );
}
