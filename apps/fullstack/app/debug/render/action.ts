"use server";

import { getCookieString } from "@snip/auth/session";

export async function rerenderPage(page_id: number) {
    await fetch(`http://snip_images/rerender/${page_id}`, {
        method: "POST",
        headers: {
            cookie: await getCookieString({
                api: {
                    identifier: "socket",
                },
            }),
        },
    }).catch((e) => {
        console.error(e);
    });
}
