"use client";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";

import { BookDataRet, PageDataRet } from "@snip/database/types";
import { ViewPort } from "@snip/render/viewport";

import { Page } from "components/editor/viewer/page";
import { PageWorker } from "components/editor/worker/pageWorker";
import { SubmitButton } from "components/form/buttons";
import { TextInput } from "components/form/inputs/text";
export function RequestPage() {
    return (
        <form
            action={(formData: FormData) => {
                const page_id = formData.get("page") as string;
                if (page_id && !isNaN(parseInt(page_id))) {
                    // go to thispage?page=page_id
                    window.location.href = `/debug/render?page=${page_id}`;
                }
            }}
        >
            Select page to render:
            <TextInput
                label="Page id"
                name="page"
                placeholder="Page id"
                more={
                    <SubmitButton className="btn btn-primary">
                        Render
                    </SubmitButton>
                }
            />
        </form>
    );
}

export function Client({
    book,
    page,
}: {
    book: BookDataRet;
    page: PageDataRet;
}) {
    const [worker, setWorker] = useState<PageWorker>();
    useEffect(() => {
        if (!worker) {
            const viewport = new ViewPort();
            setWorker(new PageWorker(viewport));
        }
    }, [worker]);

    if (!page || !book) {
        return null;
    }

    return (
        <div className="d-flex flex-column gap-3">
            <Page
                className="border border-primary rounded"
                id="main"
                pageData={page}
            />
            <Button
                variant="primary"
                onClick={async () => {
                    await worker?.rerender();
                }}
            >
                Rerender
            </Button>
        </div>
    );
}
