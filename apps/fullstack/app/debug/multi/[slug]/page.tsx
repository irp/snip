"use client";
import Link from "next/link";
import { useEffect, useState } from "react";

import { randomAnimal } from "@snip/common/random/index";

export default function Page({ params }: { params: { slug: string } }) {
    const [state, setState] = useState(0);
    const [nextPage, setRandomAnimal] = useState<string | null>(null);

    useEffect(() => {
        setRandomAnimal(randomAnimal());
    }, []);

    return (
        <div style={{ padding: "1rem" }}>
            <div>Page Slug: {params.slug}</div>
            <div>Page State: {state}</div>
            <button onClick={() => setState(state + 1)}>Increment</button>
            <div>
                <Link href={"./" + nextPage}>Go to {nextPage}</Link>
            </div>
        </div>
    );
}
