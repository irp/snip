import { getSessionAction } from "lib/session";
import { NextResponse } from "next/server";

export const GET = async () => {
    const session = await getSessionAction();
    return NextResponse.json({ session });
};
