import { parseMDXFileFromSlug } from "./mdx_parsing";

/** Loads remote files from the filesystem
 * i.e. the docs folder.
 *
 * This does only allow request with files which
 * are in the docs folder.
 *
 * see generateStaticParams for the list of files
 */
export default async function RemoteMdxPage() {
    const { content } = await parseMDXFileFromSlug(["getting-started"]);
    return <>{content}</>;
}
