import { readdir, readFile, stat } from "fs/promises";
import { glob } from "glob";
import { compileMDX } from "next-mdx-remote/rsc";
import path from "path";
import { JSXElementConstructor, ReactElement } from "react";
import rehypeHighlight from "rehype-highlight";
import remarkGfm from "remark-gfm";
import remarkMath from "remark-math";
import remarkUnwrapImages from "remark-unwrap-images";

import { NavigationItem } from "components/common/Navbar";

const DOCS_FOLDER =
    process.env.NODE_ENV == "development" ? "../../docs" : "/docs";

export type FrontMatter = {
    label?: string;
    icon?: JSX.Element;
    priority?: number;
};

// Get all files from docs folder and return an array of paths
export async function getAllowedMDXFiles(
    folder: string | undefined,
    recursive = true,
): Promise<
    {
        slug: string[];
        frontmatter: FrontMatter;
        content: ReactElement<unknown, string | JSXElementConstructor<unknown>>;
    }[]
> {
    const ending = recursive ? "/**/*.mdx" : "/*.mdx";
    //glob all mdx files
    let files: string[];

    if (!folder) {
        files = await glob(path.join(DOCS_FOLDER, ending));
    } else {
        files = await glob(path.join(DOCS_FOLDER, folder, ending));
    }

    //map to array of paths
    const paths = await Promise.all(
        files.map(async (file) => {
            const path = file
                .replace(`${DOCS_FOLDER}/`, "")
                .replace(".mdx", "");

            //get frontmatter
            // Optionally provide a type for your frontmatter object
            const { content, frontmatter } = await compileMDX<FrontMatter>({
                source: await readFile(file, "utf-8"),
                options: { parseFrontmatter: true },
            });

            return { slug: path.split("/"), frontmatter, content };
        }),
    );

    return paths.sort((a, b) => {
        if (a.frontmatter.priority && b.frontmatter.priority) {
            return b.frontmatter.priority - a.frontmatter.priority;
        } else if (a.frontmatter.priority) {
            return -1;
        } else if (b.frontmatter.priority) {
            return 1;
        } else {
            return 0;
        }
    });
}

export async function getFoldersInDir(dirPath?: string): Promise<string[]> {
    if (!dirPath) {
        dirPath = DOCS_FOLDER;
    }

    try {
        const files = await readdir(dirPath);
        const folders = [];

        for (const file of files) {
            const filePath = path.join(dirPath, file);
            const fileStat = await stat(filePath);

            if (fileStat.isDirectory()) {
                folders.push(file);
            }
        }

        return folders;
    } catch (error) {
        console.error("Error reading directory:", error);
        return [];
    }
}

export async function getSideBarItems(
    folder: string,
    recursive = true,
): Promise<NavigationItem[]> {
    const allowedFiles = await getAllowedMDXFiles(folder, recursive);

    return allowedFiles.map(({ slug, frontmatter }) => {
        return {
            label: frontmatter.label || "No label",
            href: `/docs/${slug.join("/")}`,
        };
    });
}

import rehypeKatex from "rehype-katex";

import { getComponents } from "components/docs/components";

/**
 * Parses an MDX file and returns the compiled result.
 * @param source - The source code of the MDX file.
 * @param file_path - The file path of the MDX file.
 * @returns A promise that resolves to the compiled MDX result.
 */
export async function parseMDXFileFromSlug(slug: string[]) {
    const pa = path.join(
        DOCS_FOLDER,
        ...slug.slice(0, -1),
        slug.at(-1) + ".mdx",
    );

    const source = await readFile(pa, "utf-8");
    const components = getComponents(pa);

    return await compileMDX<FrontMatter>({
        source: source,
        options: {
            parseFrontmatter: true,
            mdxOptions: {
                remarkPlugins: [remarkGfm, remarkUnwrapImages, remarkMath],
                rehypePlugins: [
                    // @ts-expect-error highlight
                    [rehypeHighlight],
                    // @ts-expect-error katex
                    [rehypeKatex, { strict: true, throwOnError: true }],
                ],
            },
        },
        components,
    });
}
