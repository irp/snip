import type { Metadata } from "next";
import { ReactNode } from "react";
import { Container, Row } from "react-bootstrap";

import { getFoldersInDir, getSideBarItems } from "./mdx_parsing";

import { NavigationItem } from "components/common/Navbar";
import {
    ContentWrapper,
    NavigationFolder,
    SidebarFolders,
} from "components/common/Sidebar";

import "highlight.js/styles/stackoverflow-light.css";
import styles from "./layout.module.scss";

const folder2label = {
    user: { label: "User Guide", priority: 1 },
    deployment: { label: "Deployment", priority: 2 },
    development: { label: "Development", priority: 3 },
};
type FolderKey = "user" | "deployment" | "development";

export default async function DocsLayout({
    children,
}: {
    children: ReactNode;
}) {
    const folderStrings = await getFoldersInDir();
    const folders: (NavigationFolder | NavigationItem)[] = await Promise.all(
        folderStrings.map(async (folder) => {
            const childs = await getSideBarItems(folder);
            if (childs.length === 0) {
                return {
                    href: "",
                    label: folder2label[folder as FolderKey]?.label || folder,
                    children: [],
                    priority: folder2label[folder as FolderKey]?.priority || 0,
                };
            }
            return {
                href: childs[0]!.href,
                label: folder2label[folder as FolderKey]?.label || folder,
                children: childs,
                priority: folder2label[folder as FolderKey]?.priority || 0,
            };
        }),
    );

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const sorted = folders.sort((a: any, b: any) => a.priority - b.priority);

    const navItemsRoot = await getSideBarItems("", false);
    navItemsRoot[0]!.href = "/docs/getting-started";
    //Insert the root items at the beginning of the folders
    sorted.unshift(navItemsRoot[0]!);

    return (
        <main className={styles.main}>
            <SidebarFolders
                folders={sorted}
                back_type="home"
                mobile_type="popup"
            />
            <ContentWrapper>
                <Container className={styles.wrapper}>
                    <Row>{children}</Row>
                </Container>
            </ContentWrapper>
        </main>
    );
}

export const metadata: Metadata = {
    title: "Documentation - Snip",
};
