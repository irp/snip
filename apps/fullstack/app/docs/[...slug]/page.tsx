import Link from "next/link";
import { Button } from "react-bootstrap";
import { FaChevronLeft, FaChevronRight } from "react-icons/fa";

import { getAllowedMDXFiles, parseMDXFileFromSlug } from "../mdx_parsing";

import "katex/dist/katex.min.css";

/** Loads remote files from the filesystem
 * i.e. the docs folder.
 *
 * This does only allow request with files which
 * are in the docs folder.
 *
 * see generateStaticParams for the list of files
 */
export default async function RemoteMdxPage({
    params,
}: {
    params: {
        slug: string[];
    };
}) {
    // Read the file from the filesystem
    const { content } = await parseMDXFileFromSlug(params.slug);

    const folder = params.slug[0]!;
    const allowed = await getAllowedMDXFiles(folder, false);
    const thisIdx = allowed.findIndex(
        (file) => file.slug.join("/") === params.slug.join("/"),
    );
    const next = allowed[thisIdx + 1];
    const previous = allowed[thisIdx - 1];

    return (
        <>
            {content}
            <div className="d-flex justify-content-between mt-3">
                {previous && (
                    <Link href={`./${previous.slug.at(-1)}`}>
                        <Button variant="outline-secondary">
                            <FaChevronLeft />
                            {previous?.frontmatter?.label || "Previous"}
                        </Button>
                    </Link>
                )}
                {next && (
                    <Link
                        href={`./${next.slug.at(-1)}`}
                        style={{ marginLeft: "auto" }}
                    >
                        <Button variant="outline-secondary">
                            {next?.frontmatter?.label || "Next"}
                            <FaChevronRight />
                        </Button>
                    </Link>
                )}
            </div>
        </>
    );
}

export const dynamicParams = false;
export const dynamic = "force-dynamic";
export async function generateStaticParams() {
    const allowedParams = await getAllowedMDXFiles(undefined);

    return allowedParams.map(({ slug }) => {
        return { slug };
    });
}
