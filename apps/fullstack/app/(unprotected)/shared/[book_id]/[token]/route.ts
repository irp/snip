import { getSessionAction } from "lib/session";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import { UI_TOKEN_LENGTH_IN_CHARS } from "@snip/auth/tokens/ui";
import { verifyUIToken } from "@snip/auth/tokens/ui";
import { TokenData } from "@snip/database/types";

export const GET = resolveSharedLink;

const schema = z
    .object({
        token: z.string().length(UI_TOKEN_LENGTH_IN_CHARS),
        book_id: z.coerce.number().int(),
    })
    .strict();

async function resolveSharedLink(
    req: NextRequest,
    { params }: { params: { book_id: string; token: string } },
) {
    const url = req.nextUrl.clone();
    url.protocol = req.headers.get("x-forwarded-proto") || url.protocol;
    url.host = req.headers.get("x-forwarded-host") || url.host;
    url.port = req.headers.get("x-forwarded-port") || url.port;
    // Validate ui token
    const res = schema.safeParse(params);

    if (res.success === false) {
        return NextResponse.json(
            {
                error: "Invalid token",
                details: "The token is invalid",
            },
            { status: 400 },
        );
    }

    const { book_id, token } = res.data;
    let tokenData: TokenData;
    try {
        tokenData = await verifyUIToken(token, book_id);
    } catch (_e) {
        return NextResponse.json(
            {
                error: "Invalid token",
                details: "The token is invalid",
            },
            { status: 400 },
        );
    }

    if (tokenData.expires_at! < new Date()) {
        return NextResponse.redirect(new URL(`/expired`, url));
    }

    const session = await getSessionAction();
    session.anonymous = {
        tokens: [
            ...(session.anonymous?.tokens.filter(
                (t) => t.id !== tokenData.id,
            ) || []),
            {
                id: tokenData.id!,
                book_id: tokenData.book_id,
                expires_at: tokenData.expires_at!.getTime(),
            },
        ],
    };
    await session.save();

    return NextResponse.redirect(new URL(`/books/${book_id}`, url));
}
