"use server";
import { getSessionAction } from "lib/session";
import Link from "next/link";

import service from "@snip/database";
import { getPrimaryEmail } from "@snip/database/services/user";

import { ResendForm, VerifyForm } from "./form";

export default async function VerifyPage() {
    const session = await getSessionAction();
    if (!session.user) {
        return (
            <div className="d-flex flex-grow-1 justify-content-center align-items-center flex-column">
                <h1>Not logged in</h1>
                <p>
                    We could not read the user data from the session cookie.
                    Please login first to verify your email.
                </p>
                <Link href="/frontpage">Go back to home</Link>
            </div>
        );
    }
    const user = await service.user.getById(session.user.id);

    if (user.emails_verified.every((e) => e)) {
        return (
            <div className="d-flex flex-grow-1 justify-content-center align-items-center flex-column">
                <h1>Email verified</h1>
                <p>Your email is verified.</p>
                <Link href="/frontpage">Go back to home</Link>
            </div>
        );
    }

    return (
        <div className="d-flex flex-grow-1 justify-content-center align-items-center flex-column">
            <VerifyForm />
            <ResendForm email={getPrimaryEmail(user) || "Unknown"} />
        </div>
    );
}
