"use client";

import { verifyEmailAction } from "app/api/(actions)/verifyEmailAction";
import { useRedirect } from "lib/hooks/useRedirect";
import { FormEventHandler, useState } from "react";
import { InputGroup } from "react-bootstrap";
import { useFormState } from "react-dom";

import { Form, FormHeader } from "components/form";
import { FormError } from "components/form/alert";
import { SubmitButton } from "components/form/buttons";
import { TextInput } from "components/form/inputs/text";

import styles from "./verifyForm.module.scss";

export function ResendForm({ email }: { email: string }) {
    const [pending, setPending] = useState(false);
    const [emailSend, setEmailSend] = useState(false);
    const [error, setError] = useState(null);

    const onSubmit: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();
        setPending(true);
        const res = await fetch("/email/account/verifyEmail", {
            method: "POST",
        });
        const body = await res.json();
        if (!res.ok) {
            setError(body.details || body.error);
            setPending(false);
            return;
        }
        setEmailSend(true);

        setTimeout(() => {
            setPending(false);
        }, 2000);
    };

    return (
        <Form data-glassmorphism="false" onSubmit={onSubmit}>
            <p className="mt-3 text-center">You did not receive an email?</p>
            <FormError>{error}</FormError>
            <SubmitButton
                className="btn btn-outline-secondary"
                style={{ width: "350px", height: "4rem" }}
                pending={pending}
                disabled={emailSend}
            >
                {emailSend ? (
                    <span>We send you an email to</span>
                ) : (
                    <span>Resend verification email to</span>
                )}
                <br></br>
                <code>{email}</code>
            </SubmitButton>
        </Form>
    );
}

export function VerifyForm() {
    const [state, formAction] = useFormState(verifyEmailAction, null);

    // Redirect to default page if logged in
    const pending_direct = useRedirect(
        "/frontpage",
        state != null && "success" in state,
    );

    return (
        <Form action={formAction}>
            <FormHeader>Verify your email!</FormHeader>
            <FormError>
                {state !== null &&
                    "error" in state &&
                    state.error.name + " " + state.error.details}
            </FormError>
            <p>
                We have sent you an email with a verification link. Please click
                on the link to verify your email. Alternatively, you can enter
                the verification code we send in that email below.
            </p>
            <InputGroup className={styles.inputGroup}>
                <TextInput
                    name="token"
                    label="Verification code"
                    style={{
                        flexGrow: "1",
                        borderTopRightRadius: 0,
                        borderBottomRightRadius: 0,
                        minHeight: "8rem",
                        height: "max-content",
                        resize: "none",
                        wordBreak: "break-all",
                    }}
                    autoComplete="off"
                    autoCorrect="off"
                    spellCheck="false"
                    as="textarea"
                />
                <SubmitButton
                    style={{ flexShrink: "0" }}
                    className="btn btn-primary"
                    pending={pending_direct}
                >
                    Verify
                </SubmitButton>
            </InputGroup>
        </Form>
    );
}
