import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { NextRequest, NextResponse } from "next/server";

import {
    unsealEmailVerifyToken,
    VerifyEmailToken,
} from "@snip/auth/tokens/emailVerify";
import { SnipSessionData } from "@snip/auth/types";
import service from "@snip/database";

export const GET = wrapperLoggedInUser(GetVerify);

async function GetVerify(
    req: NextRequest,
    props: DeepPartialExcept<RequestProps, "session.user">,
): Promise<NextResponse<unknown>> {
    const url = req.nextUrl.clone();
    url.protocol = req.headers.get("x-forwarded-proto") || url.protocol;
    url.host = req.headers.get("x-forwarded-host") || url.host;
    url.port = req.headers.get("x-forwarded-port") || url.port;

    const { searchParams } = new URL(req.url);
    const session = props.session as SnipSessionData;
    let token = searchParams.get("token") ?? undefined;

    // Redirect to verify email page if no token is provided
    if (!token) {
        return NextResponse.redirect(new URL("/verify/email", url));
    }

    if (!session.user) {
        return NextResponse.json({ error: "User not found" }, { status: 400 });
    }

    token = token.replace(/\s/g, "");

    let tokenData: VerifyEmailToken;
    try {
        tokenData = await unsealEmailVerifyToken(token, props.session.user.id);
    } catch (_error) {
        return NextResponse.json({ error: "Invalid token" }, { status: 400 });
    }

    await service.user.verifySignupWithPassword(tokenData.email);
    session.user.email_verified = true;
    await session.save();
    return NextResponse.redirect(new URL("/frontpage", url));
}
