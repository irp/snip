import type { Metadata } from "next/types";

import { SignupForm } from "./signupForm";

export default function SignUp() {
    return (
        <div className="d-flex flex-grow-1 justify-content-center align-items-center">
            <SignupForm />
        </div>
    );
}

export const metadata: Metadata = {
    title: "SignUp",
};
