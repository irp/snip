"use client";

import {
    SignupState,
    signupWithPasswordAction,
} from "app/api/(actions)/signupAction";
import { useRedirect } from "lib/hooks/useRedirect";
import Link from "next/link";
import { useFormState } from "react-dom";

import { Form, FormHeader } from "components/form";
import { FormError } from "components/form/alert";
import { SecondaryButton, SubmitButton } from "components/form/buttons";
import { EmailInput, PasswordInput } from "components/form/inputs/text";

export function SignupForm() {
    const [state, formAction] = useFormState(signupWithPasswordAction, null);

    let valid: SignupState["valid"] = {
        email: undefined,
        password: undefined,
        confirmPassword: undefined,
    };
    if (state !== null && "valid" in state) {
        valid = state.valid;
    }

    // Redirect to default page if logged in
    const pending_direct = useRedirect("/", state !== null && "user" in state);

    return (
        <Form action={formAction}>
            <FormHeader>Create an account</FormHeader>
            <FormError>
                {state &&
                    "error" in state &&
                    state.error.name + " " + state.error.details}
            </FormError>
            <EmailInput
                isValid={valid.email != undefined && valid.email}
                isInvalid={valid.email != undefined && !valid.email}
            />
            <PasswordInput
                isValid={valid.password != undefined && valid.password}
                isInvalid={valid.password != undefined && !valid.password}
            />
            <PasswordInput
                name="confirmPassword"
                label="Confirm Password"
                invalidFeedback="Passwords do not match!"
                isValid={
                    valid.confirmPassword != undefined && valid.confirmPassword
                }
                isInvalid={
                    valid.confirmPassword != undefined && !valid.confirmPassword
                }
            />
            <div className="d-flex align-items-center text-center mb-3 flex-column">
                <SubmitButton style={{ width: "90%" }} pending={pending_direct}>
                    Sign up
                </SubmitButton>
            </div>
            <div className="d-flex align-items-center justify-content-center">
                <p className="mb-0 me-2">Already have an account?</p>
                <Link href="/login">
                    <SecondaryButton>Login</SecondaryButton>
                </Link>
            </div>
        </Form>
    );
}
