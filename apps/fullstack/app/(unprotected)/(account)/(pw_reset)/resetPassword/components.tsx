"use client";
import Link from "next/link";
import { Dispatch, FormEventHandler, SetStateAction, useState } from "react";

import { Form, FormHeader } from "components/form";
import { FormError } from "components/form/alert";
import { SecondaryButton, SubmitButton } from "components/form/buttons";
import {
    EmailInput,
    PasswordInput,
    TextInput,
} from "components/form/inputs/text";
import { validatePW } from "components/form/validation";

export function ResetPasswordForm({
    token,
    email,
}: {
    token: string;
    email: string;
}) {
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [error, setError] = useState("");
    const [success, setSuccess] = useState(false);

    const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
        event.preventDefault();

        if (password !== confirmPassword) {
            setError("Passwords do not match");
            return;
        }

        try {
            await submitForValidation(token, email, password);
            setSuccess(true);
        } catch (e) {
            if (e instanceof Error) {
                setError(e.message);
            }
        }
    };
    const pw_valid = validatePW(password);

    return (
        <Form onSubmit={handleSubmit} style={{ minWidth: "500px" }}>
            <FormHeader>Reset your password</FormHeader>
            <FormError>{error}</FormError>

            {!success ? (
                <>
                    <PasswordInput
                        isValid={pw_valid}
                        isInvalid={!pw_valid}
                        onInput={(e: React.ChangeEvent<HTMLInputElement>) =>
                            setPassword(e.target.value)
                        }
                    />
                    <PasswordInput
                        label="Confirm Password"
                        name="confirmPassword"
                        isValid={password == confirmPassword && pw_valid}
                        isInvalid={password != confirmPassword || !pw_valid}
                        onInput={(e: React.ChangeEvent<HTMLInputElement>) =>
                            setConfirmPassword(e.target.value)
                        }
                    />
                    <div className="d-flex align-items-center text-center mb-3 flex-column">
                        <button
                            className="btn btn-primary btn-block fa-lg gradient-custom-2 mb-2"
                            style={{ width: "90%" }}
                            type="submit"
                        >
                            Reset Password
                        </button>
                    </div>
                </>
            ) : (
                <>
                    <div>Password was changed successfully!</div>
                    <div className="d-flex align-items-center text-center flex-column mb-3">
                        <Link href="/login">
                            <SubmitButton style={{ width: "90%" }}>
                                Login
                            </SubmitButton>
                        </Link>
                    </div>
                </>
            )}
        </Form>
    );
}

function TokenForm({
    token,
    setToken,
    email,
    setEmail,
    submit,
    error,
}: {
    token: string;
    setToken: Dispatch<SetStateAction<string>>;
    email: string;
    setEmail: Dispatch<SetStateAction<string>>;
    submit: (token: string, email: string) => Promise<void>;
    error?: string;
}) {
    const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
        event.preventDefault();
        // Handle token submission logic here
        await submit(token, email);
    };

    return (
        <Form onSubmit={handleSubmit}>
            <FormHeader>
                To reset your password please input the token sent to your email
            </FormHeader>
            <FormError>{error}</FormError>

            <EmailInput
                onInput={(e: React.ChangeEvent<HTMLInputElement>) =>
                    setEmail(e.target.value)
                }
                value={email}
            />
            <TextInput
                label="Token"
                name="token"
                value={token}
                onInput={(e: React.ChangeEvent<HTMLInputElement>) =>
                    setToken(e.target.value)
                }
                autoComplete="off"
            />

            <div className="d-flex align-items-center text-center flex-column mb-3">
                <SubmitButton style={{ width: "90%" }}>Submit</SubmitButton>
            </div>
            <div className="d-flex align-items-center justify-content-center">
                <p className="mb-0 me-2">Did&apos;t receive an email?</p>
                <Link href="/forgotPassword">
                    <SecondaryButton>Request new</SecondaryButton>
                </Link>
            </div>
        </Form>
    );
}

export function ResetPasswordFullForm({
    token,
    email,
}: {
    token: string;
    email: string;
}) {
    const [token_, setToken] = useState(token);
    const [email_, setEmail] = useState(email);
    const [error, setError] = useState<string>();
    const [tokenValid, setTokenValid] = useState(false);

    async function submit(token: string, email: string) {
        try {
            const valid = await submitForValidation(token, email);
            setTokenValid(valid);
            setToken(token);
            setEmail(email);
        } catch (e) {
            if (e instanceof Error) {
                setError(e.message);
            }
            setTimeout(() => {
                setError(undefined);
            }, 10000);
        }
    }

    if (tokenValid) {
        return <ResetPasswordForm token={token_} email={email_} />;
    }
    return (
        <TokenForm
            token={token_}
            email={email_}
            setToken={setToken}
            setEmail={setEmail}
            submit={submit}
            error={error}
        />
    );
}

export async function submitForValidation(
    token: string,
    email: string,
    password?: string,
): Promise<true> {
    token.replaceAll(" ", "");

    const data = {
        token: token.replaceAll(" ", ""),
        email,
    } as { token: string; email: string; password?: string };
    if (password) {
        data.password = password;
    }

    const response = await fetch("email/account/updatePassword", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
    }).then((res) => res.json());

    if (response.valid || response.success) {
        return true;
    } else {
        throw new Error(response.details);
    }
}
