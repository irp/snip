"use client";
import { useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";

import {
    ResetPasswordForm,
    ResetPasswordFullForm,
    submitForValidation,
} from "./components";

export default function ResetPassword() {
    const searchParams = useSearchParams();
    const [validToken, setValidToken] = useState(false);

    useEffect(() => {
        if (!searchParams.has("token")) return;
        if (!searchParams.has("email")) return;

        submitForValidation(
            searchParams.get("token") as string,
            searchParams.get("email") as string,
        )
            .then(() => {
                setValidToken(true);
            })
            .catch(() => {
                setValidToken(false);
            });
    }, [searchParams]);

    if (validToken) {
        return (
            <ResetPasswordForm
                token={searchParams.get("token") as string}
                email={searchParams.get("email") as string}
            />
        );
    }
    return (
        <ResetPasswordFullForm
            token={searchParams.get("token") as string}
            email={searchParams.get("email") as string}
        />
    );
}
