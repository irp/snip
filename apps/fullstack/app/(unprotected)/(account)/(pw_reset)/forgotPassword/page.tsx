"use client";
import Link from "next/link";
import { FormEventHandler, useState } from "react";

import { Form, FormHeader } from "components/form";
import { FormError } from "components/form/alert";
import { EmailInput } from "components/form/inputs/text";
import { validateEmail } from "components/form/validation";

import styles from "components/form/Form.module.scss";

export default function ForgotPassword() {
    const [email, setEmail] = useState("");
    const [success, setSuccess] = useState(false);
    const [error, setError] = useState("");

    const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
        event.preventDefault();

        const response = await fetch("/email/account/passwordReset/" + email, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email,
            }),
        }).then((res) => res.json());

        if (response.success) {
            setSuccess(true);
            //setEmail("");
        } else {
            setError("Could not reset email, does the email exist?");
        }
    };

    const email_valid = validateEmail(email);

    if (success) {
        return (
            <div className={styles.wrapper}>
                <div className={styles.form}>
                    <div className={styles.header}>
                        An email has been sent to{" "}
                        <strong>{email || "you"}</strong> with instructions on
                        how to reset your password.
                    </div>
                    <Link href="/account/login">Back to login</Link>
                </div>
            </div>
        );
    }

    return (
        <Form onSubmit={handleSubmit}>
            <FormHeader>Reset your password</FormHeader>
            <FormError>{error}</FormError>
            <EmailInput
                isValid={email_valid}
                isInvalid={!email_valid}
                invalidFeedback="Not a valid email address."
                onInput={(e: React.ChangeEvent<HTMLInputElement>) =>
                    setEmail(e.target.value)
                }
            />

            <div className="d-flex align-items-center text-center flex-column">
                <button
                    className="btn btn-primary btn-block fa-lg gradient-custom-2 mb-2"
                    style={{ width: "90%" }}
                    type="submit"
                >
                    Submit
                </button>
                <Link className="text-muted text-center" href="/account/login">
                    Back to login
                </Link>
            </div>
        </Form>
    );
}
