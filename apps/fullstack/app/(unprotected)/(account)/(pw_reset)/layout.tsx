export default function PwResetLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <div className="d-flex h-100 justify-content-center align-items-center">
            {children}
        </div>
    );
}
