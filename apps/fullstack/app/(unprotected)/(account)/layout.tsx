import { ReactNode } from "react";

import BackButton from "components/common/other/BackButton";

export default function CenterLayout({ children }: { children: ReactNode }) {
    return (
        <div className="d-flex flex-column flex-grow-1 p-2">
            {children}
            <BackButton
                variant="outline-primary"
                className="align-self-start"
            />
        </div>
    );
}
