"use client";

import { loginWithPasswordAction } from "app/api/(actions)/loginAction";
import { useRedirect } from "lib/hooks/useRedirect";
import Link from "next/link";
import { Button } from "react-bootstrap";
import { useFormState } from "react-dom";

import { Form, FormHeader } from "components/form";
import { FormError } from "components/form/alert";
import { SecondaryButton, SubmitButton } from "components/form/buttons";
import { EmailInput, PasswordInput } from "components/form/inputs/text";

export function LoginForm({ sso_enabled }: { sso_enabled: boolean }) {
    const [state, formAction] = useFormState(loginWithPasswordAction, null);

    // Redirect to default page if logged in
    const pending_direct = useRedirect("/", state !== null && "user" in state);

    return (
        <Form action={formAction}>
            <FormHeader>Please login to your account</FormHeader>
            <EmailInput />
            <PasswordInput />
            <FormError>
                {state !== null && "error" in state && state.error.details}
            </FormError>

            <div className="d-flex align-items-center text-center mb-3 flex-column">
                <SubmitButton style={{ width: "90%" }} pending={pending_direct}>
                    Log in
                </SubmitButton>
                {
                    // Render the SSO providers
                    sso_enabled && (
                        <Link href="/sso" passHref legacyBehavior prefetch>
                            <Button
                                variant="outline-dark"
                                style={{ width: "90%", marginBottom: "0.5rem" }}
                            >
                                Log in with SSO
                            </Button>
                        </Link>
                    )
                }
                <Link className="text-muted" href="/forgotPassword">
                    Forgot password?
                </Link>
            </div>

            <div className="d-flex align-items-center justify-content-center">
                <p className="mb-0 me-2">Don&apos;t have an account?</p>
                <Link href="/signup">
                    <SecondaryButton>Create new</SecondaryButton>
                </Link>
            </div>
        </Form>
    );
}
