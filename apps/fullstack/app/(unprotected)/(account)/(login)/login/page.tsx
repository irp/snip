import type { Metadata } from "next/types";

import { loadSSOConfig } from "../sso/ssoProviders";
import { LoginForm } from "./loginForm";

export default async function Login() {
    const sso_enabled = loadSSOConfig().enabled;

    return (
        <div className="d-flex flex-grow-1 justify-content-center align-items-center h-100">
            <LoginForm sso_enabled={sso_enabled} />
        </div>
    );
}

export const metadata: Metadata = {
    title: "Login",
};
