import fs from "fs/promises";
import path from "path";
import z from "zod";

import { config } from "@snip/config/server";

import { BackToLoginBtn, SSO } from "./component";

import { Form, FormHeader } from "components/form";

export type SSOProviderServer = z.infer<
    typeof config.sso.schema
>["providers"][number];
export type SSOProvider = Omit<
    Omit<SSOProviderServer, "clientId">,
    "clientSecret"
>;

export async function ProvidersForm({
    providers,
}: {
    providers: SSOProvider[];
}) {
    return (
        <Form>
            <FormHeader>Please choose Single-Sign-On provider</FormHeader>

            <div className="d-flex align-items-center text-center mb-3 flex-column gap-2">
                {
                    // Render the SSO providers
                    providers.map((provider) => (
                        <SSO key={provider.providerId} {...provider} />
                    ))
                }
            </div>

            <div className="d-flex align-items-center justify-content-center flex-column">
                <BackToLoginBtn />
            </div>
        </Form>
    );
}

export function loadSSOConfig() {
    config.sso.reload();
    return config.sso.parse();
}

export async function parseSSOProviderForUi(
    providers: SSOProviderServer[],
): Promise<SSOProvider[]> {
    // Copy providers
    const providers_copy = structuredClone(providers);

    // Check if img paths are valid and load image as data uri
    for (const provider of providers_copy) {
        const imgPath = path.join(config.config_dir, provider.img);
        if (!(await exists(imgPath))) {
            console.warn(
                `Image file ${imgPath} does not exist! Using placeholder.`,
            );
            provider.img = "/img/favicon/favicon-128x128-light.png";
            continue;
        }

        const img = await fs.readFile(imgPath);
        provider.img = `data:image/png;base64,${img.toString("base64")}`;
        provider.clientSecret = "redacted";
    }
    return providers_copy;
}

async function exists(f: string) {
    try {
        await fs.stat(f);
        return true;
    } catch {
        return false;
    }
}
