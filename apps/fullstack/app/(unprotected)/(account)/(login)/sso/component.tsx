"use client";
import Image from "next/image";
import Link from "next/link";
import { Button, Card } from "react-bootstrap";

import type { SSOProvider } from "./ssoProviders";

import styles from "./sso.module.scss";

export function SSO({ name, description, img, providerId, type }: SSOProvider) {
    return (
        <Card className={styles.wrapper}>
            <div className={styles.inner}>
                <SSOBrand src={img} />
                <div>
                    <div className={styles.title}>{name}</div>
                    <div className={styles.description}>{description}</div>
                </div>
            </div>
            <div className={styles.buttons}>
                <LoginLinkButton
                    providerId={providerId}
                    name={name}
                    type={type}
                />
            </div>
        </Card>
    );
}

function SSOBrand({ src }: { src: string }) {
    return (
        <div className={styles.img_wrapper}>
            <Image
                src={src}
                alt=""
                width={96}
                height={96}
                style={{ objectFit: "contain" }}
            />
        </div>
    );
}

export function BackToLoginBtn() {
    return (
        <Link href="/login" passHref legacyBehavior prefetch>
            <Button variant="outline-dark" style={{ width: "90%" }}>
                Back to standard login
            </Button>
        </Link>
    );
}

function LoginLinkButton({
    type,
    name,
    providerId,
}: {
    type: SSOProvider["type"];
    name: SSOProvider["name"];
    providerId: SSOProvider["providerId"];
}) {
    let href: string;
    if (type === "oidc") {
        href = `/sso/oidc/login_redirect?providerId=${providerId}`;
    } else {
        throw new Error(`Unsupported SSO provider type: ${type}`);
    }

    return (
        <a href={href}>
            <Button variant="primary">Use {name} for login</Button>
        </a>
    );
}
