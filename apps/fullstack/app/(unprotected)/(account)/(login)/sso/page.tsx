import { Metadata } from "next/types";

import { BackToLoginBtn } from "./component";
import {
    loadSSOConfig,
    parseSSOProviderForUi,
    ProvidersForm,
} from "./ssoProviders";

export default async function LoginSSO() {
    // Get available sso providers from yml file
    const conf = await loadSSOConfig();
    const providers = await parseSSOProviderForUi(conf.providers);

    if (conf.enabled === false) {
        return (
            <div className="d-flex flex-column flex-grow-1 justify-content-center align-items-center w-100">
                <div>
                    <p>
                        Single-Sign-On is not enabled for this deployment.
                        Please contact your administrator.
                    </p>
                    <div className="d-flex align-items-center justify-content-center flex-column">
                        <BackToLoginBtn />
                    </div>
                </div>
            </div>
        );
    }

    if (providers.length === 0) {
        return (
            <div className="d-flex flex-column flex-grow-1 justify-content-center align-items-center w-100">
                <div>
                    <p>
                        This Deployment does not have any SSO providers
                        configured. Please contact your administrator.
                    </p>
                    <div className="d-flex align-items-center justify-content-center flex-column">
                        <BackToLoginBtn />
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div className="d-flex flex-column flex-grow-1 justify-content-center align-items-center">
            <ProvidersForm providers={providers} />
        </div>
    );
}

export const metadata: Metadata = {
    title: "Login SSO",
};
