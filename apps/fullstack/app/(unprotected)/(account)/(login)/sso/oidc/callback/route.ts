import { unknownErrorToResponse } from "lib/errorparsing";
import { cookies, headers } from "next/headers";
import { NextRequest, NextResponse } from "next/server";
import { UserInfoResponse } from "openid-client";

import { getSession } from "@snip/auth/session";
import config from "@snip/config/server";
import service from "@snip/database";
import { UserDataRet } from "@snip/database/types";

import { loadSSOConfig } from "../../ssoProviders";
import { CookieSchema, getAccessToken, getUserInfo, Token } from "../oidc";

/** Callback or redirect url
 * which is used to handle the response from the
 * sso provider.
 *
 * Sometimes also called redirect uri.
 */
export async function GET(req: NextRequest) {
    // check if cookie is set
    const cookieStore = cookies();
    const cookie = cookieStore.get("sso_login");
    if (!cookie) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: "No cookie found for sso flow. Can't continue!",
            },
            {
                status: 400,
            },
        );
    }

    // Parse the cookie
    let data;
    try {
        data = JSON.parse(atob(cookie.value));
    } catch (e) {
        return unknownErrorToResponse("Failed to parse cookie", e, 400);
    }

    // Validate the cookie
    const pCookie = CookieSchema.safeParse(data);
    if (pCookie.success == false) {
        return NextResponse.json(
            { error: "Invalid cookie value", details: pCookie.error },
            {
                status: 400,
            },
        );
    }
    const cookieData = pCookie.data;

    /* ---------------------------- Provider requests --------------------------- */

    // Get provider by id
    const conf = await loadSSOConfig();
    const provider = conf.providers.find(
        (p) => p.providerId === cookieData.providerId,
    );
    if (!provider) {
        return NextResponse.json(
            { error: "Invalid provider", details: "Provider not found" },
            {
                status: 400,
            },
        );
    }

    // Get the access token
    let token: Token;
    try {
        const redirectUrl = config.getServerUrl();
        redirectUrl.pathname = req.nextUrl.pathname;
        redirectUrl.search = req.nextUrl.search;
        token = await getAccessToken(redirectUrl, cookieData, provider);
    } catch (e: unknown) {
        console.error(e);
        return unknownErrorToResponse("Failed to get access token", e, 400);
    }

    // Get user info
    let userInfo: UserInfoResponse;
    try {
        userInfo = await getUserInfo(token, provider);

        if (
            userInfo.email == undefined ||
            userInfo.email_verified == undefined
        ) {
            throw new Error(
                "OpenID Connect UserInfo endpoint did not return email",
            );
        }
    } catch (e: unknown) {
        return unknownErrorToResponse("Failed to get user info", e, 400);
    }
    const { email, email_verified, sub } = userInfo;

    /* ---------------------- Create account or login user ---------------------- */

    // Check if email is verified
    if (!email_verified) {
        return NextResponse.json(
            {
                error: "Email not verified",
                details:
                    "Please verify your email address with the provider first!",
            },
            {
                status: 400,
            },
        );
    }

    // Check if user exists and login
    // if the user does not exist, create an user
    let user: UserDataRet;
    try {
        user = await service.user
            .loginWithSSO(cookieData.providerId, sub)
            .catch(async (e: unknown) => {
                if (e instanceof Error && e.message.includes("not found")) {
                    // Create user
                    return await service.user.signupWithSSO(
                        cookieData.providerId,
                        sub,
                        email,
                        email_verified,
                    );
                } else {
                    throw e;
                }
            });
    } catch (e: unknown) {
        return unknownErrorToResponse("Failed to login or create user", e, 500);
    }

    // Write secure session data
    cookieStore.delete("sso_login");
    const session = await getSession(cookieStore);
    session.user = {
        id: user.id,
        email_verified: user.emails_verified.some((v) => v),
        created: user.created?.getTime() ?? new Date().getTime(),
    };
    await session.save();
    // Update last login info in the background
    updateLastLoginInfo(user.id);

    /* ------------------------------- redirects -------------------------------- */
    // if another user exists with the same email redirect to merge page
    const users = await service.user.getByEmail(email);

    const url = req.nextUrl.clone();
    url.protocol = req.headers.get("x-forwarded-proto") || url.protocol;
    url.host = req.headers.get("x-forwarded-host") || url.host;
    url.port = req.headers.get("x-forwarded-port") || url.port;

    if (users.length > 1) {
        return NextResponse.redirect(new URL(`/account/merge`, url));
    } else {
        return NextResponse.redirect(new URL(`/`, url));
    }
}
function updateLastLoginInfo(userId: number) {
    const ip = headers().get("X-Forwarded-For");
    if (!ip) return;
    service.user.addLastLogin(userId, ip).catch((e) => {
        console.error("Failed to update login info", e);
    });
}
