/** This file contains al building blocks
 * to authenticate using open id connect auth.
 *
 * Ref: https://openid.net/specs/openid-connect-core-1_0.html#rnc
 * https://github.com/panva/openid-client/blob/HEAD/examples/oidc.ts
 */
import * as client from "openid-client";
import z from "zod";

import { config as snip_config } from "@snip/config/server";

import type { SSOProviderServer } from "../ssoProviders";

const getProviderConfig = async ({
    server,
    clientId,
    clientSecret,
}: SSOProviderServer) => {
    return await client.discovery(
        new URL(server),
        clientId,
        clientSecret,
        client.ClientSecretBasic(clientSecret), //Only basic is supported by helmholz
    );
};

export async function getRedirectUrl(provider: SSOProviderServer): Promise<{
    redirectTo: URL;
    cookie: Cookie;
}> {
    const config = await getProviderConfig(provider);

    /* You must store the code_verifier and nonce in the
     * end-user session such that it can be recovered as the user gets redirected
     * from the authorization server back to your application.
     */
    const code_verifier = client.randomPKCECodeVerifier();
    const code_challenge =
        await client.calculatePKCECodeChallenge(code_verifier);

    let nonce: string | undefined = undefined;

    // Add path to the server url
    // we do not allow x-forward-headers here!
    const redirect_uri = snip_config.getServerUrl();
    redirect_uri.pathname = "/sso/oidc/callback";

    const parameters: Record<string, string> = {
        redirect_uri: redirect_uri.toString(),
        scope: "openid email",
        code_challenge,
        code_challenge_method: "S256",
    };

    /**
     * We cannot be sure the AS supports PKCE so we're going to use state too. Use
     * of PKCE is backwards compatible even if the AS doesn't support it which is
     * why we're using it regardless.
     */
    if (!config.serverMetadata().supportsPKCE()) {
        nonce = client.randomNonce();
        parameters.nonce = nonce;
    }

    // Here we build the URL to redirect the user to
    const redirectTo = client.buildAuthorizationUrl(config, parameters);

    return {
        redirectTo,
        cookie: { code_verifier, nonce, providerId: provider.providerId },
    };
}

/**
 * Retrieves an access token using the authorization code grant flow.
 *
 * @param callbackURL - The URL to which the authorization server will redirect the user
 * after granting authorization. This is the current URL including the search parameters!
 * @param cookie - An object containing the code verifier and optionally a nonce.
 * @param cookie.code_verifier - The PKCE code verifier used in the authorization request.
 * @param cookie.nonce - An optional nonce to be validated in the ID token.
 * @param provider - The SSO provider configuration.
 * @returns A promise that resolves to the authorization response containing the access token.
 */
export async function getAccessToken(
    callbackURL: URL | Request,
    cookie: Cookie,
    provider: SSOProviderServer,
) {
    if (!provider) {
        throw new Error("Invalid provider");
    }

    const config = await getProviderConfig(provider);

    return await client.authorizationCodeGrant(config, callbackURL, {
        pkceCodeVerifier: cookie.code_verifier,
        expectedNonce: cookie.nonce,
        idTokenExpected: true,
    });
}

export async function getUserInfo(token: Token, provider: SSOProviderServer) {
    const config = await getProviderConfig(provider);

    const accessToken = token.access_token;
    const claims = token.claims();

    if (!claims || !claims.sub) {
        throw new Error("No sub claim in id_token");
    }

    const uInfo = await client.fetchUserInfo(config, accessToken, claims.sub);

    if (uInfo.email == undefined || uInfo.email_verified == undefined) {
        throw new Error(
            "OpenID Connect UserInfo endpoint did not return email",
        );
    }

    return uInfo;
}

export const CookieSchema = z.object({
    code_verifier: z.string(),
    nonce: z.string().optional(),
    providerId: z.string(),
});

export type Cookie = z.infer<typeof CookieSchema>;
export type Token = client.TokenEndpointResponse &
    client.TokenEndpointResponseHelpers;
