import { unknownErrorToResponse } from "lib/errorparsing";
import { cookies } from "next/headers";
import { NextRequest, NextResponse } from "next/server";

import config from "@snip/config/server";

import { loadSSOConfig } from "../../ssoProviders";
import { getRedirectUrl } from "../oidc";

/** Quick redirect route which
 * which generates and also stores
 * pcke verifier in a cookie.
 *
 */
export async function GET(req: NextRequest) {
    // Check if using the hostname defined in config for the url, else
    // redirect
    const url = req.nextUrl.clone();
    url.protocol = req.headers.get("x-forwarded-proto") || url.protocol;
    url.host = req.headers.get("x-forwarded-host") || url.host;
    url.port = req.headers.get("x-forwarded-port") || url.port;
    if (url.hostname !== config.hostname) {
        const newUrl = config.getServerUrl();
        newUrl.pathname = req.nextUrl.pathname;
        newUrl.search = req.nextUrl.search;
        return NextResponse.redirect(newUrl);
    }

    const searchParams = new URLSearchParams(req.nextUrl.search);

    // Check if we have a provider id in the search params
    const providerId = searchParams.get("providerId");
    if (!providerId) {
        return NextResponse.json(
            {
                error: "Invalid request",
                details: "No provider_id in request",
            },
            { status: 404 },
        );
    }

    const conf = await loadSSOConfig();

    // Check if provider is valid
    const provider = conf.providers.find((p) => p.providerId === providerId);
    if (!provider) {
        return NextResponse.json(
            {
                error: "Invalid provider",
                details:
                    "Provider not found. Ask your deployment administrator for more information.",
            },
            { status: 400 },
        );
    }

    try {
        const { redirectTo, cookie } = await getRedirectUrl(provider);

        // Store the session in a cookie
        const J = btoa(JSON.stringify(cookie));
        const cookieStore = cookies();
        cookieStore.set(`sso_login`, J, {
            secure: true,
            httpOnly: true,
        });

        return NextResponse.redirect(redirectTo);
    } catch (e: unknown) {
        return unknownErrorToResponse(
            "Failed to generate redirect url",
            e,
            500,
        );
    }
}

export const dynamic = "force-dynamic";
