import { ReactNode } from "react";

import "./layout.scss";

export default function RootLayout({ children }: { children: ReactNode }) {
    return (
        <>
            <main
                style={{
                    overflow: "auto",
                    overscrollBehavior: "none",
                    height: "100%",
                }}
            >
                {children}
            </main>
        </>
    );
}
