import { Metadata } from "next/types";

export default function Privacy() {
    return (
        <div className="container mt-3 flex-grow-1">
            <h1>Privacy Policy</h1>
            <p>
                Snip takes your privacy seriously. To better protect your
                privacy we provide this privacy policy notice explaining the way
                your personal information is collected and used.
            </p>
            <h2>Collection of Routine Information</h2>
            <p>
                We collect information from you when you register and login to
                our app. This information includes, but is not limited to, IP
                addresses, timestamps, and access log metrics such as request
                methods, URLs, response status codes, and user agents. The
                information is tracked for routine administration, maintenance,
                and performance monitoring purposes. This data is keep for up to
                31 days.
            </p>
            <h2>Cookies</h2>
            <p>
                Where necessary, this app uses cookies to store information
                about a user. Session cookies are used to identify a particular
                user and their logged-in status. This cookie is deleted when the
                member logs out of the app. We do not store any personal
                information in the cookies.
            </p>
            <h2>Links to Third Party Websites</h2>
            <p>
                We have included links on this app for your use and reference.
                We are not responsible for the privacy policies on these
                websites. You should be aware that the privacy policies of these
                websites may differ from our own.
            </p>
            <h2>Security</h2>
            <p>
                The security of your personal information is important to us,
                but remember that no method of transmission over the Internet,
                or method of electronic storage, is 100% secure. While we strive
                to use commercially acceptable means to protect your personal
                information, we cannot guarantee its absolute security.
            </p>
            <h2>Changes To This Privacy Policy</h2>
            <p>
                This Privacy Policy is effective as of 18th February 2024 and
                will remain in effect except with respect to any changes in its
                provisions in the future, which will be in effect immediately
                after being posted on this page.
            </p>
            <p>
                We reserve the right to update or change out Privacy Policy at
                any time and you should check this Privacy Policy periodically.
                If we make any material changes to this Privacy Policy, we will
                notify you either through the email address you have provided
                us, or by placing a prominent notice on out app.
            </p>
        </div>
    );
}

export const metadata: Metadata = {
    title: "Privacy Policy",
};
