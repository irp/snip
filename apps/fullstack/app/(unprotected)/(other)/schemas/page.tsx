import Link from "next/link";
import { Metadata } from "next/types";

import { allKnownSchemas } from "./json/getAll";

import styles from "./schema.module.scss";

/** List all snip type schemas */
export default async function Page() {
    return (
        <div className="container flex-grow-1 mt-3">
            <h1>Snippet schemas</h1>
            <p>
                Snippets are small pieces of data that can be visualized on a
                page. They can represent anything from a simple text string to a
                complex data structure. Snippets are strongly typed and must
                conform to a specific schema. You may find the schemas for all
                registered snippets below.
            </p>
            <p>
                Schemas are given in the{" "}
                <a href="https://json-schema.org/">JSON Schema</a> format.
                Generally snippets require some data to be displayed, and may
                also include a view field to define the default visual
                appearance.
            </p>
            <p>
                Internally we organized snippets into namespaces, which are used
                to group related snippets. For example, snippets related to a
                specific measuring device or those associated with a particular
                workgroup are categorized under relevant namespaces.
            </p>
            <h2>Available snippet types</h2>
            <p>
                The table below lists all available snippet types, automatically
                generated based on the registered snippets within each
                namespace.
            </p>
            <SchemasTables />
            <p style={{ fontSize: "0.8rem" }}>
                * Markdown support is experimental. Find a list of supported
                features <Link href="./schemas/markdown">here</Link>.
            </p>
        </div>
    );
}

async function SchemasTables() {
    const schemas = allKnownSchemas();
    schemas.delete("base");

    // Group by namespace i.e. everything before the last /
    const namespaces = new Map<string, [string, string | undefined][]>();
    for (const [key, _] of schemas.entries()) {
        const namespace = key.split("/").slice(0, -1).join("/");
        if (!namespaces.has(namespace)) {
            namespaces.set(namespace, []);
        }
        try {
            namespaces
                .get(namespace)
                ?.push([key, schemas.get(key)?.description]);
        } catch (e) {
            console.error(e);
        }
    }

    return (
        <div className={styles.schemas}>
            <figure>
                <figcaption>General snippets</figcaption>
                <table className={styles.table + " table"}>
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <NamespaceRows
                            rows={namespaces.get("") || []}
                            namespace={undefined}
                        />
                    </tbody>
                </table>
            </figure>
            {namespaces.size > 1 && (
                <figure>
                    <figcaption>External snippets</figcaption>

                    <table className={styles.table}>
                        <thead>
                            <tr>
                                <th className={styles.type}>Type</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            {Array.from(namespaces.entries()).map(
                                ([namespace, rows]) => {
                                    if (namespace === "") {
                                        return null;
                                    }
                                    return (
                                        <NamespaceRows
                                            key={namespace}
                                            namespace={namespace}
                                            rows={rows}
                                        />
                                    );
                                },
                            )}
                        </tbody>
                    </table>
                </figure>
            )}
        </div>
    );
}

function NamespaceRows({
    namespace,
    rows,
}: {
    namespace: string | undefined;
    rows: [string, string | undefined][];
}) {
    if (rows.length === 0) {
        return null;
    }

    return (
        <>
            <tr>
                {namespace ? (
                    <th rowSpan={rows.length} scope="rowgroup">
                        {namespace}
                    </th>
                ) : null}
                <td scope="row">
                    <Link href={`./schemas/json/${rows[0]![0]}`}>
                        {rows[0]![0]}
                    </Link>
                </td>
                <td>{rows[0]![1] || "No description available"}</td>
            </tr>
            {rows.slice(1).map(([key, des]) => (
                <tr key={key}>
                    <td scope="row">
                        <Link href={`./schemas/json/${key}`}>{key}</Link>
                    </td>
                    <td>{des || "No description available"}</td>
                </tr>
            ))}
        </>
    );
}

export const metadata: Metadata = {
    title: "Snip Schemas",
};
