import { NAMESPACE_TO_SNIP_TYPE } from "@snip/snips";
import { BaseSnip } from "@snip/snips/general/base";
/**
 * Generates a map of all known schemas from the `NAMESPACE_TO_SNIP_TYPE` mapping.
 *
 * This function iterates over the `NAMESPACE_TO_SNIP_TYPE` map, which contains mappings
 * between namespaces and snip types. For each snip type, it retrieves the schema associated
 * with the snip class and adds it to a new map. The resulting map contains all known schemas,
 * indexed by their respective snip types.
 *
 * @returns {Map<string, Exclude<typeof BaseSnip.jsonSchema, undefined>>} A map of all known schemas,
 *          where the keys are the snip types and the values are the corresponding schemas.
 */
export function allKnownSchemas() {
    const schemas = new Map<
        string,
        Exclude<typeof BaseSnip.jsonSchema, undefined>
    >();

    const foundClasses: (typeof BaseSnip)[] = [];
    for (const [_, mapping] of NAMESPACE_TO_SNIP_TYPE) {
        for (const [type, SnipClass] of mapping) {
            try {
                const schema = SnipClass.jsonSchema;
                // Filter duplicates
                if (schema && !foundClasses.includes(SnipClass)) {
                    // Add date parsing to the schema
                    schemas.set(type, schema);
                }
            } catch (e) {
                console.error("Failed to add schema for: " + type);
                console.error(e);
            }
            foundClasses.push(SnipClass);
        }
    }

    return schemas;
}
