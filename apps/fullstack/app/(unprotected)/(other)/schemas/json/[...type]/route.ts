import { NextResponse } from "next/server";

import { hasOwnProperty } from "@snip/common";

import { allKnownSchemas } from "../getAll";

export const GET = GETS;

/** Given a snip type parse the route */
async function GETS(
    _request: Request,
    { params }: { params: { type: string[] } },
) {
    const namespace = params.type.join("/");
    const schemas = allKnownSchemas();
    if (!schemas.has(namespace)) {
        return NextResponse.json(
            {
                error: "invalid request!",
                details: "Snip schema was not found for: " + namespace,
            },
            { status: 404 },
        );
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let json: any;
    try {
        // We need to merge the schema with the date parsing
        // otherwise the schema will not be valid
        json = schemas.get(namespace)!;
    } catch (_) {
        return NextResponse.json(
            {
                error: "invalid request!",
                details: "Failed to parse schema for: " + namespace,
            },
            { status: 500 },
        );
    }

    // Remove id, last_updated, created and page_id
    // as they are normally set by the server
    parseSchemaForUser(json);
    // generate snip id based on namespace and name
    const id = namespace.split("/").join("_");

    return NextResponse.json({
        $schema: "https://json-schema.org/draft/2020-12/schema",
        $id: id,
        ...json,
    });
}

/** Generate a page for each specification i.e. snip type
 */
export async function generateStaticParams() {
    const schemas = allKnownSchemas();

    return Object.keys(schemas).map((type) => {
        return {
            params: {
                type: type.split("/"),
            },
        };
    });
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function parseSchemaForUser(schema: any) {
    /** parse the extended format into a user usable format */

    if (!schema) {
        return;
    }
    if (!hasOwnProperty(schema, "properties")) {
        return;
    }

    for (const [key, val] of Object.entries(schema.properties)) {
        if (key === "id") {
            delete schema.properties.id;
        }
        if (key === "last_updated") {
            delete schema.properties.last_updated;
        }
        if (key === "created") {
            delete schema.properties.created;
        }
        if (key === "page_id") {
            delete schema.properties.page_id;
        }
        if (key === "created_by") {
            delete schema.properties.created_by;
        }

        if (hasOwnProperty(val, "properties")) {
            parseSchemaForUser(val);
        }
    }
}
