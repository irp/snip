import { NextResponse } from "next/server";

import { allKnownSchemas } from "./getAll";

/** Get all available json schemas */
export const GET = GETs;

async function GETs(_request: Request) {
    const schemas = allKnownSchemas();

    // Return an array of [{name,url}]
    const ret = [];
    for (const [key, _] of schemas.entries()) {
        ret.push(key);
    }
    return NextResponse.json(ret);
}

export const dynamicParams = false;
export const dynamic = "force-static";
