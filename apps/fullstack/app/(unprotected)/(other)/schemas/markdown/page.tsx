"use client";
import { useEffect, useRef, useState } from "react";

import { MarkdownSnip } from "@snip/snips/general/markdown";

import { NumberInput } from "components/form/inputs/number";

export default function Page() {
    const [wrap, setWrap] = useState(400);

    return (
        <div
            style={{
                height: "100%",
                display: "flex",
                gap: "1rem",
                overflow: "auto",
                justifyContent: "center",
            }}
        >
            <div
                style={{
                    height: "100%",
                    display: "flex",
                    flexDirection: "column",
                    padding: "1rem",
                    gap: "1rem",
                    maxWidth: "810px",
                }}
            >
                <h1>Supported Markdown features</h1>
                <p>
                    We support a subset of the{" "}
                    <a
                        target="_blank"
                        href="https://spec.commonmark.org/0.31.2"
                    >
                        CommonMark
                    </a>{" "}
                    spec for markdown. You may find a list of all supported
                    features below. We might able to support more features in
                    the future. Feel free to suggest any improvements if you
                    find something missing or formatting issues.
                </p>
                <div>
                    <h3>Headings</h3>
                    <div>
                        An heading consists of a string of characters, parsed as
                        inline content, between an opening sequence of 1 to 6
                        unescaped <b>#</b> characters.
                    </div>
                    <EditorWithPreview wrap={wrap} style={{ height: "210px" }}>
                        {`
# foo
## foo
### foo
#### foo
##### foo
###### foo
`}
                    </EditorWithPreview>
                </div>
                <div>
                    <h3>Code blocks</h3>
                    <div>
                        Code blocks are supported using triple backticks or by
                        using indentations. At the moment we only support
                        highlighting for Javascript and python. Feel free to
                        suggest more languages if you need them.
                    </div>
                    <EditorWithPreview wrap={wrap} style={{ height: "210px" }}>
                        {`                  
\`\`\`javascript
function foo() {
    return "bar";
}
\`\`\`
                    `}
                    </EditorWithPreview>
                    <EditorWithPreview
                        wrap={wrap}
                        style={{ height: "210px", marginTop: "1rem" }}
                    >
                        {`
\`\`\`python
def foo():
    return "bar"
\`\`\`
                    `}
                    </EditorWithPreview>
                </div>
                <div>
                    <h3>Lists</h3>
                    <div>
                        We support both ordered and unordered lists. Lists can
                        may be nested to any level.
                    </div>
                    <EditorWithPreview style={{ height: "210px" }} wrap={wrap}>
                        {`
1. First item
2. Second item
3. Third item
    - Nested item
    - Nested item`}
                    </EditorWithPreview>
                    <EditorWithPreview style={{ height: "210px" }} wrap={wrap}>
                        {`
1. First item
2. Second item is very long and should wrap lines at some point
    - Test                       
`}
                    </EditorWithPreview>
                </div>
                <div>
                    <h3>Paragraphs</h3>
                    <div>
                        Paragraphs are separated by a blank line. Paragraphs
                        automatically wrap depending on the `wrap` value you
                        provide (is set in the settings).
                    </div>
                    <div style={{ display: "flex", marginBottom: "0.2rem" }}>
                        <NumberInput
                            label="AutoWrap"
                            value={wrap}
                            onInput={(
                                e: React.ChangeEvent<HTMLInputElement>,
                            ) => {
                                setWrap(parseInt(e.target.value));
                            }}
                        />
                    </div>
                    <EditorWithPreview style={{ height: "210px" }} wrap={wrap}>
                        {`
Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deserunt dolore est architecto. Voluptatum blanditiis quos et voluptas amet dicta mollitia laudantium recusandae consequatur sint dolorum, cum incidunt aliquam tempora libero.
                    `}
                    </EditorWithPreview>
                </div>
                <div>
                    <h3>Inline code</h3>
                    <div>Code block</div>
                    <EditorWithPreview style={{ height: "210px" }} wrap={wrap}>
                        {`
This is a paragraph with some \`inline code\` in it.
                    `}
                    </EditorWithPreview>
                </div>
            </div>
        </div>
    );
}

function MarkdownPreview({ text, wrap }: { text: string; wrap?: number }) {
    const ref = useRef<HTMLCanvasElement>(null);

    useEffect(() => {
        if (!ref.current || text.length < 1) return;
        const canvas = ref.current;

        const mdsnip = new MarkdownSnip({
            text,
            book_id: -1,
            lineWrap: wrap,
        });

        const resizeObs = new ResizeObserver((entries) => {
            canvas.width =
                entries[0]!.contentRect.width * window.devicePixelRatio;
            canvas.height =
                entries[0]!.contentRect.height * window.devicePixelRatio;
            console.log("resize", canvas.width, canvas.height);
            const context = canvas.getContext("2d")!;
            //context.translate(0, 25);
            //context.clearRect(0, 0, canvas.width, canvas.height);
            mdsnip.ready?.then(() => mdsnip.render(context));
        });
        resizeObs.observe(canvas);

        return () => {
            resizeObs.disconnect();
        };
    }, [text, ref, wrap]);

    return (
        <canvas
            ref={ref}
            style={{
                width: "50%",
                height: "100%",
                maxHeight: "100%",
                border: "1px solid black",
                flexShrink: "shrink",
                padding: "0.25rem",
            }}
        />
    );
}

function EditorWithPreview({
    children,
    wrap,
    ...props
}: {
    children?: string;
    wrap?: number;
} & React.HTMLAttributes<HTMLDivElement>) {
    const [text, setText] = useState(children?.trimStart() || "");

    return (
        <div className="d-flex gap-1" {...props}>
            <textarea
                value={text}
                onChange={(e) => setText(e.target.value)}
                style={{
                    width: "50%",
                    height: "100%",
                    resize: "none",
                    overflowY: "auto",
                    padding: "0.25rem",
                }}
            />
            <MarkdownPreview text={text} wrap={wrap} />
        </div>
    );
}
