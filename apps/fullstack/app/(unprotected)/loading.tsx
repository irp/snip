// Global loading spinner is shown whenever the app is loading data

export default function Loading() {
    return (
        <div className="d-flex justify-content-center align-items-center w-100 flex-grow-1">
            <div className="spinner-grow" role="status"></div>
        </div>
    );
}
