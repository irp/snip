import { getSessionAction } from "lib/session";
import type { Metadata } from "next";

import Features from "components/frontpage/features";
import Hero from "components/frontpage/hero";

/** Index page
 * This is the introduction page of the application. It is only
 * shown when the user is not logged in.
 *
 * It includes
 * - a short introduction
 * - CTA to login/signup
 * - Features (open-source, integrations, ...)
 *
 */

export default async function FrontPage() {
    const session = await getSessionAction();

    return (
        <>
            <Hero
                isLoggedIn={session.user !== undefined}
                isVerified={session.user?.email_verified}
            />
            <Features />
        </>
    );
}

export const metadata: Metadata = {
    title: "Snip - our digital lab book",
};
