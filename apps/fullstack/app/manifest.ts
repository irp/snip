import type { MetadataRoute } from "next";

export default function manifest(): MetadataRoute.Manifest {
    return {
        name: "SNIP - Our digital lab book",
        short_name: "SNIP",
        // description: "Next.js App",
        display: "standalone",
        display_override: ["standalone"],
        start_url: "/?standalone=true",
        icons: [
            {
                src: "/img/favicon/favicon-32x32-light.png",
                sizes: "32x32",
                type: "image/png",
            },

            {
                src: "/img/favicon/favicon-64x64-light.png",
                sizes: "64x64",
                type: "image/png",
            },

            {
                src: "/img/favicon/favicon-128x128-light.png",
                sizes: "128x128",
                type: "image/png",
            },
            {
                src: "/img/favicon/apple-touch-icon.png",
                sizes: "180x180",
                type: "image/png",
            },
            {
                src: "/img/favicon/icon-512-mask.png",
                sizes: "512x512",
                type: "image/png",
                purpose: "maskable",
            },
        ],
    };
}
