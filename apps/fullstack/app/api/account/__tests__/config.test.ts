import { GET, POST } from "../config/route";
import { setSessionType } from "__mocks__/mockConfig";
import { createMockRequest } from "__mocks__/request";

vi.mock("@snip/database/sql.connection", () => {
    return {
        pool_permissions: {
            query: vi.fn(async (sql, params) => {
                // this is a unit test, so we don't care about the actual query
                // we have to check that at some point though
                if (
                    globalThis.mockConfig.service.throw_error == true ||
                    globalThis.mockConfig.service.throw_error ==
                        "sql_connection.query"
                ) {
                    throw new Error(
                        globalThis.mockConfig.service.error_message ||
                            "Mock error",
                    );
                }
                return [{ id: 1 }];
            }),
        },
    };
});

describe("GET api/account/config", () => {
    it("should return 401 if user is not logged in", async () => {
        setSessionType("none");

        const req = await createMockRequest({
            method: "GET",
        });
        const res = await GET(req, {});
        expect(res.status).toBe(401);
    });

    it("should return 200", async () => {
        setSessionType("user");

        const req = await createMockRequest({
            method: "GET",
        });
        const res = await GET(req, {});
        expect(res.status).toBe(200);
        const data = await res.json();
        expect(data).toBeDefined();
        for (const key of [
            "zoom1",
            "zoom2",
            "text_lineWrap",
            "text_lineHeight",
            "text_fontSize",
            "text_fontColor",
            "text_font",
            "user_color",
            "pen_size",
            "pen_color",
            "pen_colors",
            "pen_smoothing",
        ]) {
            expect(data[key]).toBeDefined();
        }
    });

    it("should autogenerate new config if user config not found", async () => {
        setSessionType("user");
        globalThis.mockConfig.service.throw_error = "userConfig.getByUserId";
        globalThis.mockConfig.service.error_message = "not found";

        const req = await createMockRequest({
            method: "GET",
        });
        const res = await GET(req, {});
        expect(res.status).toBe(200);
    });

    it("should return 500 if unknown error fetching user config", async () => {
        setSessionType("user");
        globalThis.mockConfig.service.throw_error = "userConfig.getByUserId";

        const req = await createMockRequest({
            method: "GET",
        });
        const res = await GET(req, {});
        expect(res.status).toBe(500);
    });
});

describe("POST api/account/config", () => {
    it("should return 401 if user is not logged in", async () => {
        setSessionType("none");

        const req = await createMockRequest({
            method: "POST",
        });
        const res = await POST(req, {});
        expect(res.status).toBe(401);
    });

    it("should return 400 if bad body", async () => {
        setSessionType("user");

        const req = await createMockRequest({
            method: "POST",
            body: {},
        });
        const res = await POST(req, {});
        expect(res.status).toBe(400);
    });

    it("should return 200", async () => {
        setSessionType("user");

        const req = await createMockRequest({
            method: "POST",
            body: {
                zoom1: 2,
                zoom2: 2,
                text_lineWrap: 2,
                text_lineHeight: 2,
                text_fontSize: 2,
                text_fontColor: "#111111",
                text_font: "Arial",
                pen_size: 2,
                pen_color: "#111111",
                pen_colors: ["#111111"],
                pen_smoothing: 0.6,
            },
        });
        const res = await POST(req, {});
        expect(res.status).toBe(200);
    });

    it("should allow for partial updates", async () => {
        setSessionType("user");

        const values = {
            zoom1: 2,
            zoom2: 1,
            text_lineWrap: 1,
            text_lineHeight: 1,
            text_fontSize: 1,
            text_fontColor: "#000000",
            text_font: "Arial",
            pen_size: 1,
            pen_color: "#000000",
            pen_colors: ["#000000"],
            pen_smoothing: 0.5,
            user_color: "#000000",
        };

        for (const key in values) {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    // @ts-ignore
                    [key]: values[key],
                },
            });

            const res = await POST(req, {});
            expect(res.status).toBe(200);
        }
    });

    it("should return 400 if request is invalid", async () => {
        setSessionType("user");

        const req = await createMockRequest({
            method: "POST",
            body: {
                zoom1: "a",
                zoom2: "a",
            },
        });
        const res = await POST(req, {});
        expect(res.status).toBe(400);
    });

    it("should return 400 if bad font", async () => {
        setSessionType("user");

        const req = await createMockRequest({
            method: "POST",
            body: {
                text_font: "a",
            },
        });
        const res = await POST(req, {});
        expect(res.status).toBe(400);
    });

    it("should return 500 if unknown error updating user config", async () => {
        globalThis.mockConfig.service.throw_error = "sql_connection.query";
        setSessionType("user");

        const req = await createMockRequest({
            method: "POST",
            body: {
                zoom1: 2,
                zoom2: 2,
                text_lineWrap: 2,
                text_lineHeight: 2,
                text_fontSize: 2,
                text_fontColor: "#111111",
                text_font: "Arial",
                pen_size: 2,
                pen_color: "#111111",
                pen_colors: ["#111111"],
                pen_smoothing: 0.6,
                user_color: "#111111",
            },
        });
        const res = await POST(req, {});
        expect(res.status).toBe(500);
    });
});
