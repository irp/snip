import { group_default } from "__mocks__/@snip/database/utils";
import { GET, POST } from "../groups/route";
import { setSessionType } from "__mocks__/mockConfig";
import { createMockRequest } from "__mocks__/request";

describe("GET api/account/groups", () => {
    it("should return 401 if user is not logged in", async () => {
        setSessionType("none");
        const req = await createMockRequest({
            method: "GET",
        });
        const res = await GET(req, {});
        expect(res.status).toBe(401);
    });

    it("should return 200", async () => {
        globalThis.mockConfig.session.user = {
            id: 1,
        } as any;
        const req = await createMockRequest({
            method: "GET",
        });
        const res = await GET(req, {});
        expect(res.status).toBe(200);
    });
});

describe("POST api/account/groups", () => {
    it("should return 401 if user is not logged in", async () => {
        setSessionType("none");
        const req = await createMockRequest({
            method: "POST",
            content_type: "application/json",
            body: { name: "Test" },
        });
        const res = await POST(req, {});
        expect(res.status).toBe(401);
    });

    it("should return 400 if bad body", async () => {
        globalThis.mockConfig.session.user = {
            id: 1,
        } as any;
        const req = await createMockRequest({
            method: "POST",
            content_type: "application/json",
            body: { name: "b" },
        });
        const res = await POST(req, {});
        expect(res.status).toBe(400);
    });

    it("should create a group", async () => {
        const req = await createMockRequest({
            method: "POST",
            content_type: "application/json",
            body: { name: "Test" },
        });
        const res = await POST(req, {});
        expect(res.status).toBe(200);
    });
});

describe("GET api/account/groups/[id]", () => {
    test.todo("tests add member");
    test.todo("tests remove member");
    test.todo("tests get members");
});
