import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import service from "@snip/database";
import { GroupDataRet, MemberDataRet } from "@snip/database/types";

export type MemberWithSelfRet = MemberDataRet & { self?: boolean };
export interface GroupDataWithMembersRet extends GroupDataRet {
    members: MemberWithSelfRet[];
}

/** Get all groups
 * the user is a member of
 * with their members
 */
export const GET = wrapperLoggedInUser(getGroups);

async function getGroups(
    _req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    // Get all groups
    const groups = (await service.group.getByUserId(
        session.user.id,
    )) as GroupDataWithMembersRet[];

    // Get all members
    const members: MemberWithSelfRet[][] = await Promise.all(
        groups.map((group) => service.group.getMembers(group.id!)),
    );

    // Assign members to groups
    for (let i = 0; i < groups.length; i++) {
        // Update self
        for (let j = 0; j < members[i]!.length; j++) {
            if (members[i]![j]!.user_id === session.user.id) {
                members[i]![j]!.self = true;
                break;
            }
        }
        groups[i]!.members = members[i]!;
    }
    return NextResponse.json(groups);
}

/** Create a new group
 * and add the user as owner
 */
export const POST = wrapperLoggedInUser(createGroup);

const CreateGroupRequestSchema = z
    .object({
        name: z
            .string()
            .min(3, {
                message: "The group name must be at least 3 characters",
            })
            .max(255, {
                message: "The group name must be at most 255 characters",
            }),
        description: z.string().optional(),
    })
    .strict();

async function createGroup(
    req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    const res = await req.json();
    const result = CreateGroupRequestSchema.safeParse(res);

    if (result.success === false) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: result.error.format(),
            },
            { status: 400 },
        );
    }

    const groupData = {
        name: result.data.name,
        description: result.data.description,
    };

    const group = (await service.group.insert(
        groupData,
    )) as GroupDataWithMembersRet;
    const owner = await service.group.addMember(
        session.user.id,
        group.id!,
        "owner",
    );
    group.members = [
        {
            ...owner,
            self: true,
        },
    ];

    return NextResponse.json(group);
}
