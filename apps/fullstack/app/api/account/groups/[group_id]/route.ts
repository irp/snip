import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import service from "@snip/database";

import { GroupDataWithMembersRet } from "../route";

/** Delete a group
 */
export const DELETE = wrapperLoggedInUser(deleteGroup);
/** Get a group
 */
export const GET = wrapperLoggedInUser(getGroup);
/** Modify the group
 */
export const POST = wrapperLoggedInUser(editGroup);

async function deleteGroup(
    _req: NextRequest,
    { session, params }: DeepPartialExcept<RequestProps, "session.user">,
) {
    let group_id;
    if (params?.group_id && !isNaN(parseInt(params.group_id))) {
        group_id = parseInt(params.group_id);
    } else {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: "Invalid group_id",
            },
            { status: 400 },
        );
    }

    /** To allow a delete
     * the following conditions
     * must be met:
     *
     * 1. The user must be the owner
     * 2. The group must not have any members
     */

    const role = await service.group.getRole(session.user.id, group_id);

    if (role !== "owner") {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "Only the owner can delete the group",
            },
            { status: 403 },
        );
    }

    const members = await service.group.getMembers(group_id);
    if (members.length > 1) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details:
                    "The group still has members and cannot be deleted remove all members and invites first",
            },
            { status: 400 },
        );
    }

    /** Deletion also
     * deletes all entries in the acl
     * table
     */
    try {
        await service.group.removeMember(session.user.id, group_id);
        await service.group.delete(group_id);
    } catch (e) {
        console.error(e);
        return NextResponse.json(
            {
                error: "Internal Server Error",
                details: "Error deleting group",
            },
            { status: 500 },
        );
    }

    return NextResponse.json({ success: true });
}

async function getGroup(
    req: NextRequest,
    { params, session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    let group_id;
    if (params?.group_id && !isNaN(parseInt(params.group_id))) {
        group_id = parseInt(params.group_id);
    } else {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: "Invalid group_id",
            },
            { status: 400 },
        );
    }

    const role = await service.group.getRole(session.user.id, group_id);

    if (!role) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You are not a member of this group",
            },
            { status: 401 },
        );
    }

    const group = (await service.group.getById(
        group_id,
    )) as GroupDataWithMembersRet;
    group.members = await service.group.getMembers(group_id);

    //Add self
    for (const member of group.members) {
        if (member.user_id === session.user.id) {
            member.self = true;
            break;
        }
    }

    return NextResponse.json(group);
}

const EditGroupRequestSchema = z
    .object({
        // Allow to change the name
        name: z.string().min(3).max(255).optional(),
        //Allow to change the description
        description: z.string().optional(),
        //Potentially allow to modify the members
        updateMemberRole: z
            .array(
                z.object({
                    user_id: z.number(),
                    // Only allow to promote to moderator or demote to member
                    role: z.enum(["moderator", "member"]),
                }),
            )
            .optional(),
        removeMember: z.array(z.number()).optional(),
        transferOwnership: z.number().optional(),
    })
    .strict();

async function editGroup(
    req: NextRequest,
    { params, session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    let group_id;
    if (params?.group_id && !isNaN(parseInt(params.group_id))) {
        group_id = parseInt(params.group_id);
    } else {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: "Invalid group_id",
            },
            { status: 400 },
        );
    }

    // Parse / validate body
    const res = await req.json();

    const result = EditGroupRequestSchema.safeParse(res);

    if (result.success === false) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: result.error.errors.map((e) => e.message).join(", "),
            },
            { status: 400 },
        );
    }
    const {
        name,
        description,
        updateMemberRole,
        removeMember,
        transferOwnership,
    } = result.data;

    //Check if user is allowed to edit
    //the group
    const isAllowedToEdit = await service.group.isAllowedToEdit(
        session.user.id,
        group_id,
    );
    if (!isAllowedToEdit) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You are not allowed to edit this group",
            },
            { status: 401 },
        );
    }

    const oldGroupData = await service.group.getById(group_id);

    /* -------------------------------------------------------------------------- */
    /*                       simple updates name/description                      */
    /* -------------------------------------------------------------------------- */
    // Allowed by all moderators & owner
    if (name) {
        // Update name
        oldGroupData.name = name;
    }
    if (description) {
        // Update description
        oldGroupData.description = description;
    }

    await service.group.update(oldGroupData);

    /* -------------------------------------------------------------------------- */
    /*                           updateMemberRole                                  */
    /* -------------------------------------------------------------------------- */
    // Only owner can update moderators
    if (updateMemberRole) {
        const groupMembers = await service.group.getMembers(group_id);

        const owner = groupMembers.find((member) => member.role === "owner");
        if (owner?.user_id !== session.user.id) {
            return NextResponse.json(
                {
                    error: "Unauthorized",
                    details:
                        "Only the owner may edit promote or demote members",
                },
                { status: 401 },
            );
        }

        // Only members or moderators are valid targets for
        // role changes
        const members = groupMembers.filter(
            (member) => member.role === "member" || member.role === "moderator",
        );

        //Check if updates are valid
        for (const member of updateMemberRole) {
            const memberInGroup = members.find(
                (m) => m.user_id === member.user_id,
            );

            if (!memberInGroup) {
                return NextResponse.json(
                    {
                        error: "Bad Request",
                        details: `User ${member.user_id} not in group`,
                    },
                    { status: 400 },
                );
            }
        }

        // All members are valid!
        // Update roles
        const updatedMembers = [];
        for (const member of updateMemberRole) {
            updatedMembers.push(
                //also updates even though it is named addMember
                await service.group.updateMember(
                    member.user_id,
                    group_id,
                    member.role,
                ),
            );
        }
    }

    /* -------------------------------------------------------------------------- */
    /*                               remove members                               */
    /* -------------------------------------------------------------------------- */
    // Only owner can remove moderators
    // Moderators can remove members
    if (removeMember) {
        const groupMembers = await service.group.getMembers(group_id);

        const owner = groupMembers.find((member) => member.role === "owner");
        // Only members or moderators are valid targets for
        // role changes
        const members = groupMembers.filter(
            (member) => member.role === "member" || member.role === "moderator",
        );
        for (const remove_id of removeMember) {
            const toRemove = members.find((m) => m.user_id === remove_id);
            console.log("toRemove", toRemove);
            console.log(members);

            if (!toRemove) {
                return NextResponse.json(
                    {
                        error: "Bad Request",
                        details: `User ${remove_id} not in group`,
                    },
                    { status: 400 },
                );
            }
            if (toRemove.role === "moderator") {
                if (owner?.user_id !== session.user.id) {
                    return NextResponse.json(
                        {
                            error: "Unauthorized",
                            details: "Only the owner may remove moderators",
                        },
                        { status: 401 },
                    );
                }
            }
            // All members are valid!
            // Remove members
            await service.group.removeMember(remove_id, group_id);
        }

        if (owner?.user_id !== session.user.id) {
            return NextResponse.json(
                {
                    error: "Unauthorized",
                    details: "Only the owner may remove moderators",
                },
                { status: 401 },
            );
        }
    }

    /* -------------------------------------------------------------------------- */
    /*                               transfer owner                               */
    /* -------------------------------------------------------------------------- */
    // Only owner can transfer ownership
    if (transferOwnership) {
        const groupMembers = await service.group.getMembers(group_id);

        const owner = groupMembers.find((member) => member.role === "owner");

        if (owner?.user_id !== session.user.id) {
            return NextResponse.json(
                {
                    error: "Unauthorized",
                    details: "Only the owner may transfer ownership",
                },
                { status: 401 },
            );
        }

        const newOwner = groupMembers.find(
            (member) => member.user_id === transferOwnership,
        );

        if (!newOwner) {
            return NextResponse.json(
                {
                    error: "Bad Request",
                    details: `User ${transferOwnership} not in group`,
                },
                { status: 400 },
            );
        }

        // All members are valid!
        // Transfer ownership
        console.log("Transfer ownership to", newOwner.user_id);
        await service.group.updateMember(newOwner.user_id, group_id, "owner");
        await service.group.updateMember(
            session.user.id,
            group_id,
            "moderator",
        );
    }

    return NextResponse.json({ success: true });
}
