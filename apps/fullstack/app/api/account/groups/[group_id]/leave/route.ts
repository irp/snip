import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { NextRequest, NextResponse } from "next/server";

import service from "@snip/database";

export const POST = wrapperLoggedInUser(leaveGroup);

async function leaveGroup(
    _req: NextRequest,
    { session, params }: DeepPartialExcept<RequestProps, "session.user">,
) {
    let group_id;
    if (params?.group_id && !isNaN(parseInt(params.group_id))) {
        group_id = parseInt(params.group_id);
    } else {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: "Invalid group_id",
            },
            { status: 400 },
        );
    }

    /** A user can leave a group if
     * - the user is not the owner
     */
    const role = await service.group.getRole(session.user.id, group_id);

    if (!role || role === "owner") {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: "You can't leave this group",
            },
            { status: 400 },
        );
    }

    // Leave group
    await service.group.removeMember(session.user.id, group_id);
    return NextResponse.json({ success: true }, { status: 200 });
}
