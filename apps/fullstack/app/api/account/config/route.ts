import { type } from "arktype";
import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { Font, HEXColor } from "lib/validation/types";
import { validateArk } from "lib/validation/validate";
import { NextRequest, NextResponse } from "next/server";

import service from "@snip/database";
import { pool_permissions as pool } from "@snip/database/sql.connection";
import { UserConfigDataRet } from "@snip/database/types";

export const GET = wrapperLoggedInUser(GetUserConfig);

export const POST = wrapperLoggedInUser(EditUserConfig);

async function GetUserConfig(
    _req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    let config: UserConfigDataRet | undefined;
    try {
        config = await service.userConfig.getByUserId(session.user.id);
    } catch (e) {
        if (e instanceof Error && e.message.includes("not found")) {
            // Create default config
            config = await service.userConfig.insert({
                user_id: session.user.id,
            });
        } else {
            return NextResponse.json(
                {
                    error: "Internal Server Error",
                    details: "Error fetching user config",
                },
                { status: 500 },
            );
        }
    }
    return NextResponse.json<UserConfigDataRet>(config, { status: 200 });
}

const EditUserConfigSchema = type({
    zoom1: type("number").optional(),
    zoom2: type("number").optional(),
    text_lineWrap: type("number").optional(),
    text_lineHeight: type("number").optional(),
    text_fontSize: type("number").optional(),
    text_fontColor: HEXColor.optional(),
    text_font: Font.optional(),
    text_font_id: type("number").optional(),
    user_color: HEXColor.optional(),
    pen_size: type("number").optional(),
    pen_color: HEXColor.optional(),
    pen_colors: HEXColor.array().atMostLength(128).optional(),
});

async function EditUserConfig(
    req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    // Check if any updates are given
    const update_config = (await req.json()) as unknown;

    // Validate
    const [updateConfig, error] = validateArk(
        EditUserConfigSchema(update_config),
    );

    if (error !== null) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: error.summary,
            },
            { status: 400 },
        );
    }

    if (Object.keys(updateConfig).length === 0) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: "No updates given",
            },
            { status: 400 },
        );
    }

    // font to font id
    // Get id and delete font name
    if (updateConfig.text_font) {
        try {
            await pool
                .query("SELECT id FROM fonts WHERE name = ?", [
                    updateConfig.text_font,
                ])
                .then((result) => {
                    updateConfig["text_font_id"] = result[0].id;
                    delete updateConfig["text_font"];
                });
        } catch (_e) {
            return NextResponse.json(
                {
                    error: "Internal Server Error",
                    details: "Error fetching font",
                },
                { status: 500 },
            );
        }
    }

    // Update
    const user_config = await service.userConfig.updateByUserId({
        ...updateConfig,
        user_id: session.user.id,
    });

    return NextResponse.json<UserConfigDataRet>(user_config, { status: 200 });
}
