import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { NextRequest, NextResponse } from "next/server";
import z from "zod";

import service from "@snip/database";

export const POST = wrapperLoggedInUser(SetPrimaryCredential);

const schema = z.object({
    credential_id: z.coerce.number(),
});

async function SetPrimaryCredential(
    req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    const json = await req.json();

    const validData = schema.safeParse(json);

    if (!validData.success) {
        return NextResponse.json(
            {
                error: "Invalid data",
                details: validData.error.format(),
            },
            {
                status: 400,
            },
        );
    }
    const { credential_id } = validData.data;

    const user = await service.user.getById(session.user.id);

    if (user.credential_ids.indexOf(credential_id) === -1) {
        return NextResponse.json(
            {
                error: "Invalid credential",
                details: "Credential does not belong to user",
            },
            {
                status: 400,
            },
        );
    }

    try {
        await service.user.setPrimaryCredential(session.user.id, credential_id);
    } catch (e) {
        console.error(e);
        return NextResponse.json(
            { error: "Failed to set primary credential" },
            { status: 500 },
        );
    }

    return NextResponse.json({ success: true });
}
