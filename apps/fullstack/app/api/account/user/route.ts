import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { NextRequest, NextResponse } from "next/server";
import z from "zod";

import { SnipSessionData } from "@snip/auth/types";
import service from "@snip/database";
import { NotFoundError } from "@snip/database/errors";
import {
    LastLoginRet,
    UserConfigDataRet,
    UserDataRet,
} from "@snip/database/types";

import { GroupDataWithMembersRet } from "../groups/route";

export const GET = wrapperLoggedInUser(GetUserData);

export type UserRouteResponseReturn = {
    user: UserDataRet;
    userConfig?: UserConfigDataRet;
    groups?: GroupDataWithMembersRet[];
    lastLogins?: LastLoginRet[];
};

const GetUserDataSchema = z
    .object({
        config: z.coerce.boolean().optional(),
        groups: z.coerce.boolean().optional(),
        last_logins: z.coerce.boolean().optional(),
    })
    .strict();

async function GetUserData(
    req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    // Might have an optional parameter for config and groups
    const { searchParams } = new URL(req.url);
    const res = GetUserDataSchema.safeParse(
        Object.fromEntries(searchParams.entries()),
    );

    if (res.success === false) {
        return NextResponse.json(
            { error: "Invalid query parameters", details: res.error.format() },
            { status: 400 },
        );
    }
    const { config, groups, last_logins } = res.data;

    // Create promises we resolve together at the end

    const userDataP = service.user.getById(session.user.id);

    let userConfigDataP: Promise<UserConfigDataRet> | undefined;
    if (config) {
        userConfigDataP = service.userConfig
            .getByUserId(session.user.id)
            .catch(async (e) => {
                // Sometimes the user config may not be initialized
                if (
                    e instanceof NotFoundError ||
                    e.message.includes("not found")
                ) {
                    return await service.userConfig.insert({
                        user_id: session.user.id,
                        text_font: "Arial",
                    });
                }
                throw e;
            });
    }

    // Groups
    let groupsDataP: Promise<GroupDataWithMembersRet[]> | undefined;
    if (groups) {
        groupsDataP = service.group
            .getByUserId(session.user.id)
            .then(async (groups) => {
                const groupMembersPromises = groups.map((group) => {
                    return service.group.getMembers(group.id!);
                });
                const groupMembersResults =
                    await Promise.all(groupMembersPromises);
                (groups as GroupDataWithMembersRet[]).forEach(
                    (group, index) => {
                        group.members = groupMembersResults[index]!;
                        // Add self flag
                        for (let j = 0; j < group.members.length; j++) {
                            if (group.members[j]!.user_id === session.user.id) {
                                group.members[j]!.self = true;
                                break;
                            }
                        }
                    },
                );
                return groups as GroupDataWithMembersRet[];
            });
    }

    // Last logins
    let lastLoginsDataP: Promise<LastLoginRet[]> | undefined;
    if (last_logins) {
        lastLoginsDataP = service.user.getLastLogins(session.user.id);
    }

    // Resolve promises
    let ret: UserRouteResponseReturn;
    try {
        const [userData, userConfigData, groupsData, lastLoginsData] =
            await Promise.all([
                userDataP,
                userConfigDataP,
                groupsDataP,
                lastLoginsDataP,
            ]);
        ret = {
            user: userData,
            userConfig: userConfigData,
            groups: groupsData,
            lastLogins: lastLoginsData,
        };
    } catch (_e) {
        await (session as SnipSessionData).destroy();
        return NextResponse.json(
            { error: "Failed to get user data" },
            { status: 500 },
        );
    }

    return NextResponse.json<UserRouteResponseReturn>(ret, { status: 200 });
}
