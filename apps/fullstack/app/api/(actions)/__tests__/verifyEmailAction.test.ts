import service from "@snip/database";
import { verifyEmailAction } from "../verifyEmailAction";
import { createEmailVerifyToken } from "@snip/auth/tokens/emailVerify";

vi.spyOn(service.user, "verifySignupWithPassword").mockImplementation(
    async () => {
        return true as any;
    },
);

it("invalid session should return error", async () => {
    globalThis.mockConfig.session = {} as any;

    const result = await verifyEmailAction(null, new FormData());
    expect(result).toMatchObject({
        error: {
            name: "User not found",
            details: "Login required",
        },
    });
});

it("invalid form data should return error", async () => {
    const formData = new FormData();
    formData.append("invalid", "token");

    const result = await verifyEmailAction(null, formData);
    expect(result).toMatchObject({
        error: {
            name: "Invalid token",
            details: "",
        },
    });
});

it("invalid token should return error", async () => {
    const formData = new FormData();
    formData.append("token", "invalid token");

    const result = await verifyEmailAction(null, formData);
    expect(result).toMatchObject({
        error: {
            name: "Invalid token",
            details: "",
        },
    });
});

it("should return success on valid token", async () => {
    globalThis.mockConfig.session = {
        user: {
            id: 1,
        },
        save: vi.fn(),
    } as any;
    const formData = new FormData();
    const token = await createEmailVerifyToken({
        id: 1,
        email: "test@example.de",
        created: new Date(),
        last_updated: new Date(),
        user_id: 1,
        credential_type: "password",
    });
    formData.append("token", token);

    const result = await verifyEmailAction(null, formData);
    expect(result).toMatchObject({ success: true });
});
