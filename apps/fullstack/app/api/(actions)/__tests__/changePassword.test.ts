import { changePasswordAction } from "../changePasswordAction";

it("invalid session should return error", async () => {
    globalThis.mockConfig.session = {} as any;

    const result = await changePasswordAction(null, new FormData());
    expect(result).toMatchObject({
        error: {
            name: "User not found",
            details: "Login required",
        },
    });
});

it("invalid form data should return error", async () => {
    const formData = new FormData();
    formData.append("invalid", "some");

    const result = await changePasswordAction(null, formData);
    expect(result).toMatchObject({
        valid: {
            currentPassword: false,
            newPassword: false,
            confirmPassword: false,
        },
    });

    // Should return partial validation dict if only on field is invalid
    const fields = ["currentPassword", "newPassword", "confirmPassword"];
    for (const field of fields) {
        const formData = new FormData();
        formData.append(field, "test@test.de");
        const result = await changePasswordAction(null, formData);

        // All other fields should be false
        expect((result as any).valid[field]).toBe(undefined);
    }
});

it("invalid new password", async () => {
    const formData = new FormData();
    formData.append("currentPassword", "password12@");
    formData.append("newPassword", "test");
    formData.append("confirmPassword", "test");

    const result = await changePasswordAction(null, formData);

    expect((result as any).valid.newPassword).toBe(false);
});

it("passwords do not match", async () => {
    const formData = new FormData();
    formData.append("currentPassword", "password12@");
    formData.append("newPassword", "password12@");
    formData.append("confirmPassword", "password12@1");

    const result = await changePasswordAction(null, formData);

    expect((result as any).valid.confirmPassword).toBe(false);
});

it("current password invalid", async () => {
    globalThis.mockConfig.service.throw_error = "user.loginWithPassword";
    const formData = new FormData();
    formData.append("currentPassword", "password12@");
    formData.append("newPassword", "password12@");
    formData.append("confirmPassword", "password12@");

    const result = await changePasswordAction(null, formData);

    expect((result as any).valid.currentPassword).toBe(false);
});

it("handle error in changePassword service", async () => {
    globalThis.mockConfig.service.throw_error = "user.changePassword";
    const formData = new FormData();
    formData.append("currentPassword", "password12@");
    formData.append("newPassword", "password12@");
    formData.append("confirmPassword", "password12@");

    const result = await changePasswordAction(null, formData);

    expect(result).toMatchObject({
        error: {
            name: "Error changing password",
        },
    });
});

it("should return success on valid values", async () => {
    const formData = new FormData();
    formData.append("currentPassword", "password12@");
    formData.append("newPassword", "password12@2");
    formData.append("confirmPassword", "password12@2");

    const result = await changePasswordAction(null, formData);

    expect(result).toMatchObject({
        success: true,
        valid: {
            currentPassword: true,
            newPassword: true,
            confirmPassword: true,
        },
    });
});
