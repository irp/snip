import service from "@snip/database";
import { loginWithPasswordAction, loginWithSSOAction } from "../loginAction";

const config = {
    userRet: {
        id: 1,
        emails: ["test@example.de"],
        emails_verified: [true],
        created: new Date(),
        last_updated: new Date(),
        credential_ids: [1],
        credential_types: ["password"],
        primary_credential_id: 1,
    },
};

vi.spyOn(service.user, "loginWithPassword").mockImplementation(async () => {
    if (!config.userRet) {
        throw new Error("Invalid Email or Password!");
    }
    return config.userRet;
});

beforeEach(() => {
    const headers = new Headers();
    headers.set("x-forwarded-for", "192.168.0.0");
    globalThis.mockConfig.headers = headers;
});

it("should return user data on successful login", async () => {
    const formData = new FormData();
    formData.append("email", "test@example.de");
    formData.append("password", "password");
    const result = await loginWithPasswordAction(null, formData);

    expect(result).toMatchObject({ user: config.userRet });
});

it("invalid form data should return error", async () => {
    const formData = new FormData();
    formData.append("email", "test@example.de");
    formData.append("invalid", "password");
    const result = await loginWithPasswordAction(null, formData);

    await expect(result).toMatchObject({
        error: {
            name: "InvalidFormData",
        },
    });
});

it("update info throw error", async () => {
    globalThis.mockConfig.service.throw_error = "user.addLastLogin";
    const consoleSpy = vi.spyOn(console, "error").mockImplementation(() => {});
    const formData = new FormData();
    formData.append("email", "test@example.de");
    formData.append("password", "password");
    const result = await loginWithPasswordAction(null, formData);
    expect(result).toMatchObject({ user: config.userRet });
});

it("should throw error on invalid email or password", async () => {
    const formData = new FormData();
    formData.append("email", "test@example.de");
    formData.append("password", "password");
    config.userRet = null as any;
    const result = await loginWithPasswordAction(null, formData);

    expect(result).toMatchObject({
        error: {
            name: "LoginError",
        },
    });
});

it("not implemented", async () => {
    await expect(loginWithSSOAction()).rejects.toThrow("Not implemented");
});
