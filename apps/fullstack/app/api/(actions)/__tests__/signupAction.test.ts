import { signupWithPasswordAction } from "../signupAction";

global.fetch = vi.fn().mockResolvedValueOnce({
    ok: true,
    json: vi.fn().mockResolvedValue({}),
});

it("invalid form should return validation dict", async () => {
    const result = await signupWithPasswordAction(null, new FormData());
    expect(result).toMatchObject({
        valid: {
            email: false,
            password: false,
            confirmPassword: false,
        },
    });

    // Should return partial validation dict if only on field is invalid
    const fields = ["email", "password", "confirmPassword"];
    for (const field of fields) {
        const formData = new FormData();
        formData.append(field, "test@test.de");
        const result = await signupWithPasswordAction(null, formData);

        // All other fields should be false
        expect((result as any).valid[field]).toBe(undefined);
    }
});

it("invalid password", async () => {
    const formData = new FormData();
    formData.append("email", "test@test.de");
    const pw = "test";
    formData.append("password", pw);
    formData.append("confirmPassword", pw);

    const result = await signupWithPasswordAction(null, formData);

    expect((result as any).valid.password).toBe(false);
});

it("passwords do not match", async () => {
    const formData = new FormData();
    formData.append("email", "test@test.de");
    formData.append("password", "password12@");
    formData.append("confirmPassword", "password12@1");

    const result = await signupWithPasswordAction(null, formData);

    expect((result as any).valid.confirmPassword).toBe(false);
});

it("should error if user already signup", async () => {
    globalThis.mockConfig.service.throw_error = "user.signupWithPassword";
    globalThis.mockConfig.service.error_message = "User already exists unique";
    const formData = new FormData();
    formData.append("email", "test@test.de");
    formData.append("password", "password12@");
    formData.append("confirmPassword", "password12@");

    const result = await signupWithPasswordAction(null, formData);

    // raises more meaningful error in production
    expect(result).toMatchObject({
        error: {
            name: "Email already registered!",
        },
    });
});

it("should throw error on unexpected error", async () => {
    globalThis.mockConfig.service.throw_error = "user.signupWithPassword";
    globalThis.mockConfig.service.error_message = "Unexpected error";
    const formData = new FormData();
    formData.append("email", "test@test.de");
    formData.append("password", "password12@");
    formData.append("confirmPassword", "password12@");

    const result = await signupWithPasswordAction(null, formData);

    expect(result).toMatchObject({
        error: {
            name: "Error",
        },
    });
});

it("should return user data on successful signup", async () => {
    const formData = new FormData();
    formData.append("email", "test@test.de");
    formData.append("password", "password12@");
    formData.append("confirmPassword", "password12@");
    const result = await signupWithPasswordAction(null, formData);

    expect(result).toMatchObject({
        user: {
            id: globalThis.mockConfig.session!.user!.id,
        },
    });
});
