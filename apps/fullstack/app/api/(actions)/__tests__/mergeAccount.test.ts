import service from "@snip/database";
import { mergeAccountsAction } from "../mergeAccountsAction";

it("should return error if session is invalid", async () => {
    globalThis.mockConfig.session = {} as any;

    const result = await mergeAccountsAction(null, new FormData());
    expect(result).toMatchObject({
        error: {
            name: "User not found",
            details: "Login required",
        },
    });
});

it("should return error if form data is invalid", async () => {
    const formData = new FormData();
    formData.append("invalid", "some");

    const result = await mergeAccountsAction(null, formData);
    expect(result).toMatchObject({
        error: {
            name: "InvalidFormData",
            details: "Invalid form data",
        },
    });
});

it("should return error if emails do not match", async () => {
    const formData = new FormData();
    formData.append("email", "test@example.de");
    formData.append("password", "password");

    vi.spyOn(service.user, "loginWithPassword").mockResolvedValueOnce({
        id: 2,
        emails: ["different@example.de"],
    } as any);

    const result = await mergeAccountsAction(null, formData);
    expect(result).toMatchObject({
        error: {
            name: "InvalidFormData",
            details: "Cannot merge accounts with different emails! Contact us.",
        },
    });
});

it("should merge accounts successfully", async () => {
    const formData = new FormData();
    formData.append("email", "test@example.de");
    formData.append("password", "password");

    vi.spyOn(service.user, "loginWithPassword").mockResolvedValueOnce({
        id: 2,
        emails: ["test@example.de"],
    } as any);

    vi.spyOn(service.user, "getById").mockResolvedValueOnce({
        id: 1,
        emails: ["test@example.de"],
    } as any);

    const result = await mergeAccountsAction(null, formData);
    expect(result).toMatchObject({
        success: true,
    });
});

it("should handle errors during account merge", async () => {
    const formData = new FormData();
    formData.append("email", "test@example.de");
    formData.append("password", "password");

    vi.spyOn(service.user, "loginWithPassword").mockResolvedValueOnce({
        id: 2,
        emails: ["test@example.de"],
    } as any);

    vi.spyOn(service.user, "getById").mockResolvedValueOnce({
        id: 1,
        emails: ["test@example.de"],
    } as any);

    vi.spyOn(service.user, "mergeUsers").mockRejectedValueOnce(
        new Error("Merge error"),
    );

    const result = await mergeAccountsAction(null, formData);
    expect(result).toMatchObject({
        error: {
            name: "Error",
            details: "Merge error",
        },
    });
});
