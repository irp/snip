"use server";

import { ErrorDict, errorToDict } from "lib/actions/errorToMessage";
import { getSessionAction } from "lib/session";
import z from "zod";

import {
    unsealEmailVerifyToken,
    VerifyEmailToken,
} from "@snip/auth/tokens/emailVerify";
import service from "@snip/database";

const VerifyFormData = z.object({
    token: z.string(),
});

async function _verifyEmailAction(
    _prevState: ErrorDict | null | { success: true },
    formData: FormData,
): Promise<ErrorDict | { success: true }> {
    const session = await getSessionAction();
    if (!session.user) {
        return {
            error: {
                name: "User not found",
                details: "Login required",
            },
        };
    }

    const validData = VerifyFormData.safeParse(
        Object.fromEntries(formData.entries()),
    );
    if (validData.success === false) {
        return {
            error: {
                name: "Invalid token",
                details: "",
            },
        };
    }
    let { token } = validData.data;
    //Strip all spaces from token
    token = token.replace(/\s/g, "");

    let tokenData: VerifyEmailToken;
    try {
        tokenData = await unsealEmailVerifyToken(token, session.user.id);
    } catch (_error) {
        return {
            error: {
                name: "Invalid token",
                details: "",
            },
        };
    }

    await service.user.verifySignupWithPassword(tokenData.email);
    session.user.email_verified = true;
    await session.save();
    return {
        success: true,
    };
}

export const verifyEmailAction = errorToDict(_verifyEmailAction);
