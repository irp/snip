"use server";

import { ErrorDict, errorToDict } from "lib/actions/errorToMessage";
import { getSessionAction } from "lib/session";
import { headers } from "next/headers";
import z from "zod";

import { getCookieString } from "@snip/auth/session";
import service from "@snip/database";
import { UserDataRet } from "@snip/database/types";

import { validateEmail, validatePW } from "components/form/validation";

const SignupFormSchema = z.object({
    email: z.string().email(),
    password: z.string(),
    confirmPassword: z.string(),
});

export type SignupState = {
    // true if the form data is valid
    // false if the form data is invalid
    // unset if the form data has not been validated
    valid: {
        email?: boolean;
        password?: boolean;
        confirmPassword?: boolean;
    };
    user?: UserDataRet;
};

async function _signupWithPasswordAction(
    _prevState: SignupState | ErrorDict | null,
    formData: FormData,
): Promise<SignupState | ErrorDict> {
    const validData = SignupFormSchema.safeParse(
        Object.fromEntries(formData.entries()),
    );

    if (validData.success === false) {
        const d = validData.error.format();

        const valid: SignupState["valid"] = {};
        if (d.email !== undefined) valid.email = false;
        if (d.password !== undefined) valid.password = false;
        if (d.confirmPassword !== undefined) valid.confirmPassword = false;

        return {
            valid,
        };
    }

    /** Validate response */
    const { email, password, confirmPassword } = validData.data;
    const valid: SignupState["valid"] = {};
    valid.email = validateEmail(email);
    valid.password = validatePW(password);
    valid.confirmPassword = password === confirmPassword;

    if (!valid.email || !valid.password || !valid.confirmPassword) {
        return {
            valid,
        };
    }

    // Try to login
    let user: UserDataRet;
    try {
        user = await service.user.signupWithPassword(
            email.toLowerCase(),
            password,
        );
    } catch (e) {
        if (e instanceof Error && e.message.includes("unique")) {
            // User already exists
            return {
                error: {
                    name: "Email already registered!",
                    details: "",
                },
            };
        }
        throw e;
    }

    // Write session data
    const session = await getSessionAction();

    session.user = {
        id: user.id,
        email_verified: user.emails_verified.some((v) => v),
        created: user.created?.getTime() ?? new Date().getTime(),
    };
    await session.save();
    // Update last login info in the background
    updateLastLoginInfoAction(user.id);

    // send email verification via email service
    await fetch("http://snip_email/email/account/verifyEmail/" + user.id, {
        method: "POST",
        headers: {
            cookie: await getCookieString({
                api: {
                    identifier: "signup",
                },
            }),
        },
    }).catch((e) => {
        console.error(e);
    });

    return {
        valid: {
            email: true,
            password: true,
            confirmPassword: true,
        },
        user,
    };
}

function updateLastLoginInfoAction(userId: number) {
    const ip = headers().get("X-Forwarded-For");
    if (!ip) return;
    service.user.addLastLogin(userId, ip).catch((e) => {
        console.error("Failed to update login info", e);
    });
}

export const signupWithPasswordAction = errorToDict(_signupWithPasswordAction);
