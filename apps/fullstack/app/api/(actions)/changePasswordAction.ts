"use server";
import { ErrorDict, errorToDict } from "lib/actions/errorToMessage";
import { getSessionAction } from "lib/session";
import z from "zod";

import service from "@snip/database";
import { CredentialsData } from "@snip/database/types";

import { validatePW } from "components/form/validation";

const ChangePasswordSchema = z.object({
    currentPassword: z.string(),
    newPassword: z.string(),
    confirmPassword: z.string(),
});

export type ChangePasswordState = {
    // true if the form data is valid
    // false if the form data is invalid
    // unset if the form data has not been validated
    valid: {
        currentPassword?: boolean;
        newPassword?: boolean;
        confirmPassword?: boolean;
    };
    success?: true;
};

async function _changePasswordAction(
    _prevState: ChangePasswordState | ErrorDict | null,
    formData: FormData,
): Promise<ChangePasswordState | ErrorDict> {
    const session = await getSessionAction();
    if (!session.user) {
        return {
            error: {
                name: "User not found",
                details: "Login required",
            },
        };
    }

    // Validate form data
    const validData = ChangePasswordSchema.safeParse(
        Object.fromEntries(formData.entries()),
    );
    if (validData.success === false) {
        const d = validData.error.format();

        const valid: ChangePasswordState["valid"] = {};
        if (d.currentPassword !== undefined) valid.currentPassword = false;
        if (d.newPassword !== undefined) valid.newPassword = false;
        if (d.confirmPassword !== undefined) valid.confirmPassword = false;

        return {
            valid,
        };
    }

    // Validate passwords
    const { currentPassword, newPassword, confirmPassword } = validData.data;
    const valid: ChangePasswordState["valid"] = {};
    // Check if newPassword and confirmPassword match
    valid.newPassword = validatePW(newPassword);
    valid.confirmPassword = newPassword === confirmPassword;

    // Check if currentPassword is correct
    const cred: CredentialsData =
        await service.user.getPasswordCredentialByUserId(session.user.id);

    try {
        await service.user.loginWithPassword(cred.email, currentPassword);
        valid.currentPassword = true;
    } catch (_e) {
        valid.currentPassword = false;
    }

    if (
        !valid.newPassword ||
        !valid.confirmPassword ||
        !valid.currentPassword
    ) {
        return {
            valid,
        };
    }

    // Update password
    try {
        await service.user.changePassword(cred.email, newPassword);
    } catch (e) {
        const message = e instanceof Error ? e.message : "Unknown error";
        return {
            error: {
                name: "Error changing password",
                details: message,
            },
        };
    }

    return {
        valid,
        success: true,
    };
}

export const changePasswordAction = errorToDict(_changePasswordAction);
