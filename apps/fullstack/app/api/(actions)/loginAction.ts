"use server";

import { ErrorDict, errorToDict } from "lib/actions/errorToMessage";
import { getSessionAction } from "lib/session";
import { headers } from "next/headers";
import z from "zod";

import service from "@snip/database";
import { UserDataRet } from "@snip/database/types";

const LoginSchema = z.object({
    email: z.string(),
    password: z.string(),
});

type LoginWithPasswordData = {
    user: UserDataRet;
};

async function _loginWithPasswordAction(
    _prevState: LoginWithPasswordData | ErrorDict | null,
    formData: FormData,
): Promise<LoginWithPasswordData | ErrorDict> {
    const validData = LoginSchema.safeParse(
        Object.fromEntries(formData.entries()),
    );

    if (validData.success === false) {
        return {
            error: {
                name: "InvalidFormData",
                details: validData.error.format(),
            },
        };
    }

    const { email, password } = validData.data;

    // Try to login
    let user: UserDataRet;
    try {
        user = await service.user.loginWithPassword(email, password);
    } catch (_e) {
        return {
            error: {
                name: "LoginError",
                details: "Invalid Email or Password!",
            },
        };
    }

    // Write session data
    const session = await getSessionAction();
    session.user = {
        id: user.id,
        email_verified: user.emails_verified.some((v) => v),
        created: user.created?.getTime() ?? new Date().getTime(),
    };
    await session.save();
    // Update last login info in the background
    updateLastLoginInfoAction(user.id);

    return { user };
}

export async function loginWithSSOAction() {
    throw new Error("Not implemented");
}

function updateLastLoginInfoAction(userId: number) {
    const ip = headers().get("X-Forwarded-For");
    if (!ip) return;
    service.user.addLastLogin(userId, ip).catch((e) => {
        console.error("Failed to update login info", e);
    });
}

export const loginWithPasswordAction = errorToDict(_loginWithPasswordAction);
