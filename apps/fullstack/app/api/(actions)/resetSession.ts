"use server";
import { getSessionAction } from "lib/session";

import service from "@snip/database";

export async function resetSession() {
    const session = await getSessionAction();

    if (!session.user) {
        return;
    }

    const userData = await service.user.getById(session.user.id);
    delete session.user;
    delete session.api;
    delete session.anonymous;
    session.user = {
        id: userData.id,
        email_verified: userData.emails_verified.some((v) => v),
        created: userData?.created?.getTime() ?? new Date().getTime(),
    };
    await session.save();
}
