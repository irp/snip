"use server";
import { ErrorDict, errorToDict } from "lib/actions/errorToMessage";
import { getSessionAction } from "lib/session";
import z from "zod";

import service from "@snip/database";

type MergeAccountsData = {
    success: true;
};

const MergeAccountsSchema = z.object({
    email: z.string(),
    password: z.string(),
});

async function _mergeAccountsAction(
    _prevState: MergeAccountsData | ErrorDict | null,
    formData: FormData,
): Promise<MergeAccountsData | ErrorDict> {
    const session = await getSessionAction();
    if (!session.user) {
        return {
            error: {
                name: "User not found",
                details: "Login required",
            },
        };
    }

    // Validate form data
    const validData = MergeAccountsSchema.safeParse(
        Object.fromEntries(formData.entries()),
    );

    if (validData.success === false) {
        return {
            error: {
                name: "InvalidFormData",
                details: "Invalid form data",
            },
        };
    }

    const { email, password } = validData.data;

    // Get other user data
    const other_user = await service.user.loginWithPassword(email, password);

    // Check if the other users emails include the email of the current user
    const user = await service.user.getById(session.user.id);

    if (!user.emails.includes(email)) {
        return {
            error: {
                name: "InvalidFormData",
                details:
                    "Cannot merge accounts with different emails! Contact us.",
            },
        };
    }

    // Merge accounts
    // always from other account to allow the user to stay
    // logged in
    await service.user.mergeUsers(other_user.id, session.user.id);

    return {
        success: true,
    };
}

export const mergeAccountsAction = errorToDict(_mergeAccountsAction);
