import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { NextRequest, NextResponse } from "next/server";

import service from "@snip/database";

export const POST = wrapperLoggedInUser(moveFolderOrFile);

async function moveFolderOrFile(
    req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    const { type, itemID, moveToID } = await req.json();

    // Transform array to dict and a
    const success = await service.folder.move(
        type,
        itemID,
        moveToID,
        session.user.id,
    );

    return NextResponse.json(
        {
            success,
        },
        { status: 200 },
    );
}
