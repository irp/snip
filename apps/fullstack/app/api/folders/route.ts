import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { NextRequest, NextResponse } from "next/server";

import service from "@snip/database";

export const DELETE = wrapperLoggedInUser(deleteFolder);
export const POST = wrapperLoggedInUser(createFolder);
export const GET = wrapperLoggedInUser(getFolderAndBooks);

async function deleteFolder(
    req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    const { folderID } = await req.json();

    // Transform array to dict and a
    const success = await service.folder.delete(folderID, session.user.id);

    return NextResponse.json(
        {
            success,
        },
        { status: 200 },
    );
}

async function createFolder(
    req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    let { name, comment, parent_id } = await req.json();
    name = name || null;
    comment = comment || null;
    parent_id = parent_id || null;

    // Parsing is done by service
    const folder = await service.folder.insert({
        name,
        comment,
        parent_id,
        user_id: session.user.id,
    });

    return NextResponse.json(folder, { status: 200 });
}

async function getFolderAndBooks(
    req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    const fileData = await service.folder.getFilesByUserId(session.user.id);

    // Transform array to dict and a
    const fileMap: FileMap = {};
    fileData.forEach((f) => {
        if (!f.parentId) {
            f.parentId = "root";
        }
        fileMap[f.id] = f;
    });

    // Add root folder
    fileMap["root"] = {
        id: "root",
        name: "Home",
        isDir: true,
        parentId: undefined,
        numPages: undefined,
        created: undefined,
        last_updated: undefined,
        finished: undefined,
    };

    return NextResponse.json(fileMap, { status: 200 });
}

export const dynamic = "force-dynamic";
