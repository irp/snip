import {
    RequestPropsWithPerms,
    wrapperBookPerms,
} from "lib/route_wrapper/wrapperBookPerms";
import { headers } from "next/headers";
import { NextRequest, NextResponse } from "next/server";

import { signToken } from "@snip/auth/tokens/socket";

export const POST = wrapperBookPerms(generateToken, false);

async function generateToken(
    req: NextRequest,
    { perms, book_id, session }: RequestPropsWithPerms,
) {
    //Check access
    if (!perms.pRead) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have read access to this book!",
            },
            { status: 401 },
        );
    }

    // Get (optional) token from headers or user_id from session
    const headerList = headers();
    const token = headerList.get("Authorization");
    const user_id = session.user?.id;

    // One should always be set
    if (!token && !user_id) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You need to be logged in or provide a token!",
            },
            { status: 401 },
        );
    }

    // Generate token for socket connection
    const socket_token = await signToken({
        user_id,
        book_id,
        token: token!,
    });

    return NextResponse.json({ token: socket_token });
}
