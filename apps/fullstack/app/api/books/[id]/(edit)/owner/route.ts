import {
    RequestPropsWithPerms,
    wrapperBookPerms,
} from "lib/route_wrapper/wrapperBookPerms";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import service from "@snip/database";

export const GET = wrapperBookPerms(getOwner, false);
export const POST = wrapperBookPerms(setOwner, false);

async function getOwner(
    _req: NextRequest,
    { perms, book_id }: RequestPropsWithPerms,
) {
    if (!perms.pRead) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have read access for this book!",
            },
            { status: 401 },
        );
    }
    const owner: BookOwner = await service.book.getOwner(book_id);
    return NextResponse.json(owner);
}

export interface BookOwner {
    id: number;
    entity_name: string;
    entity_type: "user" | "group";
}

const requestSchema = z.object({
    entity_id: z.number(),
    entity_type: z.enum(["user", "group"]),
});

async function setOwner(
    req: NextRequest,
    { perms, book_id, session }: RequestPropsWithPerms,
) {
    if (!perms.pACL) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have write access to this book!",
            },
            { status: 401 },
        );
    }

    if (!session.user) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "Only logged in users may access this route",
            },
            { status: 401 },
        );
    }

    const this_book = await service.book.getById(book_id);

    // Should never happen if a person has pACL, just incase!
    if (!this_book) {
        return NextResponse.json(
            {
                error: "Not Found",
                details: "The book does not exist!",
            },
            { status: 404 },
        );
    }

    /* -------------------------------------------------------------------------- */
    // Check if user is allowed to transfer owner
    // user is allowed to transfer if
    // 1. user is the current owner of the book
    // 2. user is an admin
    // 3. user is group owner of the group that owns the book

    let is_allowed = false;
    if (
        this_book.owner_id &&
        this_book.owner_id === session.user.id &&
        this_book.owner_type === "user"
    ) {
        is_allowed = true;
    }
    if (this_book.owner_id && this_book.owner_type === "group") {
        const role = await service.group.getRole(
            session.user.id,
            this_book.owner_id,
        );
        if (role === "owner") {
            is_allowed = true;
        }
    }

    if (!is_allowed) {
        const is_admin = await service.user.isAdmin(session.user.id);
        if (is_admin) {
            is_allowed = true;
        }
    }

    if (!is_allowed) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details:
                    "You are not allowed to change the ownership! You must be the owner of the book, or the group owner of the group that owns the book.",
            },
            { status: 401 },
        );
    }

    /* -------------------------------------------------------------------------- */
    // Validate request

    const body: unknown = await req.json();
    const acl = requestSchema.safeParse(body);
    if (acl.success === false) {
        return NextResponse.json(
            {
                error: "Invalid request",
                details: acl.error.issues,
            },
            { status: 400 },
        );
    }

    /* -------------------------------------------------------------------------- */
    // Set owner

    try {
        const owner = await service.book.setOwner(
            acl.data.entity_id,
            acl.data.entity_type,
            book_id,
        );
        return NextResponse.json(owner);
    } catch (e) {
        if (
            e instanceof Error &&
            e.message.includes("Entity not found or multiple entities found")
        ) {
            return NextResponse.json(
                {
                    error: "Bad Request",
                    details: "The user or group does not exist!",
                },
                { status: 400 },
            );
        }
        throw e;
    }
}
