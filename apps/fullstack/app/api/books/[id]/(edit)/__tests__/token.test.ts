import { createMockRequest } from "__mocks__/request";
import { GET, POST, DELETE } from "../tokens/route";

describe("token", () => {
    describe("GET (get all tokens)", () => {
        it("should return all tokens for a book if the user has read access", async () => {
            const req = await createMockRequest({
                method: "GET",
            });
            const response = await GET(req, {
                params: { id: "1" },
            });
            const data = await response.json();
            expect(response.status).toBe(200);
            expect(data.length).toBeGreaterThan(0);
        });

        it("should return 401 if user does not have read access", async () => {
            globalThis.mockConfig.book_perms.pACL = false;
            const req = await createMockRequest({
                method: "GET",
            });
            const response = await GET(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(401);

            mockConfig.book_perms.pACL = true;
        });
    });

    describe("POST (add a token)", () => {
        beforeEach(() => {
            // Set user to owner
            globalThis.mockConfig.session.user!.id = 1;
        });

        it("should return the created token if the user has ACL access", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    type: "api",
                    description: "Test Token",
                },
            });
            const response = await POST(req, {
                params: { id: "1" },
            });
            const data = await response.json();
            expect(response.status).toBe(200);
            expect(data.token).toEqual(expect.any(String));
            expect(data.tokenData.id).toEqual(expect.any(Number));
            expect(data.tokenData.type).toBe("api");
            expect(data.tokenData.description).toBe("Test Token");
        });

        it("should return 400 if the request body is invalid", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    type: "invalid",
                },
            });
            const response = await POST(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(400);
        });

        it("should return 401 if the user does not have ACL access", async () => {
            mockConfig.book_perms.pACL = false;
            const req = await createMockRequest({
                method: "POST",
                body: {
                    type: "api",
                    description: "Test Token",
                },
            });
            const response = await POST(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(401);
            mockConfig.book_perms.pACL = true;
        });

        it("should return 401 if the user is not authenticated", async () => {
            globalThis.mockConfig.session.user = undefined;
            const req = await createMockRequest({
                method: "POST",
                body: {
                    type: "api",
                    description: "Test Token",
                },
            });
            const response = await POST(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(401);
        });
    });

    describe("DELETE (delete a token)", () => {
        beforeEach(() => {
            // Set user to owner
            globalThis.mockConfig.session.user!.id = 1;
        });

        it("should delete a token if the user has ACL access", async () => {
            const req = await createMockRequest({
                method: "DELETE",
                body: {
                    id: 1,
                },
            });
            const response = await DELETE(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(200);
            expect(await response.json()).toBe(true);
        });

        it("should return 400 if the request body is invalid", async () => {
            const req = await createMockRequest({
                method: "DELETE",
                body: {
                    id: "invalid",
                },
            });
            const response = await DELETE(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(400);
        });

        it("should return 401 if the user does not have ACL access", async () => {
            mockConfig.book_perms.pACL = false;
            const req = await createMockRequest({
                method: "DELETE",
                body: {
                    id: 1,
                },
            });
            const response = await DELETE(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(401);
            mockConfig.book_perms.pACL = true;
        });

        it("should return 401 if the user is not authenticated", async () => {
            globalThis.mockConfig.session.user = undefined;
            const req = await createMockRequest({
                method: "DELETE",
                body: {
                    id: 1,
                },
            });
            const response = await DELETE(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(401);
        });

        it("should return 404 if the token does not exist", async () => {
            globalThis.mockConfig.service.not_found = true;
            const req = await createMockRequest({
                method: "DELETE",
                body: {
                    id: 1231231,
                },
            });
            const response = await DELETE(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(404);
        });
    });
});
