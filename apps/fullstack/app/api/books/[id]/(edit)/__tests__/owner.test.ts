import { createMockRequest } from "__mocks__/request";
import { BookOwner, GET, POST } from "../owner/route";

describe("owner", () => {
    describe("GET (get owner)", () => {
        it("should return owner details if user has read permission", async () => {
            const req = await createMockRequest({
                method: "GET",
            });
            const response = await GET(req, {
                params: { id: "1" },
            });
            const data = await response.json();
            expect(response.status).toBe(200);
            expect(data).toEqual({
                id: 1,
                entity_name: "Test",
                entity_type: "user",
            });
        });

        it("should return 401 if user does not have read permission", async () => {
            globalThis.mockConfig.book_perms.pRead = false;
            const req = await createMockRequest({
                method: "GET",
            });
            const response = await GET(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(401);

            mockConfig.book_perms.pRead = true;
        });
    });

    describe("POST (set owner)", () => {
        beforeEach(() => {
            // Set user to owner
            globalThis.mockConfig.session.user!.id = 1;
        });
        it("should return owner details if user has write permission", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    entity_id: 1,
                    entity_type: "user",
                },
            });
            const response = await POST(req, {
                params: { id: "1" },
            });
            const data = await response.json();
            expect(response.status).toBe(200);
            expect(data).toEqual({
                id: 1,
                entity_name: "Test",
                entity_type: "user",
            });
        });
        it("should return 400 if body is invalid", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    name: 1,
                },
            });
            const response = await POST(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(400);
        });
        it("should return 401 if user does not have write permission", async () => {
            mockConfig.book_perms.pACL = false;
            const req = await createMockRequest({
                method: "POST",
            });
            const response = await POST(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(401);
            mockConfig.book_perms.pACL = true;
        });
        it("should return 404 if user is not the owner or an admin", async () => {
            globalThis.mockConfig.session.user!.id = 2;
            const req = await createMockRequest({
                method: "POST",
                body: {
                    entity_id: 2,
                },
            });
            const response = await POST(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(401);
        });
        it("should return 400 if book owner is not found", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    entity_name: "error",
                },
            });
            const response = await POST(req, {
                params: { id: "1" },
            });
            expect(response.status).toBe(400);
        });
    });
});
