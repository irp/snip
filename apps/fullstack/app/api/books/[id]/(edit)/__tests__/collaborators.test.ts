import { createMockRequest } from "__mocks__/request";
import { GET, POST, DELETE } from "../collaborators/route";

const PARAMS = { params: { id: "1" } };

describe("api/books/[id]/collaborators", () => {
    beforeEach(() => {
        globalThis.mockConfig.book_perms.pACL = true;
    });

    describe("GET (get collaborators)", () => {
        it("200", async () => {
            const req = await createMockRequest({
                method: "GET",
            });
            const res = await GET(req, PARAMS);
            expect(res.status).toBe(200);
            const data = await res.json();
            expect(data.pendingInvites).toHaveLength(1);
            expect(data.self).toBeDefined();
            expect(data.collaborators).toHaveLength(2);
        });
        it("401 if user does not have permission to read book collaborators", async () => {
            mockConfig.book_perms.pRead = false;
            const req = await createMockRequest({
                method: "GET",
            });
            const res = await GET(req, PARAMS);
            expect(res.status).toBe(401);
            mockConfig.book_perms.pACL = true;
        });
    });

    describe("POST (update collaborators)", () => {
        it("200", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    id: 2,
                    type: "user",
                    pRead: true,
                    pWrite: true,
                    pDelete: true,
                    pACL: true,
                },
            });
            const res = await POST(req, PARAMS);
            const data = await res.json();
            expect(data).toBe(true);
            expect(res.status).toBe(200);
        });

        it("401 if bad request", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    id: 1,
                    type: "user",
                    pRead: true,
                    pWrite: true,
                    pDelete: true,
                    //missing pACL
                },
            });
            const res = await POST(req, PARAMS);
            expect(res.status).toBe(400);
        });

        it("401 if user does not have permission to edit book collaborators", async () => {
            mockConfig.book_perms.pACL = false;
            const req = await createMockRequest({
                method: "POST",
            });
            const res = await POST(req, {
                params: { id: "1" },
            });
            expect(res.status).toBe(401);
            mockConfig.book_perms.pACL = true;
        });

        it("400 if entity_id is owner", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    id: 42,
                    type: "user",
                    pRead: true,
                    pWrite: true,
                    pDelete: true,
                    pACL: true,
                },
            });
            const res = await POST(req, {
                params: { id: "1" },
            });
            expect(res.status).toBe(400);
        });

        it("400 if entity_id is self", async () => {
            globalThis.mockConfig.session.user!.id = 123;
            const req = await createMockRequest({
                method: "POST",
                body: {
                    id: 123,
                    type: "user",
                    pRead: true,
                    pWrite: true,
                    pDelete: true,
                    pACL: true,
                },
            });
            const res = await POST(req, {
                params: { id: "1" },
            });
            expect(res.status).toBe(400);
        });
    });

    describe("DELETE (remove collaborator)", () => {
        it("200", async () => {
            globalThis.mockConfig.session.user!.id = 1;
            const req = await createMockRequest({
                method: "DELETE",
                body: {
                    id: 2,
                    type: "user",
                },
            });
            const res = await DELETE(req, PARAMS);
            expect(res.status).toBe(200);
        });

        it("400 bad request", async () => {
            const req = await createMockRequest({
                method: "DELETE",
                body: {
                    id: 1,
                    //missing type
                },
            });
            const res = await DELETE(req, PARAMS);
            expect(res.status).toBe(400);
        });

        it("401 if user is not owner", async () => {
            globalThis.mockConfig.session.user!.id = 41;
            const req = await createMockRequest({
                method: "DELETE",
                body: {
                    id: 1,
                    type: "user",
                },
            });
            const res = await DELETE(req, PARAMS);
            expect(res.status).toBe(401);
        });
    });
});
