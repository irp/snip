import { createMockRequest } from "__mocks__/request";
import { GET, POST } from "../background/route";

describe("background", () => {
    describe("GET (get available background types)", () => {
        it("should return 200", async () => {
            const req = await createMockRequest({
                method: "GET",
            });
            const res = await GET(req, {
                params: { id: "1" },
            });
            expect(res.status).toBe(200);
            expect(await res.json()).toEqual({
                backgroundTypes: [
                    {
                        id: 1,
                        name: "Background 1",
                        description: "This is a background",
                    },
                    {
                        id: 2,
                        name: "Background 2",
                        description: "This is another background",
                    },
                ],
                selectedID: 1,
            });
        });
        it("should return 401 if user does not have read access", async () => {
            globalThis.mockConfig.book_perms.pRead = false;
            const req = await createMockRequest({
                method: "GET",
            });
            const res = await GET(req, {
                params: { id: "1" },
            });
            expect(res.status).toBe(401);
        });
    });

    describe("POST (set background type)", () => {
        it("allow to change the book default background", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    background_type_id: 1,
                },
            });
            const res = await POST(req, {
                params: { id: "1" },
            });
            expect(res.status).toBe(200);
        });
        it("allow to change a page specific background", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    background_type_id: 1,
                    page_ids: [1, 2, 3],
                },
            });
            const res = await POST(req, {
                params: { id: "1" },
            });
            expect(res.status).toBe(200);

            const req2 = await createMockRequest({
                method: "POST",
                body: {
                    background_type_id: 1,
                    page_numbers: [1, 2, 3],
                },
            });
            const res2 = await POST(req2, {
                params: { id: "1" },
            });
            expect(res2.status).toBe(200);
        });

        it("should return 401 if user does not have write access", async () => {
            globalThis.mockConfig.book_perms.pWrite = false;
            const req = await createMockRequest({
                method: "POST",
                body: {
                    background_type_id: 1,
                },
            });
            const res = await POST(req, {
                params: { id: "1" },
            });
            expect(res.status).toBe(401);
        });
        it("should return 400 if request is invalid", async () => {
            const req = await createMockRequest({
                method: "POST",
                body: {
                    background_type_id: "1asdas",
                },
            });
            const res = await POST(req, {
                params: { id: "1" },
            });
            expect(res.status).toBe(400);
        });
    });
});
