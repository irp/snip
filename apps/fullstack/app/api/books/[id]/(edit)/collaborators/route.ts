import {
    RequestPropsWithPerms,
    wrapperBookPerms,
} from "lib/route_wrapper/wrapperBookPerms";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import service from "@snip/database";
import {
    CollaborationInviteDataRet,
    Entity,
    ENTITY_GROUP,
    ENTITY_USER,
    RESOURCE_BOOK,
} from "@snip/database/types";

/** Get all collaborators from the book
 */
export const GET = wrapperBookPerms(getCollaborators, false);
/** Updates the collaborators for the book
 */
export const POST = wrapperBookPerms(setCollaborators, false);
/** Delete a collaborator
 */
export const DELETE = wrapperBookPerms(deleteCollaborator, false);

export interface Base {
    id: number;
    pRead: boolean;
    pWrite: boolean;
    pDelete: boolean;
    pACL: boolean;
}

export interface CollaboratorUserData extends Base {
    type: "user";
    email: string;
    self?: boolean;
    owner?: boolean;
}
export interface CollaboratorGroupData extends Base {
    type: "group";
    name: string;
    owner?: boolean;
}

export type CollaboratorData = CollaboratorUserData | CollaboratorGroupData;

export interface CollaboratorRouteReturn {
    pendingInvites: Required<CollaborationInviteDataRet>[];
    collaborators: CollaboratorData[];
    self: Base;
}

async function getCollaborators(
    _req: NextRequest,
    { perms, book_id, session }: RequestPropsWithPerms,
) {
    if (!perms.pRead) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have read access to this book!",
            },
            { status: 401 },
        );
    }

    if (!session.user) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "Only logged in users may access this route",
            },
            { status: 401 },
        );
    }

    // Get invites for book
    const pendingInvites =
        await service.collaboratorsInvite.getPendingByBookId(book_id);

    // Get all collaborators for the book
    // Split into groups, users and links
    const bookPerms = await service.permission.getResolvedForBook(book_id);
    const book = await service.book.getById(book_id);

    // Get own permissions
    const ownPermsP = service.permission.getByUserAndBookReduced(
        session.user.id,
        book_id,
    );

    const collaboratorDataArray: CollaboratorData[] = [];
    bookPerms.forEach((perm) => {
        switch (perm.entity_type) {
            case "user":
                collaboratorDataArray.push({
                    type: "user",
                    email: perm.entity_name,
                    pRead: perm.pRead ?? false,
                    pWrite: perm.pWrite ?? false,
                    pDelete: perm.pDelete ?? false,
                    pACL: perm.pACL ?? false,
                    id: perm.entity_id,
                    self: perm.entity_id === session.user?.id,
                    owner:
                        perm.entity_id === book.owner_id &&
                        book.owner_type === "user",
                });
                break;
            case "group":
                collaboratorDataArray.push({
                    type: "group",
                    name: perm.entity_name,
                    pRead: perm.pRead ?? false,
                    pWrite: perm.pWrite ?? false,
                    pDelete: perm.pDelete ?? false,
                    pACL: perm.pACL ?? false,
                    id: perm.entity_id,
                    owner:
                        perm.entity_id === book.owner_id &&
                        book.owner_type == "group",
                });
                break;
            default:
                console.error("Unknown entity type: ", perm.entity_type);
        }
    });

    const ownPerms = await ownPermsP;

    return NextResponse.json<CollaboratorRouteReturn>({
        pendingInvites,
        collaborators: collaboratorDataArray,
        self: {
            pRead: ownPerms.pRead,
            pWrite: ownPerms.pWrite,
            pDelete: ownPerms.pDelete,
            pACL: ownPerms.pACL,
            id: session.user.id,
        },
    });
}

const schemaUpdate = z.object({
    //Required fields
    id: z.number(),
    type: z.enum(["user", "group"]),
    pRead: z.boolean(),
    pWrite: z.boolean(),
    pDelete: z.boolean(),
    pACL: z.boolean(),
});

async function setCollaborators(
    req: NextRequest,
    { perms, book_id, session }: RequestPropsWithPerms,
) {
    if (!perms.pACL || !session.user?.id) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You are not allowed to change the ACL of this book!",
            },
            { status: 401 },
        );
    }

    // Parse acl from request body
    const body: unknown = await req.json();
    const collaborator = schemaUpdate.safeParse(body);
    if (collaborator.success === false) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: collaborator.error.errors.map((e) => e.message),
            },
            { status: 400 },
        );
    }
    const { id, type, pRead, pWrite, pDelete, pACL } = collaborator.data;

    const book = await service.book.getById(book_id);

    // Cannot change the owner
    if (
        (type == "user" && book.owner_id === id) ||
        (type == "group" && book.owner_id === id)
    ) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: "Owner permissions cannot be modified",
            },
            { status: 400 },
        );
    }

    // Cannot change own permissions
    if (type === "user" && id === session.user.id) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: "You cannot modify your own permissions",
            },
            { status: 400 },
        );
    }

    const entity_type: Entity = type === "user" ? ENTITY_USER : ENTITY_GROUP;

    // Update acl
    // Throws not found error if not found
    const success = await service.permission.updateByEntityAndResource(
        id,
        book_id,
        entity_type,
        RESOURCE_BOOK,
        pRead,
        pWrite,
        pDelete,
        pACL,
    );

    return NextResponse.json(success);
}

const schemaDelete = z.object({
    id: z.number(),
    type: z.enum(["user", "group", "invite"]),
});

async function deleteCollaborator(
    req: NextRequest,
    { book_id, session }: RequestPropsWithPerms,
) {
    // Parse acl from request body
    const body: unknown = await req.json();
    const collaborator = schemaDelete.safeParse(body);
    if (collaborator.success === false) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: collaborator.error.errors.map((e) => e.message),
            },
            { status: 400 },
        );
    }
    const { id, type } = collaborator.data;

    // only owner may delete!
    // TODO: group owners?!
    const book = await service.book.getById(book_id);
    if (
        !session.user?.id ||
        (book.owner_id !== session.user.id && book.owner_type == "user")
    ) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "Only the owner may delete collaborators",
            },
            { status: 401 },
        );
    }

    let success = false;
    if (type === "invite") {
        // Delete invite
        success = await service.collaboratorsInvite.delete(id);
    } else {
        const entity_type: Entity =
            type === "user" ? ENTITY_USER : ENTITY_GROUP;

        const perm = await service.permission.getByEntityAndResource(
            id,
            book_id,
            entity_type,
            RESOURCE_BOOK,
        );
        success = await service.permission.delete(perm.id);
    }

    return NextResponse.json(success);
}
