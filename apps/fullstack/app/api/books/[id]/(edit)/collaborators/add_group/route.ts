import {
    RequestPropsWithPerms,
    wrapperBookPerms,
} from "lib/route_wrapper/wrapperBookPerms";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import service from "@snip/database";
import { ENTITY_GROUP, RESOURCE_BOOK } from "@snip/database/types";

import { CollaboratorGroupData } from "../route";

/** Get all collaborators from the book
 */
export const POST = wrapperBookPerms(addGroupCollaborator);

async function addGroupCollaborator(
    req: NextRequest,
    { perms, book_id, session }: RequestPropsWithPerms,
) {
    if (!perms.pACL) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details:
                    "You do not have ACL access to this book! You need to have ACL access to add collaborators.",
            },
            { status: 401 },
        );
    }

    if (!session.user) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "Only logged in users may access this route",
            },
            { status: 401 },
        );
    }

    const bodySchema = z.object({
        //Group to invite
        group_id: z.coerce.number().int(),
    });

    // Parse acl from request body
    const body: unknown = await req.json();
    const res = bodySchema.safeParse(body);
    if (res.success === false) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: res.error.errors.map((e) => e.message),
            },
            { status: 400 },
        );
    }
    const { group_id } = res.data;
    const user_id = session.user.id;

    // Get book
    const [, group, member] = await Promise.all([
        service.book.getById(book_id),
        service.group.getById(group_id),
        service.group.getMember(user_id, group_id),
    ]);

    if (member.role !== "owner" && member.role !== "moderator") {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details:
                    "Only the owner or moderator of the group may add collaborators",
            },
            { status: 401 },
        );
    }

    // Check if not already collaborator
    const alreadyCollaborator = await isAlreadyCollaboratorGroup(
        group_id,
        book_id,
    );

    if (alreadyCollaborator === true) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: "Group is already a collaborator",
            },
            { status: 400 },
        );
    }

    // Add permission
    const acl = await service.permission.insert({
        entity_id: group_id,
        entity_type: ENTITY_GROUP,
        resource_id: book_id,
        resource_type: RESOURCE_BOOK,
        pRead: true,
        pWrite: true,
        pDelete: false,
        pACL: false,
    });

    return NextResponse.json<CollaboratorGroupData>({
        id: acl.resource_id,
        type: "group",
        name: group.name,
        owner: false,
        pRead: acl.pRead ?? false,
        pWrite: acl.pWrite ?? false,
        pDelete: acl.pDelete ?? false,
        pACL: acl.pACL ?? false,
    });
}

// check if row for user exist in permissions table
async function isAlreadyCollaboratorGroup(group_id: number, book_id: number) {
    try {
        await service.permission.getByEntityAndResource(
            group_id,
            book_id,
            ENTITY_GROUP,
            RESOURCE_BOOK,
        );
    } catch (error) {
        if (error instanceof Error && error.name === "NotFoundError") {
            return false;
        }
        throw error;
    }
    return true;
}
