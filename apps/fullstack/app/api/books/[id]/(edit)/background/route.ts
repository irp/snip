import {
    RequestPropsWithPerms,
    wrapperBookPerms,
} from "lib/route_wrapper/wrapperBookPerms";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import service from "@snip/database";

/**
 * Get all background types for a book
 * and the selected background type
 */
export const GET = wrapperBookPerms(getBackgroundTypes);

/** Set current
 * background type for a book (page or default)
 */
export const POST = wrapperBookPerms(setBackground, false);

async function getBackgroundTypes(
    _req: NextRequest,
    { perms, book_id }: RequestPropsWithPerms,
) {
    if (!perms.pRead) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have read access to this book!",
            },
            { status: 401 },
        );
    }

    // Fetch all background types
    const backgroundTypes = await service.backgroundType.getAll();

    // Selected
    const selectedID =
        await service.backgroundType.getSelectedByBookId(book_id);

    return NextResponse.json({ backgroundTypes, selectedID });
}

const requestSchema = z.object({
    background_type_id: z.number(),
    page_numbers: z.array(z.number()).optional(),
    page_ids: z.array(z.number()).optional(),
});

async function setBackground(
    req: NextRequest,
    { perms, book_id }: RequestPropsWithPerms,
) {
    if (!perms.pWrite) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have write access to this book!",
            },
            { status: 401 },
        );
    }

    // Validate request
    const body: unknown = await req.json();
    const acl = requestSchema.safeParse(body);

    if (acl.success === false) {
        return NextResponse.json(
            {
                error: "Invalid request",
                details: acl.error.issues,
            },
            { status: 400 },
        );
    }

    // Set default background if no page_ids or page_numbers are provided
    const promises = [];
    if (!acl.data.page_ids && !acl.data.page_numbers) {
        promises.push(
            service.backgroundType.setBookDefault(
                book_id,
                acl.data.background_type_id,
            ),
        );
    }
    if (acl.data.page_ids) {
        promises.push(
            service.backgroundType.setPageByIds(
                book_id,
                acl.data.page_ids,
                acl.data.background_type_id,
            ),
        );
    }
    if (acl.data.page_numbers) {
        promises.push(
            service.backgroundType.setPageByNumbers(
                book_id,
                acl.data.page_numbers,
                acl.data.background_type_id,
            ),
        );
    }

    try {
        await Promise.all(promises);
        return NextResponse.json(true);
    } catch (e) {
        if (e instanceof Error && e.message === "Invalid background type") {
            return NextResponse.json(
                {
                    error: "Invalid request",
                    details: "Invalid background type",
                },
                { status: 400 },
            );
        }
        throw e;
    }
}
