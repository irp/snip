import {
    RequestPropsWithPerms,
    wrapperBookPerms,
} from "lib/route_wrapper/wrapperBookPerms";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import { generateAPITokenTuple } from "@snip/auth/tokens/api";
import { generateUIToken } from "@snip/auth/tokens/ui";
import {
    randomAdjective,
    randomAnimal,
    randomColor,
} from "@snip/common/random/index";
import service from "@snip/database";
import { TokenData } from "@snip/database/types";

/** Get all tokens
 * (ui and api)
 */
export type GetTokensResponse = TokenData[];
export const GET = wrapperBookPerms(getTokens, false);

/** Create a new token
 *  for the book
 */
export type PostTokenResponse = {
    token?: string;
    tokenData: TokenData;
};
export const POST = wrapperBookPerms(addToken, false);

/** Delete a token
 */
export const DELETE = wrapperBookPerms(deleteToken, false);

async function getTokens(
    _req: NextRequest,
    { perms, book_id }: RequestPropsWithPerms,
) {
    if (!perms.pACL) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have read access to this book!",
            },
            { status: 401 },
        );
    }

    const tokens = await service.token.getByBookId(book_id);
    return NextResponse.json<GetTokensResponse>(tokens);
}

const addSchema = z.object({
    type: z.enum(["ui", "api"]),
    description: z
        .string()
        .min(5, { message: "Description must be at least 5 characters" })
        .default(() => {
            return (
                randomAdjective() + " " + randomColor() + " " + randomAnimal()
            );
        }),
    expires_at: z.coerce.date().optional(),
});

async function addToken(
    req: NextRequest,
    { session, perms, book_id }: RequestPropsWithPerms,
) {
    if (!perms.pACL) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have ACL access to this book!",
            },
            { status: 401 },
        );
    }

    if (!session.user) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "Only users can create tokens!",
            },
            { status: 401 },
        );
    }

    // Validate request
    const body: unknown = await req.json();
    const res = addSchema.safeParse(body);

    if (res.success === false) {
        return NextResponse.json(
            {
                error: "Invalid request",
                details: res.error.errors.map((e) => e.message).join(", "),
            },
            { status: 400 },
        );
    }
    const { type, description, expires_at } = res.data;

    if (type === "api") {
        // Generate api token
        const { token, hashed_token } = await generateAPITokenTuple();

        // Insert into db
        const data = await service.token.insert({
            hashed_token,
            book_id,
            created_by: session.user.id,
            description,
            type: "api",
            expires_at,
        });

        return NextResponse.json({
            token,
            tokenData: data,
        });
    }

    if (type === "ui") {
        const token = await generateUIToken();

        const data = await service.token.insert({
            hashed_token: token,
            book_id,
            created_by: session.user.id,
            description,
            type: "ui",
            expires_at,
        });

        return NextResponse.json({
            tokenData: data,
        });
    }

    return NextResponse.json(
        {
            error: "Invalid request",
            details: "Invalid token type",
        },
        { status: 400 },
    );
}

async function deleteToken(
    req: NextRequest,
    { session, perms, book_id }: RequestPropsWithPerms,
) {
    if (!perms.pACL) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have ACL access to this book!",
            },
            { status: 401 },
        );
    }

    if (!session.user) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "Only users can delete tokens!",
            },
            { status: 401 },
        );
    }

    const body = await req.json();
    const schema = z.object({
        id: z.number().int(),
    });

    const result = schema.safeParse(body);
    if (result.success === false) {
        return NextResponse.json(
            {
                error: "Invalid request",
                details: result.error.errors.map((e) => e.message).join(", "),
            },
            { status: 400 },
        );
    }

    let tokenData: TokenData;
    try {
        tokenData = await service.token.getById(result.data.id);
    } catch (e) {
        return NextResponse.json(
            {
                error: "Invalid request",
                details: "Token not found",
            },
            { status: 404 },
        );
    }

    if (tokenData.book_id !== book_id) {
        return NextResponse.json(
            {
                error: "Invalid request",
                details: "Token not found",
            },
            { status: 404 },
        );
    }

    await service.token.delete(result.data.id);

    return NextResponse.json(true);
}
