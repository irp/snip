import { readFile } from "fs/promises";
import { POST } from "../route";

import {
    createMockRequest,
    createMockRequestMultipart,
} from "__mocks__/request";

/** Mock client socket */
vi.mock("socket.io-client", () => {
    return {
        io: vi.fn(() => {
            return {
                emit: vi.fn(),
                on: vi.fn(),
                connect: vi.fn(),
            };
        }),
    };
});

const PARAMS = { params: { id: "1" } };

describe("api/books/[id]/upload", () => {
    let consoleSpy: any;
    beforeEach(() => {
        consoleSpy = vi.spyOn(console, "error").mockImplementation(() => {});
    });

    /** Errors */
    describe("should gracefully handle errors", () => {
        it("return 401 if user does not have pWrite", async () => {
            globalThis.mockConfig.book_perms.pWrite = false;
            const req = await createMockRequest({ method: "POST" });
            const res = await POST(req, PARAMS);

            expect(await res.json()).toEqual({
                error: "Unauthorized",
                details:
                    "You do not have write access to this book or it is finished!",
            });
            expect(res.status).toBe(401);
        });
        it("return 400 if the application content type is not allowed", async () => {
            //Set perms to true
            mockConfig.book_perms.pWrite = true;
            const req = await createMockRequest({
                content_type: "application/asdasd",
            });
            const res = await POST(req, PARAMS);

            expect(await res.json()).toEqual(
                expect.objectContaining({
                    error: "Parsing error! (Invalid content type or malformed)",
                }),
            );
            expect(res.status).toBe(400);
        });
        it("return 400 if no data is given", async () => {
            const req = await createMockRequestMultipart();
            const res = await POST(req, PARAMS);

            const data = await res.json();
            expect(res.status).toBe(400);
            expect(data).toEqual(
                expect.objectContaining({
                    error: "No data provided!",
                    details: "Please provide a file or a multipart form!",
                }),
            );
        });

        it("return 500 if server error", async () => {
            globalThis.mockConfig.service.throw_error = true;
            const req = await createMockRequestMultipart({
                files: [
                    {
                        name: "json",
                        filename: "test.json",
                        type: "application/json",
                    },
                ],
                dir: __dirname + "/test_files",
            });
            const res = await POST(req, PARAMS);
            expect(await res.json()).toEqual(
                expect.objectContaining({
                    error: "Internal Server Error",
                }),
            );
            expect(res.status).toBe(500);
            expect(consoleSpy).toHaveBeenCalled();
        });
    });

    describe("should handle multipart/form-data", () => {
        it("image files (single)", async () => {
            const types = ["png", "jpg", "webp"];

            // Create a mock request with a file attached
            for (const type of types) {
                const req = await createMockRequestMultipart({
                    files: [
                        {
                            name: "image",
                            filename: "test." + type,
                            type: "image/" + type,
                        },
                    ],
                    dir: __dirname + "/test_files",
                });

                // Call the handler with the mock request
                const res = await POST(req, PARAMS);
                const data = await res.json();
                // Assert the response
                expect(res.status).toBe(200);
                expect(data).toBeDefined();
                expect(data.ids).toBeDefined();
                expect(data.ids.length).toBe(1);
            }
        });
        it("image files (multiple)", async () => {
            const types = ["png", "jpg", "webp"];

            // Create a mock request with a file attached
            for (const type of types) {
                const req = await createMockRequestMultipart({
                    files: [
                        {
                            name: "image",
                            filename: "test." + type,
                            type: "image/" + type,
                        },
                        {
                            name: "image",
                            filename: "test." + type,
                            type: "image/" + type,
                        },
                    ],
                    dir: __dirname + "/test_files",
                });

                // Call the handler with the mock request
                const res = await POST(req, PARAMS);
                const data = await res.json();

                // Assert the response
                expect(res.status).toBe(200);
                expect(data).toBeDefined();
                expect(data.ids).toBeDefined();
                expect(data.ids.length).toBe(2);
            }
        });
        it("text files (single)", async () => {
            const types = ["txt"];

            // Create a mock request with a file attached
            for (const type of types) {
                const req = await createMockRequestMultipart({
                    files: [
                        {
                            name: "text",
                            filename: "test." + type,
                            type: "text/" + type,
                        },
                    ],
                    dir: __dirname + "/test_files",
                });

                // Call the handler with the mock request
                const res = await POST(req, PARAMS);
                const data = await res.json();

                // Assert the response
                expect(res.status).toBe(200);
                expect(data).toBeDefined();
                expect(data.ids).toBeDefined();
                expect(data.ids.length).toBe(1);
            }
        });
        it("text files (multiple)", async () => {
            const types = ["txt"];

            // Create a mock request with a file attached
            for (const type of types) {
                const req = await createMockRequestMultipart({
                    files: [
                        {
                            name: "text",
                            filename: "test." + type,
                            type: "text/" + type,
                        },
                        {
                            name: "text",
                            filename: "test." + type,
                            type: "text/" + type,
                        },
                    ],
                    dir: __dirname + "/test_files",
                });

                // Call the handler with the mock request
                const res = await POST(req, PARAMS);
                const data = await res.json();

                // Assert the response
                expect(res.status).toBe(200);
                expect(data).toBeDefined();
                expect(data.ids).toBeDefined();
                expect(data.ids.length).toBe(2);
            }
        });
        it("json file (single)", async () => {
            const req = await createMockRequestMultipart({
                files: [
                    {
                        name: "json",
                        filename: "test.json",
                        type: "application/json",
                    },
                ],
                dir: __dirname + "/test_files",
            });

            // Call the handler with the mock request
            const res = await POST(req, PARAMS);
            const data = await res.json();

            // Assert the response
            expect(res.status).toBe(200);
            expect(data).toBeDefined();
            expect(data.ids).toBeDefined();
            expect(data.ids.length).toBe(1);
        });
        it("json file (multiple)", async () => {
            const req = await createMockRequestMultipart({
                files: [
                    {
                        name: "json",
                        filename: "test.json",
                        type: "application/json",
                    },
                    {
                        name: "json",
                        filename: "test.json",
                        type: "application/json",
                    },
                ],
                dir: __dirname + "/test_files",
            });

            // Call the handler with the mock request
            const res = await POST(req, PARAMS);
            const data = await res.json();

            // Assert the response
            expect(res.status).toBe(200);
            expect(data).toBeDefined();
            expect(data.ids).toBeDefined();
            expect(data.ids.length).toBe(2);
        });

        it("json fields (single)", async () => {
            const json_data = readFile(
                __dirname + "/test_files/test.json",
                "utf-8",
            );
            //parse data
            const json = JSON.parse(await json_data);

            const req = await createMockRequestMultipart({
                fields: [json],
            });

            const res = await POST(req, PARAMS);
            const data = await res.json();
            // Assert the response
            expect(res.status).toBe(200);
            expect(data).toBeDefined();
            expect(data.ids).toBeDefined();
            expect(data.ids.length).toBe(1);
        });

        describe("handle errors in multipart/form-data gracefully", () => {
            it("return 400 if multiform file is empty", async () => {
                const req = await createMockRequestMultipart({
                    files: [
                        {
                            name: "json",
                            filename: "test.json",
                            type: "application/json",
                        },
                    ],
                    dir: __dirname + "/test_files",
                    empty: true,
                });

                // Call the handler with the mock request
                const res = await POST(req, PARAMS);

                // Assert the response
                expect(res.status).toBe(400);
                expect(await res.json()).toEqual({
                    error: "Parsing error! (Invalid content type or malformed)",
                    details:
                        "File [json]: No data received! Please check if the send file is not empty!",
                });
            });

            it("return 400 if multiform file is not supported", async () => {
                const req = await createMockRequestMultipart({
                    files: [
                        {
                            name: "json",
                            filename: "test.json",
                            type: "application/asdasd",
                        },
                    ],
                    dir: __dirname + "/test_files",
                });

                // Call the handler with the mock request
                const res = await POST(req, PARAMS);

                // Assert the response
                expect(res.status).toBe(400);
                expect(await res.json()).toEqual({
                    error: "Parsing error! (Invalid content type or malformed)",
                    details:
                        "File [json]: Invalid multipart file type: application/asdasd! Supported types: image/*, text/*, application/json",
                });
            });

            it("return 400 if snip type not given", async () => {
                const req = await createMockRequestMultipart({
                    fields: [
                        {
                            data: {
                                text: "test",
                            },
                        },
                    ],
                });

                // Call the handler with the mock request
                const res = await POST(req, PARAMS);

                // Assert the response
                expect(res.status).toBe(400);
                expect(await res.json()).toEqual({
                    error: "Data ill-formatted",
                    details:
                        'No snip type given! Include \'type\' field! Currently given fields {"data":{"text":"test"}}',
                });
            });

            it("return 400 if snip data not given", async () => {
                const req = await createMockRequestMultipart({
                    fields: [
                        {
                            type: "text",
                        },
                    ],
                });

                // Call the handler with the mock request
                const res = await POST(req, PARAMS);
                const dat = await res.json();
                // Assert the response
                expect(res.status).toBe(400);
                expect(dat).toEqual({
                    error: "Data ill-formatted",
                    details:
                        'No snip data given! Include \'data\' field! Currently given fields {"type":"text"}',
                });
            });

            it("return 400 if array snip malformed", async () => {
                const req = await createMockRequestMultipart({
                    fields: [
                        {
                            type: "text",
                            data: {
                                snips: [
                                    {
                                        data: {
                                            text: "test",
                                        },
                                    },
                                ],
                            },
                        },
                    ],
                });

                const res = await POST(req, PARAMS);

                expect(res.status).toBe(400);
                expect(await res.json()).toEqual({
                    error: "Data ill-formatted",
                    details:
                        'Error parsing array snip: Error: No snip type given! Include \'type\' field! Currently given fields {"data":{"text":"test"}}',
                });
            });
        });
    });
    describe("should handle application/json", () => {
        it("json file (single)", async () => {
            // Create a mock request with a file attached
            const json_data = readFile(
                __dirname + "/test_files/test.json",
                "utf-8",
            );
            //parse data
            const json = JSON.parse(await json_data);
            const req = await createMockRequest({
                content_type: "application/json",
                body: json,
            });

            // Call the handler with the mock request
            const res = await POST(req, PARAMS);
            const data = await res.json();

            // Assert the response
            expect(res.status).toBe(200);
            expect(data).toBeDefined();
            expect(data.ids).toBeDefined();
            expect(data.ids.length).toBe(1);
        });
        it("return 400 if json is invalid", async () => {
            const req = await createMockRequest({
                content_type: "application/json",
                body: "invalid value",
            });

            // Call the handler with the mock request
            const res = await POST(req, PARAMS);

            // Assert the response
            expect(res.status).toBe(400);
            expect(await res.json()).toEqual({
                error: "Parsing error! (Invalid content type or malformed)",
                details:
                    "Invalid JSON! JSON must be an object!, Currently given: 'invalid value'",
            });
        });
    });

    describe("should handle text/plain", () => {
        it("text file (single)", async () => {
            // Create a mock request with a file attached
            const req = await createMockRequest({
                content_type: "text/plain",
                body: "test",
            });

            // Call the handler with the mock request
            const res = await POST(req, PARAMS);
            const data = await res.json();

            // Assert the response
            expect(res.status).toBe(200);
            expect(data).toBeDefined();
            expect(data.ids).toBeDefined();
            expect(data.ids.length).toBe(1);
        });
    });

    describe("should allow to test the upload", () => {
        it("return 200 if test is 'true' | '1' ", async () => {
            for (const test of ["true", "1"]) {
                const req = await createMockRequest({
                    content_type: "text/plain",
                    searchParams: new URLSearchParams(),
                    body: "test",
                });
                req.nextUrl.searchParams.set("test", test);

                // Call the handler with the mock request
                const res = await POST(req, PARAMS);

                // Assert the response
                expect(res.status).toBe(200);
                expect(await res.json()).toEqual({
                    valid: true,
                });
            }
        });

        it("return 200 (insert snip if test is false)", async () => {
            const req = await createMockRequest({
                content_type: "text/plain",
                searchParams: new URLSearchParams(),
                body: "test",
            });
            req.nextUrl.searchParams.set("test", "false");

            // Call the handler with the mock request
            const res = await POST(req, PARAMS);
            const data = await res.json();

            // Assert the response
            expect(res.status).toBe(200);
            expect(data).toBeDefined();
            expect(data.ids).toBeDefined();
            expect(data.ids.length).toBe(1);
        });

        it("return 400 if test is invalid", async () => {
            const req = await createMockRequest({
                content_type: "text/plain",
                body: "test",
            });
            req.nextUrl.searchParams.set("test", "invalid");

            // Call the handler with the mock request
            const res = await POST(req, PARAMS);
            expect(res.status).toBe(400);
            expect(await res.json()).toEqual({
                error: "Invalid 'test' query parameter",
                details:
                    "test parameter must be either 'true' or 'false' or unset",
            });
        });
    });
});
