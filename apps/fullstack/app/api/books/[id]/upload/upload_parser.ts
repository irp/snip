import { createHash, randomUUID } from "node:crypto";
import { Readable } from "node:stream";
import { ReadableStream } from "node:stream/web";

import busboy from "busboy";
import contentType from "content-type";
import { headers } from "next/headers";
import { NextRequest } from "next/server";
import { default as sharp } from "sharp";

import { get_snip_from_data } from "@snip/snips";
import {
    BaseData,
    BaseSnip,
    BaseView,
    SnipData,
} from "@snip/snips/general/base";
import { BlobData } from "@snip/snips/general/image";
import { ImageSnip } from "@snip/snips/general/image";
import { TextSnip } from "@snip/snips/general/text";
import { MacroSpecSnip } from "@snip/snips/uprp/spec/macrospec";

export interface Fields {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
}
export interface Files {
    [key: string]: File;
}
export interface File {
    filename?: string;
    encoding?: string;
    mimetype: contentType.ParsedMediaType;
    data: Buffer | string;
}

export async function parse_to_fields_and_files(
    req: NextRequest,
): Promise<[Fields, Files]> {
    let fields: Fields;
    let files: Files;
    const headersList = headers();
    const cont_header = headersList.get("content-type");
    if (!cont_header) {
        throw Error(
            `Invalid content type "${cont_header}"! Only multipart/form-data, application/x-www-form-urlencoded, text/plain and application/json are allowed!`,
        );
    }
    const contType = contentType.parse(cont_header);
    // convert req.body to readable
    const s = Readable.fromWeb(req.body as ReadableStream);

    switch (contType.type) {
        case "multipart/form-data":
        case "application/x-www-form-urlencoded":
            [fields, files] = await _parse_multipart_form_with_busboy(
                cont_header,
                s,
            );
            break;
        case "text/plain":
            // Interpreted as plain text snip
            fields = await _parse_text(s);
            files = {};
            break;
        case "application/json":
            fields = await _parse_json(s);
            files = {};
            break;
        default:
            throw Error(
                `Invalid content type "${cont_header}"! Only multipart/form-data, application/x-www-form-urlencoded, text/plain and application/json are allowed!`,
            );
    }
    return [fields, files];
}

async function _parse_multipart_form_with_busboy(
    cont_header: string,
    stream: Readable,
): Promise<[Fields, Files]> {
    return new Promise<[Fields, Files]>((resolve, reject) => {
        const fields: Fields = {};
        const files: Files = {};
        const bb = busboy({
            headers: {
                "content-type": cont_header,
            },
        });

        bb.on("file", (name, file_stream, info) => {
            const mime_type = contentType.parse(info.mimeType);
            // Check for allowed mimetype is allowed
            if (
                !mime_type.type.includes("text/") &&
                !mime_type.type.includes("image/") &&
                !mime_type.type.includes("application/json")
            ) {
                reject(
                    new Error(
                        "File [" +
                            name +
                            "]: Invalid multipart file type: " +
                            info.mimeType +
                            "! " +
                            "Supported types: image/*, text/*, application/json",
                    ),
                );
                file_stream.resume();
                return;
            }

            // Create a random key
            const key = randomUUID();

            // Parse file to buffer
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            let f: any = null;
            file_stream.on("data", (data) => {
                if (f === null) {
                    f = data;
                } else {
                    f = Buffer.concat([f, data]);
                }
            });
            file_stream.on("close", () => {
                if (f === null || f.length == 0) {
                    reject(
                        new Error(
                            "File [" +
                                name +
                                "]: No data received! " +
                                "Please check if the send file is not empty!",
                        ),
                    );
                    return;
                }
                files[key] = {
                    filename: info.filename,
                    encoding: info.encoding,
                    mimetype: mime_type,
                    data: f,
                };
            });
            file_stream.on("error", reject);
        });

        bb.on("field", (name, val) => {
            fields[name] = JSON.parse(val);
        });

        bb.on("close", () => {
            resolve([fields, files]);
        });

        bb.on("error", reject);

        return stream.pipe(bb);
    });
}

export async function _parse_json(stream: Readable): Promise<Fields> {
    const buf = await buffer(stream);
    const rawBody = buf.toString("utf8");
    const fields = JSON.parse(rawBody) as Fields;

    // check that field is not a string
    if (typeof fields === "string") {
        throw Error(
            "Invalid JSON! JSON must be an object!, Currently given: '" +
                fields +
                "'",
        );
    }
    return fields;
}

async function _parse_text(stream: Readable): Promise<Fields> {
    const buf = await buffer(stream);
    return {
        type: "text",
        data: {
            text: buf.toString("utf8"),
        },
        view: {},
    };
}
async function buffer(readable: Readable) {
    const chunks = [];
    for await (const chunk of readable) {
        chunks.push(typeof chunk === "string" ? Buffer.from(chunk) : chunk);
    }
    return Buffer.concat(chunks);
}

/** Parse the fields given by the
 * user to a snipdata object
 *
 * Only one snip is allowed as fields
 */
async function fields_to_snipdata(
    fields: Fields,
    book_id: number,
): Promise<SnipData<BaseData, BaseView> | null> {
    if (Object.keys(fields).length == 0) {
        //console.error("No fields given");
        return null;
    }

    if (!fields.type && !fields.data?.type) {
        throw Error(
            `No snip type given! Include 'type' field! Currently given fields ${JSON.stringify(
                fields,
            )}`,
        );
    }

    if (!fields.data) {
        throw Error(
            `No snip data given! Include 'data' field! Currently given fields ${JSON.stringify(
                fields,
            )}`,
        );
    }

    // If parsed json has no view, add empty view
    if (!fields.view) {
        fields.view = {};
    }

    // Check if array snip with a bit of recursion
    if (fields.data.snips) {
        fields.data.snips = await Promise.all(
            fields.data.snips.map(async (snip: Fields) => {
                try {
                    return await fields_to_snipdata(snip, book_id);
                } catch (e) {
                    throw Error("Error parsing array snip: " + e);
                }
            }),
        );
    }

    // Set book id
    fields.book_id = book_id;

    return fields as SnipData<BaseData, BaseView>;
}

export async function fields_to_snip(
    fields: Fields,
    book_id: number,
): Promise<BaseSnip | null> {
    const snip_data = await fields_to_snipdata(fields, book_id);
    if (!snip_data) {
        return null;
    }

    // Parse data to snip
    const snip = get_snip_from_data(snip_data);
    return snip;
}

/** Parse the files given by the user
 * to a snipdata object
 *
 * Multiple snips are allowed as files
 */
export async function files_to_snips(
    files: Files,
    book_id: number,
): Promise<BaseSnip[]> {
    const snip_promises: Promise<BaseSnip | null>[] = [];

    for (const [, file] of Object.entries(files)) {
        switch (file.mimetype.type) {
            case "image/png":
            case "image/jpeg":
            case "image/jpg":
            case "image/webp":
                snip_promises.push(_image_file_to_snip(file, book_id));
                break;
            case "text/plain":
            case "text/txt":
                snip_promises.push(_text_file_to_snip(file, book_id));
                break;
            case "text/macro":
            case "text/macro.spec":
                snip_promises.push(_macro_file_to_snip(file, book_id));
                break;
            case "application/json":
                snip_promises.push(_json_file_to_snip(file, book_id));
                break;
        }
    }

    return (await Promise.all(snip_promises)).filter((s) => s !== null);
}

async function _image_file_to_snip(
    file: File,
    book_id: number,
): Promise<ImageSnip> {
    // Compress image to webp
    const buffer = await sharp(file.data)
        .webp({
            quality: 80,
            effort: 6,
        })
        .toBuffer();

    // Create md5 hash
    const md5 = createHash("md5").update(buffer).digest("hex");

    // Create blob with buffer
    const blob: BlobData = {
        id: -1,
        size: buffer.length,
        data: buffer.toString("base64"),
        mime: "image/webp",
        hash: md5,
    };

    // Create image snip
    const snip = new ImageSnip({
        blob: blob,
        book_id: book_id,
    });
    return snip;
}

async function _text_file_to_snip(
    file: File,
    book_id: number,
): Promise<TextSnip> {
    // Parse text
    const text = file.data instanceof Buffer ? file.data.toString() : file.data;

    // Create text snip
    const snip = new TextSnip({
        text: text,
        book_id: book_id,
        lineWrap: -1,
    });
    return snip;
}

async function _macro_file_to_snip(
    file: File,
    book_id: number,
): Promise<MacroSpecSnip> {
    const buffer =
        file.data instanceof Buffer ? file.data : Buffer.from(file.data);
    // Create md5 hash
    const md5 = createHash("md5").update(buffer).digest("hex");

    // Create blob with buffer
    const blob: BlobData = {
        id: -1,
        size: buffer.length,
        data: buffer.toString("base64"),
        mime: file.mimetype.type,
        hash: md5,
    };

    // Create macro.spec snip
    const snip = new MacroSpecSnip({
        blob,
        book_id,
    });

    return snip;
}

async function _json_file_to_snip(
    file: File,
    book_id: number,
): Promise<BaseSnip | null> {
    // Parse json
    const json = JSON.parse(
        file.data instanceof Buffer ? file.data.toString() : file.data,
    );

    // Create snip
    const snip = await fields_to_snip(json, book_id);

    return snip;
}
