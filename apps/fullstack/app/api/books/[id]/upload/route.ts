import {
    RequestPropsWithPerms,
    wrapperBookPerms,
} from "lib/route_wrapper/wrapperBookPerms";
import { NextRequest, NextResponse } from "next/server";
import { io } from "socket.io-client";

import { signApiToken } from "@snip/auth/tokens/socket";
import config from "@snip/config/server";
import service from "@snip/database";
import { SnipDataInsert } from "@snip/database/types";
import { BaseSnip } from "@snip/snips/general/base";

import {
    type Fields,
    fields_to_snip,
    type Files,
    files_to_snips,
    parse_to_fields_and_files,
} from "./upload_parser";

/** Essentially we need
 * a session & the user to be logged in
 * and want the perms of the user for this
 * book.
 */
export const POST = wrapperBookPerms(upload);

async function upload(
    req: NextRequest,
    { perms, book_id }: RequestPropsWithPerms,
) {
    //Check access
    if (!perms.pWrite) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details:
                    "You do not have write access to this book or it is finished!",
            },
            { status: 401 },
        );
    }

    //optional query test
    const test = req.nextUrl.searchParams.get("test")?.toLowerCase();
    let database_upload = true;
    if (test && (test == "true" || test == "1")) {
        database_upload = false;
    } else if (test && (test == "false" || test == "0")) {
        database_upload = true;
    } else if (test) {
        return NextResponse.json(
            {
                error: "Invalid 'test' query parameter",
                details:
                    "test parameter must be either 'true' or 'false' or unset",
            },
            { status: 400 },
        );
    }

    // Parse fields/files of request depending on content type
    let fields: Fields | undefined;
    let files: Files | undefined;
    try {
        [fields, files] = await parse_to_fields_and_files(req);
    } catch (e) {
        let msg = "";
        if (e instanceof Error) {
            msg = e.message;
        }
        return NextResponse.json(
            {
                error: "Parsing error! (Invalid content type or malformed)",
                details: msg,
            },
            { status: 400 },
        );
    }

    // Return if no data is given
    if (
        (!fields || Object.keys(fields).length == 0) &&
        (!files || Object.keys(files).length == 0)
    ) {
        return NextResponse.json(
            {
                error: "No data provided!",
                details: "Please provide a file or a multipart form!",
            },
            { status: 400 },
        );
    }

    // Check if the data conforms to a snippet type specification
    // i.e. if we know how to parse it
    // If not we return an (hopefully meaningful) error
    // (First we check the fields, then the files)
    let snips: BaseSnip[] = [];
    try {
        if (fields) {
            const s = await fields_to_snip(fields, book_id);
            if (s) snips.push(s);
        }
        if (files) {
            snips = snips.concat(await files_to_snips(files, book_id));
        }
        // Remove all null values
        snips = snips.filter((snip) => snip !== null);
    } catch (e) {
        let msg = "";
        if (e instanceof Error) {
            msg = e.message;
        }
        //print traceback
        return NextResponse.json(
            {
                error: "Data ill-formatted",
                details: msg,
            },
            { status: 400 },
        );
    }

    // If upload was not specified return with valid message
    if (!database_upload) {
        return NextResponse.json({ valid: true });
    }

    // Upload snips to the database
    // This should normally not fail
    // nonetheless we expose the error message
    // here if it does
    const ids: number[] = [];
    for (const snip of snips) {
        const snip_data = snip.to_data();
        const snip_data_ = await service.snip.insert(
            snip_data as SnipDataInsert,
        );
        ids.push(snip_data_.id!);
    }

    // Trigger ui update for clients
    // this is handled by the websocket
    // connection

    const client = getSocketClient();
    client.on("connect", async () => {
        console.log("Connected to socket server");
        client.emit("api:external_upload", ids);
        client.close();
    });
    client.connect();

    // Return inserted snip ids and references
    // to links
    const links: Record<string, { ui: string; api: string }> = {};

    for (const id of ids) {
        links[id] = {
            ui: config.getServerUrl() + `snips/${id}`,
            api: config.getServerUrl() + `snips/${id}/raw`,
        };
    }

    return NextResponse.json({ ids: ids, links }, { status: 200 });
}

/** Connects to socket server in admin
 * namespace and returns the socket.
 *
 * This should only be used server side.
 */
function getSocketClient() {
    const token = signApiToken({
        api: true,
        date: Date.now(),
    });

    const client = io(`ws://snip_socket:80/api`, {
        reconnectionDelay: 1000,
        reconnection: true,
        reconnectionAttempts: 10,
        path: "/socket/",
        transports: ["websocket"],
        agent: false,
        upgrade: false,
        withCredentials: true,
        autoConnect: false,
        extraHeaders: {
            Authorization: `Bearer ${token}`,
        },
    });

    client.on("error", (error) => {
        console.error("Socket error", error);
    });
    client.on("connect_error", (error) => {
        console.error("Socket connect error", error);
        client.close();
    });
    client.on("reconnect", (attempt) => {
        console.log("Reconnected to socket server", attempt);
    });

    return client;
}
