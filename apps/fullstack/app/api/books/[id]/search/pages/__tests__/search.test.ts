import { createMockRequest } from "__mocks__/request";

import { GET } from "../route";

/** Mock the database connection
 * so that we can test the API
 */
vi.mock("@snip/database/sql.connection", () => {
    return {
        pool_data: {
            query: vi.fn(async (sql, params) => {
                // this is a unit test, so we don't care about the actual query
                // we have to check that at some point though
                if (
                    globalThis.mockConfig.service.throw_error == true ||
                    globalThis.mockConfig.service.throw_error ==
                        "sql_connection.query"
                ) {
                    throw new Error(
                        globalThis.mockConfig.service.error_message ||
                            "Mock error",
                    );
                } else {
                    return [{ page_id: 1 }, { page_id: 2 }];
                }
            }),
        },
    };
});

const PARAMS = { params: { id: "1" } };

describe("api/books/[id]/search/pages", () => {
    let consoleSpy: any;
    beforeAll(() => {
        consoleSpy = vi.spyOn(console, "error").mockImplementation(() => {});
    });

    describe("should gracefully handle errors", () => {
        it("should return 400 if no searchParams are given", async () => {
            const req = await createMockRequest({
                method: "GET",
                searchParams: new URLSearchParams(),
            });
            const res = await GET(req, PARAMS);
            expect(res.status).toBe(400);
            expect(await res.json()).toEqual(
                expect.objectContaining({
                    error: "Invalid search query",
                }),
            );
        });

        it("should return 400 if search query is invalid", async () => {
            const req = await createMockRequest({
                method: "GET",
                searchParams: new URLSearchParams([["search", ""]]),
            });
            const res = await GET(req, PARAMS);
            expect(res.status).toBe(400);
            expect(await res.json()).toEqual(
                expect.objectContaining({
                    error: "Invalid search query",
                }),
            );
        });

        it("should return 500 if database throws an error", async () => {
            globalThis.mockConfig.service.throw_error = "sql_connection.query";
            const req = await createMockRequest({
                method: "GET",
                searchParams: new URLSearchParams([["search", "test"]]),
            });
            const res = await GET(req, PARAMS);
            expect(res.status).toBe(500);
            expect(await res.json()).toEqual(
                expect.objectContaining({
                    error: "Internal Server Error",
                }),
            );
        });

        it("should return 401 if user has no read access", async () => {
            globalThis.mockConfig.book_perms.pRead = false;
            const req = await createMockRequest({
                method: "GET",
                searchParams: new URLSearchParams([["search", "test"]]),
            });
            const res = await GET(req, { params: { id: "2" } });
            expect(res.status).toBe(401);
            expect(await res.json()).toEqual(
                expect.objectContaining({
                    error: "Unauthorized",
                }),
            );
            expect(consoleSpy).toHaveBeenCalledTimes(1);
            mockConfig.book_perms.pRead = true;
        });
    });

    it("should return a list of pageids", async () => {
        const req = await createMockRequest({
            method: "GET",
            searchParams: new URLSearchParams([["search", "test"]]),
        });
        const res = await GET(req, PARAMS);
        expect(res.status).toBe(200);
        expect(await res.json()).toEqual([1, 2]);
    });

    it("detect all search modifiers", async () => {
        //Set perms to true
        mockConfig.book_perms.pRead = true;

        let search = "";
        search += "page:1";
        search += " pageid:2";
        search += " snip:3";
        search += " snipid:4";
        search += " snip_text:5";
        search += ' "default search"';
        const req = await createMockRequest({
            method: "GET",
            searchParams: new URLSearchParams([["search", search]]),
        });

        const res = await GET(req, PARAMS);

        expect(res.status).toBe(200);
    });

    it("detect multiples of all search modifiers", async () => {
        //Set perms to true
        mockConfig.book_perms.pRead = true;

        let search = "";
        search += "page:1";
        search += " pageid:2";
        search += " snip:3";
        search += " snipid:4";
        search += " snip_text:5";
        search += ' "default search" ';

        search += search;

        const req = await createMockRequest({
            method: "GET",
            searchParams: new URLSearchParams([["search", search]]),
        });

        const res = await GET(req, PARAMS);

        expect(res.status).toBe(200);
    });

    it("perfom no search if empty string is given", async () => {
        const req = await createMockRequest({
            method: "GET",
            searchParams: new URLSearchParams([["search", "     "]]),
        });

        const res = await GET(req, PARAMS);
        expect(res.status).toBe(200);
        expect(await res.json()).toEqual([]);

        const req2 = await createMockRequest({
            method: "GET",
            searchParams: new URLSearchParams([["search", " "]]),
        });

        const res2 = await GET(req2, PARAMS);
        expect(res2.status).toBe(200);
        expect(await res2.json()).toEqual([]);
    });

    it("perform only snip table search", async () => {
        const req = await createMockRequest({
            method: "GET",
            searchParams: new URLSearchParams([["search", "snipid:1"]]),
        });

        const res = await GET(req, PARAMS);
        expect(res.status).toBe(200);
        expect(await res.json()).toEqual([1, 2]);
    });

    it("perform only pages table search", async () => {
        const req = await createMockRequest({
            method: "GET",
            searchParams: new URLSearchParams([["search", "pageid:1"]]),
        });

        const res = await GET(req, PARAMS);
        expect(res.status).toBe(200);
        expect(await res.json()).toEqual([1, 2]);
    });
});
