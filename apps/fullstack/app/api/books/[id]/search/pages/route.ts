import {
    RequestPropsWithPerms,
    wrapperBookPerms,
} from "lib/route_wrapper/wrapperBookPerms";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import { pool_data } from "@snip/database/sql.connection";

import { parseSearchString, SearchModifier } from "../search_parsing";

export const GET = wrapperBookPerms(searchForPages);

const schemaQuery = z.object({
    search: z.string().min(1),
});

/**
 * Search for pages by string and return a list of pageids
 *
 * README
 * ------
 * See the searchbar docs for more explanation on how
 * the search should work.
 *
 * Parameters:
 * -----------
 * - id: number The book id
 * - search: string The search query
 */
async function searchForPages(
    req: NextRequest,
    { perms, book_id }: RequestPropsWithPerms,
) {
    // Check read access
    if (!perms.pRead) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have read access to this book!",
            },
            { status: 401 },
        );
    }

    // Get search params
    const searchParams = req.nextUrl.searchParams;
    const search = searchParams.get("search");

    // Parse with zod
    const res = schemaQuery.safeParse({ search });

    if (res.success === false) {
        return NextResponse.json(
            {
                error: "Invalid search query",
                details: res.error.errors,
            },
            { status: 400 },
        );
    }

    // Parse search string with modifiers
    const parsed_search: SearchModifier[] = parseSearchString(res.data.search);

    /** Build sql query
     *  the basic query is just selecting all pages from the book
     *  and then we further restrict the results based on the modifiers
     *
     *  */

    const search_params = new Map<string, string[] | string>();
    search_params.set("book_ids", [book_id.toString()]);

    for (const modifier of parsed_search) {
        switch (modifier.type) {
            case "pageid":
                // Set page id values if the do not exist else update
                if (search_params.has("page_ids")) {
                    search_params.set(
                        "page_ids",
                        (search_params.get("page_ids") as string[])?.concat(
                            modifier.value,
                        ) ?? [],
                    );
                } else {
                    search_params.set("page_ids", modifier.value);
                }
                break;
            case "page":
                // Page number
                if (search_params.has("page_numbers")) {
                    search_params.set(
                        "page_numbers",
                        (search_params.get("page_numbers") as string[])?.concat(
                            modifier.value,
                        ) ?? [],
                    );
                } else {
                    search_params.set(
                        "page_numbers",
                        modifier.value.map((v) => parseInt(v) - 1).map(String),
                    );
                }
                break;
            case "snipid":
                // Set snip id values if the do not exist else update
                if (search_params.has("snip_ids")) {
                    search_params.set(
                        "snip_ids",
                        (search_params.get("snip_ids") as string[])?.concat(
                            modifier.value,
                        ) ?? [],
                    );
                } else {
                    search_params.set("snip_ids", modifier.value);
                }
                break;
            case "snip":
                if (search_params.has("snip_type")) {
                    search_params.set(
                        "snip_type",
                        (search_params.get("snip_type") as string[])?.concat(
                            modifier.value,
                        ) ?? [],
                    );
                } else {
                    search_params.set("snip_type", modifier.value);
                }
                break;
            default:
                if (search_params.has("snip_text")) {
                    search_params.set(
                        "snip_text",
                        (search_params.get("snip_text") as string[])?.concat(
                            modifier.value,
                        ) ?? [],
                    );
                } else {
                    search_params.set("snip_text", modifier.value);
                }

                break;
        }
    }

    let perform_search_pages_table = false;
    let sql_base_pages = `Select id as page_id FROM pages WHERE book_id in (:book_ids)`;
    if (
        search_params.has("page_ids") &&
        search_params.get("page_ids")!.length > 0
    ) {
        sql_base_pages += ` AND id IN (:page_ids)`;
        perform_search_pages_table = true;
    }
    if (
        search_params.has("page_numbers") &&
        search_params.get("page_numbers")!.length > 0
    ) {
        sql_base_pages += ` AND page_number IN (:page_numbers)`;
        perform_search_pages_table = true;
    }

    let perform_search_snips_table = false;
    let sql_base_snips = `Select page_id FROM snips WHERE book_id in (:book_ids)`;
    if (
        search_params.has("snip_ids") &&
        search_params.get("snip_ids")!.length > 0
    ) {
        sql_base_snips += ` AND id IN (:snip_ids)`;
        perform_search_snips_table = true;
    }
    if (
        search_params.has("snip_type") &&
        search_params.get("snip_type")!.length > 0
    ) {
        const snip_types = search_params.get("snip_type")!;
        for (let i = 0; i < snip_types.length; i++) {
            sql_base_snips += ` AND type LIKE concat('%', :snip_type_${i}, '%')`;
            search_params.set(`snip_type_${i}`, snip_types[i]!);
        }
        search_params.delete("snip_type");
        perform_search_snips_table = true;
    }
    if (
        search_params.has("snip_text") &&
        search_params.get("snip_text")!.length > 0
    ) {
        const snip_content = search_params.get("snip_text")!;
        for (let i = 0; i < snip_content.length; i++) {
            // NOTE: if this is too slow we might want to think about a fulltext index/ searchable column
            sql_base_snips += ` AND JSON_VALUE(data_json, '$.text') LIKE concat('%', :snip_text_${i}, '%')`;
            search_params.set(`snip_text_${i}`, snip_content[i]!);
        }
        search_params.delete("snip_text");
        perform_search_snips_table = true;
    }

    let sql: string;
    if (perform_search_pages_table && perform_search_snips_table) {
        sql = `SELECT DISTINCT page_id FROM (${sql_base_pages} INTERSECT ${sql_base_snips})`;
    }
    if (perform_search_pages_table && !perform_search_snips_table) {
        sql = sql_base_pages;
    }
    if (!perform_search_pages_table && perform_search_snips_table) {
        sql = sql_base_snips;
    }
    if (!perform_search_pages_table && !perform_search_snips_table) {
        // Return empty list (no pages found)
        return NextResponse.json([], { status: 200 });
    }

    const page_ids: number[] = await pool_data
        .query(
            { namedPlaceholders: true, sql: sql! },
            Object.fromEntries(search_params),
        )
        .then((res) => {
            if (res.length < 1) {
                // Return empty list (no pages found)
                return [];
            }
            return (res as { page_id: number }[]).map((row) => row.page_id);
        });

    return NextResponse.json(page_ids, { status: 200 });
}
