import {
    parseNumberRangeValue,
    parseSearchString,
    type SearchModifier,
    splitStringByWords,
    splitStringModifier,
} from "../search_parsing";

describe("splitStringByWords", () => {
    it("should split the input string into words", () => {
        const inputString = 'Hello "world"! This is a test.';
        const expectedResult = [
            "Hello",
            "world",
            "!",
            "This",
            "is",
            "a",
            "test.",
        ];

        const result = splitStringByWords(inputString);

        expect(result).toEqual(expectedResult);
    });

    it("should handle empty input string", () => {
        const inputString = "";
        const expectedResult: string[] = [];

        const result = splitStringByWords(inputString);

        expect(result).toEqual(expectedResult);
    });

    it("should handle input string with only quotes", () => {
        const inputString = '""';
        const expectedResult: string[] = [];

        const result = splitStringByWords(inputString);

        expect(result).toEqual(expectedResult);
    });

    it("should handle input string with multiple quoted words", () => {
        const inputString = '"Hello" "world"!';
        const expectedResult = ["Hello", "world", "!"];

        const result = splitStringByWords(inputString);

        expect(result).toEqual(expectedResult);
    });

    it("should handle input with numbers and comma seperation", () => {
        const inputString = "hello 1,2,3";
        const expectedResult = ["hello", "1,2,3"];

        const result = splitStringByWords(inputString);

        expect(result).toEqual(expectedResult);
    });

    it("should handle modifiers", () => {
        const inputString = 'snip:1,2,3 "hello world" page:1-5';
        const expectedResult = ["snip:1,2,3", "hello world", "page:1-5"];

        const result = splitStringByWords(inputString);

        expect(result).toEqual(expectedResult);
    });
});

describe("parseNumberRangeValue", () => {
    it("should parse a single number", () => {
        const result = parseNumberRangeValue("5");
        expect(result).toEqual(["5"]);
    });

    it("should parse a range of numbers", () => {
        const result = parseNumberRangeValue("1-5");
        expect(result).toEqual(["1", "2", "3", "4", "5"]);
    });

    it("should ignore non-numeric values", () => {
        const result = parseNumberRangeValue("1-5,abc,10-12,def,a-g");
        expect(result).toEqual(["1", "2", "3", "4", "5", "10", "11", "12"]);
    });

    it("should handle invalid ranges", () => {
        const result = parseNumberRangeValue("5-1");
        expect(result).toEqual([]);
    });

    it("should handle empty input", () => {
        const result = parseNumberRangeValue("");
        expect(result).toEqual([]);
    });

    it("should handle input with only non-numeric values", () => {
        const result = parseNumberRangeValue("abc,def,ghi");
        expect(result).toEqual([]);
    });

    it("should handle input with multiple ranges", () => {
        const result = parseNumberRangeValue("1-3,5-7,9-10");
        expect(result).toEqual(["1", "2", "3", "5", "6", "7", "9", "10"]);
    });
});

describe("parseSearchString", () => {
    it("simple search default modifier", () => {
        const search = "this is a search";
        const expectedResult: SearchModifier[] = [
            {
                type: "default",
                value: ["this", "is", "a", "search"],
            },
        ];
        const result = parseSearchString(search);
        expect(result).toEqual(expectedResult);
    });

    it("simple search page modifier", () => {
        const validModifiers = ["page", "snip", "pageid", "snipid"];

        for (const modifier of validModifiers) {
            const search = `${modifier}:1`;
            const expectedResult: SearchModifier[] = [
                {
                    type: modifier as SearchModifier["type"],
                    value: ["1"],
                },
            ];
            const result = parseSearchString(search);
            expect(result).toEqual(expectedResult);
        }
    });

    it("complex search", () => {
        const search = 'page:1,2,3 "this is a search" wow snip:test,hello';
        const expectedResult: SearchModifier[] = [
            {
                type: "default",
                value: ["this is a search", "wow"],
            },
            {
                type: "page",
                value: ["1", "2", "3"],
            },
            {
                type: "snip",
                value: ["test", "hello"],
            },
        ];
        const result = parseSearchString(search);
        expect(result).toEqual(expectedResult);
    });

    it("should handle nonexistent modifiers", () => {
        const search = "hello:world";
        const expectedResult: SearchModifier[] = [
            {
                type: "default",
                value: ["hello:world"],
            },
        ];
        const result = parseSearchString(search);
        expect(result).toEqual(expectedResult);
    });

    // Add more test cases here...
});
describe("splitStringModifier", () => {
    it("should split the input string into modifiers and remaining string", () => {
        const inputString = 'snip:1,2,3 "hello world" page:1-5';
        const expectedModifiers = ["snip:1,2,3", "page:1-5"];
        const expectedRemainingString = '"hello world"';

        const [modifiers, remainingString] = splitStringModifier(inputString);

        expect(modifiers).toEqual(expectedModifiers);
        expect(remainingString).toEqual(expectedRemainingString);
    });

    it("should handle input string with no modifiers", () => {
        const inputString = 'Hello "world"! This is a test.';
        const expectedModifiers: string[] = [];
        const expectedRemainingString = 'Hello "world"! This is a test.';

        const [modifiers, remainingString] = splitStringModifier(inputString);

        expect(modifiers).toEqual(expectedModifiers);
        expect(remainingString).toEqual(expectedRemainingString);
    });

    it("should handle empty input string", () => {
        const inputString = "";
        const expectedModifiers: string[] = [];
        const expectedRemainingString = "";

        const [modifiers, remainingString] = splitStringModifier(inputString);

        expect(modifiers).toEqual(expectedModifiers);
        expect(remainingString).toEqual(expectedRemainingString);
    });

    it("should handle input string with only modifiers", () => {
        const inputString = "snip:1,2,3 page:1-5";
        const expectedModifiers = ["snip:1,2,3", "page:1-5"];
        const expectedRemainingString = "";

        const [modifiers, remainingString] = splitStringModifier(inputString);

        expect(modifiers).toEqual(expectedModifiers);
        expect(remainingString).toEqual(expectedRemainingString);
    });

    it("should handle input string with multiple modifiers", () => {
        const inputString = 'snip:1,2,3 "hello world" page:1-5 author:John';
        const expectedModifiers = ["snip:1,2,3", "page:1-5", "author:John"];
        const expectedRemainingString = '"hello world"';

        const [modifiers, remainingString] = splitStringModifier(inputString);

        expect(modifiers).toEqual(expectedModifiers);
        expect(remainingString).toEqual(expectedRemainingString);
    });
});
