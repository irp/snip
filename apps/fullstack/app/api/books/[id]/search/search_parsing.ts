export interface InternalSearch {
    book_id: number;
    raw_search: string;
    parsed: SearchModifier[];
}

export interface SearchModifier {
    type: "pageid" | "snipid" | "page" | "snip" | "default";
    value: string[];
}

/** Parse a search string into a list of modifiers which
 * can be used to query the database
 *
 *
 */
export function parseSearchString(search: string): SearchModifier[] {
    const parsed: SearchModifier[] = [];
    const raw_search = search.trim();

    // Remove modifiers
    const [modifiers, remainingString] = splitStringModifier(raw_search);

    // Parse remaining string
    if (remainingString !== "") {
        parsed.push({
            type: "default",
            value: splitStringByWords(remainingString),
        });
    }

    // Parse modifiers
    for (const modifier of modifiers) {
        const [key, value] = modifier.split(/:|=/);

        // Check if key is a valid modifier
        switch (key) {
            case "pageid":
            case "snipid":
            case "page":
                parsed.push({
                    type: key as SearchModifier["type"],
                    value: parseNumberRangeValue(value!),
                });
                break;
            case "snip":
                parsed.push({
                    type: key as SearchModifier["type"],
                    value: parseStringValue(value!).flatMap((v) =>
                        v.split(","),
                    ),
                });
                break;
            default:
                parsed.push({ type: "default", value: [modifier] });
                break;
        }
    }

    return parsed;
}

/** Split a string by words, ignoring words inside quotes
 * e.g. "hello world" => ["hello world"]
 *
 */
export function splitStringByWords(inputString: string): string[] {
    // Regular expression to match words outside quotes including dots, commas, or numbers
    const regex = /"[^"]+"|[\w\d:,'-.\S]+/g;

    // Use the regular expression to split the string
    const result = inputString.match(regex) || [];

    // Remove the quotes from words inside quotes
    const finalResult = result.map((word) =>
        word.startsWith('"') && word.endsWith('"') ? word.slice(1, -1) : word,
    );

    // Remove empty strings
    return finalResult.filter((word) => word !== "");
}

/** Parse a modifier value which by specification is a number, range or list
 * e.g. snip:1,2,3 or snip:1-3 or snip:1 or snip:1,2-4
 *
 */
export function parseNumberRangeValue(value: string): string[] {
    const values = value.split(",");

    // Check if all values are numbers or ranges
    const parsed: string[] = [];
    for (const value of values) {
        const [start, end] = value.split("-");

        if (end) {
            // Check if both start and end are numbers
            if (isNaN(parseInt(start!)) || isNaN(parseInt(end))) {
                // error
                continue;
            }
            // Check if start is smaller than end
            if (parseInt(start!) > parseInt(end)) {
                // error
                continue;
            }
            // Push all numbers in range
            for (let i = parseInt(start!); i <= parseInt(end); i++) {
                parsed.push(i.toString());
            }
        } else {
            // Check if start is a number
            if (isNaN(parseInt(start!))) {
                // error
                continue;
            }
            parsed.push(start!);
        }
    }
    return parsed;
}

/** Parse a string modifier value which by specification is a list
 *
 */
export function parseStringValue(value: string): string[] {
    return splitStringByWords(value);
}

/** Remove modifiers from a string
 * e.g. page:1,2,3 "this is a search" snip:test,hello
 * => ["page:1,2,3","snip:test,hello"], "this is a search"
 *
 */
export function splitStringModifier(inputString: string): [string[], string] {
    const modifiersRegex = /(\b\w+:\S+\b)/g;
    const modifiers: string[] = [];
    let remainingString = inputString.replace(modifiersRegex, (match) => {
        modifiers.push(match);
        return "";
    });
    remainingString = remainingString.trim();
    return [modifiers, remainingString];
}
