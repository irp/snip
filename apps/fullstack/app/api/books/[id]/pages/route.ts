import {
    RequestPropsWithPerms,
    wrapperBookPerms,
} from "lib/route_wrapper/wrapperBookPerms";
import { NextRequest, NextResponse } from "next/server";

import service from "@snip/database";

/** Get all pages from the book
 */
export const GET = wrapperBookPerms(getPages);

async function getPages(
    req: NextRequest,
    { perms, book_id }: RequestPropsWithPerms,
) {
    // Check read access
    if (!perms.pRead) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have read access to this book!",
            },
            { status: 401 },
        );
    }

    // Get all pages
    const pages = await service.page.getByBookId(book_id);
    return NextResponse.json(pages);
}
