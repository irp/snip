import {
    RequestPropsWithPerms,
    wrapperBookPerms,
} from "lib/route_wrapper/wrapperBookPerms";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import service from "@snip/database";

/** Get the book by id
 */
export const GET = wrapperBookPerms(getBook, false);
/** Modify the book
 */
export const POST = wrapperBookPerms(editBook, false);

async function getBook(
    _req: NextRequest,
    { perms, book_id }: RequestPropsWithPerms,
) {
    // Check read access
    if (!perms.pRead) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have read access to this book!",
            },
            { status: 401 },
        );
    }

    const book = await service.book.getById(book_id);
    return NextResponse.json(book);
}

async function editBook(
    req: NextRequest,
    { perms, book_id, session }: RequestPropsWithPerms,
) {
    if (!perms.pWrite) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have write access to this book!",
            },
            { status: 401 },
        );
    }

    const res = await req.json();

    const schema = z
        .object({
            title: z
                .string()
                .min(3, {
                    message: "The book title must be at least 3 characters",
                })
                .max(256, {
                    message: "The book title must be at most 256 characters",
                })
                .optional(),
            comment: z.string().optional(),
            finished: z.boolean().optional(),
            cover_page_id: z.number().optional(),
            owner_user_id: z.number().optional(),
        })
        .strict();

    const result = schema.safeParse(res);
    if (result.success === false) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: result.error.errors.map((e) => e.message).join(", "),
            },
            { status: 400 },
        );
    }

    const { title, comment, finished, cover_page_id, owner_user_id } =
        result.data;

    let performUpdate = false;
    const book = await service.book.getById(book_id);
    let updatedBook = { ...book };
    if (title && title !== book.title) {
        updatedBook.title = title;
        performUpdate = true;
    }

    if (comment && comment !== book.comment) {
        updatedBook.comment = comment;
        performUpdate = true;
    }

    if (finished !== undefined && finished !== (book.finished !== null)) {
        updatedBook.finished = finished ? new Date() : null;
        performUpdate = true;
    }

    if (cover_page_id && cover_page_id !== book.cover_page_id) {
        const page = await service.page.getById(cover_page_id);
        if (!page) {
            throw new Error("Page does not exist");
        }
        if (page.book_id !== book_id) {
            throw new Error("Page does not belong to this book");
        }
        updatedBook.cover_page_id = cover_page_id;
        performUpdate = true;
    }

    /* -------------------------------------------------------------------------- */
    /*                           special: transfer owner                          */
    /* -------------------------------------------------------------------------- */

    if (
        owner_user_id &&
        owner_user_id !== book.owner_id &&
        book.owner_type === "user"
    ) {
        // perms
        if (!session.user || session.user.id !== book.owner_id) {
            return NextResponse.json(
                {
                    error: "Unauthorized",
                    details: "You are not the owner of this book!",
                },
                { status: 401 },
            );
        }
        updatedBook.owner_id = owner_user_id;
        performUpdate = true;
    }

    /* -------------------------------------------------------------------------- */
    /*                               preform update                               */
    /* -------------------------------------------------------------------------- */

    if (performUpdate) {
        updatedBook = await service.book.update(updatedBook);
    }

    return NextResponse.json(updatedBook, { status: 200 });
}
