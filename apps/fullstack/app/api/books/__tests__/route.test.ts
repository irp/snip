import { createMockRequest } from "__mocks__/request";
import { POST, GET } from "../route";
import { setSessionType } from "__mocks__/mockConfig";

describe("POST api/books/ (create)", () => {
    describe("should gracefully handle errors", () => {
        it("return 400 on invalid query", async () => {
            const req = await createMockRequest({
                method: "POST",
                content_type: "application/json",
                body: "asdasdaasd!",
            });
            const res = await POST(req, {});
            expect(res.status).toBe(400);
            expect(await res.json()).toEqual({
                error: "Bad Request",
                details: "Expected object, received string",
            });
        });

        it("return 400 on missing title", async () => {
            const req = await createMockRequest({
                method: "POST",
                content_type: "application/json",
                body: {},
            });
            const res = await POST(req, {});
            expect(res.status).toBe(400);
            expect(await res.json()).toEqual({
                error: "Bad Request",
                details: "The book title is required",
            });
        });

        it("return 400 on invalid title", async () => {
            const req = await createMockRequest({
                method: "POST",
                content_type: "application/json",
                body: { title: "a" },
            });
            const res = await POST(req, {});
            expect(res.status).toBe(400);
            expect(await res.json()).toEqual({
                error: "Bad Request",
                details: "The book title must be at least 3 characters",
            });

            const req2 = await createMockRequest({
                method: "POST",
                content_type: "application/json",
                body: { title: "a".repeat(257) },
            });
            const res2 = await POST(req2, {});
            expect(res2.status).toBe(400);
            expect(await res2.json()).toEqual({
                error: "Bad Request",
                details: "The book title must be at most 256 characters",
            });
        });
    });

    it("should create a book", async () => {
        const req = await createMockRequest({
            method: "POST",
            content_type: "application/json",
            body: { title: "Test" },
        });
        const res = await POST(req, {});
        expect(res.status).toBe(200);
        const data = await res.json();

        expect(data.id).toBeDefined();
        expect(data.title).toBe("Test");
    });

    it("should create a book with comment", async () => {
        const req = await createMockRequest({
            method: "POST",
            content_type: "application/json",
            body: { title: "Test", comment: "Test comment" },
        });
        const res = await POST(req, {});
        expect(res.status).toBe(200);
        const data = await res.json();

        expect(data.id).toBeDefined();
        expect(data.title).toBe("Test");
        expect(data.comment).toBe("Test comment");
    });
});

describe("GET api/books/ (get)", () => {
    it("should get books", async () => {
        const req = await createMockRequest({
            method: "GET",
        });
        const res = await GET(req, {});
        expect(res.status).toBe(200);
        const data = await res.json();

        // Check that book and pages in data
        expect(data).toHaveProperty("books");
        expect(data.books).toBeInstanceOf(Array);

        // With pages
        const req2 = await createMockRequest({
            method: "GET",
            searchParams: new URLSearchParams({ include: "pages" }),
        });
        const res2 = await GET(req2, {});
        expect(res2.status).toBe(200);
        const data2 = await res2.json();
        expect(data2).toHaveProperty("pages");
        expect(data2.pages).toBeInstanceOf(Array);

        // Test with api session
        setSessionType("api_token");
        const res3 = await GET(req, {});
        expect(res3.status).toBe(401);
        setSessionType("ui_token");
        const res4 = await GET(req, {});
        expect(res4.status).toBe(401);
    });
});
