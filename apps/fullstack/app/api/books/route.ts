import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";

import service from "@snip/database";
import { BookDataInsert, BookDataRet, PageDataRet } from "@snip/database/types";

/** Get all books for user
 */
export const GET = wrapperLoggedInUser(getBooks);

/** Create a new book
 * with the user as author/owner
 */
export const POST = wrapperLoggedInUser(createBook);

async function createBook(
    req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    const res = await req.json();

    const schema = z
        .object({
            title: z
                .string({
                    required_error: "The book title is required",
                })
                .min(3, {
                    message: "The book title must be at least 3 characters",
                })
                .max(256, {
                    message: "The book title must be at most 256 characters",
                }),
            comment: z.string().optional(),
        })
        .strict();

    const result = schema.safeParse(res);
    if (result.success === false) {
        return NextResponse.json(
            {
                error: "Bad Request",
                details: result.error.errors.map((e) => e.message).join(", "),
            },
            { status: 400 },
        );
    }

    const { title, comment } = result.data;

    // Create book with user as author
    const data: BookDataInsert = {
        id: -1,
        title,
        comment: comment,
        owner_user_id: session.user.id,
        last_updated: new Date(),
    };
    const insertedBook = await service.book.insert(data);
    return NextResponse.json(insertedBook, { status: 200 });
}

export interface BooksGetRet {
    books: BookDataRet[];
    pages?: PageDataRet[][];
}

async function getBooks(
    req: NextRequest,
    { session }: DeepPartialExcept<RequestProps, "session.user">,
) {
    const books = await service.book.getByUserId(session.user.id);
    const ret: BooksGetRet = { books };

    // Check if pages is included
    const includes = req.nextUrl.searchParams.get("include");
    if (includes && includes.includes("pages")) {
        const page_promises = books.map((b) => service.page.getByBookId(b.id));
        ret["pages"] = await Promise.all(page_promises);
    }

    return NextResponse.json<BooksGetRet>(ret, { status: 200 });
}
