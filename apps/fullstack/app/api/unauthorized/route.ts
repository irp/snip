import { NextResponse } from "next/server";

export const GET = Unauthorized;
export const POST = Unauthorized;

async function Unauthorized() {
    return NextResponse.json(
        {
            error: "Unauthorized",
            details:
                "Please login or use an api token to access this resource.",
        },
        {
            status: 401,
        },
    );
}
