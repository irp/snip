import type { Metadata, Viewport } from "next";

import Footer from "components/common/Footer";
import { BrowserVersionAlert } from "components/common/utils";

import "components/styles/bootstrap.scss";
import "components/styles/global.scss";

export default function RootLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <html lang="en" style={{ height: "100vh" }}>
            <BrowserVersionAlert />
            <body
                className="d-flex flex-column"
                style={{
                    overflow: "hidden",
                    scrollBehavior: "unset",
                }}
            >
                {children}
                <Footer />
            </body>
        </html>
    );
}

export const viewport: Viewport = {
    width: "device-width",
    height: "device-height",
    initialScale: 1,
    minimumScale: 1,
    maximumScale: 1,
    userScalable: false,
    viewportFit: "cover",
};

export const metadata: Metadata = {
    icons: {
        icon: [
            {
                url: "/img/favicon/favicon-32x32-light.png",
                sizes: "32x32",
                media: "(prefers-color-scheme: light)",
                type: "image/png",
            },
            {
                url: "/img/favicon/favicon-32x32-dark.png",
                sizes: "32x32",
                media: "(prefers-color-scheme: dark)",
                type: "image/png",
            },
            {
                url: "/img/favicon/favicon-64x64-light.png",
                sizes: "64x64",
                media: "(prefers-color-scheme: light)",
                type: "image/png",
            },
            {
                url: "/img/favicon/favicon-64x64-dark.png",
                sizes: "64x64",
                media: "(prefers-color-scheme: dark)",
                type: "image/png",
            },
            {
                url: "/img/favicon/favicon-128x128-light.png",
                sizes: "128x128",
                media: "(prefers-color-scheme: light)",
                type: "image/png",
            },
            {
                url: "/img/favicon/favicon-128x128-dark.png",
                sizes: "128x128",
                media: "(prefers-color-scheme: dark)",
                type: "image/png",
            },
        ],
        shortcut: "/favicon.ico",
        apple: "/img/favicon/apple-touch-icon.png",
    },
    appleWebApp: {
        capable: true,
        title: "SNIP",
        statusBarStyle: "black-translucent",
    },
};

export const dynamic = "force-dynamic";
