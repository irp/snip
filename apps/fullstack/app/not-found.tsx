import Link from "next/link";

import { NotFoundErrorMsg } from "components/common/utils";

import "./(unprotected)/layout.scss";
import styles from "components/frontpage/frontpage.module.scss";

export default function NotFound() {
    return (
        <div
            className={
                styles.main +
                " d-flex flex-column justify-content-center align-items-center"
            }
        >
            <div>
                <div
                    className="text-primary p-5"
                    style={{
                        backgroundColor: "#adb5bd90",
                        borderRadius: "10px",
                        fontWeight: "bold",
                    }}
                >
                    <h1 className="text-center">404 - Page not found</h1>
                    <hr />
                    <NotFoundErrorMsg />
                    <p
                        className="text-center"
                        style={{ fontSize: "1.1rem", margin: 0 }}
                    >
                        <Link href="/">Go back to the homepage</Link>
                    </p>
                </div>
            </div>
        </div>
    );
}
