import Loading from "app/(unprotected)/loading";

export default function LoadingPage() {
    return (
        <div className="d-flex h-100 w-100 justify-content-center align-items-center">
            <div>
                <Loading />
            </div>
        </div>
    );
}
