"use client";

import { useRouter } from "next/navigation";
import { useEffect } from "react";

import { usePagesContext } from "components/context/socket/usePagesContext";

/** For historic reasons we also need to handle hash as params
 * in the editor layout.
 *
 * This page does nothing but handling the transition
 * and otherwise returns null (the layout only).
 *
 * #page=num
 * #page_id=num
 */
export default function Page({ params }: { params: { id: string } }) {
    const router = useRouter();
    const { pagesArray, pages } = usePagesContext();

    /**
     * This only works on the initial load
     * and does not update the page on hash change
     * after the initial load.
     */
    useEffect(() => {
        const hash = window.location.hash;

        let page_number: null | number = null;
        //Get all matches url#page=[any number positive or negative]
        if (hash && hash.match(/page/g)) {
            const sel_pages = hash
                .match(/page=-?\d+/g)
                ?.map((p) => parseInt(p.split("=")[1]!))
                .sort((a, b) => a - b)
                .map((p) => {
                    if (p < 0) return pagesArray.at(p);
                    return pagesArray.at(p - 1);
                })
                .filter((p) => p !== undefined);

            if (sel_pages && sel_pages.length > 0) {
                page_number = sel_pages[0]!.page_number! + 1;
            }
        }

        // Get all page_ids in hash
        if (hash && hash.match(/page_id/g)) {
            const page_ids = hash
                .match(/page_id=\d+/g)
                ?.map((p) => parseInt(p.split("=")[1]!))
                .filter((p) => p !== undefined);

            if (page_ids) {
                const page = pages.get(page_ids[0]!);
                if (page) {
                    page_number = page.page_number! + 1;
                }
            }
        }

        if (page_number) {
            router.replace(`/books/${params.id}/pages/${page_number}`);
        } else {
            router.replace(`/books/${params.id}`);
        }
    }, [pagesArray, router, params.id, pages]);

    return null;
}
