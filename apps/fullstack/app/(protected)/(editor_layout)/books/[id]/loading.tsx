import Loading from "components/utils/Loading";

/** This loading applies to the pages layout
 * and is used to show a loading spinner.
 */
export default function LoadingPage() {
    return (
        <div
            style={{
                height: "100%",
                width: "calc(100% - 350px)",
            }}
        >
            <div className="d-flex justify-content-center align-items-center w-100 h-100">
                <Loading />
            </div>
        </div>
    );
}
