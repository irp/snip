import { getSessionAction } from "lib/session";
import { unstable_cache } from "next/cache";
import { notFound, redirect } from "next/navigation";
import { Metadata } from "next/types";

import { getPerms } from "@snip/auth/book_permissions";
import { SnipSessionData } from "@snip/auth/types";
import service from "@snip/database";

import {
    EditorContextProvider,
    EditorSidebar,
    EditorWrapper,
    permsToEnabledTools,
} from "components/editor";
import EditorResizer from "components/editor/resizer";

import "./layout.scss";

/** The layout for the editor.
 *
 * Attaches a context provider for this book socket and checks if a user is
 * allowed to view this book.
 */
export default async function EditorLayout({
    params,
    children,
}: {
    params: { id: string; page_no?: string };
    children: React.ReactNode;
}) {
    /* ----------------------------- Authentication ----------------------------- */

    // Check if book id is valid in general
    const _book_id = params.id;
    if (!_book_id || isNaN(parseInt(_book_id))) {
        return notFound();
    }
    const book_id = parseInt(_book_id);

    // Check if user session is valid
    const session = await getSessionAction();
    const { user, anonymous } = session;
    if (!user && (!anonymous || anonymous.tokens.length === 0)) {
        return notFound();
    }

    // Check if user has permissions to view this book
    const perms = await getBookPermsFromSession(book_id, session);
    if (!perms.resolved.pRead) {
        // Give meaingful error messages
        for (const p of perms.all) {
            if (
                p.auth_method === "ui_token" &&
                p.expires_at &&
                p.expires_at < new Date()
            ) {
                return redirect("/expired");
            }
        }
        return redirect("/unauthorized");
    }

    /* -------------------------------- Book Data ------------------------------- */

    // Get initial book data, pages and snips
    // TODO: in theory we could get away with just the snipids
    // and fetch the snips on demand
    const [bookData, pages, queuedSnips] = await Promise.all([
        fetchBook(book_id),
        service.page.getByBookId(book_id),
        service.snip.getQueuedByBookId(book_id),
    ]);

    const tools = permsToEnabledTools(perms.resolved);

    console.log("EditorLayout", params);

    return (
        <div className="h-100 overflow-hidden">
            <EditorContextProvider
                bookData={bookData}
                pages={pages}
                queuedSnips={queuedSnips}
                tools={tools}
            >
                <EditorWrapper>
                    <EditorSidebar init_with_width={true} />
                    {children}
                </EditorWrapper>
            </EditorContextProvider>
        </div>
    );
}

async function getBookPermsFromSession(
    book_id: number,
    session: SnipSessionData,
) {
    const { user, anonymous } = session;

    // Get valid token with this bookid
    let potentialValid = anonymous?.tokens.filter(
        (token) => token.book_id === book_id,
    );
    if (potentialValid && potentialValid.length === 0) {
        potentialValid = undefined;
    }

    const perms = await getPerms(book_id, {
        user_id: user?.id,
        ui_token_id: anonymous?.tokens?.map((t) => t.id),
    });

    return perms;
}

const fetchBook = unstable_cache(async (book_id: number) => {
    const book = await service.book.getById(book_id);
    return book;
});

export async function generateMetadata({
    params,
}: {
    params: {
        id: string;
    };
}): Promise<Metadata> {
    // read route params
    const id = params.id;

    //Get book data
    const book = await fetchBook(parseInt(id)).catch(() => {
        return {
            title: "Book not found",
        };
    });

    return {
        title: book.title,
    };
}
