import { EditorToolbar, EditorViewer } from "components/editor";
import EditorResizer from "components/editor/resizer";

/** Only show the viewer if a page was requested else show all pages
 * in the book.
 */
export default function PageLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <>
            <EditorResizer />
            <div className="position-relative">
                <EditorToolbar />
                <EditorViewer />
                {children}
            </div>
        </>
    );
}
