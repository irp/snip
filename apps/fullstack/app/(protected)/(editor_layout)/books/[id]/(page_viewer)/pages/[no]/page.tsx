"use client";

import { useRouter } from "next/navigation";
import { useEffect } from "react";

import type { PageDataRet } from "@snip/database/types";

import { usePagesContext } from "components/context/socket/usePagesContext";
import { useEditorContext } from "components/editor/context";

/** This page does nothing but handling the transition
 * between book pages.
 *
 * no = page_number
 */
export default function Page({
    params,
}: {
    params: { id: string; no: string };
}) {
    const router = useRouter();
    const { setActivePageIds } = useEditorContext();
    const { pagesArray } = usePagesContext();

    useEffect(() => {
        if (
            params.id === undefined ||
            params.no === undefined ||
            isNaN(parseInt(params.no))
        ) {
            return;
        }

        const page_no = parseInt(params.no);
        let page: undefined | PageDataRet;
        if (page_no < 1) {
            page = pagesArray.at(page_no);
        } else {
            page = pagesArray.at(page_no - 1);
        }
        if (page) {
            router.replace(
                `/books/${params.id}/pages/${page.page_number! + 1}`,
            );
            setActivePageIds([page.id]);
        }
    }, [router, pagesArray, params.id, params.no, setActivePageIds]);

    return null;
}
