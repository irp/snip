"use client";

import { useRouter } from "next/navigation";
import { useEffect } from "react";

import type { PageDataRet } from "@snip/database/types";

import { usePagesContext } from "components/context/socket/usePagesContext";
import { useEditorContext } from "components/editor/context";

/** This page does nothing but handling the transition
 * between book pages.
 *
 * no = page_number
 */
export default function Page({
    params,
}: {
    params: { id: string; p_id: string };
}) {
    const router = useRouter();
    const { setActivePageIds } = useEditorContext();
    const { pages } = usePagesContext();

    useEffect(() => {
        if (
            params.id === undefined ||
            params.p_id === undefined ||
            isNaN(parseInt(params.p_id))
        ) {
            return;
        }

        const page_id = parseInt(params.p_id);
        const page = pages.get(page_id);
        if (page) {
            router.replace(
                `/books/${params.id}/pages/${page.page_number! + 1}`,
            );
            setActivePageIds([page.id]);
        }
    }, [router, pages, params.id, params.p_id, setActivePageIds]);

    return null;
}
