import Link from "next/link";
import { Metadata } from "next/types";

import { AdvancedUploadProvider } from "./context";
import { Editor } from "./editor";
import { ErrorFeedback } from "./errors";
import { UploadPreview } from "./preview";

export default async function Page() {
    return (
        <>
            <Header />
            <div className="container col-12">
                <AdvancedUploadProvider>
                    <div className="row w-100 row-gap-2">
                        <ErrorFeedback />
                        <Editor className="col-xxl-6 col-lg-12" />
                        <UploadPreview className="col-xxl-6 col-lg-12" />
                    </div>
                </AdvancedUploadProvider>
            </div>
        </>
    );
}

function Header() {
    return (
        <div className="col-12 col-lg-10 col-xxl-7">
            <h1>Advanced Upload</h1>
            <p>
                The advanced upload page allows you to upload snippets using
                json. You can find the supported schema for all snip types{" "}
                <Link href="/schemas">here</Link> and additionally you may find
                the documentation for our <Link href="/docs/user/api">api</Link>{" "}
                useful which basically allows you to automate the advanced
                upload process.
            </p>
        </div>
    );
}

export const metadata: Metadata = {
    title: "Advanced",
};
