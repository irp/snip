"use client";
import {
    createContext,
    Dispatch,
    ReactNode,
    SetStateAction,
    useContext,
    useState,
} from "react";

import { get_snip_from_data } from "@snip/snips";
import { BaseSnip } from "@snip/snips/general/base";

interface AdvancedUploadContextI {
    jsonText: string;
    setJsonText: Dispatch<SetStateAction<string>>;
    error: Error | undefined;
    setError: Dispatch<SetStateAction<Error | undefined>>;
    resetError: () => void;
    parseSnip: () => [BaseSnip, undefined] | [undefined, Error];
    snip: BaseSnip | undefined;
}

const AdvancedUploadContext = createContext<AdvancedUploadContextI | undefined>(
    undefined,
);

export function useAdvancedUploadContext() {
    const context = useContext(AdvancedUploadContext);
    if (!context) {
        throw new Error(
            "useAdvancedUploadContext must be used within a AdvancedUploadProvider",
        );
    }
    return context;
}

export function AdvancedUploadProvider({ children }: { children: ReactNode }) {
    const [jsonText, setJsonText] = useState("");
    const [snip, setSnip] = useState<BaseSnip | undefined>(undefined);
    const [error, setError] = useState<Error | undefined>(undefined);

    function resetError() {
        setError(undefined);
    }

    function parseSnip(): [BaseSnip, undefined] | [undefined, Error] {
        try {
            const parsed = JSON.parse(jsonText);
            const snip = get_snip_from_data(parsed);
            setSnip(snip);
            setError(undefined);
            return [snip, undefined];
        } catch (error) {
            if (error instanceof Error) {
                setError(error);
                setSnip(undefined);
            }
            return [undefined, error as Error];
        }
    }

    return (
        <AdvancedUploadContext.Provider
            value={{
                jsonText,
                setJsonText,
                error,
                resetError,
                setError,
                parseSnip,
                snip,
            }}
        >
            {children}
        </AdvancedUploadContext.Provider>
    );
}
