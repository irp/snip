"use client";
import { HtmlHTMLAttributes } from "react";
import { Button } from "react-bootstrap";
import { TbX } from "react-icons/tb";

import {
    DataValidationError,
    DataValidationErrorArray,
} from "@snip/snips/errors";

import { useAdvancedUploadContext } from "./context";

import styles from "./errors.module.scss";

export function ErrorFeedback(props: HtmlHTMLAttributes<HTMLDivElement>) {
    const { error, resetError } = useAdvancedUploadContext();

    if (!error) {
        return null;
    }

    let title;
    let message;

    if (
        error instanceof DataValidationError ||
        error instanceof DataValidationErrorArray
    ) {
        return (
            <DataValidationErrorComponent
                error={error}
                resetError={resetError}
            />
        );
    }

    if (error instanceof SyntaxError) {
        title = "Failed to parse json!";
        message = error.message.replace("JSON.parse: ", "");
    } else {
        title = "An error occurred!";
        message = error.message;
    }

    return (
        <div className={styles.wrapper} {...props}>
            <Button
                variant="none"
                className={styles.close}
                onClick={resetError}
            >
                <TbX />
            </Button>
            <h4>{title}</h4>
            <p>{message}</p>
        </div>
    );
}

function DataValidationErrorComponent({
    error,
    resetError,
}: {
    error: DataValidationError | DataValidationErrorArray;
    resetError: () => void;
}) {
    return (
        <div className={styles.wrapper}>
            <Button
                variant="none"
                className={styles.close}
                onClick={resetError}
            >
                <TbX />
            </Button>
            <div className={styles.title}>Data validation error</div>
            {error instanceof DataValidationErrorArray
                ? error.errors.map((e, i) => (
                      <div className={styles.message} key={i}>
                          {e.message}
                      </div>
                  ))
                : null}
            {error instanceof DataValidationError ? (
                <div className={styles.message}>{error.message}</div>
            ) : null}
        </div>
    );
}

export class UploadError extends Error {
    type: string;

    constructor(msg: string, type: string) {
        super(msg);
        this.type = type;
    }

    static from_json_error(error: { error: string; details: string }) {
        return new UploadError(error.details, error.error);
    }
}
