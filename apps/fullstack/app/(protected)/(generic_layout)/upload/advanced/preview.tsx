"use client";

import { HtmlHTMLAttributes, useEffect } from "react";

import type { RenderContext } from "@snip/render/types";

import { Preview } from "../../account/tools/doodleConfig";
import { useAdvancedUploadContext } from "./context";

import {
    OverlayWorkerContextProvider,
    useOverlayWorkerContext,
} from "components/context/viewer/useWorkerContext";

import styles from "./preview.module.scss";

export function UploadPreview(props: HtmlHTMLAttributes<HTMLDivElement>) {
    return (
        <div {...props} className={styles.wrapper + " " + props.className}>
            <OverlayWorkerContextProvider n={1}>
                <SnipPreview />
            </OverlayWorkerContextProvider>
        </div>
    );
}

function SnipPreview() {
    const workers = useOverlayWorkerContext();
    const { snip } = useAdvancedUploadContext();

    useEffect(() => {
        const worker = workers.get(0);
        if (!worker) return;

        function renderSnip(ctx: RenderContext) {
            if (snip) {
                console.log("Rendering snip", snip);
                snip.render(ctx);
            }
        }

        if (snip) {
            worker.add(renderSnip);
        }

        return () => {
            worker.remove(renderSnip);
        };
    }, [snip, workers]);

    return (
        <div>
            {workers.get(0) && (
                <Preview
                    worker={workers.get(0)!}
                    height="100%"
                    fit_height
                    padding={40}
                />
            )}
        </div>
    );
}
