"use client";
import hljs from "highlight.js/lib/core";
import jsonLang from "highlight.js/lib/languages/json";
import { HtmlHTMLAttributes, useEffect, useRef, useState } from "react";
import { Button, InputGroup, Spinner } from "react-bootstrap";

import { useAdvancedUploadContext } from "./context";
import { UploadError } from "./errors";

import { HoverInfo } from "components/common/utils";

import "highlight.js/styles/stackoverflow-light.css";
import styles from "./editor.module.scss";

// Then register the languages you need
hljs.registerLanguage("json", jsonLang);

export function Editor(props: HtmlHTMLAttributes<HTMLDivElement>) {
    const codeRef = useRef<HTMLElement>(null);

    const { jsonText, setJsonText } = useAdvancedUploadContext();

    /**
     * Handles the Tab key press event in the textarea.
     *
     * When the Tab key is pressed, this function prevents the default behavior (which is to move focus to the next element)
     * and instead inserts a tab character at the cursor position. This allows the user to indent text in the textarea using
     * the Tab key, similar to how it works in code editors.
     *
     * @param event - The keyboard event triggered by pressing a key in the textarea.
     */
    function checkTab(event: React.KeyboardEvent<HTMLTextAreaElement>) {
        if (event.key == "Tab") {
            event.preventDefault();
            const element = event.currentTarget;
            const beforeTab = element.value.slice(0, element.selectionStart);
            const afterTab = element.value.slice(
                element.selectionEnd,
                element.value.length,
            );
            const cursorPos = element.selectionStart + 1;
            element.value = beforeTab + "\t" + afterTab;
            element.selectionStart = cursorPos;
            element.selectionEnd = cursorPos;
            setJsonText(element.value);
        }
    }

    /**
     * Synchronizes the scroll position between the textarea and the code block.
     *
     * This function ensures that when the user scrolls the textarea, the code block
     * (which is highlighted and displayed next to the textarea) scrolls in sync. This
     * provides a consistent viewing experience for the user, allowing them to see the
     * highlighted code as they scroll through the textarea.
     *
     * @param e - The scroll event triggered by the user scrolling the textarea.
     */
    function syncScroll(e: React.UIEvent<HTMLTextAreaElement>) {
        if (codeRef.current) {
            const target = e.target as HTMLTextAreaElement;
            codeRef.current.scrollTop = target.scrollTop;
            codeRef.current.scrollLeft = target.scrollLeft;
        }
    }

    useEffect(() => {
        if (codeRef.current) {
            // rehighlight the code block when the json text changes
            delete codeRef.current.dataset.highlighted;
            hljs.highlightElement(codeRef.current);
        }
    }, [jsonText]);

    return (
        <div {...props} className={styles.wrapper + " " + props.className}>
            <div className={styles.textarea}>
                <textarea
                    wrap="false"
                    onInput={(e) => setJsonText(e.currentTarget.value)}
                    onKeyDown={checkTab}
                    autoCorrect="off"
                    spellCheck="false"
                    value={jsonText}
                    onScroll={syncScroll}
                />
                <pre>
                    <code
                        className="language-json"
                        aria-hidden="true"
                        ref={codeRef}
                    >
                        {jsonText}
                    </code>
                </pre>
            </div>
            <ToolBar />
        </div>
    );
}

function ToolBar() {
    const { jsonText, setJsonText, setError, parseSnip } =
        useAdvancedUploadContext();

    function formatJson() {
        try {
            const parsed = JSON.parse(jsonText);
            const str = JSON.stringify(parsed, null, 4);
            setJsonText(str);
        } catch (e) {
            if (e instanceof Error) {
                setError(e);
            }
        }
    }

    return (
        <div className={styles.toolbar}>
            <div className={styles.examples}>
                <InputGroup>
                    <InputGroup.Text>Examples</InputGroup.Text>

                    <HoverInfo tooltip="Example of a text snip">
                        <Button
                            variant="outline-secondary"
                            onClick={() => {
                                setJsonText(
                                    JSON.stringify(
                                        {
                                            type: "text",
                                            book_id: 123,
                                            data: {
                                                text: "Hello, World!",
                                            },
                                            view: {
                                                __comment:
                                                    "View is optional and may be changed!",
                                                size: 20,
                                                x: 100,
                                                y: 200,
                                                rot: 42,
                                            },
                                        },
                                        null,
                                        4,
                                    ),
                                );
                            }}
                        >
                            Text
                        </Button>
                    </HoverInfo>
                    <Button
                        variant="outline-secondary"
                        onClick={() => {
                            setJsonText(
                                JSON.stringify(
                                    {
                                        type: "link",
                                        book_id: 123,
                                        data: {
                                            href: "https://example.com/",
                                            snip: {
                                                type: "text",
                                                book_id: 123,
                                                data: {
                                                    text: "Click me!",
                                                },
                                                view: {
                                                    size: 20,
                                                    x: 0,
                                                    y: 0,
                                                },
                                            },
                                        },
                                        view: {
                                            show_icon: true,
                                            icon_pos_x: "right",
                                            icon_pos_y: "top",
                                        },
                                    },
                                    null,
                                    4,
                                ),
                            );
                        }}
                    >
                        Link
                    </Button>
                </InputGroup>
            </div>
            <HoverInfo tooltip="Indent and align the json data">
                <Button variant="outline-secondary" onClick={formatJson}>
                    Format
                </Button>
            </HoverInfo>
            <HoverInfo tooltip="Parse json as snip">
                <Button variant="outline-secondary" onClick={parseSnip}>
                    Test
                </Button>
            </HoverInfo>
            <HoverInfo tooltip="Validate & upload the json data as snippet">
                <UploadButton />
            </HoverInfo>
        </div>
    );
}

function UploadButton() {
    const [pending, setPending] = useState(false);

    const { setError, parseSnip } = useAdvancedUploadContext();

    async function uploadSnip() {
        // Upload the snip using the default endpoint
        // Also properly handle the response/error
        setPending(true);
        const [snip, error] = parseSnip();
        if (error) {
            setPending(false);
            return;
        }

        // Upload the snip
        const response = await fetch(`/api/books/${snip.book_id}/upload`, {
            method: "POST",
            body: JSON.stringify(snip.to_data()),
            headers: {
                "Content-Type": "application/json",
            },
        });

        if (!response.ok) {
            const api_error = await response.json();
            setError(UploadError.from_json_error(api_error));
        } else {
            //todo clear json show success
        }

        setPending(false);
    }

    return (
        <Button
            variant="outline-primary"
            onClick={uploadSnip}
            disabled={pending}
            style={{
                position: "relative",
            }}
        >
            <span style={{ visibility: pending ? "hidden" : "visible" }}>
                Upload
            </span>
            <div
                style={{
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    top: 0,
                    left: 0,
                    display: pending ? "flex" : "none",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Spinner size="sm" />
            </div>
        </Button>
    );
}
