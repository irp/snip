import { getSessionAction } from "lib/session";
import type { Metadata } from "next";
import { redirect } from "next/navigation";

import service from "@snip/database";

import { UploadWrapper } from "./UploadForm";

export default async function Upload() {
    // Get all books
    const availableBooks = await getAllBooksOfCurrentUser();

    return (
        <>
            <UploadWrapper availableBooks={availableBooks} />
        </>
    );
}

async function getAllBooksOfCurrentUser() {
    const session = await getSessionAction();

    if (!session?.user?.id) {
        redirect("/login");
    }

    const availableBooks = await service.book.getByUserId(
        session.user.id,
        true,
    );

    return availableBooks;
}

export const metadata: Metadata = {
    title: "Images",
};
