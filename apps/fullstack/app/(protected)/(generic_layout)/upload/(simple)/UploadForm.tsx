"use client";

import { Dispatch, FormEvent, SetStateAction, useState } from "react";
import { FileRejection, useDropzone } from "react-dropzone";
import { BsCamera, BsImage, BsX, BsXCircleFill } from "react-icons/bs";

import { BookDataRet } from "@snip/database/types";

import { BookSelector } from "components/books/booksSelector";

import styles from "./upload.module.scss";

export function UploadWrapper({
    availableBooks,
}: {
    availableBooks: BookDataRet[];
}) {
    availableBooks = availableBooks.sort((a, b) => {
        return (
            new Date(b.last_updated!).getTime() -
            new Date(a.last_updated!).getTime()
        );
    });

    // Filter archived books
    availableBooks = availableBooks.filter((book) => !book.finished);

    return (
        <div className="row justify-content-center">
            <div className="col-12 col-lg-10 col-xxl-7">
                <h1 style={{ paddingBottom: "1rem" }}>
                    Upload a file to a labbook
                </h1>
                <p>
                    You can choose a labbook and easily upload a file to it. The
                    uploaded file will automatically become a part of the
                    selected labbook, eliminating the need to access it
                    separately. This feature simplifies the file uploading
                    process and saves time.
                </p>
            </div>
            <UploadForm books={availableBooks} />
        </div>
    );
}

function UploadForm({ books }: { books: BookDataRet[] }) {
    // Keep track of uploads and errors for each file
    const [progress, setProgress] = useState<number[]>();
    const [error, setError] = useState<string[]>();
    const [success, setSuccess] = useState<boolean>(false);

    // Keep track of files to upload and the book to upload to
    const [files, setFiles] = useState<File[]>([]);
    const [bookIdx, setBookIdx] = useState<number | null>(0);

    /** Helper function to quickly reset the complete
     * form.
     */
    const resetForm = () => {
        setProgress(undefined);
        setError(undefined);
        setSuccess(false);
        setFiles([]);
    };

    const updateProgress = (idx: number, new_val: number) => {
        if (!progress) {
            setProgress(new Array(files.length).fill(0));
        }
        setProgress((prev) => {
            prev![idx] = new_val;
            return [...prev!];
        });
    };

    const updateError = (idx: number, new_val: string) => {
        if (!error) {
            setError(new Array(files.length).fill(null));
        }
        setError((prev) => {
            prev![idx] = new_val;
            return [...prev!];
        });
    };

    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        e.stopPropagation();

        // Upload each file and update progress
        const success = Array(files.length).fill(false);

        files.forEach((file, idx) => {
            const onProgress = (new_val: number) =>
                updateProgress(idx, new_val);
            const onSuccess = () => {
                success[idx] = true;
                if (success.every((val) => val)) {
                    setSuccess(true);
                }
            };
            const onError = (err: string) => updateError(idx, err);

            const book = bookIdx !== null ? books[bookIdx] : null;

            if (!book) {
                updateError(idx, "Please select a book first.");
                return;
            }

            uploadFile(file, book.id, onProgress, onSuccess, onError);
        });
    };

    // If upload was successful, show success message
    if (success) {
        return (
            <div className="col-xl-6 col-md-8 col-sm-12">
                <div className="alert alert-success" role="alert">
                    Upload was successful! Check your labbook{" "}
                    <a
                        href={
                            bookIdx !== null
                                ? `/books/${books[bookIdx]?.id}`
                                : "/not-found"
                        }
                        className="alert-link"
                    >
                        here
                    </a>
                    .
                </div>
                <div className="d-flex justify-content-center gap-2 mt-2">
                    <button
                        type="button"
                        className="btn btn-secondary"
                        onClick={resetForm}
                    >
                        Upload another file?
                    </button>
                </div>
            </div>
        );
    }

    // If upload failed, show error message
    if (error) {
        return (
            <div className="col-xl-6 col-md-8 col-sm-12">
                <div className="alert alert-danger" role="alert">
                    {error}
                </div>
                <button
                    type="button"
                    className="btn btn-primary"
                    onClick={resetForm}
                >
                    Try again?
                </button>
            </div>
        );
    }

    return (
        <>
            <div className="col-xl-6 col-md-8 col-sm-12">
                <form onSubmit={onSubmit}>
                    <BookSelector
                        books={books}
                        currentIdx={bookIdx}
                        setCurrentIdx={setBookIdx}
                    />

                    <Dropzone setFiles={setFiles} />
                    <div className={styles.dropzoneInfo}>
                        {/** Files list with remove cross */}
                        {files.length > 0 ? <label>Files:</label> : null}
                        <ul>
                            {files.map((file, idx) => (
                                <li key={idx}>
                                    <span>{file.name}</span>
                                    <button className="btn p-0 m-0">
                                        <BsXCircleFill
                                            style={{
                                                fontSize: "1.5rem",
                                                marginLeft: "0.5rem",
                                            }}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                e.stopPropagation();
                                                setFiles((prev) =>
                                                    prev.filter(
                                                        (_, i) => i !== idx,
                                                    ),
                                                );
                                            }}
                                        ></BsXCircleFill>
                                    </button>
                                    <div
                                        style={{
                                            position: "absolute",
                                            height: "100%",
                                            left: "0",
                                            width: progress
                                                ? `${progress[idx]}%`
                                                : 0,
                                        }}
                                    />
                                </li>
                            ))}
                        </ul>
                    </div>
                    <div className="d-flex justify-content-center gap-2 mt-2">
                        <button type="submit" className="btn btn-primary">
                            Upload to book
                        </button>
                    </div>
                </form>
            </div>
        </>
    );
}

function Dropzone({
    setFiles,
}: {
    setFiles: Dispatch<SetStateAction<File[]>>;
}) {
    // File rejection error messages
    const [error, setError] = useState<string[]>([]);

    const addError = (message: string) => {
        setError((prev) => [...prev, message]);
    };

    /** Validate file type and show error message
     * if file was rejected.
     */
    const onDrop = (acceptedFiles: File[], fileRejections: FileRejection[]) => {
        // For each rejected file, show error message
        if (fileRejections.length > 0) {
            for (const fileRejection of fileRejections) {
                addError(fileRejection.errors[0]!.message);
            }
        }
        // Add accepted files to the list
        setFiles((prev) => [...prev, ...acceptedFiles]);
    };

    // Configure dropzone
    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
        accept: {
            "image/*": [".jpeg", ".png"],
        },
        multiple: true,
    });

    return (
        <>
            <div {...getRootProps()} className={styles.dropzone}>
                <input {...getInputProps()} />
                <div className="p-2">
                    {isDragActive ? (
                        <div>Drop here! </div>
                    ) : (
                        <div>
                            Drag &apos;n&apos; drop files here, or click to
                            select files
                        </div>
                    )}
                </div>
                <div className="d-flex justify-content-center gap-2">
                    <BsImage
                        style={{ fontSize: "2.5rem", marginTop: "-0.5rem" }}
                    />
                    <BsCamera
                        style={{ fontSize: "2.5rem", marginTop: "-0.5rem" }}
                    />
                </div>
            </div>
            {/** Error messages */}
            {error.map((message, idx) => (
                <div key={idx} className="alert alert-danger" role="alert">
                    {message}
                    <button
                        className={styles.closeAlert}
                        onClick={(e) => {
                            e.preventDefault();
                            e.stopPropagation();
                            setError((prev) =>
                                prev.filter((_, i) => i !== idx),
                            );
                        }}
                    >
                        <BsX />
                    </button>
                </div>
            ))}
        </>
    );
}

function uploadFile(
    file: File,
    book_id: number,
    onLoad?: (snip_id: number) => void,
    onProgress?: (progress: number) => void,
    onError?: (error: string) => void,
) {
    // Upload file using fetch and get progress via streams
    const req = new XMLHttpRequest();
    req.open("POST", `/api/books/${book_id}/upload`);
    req.addEventListener("load", () => {
        const snip_id = JSON.parse(req.response)["id"];
        onLoad && onLoad(snip_id);
    });
    req.upload.addEventListener("progress", (e: ProgressEvent) => {
        onProgress && onProgress((e.loaded / e.total) * 100);
    });
    req.addEventListener("error", () => {
        onError && onError(req.response);
    });

    // create form data and append files
    const form = new FormData();
    form.append("file" + file, file, file.name);

    // send post request see api/book/id/upload
    req.send(form);
    return req;
}
