import type { Metadata } from "next";

import BreadCrumbNavBar from "components/common/other/BreadcrumbNavBar";

export default function ProfileLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <>
            <BreadCrumbNavBar>
                {[
                    {
                        label: "Upload",
                        href: "/upload",
                        ariaLabel: "Go to upload",
                        match: "(simple)",
                    },
                    {
                        label: "Advanced upload",
                        href: "/upload/advanced",
                        ariaLabel: "Go to advanced upload",
                        match: "advanced",
                    },
                ]}
            </BreadCrumbNavBar>
            <div className="container-fluid flex-column justify-content-center align-items-center">
                {children}
            </div>
        </>
    );
}

export const metadata: Metadata = {
    title: {
        template: "Upload - %s",
        default: "Upload",
    },
};
