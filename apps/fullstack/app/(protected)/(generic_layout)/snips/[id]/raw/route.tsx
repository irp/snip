import { wrapperLoggedInUser } from "lib/route_wrapper/wrapperLoggedIn";
import { RequestProps } from "lib/route_wrapper/wrapperSession";
import { NextRequest, NextResponse } from "next/server";

import { getPerms } from "@snip/auth/book_permissions";
import service from "@snip/database";

export const GET = wrapperLoggedInUser(getRawSnipData);

async function getRawSnipData(
    _req: NextRequest,
    { session, params }: DeepPartialExcept<RequestProps, "session.user">,
) {
    let snip_id;
    if (params?.id && !isNaN(parseInt(params.id))) {
        snip_id = parseInt(params.id);
    } else {
        return NextResponse.json(
            {
                error: "Invalid Snip ID",
                details: "The Snip ID provided is invalid",
            },
            { status: 400 },
        );
    }

    // Fetch snip from database
    let snip;
    try {
        snip = await service.snip.getById(snip_id);
    } catch (_e) {
        return NextResponse.json(
            {
                error: "Not Found",
            },
            { status: 404 },
        );
    }

    const perms = await getPerms(snip.book_id, {
        user_id: session?.user?.id,
        ui_token_id: undefined,
        bearer_token: undefined,
    });

    if (!perms.resolved.pRead) {
        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You do not have read access to this Snip!",
            },
            { status: 401 },
        );
    }

    return NextResponse.json(snip);
}
