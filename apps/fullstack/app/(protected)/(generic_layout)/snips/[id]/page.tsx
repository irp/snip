import { getSessionAction } from "lib/session";
import { redirect } from "next/navigation";

import { getPerms } from "@snip/auth/book_permissions";
import service from "@snip/database";

import { SnipBookView, SnipPageView } from "./components";

export default async function Page({
    params: { id },
}: {
    params: { id: string };
}) {
    if (!id || isNaN(parseInt(id))) {
        return redirect(`/not-found`);
    }

    // Fetch snip from database
    let snip;
    try {
        snip = await service.snip.getById(parseInt(id));
    } catch (_e) {
        return redirect(`/not-found`);
    }

    // Check if user is allowed to view the snip
    const session = await getSessionAction();

    const perms = await getPerms(snip.book_id, {
        user_id: session?.user?.id,
        ui_token_id: undefined,
        bearer_token: undefined,
    });

    if (!perms.resolved.pRead) {
        return redirect(`/unauthorized`);
    }

    if (!snip || !snip.page_id) {
        return (
            <div style={{ padding: "1rem", height: "100%", width: "100%" }}>
                <SnipBookView snip={snip} />
            </div>
        );
    }

    return (
        <div style={{ padding: "1rem", height: "100%", width: "100%" }}>
            <SnipPageView snip={snip} />
        </div>
    );
}
