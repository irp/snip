"use client";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useRef, useState } from "react";
import Button from "react-bootstrap/esm/Button";

import { SnipDataRet } from "@snip/database/types";
import { RenderContext } from "@snip/render/types";
import { ImageViewport } from "@snip/render/viewport";
import { get_snip_from_data } from "@snip/snips";
import { BaseSnip } from "@snip/snips/general/base";
import { LinkSnip } from "@snip/snips/general/link";

import { Preview } from "../../account/tools/doodleConfig";

import {
    OverlayWorkerContextProvider,
    useOverlayWorkerContext,
} from "components/context/viewer/useWorkerContext";
import Loading from "components/utils/Loading";

function ImagePagePreviewBySnip({
    snip,
}: {
    snip: BaseSnip;
    children?: React.ReactNode;
}) {
    // Show variable is used to delay pop in of the image
    const [show, setShow] = useState(false);
    const imageRef = useRef<HTMLImageElement>(null);
    const highlightedRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        /** Convert db coords to image coords */
        if (imageRef.current && highlightedRef.current) {
            const imageElement = imageRef.current;
            const viewport = new ImageViewport(imageElement, {
                width: 1400,
                height: 2000,
            });

            const handleResize = () => {
                viewport.onImageResize();
                const xy = viewport.getPosInPageCoords([snip.x, snip.y]);
                const wh = viewport.getRelativeInPageCoords([
                    snip.width,
                    snip.height,
                ]);
                highlightedRef.current!.style.top = `${xy[1]}px`;
                highlightedRef.current!.style.left = `${xy[0]}px`;
                highlightedRef.current!.style.width = `${wh[0]}px`;
                highlightedRef.current!.style.height = `${wh[1]}px`;

                // Adjust parent size
                const parent = highlightedRef.current!.parentElement!;
                parent.style.width = imageElement.width + "px";
                parent.style.height = imageElement.height + "px";
            };

            const handleLoad = () => {
                handleResize();
                setShow(true);
            };

            imageElement.addEventListener("load", handleLoad);

            // Resize observer
            const resizeObserver = new ResizeObserver(handleResize);
            resizeObserver.observe(imageElement);

            if (imageElement.complete) {
                handleLoad();
            }

            return () => {
                imageElement.removeEventListener("load", handleLoad);
                resizeObserver.disconnect();
            };
        }
    }, [snip.height, snip.width, snip.x, snip.y]);

    return (
        <>
            {!show && (
                <div
                    style={{
                        height: "100%",
                        display: "flex",
                        minWidth: "100%",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <Loading />
                </div>
            )}
            <div
                style={{
                    height: "100%",
                    position: "relative",
                    width: "100%",
                    overflow: "hidden",
                    outline: "1px solid var(--bs-primary)",
                    display: show ? "flex" : "none",
                }}
            >
                <div
                    ref={highlightedRef}
                    style={{
                        position: "absolute",
                        width: 0,
                        height: 0,
                        backgroundColor: "rgba(255, 0, 0, 0.1)",
                        border: "1px solid red",
                        visibility: show ? "visible" : "hidden",
                    }}
                >
                    {snip.type === "link" && (
                        <Link
                            href={(snip as LinkSnip).href}
                            style={{
                                position: "absolute",
                                width: "100%",
                                height: "100%",
                                zIndex: 100,
                            }}
                            target="_blank"
                        />
                    )}
                </div>
                <Image
                    src={snip.page_id!.toString()}
                    ref={imageRef}
                    alt="Page"
                    loader={({ src, width }) => {
                        // 200 hardcoded for now
                        return `/render/${src}.jpeg?w=${width}&date=${new Date().valueOf()}`;
                    }}
                    width={10}
                    height={720 * 1.41}
                    sizes="100vh"
                    style={{
                        width: "auto",
                        height: "100%",
                        visibility: show ? "visible" : "hidden",
                    }}
                    loading="eager"
                />
            </div>
        </>
    );
}

function ShowUtils({ snip }: { snip: SnipDataRet }) {
    let href = `/books/${snip.book_id}`;
    if (snip.page_id) {
        href += `/page_ids/${snip.page_id}`;
    }

    return (
        <div
            className="d-flex align-items-center text-center flex-column gap-2 "
            style={{
                maxWidth: "720px",
                marginLeft: "auto",
                marginRight: "auto",
            }}
        >
            <Button href={href} variant="primary" style={{ width: "90%" }}>
                Show in editor
            </Button>
            <Link href={`/snips/${snip.id}/raw`} className="text-muted">
                Show data of snippet
            </Link>
        </div>
    );
}

export function SnipPageView({ snip }: { snip: SnipDataRet }) {
    const snipF = get_snip_from_data(snip);

    return (
        <>
            <div
                style={{
                    height: "100%",
                    display: "flex",
                    width: "100%",
                    flexDirection: "column",
                    gap: "1rem",
                    flexGrow: 1,
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <ImagePagePreviewBySnip snip={snipF} />
                <ShowUtils snip={snip} />
            </div>
        </>
    );
}

export function SnipBookView({ snip }: { snip: SnipDataRet }) {
    const snipF = get_snip_from_data(snip);

    return (
        <OverlayWorkerContextProvider n={1}>
            <div
                style={{
                    height: "100%",
                    display: "flex",
                    width: "100%",
                    flexDirection: "column",
                    gap: "1rem",
                    flexGrow: 1,
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                Snippet is not yet placed on a page!
                <SnipPreview snip={snipF} />
                <ShowUtils snip={snip} />
            </div>
        </OverlayWorkerContextProvider>
    );
}

function SnipPreview({ snip }: { snip: BaseSnip }) {
    // Show variable is used to delay pop in of the image
    const [show, setShow] = useState(false);

    const workers = useOverlayWorkerContext();

    useEffect(() => {
        const worker = workers.get(0);
        if (!worker) return;

        function renderSnip(ctx: RenderContext) {
            if (snip) {
                snip.render(ctx);
                setShow(true);
            }
        }

        if (snip) {
            worker.add(renderSnip);
        }

        return () => {
            worker.remove(renderSnip);
        };
    }, [snip, workers]);

    return (
        <>
            {!show && (
                <div
                    style={{
                        height: "100%",
                        display: "flex",
                        minWidth: "100%",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <Loading />
                </div>
            )}
            <div
                style={{
                    height: "100%",
                    aspectRatio: "1 / 1.41",
                    backgroundColor: "white",
                    position: "relative",
                    display: show ? "flex" : "none",
                }}
            >
                <Preview
                    worker={workers.get(0)!}
                    height="100%"
                    fit_height
                    padding={40}
                />
                <div
                    style={{
                        position: "absolute",
                        top: 0,
                        left: 0,
                        fontSize: "4rem",
                        color: "rgba(0,0,0,0.05)",
                    }}
                >
                    PREVIEW
                </div>
            </div>
        </>
    );
}
