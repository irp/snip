import { ReactNode } from "react";

import { ContentWrapper, Sidebar } from "components/common/Sidebar";

import styles from "./layout.module.scss";

/** The layout in the protected page always shows a sidebar
 * and the current page in the center at the header
 */
export default function RootLayout({ children }: { children: ReactNode }) {
    return (
        <>
            <main className={styles.main}>
                <Sidebar />
                <ContentWrapper>{children}</ContentWrapper>
            </main>
        </>
    );
}
