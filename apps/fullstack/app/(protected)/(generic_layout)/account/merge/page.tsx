import { cookies } from "next/headers";
import { notFound } from "next/navigation";
import type { Metadata } from "next/types";
import { Alert, AlertHeading } from "react-bootstrap";
import { MdEmail } from "react-icons/md";

import { getSession } from "@snip/auth/session";
import service from "@snip/database";
import { UserDataRet } from "@snip/database/types";

import { type Credential, Merge } from "./mergeForm";

import styles from "./merge.module.scss";

export default async function MergeAccount() {
    const session = await getSession(cookies());

    if (!session || !session.user) {
        return notFound();
    }

    const user = await service.user.getById(session.user.id);

    const users_non_unique = await Promise.all(
        user.emails.map((email) => {
            return service.user.getByEmail(email);
        }),
    );

    const unique_users_ids = new Set(
        users_non_unique.map((u) => u.map((u) => u.id)),
    );

    // Early exist if no duplicates
    if (unique_users_ids.size !== 1) {
        return (
            <>
                <h2>Merge Accounts</h2>
                <NoAccounts />
            </>
        );
    }

    // Check which emails have duplicates
    const mergeForms = [];
    for (let i = 0; i < user.emails.length; i++) {
        if (users_non_unique[i]!.length > 1) {
            // Found a duplicate
            const email = user.emails[i]!;
            mergeForms.push(
                <MergeWithUsers
                    key={email}
                    email={email}
                    other_users={users_non_unique[i]!.filter(
                        (u) => u.id !== user.id,
                    )}
                />,
            );
        }
    }

    if (mergeForms.length === 0) {
        // Should not happen but just in case
        return (
            <>
                <h2>Merge Accounts</h2>
                <NoAccounts />
            </>
        );
    }

    return (
        <>
            <h2>Merge Accounts</h2>

            {mergeForms}
        </>
    );
}

function NoAccounts() {
    return (
        <Alert variant="success">
            <AlertHeading as="h4">
                No other accounts with the same emails detected!
            </AlertHeading>
            <div>
                We did not detect any other accounts associated with this your
                registered email addresses. If you still want to merge this
                account with another account, please contact us {""}
                <a href="mailto:gitlab+irp-snip-20373-issue-@gwdg.de">
                    <MdEmail />
                </a>
                !
            </div>
        </Alert>
    );
}

function MergeWithUsers({
    email,
    other_users,
}: {
    email: string;
    other_users: UserDataRet[];
}) {
    return (
        <div className={styles.wrapper}>
            <div className={styles.header}>
                We detected {other_users.length} other accounts with the email{" "}
                <code>{email}</code>!
            </div>
            {other_users.map((user) => {
                const c = _getCredentials(user);
                return <Merge key={user.id} email={email} other_user={c} />;
            })}
        </div>
    );
}

function _getCredentials(user: UserDataRet): Credential[] {
    const credentials: Credential[] = [];
    for (let i = 0; i < user.emails.length; i++) {
        credentials.push({
            type: user.credential_types[i]! as "password" | "sso",
            email: user.emails[i]!,
            provider: user.credential_provider_ids[i]!,
            user_id: user.id,
        });
    }
    return credentials;
}

export const metadata: Metadata = {
    title: "Merge Account",
};
