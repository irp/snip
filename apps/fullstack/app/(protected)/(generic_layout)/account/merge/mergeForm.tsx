"use client";
import { mergeAccountsAction } from "app/api/(actions)/mergeAccountsAction";
import { useRedirect } from "lib/hooks/useRedirect";
import { useFormState } from "react-dom";
import { TbPasswordUser } from "react-icons/tb";

import { Form } from "components/form";
import { FormError } from "components/form/alert";
import { SubmitButton } from "components/form/buttons";
import { EmailInput, PasswordInput } from "components/form/inputs/text";

export type Credential = {
    type: "password" | "sso";
    email: string;
    provider?: string;
    user_id: number;
};

export function Merge({
    other_user,
}: {
    email: string;
    other_user: Credential[];
}) {
    const [state, formAction] = useFormState(mergeAccountsAction, null);

    useRedirect("/account", state !== null && "success" in state);

    return (
        <Form
            action={formAction}
            style={{
                width: "max-content",
                maxWidth: "500px",
                justifyContent: "center",
                display: "flex",
                alignItems: "center",
            }}
        >
            {other_user.map((c) => {
                if (c.type === "password") {
                    return (
                        <VerifyWithPasswordEmail
                            key={c.email}
                            email={c.email}
                        />
                    );
                } else {
                    return <VerifyUsingSSO key={c.email} email={c.email} />;
                }
            })}
            <div className="d-flex flex-column w-100 gap-1">
                <FormError>
                    {state !== null && "error" in state && state.error.details}
                </FormError>
                <SubmitButton
                    style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        gap: "0.5rem",
                        fontWeight: "bold",
                        height: "3rem",
                    }}
                    disabled={other_user.every((c) => c.type === "sso")}
                >
                    <TbPasswordUser size={22} strokeWidth={1.5} />
                    <div className="w-100">Merge accounts</div>
                </SubmitButton>
            </div>
        </Form>
    );
}

function VerifyWithPasswordEmail({ email }: { email: string }) {
    return (
        <>
            <div>
                To merge please provider the credentials for the other account.
            </div>
            <div className="d-flex flex-column w-100 gap-2">
                <EmailInput disabled value={email} name="email" />
                <input type="hidden" name="email" value={email} />
                <PasswordInput />
            </div>
        </>
    );
}

function VerifyUsingSSO({ email: _email }: { email: string }) {
    return (
        <>
            <div>
                To merge an account other account using SSO is currently not
                supported. Please login into the other account and try from
                there.
            </div>
        </>
    );
}
