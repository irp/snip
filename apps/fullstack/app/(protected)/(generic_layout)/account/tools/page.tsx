import type { Metadata } from "next";

import { DoodleConfig } from "./doodleConfig";
import { FontConfig } from "./fontConfig";

import { FormInfo } from "components/form/alert";
import { Item } from "components/form/items";

export default function Settings() {
    return (
        <>
            <h2>Tool settings</h2>
            <div className="d-flex p-1 mb-2">
                The tool settings are bound to the user and will be used on any
                device you are logged in. These settings are the default values
                for the tools in the viewer. For them to take effect you need to
                reload the viewer.
            </div>
            <Item
                title="Doodle tool"
                description="The doodle tool is used to draw on the page in freehand form. You can change the size, color, and smoothing of the pen."
            >
                <DoodleConfig />
            </Item>
            <Item
                title="Text tool"
                description="The text tool is used to add text to the page. You can change the font, size, color, line height, and line wrap."
            >
                <FontConfig />
            </Item>
            <FormInfo style={{ marginInline: "3rem" }}>
                We might add more tool configuration options here in the future.
                If have any suggestions, please let us know.
            </FormInfo>
        </>
    );
}

export const metadata: Metadata = {
    title: "Tool Settings",
};
