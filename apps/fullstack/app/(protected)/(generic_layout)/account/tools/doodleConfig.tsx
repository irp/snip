"use client";
import { forwardRef, useEffect, useRef } from "react";

import { RenderContext } from "@snip/render/types";
import { DoodleSnip } from "@snip/snips/general/doodle";

import { useUserProfile } from "../(general)/context";

import {
    OverlayWorkerContextProvider,
    useOverlayWorkerContext,
} from "components/context/viewer/useWorkerContext";
import { OverlayWorker } from "components/editor/worker/overlayWorker";
import { Input } from "components/form/inputs/base";
import { MultiColorInput } from "components/form/inputs/color";
import { JoinedRangeNumberInput } from "components/form/inputs/number";

import styles from "./settings.module.scss";

export function DoodleConfig() {
    const { config, updateUserConfig } = useUserProfile();

    if (!config) {
        return <></>;
    }

    return (
        <>
            <JoinedRangeNumberInput
                value={config?.pen_size + ""}
                label="Default pen size"
                name="pen_size"
                onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const value = parseFloat(e.target.value);
                    updateUserConfig({ id: config.id!, pen_size: value });
                }}
                min={1}
                max={25}
                step={1}
                onReset={() => {
                    updateUserConfig({ id: config.id!, pen_size: 4 });
                }}
            />

            <MultiColorInput
                label="Default Colors"
                colorList={config.pen_colors ?? []}
                currentColorIdx={config.pen_colors!.indexOf(config.pen_color!)}
                setCurrentColorIdx={(idx: number) => {
                    updateUserConfig({
                        id: config.id!,
                        pen_color: config.pen_colors![idx],
                    });
                }}
                setColorList={(colors: string[]) => {
                    updateUserConfig({
                        id: config.id!,
                        pen_colors: colors,
                    });
                }}
                onReset={() => {
                    updateUserConfig({
                        id: config.id!,
                        pen_colors: [
                            "#000000",
                            "#ff0000",
                            "#00ff00",
                            "#0000ff",
                        ],
                        pen_color: "#000000",
                    });
                }}
                showHex
            />

            <JoinedRangeNumberInput
                value={config.pen_smoothing!.toFixed(2)}
                label="Default pen smoothing"
                name="pen_smoothing"
                onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const value = parseFloat(e.target.value);
                    updateUserConfig({
                        id: config.id!,
                        pen_smoothing: value,
                    });
                }}
                min={0}
                max={1}
                step={0.01}
                onReset={() => {
                    updateUserConfig({
                        id: config.id!,
                        pen_smoothing: 0.0,
                    });
                }}
            />
            <OverlayWorkerContextProvider n={1}>
                <TestDoodle />
            </OverlayWorkerContextProvider>
        </>
    );
}

function TestDoodle() {
    const workers = useOverlayWorkerContext();
    const containerRef = useRef<HTMLDivElement>(null);
    const { config } = useUserProfile();

    //Attach event listener
    useEffect(() => {
        const worker = workers.get(0);
        const container = containerRef.current;
        if (!worker || !container) return;

        //x,y,line/move
        const stack: [DoodleSnip, number][] = [];

        function addEdge(e: MouseEvent, n: boolean = false) {
            const xy = worker!.viewport.getPosInPageCoords([
                e.offsetX,
                e.offsetY,
            ]) as [number, number];

            if (n) {
                stack.push([
                    new DoodleSnip({
                        edges: [],
                        colour: config?.pen_color || "#000000",
                        lineWidth: config?.pen_size || 4,
                        smoothing: config?.pen_smoothing || 0,
                        book_id: -1,
                    }),
                    performance.now(),
                ]);
            }

            let snip = stack.at(-1)?.[0];
            if (!snip) {
                snip = new DoodleSnip({
                    edges: [],
                    colour: config?.pen_color || "#000000",
                    lineWidth: config?.pen_size || 4,
                    smoothing: config?.pen_smoothing || 0,
                    book_id: -1,
                });
            }

            snip.add_edge([...xy]);
        }

        const render = (ctx: RenderContext) => {
            const to_remove = [];
            for (let i = 0; i < stack.length; i++) {
                const [snip, time] = stack[i]!;
                if (performance.now() - time > 5000) {
                    to_remove.push(i);
                } else {
                    snip.render(ctx);
                }
            }

            //remove
            for (let i = to_remove.length - 1; i >= 0; i--) {
                stack.splice(to_remove[i]!, 1);
            }
            worker.rerender();
        };

        /* -------------------------------------------------------------------------- */
        /*                                event handler                               */
        /* -------------------------------------------------------------------------- */

        const onMouseDown = (e: MouseEvent) => {
            worker.add(render);
            console.log("mouse down");
            addEdge(e, true);
            worker.rerender();
            document.addEventListener("pointermove", onMouseMove);
            document.addEventListener("pointerup", onMouseUp);
            document.addEventListener("pointerleave", onMouseUp);
        };
        const onMouseMove = (e: MouseEvent) => {
            //return not mouse down
            if (e.buttons !== 1) return;
            addEdge(e);
            worker.rerender();
        };
        const onMouseUp = () => {
            worker.rerender();
            document.removeEventListener("pointerup", onMouseUp);
            document.removeEventListener("pointermove", onMouseMove);
        };

        container.addEventListener("pointerdown", onMouseDown);

        return () => {
            worker.remove(render);
            container.removeEventListener("pointerdown", onMouseDown);
            document.removeEventListener("pointermove", onMouseMove);
            document.removeEventListener("pointerup", onMouseUp);
            document.removeEventListener("pointerleave", onMouseUp);
        };
    }, [config?.pen_color, config?.pen_size, config?.pen_smoothing, workers]);

    return (
        <div ref={containerRef} className={styles.testDoodle}>
            <Input
                as="div"
                label={
                    <span>Test your configuration here (width=page width)</span>
                }
            >
                {workers.get(0) && (
                    <Preview worker={workers.get(0)!} ref={containerRef} />
                )}
            </Input>
        </div>
    );
}

export const Preview = forwardRef<
    HTMLDivElement,
    {
        worker: OverlayWorker;
        height?: string;
        fit_height?: boolean;
        padding?: number;
    }
>(function Preview(
    {
        worker,
        height,
        fit_height = false,
        padding = 0,
    }: {
        worker: OverlayWorker;
        height?: string;
        fit_height?: boolean;
        padding?: number;
    },
    ref,
) {
    const canvasRef = useRef<HTMLCanvasElement>(null);

    // Setup worker
    useEffect(() => {
        if (!worker || !canvasRef.current) return;

        const canvas = canvasRef.current;

        worker.setupCanvas(canvas);
        worker.viewport.resize(canvas.width, canvas.height);
        worker.viewport.resizePage(1400, 2000);
        worker.viewport.resetMatrix();
        worker.viewport.fitPage(true, fit_height, [padding, padding]);

        function visibilityChange() {
            if (document.visibilityState === "visible") {
                worker.rerender();
            }
        }
        document.addEventListener("visibilitychange", visibilityChange);

        //Attach resize observer
        const observer = new ResizeObserver((entries) => {
            const { width, height } = entries[0]!.contentRect;
            worker.viewport.resize(width, height);
            worker.viewport.resetMatrix();
            worker.viewport.fitPage(true, fit_height, [padding, padding]);
        });
        observer.observe(canvasRef.current);

        return () => {
            worker.terminate();
            observer.disconnect();
        };
    }, [fit_height, padding, worker]);

    return (
        <div
            ref={ref}
            style={{
                width: "100%",
                height: height || "8rem",
                position: "relative",
            }}
        >
            <canvas
                ref={canvasRef}
                style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    pointerEvents: "none",
                }}
            ></canvas>
        </div>
    );
});
