"use client";
import { useEffect } from "react";

import { FONT_URLS } from "@snip/database/fonts/internal";
import { RenderContext } from "@snip/render/types";
import { TextSnip } from "@snip/snips/general/text";

import { useUserProfile } from "../(general)/context";
import { lorem } from "./data";
import { Preview } from "./doodleConfig";

import {
    OverlayWorkerContextProvider,
    useOverlayWorkerContext,
} from "components/context/viewer/useWorkerContext";
import { Input } from "components/form/inputs/base";
import { ColorInput } from "components/form/inputs/color";
import { NumberInput } from "components/form/inputs/number";
import { DropdownInput } from "components/form/inputs/option";

import styles from "./settings.module.scss";

export function FontConfig() {
    const { config, updateUserConfig } = useUserProfile();

    if (!config) {
        return <></>;
    }

    return (
        <>
            <TextFontInput
                value={config.text_font!}
                onSelect={(key) => {
                    updateUserConfig({ id: config.id!, text_font: key });
                }}
                onReset={() => {
                    updateUserConfig({
                        id: config.id!,
                        text_font: "Arial",
                    });
                }}
            />
            <ColorInput
                color={config.text_fontColor!}
                setColor={(color) => {
                    updateUserConfig({
                        id: config.id!,
                        text_fontColor: color,
                    });
                }}
                showHex
                label="Font color"
                name="text_fontColor"
                onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const value = e.target.value;
                    updateUserConfig({
                        id: config.id!,
                        text_fontColor: value,
                    });
                }}
                onReset={() => {
                    updateUserConfig({
                        id: config.id!,
                        text_fontColor: "#000000",
                    });
                }}
            />
            <NumberInput
                value={config.text_fontSize!.toFixed(0)}
                label="Font size"
                name="text_fontSize"
                onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const value = parseFloat(e.target.value);
                    updateUserConfig({
                        id: config.id!,
                        text_fontSize: value,
                    });
                }}
                min={1}
                max={25}
                step={1}
                onReset={() => {
                    updateUserConfig({
                        id: config.id!,
                        text_fontSize: 12,
                    });
                }}
            />
            <NumberInput
                value={config.text_lineHeight!.toFixed(2)}
                label="Line height"
                name="text_lineHeight"
                onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const value = parseFloat(e.target.value);
                    updateUserConfig({
                        id: config.id!,
                        text_lineHeight: value,
                    });
                }}
                min={0.1}
                max={3}
                step={0.01}
                onReset={() => {
                    updateUserConfig({
                        id: config.id!,
                        text_lineHeight: 1.25,
                    });
                }}
            />
            <NumberInput
                value={config.text_lineWrap!.toFixed(0)}
                label="Line wrap"
                name="text_lineWrap"
                onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                    const value = parseFloat(e.target.value);
                    updateUserConfig({
                        id: config.id!,
                        text_lineWrap: value,
                    });
                }}
                min={100}
                max={1000}
                step={1}
                onReset={() => {
                    updateUserConfig({
                        id: config.id!,
                        text_lineWrap: 400,
                    });
                }}
            />
            <OverlayWorkerContextProvider n={1}>
                <FontPreview />
            </OverlayWorkerContextProvider>
        </>
    );
}

function TextFontInput({
    value,
    onSelect = () => {},
    onReset = () => {},
}: {
    value: string;
    onSelect: (key: string | null, e: React.SyntheticEvent<unknown>) => void;
    onReset: () => void;
}) {
    return (
        <DropdownInput
            value={value}
            label="Default font family"
            options={FONT_URLS.map(([, name]) => {
                return {
                    label: <span style={{ fontFamily: name }}>{name}</span>,
                    eventKey: name,
                };
            })}
            onSelect={onSelect}
            onReset={onReset}
        />
    );
}

function FontPreview() {
    const workers = useOverlayWorkerContext();
    const { config } = useUserProfile();

    useEffect(() => {
        const worker = workers.get(0);
        if (!config || !worker) return;

        const text = new TextSnip({
            text: lorem,
            fontFamily: config.text_font ?? "Arial",
            fontSize: config.text_fontSize,
            colour: config.text_fontColor,
            lineHeight: config.text_lineHeight,
            lineWrap: config.text_lineWrap,
            book_id: -1,
        });
        text.x = 700 - text.width / 2;
        text.y = 1000 - text.height / 2;

        const render = (ctx: RenderContext) => {
            ctx.fillStyle = "red";
            text.render(ctx);
        };

        worker.add(render);
        worker.rerender();

        return () => {
            worker.remove(render);
        };
    }, [config, workers]);

    return (
        <div className={styles.testDoodle}>
            <Input
                as="div"
                label="Example (width=page width)"
                className={styles.testDoodle}
            >
                {workers.get(0) && (
                    <Preview worker={workers.get(0)!} height="20rem" />
                )}
            </Input>
        </div>
    );
}
