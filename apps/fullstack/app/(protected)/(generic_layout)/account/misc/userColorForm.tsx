"use client";
import { useUserProfile } from "../(general)/context";

import { ColorInput } from "components/form/inputs/color";

export default function UserColorForm() {
    const { config, updateUserConfig } = useUserProfile();

    const color = config?.user_color || "#000000";

    return (
        <ColorInput
            color={color}
            setColor={(color) => {
                updateUserConfig({
                    id: config!.id!,
                    user_color: color,
                });
            }}
            height="2rem"
            showHex
            fill
        />
    );
}
