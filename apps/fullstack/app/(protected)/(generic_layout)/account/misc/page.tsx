import type { Metadata } from "next";

import UserColorForm from "./userColorForm";

import { Item } from "components/form/items";

export default function Settings() {
    return (
        <>
            <h2>Miscellaneous settings</h2>
            <div className="d-flex p-1 mb-2">
                This page consolidates various configuration options that do not
                belong to other specific categories, providing a centralized
                location for managing these settings.
            </div>
            <Item
                title="User Color"
                description="The color used for your pointer in other users' viewers. And for your username banner next to your currently active pages in the viewer."
            >
                <UserColorForm />
            </Item>
        </>
    );
}

export const metadata: Metadata = {
    title: "Misc Settings",
};
