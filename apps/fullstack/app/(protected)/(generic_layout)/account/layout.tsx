import type { Metadata } from "next";

import { UserProfileProvider } from "./(general)/context";

import BreadCrumbNavBar from "components/common/other/BreadcrumbNavBar";

export default function ProfileLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <>
            <BreadCrumbNavBar>
                {[
                    {
                        label: "General",
                        href: "/account",
                        ariaLabel: "Go to General section",
                        match: "(general)",
                    },
                    {
                        label: "Tools",
                        href: "/account/tools",
                        ariaLabel: "Go to Tools section",
                        match: "tools",
                    },
                    {
                        label: "Groups",
                        href: "/account/groups",
                        ariaLabel: "Go to Groups section",
                        match: "groups",
                    },
                    {
                        label: "Miscellaneous",
                        href: "/account/misc",
                        ariaLabel: "Go to Miscellaneous section",
                        match: "misc",
                    },
                ]}
            </BreadCrumbNavBar>

            <div className="container">
                <UserProfileProvider>{children}</UserProfileProvider>
            </div>
        </>
    );
}

export const metadata: Metadata = {
    title: {
        template: "Account - %s",
        default: "Account",
    },
};
