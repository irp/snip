import {
    changePasswordAction,
    ChangePasswordState,
} from "app/api/(actions)/changePasswordAction";
import { useEffect } from "react";
import { useFormState } from "react-dom";

import { FormError } from "components/form/alert";
import { SubmitButton } from "components/form/buttons";
import { PasswordInput } from "components/form/inputs/text";

/* -------------------------------------------------------------------------- */
/*                               Password Change                              */
/* -------------------------------------------------------------------------- */

export function ChangePasswordForm({
    handleClose,
    disabled,
}: {
    handleClose: () => void;
    disabled: boolean;
}) {
    const [state, formAction] = useFormState(changePasswordAction, null);

    let valid: ChangePasswordState["valid"] = {
        currentPassword: undefined,
        newPassword: undefined,
        confirmPassword: undefined,
    };
    if (state !== null && "valid" in state) {
        valid = state.valid;
    }

    let success = false;
    if (state !== null && "success" in state) {
        success = state.success === true;
    }

    useEffect(() => {
        if (success) {
            setTimeout(() => {
                handleClose();
            }, 4000);
        }
    });

    if (success) {
        return (
            <div className="alert alert-success" role="alert">
                Password changed! Returning to account page...
            </div>
        );
    }

    return (
        <form action={formAction} className="mt-3 d-flex gap-2 flex-column">
            <FormError>
                {state && "error" in state && state.error.details}
            </FormError>
            <PasswordInput
                name="currentPassword"
                label="Current password"
                invalidFeedback="Invalid or incorrect password given!"
                isValid={
                    valid.currentPassword != undefined && valid.currentPassword
                }
                isInvalid={
                    valid.currentPassword != undefined && !valid.currentPassword
                }
                disabled={success || disabled}
            />
            <PasswordInput
                name="newPassword"
                label="New password"
                isValid={valid.newPassword != undefined && valid.newPassword}
                isInvalid={valid.newPassword != undefined && !valid.newPassword}
                disabled={success || disabled}
            />
            <PasswordInput
                name="confirmPassword"
                label="Confirm new password"
                invalidFeedback="Passwords do not match!"
                isValid={
                    valid.confirmPassword != undefined && valid.confirmPassword
                }
                isInvalid={
                    valid.confirmPassword != undefined && !valid.confirmPassword
                }
                disabled={success || disabled}
            />
            <SubmitButton disabled={success || disabled}>
                {success ? "Password changed!" : "Change password"}
            </SubmitButton>
        </form>
    );
}
