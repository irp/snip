"use client";
import { Button } from "react-bootstrap";

import { useUserProfile } from "./context";

export function DownloadUserData() {
    const { user, config, lastLogins, groups } = useUserProfile();

    function downloadUserData() {
        const idx =
            user?.credential_ids?.findIndex(
                (id) => id === user?.primary_credential_id,
            ) ?? 0;

        saveTemplateAsFile(`${user?.emails[idx]}.json`, {
            user,
            config,
            lastLogins,
            groups,
        });
    }

    return (
        <Button variant="outline-dark" onClick={downloadUserData}>
            Download user data (JSON)
        </Button>
    );
}

const saveTemplateAsFile = (filename: string, dataObjToWrite: unknown) => {
    const blob = new Blob([JSON.stringify(dataObjToWrite)], {
        type: "text/json",
    });
    const link = document.createElement("a");

    link.download = filename;
    link.href = window.URL.createObjectURL(blob);
    link.dataset.downloadurl = ["text/json", link.download, link.href].join(
        ":",
    );

    const evt = new MouseEvent("click", {
        view: window,
        bubbles: true,
        cancelable: true,
    });

    link.dispatchEvent(evt);
    link.remove();
};
