import { LogoutBtn } from "app/(errors)/expired/logoutBtn";
import { Metadata } from "next";

import { UserProfileProvider } from "./context";
import { CredentialList } from "./credentials";
import { DownloadUserData } from "./downloadUserData";
import { LastLogin } from "./lastLogin";

import { Item } from "components/form/items";

export default function ProfileOverviewPage() {
    return (
        <UserProfileProvider>
            <h2>General account settings</h2>
            <Item
                title="Login credentials"
                description="Change your email address or password or add an Single Sign-On (SSO) provider."
            >
                <CredentialList />
            </Item>

            <Item
                title="Last logins"
                description="The last time you logged in. This might be helpful to verify if someone else accessed your account."
            >
                <LastLogin />
            </Item>
            <Item
                title="User data"
                description="You can download all your user data in a JSON format. This includes all your personal data, settings, and groups. This does not include any book data."
            >
                <DownloadUserData />
            </Item>
            <Item
                title="Logout"
                description="Logout from your account and invalidate your session cookie."
            >
                <LogoutBtn variant="outline-danger" />
            </Item>
        </UserProfileProvider>
    );
}

export const metadata: Metadata = {
    title: "Settings",
};
