"use client";

import useUser, { UserHook } from "lib/hooks/useUser";
import { createContext, ReactNode, useContext } from "react";

export const useUserProfile = () => {
    const context = useContext(UserProfileContext);
    if (!context) {
        throw new Error(
            "useUserProfile must be used within a UserProfileProvider",
        );
    }
    return context;
};

const UserProfileContext = createContext<UserHook | null>(null);

export const UserProfileProvider = ({ children }: { children: ReactNode }) => {
    const ret = useUser(true, false, true);

    return (
        <UserProfileContext.Provider value={ret}>
            {children}
        </UserProfileContext.Provider>
    );
};
