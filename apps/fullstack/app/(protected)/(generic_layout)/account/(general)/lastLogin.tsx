"use client";
import { useDate } from "lib/hooks/useDate";

import { useUserProfile } from "./context";

export function LastLogin() {
    const { lastLogins, isLoading } = useUserProfile();

    return (
        <>
            <div className="d-flex justify-content-between placeholder-glow">
                {isLoading || !lastLogins ? (
                    <>
                        <code className="placeholder col-3" />
                        <code className="placeholder col-1" />
                    </>
                ) : (
                    <div className="d-flex gap-2 flex-column align-items-center justify-content-between w-100">
                        {lastLogins.toReversed().map((ll, i) => (
                            <div
                                key={i}
                                className="d-flex gap-2 align-items-center justify-content-between w-100"
                            >
                                <code>{ll.ip}</code>
                                <code>
                                    <Date>{ll.time}</Date>
                                </code>
                            </div>
                        ))}
                    </div>
                )}
            </div>
        </>
    );
}

function Date({ children }: { children: Date | string }) {
    const formattedDate = useDate(children, "full");

    return <>{formattedDate}</>;
}
