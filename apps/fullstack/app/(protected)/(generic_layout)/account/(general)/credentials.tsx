"use client";
import { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { TbEdit } from "react-icons/tb";

import { UserDataRet } from "@snip/database/types";

import { ChangeEmailForm } from "./changeEmailForm";
import { ChangePasswordForm } from "./changePasswordForm";
import { useUserProfile } from "./context";

import { Item } from "components/form/items";

import styles from "./credentials.module.scss";

export function CredentialList() {
    const { user, isLoading } = useUserProfile();

    return (
        <>
            <div className="d-flex justify-content-between placeholder-glow">
                {isLoading || !user ? (
                    <div className={styles.table}>
                        <CredentialHeader />
                        <div className={styles.row}>
                            <code className="placeholder col-3" />
                            <code className="placeholder w-100" />
                            <code className="placeholder w-100" />
                            <code className="placeholder w-100" />
                            <code className="placeholder w-100" />
                        </div>
                    </div>
                ) : (
                    <div className={styles.table}>
                        <CredentialHeader />
                        {user.emails.map((email, i) => {
                            return (
                                <CredentialRow
                                    id={user.credential_ids[i]!}
                                    key={user.credential_ids[i]!}
                                    isPrimary={
                                        user.primary_credential_id ==
                                        user.credential_ids[i]!
                                    }
                                    type={user.credential_types[i]!}
                                    email={email}
                                    emailVerified={user.emails_verified[i]!}
                                    ssoProvider={
                                        user.credential_provider_ids[i]
                                    }
                                />
                            );
                        })}
                    </div>
                )}
            </div>
        </>
    );
}

interface Credential {
    id: number;
    isPrimary: boolean;
    type: UserDataRet["credential_types"][0];
    email: string;
    emailVerified: boolean;
    ssoProvider?: string | null;
}

function CredentialHeader() {
    return (
        <div className={styles.header}>
            <div>Email</div>
            <div>Primary</div>
            <div>Verified</div>
            <div>Credential Type</div>
            <div>Edit</div>
        </div>
    );
}

function CredentialRow(credential: Credential) {
    return (
        <div className={styles.row}>
            <div className={styles.email}>{credential.email}</div>
            <div className={styles.primary} data-primary={credential.isPrimary}>
                {credential.isPrimary ? "Yes" : "No"}
            </div>
            <div
                className={styles.verified}
                data-verified={credential.emailVerified}
            >
                {credential.emailVerified ? "Yes" : "No"}
            </div>
            <div className={styles.type}>
                {credential.type == "sso"
                    ? `SSO (${credential.ssoProvider})`
                    : credential.type}
            </div>
            <div>
                <EditCredentialForm credential={credential} />
            </div>
        </div>
    );
}

function EditCredentialForm({ credential }: { credential: Credential }) {
    const [show, setShow] = useState(false);
    const { mutate } = useUserProfile();
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <Button
                variant="link"
                title="Edit credentials"
                style={{ padding: 0, width: "100%" }}
                onClick={handleShow}
            >
                <TbEdit size={18} />
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                centered
                size="lg"
                fullscreen="md-down"
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Edit credential</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Item
                        title="Set as primary email"
                        description="This will be the email address will be displayed as your primary email address and other users will see this email address in collaborative books."
                    >
                        <Button
                            variant="outline-primary"
                            disabled={credential.isPrimary}
                            onClick={() => {
                                fetch("/api/account/user/primary_credential", {
                                    method: "POST",
                                    headers: {
                                        "Content-Type": "application/json",
                                    },
                                    body: JSON.stringify({
                                        credential_id: credential.id,
                                    }),
                                })
                                    .then(() => {
                                        mutate();
                                        handleClose();
                                    })
                                    .catch(console.error);
                            }}
                        >
                            {credential.isPrimary
                                ? "Is already primary"
                                : "Set as primary"}
                        </Button>
                    </Item>
                    <Item
                        title="Change email address"
                        description="Please enter your new email address and confirm it. You will receive an email on the new address to confirm the change with further instructions."
                    >
                        <ChangeEmailForm
                            currentEmail={credential.email}
                            credentialId={credential.id}
                            handleClose={handleClose}
                            disabled={credential.type === "sso"}
                        />
                    </Item>
                    <Item
                        title="Change password"
                        description="Changing your password requires to use your current password or verification via email."
                    >
                        <ChangePasswordForm
                            handleClose={handleClose}
                            disabled={credential.type === "sso"}
                        />
                    </Item>
                </Modal.Body>
            </Modal>
        </>
    );
}
