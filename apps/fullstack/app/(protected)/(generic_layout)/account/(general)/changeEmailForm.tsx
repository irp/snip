import { useEffect } from "react";
import { useFormState } from "react-dom";

import { Form } from "components/form";
import { FormError } from "components/form/alert";
import { SubmitButton } from "components/form/buttons";
import { EmailInput, PasswordInput } from "components/form/inputs/text";
import { validateEmail } from "components/form/validation";

export function ChangeEmailForm({
    currentEmail,
    credentialId,
    handleClose,
    disabled,
}: {
    currentEmail: string;
    credentialId: number;
    handleClose: () => void;
    disabled: boolean;
}) {
    const [state, formAction] = useFormState(changeEmail, null);

    useEffect(() => {
        if (state?.success) {
            setTimeout(() => {
                handleClose();
            }, 4000);
        }
    });

    if (state?.success) {
        return (
            <div className="alert alert-success" role="alert">
                Email send! Returning to account page...
            </div>
        );
    }

    return (
        <Form action={formAction} className="mt-3 d-flex gap-2 flex-column">
            <FormError>
                {state?.error ? (
                    <>
                        {state.error}
                        {state.details && (
                            <div className="mt-2">{state.details}</div>
                        )}
                    </>
                ) : null}
            </FormError>
            <EmailInput
                name="currentEmail"
                label="Current email address"
                disabled={true}
                value={currentEmail}
            />
            <EmailInput
                name="newEmail"
                label="New email address"
                isValid={
                    state != null && "valid" in state && state.valid.newEmail
                }
                isInvalid={
                    state != null && "valid" in state && !state.valid.newEmail
                }
                disabled={state?.success || disabled}
            />
            <EmailInput
                name="confirmNewEmail"
                label="Confirm new email address"
                isValid={
                    state != null &&
                    "valid" in state &&
                    state.valid.confirmNewEmail
                }
                isInvalid={
                    state != null &&
                    "valid" in state &&
                    !state.valid.confirmNewEmail
                }
                invalidFeedback="Emails do not match!"
                disabled={state?.success || disabled}
            />
            <PasswordInput
                isValid={
                    state != null && "valid" in state && state.valid.password
                }
                isInvalid={
                    state?.valid.password != undefined
                        ? !state.valid.password
                        : undefined
                }
                disabled={state?.success || disabled}
            />
            <input type="hidden" name="credentialId" value={credentialId} />
            <SubmitButton disabled={state?.success || disabled}>
                {state?.success
                    ? "Email send, check your inbox!"
                    : "Change email"}
            </SubmitButton>
        </Form>
    );
}

interface ChangeEmailState {
    valid: {
        newEmail?: boolean;
        confirmNewEmail?: boolean;
        password?: boolean;
    };
    success?: boolean;
    error?: string;
    details?: string;
}

async function changeEmail(
    _previousState: ChangeEmailState | null,
    formData: FormData,
): Promise<ChangeEmailState> {
    const valid: ChangeEmailState["valid"] = {};

    // A bit of client side validation
    valid["newEmail"] = validateEmail(
        (formData.get("newEmail") as string) || "",
    );
    valid["confirmNewEmail"] =
        formData.get("newEmail") === formData.get("confirmNewEmail");

    //Early return if any validation failed
    if (!valid["newEmail"] || !valid["confirmNewEmail"]) {
        return {
            valid,
        };
    }

    // Request to change email
    const res = await fetch("/email/account/changeEmail", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            newEmail: formData.get("newEmail"),
            password: formData.get("password"),
            credential_id: formData.get("credentialId"),
        }),
    });

    if (!res.ok) {
        try {
            const data = await res.json();

            if ("details" in data) {
                if (data.details.includes("password")) {
                    valid["password"] = false;
                }
            }
            return {
                valid,
                ...data,
            };
        } catch (e) {
            console.error(e);
            return {
                valid,
                error: "An unknown error occurred, please try again later!",
            };
        }
    }

    const data = await res.json();
    return {
        valid,
        success: data.success,
        error: undefined,
        details: undefined,
    };
}
