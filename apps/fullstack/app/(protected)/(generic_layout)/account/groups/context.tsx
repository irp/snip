import {
    GroupDataWithMembersRet,
    MemberWithSelfRet,
} from "app/api/account/groups/route";
import { createContext, useCallback, useContext, useState } from "react";

import { MemberRole } from "@snip/database/types";

interface GroupContext {
    id: number;
    description: string;
    name: string;
    owner: MemberWithSelfRet;
    members: MemberWithSelfRet[];
    self: MemberWithSelfRet;
    selfAllowedToEdit: boolean;
    selfIsModerator: boolean;
    selfIsOwner: boolean;
    changeName: (name: string) => void;
    changeDescription: (description: string) => void;
    changeOwner: (owner: MemberWithSelfRet) => void;
    changeMemberRole: (member: MemberWithSelfRet, role: MemberRole) => void;
    addMember: (member: MemberWithSelfRet) => void;
    removeMember: (member: MemberWithSelfRet) => void;
    error: string | null;
}

const GroupContext = createContext<GroupContext | null>(null);

/** The group context adds a bit of group functionality
 * as passed down the data is quite tedious to handle.
 */
export function GroupContextProvider({
    group,
    setGroup,
    children,
}: {
    group: GroupDataWithMembersRet;
    setGroup: (group: GroupDataWithMembersRet) => void;
    children: React.ReactNode;
}) {
    const [error, setError] = useState<string | null>(null);

    const setGroupWithErrorHandling = useCallback(
        (group: GroupDataWithMembersRet) => {
            try {
                setGroup(group);
            } catch (e) {
                if (e instanceof Error) setError(e.message);
                console.error(e);
            }
        },
        [setGroup],
    );

    function changeName(name: string) {
        setGroupWithErrorHandling({ ...group, name });
    }

    function changeDescription(description: string) {
        // Optimistic update
        setGroupWithErrorHandling({ ...group, description });
    }

    function changeOwner(newOwner: MemberWithSelfRet) {
        const members: MemberWithSelfRet[] = group.members.map((m) => {
            if (m.user_id === newOwner.user_id) {
                return { ...m, role: "owner" } as MemberWithSelfRet;
            }
            if (m.role === "owner") {
                return {
                    ...m,
                    role: "moderator",
                } as MemberWithSelfRet;
            }
            return m;
        });
        setGroupWithErrorHandling({ ...group, members });
    }

    function changeMemberRole(member: MemberWithSelfRet, role: MemberRole) {
        const members = group.members.map((m) =>
            m.user_id === member.user_id ? { ...m, role } : m,
        );
        setGroupWithErrorHandling({ ...group, members });
    }

    function addMember(member: MemberWithSelfRet) {
        setGroupWithErrorHandling({
            ...group,
            members: [...group.members, member],
        });
    }

    function removeMember(member: MemberWithSelfRet) {
        setGroupWithErrorHandling({
            ...group,
            members: group.members.filter((m) => m.user_id !== member.user_id),
        });
    }
    const self = group.members.find((m) => m.self)!;
    const owner = group.members.find((m) => m.role === "owner")!;
    const selfAllowedToEdit =
        self && (self.role === "owner" || self.role === "moderator");

    return (
        <GroupContext.Provider
            value={{
                id: group.id,
                description: group.description ?? "No group description",
                name: group.name,
                owner,
                members: group.members,
                changeName,
                changeDescription,
                changeOwner,
                changeMemberRole,
                addMember,
                removeMember,
                self,
                selfAllowedToEdit,
                selfIsModerator: self?.role === "moderator",
                selfIsOwner: self?.role === "owner",
                error,
            }}
        >
            {children}
        </GroupContext.Provider>
    );
}

export function useGroupContext() {
    const context = useContext(GroupContext);
    if (!context) {
        throw new Error(
            "useGroupContext must be used within a GroupContextProvider",
        );
    }
    return context;
}
