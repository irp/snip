import type { Metadata } from "next";

import { GroupList } from "./groupList";

export default function Groups() {
    return (
        <div className="placeholder-glow">
            <h2>Groups</h2>
            <div className="d-flex p-1 mb-2 ">
                Groups allows you to organize your team and to collaborate on
                projects easier. Instead of inviting each member individually to
                a book, you can instead add them to a group and then assign the
                group to a book.
            </div>
            <GroupList />
        </div>
    );
}

export const metadata: Metadata = {
    title: "Groups",
};
