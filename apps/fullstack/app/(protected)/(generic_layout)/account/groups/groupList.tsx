"use client";
import { GroupDataWithMembersRet } from "app/api/account/groups/route";
import useGroups from "lib/hooks/useGroups";
import { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { BiGroup } from "react-icons/bi";
import { TbLogout } from "react-icons/tb";

import { GroupContextProvider, useGroupContext } from "./context";
import { CreateGroupForm } from "./createGroupForm";
import { EditGroupForm } from "./editGroupForm";

import { ButtonWithConfirm } from "components/form/buttons";
import { Item } from "components/form/items";

import styles from "./groups.module.scss";
export function GroupList() {
    const {
        groups,
        isLoading,
        createGroup,
        deleteGroup,
        updateGroup,
        leaveGroup,
    } = useGroups();

    return (
        <>
            <Item
                title="Your groups"
                description="If you are an owner or group moderator you can manage the group members, invite users to join, and change settings. If you are a group member you may leave the group.
                "
            >
                <span
                    className={"placeholder " + styles.group}
                    hidden={!isLoading}
                >
                    Loading...
                </span>
                {groups?.map((group, i) => (
                    <Group
                        key={i}
                        group={group}
                        setGroup={(group) => updateGroup(group.id, group)}
                        deleteGroup={() => deleteGroup(group.id!)}
                        leaveGroup={() => leaveGroup(group.id!)}
                    />
                ))}
                {!isLoading && groups?.length === 0 && (
                    <span className={styles.group}>
                        You are not a member of any group yet.
                    </span>
                )}
            </Item>
            <Item
                title="Create group"
                description="Create a new group and invite members to join."
            >
                <CreateGroupForm createGroup={createGroup} />
            </Item>
        </>
    );
}

function Group({
    group,
    setGroup,
    deleteGroup,
    leaveGroup,
}: {
    group: GroupDataWithMembersRet;
    setGroup: (group: GroupDataWithMembersRet) => void;
    deleteGroup: () => void;
    leaveGroup: () => void;
}) {
    return (
        <GroupContextProvider group={group} setGroup={setGroup}>
            <div className={styles.group}>
                <div className={styles.groupContent}>
                    <GroupOverview />
                </div>
                <div className={styles.actions}>
                    <EditGroupForm deleteGroup={deleteGroup} />
                    <LeaveGroupForm leaveGroup={leaveGroup} />
                </div>
            </div>
        </GroupContextProvider>
    );
}

function GroupOverview() {
    const { description, name, self, members } = useGroupContext();

    return (
        <>
            <div className={styles.header}>
                <div className={styles.title}>
                    <BiGroup size={20} />
                    {name}
                </div>
                <div className={styles.badge}>
                    {self?.role.slice(0, 1).toUpperCase() + self?.role.slice(1)}
                </div>
                <div className={styles.badge}>{members.length} members</div>
            </div>
            <div className="d-flex justify-content-start gap-1"></div>
            <div className="d-flex">
                <div className={styles.description}>{description}</div>
            </div>
        </>
    );
}

function LeaveGroupForm({ leaveGroup }: { leaveGroup: () => void }) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const { self, name } = useGroupContext();

    return (
        <>
            <Button
                variant="link"
                disabled={self?.role === "owner"}
                title="Leave group"
                onClick={handleShow}
            >
                <TbLogout size={20} />
            </Button>

            <Modal show={show} onHide={handleClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>
                        Leave the group <q>{name}</q>?
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    You are about to leave the group &quot;{name}&quot;. This
                    will remove you from the group and you will lose access to
                    all group resources!
                </Modal.Body>
                <Modal.Footer>
                    <ButtonWithConfirm
                        variant="primary"
                        onConfirmed={leaveGroup}
                    >
                        Leave group!
                    </ButtonWithConfirm>
                </Modal.Footer>
            </Modal>
        </>
    );
}
