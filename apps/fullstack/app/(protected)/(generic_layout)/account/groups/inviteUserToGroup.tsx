"use client";

import { useRef } from "react";
import { useFormState } from "react-dom";

import { MemberDataRet } from "@snip/database/types";

import { useGroupContext } from "./context";

import { FormError } from "components/form/alert";
import { SubmitButton } from "components/form/buttons";
import { EmailInput } from "components/form/inputs/text";

type State = {
    error?: string;
    details?: string;
    members?: MemberDataRet[];
};

export function InviteUserToGroup() {
    const formRef = useRef<HTMLFormElement>(null);
    const { id, addMember } = useGroupContext();

    async function inviteNewUser(
        id: string,
        _prevState: State,
        formData: FormData,
    ): Promise<State> {
        //Request to email service
        const res = await fetch("/email/group/inviteUser", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                id: id,
                email: formData.get("email"),
            }),
        });

        const data = (await res.json()) as State;
        if (!res.ok) {
            console.warn("Error: ", res.statusText);
        }
        for (const member of data?.members || []) {
            addMember(member);
        }
        return data;
    }

    const [state, formAction] = useFormState<State>(
        // @ts-expect-error no idea
        inviteNewUser.bind(null, id),
        {},
    );

    return (
        <form ref={formRef} action={formAction}>
            <FormError>{state.error && state.details}</FormError>
            <EmailInput
                name="email"
                className="d-flex flex-grow-1"
                more={
                    <SubmitButton className="btn btn-primary">
                        Invite member
                    </SubmitButton>
                }
            />
        </form>
    );
}
