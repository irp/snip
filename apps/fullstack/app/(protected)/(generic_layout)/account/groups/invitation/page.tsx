import { getSessionAction } from "lib/session";
import Link from "next/link";

import { unsealGroupInviteToken } from "@snip/auth/tokens/groupInvite";
import service from "@snip/database";

export default async function InvitePage({
    searchParams,
}: {
    searchParams: { [key: string]: string | string[] | undefined };
}) {
    const session = await getSessionAction();
    const token = searchParams["token"] as string | undefined;
    const group_id = searchParams["id"] as string | undefined;
    const user_id = session.user?.id;

    if (!token || !group_id || !user_id || isNaN(parseInt(group_id))) {
        throw new Error("Invalid request parameters given");
    }

    // Get member to verify token
    const potentialMember = await service.group.getMember(
        user_id,
        parseInt(group_id),
    );

    const tokenData = await unsealGroupInviteToken(
        token,
        potentialMember,
    ).catch(() => {
        throw new Error("Invalid token");
    });

    if (potentialMember.role !== "invited") {
        throw new Error("Invitation already accepted!");
    }

    await service.group.addMember(user_id, tokenData.group_id, "member");

    return (
        <div className="d-flex flex-column justify-content-center align-items-center mt-5">
            <div
                className="text-primary p-5"
                style={{
                    margin: "auto",
                    backgroundColor: "var(--bs-body-bg)",
                    borderRadius: "var(--bs-border-radius)",
                    fontWeight: "bold",
                    maxWidth: "600px",
                }}
            >
                <h1>
                    You have joined the group <q>{tokenData.group_name}</q>
                </h1>
                <hr />
                <p className="text-center text-dark">
                    You are now able to access all books this group has access
                    to. Enjoy!
                </p>
                <hr />
                <p
                    className="text-center mt-3"
                    style={{ fontSize: "1.1rem", margin: 0 }}
                >
                    <Link href="/account/groups">My groups</Link>
                    <br />
                    <Link href="/books">Bookshelf</Link>
                </p>
            </div>
        </div>
    );
}
