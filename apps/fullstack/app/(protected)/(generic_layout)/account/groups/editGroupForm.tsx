import { MemberWithSelfRet } from "app/api/account/groups/route";
import { useDate } from "lib/hooks/useDate";
import { useRef, useState } from "react";
import { Button, FormSelect, InputGroup, Modal } from "react-bootstrap";
import { TbEdit } from "react-icons/tb";

import { MemberRole } from "@snip/database/types";

import { useGroupContext } from "./context";
import { InviteUserToGroup } from "./inviteUserToGroup";

import { FormError, FormWarn } from "components/form/alert";
import { ButtonWithConfirm } from "components/form/buttons";
import { LongTextInput, TextInput } from "components/form/inputs/text";
import { Item } from "components/form/items";

import styles from "./groups.module.scss";

export function EditGroupForm({ deleteGroup }: { deleteGroup: () => void }) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const {
        name,
        description,
        changeDescription,
        changeName,
        self,
        error,
        selfAllowedToEdit,
        members,
    } = useGroupContext();

    return (
        <>
            <Button variant="link" title="Edit group" onClick={handleShow}>
                <TbEdit size={20} />
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                centered
                size="xl"
                fullscreen="lg-down"
                backdrop="static"
            >
                <Modal.Header closeButton>
                    <Modal.Title>Manage the group</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <FormWarn>
                        {!selfAllowedToEdit &&
                            `You are not be able to change the group's settings. Only the owner or a moderator can change the group settings and invite new users!`}
                    </FormWarn>
                    <FormError>{error}</FormError>

                    <Item
                        title="Group name"
                        description="The name of the group. This will be displayed to all group members."
                    >
                        <TextInput
                            label="Group name"
                            value={name}
                            onInput={(
                                e: React.ChangeEvent<HTMLInputElement>,
                            ) => {
                                changeName(e.target.value);
                            }}
                            disabled={!selfAllowedToEdit}
                        />
                    </Item>
                    <Item
                        title="Group description"
                        description="A short description of the group. This will be displayed to all group members."
                    >
                        <LongTextInput
                            label="Group description"
                            value={description || ""}
                            onInput={(
                                e: React.ChangeEvent<HTMLInputElement>,
                            ) => {
                                changeDescription(e.target.value);
                            }}
                            disabled={!selfAllowedToEdit}
                        />
                    </Item>
                    <Item
                        title="Group members"
                        description="Manage the group members. You can add new members, remove members, and change the member type."
                    >
                        <MemberTable />
                    </Item>
                    {self?.role == "owner" || self?.role == "moderator" ? (
                        <Item
                            title="Invite new user to group"
                            description="Invite new members to join the group. You can invite users by their email address."
                        >
                            <InviteUserToGroup />
                        </Item>
                    ) : null}

                    {self?.role == "owner" && (
                        <>
                            <Item
                                title="Transfer ownership"
                                description="Transfer the ownership of the group to another member. You will be demoted to a moderator."
                            >
                                <TransferOwnerInput />
                            </Item>
                            <Item
                                title="Delete group"
                                description="Delete the group. This action is irreversible and will remove access to all group resources. To delete the group, you must be the owner and there must be no other members."
                            >
                                <ButtonWithConfirm
                                    onConfirmed={() => {
                                        deleteGroup();
                                        handleClose();
                                    }}
                                    disabled={members.length > 1}
                                >
                                    Delete group
                                </ButtonWithConfirm>
                            </Item>
                        </>
                    )}
                </Modal.Body>
            </Modal>
        </>
    );
}

function MemberTable() {
    const { members } = useGroupContext();

    return (
        <table className={styles.memberTable}>
            <colgroup>
                <col span={1} style={{ minWidth: "20rem" }} />
                <col span={1} style={{ minWidth: "8rem" }} />
                <col span={1} style={{ minWidth: "1rem" }} />
                <col span={1} style={{ width: "1rem" }} />
            </colgroup>
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Joined</th>
                    <th>Member Type</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {members.map((member, i) => (
                    <MemberTableRow key={i} member={member} />
                ))}
            </tbody>
        </table>
    );
}

function MemberTableRow({ member }: { member: MemberWithSelfRet }) {
    const { changeMemberRole, removeMember, selfAllowedToEdit, selfIsOwner } =
        useGroupContext();
    function onConfirmed() {
        removeMember(member);
    }

    //Show owner or edit form depending on role of member
    let options: { value: MemberRole; label: string }[];
    switch (member.role) {
        case "owner":
            options = [{ value: "owner", label: "Owner" }];
            break;
        case "invited":
            options = [{ value: "invited", label: "Invited" }];
            break;
        default:
            options = [
                { value: "moderator", label: "Moderator" },
                { value: "member", label: "Member" },
            ];
            break;
    }

    const joined_at = useDate(member.joined_at || new Date());

    const primaryEmail = primaryEmailFromMember(member);

    return (
        <tr>
            <td>
                <code>{primaryEmail}</code>
            </td>
            <td>{joined_at}</td>
            <td>
                <select
                    className="form-select"
                    disabled={!selfIsOwner || member.self}
                    onChange={(e) => {
                        changeMemberRole(member, e.target.value as MemberRole);
                        return;
                    }}
                    defaultValue={member.role}
                >
                    {options.map((option) => (
                        <option key={option.value} value={option.value}>
                            {option.label}
                        </option>
                    ))}
                </select>
            </td>
            {(selfAllowedToEdit && selfIsOwner) ||
                (selfAllowedToEdit && member.role == "member" && (
                    <td>
                        <ButtonWithConfirm
                            variant="outline-danger"
                            onConfirmed={onConfirmed}
                        >
                            Remove
                        </ButtonWithConfirm>
                    </td>
                ))}
        </tr>
    );
}

function TransferOwnerInput() {
    const selectRef = useRef<HTMLSelectElement>(null);
    const { members, self, changeOwner } = useGroupContext();

    function onConfirmed() {
        if (!selectRef.current) return;
        const id = selectRef.current.value;
        console.warn("Change owner to", id);
        const owner = members.find((m) => m.user_id == parseInt(id));
        if (owner) {
            changeOwner(owner);
        }
    }

    return (
        <InputGroup>
            <FormSelect ref={selectRef} defaultValue={self?.user_id}>
                {members.map((member) => {
                    const pEmail = primaryEmailFromMember(self!);
                    return (
                        <option
                            key={member.user_id}
                            value={member.user_id}
                            disabled={member.self || member.role == "owner"}
                        >
                            {pEmail}
                        </option>
                    );
                })}
            </FormSelect>
            <ButtonWithConfirm variant="outline-dark" onConfirmed={onConfirmed}>
                Transfer ownership
            </ButtonWithConfirm>
        </InputGroup>
    );
}

function primaryEmailFromMember(member: MemberWithSelfRet) {
    const primaryIDX = member.credential_ids.findIndex(
        (id) => id == member.primary_credential_id,
    );
    return member.emails[primaryIDX];
}
