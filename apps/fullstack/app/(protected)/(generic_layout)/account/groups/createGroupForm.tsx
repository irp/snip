"use client";
import { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

import { SubmitButton } from "components/form/buttons";
import { LongTextInput, TextInput } from "components/form/inputs/text";
import { Item } from "components/form/items";

export function CreateGroupForm({
    createGroup,
}: {
    createGroup: (name: string, description?: string) => Promise<void>;
}) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [error, setError] = useState<string>();

    async function formAction(formData: FormData) {
        const name = formData.get("name") as string;
        const description = formData.get("description") as string;
        try {
            await createGroup(name, description);
            handleClose();
        } catch (e) {
            if (e instanceof Error) {
                setError(e.message);
            }
            console.error(e);
        }
    }

    return (
        <>
            <Button variant="outline-dark" onClick={handleShow}>
                Create group
            </Button>
            <Modal
                show={show}
                onHide={handleClose}
                centered
                size="xl"
                fullscreen="lg-down"
                backdrop="static"
            >
                <Modal.Header closeButton>
                    <Modal.Title>Create a new group</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form action={formAction}>
                        <Item
                            title="Group name"
                            description="The name of the group. This will be displayed to all group members."
                        >
                            <TextInput
                                label="Group name"
                                name="name"
                                isInvalid={error !== null}
                                invalidFeedback={error}
                                required
                                autoComplete="off"
                            />
                        </Item>
                        <Item
                            title="Group description"
                            description="A short description of the group. This will be displayed to all group members."
                        >
                            <LongTextInput
                                label="Group description"
                                name="description"
                            />
                        </Item>
                        <SubmitButton className="btn btn-outline-dark">
                            Create group
                        </SubmitButton>
                    </form>
                </Modal.Body>
            </Modal>
        </>
    );
}
