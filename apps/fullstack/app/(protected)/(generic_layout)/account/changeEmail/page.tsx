import { cookies } from "next/headers";
import Link from "next/link";

import { getSession } from "@snip/auth/session";
import {
    EmailChangeToken,
    unsealEmailChangeToken,
} from "@snip/auth/tokens/emailChange";
import service from "@snip/database";

export default async function VerifyPage({
    searchParams,
}: {
    searchParams: { [key: string]: string | string[] | undefined };
}) {
    const session = await getSession(cookies());
    const user = await service.user.getById(session!.user!.id);
    const token = searchParams["token"] as string | undefined;
    const credential_id = searchParams["credential_id"] as string | undefined;

    if (!user || !token || !credential_id || isNaN(Number(credential_id))) {
        return (
            <div className="d-flex flex-grow-1 justify-content-center align-items-center flex-column">
                <h1>No token provided or invalid token</h1>
                <p>
                    If you want to request an email change, please go to your
                    profile settings and request a new email change.
                </p>
                <Link href="/account">Go back to profile</Link>
            </div>
        );
    }

    let tokenData: EmailChangeToken | undefined;
    if (token) {
        try {
            tokenData = await unsealEmailChangeToken(
                token,
                user,
                Number(credential_id),
            );
        } catch (_error) {
            tokenData = undefined;
        }
    }

    let old_email: string | undefined;
    for (let i = 0; i < user.credential_ids.length; i++) {
        if (user.credential_ids[i] === Number(credential_id)) {
            old_email = user.emails[i]!;
            break;
        }
    }

    if (!tokenData) {
        return (
            <div className="d-flex flex-grow-1 justify-content-center align-items-center flex-column">
                <h1>Invalid token detected</h1>
                <p>
                    If you want to request an email change, token might already
                    be used or expired. Please go to your profile settings and
                    request a new email change.
                </p>
                <Link href="/account">Go back to profile</Link>
            </div>
        );
    }

    // Perform email change

    try {
        await service.user.changeEmail(
            tokenData.from_credential_id,
            tokenData.to_email,
        );
    } catch (error) {
        console.error(error);
        return (
            <div className="d-flex flex-grow-1 justify-content-center align-items-center flex-column">
                <h1>Failed to change email</h1>
                <p>
                    An error occurred while trying to change your email. Please
                    try again later. Or contact support if the problem persists.
                </p>
                <Link href="/account">Go back to profile</Link>
            </div>
        );
    }

    // Email change successful
    return (
        <div className="d-flex flex-grow-1 justify-content-center align-items-center flex-column">
            <h1>Email changed</h1>
            <p>
                Successfully changed email from <b>{old_email}</b>
                to <b>{tokenData.to_email}</b>!
            </p>
            <p>Make sure to use your new email for future logins.</p>
            <Link href="/account">Go back to profile</Link>
        </div>
    );
    //Change email
}
