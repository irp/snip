import { getSessionAction } from "lib/session";
import Link from "next/link";

import { unsealCollaborationRequestToken } from "@snip/auth/tokens/collaborationRequest";
import service from "@snip/database";
import { BookDataRet } from "@snip/database/types";

export default async function InvitePage({
    searchParams,
}: {
    searchParams: { [key: string]: string | string[] | undefined };
}) {
    const session = await getSessionAction();
    const user_id = session?.user?.id;
    if (!user_id) {
        return <>Please login to accept an invite first!</>;
    }

    const token = searchParams["token"] as string | undefined;
    const book_id = searchParams["id"] as string | undefined;

    if (!token) {
        throw new Error("Invalid request parameters given");
    }

    // Verify token
    const tokenData = await unsealCollaborationRequestToken(
        token,
        user_id,
    ).catch(() => {
        throw new Error("Invalid token");
    });

    // Accept invitation
    // This might throw an error if the invitation has already been accepted
    let book: BookDataRet;
    try {
        const invite = await service.collaboratorsInvite.acceptById(
            tokenData.invite_id,
        );
        book = await service.book.getById(invite.book_id!);
    } catch (error) {
        if (!(error instanceof Error)) {
            console.error(error);
            return <>Internal Error</>;
        }

        if (error.name == "InviteAlreadyAcceptedError") {
            return (
                <>
                    <h1>Invitation already accepted</h1>
                    <hr />
                    <p className="text-center text-dark">
                        The invitation you tried to accept has already been
                        accepted.
                    </p>
                    <hr />
                    <p
                        className="text-center mt-3"
                        style={{ fontSize: "1.1rem", margin: 0 }}
                    >
                        <Link href={`/books/${book_id}`}>View book</Link>
                    </p>
                </>
            );
        }
        if (error.name == "ExpiredError") {
            return (
                <>
                    <h1>Invitation expired</h1>
                    <hr />
                    <p className="text-center text-dark">
                        The invitation you tried to accept has expired.
                    </p>
                    <hr />
                    <p
                        className="text-center mt-3"
                        style={{ fontSize: "1.1rem", margin: 0 }}
                    >
                        <Link href="/books/">Bookshelf</Link>
                    </p>
                </>
            );
        }
        if (error.name == "NotFoundError") {
            return (
                <>
                    <h1>Invitation not found</h1>
                    <hr />
                    <p className="text-center text-dark">
                        The invitation you tried to accept was not found. It
                        might have been cancelled.
                    </p>
                    <hr />
                    <p
                        className="text-center mt-3"
                        style={{ fontSize: "1.1rem", margin: 0 }}
                    >
                        <Link href="/books/">Bookshelf</Link>
                    </p>
                </>
            );
        }

        throw new Error("Invalid invitation");
    }

    return (
        <>
            <h1>
                Join have accepted the invitation to collaborate on{" "}
                <q>{book.title}</q>
            </h1>
            <hr />
            <p className="text-center text-dark">
                You are now able to access this book and collaborate with others
                on it.
            </p>
            <hr />
            <p
                className="text-center mt-3"
                style={{ fontSize: "1.1rem", margin: 0 }}
            >
                <Link href={`/books/${book_id}`}>Show book</Link>
                <br />
                <Link href="/books">Bookshelf</Link>
            </p>
        </>
    );
}
