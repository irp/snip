"use client";

import Link from "next/link";
import { useEffect } from "react";

export default function Error({
    error,
}: {
    error: Error & { digest?: string };
}) {
    useEffect(() => {
        // Log the error to an error reporting service
        console.error(error);
    }, [error]);

    return (
        <>
            <h1 className="text-center">Invitation error</h1>
            <hr />
            {error.message && (
                <div className="text-center p-1">
                    <code>{error.message}</code>
                </div>
            )}
            <p className="text-center text-dark">
                The token provided is invalid or has already been used. Make
                sure you are using the correct link we have sent you via email.
                If you are still having issues, please contact us.
            </p>
            <hr />
            <p
                className="text-center mt-3"
                style={{ fontSize: "1.1rem", margin: 0 }}
            >
                <Link href="/books">To bookshelf</Link>
            </p>
        </>
    );
}
