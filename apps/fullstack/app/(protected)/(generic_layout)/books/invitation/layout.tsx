export default function Layout({ children }: { children: React.ReactNode }) {
    return (
        <div className="d-flex flex-column justify-content-center align-items-center mt-5">
            <div
                className="text-primary p-5"
                style={{
                    margin: "auto",
                    backgroundColor: "var(--bs-body-bg)",
                    borderRadius: "var(--bs-border-radius)",
                    fontWeight: "bold",
                    maxWidth: "600px",
                }}
            >
                {children}
            </div>
        </div>
    );
}
