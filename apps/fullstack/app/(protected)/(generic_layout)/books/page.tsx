import type { Metadata } from "next";

import { BooksBrowser } from "components/books/browser/BooksBrowser";
import AddToHomeScreen from "components/common/pwa/AddToHomeScreen/AddToHomeScreen";
import UpdateNotification from "components/common/UpdateNotification";

export default function BookBrowser() {
    return (
        <div className="container">
            <UpdateNotification />
            <AddToHomeScreen />
            <BooksBrowser rootFolderId={"root"} />
        </div>
    );
}

export const metadata: Metadata = {
    title: "My Books",
};
