import { getSessionAction } from "lib/session";
import type { Metadata } from "next";
import { redirect } from "next/navigation";

import { getPerms } from "@snip/auth/book_permissions";
import service from "@snip/database";

/** Authentication for this book
 * any user needs pRead to view the book
 */
export default async function Page({
    children,
    params: { id },
}: {
    children: React.ReactNode;
    params: { id: string };
}) {
    if (!id || isNaN(parseInt(id))) {
        return redirect(`/not-found`);
    }

    const session = await getSessionAction();

    // Check if user is allowed to edit
    // only users not using a ui token are allowed to edit
    const perms = await getPerms(parseInt(id), {
        user_id: session?.user?.id,
        ui_token_id: undefined,
        bearer_token: undefined,
    });

    if (!perms.resolved.pRead) {
        return redirect(`/unauthorized`);
    }

    return <>{children}</>;
}

/** Metadata is shared with the children routes, this allows to only get the book
 * title once and share it with all the children routes to generate metadata
 * for the specific pages.
 * see https://nextjs.org/docs/app/api-reference/functions/generate-metadata#template-object
 */
export async function generateMetadata({
    params,
}: {
    params: {
        id: string;
    };
}): Promise<Metadata> {
    const id = params.id;
    const book = await service.book.getById(parseInt(id));
    return {
        title: {
            template: `${book.title} - %s`,
            default: `${book.title}`,
        },
    };
}
