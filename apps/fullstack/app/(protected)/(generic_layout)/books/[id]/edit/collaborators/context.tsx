"use client";
import {
    CollaboratorData,
    CollaboratorRouteReturn,
} from "app/api/books/[id]/(edit)/collaborators/route";
import { createContext, useContext, useMemo } from "react";
import useSWR from "swr";

import { CollaborationInviteDataRet } from "@snip/database/types";

export type PermissionKey = "pRead" | "pWrite" | "pDelete" | "pACL";

interface CollaboratorContext {
    // data sorted by type
    pendingInvites: CollaboratorRouteReturn["pendingInvites"];
    collaborators: CollaboratorRouteReturn["collaborators"];
    self?: CollaboratorRouteReturn["self"];
    owner?: CollaboratorData;
    // data loading and updating
    setPermissions: (
        collaborator: CollaboratorData,
        key: PermissionKey,
        value: boolean,
    ) => void;
    removeCollaborator: (collaborator: CollaboratorData) => void;
    addInvite: (invite: Required<CollaborationInviteDataRet>) => void;
    addCollaborator: (collaborator: CollaboratorData) => void;
    removeInvite: (invite: CollaborationInviteDataRet) => void;
    transferOwnership: (newOwnerId: CollaboratorData) => void;
    isLoading: boolean;
}

const CollaboratorContext = createContext<CollaboratorContext | null>(null);

export function CollaboratorContextProvider({
    book_id,
    children,
}: {
    book_id: number;
    children: React.ReactNode;
}) {
    const { data, mutate, isLoading } = useSWR<CollaboratorRouteReturn>(
        `/api/books/${book_id}/collaborators`,
        async (url: string) => {
            const res = await fetch(url);
            if (!res.ok) {
                throw new Error("Failed to fetch collaborators");
            }
            return res.json();
        },
    );

    const pendingInvites = data?.pendingInvites || [];
    const self = data?.self;

    // Parse data into different parts
    const [collaborators, owner] = useMemo(() => {
        const collaborators = data?.collaborators || [];
        const owner = collaborators.find((c) => c.owner);
        return [collaborators, owner];
    }, [data?.collaborators]);

    function setPermissions(
        collaborator: CollaboratorData,
        key: PermissionKey,
        value: boolean,
    ) {
        const optimisticData = data?.collaborators.map((c) => {
            if (c.id === collaborator.id && c.type === collaborator.type) {
                return {
                    ...c,
                    [key]: value,
                };
            }
            return c;
        });

        mutate(
            async (
                data: CollaboratorRouteReturn | undefined,
            ): Promise<CollaboratorRouteReturn> => {
                // Send to the server
                const res = await fetch(`/api/books/${book_id}/collaborators`, {
                    method: "POST",
                    body: JSON.stringify({
                        ...collaborator,
                        [key]: value,
                    }),
                });

                if (!res.ok) {
                    throw new Error("Failed to update permissions");
                }

                return {
                    ...data,
                    collaborators: optimisticData ?? [],
                } as CollaboratorRouteReturn;
            },
            {
                revalidate: false,
                rollbackOnError: true,
                optimisticData: {
                    ...data,
                    collaborators: optimisticData ?? [],
                } as CollaboratorRouteReturn,
            },
        );
    }

    function removeCollaborator(collaborator: CollaboratorData) {
        const optimisticData = data?.collaborators.filter(
            (c) => c.id !== collaborator.id,
        );

        mutate(
            async (
                data: CollaboratorRouteReturn | undefined,
            ): Promise<CollaboratorRouteReturn> => {
                // Send to the server
                const res = await fetch(`/api/books/${book_id}/collaborators`, {
                    method: "DELETE",
                    body: JSON.stringify(collaborator),
                });

                if (!res.ok) {
                    throw new Error("Failed to remove collaborator");
                }

                return {
                    ...data,
                    collaborators: optimisticData,
                } as CollaboratorRouteReturn;
            },
            {
                revalidate: false,
                rollbackOnError: true,
                optimisticData: {
                    ...data,
                    collaborators: optimisticData,
                } as CollaboratorRouteReturn,
            },
        );
    }

    function removeInvite(invite: CollaborationInviteDataRet) {
        const optimisticData = data?.pendingInvites.filter(
            (i) => i.id !== invite.id,
        );

        mutate(
            async (
                data: CollaboratorRouteReturn | undefined,
            ): Promise<CollaboratorRouteReturn> => {
                const res = await fetch(`/api/books/${book_id}/collaborators`, {
                    method: "DELETE",
                    body: JSON.stringify({
                        id: invite.id,
                        type: "invite",
                    }),
                });

                if (!res.ok) {
                    throw new Error("Failed to remove invite");
                }

                return {
                    ...data,
                    pendingInvites: optimisticData,
                } as CollaboratorRouteReturn;
            },
            {
                revalidate: false,
                rollbackOnError: true,
                optimisticData: {
                    ...data,
                    pendingInvites: optimisticData,
                } as CollaboratorRouteReturn,
            },
        );
    }

    function addInvite(invite: Required<CollaborationInviteDataRet>) {
        mutate(
            (
                data: CollaboratorRouteReturn | undefined,
            ): CollaboratorRouteReturn => {
                return {
                    ...data,
                    pendingInvites: [...data!.pendingInvites, invite],
                } as CollaboratorRouteReturn;
            },
            true,
        );
    }

    function addCollaborator(collaborator: CollaboratorData) {
        mutate(
            (
                data: CollaboratorRouteReturn | undefined,
            ): CollaboratorRouteReturn => {
                return {
                    ...data,
                    collaborators: [...data!.collaborators, collaborator],
                } as CollaboratorRouteReturn;
            },
            true,
        );
    }

    function transferOwnership(owner: CollaboratorData) {
        // apply optimistic update
        mutate(
            (
                data: CollaboratorRouteReturn | undefined,
            ): CollaboratorRouteReturn => {
                return {
                    ...data,
                    collaborators: data!.collaborators.map((c) => {
                        if (c.id === owner.id) {
                            return {
                                ...c,
                                pACL: true,
                                pDelete: true,
                                pRead: true,
                                pWrite: true,
                                owner: true,
                            };
                        }
                        return {
                            ...c,
                            owner: false,
                        };
                    }),
                } as CollaboratorRouteReturn;
            },
            false,
        );
        fetch(`/api/books/${book_id}/owner`, {
            method: "POST",
            body: JSON.stringify({
                entity_id: owner.id,
                entity_type: owner.type,
            }),
        })
            .then((_res) => {
                mutate();
            })
            .catch(() => {
                // rollback
                mutate();
            });
    }

    return (
        <CollaboratorContext.Provider
            value={{
                pendingInvites,
                collaborators,
                self,
                owner,
                setPermissions,
                removeCollaborator,
                addInvite,
                addCollaborator,
                removeInvite,
                transferOwnership,
                isLoading,
            }}
        >
            {children}
        </CollaboratorContext.Provider>
    );
}

export function useCollaboratorContext() {
    const context = useContext(CollaboratorContext);
    if (!context) {
        throw new Error(
            "useCollaboratorContext must be used within a CollaboratorContextProvider",
        );
    }
    return context;
}
