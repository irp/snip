import type { Metadata } from "next";

import { FullCollaboratorsForm } from "./components";

export default function Page({ params }: { params: { id: string } }) {
    return (
        <>
            <h2>Collaborators</h2>
            <p>
                You may edit user access to this book here. This allows you to
                control who can view or edit the book&apos;s content, ensuring
                security and privacy according to your preferences.
            </p>
            <div>
                <FullCollaboratorsForm book_id={parseInt(params.id)} />
            </div>
        </>
    );
}

export const metadata: Metadata = {
    title: "Collaborators",
};
