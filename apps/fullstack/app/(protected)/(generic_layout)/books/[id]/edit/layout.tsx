import BreadCrumbNavBar from "components/common/other/BreadcrumbNavBar";

import styles from "./(general)/bookinfo.module.scss";

export default function Layout({
    children,
    params,
}: {
    children: React.ReactNode;
    params: { id: string };
}) {
    return (
        <>
            <BreadCrumbNavBar>
                {[
                    {
                        label: "General",
                        href: `/books/${params.id}/edit`,
                        ariaLabel: "Go to General section",
                        match: "(general)",
                    },
                    {
                        label: "Collaborators",
                        href: `/books/${params.id}/edit/collaborators`,
                        ariaLabel: "Go to Collaborators section",
                        match: "collaborators",
                    },
                    {
                        label: "API",
                        href: `/books/${params.id}/edit/api`,
                        ariaLabel: "Go to API section",
                        match: "api",
                    },
                    {
                        label: "Other",
                        href: `/books/${params.id}/edit/other`,
                        ariaLabel: "Go to Other section",
                        match: "other",
                    },
                ]}
            </BreadCrumbNavBar>
            <div className="container">
                <div className={styles.container}>{children}</div>
            </div>
        </>
    );
}
