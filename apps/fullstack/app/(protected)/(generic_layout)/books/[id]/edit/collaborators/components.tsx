"use client";

import {
    CollaboratorData,
    CollaboratorGroupData,
    CollaboratorUserData,
} from "app/api/books/[id]/(edit)/collaborators/route";
import { useExpiryDate } from "lib/hooks/useDate";
import useGroups from "lib/hooks/useGroups";
import useTokens from "lib/hooks/useTokens";
import { useEffect, useMemo, useRef, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import FormCheck from "react-bootstrap/FormCheck";
import { BsTrash3 } from "react-icons/bs";
import { TbClipboard, TbClipboardCheck } from "react-icons/tb";

import {
    CollaborationInviteDataRet,
    ID,
    UITokenData,
} from "@snip/database/types";

import {
    CollaboratorContextProvider,
    PermissionKey,
    useCollaboratorContext,
} from "./context";

import { FormError, FormWarn } from "components/form/alert";
import { ButtonWithConfirm, SubmitButton } from "components/form/buttons";
import { DropdownInput, OptionInput } from "components/form/inputs/option";
import { DateTimeInput } from "components/form/inputs/other";
import { TextInput } from "components/form/inputs/text";
import { Item } from "components/form/items";
import { validateEmail } from "components/form/validation";

import styles from "./collaborators.module.scss";

export function FullCollaboratorsForm({ book_id }: { book_id: number }) {
    return (
        <CollaboratorContextProvider book_id={book_id}>
            <Item
                title="Collaborators list"
                description="List of collaborators you may
edit their permissions or remove them from the book. The View, Edit, and Delete columns signify the permissions of each collaborator. Users or groups with the ACL permission may edit the permissions of other collaborators may edit the permissions of other collaborators. Delete is not currently used for any purpose."
            >
                <CollaboratorsList />
            </Item>
            <Item
                title="Invite collaborator"
                description="Invite a collaborator to this book they will need 
    to accept the invitation before they can access the book. The invitation will be sent to their email."
            >
                <InviteForm book_id={book_id} />
                <PendingInvitesList />
            </Item>
            <Item
                title="Add group"
                description="Add a group to this book. All members of the group will have the same permissions to the book. You may only add groups you are a moderator or owner of.
        "
            >
                <GroupAddForm book_id={book_id} />
            </Item>
            <Item
                title="Create shareable link"
                description="Create a shareable link to this book. Anyone with the link will be able to access the book. You may disable the link at any time or set an expiration date."
            >
                <SharableLinkAddForm book_id={book_id} />
                <SharableLinkList book_id={book_id} />
            </Item>
            <Item
                title="Ownership"
                description="Transfer ownership of this book to another collaborator. The new owner will have full control over the book, including the ability to transfer ownership to another collaborator."
            >
                <TransferOwnerForm />
            </Item>
        </CollaboratorContextProvider>
    );
}

/* -------------------------------------------------------------------------- */
/*                              collaborator list                             */
/* -------------------------------------------------------------------------- */

function CollaboratorsList() {
    const { self, collaborators, owner, isLoading, setPermissions } =
        useCollaboratorContext();

    return (
        <div>
            <FormWarn>
                {self &&
                    !self.pACL &&
                    `You may not edit permissions for other collaborators. Only collaborators with the pACL permission may edit permissions for other collaborators.`}
            </FormWarn>

            <table className={styles.table}>
                <colgroup>
                    <col />
                    <col />
                    <col className={styles.col_switch} />
                    <col className={styles.col_switch} />
                    <col className={styles.col_switch} />
                    <col className={styles.col_switch} />
                    <col className={styles.col_remove} />
                </colgroup>
                <thead>
                    <tr>
                        <th>Email/Name</th>
                        <th>Type</th>
                        <th className={styles.col_switch}>View</th>
                        <th className={styles.col_switch}>Edit</th>
                        <th className={styles.col_switch}>Delete</th>
                        <th className={styles.col_switch}>ACL</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr className="w-100 placeholder-glow" hidden={!isLoading}>
                        <td
                            colSpan={7}
                            className="placeholder"
                            style={{ display: "table-cell" }}
                        ></td>
                    </tr>

                    <CollaboratorRow
                        collaborator={owner}
                        setPermission={(key, value) => {
                            if (!owner) return;
                            setPermissions(owner, key, value);
                        }}
                        disabledPerms
                        disabledRemove
                    />

                    {collaborators
                        .filter((c) => !c.owner)
                        .map((collaborator, i) => (
                            <CollaboratorRow
                                key={i}
                                collaborator={collaborator}
                                setPermission={(key, value) =>
                                    setPermissions(collaborator, key, value)
                                }
                                disabledRemove={
                                    owner?.type === "user" && !owner.self
                                }
                                disabledPerms={
                                    !self?.pACL ||
                                    (collaborator as CollaboratorUserData)?.self
                                }
                            />
                        ))}

                    {collaborators.length == 0 && !isLoading && (
                        <tr>
                            <td colSpan={7}>No collaborators</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
}

function CollaboratorRow({
    collaborator,
    setPermission,
    disabledPerms = false,
    disabledRemove = false,
}: {
    collaborator?: CollaboratorData;
    setPermission: (key: PermissionKey, value: boolean) => void;
    disabledPerms?: boolean;
    disabledRemove?: boolean;
}) {
    if (!collaborator) {
        return null;
    }

    let note = "";
    if (collaborator.type == "user" && collaborator.self) {
        note += "You";
    }
    if (collaborator.owner) {
        if (note.length > 0) {
            note += " & Owner";
        } else {
            note = "Owner";
        }
    }

    return (
        <tr>
            <td>
                {collaborator.type === "user"
                    ? collaborator.email
                    : collaborator.name}{" "}
                {note && (
                    <span style={{ fontVariant: "super", fontSize: "1rem" }}>
                        ({note})
                    </span>
                )}
            </td>
            <td>{collaborator.type === "user" ? "User" : "Group"}</td>

            <>
                <td>
                    <FormCheck
                        type="switch"
                        className={styles.switch}
                        checked={collaborator.pRead}
                        onChange={(e) =>
                            setPermission("pRead", e.target.checked)
                        }
                        disabled={disabledPerms}
                    />
                </td>
                <td>
                    <FormCheck
                        type="switch"
                        className={styles.switch}
                        checked={collaborator.pWrite}
                        onChange={(e) =>
                            setPermission("pWrite", e.target.checked)
                        }
                        disabled={disabledPerms}
                    />
                </td>
                <td>
                    <FormCheck
                        type="switch"
                        className={styles.switch}
                        checked={collaborator.pDelete}
                        onChange={(e) =>
                            setPermission("pDelete", e.target.checked)
                        }
                        disabled={disabledPerms}
                    />
                </td>
                <td>
                    <FormCheck
                        type="switch"
                        className={styles.switch}
                        checked={collaborator.pACL}
                        onChange={(e) =>
                            setPermission("pACL", e.target.checked)
                        }
                        disabled={disabledPerms}
                    />
                </td>
                {
                    // Only owner can remove collaborators
                    <td
                        title={
                            disabledRemove
                                ? "Only owner may remove collaborators"
                                : "Remove collaborator"
                        }
                    >
                        <RemoveCollaboratorModal
                            collaborator={collaborator}
                            disabledRemove={disabledRemove}
                        />
                    </td>
                }
            </>
        </tr>
    );
}

function RemoveCollaboratorModal({
    collaborator,
    disabledRemove,
}: {
    collaborator: CollaboratorData;
    disabledRemove: boolean;
}) {
    const { removeCollaborator } = useCollaboratorContext();
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <button
                className="btn btn-link p-0"
                onClick={handleShow}
                disabled={disabledRemove}
                aria-label="Remove collaborator"
            >
                <BsTrash3 />
            </button>
            <Modal
                show={show}
                onHide={handleClose}
                centered
                fullscreen="md-down"
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Remove collaborator</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    Are you sure you want to remove{" "}
                    <q>
                        {collaborator.type === "user"
                            ? collaborator.email
                            : collaborator.name}
                    </q>{" "}
                    from this book? They will lose access to the book and all
                    its resources!
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button
                        variant="danger"
                        onClick={() => removeCollaborator(collaborator)}
                    >
                        Remove
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

/* -------------------------------------------------------------------------- */
/*                               pending invites                              */
/* -------------------------------------------------------------------------- */

function PendingInvitesList() {
    const { pendingInvites } = useCollaboratorContext();

    return (
        <div>
            <table className={styles.table}>
                <colgroup>
                    <col />
                    <col className={styles.col_date} />
                    <col className={styles.col_remove} />
                </colgroup>
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Expires at</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        // Empty row if no pending invites
                        pendingInvites.length == 0 && (
                            <tr>
                                <td colSpan={3}>No pending invites</td>
                            </tr>
                        )
                    }
                    {pendingInvites.map((invite, i) => (
                        <InviteRow key={i} invite={invite} />
                    ))}
                </tbody>
            </table>
        </div>
    );
}

function InviteRow({ invite }: { invite: CollaborationInviteDataRet }) {
    const { self, removeInvite } = useCollaboratorContext();

    const expires_at = useExpiryDate(invite?.expire_at || undefined);

    // Get primary email
    const idx = invite.credential_ids.findIndex(
        (c) => c === invite.primary_credential_id,
    );
    const primary_email = invite.emails[idx];

    return (
        <tr>
            <td>{primary_email}</td>
            <td>{expires_at}</td>
            <td>
                <Button
                    variant="link"
                    className="p-0"
                    aria-label="Remove invite"
                    onClick={() => removeInvite(invite)}
                    disabled={!self?.pACL}
                >
                    <BsTrash3 />
                </Button>
            </td>
        </tr>
    );
}

function InviteForm({ book_id }: { book_id: number }) {
    const { addInvite, self } = useCollaboratorContext();
    const [error, setError] = useState<string>();
    const [pending, setPending] = useState(false);
    const ref = useRef<HTMLFormElement>(null);

    async function action(formData: FormData) {
        const email = formData.get("email") as string;
        if (!email) {
            setError("Email is invalid");
            return;
        }
        const valid = validateEmail(email);
        if (valid !== true) {
            setError("Email is invalid");
            return;
        }
        setError(undefined);

        // Send invite
        setPending(true);
        const res = await fetch("/email/book/inviteCollaborator", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email,
                book_id,
            }),
        });

        if (!res.ok) {
            const data = await res.json();
            setError(data.error);
            setPending(false);
            return;
        }
        const data = await res.json();
        addInvite(data.invite);
        setPending(false);
        ref.current?.reset();
    }

    return (
        <form ref={ref} action={action}>
            <TextInput
                name="email"
                label="Invite by email"
                placeholder="Enter email"
                className="w-100"
                required
                isInvalid={error !== undefined}
                invalidFeedback={error}
                more={
                    <SubmitButton
                        className="btn btn-primary"
                        onClick={() => {}}
                        style={{ minWidth: "100px" }}
                        disabled={!self?.pACL || pending}
                    >
                        Invite
                    </SubmitButton>
                }
            />
        </form>
    );
}

/* -------------------------------------------------------------------------- */
/*                         adding a group to the book                         */
/* -------------------------------------------------------------------------- */

function GroupAddForm({ book_id }: { book_id: number }) {
    // Get groups of user
    const { groups, isLoading, error: errorGroups } = useGroups();
    const { collaborators, addCollaborator, self } = useCollaboratorContext();

    const options = useMemo(() => {
        const options = groups
            ?.filter((group) => {
                //Filter all that are already collaborators
                return !collaborators.find(
                    (c) => c.type === "group" && c.id === group.id,
                );
            })
            .map((group) => {
                // Disable if user is not owner or moderator
                return {
                    label: group.name,
                    value: group.id!.toString(),
                    disabled:
                        group.members.find(
                            (m) =>
                                (m.self && m.role === "owner") ||
                                m.role === "moderator",
                        ) === undefined,
                };
            });
        options?.unshift({ label: "Select group", value: "", disabled: false });
        return options;
    }, [collaborators, groups]);

    const [invalidFeedback, setInvalidFeedback] = useState<string>();

    async function action(formData: FormData) {
        const group_id = formData.get("group");
        if (group_id === "") {
            setInvalidFeedback("Please select a group");
        }

        const res = await fetch(
            `/api/books/${book_id}/collaborators/add_group`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ group_id }),
            },
        );

        if (!res.ok) {
            const data = await res.json();
            setInvalidFeedback(data.details);
            return;
        }

        setInvalidFeedback(undefined);
        addCollaborator(await res.json());
    }

    return (
        <form className="placeholder-glow" action={action}>
            <FormError>{errorGroups?.message}</FormError>
            <div
                className="placeholder col-12 btn justify-content-center align-items-center"
                style={{
                    height: "3.5rem",
                    display: isLoading ? "flex" : "none",
                }}
            >
                Loading groups...
            </div>

            {!isLoading && (
                <OptionInput
                    options={options ?? []}
                    label="Group"
                    name="group"
                    type="select"
                    isInvalid={invalidFeedback !== undefined}
                    invalidFeedback={invalidFeedback}
                    more={
                        <SubmitButton
                            className="btn btn-primary"
                            style={{ minWidth: "100px" }}
                            disabled={!self?.pACL}
                        >
                            Add
                        </SubmitButton>
                    }
                />
            )}
        </form>
    );
}

/* -------------------------------------------------------------------------- */
/*                               sharable links                               */
/* -------------------------------------------------------------------------- */

function SharableLinkList({ book_id }: { book_id: number }) {
    const { self } = useCollaboratorContext();

    const { ui_tokens, isLoading, deleteToken } = useTokens(book_id);

    return (
        <div>
            <table className={styles.table}>
                <colgroup>
                    <col className={styles.col_copy} />
                    <col />
                    <col className={styles.col_date} />
                    <col className={styles.col_remove} />
                </colgroup>
                <thead>
                    <tr>
                        <th></th>
                        <th>Description</th>
                        <th>Expires at</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr className="w-100 placeholder-glow" hidden={!isLoading}>
                        <td
                            colSpan={4}
                            className="placeholder"
                            style={{ display: "table-cell" }}
                        >
                            {isLoading ? "Loading..." : "No links"}
                        </td>
                    </tr>

                    {
                        // Empty row if no pending invites
                        !ui_tokens?.length && !isLoading && (
                            <tr>
                                <td colSpan={4}>No links</td>
                            </tr>
                        )
                    }

                    {ui_tokens?.map((token, i) => (
                        <ShareableLinkRow
                            key={i}
                            token={token}
                            deleteToken={() => deleteToken(token.id!)}
                            disabledRemove={!self?.pACL}
                            book_id={book_id}
                        />
                    ))}
                </tbody>
            </table>
        </div>
    );
}

function ShareableLinkRow({
    book_id,
    token,
    deleteToken,
    disabledRemove,
}: {
    book_id: number;
    token: UITokenData;
    disabledRemove?: boolean;
    deleteToken: () => void;
}) {
    const expire_at = useExpiryDate(token.expires_at!);
    const [copied, setCopied] = useState(false);

    return (
        <tr>
            <td className="text-center">
                <Button
                    variant="link"
                    className="p-0"
                    aria-label="Copy link"
                    onClick={() => {
                        const url = new URL(
                            `/shared/${book_id}/${encodeURIComponent(token.hashed_token)}`,
                            window.location.href,
                        );
                        navigator.clipboard.writeText(url.toString());
                        setCopied(true);
                        setTimeout(() => setCopied(false), 5000);
                    }}
                    style={{
                        scale: 1.05,
                        cursor: "copy",
                    }}
                >
                    {copied ? <TbClipboardCheck /> : <TbClipboard />}
                </Button>
            </td>
            <td>{token.description}</td>
            <td>{expire_at}</td>
            <td>
                <Button
                    variant="link"
                    className="p-0"
                    aria-label="Remove invite"
                    onClick={deleteToken}
                    disabled={disabledRemove}
                >
                    <BsTrash3 />
                </Button>
            </td>
        </tr>
    );
}

function SharableLinkAddForm({ book_id }: { book_id: number }) {
    const { self } = useCollaboratorContext();

    const { createToken } = useTokens(book_id);
    const [error, setError] = useState<string>();
    async function action(formData: FormData) {
        const description = formData.get("description") as string;
        const expires_at = formData.get("expires_at") as string;

        try {
            await createToken(
                "ui",
                description === "" ? undefined : description,
                expires_at ? new Date(expires_at) : undefined,
            );
        } catch (e) {
            if (e instanceof Error) {
                setError(e.message);
            }
        }
    }
    const tzoffset = new Date().getTimezoneOffset() * 60000;
    const defaultExpiresAt = new Date(
        Date.now() - tzoffset + 1000 * 60 * 60 * 24,
    );
    defaultExpiresAt.setMilliseconds(0);
    defaultExpiresAt.setSeconds(0);

    return (
        <form action={action}>
            <FormError>{error}</FormError>
            <TextInput
                name="description"
                label="Description"
                placeholder="Enter description"
                className="w-100"
                more={
                    <>
                        <DateTimeInput
                            name="expires_at"
                            label="Expires at (optional)"
                            defaultValue={defaultExpiresAt
                                .toISOString()
                                .slice(0, -1)}
                            style={{ width: "100%", borderRadius: "0" }}
                        />
                        <SubmitButton
                            className="btn btn-primary"
                            style={{ minWidth: "100px" }}
                            disabled={!self?.pACL}
                        >
                            Create
                        </SubmitButton>
                    </>
                }
            />
        </form>
    );
}

/* -------------------------------------------------------------------------- */
/*                                  ownership                                 */
/* -------------------------------------------------------------------------- */

function TransferOwnerForm() {
    const { collaborators, owner, transferOwnership } =
        useCollaboratorContext();
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [selected, setSelected] = useState<ID | undefined>(undefined);

    useEffect(() => {
        if (owner) {
            setSelected(owner.id);
        }
    }, [owner]);

    return (
        <>
            <Button
                variant="outline-dark"
                onClick={handleShow}
                disabled={!(owner as CollaboratorUserData)?.self}
            >
                {!(owner as CollaboratorUserData)?.self
                    ? "Only the owner can transfer ownership"
                    : "Transfer ownership"}
            </Button>
            <Modal
                show={show}
                onHide={handleClose}
                centered
                fullscreen="md-down"
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Transfer ownership</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <div className="d-flex gap-2 justify-content-between pb-2">
                        <div>Current owner:</div>
                        <div>
                            {(owner as CollaboratorUserData)?.email ||
                                (owner as CollaboratorGroupData)?.name}
                        </div>
                    </div>
                    <DropdownInput
                        label="New owner"
                        name="owner"
                        value={selected}
                        options={collaborators
                            .filter((c) => c.type === "user")
                            .map((c) => ({
                                eventKey: c.id,
                                label: (c as CollaboratorUserData).email,
                            }))}
                        onSelect={(id) => {
                            // Transfer ownership
                            setSelected(Number(id));
                        }}
                    />
                </Modal.Body>
                <Modal.Footer>
                    <ButtonWithConfirm
                        onConfirmed={() => {
                            transferOwnership(
                                collaborators.find((c) => c.id === selected)!,
                            );
                            handleClose();
                        }}
                        confirmFeedback={"This is not reversible! You sure?"}
                        timeout={5000}
                    >
                        Transfer ownership
                    </ButtonWithConfirm>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
