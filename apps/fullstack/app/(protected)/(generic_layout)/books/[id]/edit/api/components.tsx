"use client";
import { PostTokenResponse } from "app/api/books/[id]/(edit)/tokens/route";
import { useDate, useExpiryDate } from "lib/hooks/useDate";
import useTokens from "lib/hooks/useTokens";
import { useState } from "react";
import {
    Button,
    Modal,
    ToggleButton,
    ToggleButtonGroup,
} from "react-bootstrap";
import { TbTrash } from "react-icons/tb";

import { APITokenData } from "@snip/database/types";

import {
    CollaboratorContextProvider,
    useCollaboratorContext,
} from "../collaborators/context";

import { ButtonWithConfirm, SubmitButton } from "components/form/buttons";
import { DateTimeInput } from "components/form/inputs/other";
import { TextInput } from "components/form/inputs/text";

import styles from "../collaborators/collaborators.module.scss";

export function TokenFormAndList({ book_id }: { book_id: number }) {
    return (
        <CollaboratorContextProvider book_id={book_id}>
            <CreateTokenForm book_id={book_id} />
            <TokenList book_id={book_id} />
        </CollaboratorContextProvider>
    );
}

function CreateTokenForm({ book_id }: { book_id: number }) {
    const { self } = useCollaboratorContext();
    const { createToken } = useTokens(book_id);
    const [error, setError] = useState<string>();
    const [show, setShow] = useState(false);
    const [returnToken, setReturnToken] = useState<PostTokenResponse | null>(
        null,
    );

    async function action(formData: FormData) {
        const description = formData.get("description") as string;
        const expires_at = formData.get("expires_at") as string;

        try {
            const res = await createToken(
                "api",
                description,
                expires_at ? new Date(expires_at) : undefined,
            );
            console.log(res);
            setReturnToken(res);
            setShow(true);
            setError(undefined);
        } catch (error) {
            if (error instanceof Error) {
                setError(error.message);
            }
            console.error(error);
        }
    }

    const tzoffset = new Date().getTimezoneOffset() * 60000;
    const defaultExpiresAt = new Date(
        Date.now() - tzoffset + 1000 * 60 * 60 * 24 * 365,
    );
    defaultExpiresAt.setMilliseconds(0);
    defaultExpiresAt.setSeconds(0);

    return (
        <>
            <form className="placeholder-glow" action={action}>
                <TextInput
                    name="description"
                    label="Description"
                    isInvalid={error !== undefined}
                    invalidFeedback={error}
                    more={
                        <>
                            <DateTimeInput
                                name="expires_at"
                                label="Expires at (optional)"
                                defaultValue={defaultExpiresAt
                                    .toISOString()
                                    .slice(0, -1)}
                                style={{ width: "100%", borderRadius: "0" }}
                            />
                            <SubmitButton
                                className="btn btn-primary"
                                style={{ minWidth: "100px" }}
                                disabled={!self?.pACL}
                            >
                                Create
                            </SubmitButton>
                        </>
                    }
                />
            </form>
            <TokenCreateModal
                show={show}
                handleClose={() => setShow(false)}
                token={returnToken}
            />
        </>
    );
}

function TokenCreateModal({
    show,
    handleClose,
    token,
}: {
    show: boolean;
    handleClose: () => void;
    token: PostTokenResponse | null;
}) {
    return (
        <Modal show={show} onHide={handleClose}>
            <Modal.Header>
                <Modal.Title>API Token created!</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>
                    Your API token has been created. Please copy it and store it
                    in a safe place. You will not be able to see it again and we
                    do not store it.
                </p>
                <TokenCodeBox token={token!} />
            </Modal.Body>
            <Modal.Footer>
                <ButtonWithConfirm
                    variant="secondary"
                    onConfirmed={handleClose}
                >
                    I have copied the token and stored it in a safe place!
                </ButtonWithConfirm>
            </Modal.Footer>
        </Modal>
    );
}

function TokenList({ book_id }: { book_id: number }) {
    const { api_tokens, deleteToken } = useTokens(book_id);
    const { self } = useCollaboratorContext();

    return (
        <div>
            <table className={styles.table}>
                <colgroup>
                    <col />
                    <col className={styles.col_date} />
                    <col className={styles.col_date} />
                    <col className={styles.col_remove} />
                </colgroup>
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Created at</th>
                        <th>Expires at</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {api_tokens?.length === 0 && (
                        <tr>
                            <td colSpan={4}>No API tokens found</td>
                        </tr>
                    )}
                    {api_tokens?.map((token) => (
                        <TokenRow
                            key={token.id}
                            token={token}
                            deleteToken={() => {
                                token.id && deleteToken(token.id);
                            }}
                            disabledRemove={!self?.pACL}
                        />
                    ))}
                </tbody>
            </table>
        </div>
    );
}

function TokenRow({
    token,
    deleteToken,
    disabledRemove,
}: {
    token: APITokenData;
    disabledRemove?: boolean;
    deleteToken: () => void;
}) {
    const expire_at = useExpiryDate(token.expires_at!);
    const created_at = useDate(token.created_at, "full");
    return (
        <tr>
            <td>{token.description}</td>
            <td>{created_at}</td>
            <td>{expire_at}</td>
            <td>
                <Button
                    variant="link"
                    className="p-0"
                    aria-label="Remove invite"
                    onClick={deleteToken}
                    disabled={disabledRemove}
                >
                    <TbTrash />
                </Button>
            </td>
        </tr>
    );
}

function TokenCodeBox({ token }: { token: PostTokenResponse }) {
    const [type, setType] = useState<"raw" | "ini">("ini");

    let codeString = "";
    let label = "";
    if (type === "raw") {
        codeString = token.token || "";
        label = "Token as string for use in headers or with raw requests.";
    } else if (type === "ini") {
        codeString = `[${token.tokenData.description}]\ntoken=${token.token}\nbook_id=${token.tokenData.book_id}`;
        label = "Token in INI format for use with the snip-python library.";
    }
    return (
        <>
            <div className="d-flex justify-content-between align-items-center mb-1">
                <ToggleButtonGroup
                    type="radio"
                    defaultValue={["raw", "ini"]}
                    name="python_toggle"
                    value={type}
                >
                    <ToggleButton
                        id="tbg-radio-1"
                        value={"raw"}
                        variant="outline-primary"
                        onChange={() => setType("raw")}
                    >
                        Raw
                    </ToggleButton>
                    <ToggleButton
                        id="tbg-radio-2"
                        value={"ini"}
                        variant="outline-primary"
                        onClick={() => setType("ini")}
                    >
                        INI
                    </ToggleButton>
                </ToggleButtonGroup>
                <Button
                    variant="outline-secondary"
                    onClick={() => navigator.clipboard.writeText(codeString)}
                >
                    Copy
                </Button>
            </div>
            <label
                style={{
                    fontSize: "0.8rem",
                    color: "gray",
                }}
            >
                {label}
            </label>
            <div className="border  flex-wrap">
                <pre className="m-0 p-1" style={{ whiteSpace: "pre-wrap" }}>
                    <code>{codeString}</code>
                </pre>
            </div>
        </>
    );
}
