import type { Metadata } from "next";

import service from "@snip/database";

import { BookInfo } from "./bookInfo";

export const revalidate = 360; // revalidate the data at most every hour

export default async function EditBook({
    params: { id },
}: {
    params: { id: string };
}) {
    //Get book data
    const book = await service.book.getById(parseInt(id));

    return (
        <>
            <BookInfo book={book} />
        </>
    );
}

export const metadata: Metadata = {
    title: "Edit",
};
