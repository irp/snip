import type { Metadata } from "next";

import { BackgroundTypeEditor } from "./backgroundTypeEditor";

export default function Page({ params: { id } }: { params: { id: string } }) {
    const b_id = parseInt(id);
    if (!b_id) {
        return <>Invalid page params!</>;
    }

    return (
        <>
            <BackgroundTypeEditor book_id={b_id}></BackgroundTypeEditor>
        </>
    );
}

export const metadata: Metadata = {
    title: "Other",
};
