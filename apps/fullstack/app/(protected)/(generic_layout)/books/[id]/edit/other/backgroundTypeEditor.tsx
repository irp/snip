"use client";
import Loading from "app/(unprotected)/loading";
import { fetcher } from "lib/fetcher";
import { useCallback, useEffect, useRef } from "react";
import useSWR from "swr";

import { BackgroundTypeData, ID } from "@snip/database/types";
import { render_background_by_id } from "@snip/render/backgrounds/index";

import { FormError } from "components/form/alert";
import { DropdownInput } from "components/form/inputs/option";

import styles from "./background.module.scss";

export function BackgroundTypeEditor({ book_id }: { book_id: number }) {
    return (
        <div className={styles.container}>
            <h2>Background Type</h2>
            <p>
                Do you miss the good old checkered notebooks? Or do you prefer a
                more modern look? Don&lsquo;t worry, you can change the default
                background type of your book at any time!
            </p>
            <p>
                Changes to the background type on page level will override the
                default background type!
            </p>
            <BackgroundTypeSelector book_id={book_id} />
        </div>
    );
}

function BackgroundTypeSelector({ book_id }: { book_id: number }) {
    const {
        backgroundTypes,
        selected,
        isLoading,
        error,
        setBookBackgroundType,
    } = useBackgroundType(book_id);

    if (isLoading) {
        return <Loading />;
    }

    return (
        <>
            <FormError>{error}</FormError>
            <DropdownInput
                value={selected?.name}
                label="Background Type"
                options={backgroundTypes!.map((type) => {
                    return {
                        eventKey: type.name,
                        label:
                            type.name +
                            (selected?.name == type.name ? " (selected)" : ""),
                    };
                })}
                onSelect={(d) => {
                    setBookBackgroundType(
                        backgroundTypes!.find((type) => type.name === d)!.id,
                        true,
                    );
                }}
                name="background_type"
            />
            <div className={styles.wrapper}>
                <p className={styles.description}>{selected?.description}</p>
                <BackgroundPreview type={selected} />
            </div>
        </>
    );
}

export function useBackgroundType(book_id: number) {
    const { data, error, mutate } = useSWR<{
        backgroundTypes: BackgroundTypeData[];
        selectedID: ID;
    }>(`/api/books/${book_id}/background`, fetcher);

    return {
        backgroundTypes: data?.backgroundTypes,
        selected: data?.backgroundTypes.find(
            (type) => type.id === data.selectedID,
        ),
        isLoading: !error && !data,
        error,
        setBookBackgroundType: async (
            background_type_id: number,
            server_update = true,
        ) => {
            const optimistic = {
                backgroundTypes: data!.backgroundTypes,
                selectedID: background_type_id,
            };

            await mutate(
                async () => {
                    if (server_update) {
                        const res = await fetch(
                            `/api/books/${book_id}/background`,
                            {
                                method: "POST",
                                body: JSON.stringify({ background_type_id }),
                            },
                        );
                        if (!res.ok) {
                            throw new Error((await res.json()).details);
                        }
                    }
                    return optimistic;
                },
                {
                    optimisticData: optimistic,
                    rollbackOnError: true,
                    revalidate: false,
                },
            );

            return;
        },
    };
}

function BackgroundPreview({ type }: { type?: BackgroundTypeData | null }) {
    // Shows the background as preview in a canvas
    const containerRef = useRef<HTMLDivElement | null>(null);
    const canvasRef = useRef<HTMLCanvasElement | null>(null);

    const draw = useCallback(() => {
        if (!canvasRef.current || !containerRef.current || !type) return;
        // Check if size updated
        canvasRef.current.width = containerRef.current.clientWidth;
        canvasRef.current.height = containerRef.current.clientHeight;

        // Redraw
        const ctx = canvasRef.current.getContext("2d")!;
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        render_background_by_id(type.id, ctx, {
            width: 1400,
            height: canvasRef.current.height,
        });
    }, [type]);

    // Draw loop on width or height changes
    useEffect(() => {
        if (containerRef.current) {
            const resizeObserver = new ResizeObserver(draw);
            resizeObserver.observe(containerRef.current);

            return () => {
                resizeObserver.disconnect();
            };
        }
    }, [containerRef, draw]);

    useEffect(() => {
        //Render canvas
        if (canvasRef) {
            draw();
        }
    }, [draw, canvasRef]);

    return (
        <div ref={containerRef} className={styles.preview + " m-3"}>
            <canvas ref={canvasRef} />
        </div>
    );
}
