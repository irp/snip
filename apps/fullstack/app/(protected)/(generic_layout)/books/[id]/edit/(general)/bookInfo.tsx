"use client";

import { useDate } from "lib/hooks/useDate";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useRef, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import FormCheck from "react-bootstrap/FormCheck";
import { useFormState } from "react-dom";
import useSWR from "swr";

import { isDate } from "@snip/common/dates";
import { BookDataResolved, PageDataRet } from "@snip/database/types";

import { updateBookAction } from "./action";

import { PreviewSinglePage } from "components/editor/sidebar/previews/previewPages";
import { FormError } from "components/form/alert";
import { SubmitButton } from "components/form/buttons";
import { LongTextInput, TextInput } from "components/form/inputs/text";
import { Item } from "components/form/items";

import styles from "./bookinfo.module.scss";

export function BookInfo({ book }: { book: BookDataResolved }) {
    return (
        <>
            <h2>General book settings</h2>
            <div className={styles.contentNew}>
                <Item
                    title="Front page"
                    description="The front page of the book. This is shown
                in previews e.g. on the bookshelf."
                    className={styles.grid_1_3}
                >
                    <FrontPage book={book} />
                </Item>
                <Item title="Title" description="Edit the title of this book.">
                    <TitleEditor book={book} />
                </Item>
                <Item
                    title="Comment"
                    description="Add a comment to this book. This is not required but might help you remember the purpose of this book."
                >
                    <CommentEditor book={book} />
                </Item>
                <Item
                    title="Finished"
                    description="Mark this book as finished. This will disable all editing features. This is reversible at any time."
                >
                    <Finished book={book} />
                </Item>
            </div>
        </>
    );
}

/* -------------------------------------------------------------------------- */
/*                                  font page                                 */
/* -------------------------------------------------------------------------- */

function FrontPage({ book }: { book: BookDataResolved }) {
    const [state, formAction] = useFormState(updateBookAction, {
        bookData: book,
    });
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <FormError>{state.message}</FormError>
            <Link href={`/books/${book.id}`}>
                <div className={styles.frontpage}>
                    <div>
                        <Image
                            src={state.bookData.id.toString()}
                            alt="No cover page set!"
                            width={240}
                            height={240 * 1.41}
                            loader={({ src, width }) => {
                                //we ignore the width and load a high res preview
                                return `/render/book_preview/${src}.jpeg?w=750&date=${state.bookData.last_updated?.valueOf()}`;
                            }}
                        />
                    </div>
                </div>
            </Link>
            <Button variant="outline-dark" onClick={handleShow}>
                Change front page
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                centered
                size="xl"
                fullscreen="md-down"
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Change front page</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Item
                        title="Front page"
                        description="The front page of the book. This is shown in previews e.g. on the bookshelf. The currently selected
                            is shown by the outline"
                    >
                        <ChangeFrontPageForm
                            book={state.bookData}
                            formAction={formAction}
                            closeModal={handleClose}
                        />
                    </Item>
                </Modal.Body>
            </Modal>
        </>
    );
}

function ChangeFrontPageForm({
    book,
    formAction,
    closeModal,
}: {
    book: BookDataResolved;
    formAction: (formData: FormData) => void;
    closeModal: () => void;
}) {
    const ref = useRef<HTMLFormElement>(null);
    const [selectedId, setSelectedId] = useState<number>(book.cover_page_id!);
    const { data: pages, isLoading } = useSWR(
        `/api/books/${book.id}/pages`,
        async (url) => {
            const res = await fetch(url);
            return res.json();
        },
    );

    useEffect(() => {
        if (selectedId && selectedId !== book.cover_page_id) {
            const formData = new FormData();
            formData.set("cover_page_id", selectedId.toString());
            formAction(formData);
            closeModal();
        }
    }, [book.cover_page_id, closeModal, formAction, selectedId]);

    return (
        <form
            ref={ref}
            className={styles.pages}
            action={() => {
                const formData = new FormData();
                formData.set("cover_page_id", selectedId.toString());
                return formAction(formData);
            }}
        >
            {Array.from({ length: book.num_pages ?? 0 }).map((_, i) => (
                <div
                    key={i}
                    className="placeholder"
                    style={{
                        margin: "6px",
                        height: "fit-content",
                    }}
                    hidden={!isLoading}
                >
                    <div
                        style={{
                            width: "150px",
                            height: "calc(150px * 1.41)",
                            border: "2px solid black",
                        }}
                    ></div>
                </div>
            ))}

            {pages?.map((page: PageDataRet) => (
                <PreviewSinglePage
                    key={page.id}
                    page={page}
                    active={selectedId === page.id}
                    onClick={() => {
                        setSelectedId(page.id);
                    }}
                    onlineUsers={[]}
                    hidden={isLoading}
                />
            ))}
        </form>
    );
}

/* -------------------------------------------------------------------------- */
/*                              title and comment                             */
/* -------------------------------------------------------------------------- */

function TitleEditor({ book }: { book: BookDataResolved }) {
    const [state, formAction] = useFormState(updateBookAction, {
        bookData: book,
    });

    return (
        <form action={formAction}>
            <TextInput
                defaultValue={book.title}
                label="Book title"
                name="title"
                invalidFeedback={state.message}
                isInvalid={state.message ? true : false}
                more={
                    <SubmitButton className="btn btn-outline-dark">
                        Update
                    </SubmitButton>
                }
            />
        </form>
    );
}

function CommentEditor({ book }: { book: BookDataResolved }) {
    const [comment, setComment] = useState<string>(book.comment ?? "");
    const [state, formAction] = useFormState(updateBookAction, {
        bookData: book,
    });

    return (
        <form action={formAction} className="d-flex gap-2 flex-column">
            <LongTextInput
                label="Book comment"
                name="comment"
                value={comment}
                onInput={(e: React.ChangeEvent<HTMLInputElement>) =>
                    setComment(e.target.value)
                }
                isInvalid={state.message ? true : false}
                invalidFeedback={state.message}
                more={
                    <SubmitButton className="btn btn-outline-dark">
                        Update
                    </SubmitButton>
                }
            />
        </form>
    );
}

/* -------------------------------------------------------------------------- */
/*                                  finished                                  */
/* -------------------------------------------------------------------------- */

function Finished({ book }: { book: BookDataResolved }) {
    const [check, setCheck] = useState(book.finished ? true : false);
    const [state, formAction] = useFormState(updateBookAction, {
        bookData: book,
    });

    const isDateSet = isDate(state?.bookData?.finished ?? undefined);

    const date = useDate(state?.bookData?.finished ?? undefined);

    return (
        <div className={styles.finished}>
            <form
                action={async (formData) => {
                    formData.set("finished", check ? "false" : "true");
                    return formAction(formData);
                }}
            >
                <FormError>{state.message}</FormError>
                <FormCheck
                    type="switch"
                    label={
                        isDateSet ? "Mark as unfinished" : "Mark as finished"
                    }
                    id="finished-switch"
                    name="finished"
                    value={state.bookData.finished ? "false" : "true"}
                    checked={state.bookData.finished ? true : false}
                    onChange={(e) => {
                        setCheck(e.target.checked);
                        e.currentTarget.form?.requestSubmit();
                    }}
                />
                {
                    // If the book is marked as finished show the date
                    isDateSet ? (
                        <p className={styles.date} hidden={!isDateSet}>
                            This book was marked as finished on:{" "}
                            <code>{date}</code>
                        </p>
                    ) : (
                        <p className={styles.date} hidden={isDateSet}>
                            This book is not marked as finished.
                        </p>
                    )
                }
            </form>
        </div>
    );
}
