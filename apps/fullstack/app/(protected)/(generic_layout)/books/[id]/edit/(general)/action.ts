"use server";

import { errorToMessage } from "lib/actions/errorToMessage";
import { getBookPerms } from "lib/actions/isLoggedIn";
import { z } from "zod";

import service from "@snip/database";
import { pool_data } from "@snip/database/sql.connection";
import { BookDataResolved } from "@snip/database/types";

/** This action is included in forms
 * in the page editor
 */
async function _updateBookAction(
    prevState: { bookData: BookDataResolved; message?: string },
    formData: FormData,
): Promise<{ bookData: BookDataResolved; message?: string }> {
    // Get headers
    const perms = await getBookPerms(prevState.bookData.id);
    if (!perms.pACL) {
        throw new Error("You are not allowed to edit this book");
    }
    const schema = z.object({
        title: z.string().min(3).max(256).optional(),
        comment: z.string().optional(),
        finished: z.string().optional(),
        cover_page_id: z.coerce.number().optional(),
    });
    // Validate form data
    const res = schema.safeParse(Object.fromEntries(formData.entries()));

    if (res.success === false) {
        throw new Error(res.error.errors[0]!.message);
    }
    const { title, comment, finished, cover_page_id } = res.data;

    if (title && title !== prevState.bookData.title) {
        await pool_data.query("UPDATE books SET title = ? WHERE id = ?", [
            title,
            prevState.bookData.id,
        ]);
        prevState.bookData.title = title;
    }

    if (comment && comment !== prevState.bookData.comment) {
        await pool_data.query("UPDATE books SET comment = ? WHERE id = ?", [
            comment,
            prevState.bookData.id,
        ]);
        prevState.bookData.comment = comment;
    }

    if (finished) {
        if (finished === "true") {
            // Update serverside
            prevState.bookData.finished = new Date();
        } else if (finished === "false") {
            prevState.bookData.finished = null;
        } else {
            throw new Error("Invalid finished value");
        }
        try {
            await pool_data.query(
                "UPDATE books SET finished = ? WHERE id = ?",
                [prevState.bookData.finished, prevState.bookData.id],
            );
        } catch (e) {
            console.error(e);
        }
    }

    if (cover_page_id) {
        //Check if page exits and is in the book
        const page = await service.page.getById(cover_page_id);
        if (!page) {
            throw new Error("Page does not exist");
        }
        if (page.book_id !== prevState.bookData.id) {
            throw new Error("Page does not belong to this book");
        }
        await pool_data.query(
            "UPDATE books SET cover_page_id = ? WHERE id = ?",
            [cover_page_id, prevState.bookData.id],
        );
        prevState.bookData.cover_page_id = cover_page_id;
        prevState.bookData.last_updated = new Date();
    }

    return {
        bookData: {
            ...prevState.bookData,
        },
    };
}

export const updateBookAction = errorToMessage(_updateBookAction);
