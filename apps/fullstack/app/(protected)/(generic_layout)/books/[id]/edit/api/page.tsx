import type { Metadata } from "next";
import Link from "next/link";

import { TokenFormAndList } from "./components";

import { Item } from "components/form/items";

export default function Page({ params }: { params: { id: string } }) {
    const book_id = parseInt(params.id);

    return (
        <>
            <h2>API</h2>
            <p>
                We allow you to automate some parts of your workflow by
                providing an API. You may use the token below to authenticate
                your requests to endpoints. The token is unique to this book and
                may be revoked at any time.
            </p>
            <div>
                <Item
                    title="Access token"
                    description={
                        <div>
                            An access token is a unique string that identifies a
                            user and provides access to the API. This token is
                            unique to this book and may be revoked at any time.
                            You may use this token to authenticate your requests
                            to endpoints. For more information, see the{" "}
                            <Link href="/docs/user/api" target="_blank">
                                user guide
                            </Link>{" "}
                            on API usage.
                        </div>
                    }
                >
                    <TokenFormAndList book_id={book_id} />
                </Item>
            </div>
        </>
    );
}

export const metadata: Metadata = {
    title: "API",
};
