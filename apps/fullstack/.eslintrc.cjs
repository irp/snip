module.exports = {
    root: true,
    extends: ["@snip/eslint-config/next"],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        project: true,
    },
};