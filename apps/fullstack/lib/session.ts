import { cookies } from "next/headers";

import { getSession } from "@snip/auth/session";

/** Get session in the route handlers
 * @param req
 * @param res
 */
export async function getSessionAction() {
    return await getSession(cookies());
}
