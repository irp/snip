import "server-only";

import { getSessionAction } from "lib/session";
import { cache } from "react";

import service from "@snip/database";
import {
    ENTITY_USER,
    ID,
    PermissionACLData,
    RESOURCE_BOOK,
} from "@snip/database/types";

export async function isLoggedIn() {
    const session = await getSessionAction();
    if (!session || !session.user) {
        return false;
    }
    return true;
}

export async function getBookPerms(book_id: ID): Promise<PermissionACLData> {
    const session = await getSessionAction();
    if (!session || !session.user) {
        return {
            id: -10,
            pRead: false,
            pWrite: false,
            pACL: false,
            pDelete: false,
            entity_id: -1,
            entity_type: ENTITY_USER,
            resource_id: book_id,
            resource_type: RESOURCE_BOOK,
        };
    }
    const perms = await cache_getByUserAndBookReduced(session.user.id, book_id);
    return perms;
}

const cache_getByUserAndBookReduced = cache(
    async (user_id: ID, book_id: ID) => {
        return service.permission.getByUserAndBookReduced(user_id, book_id);
    },
);
