/* eslint-disable @typescript-eslint/no-explicit-any */
export function errorToMessage<
    I extends Array<unknown>,
    O extends { message?: string; [key: string]: any },
>(fn: (...args: I) => Promise<O>) {
    const fn_new = async (...args: I) => {
        try {
            return await fn(...args);
        } catch (e) {
            if (e instanceof Error) {
                return {
                    ...(args[0] as O),
                    message: e.message,
                } as O;
            }
        }
    };
    return fn_new! as (...args: I) => Promise<O>;
}

export function errorToErrorField<
    I extends Array<unknown>,
    O extends { error?: string; [key: string]: any },
>(fn: (...args: I) => Promise<O>) {
    const fn_new = async (...args: I) => {
        try {
            return await fn(...args);
        } catch (e) {
            if (e instanceof Error) {
                return {
                    ...(args[0] as O),
                    error: e.message,
                } as O;
            }
        }
    };
    return fn_new!;
}

export type ErrorDict = {
    error: {
        name: string;
        details: string | any;
    };
};

export function errorToDict<
    I extends Array<unknown>,
    O extends {
        [key: string]: any;
    },
>(fn: (...args: I) => Promise<O | ErrorDict>) {
    const fn_new = async (...args: I) => {
        try {
            return await fn(...args);
        } catch (e) {
            if (e instanceof Error) {
                return {
                    ...(args[0] as O),
                    error: {
                        name: e.name,
                        details: e.message,
                    },
                } as O;
            }
        }
    };
    return fn_new as (...args: I) => Promise<O | ErrorDict>;
}
