"use server";
import { getSessionAction } from "lib/session";
import { redirect, RedirectType } from "next/navigation";
export async function logout() {
    const session = await getSessionAction();
    session.destroy();
    return redirect("/", RedirectType.replace);
}
