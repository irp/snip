import { ArkErrors, type } from "arktype";

export function validateArk<T>(
    vType: T | ArkErrors,
): [T, null] | [null, ArkErrors] {
    if (vType instanceof type.errors) {
        return [null, vType];
    }
    return [vType, null];
}
