import { FONT_URLS } from "@snip/database/fonts/internal";
import { HEXColor, NumberRange, Font } from "../types";
import { validateArk } from "../validate";

describe("HEXColor validation", () => {
    it("should validate correct 6-digit HEX colors", () => {
        const valid_colors = ["#FFFFFF", "#000000", "#1a2b3c"];

        for (const color of valid_colors) {
            const [type, error] = validateArk(HEXColor(color));
            expect(error).toBeNull();
        }
    });

    it("should validate correct 3-digit HEX colors", () => {
        const valid_colors = ["#FFF", "#000", "#1a2"];

        for (const color of valid_colors) {
            const [type, error] = validateArk(HEXColor(color));
            expect(error).toBeNull();
        }
    });

    it("should reject invalid HEX colors", () => {
        const invalid_colors = ["#ZZZ", "123456", "#12345"];
        for (const color of invalid_colors) {
            const [type, error] = validateArk(HEXColor(color));
            expect(error).not.toBeNull();
        }
    });
});

describe("NumberRange validation", () => {
    it("should validate numbers within the specified range", () => {
        const AgeRange = NumberRange(18, 99);
        const validNumbers = [18, 25, 99];

        for (const num of validNumbers) {
            const [type, error] = validateArk(AgeRange(num));
            expect(error).toBeNull();
        }
    });

    it("should reject numbers outside the specified range", () => {
        const AgeRange = NumberRange(18, 99);
        const invalidNumbers = [17, 100, -1];

        for (const num of invalidNumbers) {
            const [type, error] = validateArk(AgeRange(num));
            expect(error).not.toBeNull();
        }
    });
});

describe("Font validation", () => {
    it("should reject invalid fonts", () => {
        const FontVal = Font("doesn't exist");
        const [type, error] = validateArk(FontVal);
        expect(error).not.toBeNull();
    });

    it("should accept valid fonts", () => {
        const validFont = FONT_URLS[0]![1]!;
        const FontVal = Font(validFont);

        const [type, error] = validateArk(FontVal);
        expect(error).toBeNull();
        expect(type).toBe(validFont);
    });

    it("should handle empty input", () => {
        const FontVal = Font("");

        const [type, error] = validateArk(FontVal);
        expect(error).not.toBeNull();
    });
});
