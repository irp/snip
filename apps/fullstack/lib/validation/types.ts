/** For runtime validation we mostly use arktype
 * This file is for defining custom types for validation
 * and an error parsing function to convert errors
 * to a common format for the frontend.
 *
 */
import { type } from "arktype";

import { FONT_URLS } from "@snip/database/fonts/internal";

/**
 * Represents a HEX color code in the format `#RRGGBB` or `#RGB`.
 *
 * This type ensures that the string adheres to the standard HEX color format:
 * - Starts with a `#` symbol.
 * - Followed by either 6 characters (for `#RRGGBB`) or 3 characters (for `#RGB`).
 * - Characters must be hexadecimal digits (0-9, A-F, case-insensitive).
 *
 * Examples of valid HEX colors:
 * - `#FFFFFF` (white)
 * - `#000` (black)
 * - `#1a2b3c`
 *
 * Examples of invalid HEX colors:
 * - `#ZZZ` (invalid characters)
 * - `123456` (missing `#`)
 * - `#12345` (incorrect length)
 */
export const HEXColor = type(/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/).describe(
    "color code matching the format #RRGGBB or #RGB",
);

/**
 * Creates a runtime validation type for a number within a specified range.
 *
 * This function generates a type that ensures a number is within the inclusive range
 * defined by `min` and `max`. It uses arktype's runtime validation to enforce the range.
 *
 * @param min - The minimum value (inclusive) of the range.
 * @param max - The maximum value (inclusive) of the range.
 * @returns A runtime validation type that checks if a number is within the specified range.
 *
 * @example
 * ```typescript
 * const AgeRange = NumberRange(18, 99);
 * const result = AgeRange(25); // Valid, as 25 is between 18 and 99.
 * const invalidResult = AgeRange(15); // Invalid, as 15 is less than 18.
 * ```
 */
export const NumberRange = (min: number, max: number) => {
    return type(`${min} <= number <= ${max}`).describe(
        `number between ${min} and ${max} inclusive`,
    );
};

/**
 * Represents a runtime validation type for a font, ensuring it is one of the predefined fonts.
 *
 * This type uses arktype's `type.enumerated` to create a runtime validation type that checks
 * if a given value is in the allowed fonts list.
 *
 * @example
 * ```typescript
 * const validFont = Font("Arial"); // Valid, if "Arial" is in the FONT_URLS array.
 * const invalidFont = Font("UnknownFont"); // Invalid, if "UnknownFont" is not in the FONT_URLS array.
 * ```
 */

let _Font;
for (const font of FONT_URLS) {
    if (!_Font) {
        _Font = type(`'${font[1]}'`);
    } else {
        _Font = _Font.or(type(`'${font[1]}'`));
    }
}

export const Font = _Font!;
