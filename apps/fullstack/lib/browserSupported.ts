export function get_browser(): { name: string; version: string } {
    const ua = navigator.userAgent;
    let tem,
        M =
            ua.match(
                /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i,
            ) || [];
    if (/trident/i.test(M[1]!)) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return { name: "IE", version: tem[1] || "" };
    }
    if (M[1] === "Chrome") {
        tem = ua.match(/\bOPR\/(\d+)/);
        if (tem != null) {
            return { name: "Opera", version: tem[1]! };
        }
    }
    if (window.navigator.userAgent.indexOf("Edge") > -1) {
        // eslint-disable-next-line no-useless-escape
        tem = ua.match(/\Edge\/(\d+)/);
        if (tem != null) {
            return { name: "Edge", version: tem[1]! };
        }
    }
    M = M[2] ? [M[1]!, M[2]!] : [navigator.appName, navigator.appVersion, "-?"];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]!);
    }
    return {
        name: M[0]!,
        version: +M[1]! + "",
    };
}

export function isSupported(browser: { name: string; version: string }) {
    let supported = false;
    const versionNumber = parseInt(browser.version, 10); // Convert version to a number
    if (browser.name === "Chrome" && versionNumber >= 69) {
        supported = true;
    } else if (browser.name === "Edge" && versionNumber >= 79) {
        supported = true;
    } else if (browser.name === "Firefox" && versionNumber >= 105) {
        supported = true;
    } else if (browser.name === "Opera" && versionNumber >= 51) {
        supported = true;
    } else if (browser.name === "Safari" && versionNumber >= 16) {
        supported = true;
    }
    return supported;
}
