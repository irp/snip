import { NextResponse } from "next/server";

/**
 * Converts an unknown error into a standardized response object.
 *
 * @param title - The title or summary of the error.
 * @param e - The unknown error object to be parsed.
 * @param status - The HTTP status code to be returned with the response.
 * @returns A `NextResponse` object containing the error details in JSON format.
 */
export function unknownErrorToResponse(
    title: string,
    e: unknown,
    status: number,
): NextResponse {
    let message = "An unknown error occurred";
    if (e instanceof Error) {
        message = e.message;
    }
    return NextResponse.json(
        { error: title, details: message },
        {
            status,
        },
    );
}
