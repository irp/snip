"use client";
import { useRouter, useSearchParams } from "next/navigation";
import { useEffect } from "react";
import { useState } from "react";

/** Redirect to the page the user was on before logging in
 * this reads the query parameter redirect and redirects
 */
export const useRedirect = (default_location: string, isLoggedIn: boolean) => {
    const searchParams = useSearchParams();
    const router = useRouter();
    const [pending, setPending] = useState(false);

    useEffect(() => {
        if (isLoggedIn && searchParams) {
            console.log("redirecting to", searchParams.get("redirect"));
            if (searchParams.has("redirect")) {
                setPending(true);
                const redirect = searchParams.get("redirect")!.toString();
                router.push(redirect);
            } else {
                setPending(true);
                router.push(default_location);
            }
        }
    }, [default_location, isLoggedIn, router, searchParams]);

    return pending;
};
