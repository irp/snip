import { UIEvent, useRef } from "react";

/**
 * A custom hook to handle context menu events in a mobile-friendly way.
 * It triggers a custom context menu callback after a long press on touch devices
 * or a right-click on desktop devices.
 *
 * @param onContextMenu - The callback function to execute when the context menu is triggered.
 * @param duration - The duration (in milliseconds) to wait before triggering the context menu on touch devices. Default is 500ms.
 * @returns An object containing event handlers to be spread onto the target element.
 *
 * @example
 * const elemProps = useMobileSafeContextMenu((event) => {
 *   console.log("Context menu triggered", event);
 * });
 *
 * return (
 *   <div
 *     {...elemProps}
 *   >
 *     Right-click or long-press me
 *   </div>
 * );
 */
const useMobileSafeContextMenu = (
    onContextMenu: (event: UIEvent) => void,
    duration = 500,
) => {
    const pressTimer = useRef<number | null>(null);

    const start = (event: UIEvent) => {
        pressTimer.current = window.setTimeout(() => {
            onContextMenu(event); // Trigger the context menu callback
        }, duration);
    };

    const stop = () => {
        if (pressTimer.current !== null) {
            window.clearTimeout(pressTimer.current);
        }
    };

    return {
        onTouchStart: start,
        onTouchEnd: stop,
        onTouchCancel: stop,
        onContextMenu: (event: UIEvent) => {
            event.preventDefault(); // Prevent default context menu
            onContextMenu(event);
        },
    };
};

export default useMobileSafeContextMenu;
