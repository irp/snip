"use client";

import { useEffect, useState } from "react";

import { DateFormat, localDateString } from "@snip/common/dates";

/**
 * Custom hook to format a date and return the formatted date string.
 *
 * @param {Date} date - The date to be formatted.
 * @param {string} format - The format string for the date. if "short" the date returns local time with date removed for today, else returns local time with date. If "long" the date returns local time with date and time.
 * @returns {string} The formatted date string.
 */
export const useDate = (
    date: string | Date | undefined,
    format: DateFormat = "compact",
) => {
    const [formattedDate, setFormattedDate] = useState<string>();

    useEffect(() => {
        setFormattedDate(date ? localDateString(new Date(date), format) : "-");
    }, [date, format]);

    return formattedDate;
};

/**
 * Custom hook to format and display an expiry date. This returns "expired" if the date is in the past.
 *
 * @param date - The date to be formatted.
 * @param format - The format of the date. Defaults to "compact".
 * @returns The formatted expiry date.
 */
export const useExpiryDate = (
    date: Date | undefined,
    format: DateFormat = "compact",
) => {
    const [formattedDate, setFormattedDate] = useState<string>();

    useEffect(() => {
        setFormattedDate(
            date
                ? new Date(date) < new Date()
                    ? "Expired"
                    : localDateString(new Date(date), format)
                : "-",
        );
    }, [date, format]);

    return formattedDate;
};
