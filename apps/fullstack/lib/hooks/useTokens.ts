import {
    type GetTokensResponse,
    PostTokenResponse,
} from "app/api/books/[id]/(edit)/tokens/route";
import { fetcher } from "lib/fetcher";
import useSWR from "swr";

import { APITokenData, TokenData, UITokenData } from "@snip/database/types";

/** Similar to useUser
 * a convenient hook to fetch, delete and add tokens
 */
export default function useTokens(book_id: number) {
    const { data, error, mutate } = useSWR<GetTokensResponse>(
        `/api/books/${book_id}/tokens`,
        async (url: string) => {
            const data = (await fetcher(url)) as GetTokensResponse;
            // Parse dates
            data.forEach((token) => {
                token.created_at = new Date(token.created_at!);
                if (token.expires_at) {
                    token.expires_at = new Date(token.expires_at);
                }
            });

            return data;
        },
    );

    function addToken(tokenData: TokenData): void {
        mutate((currentData) => {
            if (!currentData) return;
            return [...currentData, tokenData];
        });
    }

    async function deleteToken(id: number) {
        await fetch(`/api/books/${book_id}/tokens`, {
            method: "DELETE",
            body: JSON.stringify({ id }),
        });
        mutate();
    }

    async function createToken(
        type: "ui" | "api",
        description?: string,
        expires_at?: Date,
    ) {
        const res = await fetch(`/api/books/${book_id}/tokens`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                type,
                description,
                expires_at,
            }),
        });
        if (!res.ok) {
            throw new Error((await res.json()).details);
        }

        const { token, tokenData } = (await res.json()) as PostTokenResponse;
        addToken(tokenData);
        return { token, tokenData };
    }

    return {
        tokens: data,
        ui_tokens: (data?.filter((t) => t.type === "ui") ||
            []) as UITokenData[],
        api_tokens: (data?.filter((t) => t.type === "api") ||
            []) as APITokenData[],
        isLoading: !error && !data,
        error,
        createToken,
        addToken,
        deleteToken,
    };
}
