import { GroupDataWithMembersRet } from "app/api/account/groups/route";
import { fetcher } from "lib/fetcher";
import useSWR from "swr";

import { debounce } from "@snip/common";
import { MemberRole } from "@snip/database/types";

interface GroupsHook {
    groups?: GroupDataWithMembersRet[];
    isLoading: boolean;
    error: Error | null;
    setGroups: (data: GroupDataWithMembersRet[]) => void;
    createGroup: (name: string, description?: string) => Promise<void>;
    deleteGroup: (group_id: number) => Promise<void>;
    leaveGroup: (group_id: number) => Promise<void>;
    updateGroup: (
        group_id: number,
        data: GroupDataWithMembersRet,
    ) => Promise<void>;
}

export default function useGroups(): GroupsHook {
    const {
        data: groups,
        mutate,
        isLoading,
        error,
    } = useSWR<GroupDataWithMembersRet[]>(
        "/api/account/groups",
        async (url) => {
            const data = (await fetcher(
                url,
                undefined,
                "GET",
            )) as GroupDataWithMembersRet[];

            //Parse dates
            data.forEach((group) => {
                group.created = new Date(group.created!);
                group.last_updated = new Date(group.last_updated!);

                group.members.forEach((member) => {
                    member.joined_at = new Date(member.joined_at!);
                });
            });

            return data;
        },
        {
            revalidateIfStale: true,
            revalidateOnFocus: true,
            revalidateOnReconnect: true,
            refreshInterval: 0,
            fallbackData: [],
        },
    );

    /** Creates a group
     * and adds the user as owner
     *
     * this performs a POST request
     * with optimistic updates
     */
    async function createGroup(name: string, description?: string) {
        if (!groups) return;
        const optimistic = [...groups];

        const newGroup = {
            name,
            description: description || null,
            id: -1,
            created: new Date(),
            last_updated: new Date(),
            members: [
                {
                    self: true,
                    role: "owner" as const,
                    joined_at: new Date(),
                    user_id: -1,
                },
            ],
        } as GroupDataWithMembersRet;

        await mutate(
            async () => {
                const requestBody = JSON.stringify({
                    name,
                    description,
                });

                const res = await fetch("/api/account/groups", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: requestBody,
                });
                if (!res.ok) {
                    const data = await res.json();
                    throw new Error(data.details);
                }
                const newGroup = (await res.json()) as GroupDataWithMembersRet;

                //Parse dates
                newGroup.created = new Date(newGroup.created!);
                newGroup.last_updated = new Date(newGroup.last_updated!);
                newGroup.members.forEach((member) => {
                    member.joined_at = new Date(member.joined_at!);
                });

                return [...optimistic, newGroup];
            },
            {
                optimisticData: [...optimistic, newGroup],
                rollbackOnError: true,
                revalidate: false,
            },
        );
    }

    /** Deletes a group
     *
     * this performs a DELETE request
     * with optimistic updates
     */
    async function deleteGroup(group_id: number) {
        if (!groups) return;
        const optimistic = groups.filter((group) => group.id !== group_id);

        await mutate(
            async () => {
                const res = await fetch(`/api/account/groups/${group_id}`, {
                    method: "DELETE",
                });
                if (!res.ok) {
                    const data = await res.json();
                    throw new Error(data.details);
                }
                return optimistic;
            },
            {
                optimisticData: optimistic,
                rollbackOnError: true,
                revalidate: false,
            },
        );
    }

    /** Updates a group
     * with the new data
     *
     * this performs a POST request
     * with optimistic updates
     *
     * might throw an error if the request fails
     */
    async function updateGroup(
        group_id: number,
        data: GroupDataWithMembersRet,
    ) {
        if (!groups) return;
        await mutate(
            async () => {
                const newGroup = await new Promise<GroupDataWithMembersRet>(
                    (resolve, reject) => {
                        updateGroupWithDebounce(
                            groups.find((group) => group.id === group_id)!,
                            data,
                            (error) => {
                                reject(error);
                            },
                            resolve,
                        );
                    },
                );

                return groups.map((group) =>
                    group.id === group_id ? newGroup : group,
                );
            },
            {
                optimisticData: groups.map((group) =>
                    group.id === group_id ? data : group,
                ),
                rollbackOnError: true,
                revalidate: false,
            },
        );
    }

    /** Leave a group
     *
     * this only works if the user is not the owner
     *
     */
    async function leaveGroup(group_id: number) {
        if (!groups) return;

        const optimistic = groups.filter((group) => group.id !== group_id);

        await mutate(
            async () => {
                const res = await fetch(
                    `/api/account/groups/${group_id}/leave`,
                    {
                        method: "POST",
                    },
                );
                if (!res.ok) {
                    const data = await res.json();
                    throw new Error(data.details);
                }
                return optimistic;
            },
            {
                optimisticData: optimistic,
                rollbackOnError: true,
                revalidate: false,
            },
        );
    }

    return {
        groups,
        isLoading,
        createGroup,
        deleteGroup,
        updateGroup,
        leaveGroup,
        setGroups: mutate,
        error,
    };
}

let rollback: GroupDataWithMembersRet | null = null;
function updateGroupWithDebounce(
    prev: GroupDataWithMembersRet,
    update: GroupDataWithMembersRet,
    onRollback: (error: string, data: GroupDataWithMembersRet) => void,
    onSucess: (data: GroupDataWithMembersRet) => void,
) {
    if (!rollback) {
        rollback = prev;
    }

    debouncedUpdateGroup(rollback, update, onRollback, onSucess);
}

const debouncedUpdateGroup = debounce(updateGroupRequest, 1000);

async function updateGroupRequest(
    prev: GroupDataWithMembersRet,
    update: GroupDataWithMembersRet,
    onRollback: (error: string, data: GroupDataWithMembersRet | null) => void,
    onSucess: (data: GroupDataWithMembersRet) => void,
) {
    const requestBody: {
        name?: string;
        description?: string;
        updateMemberRole?: {
            user_id: number;
            role: MemberRole;
        }[];
        removeMember?: number[];
        transferOwnership?: number;
    } = {};

    if (prev.name !== update.name) {
        requestBody.name = update.name;
    }
    if (prev.description !== update.description) {
        requestBody.description = update.description!;
    }

    //Check for member role updates
    const memberUpdates = update.members
        .filter(
            (m) =>
                !prev.members.find(
                    (r) => r.user_id === m.user_id && r.role === m.role,
                ),
        )
        .filter((m) => m.role !== "owner");

    if (memberUpdates.length > 0) {
        requestBody.updateMemberRole = memberUpdates.map((m) => ({
            user_id: m.user_id,
            role: m.role,
        }));
    }

    //Check if removed member
    const removedMembers = prev.members.filter(
        (r) => !update.members.find((m) => m.user_id === r.user_id),
    );
    if (removedMembers.length > 0) {
        requestBody.removeMember = removedMembers.map((m) => m.user_id);
    }

    //Check if owner changed
    const oldOwner = prev.members.find((m) => m.role === "owner");
    const newOwner = update.members.find((m) => m.role === "owner");
    if (oldOwner && newOwner && oldOwner.user_id !== newOwner.user_id) {
        requestBody.transferOwnership = newOwner.user_id;
        requestBody.updateMemberRole = requestBody.updateMemberRole?.filter(
            (m) =>
                m.user_id !== newOwner.user_id &&
                m.user_id !== oldOwner.user_id,
        );
    }

    console.log("Request body", requestBody);

    //Check if more than one update
    if (Object.keys(requestBody).length === 0) {
        return update;
    }

    const res = await fetch(`/api/account/groups/${update.id}`, {
        method: "POST",
        body: JSON.stringify(requestBody),
    });

    if (!res.ok) {
        const data = await res.json();
        onRollback(data.details, rollback);
        return;
    }
    rollback = null;
    onSucess(update);
}
