"use client";
import { GroupDataWithMembersRet } from "app/api/account/groups/route";
import { type UserRouteResponseReturn } from "app/api/account/user/route";
import useSWR, { KeyedMutator, SWRConfiguration } from "swr";

import { debounceWithReturn } from "@snip/common";
import {
    LastLoginRet,
    UserConfigDataInsert,
    UserConfigDataRet,
    UserDataRet,
} from "@snip/database/types";

import { fetcher } from "../fetcher";

export interface UserHook {
    user?: UserDataRet;
    groups?: GroupDataWithMembersRet[];
    config?: UserConfigDataRet;
    lastLogins?: LastLoginRet[];

    isLoading: boolean;
    isError: boolean;
    isLoggedIn: boolean;
    isAdmin: boolean;

    updateUserConfig: (
        userConfig: Partial<UserConfigDataInsert> & { id: number },
    ) => Promise<void>;
    mutate: KeyedMutator<UserRouteResponseReturn>;
}

export default function useUser(
    config = true,
    groups = false,
    last_logins = false,
): UserHook {
    const swrConfig: SWRConfiguration = {
        revalidateIfStale: true,
        revalidateOnFocus: true,
        revalidateOnReconnect: true,
        refreshInterval: 0,
        fallbackData: {
            user: userDataFallback,
            userConfig: config && userConfigFallback,
        },
    };

    const { data, mutate, error, isLoading } = useSWR<UserRouteResponseReturn>(
        `/api/account/user?config=${config}&groups=${groups}&last_logins=${last_logins}`,
        async (url) => {
            const data = (await fetcher(url)) as UserRouteResponseReturn;

            //Parse dates in default return
            data.user.created = new Date(data.user.created!);
            data.user.last_updated = new Date(data.user.last_updated!);

            // if config is requested
            if (data.userConfig) {
                data.userConfig.last_updated = new Date(
                    data.userConfig.last_updated,
                );
            }

            // if groups are requested
            if (data.groups) {
                data.groups.forEach((g) => {
                    g.created = new Date(g.created);
                    g.last_updated = new Date(g.last_updated);

                    g.members.forEach((m) => {
                        m.joined_at = new Date(m.joined_at);
                        if (m.user_id === data.user.id) {
                            m.self = true;
                        }
                    });
                });
            }

            // if last logins are requested
            if (data.lastLogins) {
                data.lastLogins.forEach((l) => {
                    l.time = new Date(l.time);
                });
            }

            return data as UserRouteResponseReturn;
        },
        swrConfig,
    );

    const updateServerUserConfigDebounced = debounceWithReturn(
        updateServerUserConfigData,
        500,
    );

    const updateUserConfig = async (
        userConfig: Partial<UserConfigDataInsert> & { id: number },
    ) => {
        if (userConfig) {
            const optimistic = {
                ...data,
            };

            optimistic.userConfig = {
                ...optimistic.userConfig,
                ...userConfig,
            } as UserConfigDataRet;

            // optimistic update
            await mutate(
                async () => {
                    const newConfig =
                        await updateServerUserConfigDebounced(userConfig);

                    optimistic.userConfig = newConfig;

                    return optimistic as UserRouteResponseReturn;
                },
                {
                    optimisticData: optimistic as UserRouteResponseReturn,
                    rollbackOnError: true,
                    populateCache: true,
                    revalidate: false,
                },
            );
        }
    };

    const ret: UserHook = {
        user: data?.user,
        config: data?.userConfig,
        groups: data?.groups,
        lastLogins: data?.lastLogins,

        isLoading,
        isError: error,

        isLoggedIn:
            !isLoading &&
            !error &&
            data?.user.id !== undefined &&
            data?.user.id > 0,
        isAdmin: data?.groups?.some((g) => g.name === "admin") || false,

        updateUserConfig,
        mutate,
    };
    return ret;
}

async function updateServerUserConfigData(
    configUpd: UserConfigDataInsert,
): Promise<UserConfigDataRet> {
    return await fetch("/api/account/config", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(configUpd),
    })
        .then((res) => res.json())
        .then((res) => {
            if (res.error) {
                console.error(res);
                throw new Error(res.error);
            }
            return res;
        });
}

/** THIS API SHOULD NOT BE USED FOR CRITICAL APPLICATION PARTS
 *  AS A RESPONSE CAN BE MANIPULATED BY THE USER EASILY!
 *
 * Only use this API to show or hide certain parts of the UI
 *
 */
export function useIsAdmin() {
    const {
        data: isAdmin,
        mutate: mutateIsAdmin,
        error,
    } = useSWR<boolean>("/api/authentication/isAdmin", fetcher);

    return {
        isAdmin: isAdmin,
        isLoading: !error && !isAdmin,
        isError: error,
        mutateIsAdmin,
    };
}

/* -------------------------------------------------------------------------- */
/*                                fallback data                               */
/* -------------------------------------------------------------------------- */
const userDataFallback: UserDataRet = {
    created: new Date(),
    emails: [],
    emails_verified: [],
    credential_ids: [],
    credential_types: [],
    primary_credential_id: null,
    id: -1,
    last_updated: new Date(),
    credential_provider_ids: [],
};
const userConfigFallback: UserConfigDataRet = {
    id: -1,
    last_updated: new Date(),
    pen_color: "#000000",
    pen_colors: ["#000000", "#ff0000", "#00ff00", "#0000ff"],
    pen_size: 4,
    pen_smoothing: 0.0,
    text_font_id: 1,
    text_font: "Arial",
    text_fontColor: "#000000",
    user_color: "#000000",
    text_fontSize: 12,
    text_lineHeight: 1.25,
    text_lineWrap: 400,
    user_id: -1,
    zoom1: 1,
    zoom2: 1,
};
