"use client";
import { useEffect } from "react";

/**
 * Prevents the mobile browser behaviour that moves to the next or previous page
 * in your browser's history when you swipe in from the edge of the screen.
 *
 * Only seems to work reliably on Safari. Testing on Chrome iOS showed
 * inconsistent effectiveness. Did not test other browsers.
 *
 */
export function usePreventSwipeGestures() {
    useEffect(() => {
        const touchStart = (ev: TouchEvent) => {
            // Check if the target element is an interactive element (e.g., button, input, etc.)
            if (ev.touches.length === 1) {
                const touch = ev.touches[0];
                if (touch && touch.pageX < 15) {
                    ev.preventDefault();
                }
            }
        };

        // Safari defaults to passive: true for the touchstart event, so we need  to explicitly specify false
        // See https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
        const options = { passive: false };
        window.addEventListener("touchstart", touchStart, options);
        return () => window.removeEventListener("touchstart", touchStart);
    }, []);
}
