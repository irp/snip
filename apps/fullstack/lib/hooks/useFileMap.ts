"use client";
import { useState } from "react";
import useSWR from "swr";

import { FolderDataRet, ID } from "@snip/database/types";

import { fetcher } from "../fetcher";

/** Wrapper for the
 * api/folders/ endpoint
 * to get the file map
 * create new folders
 * move folders
 * delete folders
 */
export default function useFileMap() {
    const {
        data: fileMap,
        mutate,
        error,
        isLoading,
    } = useSWR<FileMap>(
        "/api/folders",
        async (url) => {
            const data = (await fetcher(url, undefined, "GET")) as FileMap;
            return processFileMap(data);
        },
        {
            revalidateIfStale: true,
            revalidateOnFocus: true,
            revalidateOnReconnect: true,
            refreshInterval: 0,
        },
    );

    const create_folder = async (
        name: string,
        comment: string,
        parent_id?: ID,
    ) => {
        await mutate(async () => {
            const newFolder = await createFolderServer({
                name,
                comment,
                parent_id,
            });
            return { ...fileMap, [newFolder.id!]: newFolder };
        });
    };
    const [, update] = useState({});
    const move_file_to_folder = async (fileId: FileID, newParentId: FileID) => {
        const optimistic = {
            ...fileMap,
        };
        const file = optimistic[fileId];
        if (!file || file.parentId === undefined) return;
        const oldParent = optimistic[file.parentId];
        const newParent = optimistic[newParentId];
        if (
            !oldParent ||
            !newParent ||
            !newParent.childrenIds ||
            !oldParent.childrenIds
        )
            return;

        // Remove file from old parent
        oldParent.childrenIds = oldParent.childrenIds.filter(
            (childId) => childId !== fileId,
        );

        // Add file to new parent
        newParent.childrenIds.push(fileId);
        file.parentId = newParentId;

        // Force update
        update({});

        await mutate(
            async () => {
                await moveFileServer(
                    file.isDir ? "folder" : "book",
                    fileId,
                    newParentId,
                );
                return { ...optimistic };
            },
            {
                optimisticData: optimistic,
                rollbackOnError: true,
                populateCache: true,
                revalidate: false,
            },
        );
    };

    const delete_folder = async (folderId: FileID) => {
        const optimistic = {
            ...fileMap,
        };
        const folder = optimistic[folderId];
        if (folder?.parentId === undefined) return;

        const parent = optimistic[folder.parentId];

        if (
            folder.childrenIds === undefined ||
            parent?.childrenIds === undefined
        )
            return;

        // Move all children to parent
        for (const childId of folder.childrenIds) {
            const child = optimistic[childId]!;
            parent.childrenIds.push(childId);
            child.parentId = parent.id;
        }
        // Remove folder from parent
        parent.childrenIds = parent.childrenIds.filter(
            (childId) => childId !== folderId,
        );

        // Remove folder from fileMap
        delete optimistic[folderId];

        // Force update
        update({});

        await mutate(
            async () => {
                await deleteFolderServer(folderId);
                return optimistic;
            },
            {
                optimisticData: optimistic,
                rollbackOnError: true,
                populateCache: true,
                revalidate: false,
            },
        );
    };

    return {
        fileMap: fileMap,
        isLoading,
        error,
        create_folder,
        delete_folder,
        move_file_to_folder,
    };
}

async function createFolderServer(
    folder: Partial<FolderDataRet>,
): Promise<FolderDataRet> {
    const response = await fetch("/api/folders", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(folder),
    });
    const data = await response.json();
    if (response.ok) {
        return data;
    }
    throw new Error(data.error);
}

async function deleteFolderServer(folderID: string) {
    folderID = folderID.replace("folder_", "");
    const response = await fetch(`/api/folders`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ folderID }),
    });
    const data = await response.json();
    if (!response.ok) {
        throw new Error(data.error);
    }
}

async function moveFileServer(
    type: "folder" | "book",
    itemID: string,
    moveToID: string,
) {
    itemID = itemID.replace("folder_", "");
    moveToID = moveToID.replace("folder_", "");
    const response = await fetch(`/api/folders/move`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ type, itemID, moveToID }),
    });
    const data = await response.json();
    if (!response.ok) {
        throw new Error(data.error);
    }
}

function processFileMap(data: FileMap): FileMap {
    console.log("Processing file map");
    // Process file map
    const fMap = { ...data };
    const files = Object.values(fMap);
    for (const file of files) {
        // create parent and childrenIds if they don't exist
        if (file.childrenIds) {
            file.childrenIds.forEach((childId) => {
                const child = fMap[childId]!;
                if (!child.parentId) {
                    child.parentId = file.id;
                }
            });
        } else {
            file.childrenIds = [];
        }

        if (file.parentId) {
            const parent = fMap[file.parentId]!;
            if (!parent.childrenIds || !parent.childrenIds.includes(file.id)) {
                parent.childrenIds = [file.id];
            }
        }
        // Parse all dates (undefined parses to invalid date...)
        file.last_updated = file.last_updated
            ? new Date(file.last_updated)
            : undefined;
        file.created = file.created ? new Date(file.created) : undefined;

        // Thumbnails i.e. page ids
        // Create thumbnail link
        if (!file.isDir && !file.thumbnail) {
            file.thumbnail = `${file.id}`;
        }
    }

    // We do this afterwards because we want to dates to be set already
    for (const file of files) {
        // Last updated of folders is the last updated of the most recently
        // updated file in the folder.
        if (file.isDir) {
            let last_updated = file.created || new Date();
            for (const childId of file.childrenIds!) {
                const child = fMap[childId]!;
                if (child.last_updated && child.last_updated > last_updated) {
                    last_updated = child.last_updated;
                }
            }
            file.last_updated = last_updated;
        }
    }

    return fMap;
}
