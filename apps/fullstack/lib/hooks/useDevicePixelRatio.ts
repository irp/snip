"use client";
import { useCallback, useEffect, useState } from "react";

const useDevicePixelRatio = () => {
    const [pixelRatio, setPixelRatio] = useState(getDevicePixelRatio());

    const handleChange = useCallback(() => {
        setPixelRatio(window.devicePixelRatio);
    }, []);

    useEffect(() => {
        const updatePixelRatio = () => {
            setPixelRatio(window.devicePixelRatio);
        };

        const mediaMatcher = window.matchMedia(
            `screen and (resolution: ${pixelRatio}dppx)`,
        );

        const handleChange = () => {
            updatePixelRatio();
        };

        mediaMatcher.addEventListener("change", handleChange);

        return () => {
            mediaMatcher.removeEventListener("change", handleChange);
        };
    }, [pixelRatio, handleChange]);

    return pixelRatio;
};

/**
 * Returns the current device pixel ratio (DPR) given the passed options
 *
 * @param options
 * @returns current device pixel ratio
 */
export function getDevicePixelRatio(): number {
    const hasDprProp =
        typeof window !== "undefined" &&
        typeof window.devicePixelRatio === "number";
    return hasDprProp ? window.devicePixelRatio : 1; //defaults to 1
}

export default useDevicePixelRatio;
