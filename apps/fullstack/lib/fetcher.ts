interface FetcherError extends Error {
    status: number;
    info?: unknown;
}
interface ApiError_client extends Error {
    error: string;
    details?: unknown;
}

export async function fetcher(
    url: string,
    body?: Record<string, unknown>,
    method?: "GET" | "POST" | "PUT" | "DELETE",
) {
    //Check if token is in url
    const headers: Record<string, string> = {};
    const token = new URL(window.location.href).searchParams.get("token");

    const requestInit: RequestInit = {
        headers: {} as Record<string, string>,
        method: method || "GET",
    };

    if (token) {
        headers["Authorization"] = `Bearer ${token}`;
    }

    if (body) {
        headers["Content-Type"] = "application/json";
        requestInit.body = JSON.stringify(body);
    }

    requestInit.headers = headers;

    const res = await fetch(url, requestInit).then(res_parse_json);
    return res;
}

export async function res_parse_json(res: Response) {
    // If the status code is not in the range 200-299,
    // we still try to parse and throw it.
    if (!res.ok) {
        let error;
        try {
            //ApiError msg
            const json = await res.json();
            error = new Error(json.error) as ApiError_client;
            error.details = json.details;
        } catch {
            error = new Error(
                "An error occurred while fetching the data.",
            ) as FetcherError;
            // Attach extra info to the error object
            // The response might not be defined!
            error.status = res.status;
            // No json response
        }
        throw error;
    }
    return await res.json();
}
