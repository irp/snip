import { NextRequest, NextResponse } from "next/server";

import { catchInternalErrors } from "./wrapperErrors";
import { RequestProps, wrapperSession } from "./wrapperSession";

/** A small wrapper to remove some duplicate code from
 * the account API it checks if the session cookie is
 * set and returns an error if it is not.
 *
 * Also adds an error handler to the route that should catch
 * server errors and return a 500 status code.
 *
 * @param handler The handler to wrap
 */
export function wrapperLoggedInUser(
    handler: (
        request: NextRequest,
        props: DeepPartialExcept<RequestProps, "session.user">,
    ) => Promise<NextResponse>,
) {
    const h = async (req: NextRequest, props?: RequestProps) => {
        if (props && props.session && props.session.user) {
            return await handler(
                req,
                props as DeepPartialExcept<RequestProps, "session.user">,
            );
        }

        return NextResponse.json(
            {
                error: "Unauthorized",
                details: "You need to login to view this route!",
            },
            { status: 401 },
        );
    };

    return wrapperSession(catchInternalErrors(h));
}
