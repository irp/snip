import { cookies } from "next/headers";
import { NextRequest, NextResponse } from "next/server";

import { getSession } from "@snip/auth/session";
import { SnipSessionData } from "@snip/auth/types";
export interface RequestProps {
    params?: {
        [key: string]: string;
    };
    searchParams?: {
        [key: string]: string;
    };
    session: SnipSessionData;
}

/** Adds the session to the request in the app
 * router. I.e. for usage in "route.ts" files.
 */
export function wrapperSession(
    handler: (
        request: NextRequest,
        props?: RequestProps,
    ) => Promise<NextResponse>,
) {
    return async (req: NextRequest, props: Omit<RequestProps, "session">) => {
        const session = await getSession(cookies());

        return await handler(req, {
            ...props,
            session,
        });
    };
}
