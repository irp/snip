import { NextRequest, NextResponse } from "next/server";

import { RequestProps } from "./wrapperSession";

/**
 * Wraps a request handler function with error handling for internal errors.
 * If an error occurs during the execution of the handler function, it will be caught and a JSON response with an error message will be returned.
 * @param handler - The request handler function to be wrapped.
 * @returns A wrapped request handler function that handles internal errors.
 */
export function catchInternalErrors(
    handler: (
        request: NextRequest,
        props?: RequestProps,
    ) => Promise<NextResponse>,
) {
    return async (req: NextRequest, props?: RequestProps) => {
        try {
            return await handler(req, props);
        } catch (error) {
            console.error(error);
            return NextResponse.json(
                {
                    error: "Internal Server Error",
                    details: (error as Error).message,
                },
                { status: 500 },
            );
        }
    };
}
