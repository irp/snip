import { NextRequest, NextResponse } from "next/server";

import { getPerms, ParsedPermissionACLData } from "@snip/auth/book_permissions";
import { ID } from "@snip/database/types";

import { catchInternalErrors } from "./wrapperErrors";
import { RequestProps, wrapperSession } from "./wrapperSession";

export interface RequestPropsWithPerms extends RequestProps {
    perms: ParsedPermissionACLData;
    book_id: ID;
}

/**
 * Wraps a request handler function to add the perms of the user for this book.
 * This only works if the request has an id parameter or book_id parameter.
 * If it does not it will throw an error.
 *
 * @param handler - The request handler function to be wrapped.
 *
 */
export function wrapperBookPerms(
    handler: (
        request: NextRequest,
        props: RequestPropsWithPerms,
    ) => Promise<NextResponse>,
    allowToken = true,
) {
    const h = async (req: NextRequest, props?: RequestProps) => {
        // Check if the request has an id or book_id parameter
        if (!props) {
            return NextResponse.json(
                {
                    error: "Bad Request",
                    details:
                        "You need to provide a book id to view this route!",
                },
                { status: 400 },
            );
        }
        const { session, params } = props;

        if (!params) {
            return NextResponse.json(
                {
                    error: "Bad Request",
                    details:
                        "You need to provide a book id to view this route!",
                },
                { status: 400 },
            );
        }

        // Check if book id is in the parameters
        if (!("book_id" in params || "id" in params)) {
            return NextResponse.json(
                {
                    error: "Bad Request",
                    details:
                        "You need to provide a book id to view this route!",
                },
                { status: 400 },
            );
        }
        if (isNaN(parseInt(params.book_id! || params.id!))) {
            return NextResponse.json(
                {
                    error: "Bad Request",
                    details: "The book id must be a number!",
                },
                { status: 400 },
            );
        }
        const book_id = parseInt(params.book_id! || params.id!);

        // Get perms from session / headers
        let user_id: ID | undefined;
        let ui_token_id: ID | undefined;
        let bearer_token: string | undefined;

        if (session.user) {
            user_id = session.user.id;
        }
        if (allowToken && session.anonymous) {
            ui_token_id = session.anonymous.tokens.filter(
                (token) =>
                    token.book_id === book_id && token.expires_at > Date.now(),
            )[0]?.id;
        }
        if (allowToken && req.headers.get("Authorization")) {
            bearer_token = req.headers.get("Authorization")!.split(" ").at(1);
        }

        let perms: ParsedPermissionACLData;
        try {
            const { resolved } = await getPerms(book_id, {
                user_id,
                ui_token_id,
                bearer_token,
            });
            perms = resolved;
        } catch (e) {
            console.error(getPerms, book_id);
            console.error(e);
            return NextResponse.json(
                {
                    error: "Internal Server Error",
                    details: "Could not fetch permissions for book and user!",
                },
                { status: 500 },
            );
        }

        return await handler(req, {
            session,
            params,
            perms,
            book_id,
        });
    };
    return wrapperSession(catchInternalErrors(h));
}
