import { headers } from "next/headers";
import type { NextRequest } from "next/server";
import { NextResponse } from "next/server";

import { getSession } from "@snip/auth/session_edge";
import { SnipSessionData } from "@snip/auth/types";

// Define logger with prefix
//const logger = (...args) => console.log(`[Middleware NextJS]`, ...args);

async function logger(req: NextRequest, session: Promise<SnipSessionData>) {
    return;
    const headersList = headers();
    const ip = headersList.get("x-forwarded-for") || "127.0.0.1";
    const isLoggedIn = (await session).user?.id ? true : false;

    console.log(`------------------`);
    console.log(`[Middleware] Date: ${new Date().toUTCString()}`);
    console.log(`[Middleware] Request URL: ${req.nextUrl.pathname}`);
    console.log(`[Middleware] Request Method: ${req.method}`);
    console.log(`[Middleware] IP Address: ${ip}`);
    console.log(`[Middleware] Is Logged In: ${isLoggedIn}`);
    console.log(`[Middleware] User ID: ${(await session).user?.id}`);
    console.log(`------------------`);
}
export async function middleware(request: NextRequest) {
    const response = NextResponse.next();
    const session = getSession(request, response);
    logger(request, session);

    // Check if route starts with /api or not
    if (request.nextUrl.pathname.startsWith("/api")) {
        return await middleware_api(request, response, session);
    } else {
        return await middleware_ui(request, response, session);
    }
}

/** Middleware function that checks whether the user is authenticated or has
 * provided a valid token in the header. If neither is true, returns a
 * 401 Unauthorized response. Otherwise, passes the request to the next
 * middleware in the chain.
 *
 * Keep in mind this does not verify the token, it only checks if it exists.
 *
 */
async function middleware_api(
    req: NextRequest,
    res: NextResponse,
    session: Promise<SnipSessionData>,
): Promise<NextResponse> {
    // There are two ways to access the api
    // 1. The user is logged in as user or anonymous
    // 2. The user provides a token in the header
    const base = req.url;

    const { user, anonymous } = await session;
    if ((user && user.email_verified) || anonymous) {
        return res;
    }

    const token = req.headers.get("authorization")?.split(" ")[1];
    if (token) {
        return res;
    }

    return NextResponse.rewrite(new URL(`/api/unauthorized`, base));
}

/** Middleware function that checks whether the user is logged in and verified.
 * If the user is not logged in, redirects the user to the login page with the
 * current URL as the redirect parameter. If the user is logged in but not
 * verified, rewrites the URL to the account verification page.
 *
 */
async function middleware_ui(
    req: NextRequest,
    res: NextResponse,
    session: Promise<SnipSessionData>,
): Promise<NextResponse> {
    // Check if the user is logged in as user or anonymous
    const base = req.url;
    const { user, anonymous } = await session;

    // Rewrite for index page
    if (req.nextUrl.pathname === "/") {
        if (!user || !user.email_verified) {
            return NextResponse.redirect(new URL(`/frontpage`, base));
        }
        if (user && user.email_verified) {
            return NextResponse.redirect(new URL(`/books`, base));
        }
    }
    // Special parsing for login/signup
    if (req.nextUrl.pathname == "/login" || req.nextUrl.pathname == "/signup") {
        if (!user) {
            return res;
        } else {
            return NextResponse.redirect(new URL(`/frontpage`, base));
        }
    }

    // No user_id means the user is not logged in
    if (!user && !anonymous) {
        const redirect = req.nextUrl.pathname + req.nextUrl.search;
        let url_string = `/login`;
        if (redirect !== "/") {
            url_string += `?redirect=${encodeURIComponent(redirect)}`;
        }
        return NextResponse.redirect(new URL(url_string, base));
    }

    // No email_verified means the user is not verified
    if (user && !user.email_verified) {
        return NextResponse.rewrite(new URL(`/verify`, base));
    }

    // If the user is logged in and verified or anonymous
    return res;
}

/** The middleware matches all
 * routes that are not in the opt_out array
 * this should match the (unprotected) group in
 * the app router

    const opt_out = [
        "frontpage",
        "privacy",
        "imprint",
        "docs"
        "shared",
        "debug"
        // (errors)
        "unauthorized",
        "expired",
        "404",
        // (account)
        "verify",
        "signup",
        "login",
        "sso"
        "sso/callback",
        // (account)/(pw_reset)
        "forgotPassword",
        "resetPassword",
        // api
        "api/unauthorized",
        // NextJS dev 
        "schemas",
        "_next/static",
        "_next/image",
        "favicon.ico",
        "fonts"
        "img"
    ];
*/

export const config = {
    //matcher: "/((?!" + opt_out.join("|") + ").*)",
    matcher: [
        "/((?!frontpage|privacy|imprint|docs|shared|debug|404|unauthorized|expired|verify|signup|login|sso|sso/callback|forgotPassword|resetPassword|api/unauthorized|schemas|_next/static|_next/image|favicon.ico|apple-touch-icon.png|fonts|img).*)",
    ],
};
