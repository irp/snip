import { resolve } from "path";
import tsconfigPaths from "vite-tsconfig-paths";
import { defineConfig } from "vitest/config";

// setup global environment for vitest
export default defineConfig({
    test: {
        globals: true,
        setupFiles: ["./__mocks__/global_mocks.ts"],
        coverage: {
            provider: "v8",
            include: ["app/api/**/*"],
        },
        alias: [
            {
                find: /^@snip\/database$/,
                replacement: resolve("./__mocks__/@snip/database/index.ts"),
            },
            {
                find: "@snip/auth/book_permissions",
                replacement: resolve(
                    "./__mocks__/@snip/auth/book_permissions.ts",
                ),
            },
            {
                find: "@snip/auth/session",
                replacement: resolve("./__mocks__/@snip/auth/session.ts"),
            },
        ],
    },
    resolve: {
        alias: {
            "@/": new URL("./", import.meta.url).pathname,
        },
    },
});
