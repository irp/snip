#!/bin/bash
function database_ready() {
    python3 -c "
import mysql.connector
cnx = mysql.connector.connect(
    user='$MYSQL_USER',
    password='$MYSQL_PASSWORD',
    host='$MYSQL_HOST',
)
# Check if connected and return
print(cnx.is_connected())
cnx.close()
    " > /dev/null 2>&1
}

# Check that the database is available
if [ -n "$MYSQL_HOST" ]; then
    database=`echo $MYSQL_HOST | awk -F[@//] '{print $4}'`
    echo "[Entrypoint] Waiting for Database ..."
    while ! database_ready; do
        # Show some progress
        echo -n "."
        sleep 1;
    done
    echo ""
    echo "[Entrypoint] Database is ready"
    # Give it another second
    sleep 1;
    # Run migrations
    echo "[Entrypoint] Running database migrations"
    python3 ./database/mariaDB_migrate.py --host $MYSQL_HOST
    echo "[Entrypoint] Database migrations complete";
fi

# Run the main script
echo "[Entrypoint] Starting application npm run $@"
npm run $@
