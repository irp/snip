import { createServer as createServerHttp } from "http";
import next from "next";
import { parse } from "url";

import { config } from "@snip/config/server";

// Config for next server
const dev = config.NODE_ENV !== "production";
const hostname = "localhost";
const port = 4000;
const dir = "./";
// when using middleware `hostname` and `port` must be provided below
const app = next({ dev, hostname, port, dir });
const handle = app.getRequestHandler();

/**
 * Handles the callback server logic.
 *
 * @param {IncomingMessage} req - The request object.
 * @param {ServerResponse<IncomingMessage>} res - The response object.
 * @returns {Promise<void>} - A promise that resolves when the server logic is handled.
 */
async function callback_server(req, res) {
    // We attach the socket to the request object
    // to use it in the route handler
    // e.g. emitting event on post requests

    try {
        // Be sure to pass `true` as the second argument to `url.parse`.
        // This tells it to parse the query portion of the URL.
        const parsedUrl = parse(req.url, true);
        await handle(req, res, parsedUrl);
    } catch (err) {
        console.error("Error occurred handling", req.url, err);
        res.statusCode = 500;
        res.end("internal server error");
    }
}

app.prepare().then(() => {
    // See nginx for ssl setup
    const server = createServerHttp(callback_server);

    server.listen(port, () => {
        console.log(`[Next] Ready on http://${hostname}:${port}`);
    });
});
