import * as m from "../../mockConfig";
const getSession = async (cookies: Record<string, string>) => {
    return globalThis.mockConfig.session;
};

const getCookieString = (cookies: Record<string, string>) => {
    return Object.entries(cookies)
        .map(([key, value]) => `${key}=${value}`)
        .join("; ");
};

export { getCookieString, getSession };
