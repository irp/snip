// eslint-disable-next-line @typescript-eslint/no-unused-vars
import * as m from "../../mockConfig";

const getPerms = vi.fn(async () => {
    let auth_method: "bearer" | "user" | "ui_token" | "unknown" = "unknown";
    if (globalThis.mockConfig.session.anonymous) {
        auth_method = "ui_token";
    }
    if (globalThis.mockConfig.session.user) {
        auth_method = "user";
    }

    return {
        resolved: {
            ...globalThis.mockConfig.book_perms,
            auth_method,
        },
        all: [
            {
                ...globalThis.mockConfig.book_perms,
                auth_method,
            },
        ],
    };
});

export { getPerms };
