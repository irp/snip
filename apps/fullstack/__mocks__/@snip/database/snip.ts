import type { SnipDataRet } from "@snip/database/types";

import { potentiallyThrowError } from "./utils";

const mod =
    await vi.importActual<typeof import("@snip/database")>("@snip/database");
const oService = mod.default;

const snip_default = {
    id: 1,
    created_by: 1,
    data_json: "{}",
    view_json: "{}",
    data: {},
    view: {},
    type: "base",
    blob_id: null,
    book_id: 1,
    last_updated: new Date(),
    created: new Date(),
    page_id: null,
    hide: null,
};

const insert: typeof oService.snip.insert = async (data) => {
    const d = {
        ...snip_default,
        ...(data as SnipDataRet),
    };
    return await potentiallyThrowError(d, "snip.insert");
};

const snip1: Partial<typeof oService.snip> = {
    insert,
};

export const snip = snip1;
