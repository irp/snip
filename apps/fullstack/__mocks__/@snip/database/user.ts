import { potentiallyThrowError, user_default } from "./utils";

const mod =
    await vi.importActual<typeof import("@snip/database")>("@snip/database");
const oService = mod.default;

const isAdmin: typeof oService.user.isAdmin = async (_user_id) => {
    return globalThis.mockConfig.service.isAdmin;
};

const user_getByEmail: typeof oService.user.getByEmail = async (email) => {
    const d = [
        {
            ...user_default,
            emails: [email],
        },
    ];
    return await potentiallyThrowError(d, "user.getByEmail");
};

const user_getById: typeof oService.user.getById = async (id) => {
    const d = {
        ...user_default,
        id,
    };

    return await potentiallyThrowError(d, "user.getById");
};

const user_getPasswordCredentialByEmail: typeof oService.user.getPasswordCredentialByEmail =
    async (email) => {
        const d = {
            id: 1,
            user_id: 1,
            email,
            credential_type: "password" as const,
            provider_id: null,
            provider_user_id: null,
            email_verified: globalThis.mockConfig.service.email_verified,
            password_hash: "test",
            created: new Date(),
            last_updated: new Date(),
        };
        return await potentiallyThrowError(
            d,
            "user.getPasswordCredentialByEmail",
        );
    };

const user_getPasswordCredentialByUserId: typeof oService.user.getPasswordCredentialByUserId =
    async (user_id) => {
        const d = {
            id: 1,
            user_id,
            email: "test@test.de",
            credential_type: "password" as const,
            provider_id: null,
            provider_user_id: null,
            email_verified: globalThis.mockConfig.service.email_verified,
            password_hash: "test",
            created: new Date(),
            last_updated: new Date(),
        };
        return await potentiallyThrowError(
            d,
            "user.getPasswordCredentialByUserId",
        );
    };

const user_loginWithPassword: typeof oService.user.loginWithPassword = async (
    email,
    _password,
) => {
    const d = {
        ...user_default,
        emails: [email],
    };
    return await potentiallyThrowError(d, "user.loginWithPassword");
};

const user_addLastLogin: typeof oService.user.addLastLogin = async (
    _user_id,
) => {
    return await potentiallyThrowError(true, "user.addLastLogin");
};

const user_changePassword: typeof oService.user.changePassword = async (
    _user_id,
    _password,
) => {
    return await potentiallyThrowError(true, "user.changePassword");
};

const user_verifySignupWithPassword: typeof oService.user.verifySignupWithPassword =
    async (_email) => {
        return await potentiallyThrowError(
            true,
            "user.verifySignupWithPassword",
        );
    };

const user_signupWithPassword: typeof oService.user.signupWithPassword = async (
    email,
    _password,
) => {
    const d = {
        ...user_default,
        emails: [email],
    };
    return await potentiallyThrowError(d, "user.signupWithPassword");
};

const user_mergeUsers: typeof oService.user.mergeUsers = async (
    _from_id,
    _to_id,
) => {
    return await potentiallyThrowError(true, "user.mergeUsers");
};

const user_getByCredentialId: typeof oService.user.getByCredentialId = async (
    credential_id,
) => {
    const d = {
        ...user_default,
        id: 1,
    };
    return await potentiallyThrowError(d, "user.getByCredentialId");
};

const user1: Partial<typeof oService.user> = {
    isAdmin: isAdmin,
    getByEmail: user_getByEmail,
    getPasswordCredentialByEmail: user_getPasswordCredentialByEmail,
    getPasswordCredentialByUserId: user_getPasswordCredentialByUserId,
    loginWithPassword: user_loginWithPassword,
    getById: user_getById,
    addLastLogin: user_addLastLogin,
    getByCredentialId: user_getByCredentialId,
    verifySignupWithPassword: user_verifySignupWithPassword,
    signupWithPassword: user_signupWithPassword,
    changePassword: user_changePassword,
    mergeUsers: user_mergeUsers,
};

export const user = user1;
