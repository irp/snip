import { group_default, potentiallyThrowError, user_default } from "./utils";

const mod =
    await vi.importActual<typeof import("@snip/database")>("@snip/database");
const oService = mod.default;

const group_isAllowedToEdit: typeof oService.group.isAllowedToEdit = async (
    _user_id,
    _group_id,
) => {
    return true;
};

const getById: typeof oService.group.getById = async (group_id) => {
    const d = {
        ...group_default,
        id: group_id,
    };
    return await potentiallyThrowError(d, "group.getById");
};

const getMember: typeof oService.group.getMember = async (
    user_id,
    group_id,
) => {
    const d = {
        ...user_default,
        user_id,
        group_id,
        group_name: group_default.name,
        group_description: group_default.description,
        role: "owner" as const,
        joined_at: new Date(),
    };
    return await potentiallyThrowError(d, "group.getMember");
};

const getMembers: typeof oService.group.getMembers = async (group_id) => {
    const d = [
        {
            ...user_default,
            user_id: user_default.id,
            group_id,
            group_name: group_default.name,
            group_description: group_default.description,
            role: "owner" as const,
            joined_at: new Date(),
        },
    ];
    return await potentiallyThrowError(d, "group.getMembers");
};

const group_addMember: typeof oService.group.addMember = async (
    user_id,
    group_id,
    role,
) => {
    const d = {
        group_id,
        group_description: "Test description",
        group_name: "Test",
        user_id,
        role,
        joined_at: new Date(),
        emails: ["user@email.de"],
        emails_verified: [true],
        credential_ids: [1],
        credential_types: ["password"],
        primary_credential_id: 1,
    };
    return await potentiallyThrowError(d, "group.addMember");
};

const getByUserId: typeof oService.group.getByUserId = async (user_id) => {
    const d = [
        {
            id: 1,
            name: "Test",
            created: new Date(),
            last_updated: new Date(),
            owner_id: user_id,
            owner_name: "Test",
            description: "Test description",
        },
    ];
    return await potentiallyThrowError(d, "group.getByUserId");
};

const insert: typeof oService.group.insert = async (data) => {
    const d = {
        ...group_default,
        ...data,
    };
    return await potentiallyThrowError(d, "group.insert");
};

const group1: Partial<typeof oService.group> = {
    isAllowedToEdit: group_isAllowedToEdit,
    getById,
    getMember,
    addMember: group_addMember,
    getByUserId,
    getMembers,
    insert,
};

export const group = group1;
