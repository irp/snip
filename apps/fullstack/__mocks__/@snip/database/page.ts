import type { PageDataRet } from "@snip/database/types";
import { potentiallyThrowError } from "./utils";

const mod =
    await vi.importActual<typeof import("@snip/database")>("@snip/database");
const oService = mod.default;

const page_default: PageDataRet = {
    id: 1,
    book_id: 1,
    last_updated: new Date(),
    created: new Date(),
    page_number: 1,
    background_type_id: 1,
    referenced_book_id: null,
    referenced_page_id: null,
};

const getByBookId: typeof oService.page.getByBookId = async (book_id) => {
    const data = [
        {
            ...page_default,
        },
    ];
    return await potentiallyThrowError(data, "page.getByBookId");
};

const page1: Partial<typeof oService.page> = {
    getByBookId,
};

export const page = page1;
