import { NotFoundError } from "@snip/database/errors";

/**
 * Potentially throws an error based on the configuration in `mockConfig`.
 *
 * @param d - The data to be returned if no error is thrown.
 * @param match - A string that is used to match against the `throw_error` configuration.
 * @returns The data `d` if no error is thrown.
 * @throws {Error} "Mock Error" if the `throw_error` configuration is set to `true` or matches the `match` string.
 */
export async function potentiallyThrowError<T>(
    d: T,
    match: string,
): Promise<T> {
    const throw_error = globalThis.mockConfig.service.throw_error;
    if (throw_error === true) {
        throw new Error(
            globalThis.mockConfig.service.error_message || "Mock Error",
        );
    }
    if (typeof throw_error === "string" && match.includes(throw_error)) {
        throw new Error(
            globalThis.mockConfig.service.error_message || "Mock Error",
        );
    }
    const not_found = globalThis.mockConfig.service.not_found;
    if (not_found === true) {
        throw new NotFoundError("mock", 1);
    }
    if (typeof not_found === "string" && match.includes(not_found)) {
        throw new NotFoundError("mock", 1);
    }

    return d;
}

export const user_default = {
    id: 1,
    emails: ["test@test.de"],
    credential_ids: [1],
    credential_types: ["password"],
    primary_credential_id: 1,
    emails_verified: [globalThis.mockConfig.service.email_verified],
    last_updated: new Date(),
    created: new Date(),
    credential_provider_ids: [null],
};

export const group_default = {
    id: 1,
    name: "Test",
    created: new Date(),
    last_updated: new Date(),
    owner_id: 1,
    owner_name: "Test",
    description: "Test description",
};
