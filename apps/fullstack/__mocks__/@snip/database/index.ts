import {
    BookDataRet,
    ENTITY_GROUP,
    RESOURCE_BOOK,
    UserConfigDataInsert,
    UserConfigDataRet,
} from "@snip/database/types";

import { group } from "./group";
import { snip } from "./snip";
import { user } from "./user";
import { page } from "./page";
import { potentiallyThrowError } from "./utils";

const mod =
    await vi.importActual<typeof import("@snip/database")>("@snip/database");
const oService = mod.default;

const book_insert: typeof oService.book.insert = async (data) => {
    const d: BookDataRet = {
        ...data,
        id: Math.floor(Math.random() * 1000),
        last_updated: new Date(),
        finished: null,
        created: new Date(),
        cover_page_id: data.cover_page_id || null,
        comment: data.comment || null,
        owner_id: data.owner_user_id || data.owner_group_id || null,
        owner_type: data.owner_user_id ? "user" : "group",
        owner_name: "Test",
        background_type_description: "Test Background Description",
        background_type_name: "Test Background Name",
        default_background_type_id: 1,
        num_pages: 0,
    };

    return await potentiallyThrowError(d, "book.insert");
};

const book_getByUserId: typeof oService.book.getByUserId = async (
    user_id,
    pWrite?: boolean,
) => {
    const d: BookDataRet[] = [
        {
            id: Math.floor(Math.random() * 1000),
            title: "Test",
            owner_id: user_id,
            owner_name: "Test",
            owner_type: "user",
            last_updated: new Date(),
            created: new Date(),
            cover_page_id: null,
            comment: null,
            background_type_description: "Test Background Description",
            background_type_name: "Test Background Name",
            default_background_type_id: 1,
            finished: null,
            num_pages: 0,
        },
    ];

    return await potentiallyThrowError(d, "book.getByUserId");
};

const book_setOwner: typeof oService.book.setOwner = async (
    entity_id,
    entity_type,
    _book_id,
) => {
    const d = {
        id: entity_id,
        entity_name: "Test",
        entity_type,
    };
    return await potentiallyThrowError(d, "book.setOwner");
};

const book_getOwner: typeof oService.book.getOwner = async (_book_id) => {
    const d = {
        id: 1,
        entity_name: "Test",
        entity_type: "user" as const,
    };
    return await potentiallyThrowError(d, "book.getOwner");
};

const book_getById: typeof oService.book.getById = async (id) => {
    const d: BookDataRet = {
        id,
        title: "Test",
        owner_id: 1,
        owner_name: "Test",
        owner_type: "user",
        last_updated: new Date(),
        created: new Date(),
        cover_page_id: null,
        comment: null,
        background_type_description: "Test Background Description",
        background_type_name: "Test Background Name",
        default_background_type_id: 1,
        finished: null,
        num_pages: 0,
    };

    return await potentiallyThrowError(d, "book.getById");
};

/* -------------------------------------------------------------------------- */
/*                             User Config related                            */
/* -------------------------------------------------------------------------- */

const default_userConfig = {
    id: 1,
    user_id: 1,
    last_updated: new Date(),
    created: new Date(),
    pen_color: "#000000",
    pen_colors: ["#000000", "#ff0000", "#00ff00", "#0000ff"],
    pen_size: 4,
    pen_smoothing: 0.0,
    text_font_id: 1,
    text_font: "Arial",
    text_fontColor: "#000000",
    text_fontSize: 12,
    text_lineHeight: 1.25,
    text_lineWrap: 400,
    zoom1: 1,
    zoom2: 1,
    user_color: "#000000",
};

const userConfig_getByUserId: typeof oService.userConfig.getByUserId = async (
    user_id,
) => {
    const d = {
        ...default_userConfig,
        user_id,
    };
    return await potentiallyThrowError(d, "userConfig.getByUserId");
};

const userConfig_insert: typeof oService.userConfig.insert = async (data) => {
    const d = {
        ...default_userConfig,
        ...data,
    } as UserConfigDataRet;
    return await potentiallyThrowError(d, "userConfig.insert");
};

const userConfig_updateByUserId: typeof oService.userConfig.updateByUserId =
    async (data) => {
        const d = {
            ...default_userConfig,
            ...data,
        } as UserConfigDataRet;
        return await potentiallyThrowError(d, "userConfig.updateByUserId");
    };

/* -------------------------------------------------------------------------- */
/*                             Permission related                             */
/* -------------------------------------------------------------------------- */

const permission_getResolvedForBook: typeof oService.permission.getResolvedForBook =
    async (book_id) => {
        const d = [
            {
                entity_id: 1,
                entity_type: "user" as const,
                entity_name: "John Doe",
                id: 123,
                resource_id: 456,
                resource_type: RESOURCE_BOOK as typeof RESOURCE_BOOK,
                pRead: true,
                pWrite: true,
                pDelete: true,
                pACL: true,
            },
            {
                entity_id: 2,
                entity_type: "group" as const,
                entity_name: "wow groups",
                id: 1234,
                resource_id: 456,
                resource_type: RESOURCE_BOOK as typeof RESOURCE_BOOK,
                pRead: true,
                pWrite: true,
                pDelete: true,
                pACL: true,
            },
        ];
        return await potentiallyThrowError(d, "permission.getResolvedForBook");
    };

const permission_getByUserAndBookReduced: typeof oService.permission.getByUserAndBookReduced =
    async (user_id, book_id) => {
        const d = {
            entity_id: user_id,
            entity_type: ENTITY_GROUP as typeof ENTITY_GROUP,
            entity_name: "wow groups",
            id: 111,
            resource_id: book_id,
            resource_type: RESOURCE_BOOK as typeof RESOURCE_BOOK,
            pRead: true,
            pWrite: true,
            pDelete: true,
            pACL: true,
        };
        return await potentiallyThrowError(
            d,
            "permission.getByUserAndBookReduced",
        );
    };

const permission_getByEntityAndResource: typeof oService.permission.getByEntityAndResource =
    async (entity_id, entity_type, resource_id, resource_type) => {
        const d = {
            entity_id,
            entity_type: entity_type as typeof ENTITY_GROUP,
            entity_name: "Test",
            id: 123,
            resource_id,
            resource_type: resource_type as typeof RESOURCE_BOOK,
            pRead: true,
            pWrite: true,
            pDelete: true,
            pACL: true,
        };
        return await potentiallyThrowError(
            d,
            "permission.getByEntityAndResource",
        );
    };

/* -------------------------------------------------------------------------- */
/*                            Collaborator related                            */
/* -------------------------------------------------------------------------- */

const invite_default = {
    id: 1,
    accepted_at: null,
    book_id: 1,
    created_at: new Date(),
    expire_at: new Date(Date.now() + 1000 * 60 * 60 * 24),
    user_id: 11231,
    emails: ["test@test.de"],
    emails_verified: [true],
    credential_ids: [1],
    primary_credential_id: 1,
    credential_types: ["password"],
    invited_by: 123123,
    invited_by_emails: ["from@test.de"],
    invited_by_emails_verified: [true],
    invited_by_credential_ids: [2],
    invited_by_primary_credential_id: 2,
    invited_by_credential_types: ["password"],
};

const collaborator_getPendingByBookId: typeof oService.collaboratorsInvite.getPendingByBookId =
    async (book_id) => {
        const d = [
            {
                ...invite_default,
                book_id,
            },
        ];
        return await potentiallyThrowError(
            d,
            "collaboratorsInvite.getPendingByBookId",
        );
    };

const collaborator_insert: typeof oService.collaboratorsInvite.insert = async (
    data,
) => {
    const d = {
        ...invite_default,
        ...data,
    };
    return await potentiallyThrowError(d, "collaboratorsInvite.insert");
};
const background_getAll: typeof oService.backgroundType.getAll = async () => {
    const d = [
        {
            id: 1,
            name: "Background 1",
            description: "This is a background",
        },
        {
            id: 2,
            name: "Background 2",
            description: "This is another background",
        },
    ];
    return await potentiallyThrowError(d, "backgroundType.getAll");
};

const token_insert: typeof oService.token.insert = async (data) => {
    const d = {
        book_id: data.book_id,
        created_at: new Date(),
        created_by: data.created_by,
        description: data.description || null,
        expires_at: null,
        hashed_token: "test",
        type: data.type || "api",
        id: Math.floor(Math.random() * 100),
    };
    return await potentiallyThrowError(d, "token.insert");
};

const token_getById: typeof oService.token.getById = async (id) => {
    const d = {
        book_id: 1,
        created_at: new Date(),
        created_by: 1,
        description: "Test Token",
        expires_at: null,
        hashed_token: "test",
        type: "api" as const,
        id,
    };
    return await potentiallyThrowError(d, "token.getById");
};
const token_getByBookId: typeof oService.token.getByBookId = async (
    book_id,
) => {
    const d = [
        {
            book_id,
            created_at: new Date(),
            created_by: 1,
            description: "Test Token",
            expires_at: null,
            hashed_token: "test",
            type: "api" as const,
            id: 1,
        },
    ];
    return await potentiallyThrowError(d, "token.getByBookId");
};

/* -------------------------------------------------------------------------- */
/*                                Group related                               */
/* -------------------------------------------------------------------------- */

const service = {
    book: {
        insert: book_insert,
        getByUserId: book_getByUserId,
        setOwner: book_setOwner,
        getOwner: book_getOwner,
        getById: book_getById,
    },
    userConfig: {
        getByUserId: userConfig_getByUserId,
        insert: userConfig_insert,
        updateByUserId: userConfig_updateByUserId,
    },
    permission: {
        getResolvedForBook: permission_getResolvedForBook,
        getByUserAndBookReduced: permission_getByUserAndBookReduced,
        updateByEntityAndResource: async () => true,
        getByEntityAndResource: permission_getByEntityAndResource,
        delete: async () => true,
    },
    collaboratorsInvite: {
        getPendingByBookId: collaborator_getPendingByBookId,
        getPendingByBookIdAndUserId: async () => [],
        insert: collaborator_insert,
    },
    backgroundType: {
        getAll: background_getAll,
        setBookDefault: async () => true,
        setPageByIds: async () => true,
        setPageByNumbers: async () => true,
        getSelectedByBookId: async () => 1,
    },
    token: {
        insert: token_insert,
        getById: token_getById,
        getByBookId: token_getByBookId,
        delete: async () => true,
    },
    group,
    user,
    snip,
    page,
};

export default service;
