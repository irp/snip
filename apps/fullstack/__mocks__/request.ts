import { readFile } from "fs/promises";
import { NextRequest } from "next/server";
import { createRequest, RequestMethod } from "node-mocks-http";
import path from "path";
import { PassThrough } from "stream";

export async function createMockRequest({
    method = "POST",
    content_type = "text/plain",
    searchParams = new URLSearchParams(),
    body = undefined,
}: {
    method?: RequestMethod;
    content_type?: string;
    searchParams?: URLSearchParams;
    body?: unknown;
} = {}) {
    const req = await createRequest({
        method: method as RequestMethod,
        headers: {
            "Content-Type": content_type,
        },
        body: body ? toReadableStream(JSON.stringify(body)) : undefined,
    });

    // Add .json() method to the request
    req.json = async () => {
        return body;
    };

    // Add .nextUrl property to the request
    req.nextUrl = {
        searchParams,
    };

    // Create headers
    globalThis.mockConfig.headers = new Headers(req.headers as HeadersInit);
    return {
        ...req,
        headers: mockConfig.headers,
        nextUrl: {
            searchParams,
        },
    } as unknown as NextRequest;
}

interface Field {
    [key: string]: unknown;
}
interface File {
    name: string;
    filename: string;
    type: string;
}

export async function createMockRequestMultipart({
    method = "POST",
    boundary = "test_bounds",
    files = [],
    fields = [],
    empty = false,
    searchParams = new URLSearchParams(),
    dir = "test_files",
}: {
    method?: RequestMethod;
    boundary?: string;
    files?: File[];
    fields?: Field[];
    empty?: boolean;
    searchParams?: URLSearchParams;
    dir?: string;
} = {}) {
    const add_field = (stream: PassThrough, name: string, value: unknown) => {
        stream.write(
            'Content-Disposition: form-data; name="' + name + '"\r\n\r\n',
        );
        stream.write(JSON.stringify(value));
        stream.write("\r\n");
    };
    const add_file = (
        stream: PassThrough,
        name: string,
        filename: string,
        file: string | unknown[] | Buffer,
        type: string,
    ) => {
        stream.write(
            'Content-Disposition: form-data; name="' +
                name +
                '"; filename="' +
                filename +
                '"\r\nContent-Type: ' +
                type +
                "\r\n\r\n",
        );
        if (empty) {
            stream.write("");
        } else {
            // split into two parts for testing
            stream.write(file.slice(0, file.length / 2));
            stream.write(file.slice(file.length / 2));
        }
        stream.write("\r\n");
    };
    const add_boundary = (stream: PassThrough, boundary: string) => {
        stream.write("--" + boundary + "\r\n");
    };

    // create stream for body
    const stream = new PassThrough();

    add_boundary(stream, boundary);
    for (const field of fields) {
        Object.entries(field).forEach(([key, value]) => {
            add_field(stream, key, value);
            add_boundary(stream, boundary);
        });
    }
    for (const file of files) {
        const buffer_file = await readFile(path.join(dir, file.filename));
        add_file(stream, file.name, file.filename, buffer_file, file.type);
        add_boundary(stream, boundary);
    }
    stream.end("--" + boundary + "--\r\n");

    const req = createRequest({
        method: method as RequestMethod,
        headers: {
            "Content-Type": "multipart/form-data; boundary=" + boundary,
        },
    });

    const read_stream = toReadableStream(await stream.read());

    mockConfig.headers = new Headers(req.headers as HeadersInit);

    return {
        ...req,
        body: read_stream,
        headers: mockConfig.headers,
        nextUrl: {
            searchParams,
        },
    } as unknown as NextRequest;
}

export function toReadableStream(value: unknown) {
    return new ReadableStream({
        start(controller) {
            controller.enqueue(value);
            controller.close();
        },
    });
}
