import fs from "node:fs";
import os from "node:os";
import path from "node:path";

import { vi } from "vitest";

import { resetMockConfig } from "./mockConfig";

vi.mock("next/headers", () => ({
    headers: vi.fn(() => {
        return globalThis.mockConfig.headers;
    }),
    cookies: vi.fn(() => {}),
}));

function createTempDir() {
    const ostmpdir = os.tmpdir();
    const tmpdir = path.join(ostmpdir, "unit-test-");
    return fs.mkdtempSync(tmpdir);
}
const tmpdir = createTempDir();
vi.stubEnv("SNIP_CONFIG_DIR", tmpdir);

afterAll(async () => {
    await fs.rmSync(tmpdir, { recursive: true });
});

beforeEach(async () => {
    resetMockConfig();
});
