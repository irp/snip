import { SnipSessionData } from "@snip/auth/types";
import { PermissionACLData } from "@snip/database/types";
declare global {
    // eslint-disable-next-line no-var
    var mockConfig: MockConfig;
}

interface MockConfig {
    book_perms: Omit<
        PermissionACLData,
        "entity_id" | "resource_id" | "entity_type" | "resource_type"
    >;
    session: SnipSessionData;
    headers: Headers;
    service: {
        throw_error: boolean | string;
        error_message: string | undefined;
        isAdmin: boolean;
        not_found: boolean | string;
        email_verified: boolean;
    };
}

export const mockConfig: MockConfig = {
    book_perms: {
        id: -1,
        pRead: true,
        pWrite: true,
        pDelete: true,
        pACL: true,
    },
    session: {
        user: {
            id: 42,
            email_verified: true,
            created: Date.now(),
        },
        save: vi.fn(() => Promise.resolve()),
        destroy: vi.fn(),
        updateConfig: vi.fn(),
    },
    headers: new Headers(),
    service: {
        throw_error: false,
        error_message: undefined,
        isAdmin: false,
        not_found: false,
        email_verified: true,
    },
};
globalThis.mockConfig = mockConfig;

export function setConfig(config: MockConfig) {
    globalThis.mockConfig = config;
}

export function resetMockConfig() {
    globalThis.mockConfig.book_perms = {
        id: -1,
        pRead: true,
        pWrite: true,
        pDelete: true,
        pACL: true,
    };
    globalThis.mockConfig.session = {
        user: {
            id: 42,
            email_verified: true,
            created: Date.now(),
        },
        save: vi.fn(() => Promise.resolve()),
        destroy: vi.fn(),
        updateConfig: vi.fn(),
    };
    globalThis.mockConfig.headers = new Headers();
    globalThis.mockConfig.service.throw_error = false;
    globalThis.mockConfig.service.isAdmin = false;
    globalThis.mockConfig.service.not_found = false;
    globalThis.mockConfig.service.email_verified = true;
    globalThis.mockConfig.service.error_message = undefined;
    globalThis.mockConfig = mockConfig;
}

export function setSessionType(
    type: "user" | "ui_token" | "api_token" | "none",
) {
    if (type === "ui_token") {
        globalThis.mockConfig.session.anonymous = {
            tokens: [
                {
                    id: 1,
                    expires_at: Date.now() + 1000,
                    book_id: 1,
                },
            ],
        };
        delete globalThis.mockConfig.session.user;
        delete globalThis.mockConfig.session.api;
    }
    if (type === "api_token") {
        globalThis.mockConfig.session.api = {
            identifier: "test",
        };
        delete globalThis.mockConfig.session.user;
        delete globalThis.mockConfig.session.anonymous;
    }
    if (type === "user") {
        globalThis.mockConfig.session.user = {
            id: 42,
            email_verified: true,
            created: Date.now(),
        };
        delete globalThis.mockConfig.session.api;
        delete globalThis.mockConfig.session.anonymous;
    }
    if (type === "none") {
        delete globalThis.mockConfig.session.user;
        delete globalThis.mockConfig.session.api;
        delete globalThis.mockConfig.session.anonymous;
    }
}
