import { FormHTMLAttributes } from "react";
import { Modal as BootstrapModal } from "react-bootstrap";

import { Form } from ".";

import styles from "./Form.module.scss";

/** Generic modal for usage,
 * this applies the same style to all pages using
 * this component
 */
export default function ModalForm({
    children,
    show,
    onHide,
    ...props
}: FormHTMLAttributes<HTMLFormElement> & {
    show: boolean;
    onHide?: () => void;
}) {
    return (
        <BootstrapModal
            contentClassName={styles.modal}
            backdrop="static"
            fullscreen="sm-down"
            keyboard={false}
            show={show}
            onHide={onHide}
            centered
        >
            <Form data-glassmorphism="false" {...props}>
                {children}
            </Form>
        </BootstrapModal>
    );
}
