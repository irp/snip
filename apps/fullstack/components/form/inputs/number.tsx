import { HTMLProps, ReactNode, useEffect, useRef, useState } from "react";
import { Button, Form, InputGroup } from "react-bootstrap";
import { TbLock, TbLockOpen } from "react-icons/tb";

import { Input, InputFeedback, InputProps, ResetButton } from "./base";

import styles from "./inputs.module.scss";

/**
 * Renders a number input field.
 *
 * @param name - The name attribute of the input field.
 * @param label - The label for the input field.
 * @param props - Additional props to be passed to the input field.
 * @returns The rendered number input field.
 */
export function NumberInput({
    name = "number",
    label = "Number",
    ...props
}: InputProps) {
    return (
        <Input
            name={name}
            label={label}
            type="number"
            autoComplete="off"
            {...props}
        />
    );
}

/**
 * Renders a range input component.
 *
 * @param {InputProps} props - The input component props.
 * @param {string} props.name - The name of the input.
 * @param {string} props.label - The label for the input.
 * @param {boolean} props.isInvalid - Indicates if the input is invalid.
 * @param {boolean} props.isValid - Indicates if the input is valid.
 * @param {Function} props.onReset - The callback function to reset the input.
 * @param {string} props.invalidFeedback - The feedback message for invalid input.
 * @param {string} props.validFeedback - The feedback message for valid input.
 * @returns {JSX.Element} The rendered range input component.
 */
export function RangeInput({
    name = "range",
    label = "Range",
    ...props
}: InputProps) {
    //Pop the relevant props from the props object
    //and pass the rest of the props to the input element
    const {
        isInvalid,
        isValid,
        onReset,
        invalidFeedback,
        validFeedback,
        ...rest
    } = props;

    return (
        <Input
            name={name}
            label={label}
            type="range"
            as="div"
            isInvalid={isInvalid}
            isValid={isValid}
            invalidFeedback={invalidFeedback}
            validFeedback={validFeedback}
            onReset={onReset}
        >
            <RawRangeInput {...rest} />
        </Input>
    );
}

function RawRangeInput(props: InputProps) {
    const inputRef = useRef<HTMLInputElement>(null);

    const min_size_thumb = 0.7;
    const max_size_thumb = 1.2;
    function setThumbSize(size: number) {
        const input = inputRef.current;
        if (input) {
            const scaled_size = (size - 1) / (25 - 1);
            let size_thumb =
                scaled_size * (max_size_thumb - min_size_thumb) +
                min_size_thumb;
            if (size_thumb > max_size_thumb) {
                size_thumb = max_size_thumb;
            }
            input.style.setProperty("--size-thumb", size_thumb + "em");
        }
    }

    useEffect(() => {
        setThumbSize(props.value ? parseFloat(props.value.toString()) : 0);
    }, [props.value]);

    return (
        <input
            ref={inputRef}
            type="range"
            className={"form-range " + styles.range}
            {...props}
        />
    );
}

export function RawNumberInput({ ...props }: InputProps) {
    const inputRef = useRef<HTMLInputElement>(null);

    return (
        <input
            ref={inputRef}
            type="number"
            size={3}
            min={0}
            max={25}
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            onBeforeInput={(event: any) => {
                if (
                    event.key !== "Enter" &&
                    !/^\d*\.?\d{0,2}$/.test(event.data)
                ) {
                    event.preventDefault();
                }
            }}
            {...props}
        />
    );
}

export function JoinedRangeNumberInput({
    name = "range",
    label = "Range",
    isInvalid,
    isValid,
    invalidFeedback,
    validFeedback,
    onReset,
    className,
    ...props
}: InputProps) {
    return (
        <div className={className}>
            <InputGroup>
                <Form.Floating>
                    <Form.Control
                        id={name}
                        name={name}
                        placeholder=""
                        autoComplete={name}
                        as={"div"}
                        isInvalid={isInvalid}
                        isValid={isValid}
                        {...props}
                        className={styles.joinedRangeNumber}
                    >
                        <RawRangeInput {...props} />
                        <RawNumberInput {...props} />
                    </Form.Control>

                    <label htmlFor={name}>{label}</label>
                </Form.Floating>
                {onReset && <ResetButton onReset={onReset} />}
            </InputGroup>
            <InputFeedback
                isInvalid={isInvalid}
                isValid={isValid}
                invalidFeedback={invalidFeedback}
                validFeedback={validFeedback}
            />
        </div>
    );
}

export type JoinedNumbersInputProps = {
    names?: [string, string];
    label?: string;
    values?: [number, number];
    defaultValues?: [number, number];
    onReset?: () => void;
    onChange?: (first?: number, sec?: number) => void;
    onBlur?: (first?: number, sec?: number) => void;
    extraComponents?: ReactNode;
} & Omit<InputProps, "onChange" | "onBlur">;

export function JoinedNumbersInput({
    names = ["number1", "number2"],
    label = "JoinedNumbers",
    className,
    onReset,
    onChange,
    onBlur,
    values,
    defaultValues,
    extraComponents,
    ...props
}: JoinedNumbersInputProps) {
    let cn = styles.joinedNumbers;
    if (className) {
        cn += " " + className;
    }

    function blurHelper(x?: number, y?: number) {
        if (onBlur) {
            onBlur(x, y);
        }
    }
    function changeHelper(x?: number, y?: number) {
        if (onChange) {
            onChange(x, y);
        }
    }

    return (
        <div className={cn}>
            <InputGroup>
                <Form.Floating>
                    <Form.Control
                        id={names[0] + names[1]}
                        as={"div"}
                        className={styles.joinedNumbers}
                    >
                        <RawNumberInput
                            id={names[0]}
                            name={names[0]}
                            value={values ? values[0] : undefined}
                            defaultValue={
                                defaultValues ? defaultValues[0] : undefined
                            }
                            onInput={(
                                e: React.ChangeEvent<HTMLInputElement>,
                            ) => {
                                changeHelper(
                                    parseFloat(e.target.value),
                                    undefined,
                                );
                            }}
                            onKeyDown={(e: React.KeyboardEvent) => {
                                if (e.key == "Enter") {
                                    blurHelper(
                                        parseFloat(
                                            (e.target as HTMLInputElement)
                                                .value,
                                        ),
                                        undefined,
                                    );
                                }
                            }}
                            onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                                blurHelper(
                                    parseFloat(e.target.value),
                                    undefined,
                                );
                            }}
                            {...props}
                        />
                        <RawNumberInput
                            id={names[1]}
                            name={names[1]}
                            value={values ? values[1] : undefined}
                            defaultValue={
                                defaultValues ? defaultValues[1] : undefined
                            }
                            onInput={(
                                e: React.ChangeEvent<HTMLInputElement>,
                            ) => {
                                changeHelper(
                                    undefined,
                                    parseFloat(e.target.value),
                                );
                            }}
                            onKeyDown={(e: React.KeyboardEvent) => {
                                if (e.key == "Enter") {
                                    blurHelper(
                                        undefined,
                                        parseFloat(
                                            (e.target as HTMLInputElement)
                                                .value,
                                        ),
                                    );
                                }
                            }}
                            onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                                blurHelper(
                                    undefined,
                                    parseFloat(e.target.value),
                                );
                            }}
                            {...props}
                        />
                    </Form.Control>
                    <label>{label}</label>
                </Form.Floating>
                {extraComponents}
                {onReset && <ResetButton onReset={onReset} />}
            </InputGroup>
        </div>
    );
}

export interface LinkedNumbersInputPropsOld extends HTMLProps<HTMLDivElement> {
    labels: [string, string];
    names: [string, string];
    defaultValues: [number, number];
    handleChange: (values: [number, number], locked: boolean) => void;
    step?: number;
}

export type LinkedNumbersInputProps = {
    names?: [string, string];
    label?: string;
    values?: [number, number];
    defaultValues?: [number, number];
    onReset?: () => void;
    onChange?: (first?: number, sec?: number, locked?: boolean) => void;
    onBlur?: (first?: number, sec?: number, locked?: boolean) => void;
    defaultLocked?: boolean;
} & Omit<InputProps, "onChange" | "onBlur">;

/** Inputs which allow to
 * lock the aspect ratio
 * between two linked number inputs
 */
export function LinkedNumbersInput({
    names = ["number1", "number2"],
    label = "LinkedNumbers",
    values,
    defaultValues,
    className,
    onReset,
    onBlur,
    onChange,
    defaultLocked = false,
    ...props
}: LinkedNumbersInputProps) {
    let cn = styles.linkedNumbers;
    if (className) {
        cn += " " + className;
    }

    const [locked, setLocked] = useState(defaultLocked);

    function blurHelper(x?: number, y?: number) {
        if (onBlur) {
            onBlur(x, y, locked);
        }
    }
    function changeHelper(x?: number, y?: number) {
        if (onChange) {
            onChange(x, y, locked);
        }
    }

    return (
        <div className={cn}>
            <InputGroup>
                <Form.Floating>
                    <Form.Control
                        id={names[0] + names[1]}
                        as={"div"}
                        className={styles.joinedNumbers}
                    >
                        <RawNumberInput
                            id={names[0]}
                            name={names[0]}
                            value={values ? values[0] : undefined}
                            defaultValue={
                                defaultValues ? defaultValues[0] : undefined
                            }
                            onInput={(
                                e: React.ChangeEvent<HTMLInputElement>,
                            ) => {
                                changeHelper(e.target.valueAsNumber, undefined);
                            }}
                            onKeyDown={(e: React.KeyboardEvent) => {
                                if (e.key == "Enter") {
                                    blurHelper(
                                        parseFloat(
                                            (e.target as HTMLInputElement)
                                                .value,
                                        ),
                                        undefined,
                                    );
                                }
                            }}
                            onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                                blurHelper(
                                    parseFloat(e.target.value),
                                    undefined,
                                );
                            }}
                            {...props}
                        />
                        <Button
                            variant="none"
                            className={styles.lockBtn}
                            onClick={() => setLocked(!locked)}
                            type="button"
                        >
                            {locked ? <TbLock /> : <TbLockOpen />}
                        </Button>
                        <RawNumberInput
                            id={names[1]}
                            name={names[1]}
                            value={values ? values[1] : undefined}
                            defaultValue={
                                defaultValues ? defaultValues[1] : undefined
                            }
                            onInput={(
                                e: React.ChangeEvent<HTMLInputElement>,
                            ) => {
                                changeHelper(
                                    undefined,
                                    parseFloat(e.target.value),
                                );
                            }}
                            onKeyDown={(e: React.KeyboardEvent) => {
                                if (e.key == "Enter") {
                                    blurHelper(
                                        undefined,
                                        parseFloat(
                                            (e.target as HTMLInputElement)
                                                .value,
                                        ),
                                    );
                                }
                            }}
                            onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                                blurHelper(
                                    undefined,
                                    parseFloat(e.target.value),
                                );
                            }}
                            {...props}
                        />
                    </Form.Control>
                    <label>{label}</label>
                </Form.Floating>
                {onReset && <ResetButton onReset={onReset} />}
            </InputGroup>
        </div>
    );
}
