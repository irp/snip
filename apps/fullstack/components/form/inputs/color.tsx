"use client";
import { useEffect, useRef, useState } from "react";
import { Button } from "react-bootstrap";

import { Input, InputProps } from "./base";

import styles from "./inputs.module.scss";

type ColorInputProps = InputProps & {
    color: string;
    setColor: (color: string) => void;
    showHex?: boolean;
    fill?: boolean;
};

/** Color inputs is basically an input but
 * with a color picker
 *
 * Allows to specify an children which is
 * shown as icon in the input
 *
 * Additionally the the hex value of the color
 */
export function ColorInput({
    color,
    setColor,
    name = "color",
    label = "Color",
    showHex = false,
    fill = false,
    height = "auto",
    ...props
}: ColorInputProps) {
    const inputRef = useRef<HTMLInputElement>(null);

    const hex_color = useHexColor(color);

    return (
        <Input
            name={name}
            label={label}
            as="div"
            style={{
                width: "100%",
                height: "100%",
                borderTopLeftRadius: "var(--bs-border-radius)",
                borderBottomLeftRadius: "var(--bs-border-radius)",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                position: "relative",
            }}
            className={styles.colorInput}
            {...props}
        >
            <div
                style={{ display: "flex", width: "100%", position: "relative" }}
            >
                <input
                    ref={inputRef}
                    type="color"
                    style={{
                        opacity: 0,
                        position: "absolute",
                        width: "100%",
                        padding: 0,
                        left: 0,
                    }}
                    onChange={(e) => {
                        const color = e.target.value;
                        setColor(color);
                    }}
                />
                <Button
                    variant="none"
                    className={styles.color_add}
                    style={{
                        backgroundColor: color,
                        width: fill ? "100%" : "auto",
                        height: height,
                        zIndex: 10,
                    }}
                    onClick={() => {
                        inputRef.current?.click();
                        inputRef.current?.focus();
                    }}
                />
            </div>
            {
                // Show hex value of color
                showHex && <div className={styles.hex}>{hex_color}</div>
            }
        </Input>
    );
}

type MulticolorInputProps = InputProps & {
    colorList: string[];
    setColorList: (colors: string[]) => void;
    currentColorIdx: number;
    setCurrentColorIdx: (idx: number) => void;
    showHex?: boolean;
};

/** The multicolor input is a color picker
 * which allows to select multiple colors
 * in a list. Of which one is active!
 *
 * Allows to specify an children which is
 * shown as icon in the input
 *
 * Additionally the the hex value of the color
 */
export function MultiColorInput({
    colorList,
    setColorList,
    currentColorIdx,
    setCurrentColorIdx,
    showHex = false,
    name = "colors",
    label = "Colors",
    ...props
}: MulticolorInputProps) {
    // Invisible color selector input
    const inputRef = useRef<HTMLInputElement>(null);

    const hex_color = useHexColor(colorList[currentColorIdx]!);

    return (
        <>
            <Input
                id={name}
                type="color"
                as="div"
                style={{
                    width: "100%",
                    height: "100%",
                    borderTopLeftRadius: "var(--bs-border-radius)",
                    borderBottomLeftRadius: "var(--bs-border-radius)",
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                }}
                name={name}
                label={label}
                {...props}
            >
                <div className={styles.colors}>
                    {colorList.map((color, i) => {
                        return (
                            <Button
                                variant="outline-dark"
                                key={color}
                                data-active={i === currentColorIdx}
                                className={styles.color}
                                style={{
                                    backgroundColor: color,
                                }}
                                onClick={() => {
                                    setCurrentColorIdx(i);
                                }}
                                onDoubleClick={() => {
                                    setColorList(
                                        colorList.filter((_, j) => i !== j),
                                    );
                                }}
                            ></Button>
                        );
                    })}
                    <Button
                        variant="none"
                        className={styles.color_add}
                        onClick={() => {
                            inputRef.current?.click();
                            inputRef.current?.focus();
                        }}
                    >
                        +
                        <input
                            ref={inputRef}
                            type="color"
                            style={{
                                position: "absolute",
                                zIndex: -1,
                                opacity: 0,
                                height: "100%",
                            }}
                            onChange={(e) => {
                                const color = e.target.value;
                                setColorList([...colorList, color]);
                            }}
                        />
                    </Button>
                </div>
                {
                    // Show hex value of color
                    showHex && <div className={styles.hex}>{hex_color}</div>
                }
            </Input>
        </>
    );
}

/** Simple text input without padding
 *
 */
function useHexColor(str: string) {
    const [color, setColor] = useState(str);

    useEffect(() => {
        const ctx = document.createElement("canvas").getContext("2d")!;
        ctx.fillStyle = str;
        setColor(ctx.fillStyle);
    }, [str]);

    return color;
}
