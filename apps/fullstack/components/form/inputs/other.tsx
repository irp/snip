import { Input, InputProps } from "./base";

export function DateTimeInput({
    name = "datetime-local",
    label = "Date & Time",
    ...props
}: InputProps) {
    return <Input name={name} label={label} type="datetime-local" {...props} />;
}
