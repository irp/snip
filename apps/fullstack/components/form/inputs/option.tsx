import { OptionHTMLAttributes, ReactNode } from "react";
import {
    Dropdown,
    DropdownItemProps,
    DropdownMenuProps,
    Form,
} from "react-bootstrap";

import { Input, InputProps } from "./base";

import styles from "./inputs.module.scss";

type Option = OptionHTMLAttributes<HTMLOptionElement> & { label: string };

interface OptionInputProps extends Omit<InputProps, "onSelect"> {
    options: Option[];
}

/**
 * Renders a select input with options if styling for
 * the options is needed have a look at the dropdown input.
 *
 * @param {OptionInputProps} props - The props for the OptionInput component.
 * @param {string} [props.name="select"] - The name of the input.
 * @param {string} [props.label="Select"] - The label for the input.
 * @param {Array<Option>} props.options - The options to be rendered in the select input.
 * @returns {JSX.Element} The rendered OptionInput component.
 */
export function OptionInput({
    name = "select",
    label = "Select",
    options,
    ...props
}: OptionInputProps) {
    return (
        <Input name={name} label={label} as={Form.Select} {...props}>
            {options.map(({ label, ...rest }, index) => (
                <option key={index} {...rest}>
                    {label}
                </option>
            ))}
        </Input>
    );
}

export type DropdownItem = DropdownItemProps & {
    label: ReactNode;

    // label that is show on select
    labelSelect?: ReactNode;
};

interface DropdownInputProps extends Omit<InputProps, "onSelect"> {
    options: DropdownItem[];
    onSelect: (
        eventKey: string | null,
        e: React.SyntheticEvent<unknown>,
    ) => void;
    menuProps?: DropdownMenuProps;
}

export function DropdownInput({
    name = "dropdown",
    label = "Dropdown",
    options = [],
    onSelect,
    value,
    menuProps,
    ...props
}: DropdownInputProps) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (props as any).variant = "none";
    return (
        <Dropdown
            className={styles.dropdown}
            defaultValue={props.defaultValue}
            onSelect={(val, e) => {
                e?.stopPropagation(); // Prevent TabContainer from reacting
                if (onSelect) onSelect(val, e);
            }}
        >
            <Input
                name={name}
                label={label}
                as={Dropdown.Toggle}
                className={styles.toggle}
                {...props}
            >
                {options.find((option) => option.eventKey === value)?.label}
            </Input>
            <Dropdown.Menu
                className={styles.menu}
                {...menuProps}
                onClick={(e) => {
                    e.preventDefault();
                }}
            >
                {options.map(
                    ({ label, labelSelect, eventKey, ...props }, index) => (
                        <Dropdown.Item
                            key={index}
                            className={styles.item}
                            disabled={eventKey === value}
                            eventKey={eventKey}
                            as="button"
                            {...props}
                            onClick={(e) => {
                                e.preventDefault();
                            }}
                        >
                            {labelSelect ?? label}
                        </Dropdown.Item>
                    ),
                )}
            </Dropdown.Menu>
        </Dropdown>
    );
}

export function CheckInput({
    name = "check",
    label = "Check me",
    checked,
    ...props
}: Omit<InputProps, "type">) {
    return (
        <div className="px-2">
            <Form.Check id={name} checked={checked} label={label} {...props} />
        </div>
    );
}
