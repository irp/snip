"use client";
import { useState } from "react";
import { IoEye, IoEyeOff } from "react-icons/io5";

import { Input, InputProps } from "./base";

import styles from "./inputs.module.scss";

/**
 * Renders a text input component.
 *
 * @param name - The name of the input field.
 * @param label - The label for the input field.
 * @param props - Additional props to be passed to the input component.
 * @returns The rendered text input component.
 */
export function TextInput({
    name = "text",
    label = "text",
    ...props
}: InputProps) {
    return <Input name={name} label={label} type="text" {...props} />;
}

/**
 * Renders a long text input component.
 *
 * @param name - The name of the input field.
 * @param label - The label for the input field.
 * @param props - Additional props for the input field.
 * @returns The rendered long text input component.
 */
export function LongTextInput({
    name = "longtext",
    label = "Comment",
    ...props
}: InputProps) {
    return (
        <Input name={name} label={label} type="text" as="textarea" {...props} />
    );
}

/**
 * Renders an email input field.
 *
 * @param name - The name attribute of the input field.
 * @param label - The label text for the input field.
 * @param invalidFeedback - The feedback message to display when the input is invalid.
 * @param validFeedback - The feedback message to display when the input is valid.
 * @param props - Additional props to pass to the TextInputNew component.
 * @returns The rendered EmailInputNew component.
 */
export function EmailInput({
    name = "email",
    label = "Email",
    invalidFeedback = "Invalid email",
    validFeedback = "Looks good!",
    ...props
}: InputProps) {
    return (
        <Input
            name={name}
            label={label}
            invalidFeedback={invalidFeedback}
            validFeedback={validFeedback}
            type="email"
            {...props}
        />
    );
}

/**
 * Renders a password input field with customizable validation feedback.
 *
 * @param name - The name attribute of the input field.
 * @param label - The label text for the input field.
 * @param required - Indicates whether the input field is required.
 * @param isValid - Indicates whether the input field is valid.
 * @param isInvalid - Indicates whether the input field is invalid.
 * @param invalidFeedback - The validation feedback message to display when the input field is invalid.
 * @param validFeedback - The validation feedback message to display when the input field is valid.
 * @returns The rendered password input field.
 */
export function PasswordInput({
    name = "password",
    label = "Password",
    required = false,
    isValid = false,
    isInvalid = false,
    invalidFeedback = "Invalid password, must be at least 10 characters long and contain at least one special character.",
    validFeedback = "Looks good!",
    ...props
}: InputProps) {
    const [showPassword, setShowPassword] = useState(false);

    return (
        <div
            style={{
                position: "relative",
            }}
        >
            <TextInput
                name={name}
                label={label}
                required={required}
                isValid={isValid}
                isInvalid={isInvalid}
                invalidFeedback={invalidFeedback}
                validFeedback={validFeedback}
                type={showPassword ? "text" : "password"}
                {...props}
            ></TextInput>
            <div
                className={styles.showPassword}
                onClick={() => setShowPassword(!showPassword)}
            >
                {showPassword ? <IoEye /> : <IoEyeOff />}
            </div>
        </div>
    );
}
