import { InputHTMLAttributes, ReactNode, useState } from "react";
import Button, { ButtonProps } from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { FormControlProps } from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import { BsArrowCounterclockwise } from "react-icons/bs";

import styles from "./inputs.module.scss";

interface ExtraProps {
    invalidFeedback?: string;
    validFeedback?: string;
    label?: ReactNode;
    onReset?: () => void;
    more?: ReactNode;
}

export type InputProps = FormControlProps &
    ExtraProps &
    (
        | InputHTMLAttributes<HTMLInputElement>
        | InputHTMLAttributes<HTMLTextAreaElement>
    );
/**
 * Renders an input component.
 *
 * @param {InputProps} props - The input component props.
 * @returns {JSX.Element} The rendered input component.
 */
export function Input({
    name,
    label,
    invalidFeedback,
    validFeedback,
    type,
    as = "input",
    children,
    onReset,
    isInvalid,
    isValid,
    more,
    ...props
}: InputProps) {
    return (
        <div>
            <InputGroup>
                <Form.Floating>
                    <Form.Control
                        id={name}
                        name={name}
                        type={type}
                        placeholder=""
                        autoComplete={name}
                        as={as}
                        isInvalid={isInvalid}
                        isValid={isValid}
                        {...props}
                    >
                        {children}
                    </Form.Control>
                    <label htmlFor={name}>{label}</label>
                </Form.Floating>
                {onReset && <ResetButton onReset={onReset} />}
                {more}
            </InputGroup>
            <InputFeedback
                isInvalid={isInvalid}
                isValid={isValid}
                invalidFeedback={invalidFeedback}
                validFeedback={validFeedback}
            />
        </div>
    );
}

export function InputFeedback({
    isInvalid,
    isValid,
    invalidFeedback,
    validFeedback,
}: {
    isInvalid?: boolean;
    isValid?: boolean;
    invalidFeedback: React.ReactNode;
    validFeedback: React.ReactNode;
}) {
    return (
        <>
            <Form.Control.Feedback
                type="invalid"
                className={styles.invalid}
                data-active={isInvalid}
            >
                {invalidFeedback}
            </Form.Control.Feedback>
            <Form.Control.Feedback
                type="valid"
                className={styles.valid}
                data-active={isValid}
            >
                {validFeedback}
            </Form.Control.Feedback>
        </>
    );
}

export function ResetButton({
    onReset,
    ...props
}: ButtonProps & { onReset: () => void }) {
    const [transition, setTransition] = useState(false);

    return (
        <Button
            variant="none"
            className={styles.reset}
            onClick={() => {
                // add value to event
                onReset();
                setTransition(true);
                setTimeout(() => {
                    setTransition(false);
                }, 500);
            }}
            type="button"
            {...props}
        >
            <BsArrowCounterclockwise data-active={transition} />
        </Button>
    );
}

export function setInputFilter(
    textbox: Element,
    inputFilter: (value: string) => boolean,
): () => void {
    function filter(
        this: (HTMLInputElement | HTMLTextAreaElement) & {
            oldValue: string;
            oldSelectionStart: number | null;
            oldSelectionEnd: number | null;
        },
    ) {
        if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
        } else if (Object.prototype.hasOwnProperty.call(this, "oldValue")) {
            this.value = this.oldValue;

            if (
                this.oldSelectionStart !== null &&
                this.oldSelectionEnd !== null
            ) {
                this.setSelectionRange(
                    this.oldSelectionStart,
                    this.oldSelectionEnd,
                );
            }
        } else {
            this.value = "";
        }
    }

    // Add events
    [
        "input",
        "keydown",
        "keyup",
        "mousedown",
        "mouseup",
        "select",
        "contextmenu",
        "drop",
        "focusout",
    ].forEach(function (event) {
        textbox.addEventListener(event, filter);
    });

    return function () {
        [
            "input",
            "keydown",
            "keyup",
            "mousedown",
            "mouseup",
            "select",
            "contextmenu",
            "drop",
            "focusout",
        ].forEach(function (event) {
            textbox.removeEventListener(event, filter);
        });
    };
}
