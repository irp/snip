import styles from "./items.module.scss";

/** Basically a small item within a form.
 *
 * Mostly a wrapper for a title, description, and children.
 * I.e. divs with applied styling.
 */
export function Item({
    title,
    description,
    children,
    className,
}: {
    title: React.ReactNode;
    description: React.ReactNode;
    children: React.ReactNode;
    className?: string;
}) {
    return (
        <div className={styles.item + " " + className}>
            <Title>{title}</Title>
            <Description>{description}</Description>
            <Content>{children}</Content>
        </div>
    );
}

export function Title({ children }: { children: React.ReactNode }) {
    return <div className={styles.title}>{children}</div>;
}

export function Description({ children }: { children: React.ReactNode }) {
    return <div className={styles.description}>{children}</div>;
}

export function Content({ children }: { children: React.ReactNode }) {
    return <div className={styles.content}>{children}</div>;
}
