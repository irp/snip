import {
    ButtonHTMLAttributes,
    ForwardedRef,
    forwardRef,
    MouseEventHandler,
    useState,
} from "react";
import { Button, ButtonProps } from "react-bootstrap";
import { useFormStatus } from "react-dom";

export function SubmitButton({
    children,
    pending,
    ...props
}: ButtonHTMLAttributes<HTMLButtonElement> & { pending?: boolean }) {
    const { pending: form_pending } = useFormStatus();

    // Some reasonable defaults
    if (!props.className) {
        props.className = "btn btn-primary m-1";
    }
    if (!props.type) {
        props.type = "submit";
    }

    return (
        <button {...props} disabled={form_pending || pending || props.disabled}>
            {form_pending || pending ? (
                <>
                    <span
                        className="spinner-grow spinner-grow-sm"
                        role="status"
                        aria-hidden="true"
                    ></span>
                </>
            ) : (
                children
            )}
        </button>
    );
}

export function SecondaryButton({
    children,
    ...props
}: ButtonHTMLAttributes<HTMLButtonElement>) {
    if (!props.className) {
        props.className = "btn btn-outline-dark m-1";
    }
    if (!props.type) {
        props.type = "button";
    }

    return <button {...props}>{children}</button>;
}

export const ButtonWithConfirm = forwardRef(function ButtonWithConfirm(
    {
        onConfirmed,
        children,
        confirmFeedback = "Sure?",
        timeout = 3000,
        ...props
    }: ButtonProps & {
        onConfirmed: MouseEventHandler<HTMLButtonElement>;
        children: React.ReactNode;
        confirmFeedback?: React.ReactNode;
        timeout?: number;
    },
    ref: ForwardedRef<HTMLButtonElement>,
) {
    const [confirmed, setConfirmed] = useState(false);

    const handleConfirm = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        if (confirmed) {
            onConfirmed(e);
            setConfirmed(false);
        } else {
            setConfirmed(true);
        }

        setTimeout(() => {
            setConfirmed(false);
        }, timeout);
    };

    return (
        <>
            {confirmed ? (
                <Button
                    ref={ref}
                    variant="danger"
                    {...props}
                    onClick={handleConfirm}
                >
                    {confirmFeedback}
                </Button>
            ) : (
                <Button
                    ref={ref}
                    variant="outline-danger"
                    {...props}
                    onClick={handleConfirm}
                >
                    {children}
                </Button>
            )}
        </>
    );
});
