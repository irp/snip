import { ReactNode } from "react";

import styles from "./Form.module.scss";

interface MessageProps extends React.HTMLAttributes<HTMLDivElement> {
    children: ReactNode;
    onClose?: () => void;
}

/** Returns an optional form error component
 * if no children are given or the children
 * resolve to null|false nothing is
 * returned
 */
export function FormError({ children, ...props }: MessageProps) {
    if (!children) {
        return;
    }

    return (
        <Alert className="alert alert-danger" role="alert" {...props}>
            {children}
        </Alert>
    );
}

export const FormSuccess = ({ children, ...props }: MessageProps) => {
    if (!children) {
        return;
    }

    return (
        <Alert className="alert alert-success" role="alert" {...props}>
            {children}
        </Alert>
    );
};

/** Returns an optional form warning component
 * if no children are given or the children
 * resolve to null|false nothing is
 * returned
 */
export function FormWarn({ children, ...props }: MessageProps) {
    if (!children) {
        return;
    }

    return (
        <Alert className="alert alert-warning" role="alert" {...props}>
            {children}
        </Alert>
    );
}

/** Returns an optional form info component
 * if no children are given or the children
 * resolve to null|false nothing is
 * returned
 */
export function FormInfo({ children, ...props }: MessageProps) {
    if (!children) {
        return;
    }

    return (
        <Alert className="alert alert-info" role="alert" {...props}>
            {children}
        </Alert>
    );
}

function Alert({ children, ...props }: MessageProps) {
    if (!children) {
        return;
    }

    return (
        <div {...props}>
            {children}
            {
                // Close button
                props.onClose && (
                    <button
                        type="button"
                        className={"btn-close " + styles.close_btn}
                        aria-label="Close"
                        onClick={props.onClose}
                    ></button>
                )
            }
        </div>
    );
}
