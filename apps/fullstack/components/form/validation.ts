/** Validate an password
 *
 * @param password Password to validate
 *
 * @returns true if password is valid, false otherwise
 *
 * @remarks
 * ( # Start of group
 *     (?=.*[a-zA-Z])    #   must contains one lowercase/uppercase characters
 *     (?=.*[\W])        #   must contains at least one special character
 *     .                 #   match anything with previous condition checking
 *     {10,}             #   length at least 10 characters and maximum of 50
 * )  # End of group
 */
export const validatePW = (pw: string) => {
    const regex = /^((?=.*[a-zA-Z])(?=.*[\W]).{10,})$/;
    if (regex.test(pw)) {
        return true;
    } else {
        return false;
    }
};

/** Validate an email address
 *
 * @param email Email address to validate
 *
 * @returns True if email is valid, false otherwise
 *
 * @see https://stackoverflow.com/questions/201323/how-can-i-validate-an-email-address-using-a-regular-expression
 */
export const validateEmail = (email: string) => {
    const regex =
        // eslint-disable-next-line no-control-regex
        /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    if (regex.test(email)) {
        return true;
    } else {
        return false;
    }
};

// Book validation
export const validateTitle = (title: string) => {
    const regex = /^[a-zA-Z0-9 ]+$/;
    if (regex.test(title)) {
        return true;
    } else {
        return false;
    }
};

// Group validation
export const validateGroup = (group: string) => {
    const regex = /^[a-zA-Z0-9 ]+$/;
    if (regex.test(group)) {
        return true;
    } else {
        return false;
    }
};
