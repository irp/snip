import { FormHTMLAttributes, ReactNode } from "react";

import styles from "./Form.module.scss";

export function FormHeader({ children }: { children: ReactNode }) {
    return <div className={styles.header}>{children}</div>;
}

export function Form({
    children,
    ...props
}: FormHTMLAttributes<HTMLFormElement>) {
    if (!props.className) {
        props.className = styles.form;
    }

    return <form {...props}>{children}</form>;
}
