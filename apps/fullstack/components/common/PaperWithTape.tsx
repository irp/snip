import { HTMLAttributes } from "react";

import styles from "./PaperWithTape.module.scss";

interface PaperWithTapeWrapperProps extends HTMLAttributes<HTMLDivElement> {
    children: React.ReactNode;
    color?: "blue";
    show?: boolean;
}

export function PaperWithTapeWrapper({
    children,
    color = "blue",
    show = true,
    ...props
}: PaperWithTapeWrapperProps) {
    let cStyle: string | undefined = "";
    if (color === "blue") {
        cStyle = styles.blue;
    }

    return (
        <div
            className={styles.paper + " " + cStyle}
            {...props}
            style={{ display: show ? "block" : "none", ...props.style }}
        >
            <div className={styles.tapeTop} />
            {children}
        </div>
    );
}

export function PaperWithTapeHeader({
    children,
    ...props
}: HTMLAttributes<HTMLDivElement>) {
    return (
        <div className={styles.header} role="heading" aria-level={3} {...props}>
            {children}
        </div>
    );
}

export function PaperWithTapeContent({
    children,
    ...props
}: HTMLAttributes<HTMLDivElement>) {
    return (
        <div className={styles.content} {...props}>
            {children}
        </div>
    );
}

const PaperWithTape = {
    Wrapper: PaperWithTapeWrapper,
    Heading: PaperWithTapeHeader,
    Content: PaperWithTapeContent,
};

export default PaperWithTape;
