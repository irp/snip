"use client";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { Button } from "react-bootstrap";

import { NavbarDropdown } from "./Navbar";

import BrandImage from "@img/logo.png";

import styles from "./Header.module.scss";

export default function Header() {
    const router = useRouter();
    return (
        <header>
            <nav
                id={styles.header}
                className="navbar bg-primary navbar-expand text-light navbar-dark"
            >
                <Logo />
                <div className="me-auto" />
                <NavbarDropdown />
                <Button type="button" onClick={() => router.back()}>
                    Back
                </Button>
            </nav>
        </header>
    );
}

export function Logo() {
    return (
        <Link href="/frontpage" id={styles.logo}>
            <Image
                src={BrandImage}
                width={40}
                height={40}
                alt="logo"
                style={{ objectFit: "contain" }}
                priority
            />
            <div>Snip</div>
        </Link>
    );
}
