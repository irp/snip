import { logout } from "lib/actions/logout";
import { useActivePath } from "lib/hooks/useActivePath";
import useUser from "lib/hooks/useUser";
import Link from "next/link";
import { useRouter } from "next/navigation";

import { books_svg, logout_svg } from "./svgs";

import styles from "./Header.module.scss";

/** We differentiation between
 * two navigation bars
 *
 * One for guests and one for users
 *
 */
export type NavigationItem = {
    label: string;
    href: string;
    icon?: JSX.Element;

    // We might want to add more conditions in the future
    // for now only the condition of being an admin is supported
    // (only sidebar)
    condition?: "admin";
};

const navigationGuest: NavigationItem[] = [
    {
        label: "Home",
        href: "/frontpage",
        icon: <i className="icon bi-house"></i>,
    },
    {
        label: "Login",
        href: "/account/login",
        icon: <i className="icon bi-person"></i>,
    },
    {
        label: "SignUp",
        href: "/account/signup",
        icon: <i className="icon bi-person-plus"></i>,
    },
];

const navigationUser: NavigationItem[] = [
    { label: "Books", href: "/books", icon: books_svg },
    {
        label: "Account",
        href: "/account",
        icon: <i className="icon bi-person" />,
    },
];

export function NavBar({ className, id }: { className?: string; id: string }) {
    const isActivePath = useActivePath();
    const router = useRouter();
    const { isLoggedIn, mutate } = useUser();

    const navigation = isLoggedIn ? navigationUser : navigationGuest;

    return (
        <div className={className} id={id}>
            <nav>
                {navigation.map(({ href, label, icon }, index) => (
                    <Link key={index} href={href}>
                        <button
                            data-active={isActivePath(href) ? "true" : "false"}
                        >
                            {label}
                            {icon}
                        </button>
                    </Link>
                ))}
                {/*Special case for logged in users*/}
                {isLoggedIn && (
                    <>
                        <hr style={{ width: "90%", marginBlock: "0.25rem" }} />
                        <button
                            onClick={async () => {
                                await logout();
                                mutate(undefined);
                                // Refresh the current page
                                router.refresh();
                            }}
                        >
                            Logout
                            {logout_svg}
                        </button>
                    </>
                )}
            </nav>
        </div>
    );
}

export function NavbarDropdown() {
    return (
        <div className="dropdown" id={styles.dropdown}>
            <button
                id={styles.dropdown_toggle}
                className="nav-link"
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
            >
                <i className="bi bi-list" />
            </button>

            <NavBar
                className="dropdown-menu dropdown-menu-end"
                id={styles.dropdown_menu!}
            />
        </div>
    );
}
