"use client";
import { useDebounce } from "lib/hooks/useDebounce";
import { useCallback, useEffect, useRef, useState } from "react";
import { Button, Spinner } from "react-bootstrap";
import Modal from "react-bootstrap/esm/Modal";

import { useSocketContext } from "components/context/useSocketContext";

import styles from "./ConnectionIssues.module.scss";

interface Latency {
    mean: number;
    deviation: number;
    invalidMeasures: number;
    validMeasures: number;
}

export function useLatency(bufferLength: number = 100) {
    const { socket, isConnected } = useSocketContext();
    const [intervalTime, setIntervalTime] = useState(2000);
    const history = useRef<Float64Array>(
        new Float64Array(bufferLength).fill(-1),
    );
    const [latency, setLatency] = useState<Latency>({
        mean: -1,
        deviation: -1,
        invalidMeasures: 0,
        validMeasures: 0,
    });

    /** Get latency of socket connection
     *
     * Returns the timeout value if the socket is not connected.
     */
    const getLatency = useCallback(
        (timeout: number = 5000) => {
            return new Promise<number>((resolve) => {
                const start = performance.now();
                if (!socket) return resolve(timeout);
                socket?.emit("ping", () => {
                    const end = performance.now();
                    resolve(end - start);
                });
                setTimeout(() => resolve(timeout), timeout);
            });
        },
        [socket],
    );

    /** Measure interval */
    useEffect(() => {
        if (!isConnected) return;

        let i = 0;
        const int = setInterval(() => {
            const j = i; //copy
            getLatency().then((latency) => {
                history.current[j] = latency;

                // Calculate mean and deviation
                let sum = 0;
                let sum2 = 0;
                let count = 0;
                for (const l of history.current) {
                    if (l !== -1) {
                        sum += l;
                        sum2 += l * l;
                        count += 1;
                    }
                }
                const mean = sum / count;
                const deviation = Math.sqrt(sum2 / count - mean ** 2);
                setLatency({
                    mean,
                    deviation,
                    invalidMeasures: bufferLength - count,
                    validMeasures: count,
                });
            });

            i += 1;
            if (i >= bufferLength) {
                i = 0;
            }
        }, intervalTime);

        return () => clearInterval(int);
    }, [bufferLength, getLatency, intervalTime, isConnected]);

    return {
        latency,
        intervalTime,
        setIntervalTime,
    };
}

/** Component that is shown when the connection to the server is lost.
 *
 * There are two different cases:
 * - Connection is completely lost
 * - Ping or package loss is too high
 *
 * At the moment if the connection is lost forces the user to
 * reload the page.
 */
export function LatencyModal() {
    const [ignore, setIgnore] = useState(false);
    const { isConnected } = useSocketContext();
    const { latency, setIntervalTime } = useLatency();

    const isConnectedDebounced = useDebounce(isConnected, 5000);

    async function runTest() {
        return new Promise<void>((resolve) => {
            setIntervalTime(10);
            setTimeout(() => {
                setIntervalTime(2000);
                resolve();
            }, 5000);
        });
    }

    const badLatency =
        latency.validMeasures > 50 &&
        (latency.mean > 100 || latency.deviation > 50);

    return (
        <Modal
            show={(isConnectedDebounced === false || badLatency) && !ignore}
            backdrop="static"
            className={styles.latencyModal}
        >
            <Modal.Body className={styles.body}>
                {!isConnectedDebounced && <ReconnectForm />}
                {badLatency && (
                    <BadLatency
                        latency={latency}
                        runTest={runTest}
                        onIgnore={() => setIgnore(true)}
                    />
                )}
            </Modal.Body>
        </Modal>
    );
}

/** Shown if connection was lost! */
function ReconnectForm() {
    return (
        <>
            <div className={styles.alert}>Server connection was lost!</div>
            <Button
                variant="outline-primary"
                onClick={() => window.location.reload()}
            >
                Reload
            </Button>
        </>
    );
}

/** Shown if bad latency is detected */
function BadLatency({
    latency,
    runTest,
    onIgnore,
}: {
    latency: Latency;
    runTest?: () => Promise<void>;
    onIgnore?: () => void;
}) {
    const [loading, setLoading] = useState(false);

    return (
        <>
            <div className={styles.alert}>High latency detected!</div>
            <div>
                {latency.mean.toFixed(2)} ± {latency.deviation.toFixed(2)} ms
                (n={latency.validMeasures})
            </div>
            {runTest && (
                <Button
                    variant="outline-primary"
                    onClick={() => {
                        setLoading(true);
                        runTest().finally(() => setLoading(false));
                    }}
                    style={{ minWidth: "5rem" }}
                    disabled={loading}
                >
                    {loading ? <Spinner size="sm" /> : "Run test"}
                </Button>
            )}
            <Button variant="outline-primary" onClick={onIgnore}>
                Ignore
            </Button>
        </>
    );
}
