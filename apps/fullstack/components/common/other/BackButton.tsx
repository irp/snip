"use client";

import { useRouter } from "next/navigation";
import { Button, ButtonProps } from "react-bootstrap";

export default function BackButton({
    children,
    ...kwargs
}: {
    children?: React.ReactNode;
} & ButtonProps) {
    const router = useRouter();

    return (
        <Button {...kwargs} onClick={() => router.back()}>
            {children || "Back"}
        </Button>
    );
}
