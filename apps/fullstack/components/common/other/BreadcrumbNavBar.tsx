"use client";
import Link from "next/link";
import { useSelectedLayoutSegment } from "next/navigation";
import React from "react";
import { Breadcrumb } from "react-bootstrap"; // Assuming you are using react-bootstrap

interface BreadcrumbItem {
    href: string;
    label: string;
    ariaLabel: string;
    match: string | null;
    target?: string;
}

export default function BreadCrumbNavBar({
    children,
}: {
    children: BreadcrumbItem[];
}) {
    const active_segment = useSelectedLayoutSegment();

    return (
        <Breadcrumb>
            {children.map((item, i) => (
                <Breadcrumb.Item
                    key={i}
                    linkAs={Link}
                    href={item.href}
                    active={active_segment === item.match}
                    title={item.ariaLabel}
                    target={item.target}
                >
                    {item.label}
                </Breadcrumb.Item>
            ))}
        </Breadcrumb>
    );
}
