import React from "react";
import Button from "react-bootstrap/esm/Button";
import { AiOutlinePlusSquare } from "react-icons/ai";
import { FaBars } from "react-icons/fa";
import { FiShare } from "react-icons/fi";

import PaperWithTape from "components/common/PaperWithTape";

interface Props {
    closePrompt: () => void;
    doNotShowAgain: () => void;
}

export default function AddToMobileFirefoxIos(props: Props) {
    const { closePrompt, doNotShowAgain } = props;

    return (
        <PaperWithTape.Wrapper show={true}>
            <PaperWithTape.Heading>
                Add SNIP to your Home Screen
            </PaperWithTape.Heading>
            <PaperWithTape.Content>
                <div>
                    For the best experience, we highly recommend installing SNIP
                    to your home screen! Without it, you might miss out on a few
                    features and run into issue with the editor — apparently
                    Apple likes to make things difficult for developers and
                    users alike.
                </div>
                <div className="d-flex align-items-center justify-content-center flex-column  border border-1 rounded-3 p-2 gap-2 my-2 w-100">
                    <div className="d-flex gap-1 text-center">
                        <div>Click the</div>
                        <FaBars size={24} />
                        <div>icon</div>
                    </div>
                    <div className="d-flex flex-column align-items-center w-100 text-center gap-2">
                        <div>Scroll down and then click:</div>
                        <div className="d-flex align-items-center border border-1 px-2 py-1 gap-1 rounded-3">
                            <div>Share</div>
                            <FiShare size={24} />
                        </div>
                        <div>Then click:</div>
                        <div className="d-flex align-items-center border border-1 px-2 py-1 gap-1 rounded-3">
                            <div>Add to Home Screen</div>
                            <AiOutlinePlusSquare size={24} />
                        </div>
                    </div>
                </div>
                <div className="d-flex justify-content-between w-100">
                    <Button
                        className="p-1 d-flex align-items-center gap-1"
                        onClick={doNotShowAgain}
                        variant="outline-primary"
                    >
                        Don&apos;t show again
                    </Button>
                    <Button
                        className="p-1 d-flex align-items-center gap-1"
                        onClick={closePrompt}
                        variant="outline-primary"
                    >
                        Close
                    </Button>
                </div>
            </PaperWithTape.Content>
        </PaperWithTape.Wrapper>
    );
}
