import Link from "next/link";
import React from "react";
import Button from "react-bootstrap/esm/Button";

import PaperWithTape from "components/common/PaperWithTape";

interface Props {
    closePrompt: () => void;
    doNotShowAgain: () => void;
}

export default function AddToOtherBrowser(props: Props) {
    const { closePrompt, doNotShowAgain } = props;
    const searchUrl = `https://www.google.com/search?q=add+to+home+screen+for+common-mobile-browsers`;

    return (
        <PaperWithTape.Wrapper show={true}>
            <PaperWithTape.Heading>
                Add SNIP to your Home Screen
            </PaperWithTape.Heading>
            <PaperWithTape.Content>
                <div>
                    For the best experience, we recommend installing SNIP to
                    your home screen! This enables enhanced features and
                    functionality for mobile users.
                </div>
                <div className="d-flex align-items-center justify-content-center flex-column border border-1 rounded-3 p-2 gap-2 my-2 w-100">
                    <div className="d-flex gap-1 text-center flex-column">
                        <div>
                            Unfortunately, we were unable to determine which
                            browser you are using. Please search for how to
                            install a web app for your browser.
                        </div>
                        <Link
                            className="text-blue-300"
                            href={searchUrl}
                            target="_blank"
                        >
                            Try This Search
                        </Link>
                    </div>
                </div>
                <div className="d-flex justify-content-between w-100">
                    <Button
                        className="p-1 d-flex align-items-center gap-1"
                        onClick={doNotShowAgain}
                        variant="outline-primary"
                    >
                        Don&apos;t show again
                    </Button>
                    <Button
                        className="p-1 d-flex align-items-center gap-1"
                        onClick={closePrompt}
                        variant="outline-primary"
                    >
                        Close
                    </Button>
                </div>
            </PaperWithTape.Content>
        </PaperWithTape.Wrapper>
    );
}
