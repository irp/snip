"use client";
import useUserAgent from "lib/hooks/useUserAgent";
import dynamic from "next/dynamic";
import React, { useEffect, useState } from "react";

const ModuleLoading = () => null;
const AddToIosSafari = dynamic(() => import("./AddToIosSafari"), {
    loading: () => <ModuleLoading />,
});
const AddToMobileChrome = dynamic(() => import("./AddToMobileChrome"), {
    loading: () => <ModuleLoading />,
});
const AddToMobileFirefox = dynamic(() => import("./AddToMobileFirefox"), {
    loading: () => <ModuleLoading />,
});
const AddToMobileFirefoxIos = dynamic(() => import("./AddToMobileFirefoxIos"), {
    loading: () => <ModuleLoading />,
});
const AddToMobileChromeIos = dynamic(() => import("./AddToMobileChromeIos"), {
    loading: () => <ModuleLoading />,
});
const AddToSamsung = dynamic(() => import("./AddToSamsung"), {
    loading: () => <ModuleLoading />,
});
const AddToOtherBrowser = dynamic(() => import("./AddToOtherBrowser"), {
    loading: () => <ModuleLoading />,
});

type AddToHomeScreenPromptType =
    | "safari"
    | "chrome"
    | "firefox"
    | "other"
    | "firefoxIos"
    | "chromeIos"
    | "samsung"
    | "";
const STORAGE_NAME = "addToHomeScreenPrompt";

export default function AddToHomeScreen() {
    const [displayPrompt, setDisplayPrompt] =
        useState<AddToHomeScreenPromptType>("");
    const { userAgent, isMobile, isStandalone, isIOS } = useUserAgent();

    const closePrompt = () => {
        setDisplayPrompt("");
    };

    const doNotShowAgain = () => {
        // Create date 1 year from now
        const date = new Date();
        date.setFullYear(date.getFullYear() + 1);
        localStorage.setItem(
            STORAGE_NAME,
            JSON.stringify({
                dontShow: true,
                date: date.toISOString(),
            }),
        );
        setDisplayPrompt("");
    };

    useEffect(() => {
        const localStore = JSON.parse(
            localStorage.getItem(STORAGE_NAME) || "{}",
        ) as {
            dontShow?: boolean;
            date?: string;
        };

        const dontShow = localStore.dontShow || false;
        // TODO: Match date for 1 year from now
        const date = localStore.date ? new Date(localStore.date) : undefined;

        if (!dontShow) {
            // Only show prompt if user is on mobile and app is not installed
            if (isMobile && !isStandalone) {
                if (userAgent === "Safari") {
                    setDisplayPrompt("safari");
                } else if (userAgent === "Chrome") {
                    setDisplayPrompt("chrome");
                } else if (userAgent === "Firefox") {
                    setDisplayPrompt("firefox");
                } else if (userAgent === "FirefoxiOS") {
                    setDisplayPrompt("firefoxIos");
                } else if (userAgent === "ChromeiOS") {
                    setDisplayPrompt("chromeIos");
                } else if (userAgent === "SamsungBrowser") {
                    setDisplayPrompt("samsung");
                } else {
                    setDisplayPrompt("other");
                }
            }
        }
    }, [userAgent, isMobile, isStandalone, isIOS]);

    const Prompt = () => (
        <>
            {
                {
                    safari: (
                        <AddToIosSafari
                            closePrompt={closePrompt}
                            doNotShowAgain={doNotShowAgain}
                        />
                    ),
                    chrome: (
                        <AddToMobileChrome
                            closePrompt={closePrompt}
                            doNotShowAgain={doNotShowAgain}
                        />
                    ),
                    firefox: (
                        <AddToMobileFirefox
                            closePrompt={closePrompt}
                            doNotShowAgain={doNotShowAgain}
                        />
                    ),
                    firefoxIos: (
                        <AddToMobileFirefoxIos
                            closePrompt={closePrompt}
                            doNotShowAgain={doNotShowAgain}
                        />
                    ),
                    chromeIos: (
                        <AddToMobileChromeIos
                            closePrompt={closePrompt}
                            doNotShowAgain={doNotShowAgain}
                        />
                    ),
                    samsung: (
                        <AddToSamsung
                            closePrompt={closePrompt}
                            doNotShowAgain={doNotShowAgain}
                        />
                    ),
                    other: (
                        <AddToOtherBrowser
                            closePrompt={closePrompt}
                            doNotShowAgain={doNotShowAgain}
                        />
                    ),
                    "": <></>,
                }[displayPrompt]
            }
        </>
    );

    return (
        <>
            {displayPrompt !== "" ? (
                <>
                    <Prompt />
                </>
            ) : (
                <></>
            )}
        </>
    );
}
/**debug
<button onClick={() => setDisplayPrompt("firefox")}>
    FF
</button>
<button onClick={() => setDisplayPrompt("chrome")}>
    Chrome
</button>
<button onClick={() => setDisplayPrompt("safari")}>
    Safari
</button>
<button onClick={() => setDisplayPrompt("firefoxIos")}>
    FF iOS
</button>
<button onClick={() => setDisplayPrompt("chromeIos")}>
    Chrome iOS
</button>
<button onClick={() => setDisplayPrompt("samsung")}>
    Samsung
</button>
<button onClick={() => setDisplayPrompt("other")}>
    Other
</button>
 */
