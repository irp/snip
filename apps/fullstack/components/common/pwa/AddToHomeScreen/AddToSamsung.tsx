import React from "react";
import Button from "react-bootstrap/esm/Button";
import { FaBars } from "react-icons/fa";
import { TfiPlus } from "react-icons/tfi";

import PaperWithTape from "components/common/PaperWithTape";

interface Props {
    closePrompt: () => void;
    doNotShowAgain: () => void;
}

export default function AddToSamsung(props: Props) {
    const { closePrompt, doNotShowAgain } = props;

    return (
        <PaperWithTape.Wrapper show={true}>
            <PaperWithTape.Heading>
                Add SNIP to your Home Screen
            </PaperWithTape.Heading>
            <PaperWithTape.Content>
                <div>
                    For the best experience, we recommend installing SNIP to
                    your home screen! This enables enhanced features and
                    functionality for mobile users.
                </div>
                <div className="d-flex align-items-center justify-content-center flex-column  border border-1 rounded-3 p-2 gap-2 my-2 w-100">
                    <div className="d-flex gap-1 text-center">
                        <div>Click the</div>
                        <FaBars size={24} />
                        <div>icon</div>
                    </div>
                    <div className="d-flex flex-column align-items-center w-100 text-center gap-2">
                        <div>Scroll down and then click:</div>
                        <div className="d-flex align-items-center border border-1 px-2 py-1 gap-1 rounded-3">
                            <TfiPlus size={24} />
                            <div>Add page to</div>
                        </div>
                        <div>Then select:</div>
                        <div className="d-flex align-items-center border border-1 px-2 py-1 gap-1 rounded-3">
                            <div>Home Screen</div>
                        </div>
                    </div>
                </div>
                <div className="d-flex justify-content-between w-100">
                    <Button
                        className="p-1 d-flex align-items-center gap-1"
                        onClick={doNotShowAgain}
                        variant="outline-primary"
                    >
                        Don&apos;t show again
                    </Button>
                    <Button
                        className="p-1 d-flex align-items-center gap-1"
                        onClick={closePrompt}
                        variant="outline-primary"
                    >
                        Close
                    </Button>
                </div>
            </PaperWithTape.Content>
        </PaperWithTape.Wrapper>
    );
}
