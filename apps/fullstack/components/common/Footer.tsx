import Link from "next/link";

import styles from "./Footer.module.scss";
interface Props {
    className?: string;
}

import { TbBrandGit, TbBug } from "react-icons/tb";

import Package from "@package_json";

export default function Footer({ className }: Props) {
    return (
        <footer className={styles.footer + ` ${className ? className : ""}`}>
            <div className={styles.footer_content}>
                <ul>
                    <li>
                        <a href="https://gitlab.gwdg.de/irp/snip/-/blob/main/CHANGELOG.md">
                            v{Package.version}
                        </a>{" "}
                        © 2021 - {new Date().getFullYear().toString()}
                    </li>
                    <li>
                        <a
                            href="https://semohr.github.io/"
                            target="_blank"
                            rel="noreferrer"
                        >
                            Sebastian B. Mohr
                        </a>
                    </li>
                    <li>
                        <a
                            href="https://uni-goettingen.de/en/570767.html"
                            target="_blank"
                            rel="noreferrer"
                        >
                            Markus Osterhoff
                        </a>
                    </li>
                </ul>

                <ul className="text-right">
                    <li>
                        <a href="https://gitlab.gwdg.de/irp/snip/">
                            <TbBrandGit size={"1rem"} strokeWidth={1} />
                        </a>
                    </li>
                    <li>
                        <a href="https://gitlab.gwdg.de/irp/snip/-/issues">
                            <TbBug size={"1rem"} strokeWidth={1} />
                        </a>
                    </li>
                    <li>
                        <Link href="/schemas">Schemas</Link>
                    </li>
                    <li>
                        <Link href="/docs/getting-started" target={"_blank"}>
                            Docs
                        </Link>
                    </li>
                    <li>
                        <Link href="/privacy">Privacy policy</Link>
                    </li>
                    <li>
                        <Link href="/imprint">Imprint</Link>
                    </li>
                </ul>
            </div>
        </footer>
    );
}
