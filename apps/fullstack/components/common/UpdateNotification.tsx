"use client";
import Link from "next/link";
import { useEffect, useState } from "react";
import Button from "react-bootstrap/esm/Button";

import PaperWithTape from "./PaperWithTape";

const CURRENT_VERSION = "v1.12.0";

export default function UpdateNotification() {
    const [displayPrompt, setDisplayPrompt] = useState(false);

    const closePrompt = () => {
        setDisplayPrompt(false);
    };

    const doNotShowAgain = () => {
        // Create date 1 year from now
        const date = new Date();
        date.setFullYear(date.getFullYear() + 1);
        localStorage.setItem(
            "updateNotificationPrompt",
            JSON.stringify({
                dontShow: true,
                version: CURRENT_VERSION,
                date: date.toISOString(),
            }),
        );
        setDisplayPrompt(false);
    };

    useEffect(() => {
        const prompt = localStorage.getItem("updateNotificationPrompt");
        if (prompt) {
            const { dontShow, version } = JSON.parse(prompt);
            if (dontShow && version === CURRENT_VERSION) {
                setDisplayPrompt(false);
            } else {
                setDisplayPrompt(true);
            }
        } else {
            setDisplayPrompt(true);
        }
    }, []);

    return (
        <PaperWithTape.Wrapper
            show={displayPrompt}
            style={{ marginBottom: "1.5rem" }}
        >
            <PaperWithTape.Heading>
                Update Notification {CURRENT_VERSION}
            </PaperWithTape.Heading>
            <PaperWithTape.Content
                style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "0.5rem",
                }}
            >
                <div>
                    This release includes experimental support for Markdown and
                    some minor bugfixes and improvements.
                </div>
                <ChangelogAndFeedbackReminder />
                <div className="d-flex justify-content-between mt-1 w-100">
                    <Button
                        className="p-1 d-flex align-items-center gap-1"
                        variant="outline-primary"
                        onClick={doNotShowAgain}
                    >
                        Don&apos;t show again
                    </Button>
                    <Button
                        className="p-1 d-flex align-items-center gap-1"
                        variant="outline-primary"
                        onClick={closePrompt}
                    >
                        Close
                    </Button>
                </div>
            </PaperWithTape.Content>
        </PaperWithTape.Wrapper>
    );
}

function ChangelogAndFeedbackReminder() {
    return (
        <div>
            You can find the full list of changes in the{" "}
            <Link
                href="https://gitlab.gwdg.de/irp/snip/-/blob/main/CHANGELOG.md?ref_type=heads"
                target="_blank"
                rel="noopener noreferrer"
            >
                changelog
            </Link>
            . As with any major update, there might be some unexpected issues.
            If you experience any problems, please report them by creating an{" "}
            <Link href="https://gitlab.gwdg.de/irp/snip/-/issues">issue</Link>{" "}
            or contacting us at{" "}
            <Link href="mailto:uprpsnip@gwdg.de">uprpsnip@gwdg.de</Link>. our
            feedback is invaluable, and we&apos;re committed to addressing any
            concerns as quickly as possible.
        </div>
    );
}
