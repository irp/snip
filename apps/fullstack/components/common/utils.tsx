"use client";
import { get_browser, isSupported } from "lib/browserSupported";
import { usePathname } from "next/navigation";
import { ReactElement, ReactNode, useEffect } from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

export function BrowserVersionAlert() {
    //Check if browser is supported
    useEffect(() => {
        if (typeof window !== "undefined") {
            if (!isSupported(get_browser())) {
                alert(
                    "Your browser might not be supported. Please use Chrome>69, Firefox>105, Safari>16.4, Opera>48 or Edge>79.",
                );
            }
        }
    }, []);

    return null;
}

export function NotFoundErrorMsg() {
    const path = usePathname();

    return (
        <p
            className="text-center"
            style={{
                fontSize: "1.1rem",
                margin: 0,
                padding: "0.5rem",
            }}
            role="alert"
        >
            Looks like you took a wrong turn somewhere! The requested page{" "}
            <code>{path}</code> was not found.
        </p>
    );
}

/** Component to show an hover info
 * i.e. a small tooltip
 */
export function HoverInfo({
    children,
    tooltip,
}: {
    children: ReactElement;
    tooltip: ReactNode;
}) {
    return (
        <OverlayTrigger
            overlay={
                <Tooltip id="t-1" style={{ position: "fixed" }}>
                    {tooltip}
                </Tooltip>
            }
        >
            {children}
        </OverlayTrigger>
    );
}
