"use client";
import { logout } from "lib/actions/logout";
import { useActivePath } from "lib/hooks/useActivePath";
import useUser from "lib/hooks/useUser";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { HTMLAttributes, ReactNode, useState } from "react";
import Button from "react-bootstrap/Button";
import { TbBackspace, TbDots, TbDotsVertical, TbX } from "react-icons/tb";

import { Logo } from "./Header";
import { NavigationItem } from "./Navbar";
import { books_svg, logout_svg, person_svg, upload_svg } from "./svgs";

import styles from "./Sidebar.module.scss";

const sideBarItemsD: NavigationItem[] = [
    {
        label: "Books",
        href: "/books",
        icon: books_svg,
    },
    {
        label: "Account",
        href: "/account",
        icon: person_svg,
    },
    {
        label: "Upload",
        href: "/upload",
        icon: upload_svg,
    },
];

export function ContentWrapper({ children }: { children: ReactNode }) {
    return (
        <div className={styles.content}>
            <div>{children}</div>
        </div>
    );
}

export function Sidebar({
    sideBarItems = sideBarItemsD,
    mobile_type = "popup",
}: {
    sideBarItems?: NavigationItem[];
    mobile_type?: "default" | "popup";
}) {
    return (
        <SidebarFolders
            folders={sideBarItems}
            back_type="home"
            mobile_type={mobile_type}
        />
    );
}

export type NavigationFolder = {
    label: string;
    href: string;
    children: NavigationItem[];
};

export function SidebarFolders({
    folders,
    back_type = "back",
    mobile_type = "popup",
}: {
    folders: (NavigationFolder | NavigationItem)[];
    back_type?: "logout" | "home" | "back" | "none" | NavigationItem;
    mobile_type?: "default" | "popup";
}) {
    const isActivePath = useActivePath();
    const [mobileExpanded, setMobileExpanded] = useState(false);

    // Parse back link in navigation
    let back: ReactNode;
    if (typeof back_type === "string") {
        switch (back_type) {
            case "back":
                back = (
                    <LinkListItem
                        href="/"
                        special
                        img={<TbBackspace />}
                        align="left"
                    >
                        Back
                    </LinkListItem>
                );
                break;
            case "logout":
                back = <LogoutTabLink />;
                break;
            case "home":
                back = (
                    <LinkListItem href="/" special img={books_svg} align="left">
                        Home
                    </LinkListItem>
                );
                break;
            case "none":
                back = undefined;
                break;
        }
    } else {
        back = (
            <LinkListItem
                href={back_type.href}
                special
                img={back_type.icon}
                align="left"
            >
                {back_type.label}
            </LinkListItem>
        );
    }

    return (
        <div className={styles.sidebar} data-mobile-expanded={mobileExpanded}>
            <div className={styles.branding}>
                <Logo />
            </div>
            <nav>
                {mobile_type == "popup" && (
                    <Button
                        className={styles.mobile_close}
                        variant="light"
                        onClick={() => setMobileExpanded(!mobileExpanded)}
                    >
                        <TbX />
                    </Button>
                )}
                <ul className="nav nav-pills">
                    {folders.map((folder, index) => {
                        if ("children" in folder) {
                            return (
                                <FolderListItem
                                    key={index}
                                    folder={folder}
                                    isActivePath={isActivePath}
                                />
                            );
                        } else {
                            return (
                                <LinkListItem
                                    key={index}
                                    href={folder.href}
                                    active={isActivePath(folder.href)}
                                    img={folder.icon}
                                >
                                    {folder.label}
                                </LinkListItem>
                            );
                        }
                    })}

                    {back}
                </ul>
            </nav>
            {mobile_type == "popup" && (
                <Button
                    className={styles.mobile_expand}
                    variant="light"
                    onClick={() => setMobileExpanded(!mobileExpanded)}
                >
                    <TbDots />
                </Button>
            )}
        </div>
    );
}

type Props = {
    children: ReactNode;
    href?: string;
    active?: boolean;
    special?: boolean;
    align?: "left" | "right";
    img?: ReactNode;
    condition?: "admin";
} & HTMLAttributes<HTMLElement>;

function LinkListItem({
    children,
    href = undefined,
    active = false,
    special = false,
    align = "right",
    img = undefined,
    condition = undefined,
    ...props
}: Props) {
    const { isAdmin } = useUser();

    if (condition === "admin" && !isAdmin) {
        return;
    }

    if (!href) {
        return (
            <li
                data-align={align}
                data-active={active}
                data-special={special}
                {...props}
            >
                <div>
                    {img && <div className={styles.img}>{img}</div>}
                    <div>{children}</div>
                </div>
            </li>
        );
    } else {
        return (
            <li
                data-align={align}
                data-active={active}
                data-special={special}
                {...props}
            >
                <Link href={href}>
                    {img && <div className={styles.img}>{img}</div>}
                    <div>{children}</div>
                </Link>
            </li>
        );
    }
}

function FolderListItem({
    folder,
    isActivePath,
}: {
    folder: NavigationFolder;
    isActivePath: (path: string) => boolean;
}) {
    const actives = folder.children.map((child) => isActivePath(child.href));
    const any_active = actives.some((active) => active);

    if (!any_active) {
        return (
            <>
                <LinkListItem
                    href={folder.href}
                    active={false}
                    img={<TbDotsVertical />}
                    className={styles.folderListItem}
                >
                    {folder.label}
                </LinkListItem>
            </>
        );
    }

    return (
        <>
            <LinkListItem
                active={true}
                img={<TbDots />}
                className={styles.folderListItem}
            >
                {folder.label}
            </LinkListItem>
            <ul>
                {folder.children.map((child, index) => (
                    <LinkListItem
                        key={index}
                        href={child.href}
                        active={isActivePath(child.href)}
                        img={child.icon}
                    >
                        {child.label}
                    </LinkListItem>
                ))}
            </ul>
        </>
    );
}

function LogoutTabLink() {
    const { mutate } = useUser();
    const router = useRouter();
    return (
        <LinkListItem
            align="left"
            onClick={async () => {
                await logout();
                mutate(undefined);
                router.refresh();
            }}
            img={logout_svg}
            special
        >
            Logout
        </LinkListItem>
    );
}
