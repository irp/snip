/* eslint-disable @next/next/no-img-element */
/* eslint-disable @typescript-eslint/no-explicit-any */

/** MDX to react component mappings
 */
import { readFile } from "fs/promises";
import Link from "next/link";
import path from "path";
import React from "react";
import { BsFonts } from "react-icons/bs";
import { TbArrowsMove, TbEraser, TbPointer } from "react-icons/tb";

import { PenFilled } from "components/editor/tools/doodle/svgs";

import styles from "./docs.module.scss";

export const getComponents = (p: string) => {
    // To local basepah
    const dir_name = path.dirname(p);

    return {
        a: (props: any) => {
            let { href } = props;
            if (href?.endsWith(".mdx")) {
                href = href.replace(".mdx", "");
            }
            if (!href) {
                return <span>{props.children}</span>;
            }

            return <Link href={href}>{props.children}</Link>;
        },
        img: async (props: any) => {
            // Load image if it ./images/ folder
            if (props.src?.startsWith("./")) {
                const img = await readFile(
                    path.join(dir_name, props.src.slice(2)),
                );

                return (
                    <div className={styles.image}>
                        <img
                            src={
                                "data:image/jpeg;base64," +
                                img.toString("base64")
                            }
                            alt={props.alt}
                        />
                    </div>
                );
            }
        },
        table: (props: any) => {
            return (
                <div className={styles.table}>
                    <table className="table table-bordered table-striped">
                        {props.children}
                    </table>
                </div>
            );
        },
        Alert: (props: any) => {
            return (
                <div className={styles.alert}>
                    <div className={`alert alert-${props.variant}`}>
                        {props.children}
                    </div>
                </div>
            );
        },

        // Tools
        InteractionTool: TbPointer,
        MoveTool: TbArrowsMove,
        DoodleTool: () => {
            return <PenFilled width="1em" height="1em" />;
        },
        TextTool: BsFonts,
        EraserTool: TbEraser,
    };
};
