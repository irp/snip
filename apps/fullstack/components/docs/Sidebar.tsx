import Link from "next/link";

import { hasOwnProperty } from "@snip/common";

import styles from "./docs.module.scss";

interface Route_File {
    path: string;
    name: string;
}

interface Route_Folder {
    path: string;
    name: string;
    children: Route_File[] | Route_Folder[];
}

export type Route = Route_File | Route_Folder;

export function Sidebar({ routes }: { routes: Route[] }) {
    if (!routes) {
        return null;
    }

    function renderFile(route: Route_File) {
        // Shows a single file in the sidebar
        return (
            <li key={route.path}>
                <Link href={"/docs/" + route.path}>{route.name}</Link>
            </li>
        );
    }

    function renderFolder(route: Route_Folder) {
        // Shows a collapsible folder in the sidebar
        return (
            <ul key={route.path}>
                <div className={styles.header}>{route.name}</div>
                {route.children.map((child) => {
                    if (hasOwnProperty(child, "children")) {
                        return renderFolder(child as Route_Folder);
                    } else {
                        return renderFile(child);
                    }
                })}
            </ul>
        );
    }

    return (
        <div className={styles.sidebar}>
            <div className={styles.title}>Documentation</div>
            <div className={styles.content}>
                <ul>
                    {routes.map((route) =>
                        hasOwnProperty(route, "children")
                            ? renderFolder(route as Route_Folder)
                            : renderFile(route),
                    )}
                </ul>
            </div>
        </div>
    );
}
