"use client";
import Loading from "app/(unprotected)/loading";
import { useDate } from "lib/hooks/useDate";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { Dispatch, DragEventHandler, SetStateAction, useState } from "react";
import {
    BsFolder,
    BsFolder2Open,
    BsJournal,
    BsPencilSquare,
    BsTrash3,
} from "react-icons/bs";
import { MdEmail } from "react-icons/md";

import { GoUp_svg } from "./BrowserNavbar";
import { CreateBook } from "./BrowserToolbar";

import { useBooksBrowserContext } from "components/context/useBooksBrowserContext";

import styles from "./BooksBrowser.module.scss";

export function FileView() {
    const { view, isLoading, fileMap } = useBooksBrowserContext();

    if (isLoading) {
        return (
            <div
                style={{
                    borderTop: "1px solid var(--bs-gray)",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "100%",
                }}
            >
                <Loading />
            </div>
        );
    }

    if (
        !fileMap ||
        Object.keys(fileMap["root"]?.childrenIds || []).length === 0
    ) {
        return (
            <div className={styles.onboarding_wrapper}>
                <div className={styles.onboarding}>
                    <h2>Welcome to Snip - our digital labbook!</h2>
                    <div style={{ display: "flex", gap: "1rem" }}>
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                gap: "0.25rem",
                            }}
                        >
                            <div>
                                It seems like this is your first time here! You
                                can create your first book by using the{" "}
                                <q>Create Book</q> button or by utilizing the
                                corresponding button located in the toolbar
                                above. In future you will find your books listed
                                here. Feel free to explore snip on your own or
                                peek into the{" "}
                                <Link href="/docs/user/first-steps">
                                    documentation
                                </Link>{" "}
                                to get a head start.
                            </div>
                            <div>
                                Should you encounter problems or if you have any
                                questions, feel free to reach out to us{" "}
                                <a href="mailto:gitlab+irp-snip-20373-issue-@gwdg.de">
                                    <MdEmail />
                                </a>
                            </div>
                        </div>
                        <div className={styles.createBook}>
                            <CreateBook />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    switch (view) {
        case "list":
            return <FileList />;
        case "grid":
            return <FileGrid />;
        default:
            return null;
    }
}

function FileList() {
    const { fileMapOrder, fileMap, currentFolder } = useBooksBrowserContext();
    const [, setDraggedFileId] = useState<FileID>();

    if (!fileMap) return null;

    // Construct an item for each file
    const items = fileMapOrder.map((fileId) => {
        const file = fileMap[fileId]!;
        return (
            <FileListItem
                key={fileId}
                file={file}
                setDraggedFileId={setDraggedFileId}
            />
        );
    });

    return (
        <div className={styles.fileList}>
            <div className={styles.header}>
                <div className={styles.icon}></div>
                <div className={styles.name}>Name</div>
                <div className={styles.property_name}>Owner</div>
                <div className={styles.property_date}>Created</div>
                <div className={styles.property_date}>Last Updated</div>
                <div className={styles.property_num}>Pages</div>
                <div className={styles.property_icon}>Actions</div>
            </div>
            {currentFolder?.parentId && (
                <FileListItem
                    key={currentFolder.parentId}
                    file={fileMap[currentFolder.parentId]!}
                    setDraggedFileId={setDraggedFileId}
                    moveUp={true}
                />
            )}
            {items}
        </div>
    );
}

function FileListItem({
    file,
    setDraggedFileId,
    moveUp = false,
}: {
    file: FileData;
    setDraggedFileId: Dispatch<SetStateAction<FileID | undefined>>;
    moveUp?: boolean;
}) {
    const {
        setCurrentFolderId,
        moveFileToFolder,
        onContextMenuFile,
        deleteFolder,
    } = useBooksBrowserContext();
    const [isHovering, setIsHovering] = useState(false); // Whether the item is being hovered over
    const router = useRouter();

    const created = useDate(file.created);
    const last_updated = useDate(file.last_updated);

    const onClick = () => {
        if (file.isDir) {
            setCurrentFolderId(file.id);
        } else {
            //Clicking on a file opens it
            router.push(`/books/${file.id}`);
        }
    };

    /** Drag and drop handler
     *  - Only files can be dragged
     *  - Only folders can be dropped on
     *  - Changes the map of dragged file id to the folder it is being dragged over
     */
    const onDragStart: DragEventHandler<HTMLDivElement> = (e) => {
        e.dataTransfer.setData("text/plain", file.id);
        setDraggedFileId(file.id);
    };

    const onDragEnd = () => {
        setDraggedFileId(undefined);
    };
    const onDragOver: DragEventHandler<HTMLDivElement> = (e) => {
        e.preventDefault();
        // Change icon to indicate that the file can be dropped here
        setIsHovering(true);
    };
    const onDragLeave: DragEventHandler<HTMLDivElement> = (e) => {
        e.preventDefault();
        // Change icon back to normal
        setIsHovering(false);
    };

    const onDrop: DragEventHandler<HTMLDivElement> = (e) => {
        e.preventDefault();
        const droppedFileId = e.dataTransfer.getData("text/plain");
        moveFileToFolder(droppedFileId, file.id);
        setIsHovering(false);
    };

    let icon;
    if (file.isDir && isHovering) {
        icon = <BsFolder2Open />;
    } else if (file.isDir) {
        icon = <BsFolder />;
    } else {
        icon = <BsJournal />;
    }

    if (moveUp) {
        icon = <GoUp_svg />;
    }

    let edit;
    if (!file.isDir) {
        if (file.perms?.pRead || file.perms?.pACL) {
            edit = (
                <Link href={`/books/${file.id}/edit`} passHref>
                    <BsPencilSquare />
                </Link>
            );
        } else {
            edit = (
                <a data-disabled="true">
                    <BsPencilSquare />
                </a>
            );
        }
    } else if (!moveUp) {
        edit = (
            <button onClick={() => deleteFolder(file.id)}>
                <BsTrash3 />
            </button>
        );
    }

    return (
        <div
            className={styles.item}
            draggable={!file.isDir}
            onDragStart={onDragStart}
            onContextMenu={(e) => onContextMenuFile(e, file)}
            onDragEnd={onDragEnd}
            onDragOver={file.isDir ? onDragOver : undefined}
            onDragLeave={file.isDir ? onDragLeave : undefined}
            onDrop={file.isDir ? onDrop : undefined}
            data-hover={isHovering}
            onDoubleClick={onClick}
        >
            <div className={styles.icon}>{icon}</div>
            <div className={styles.name}>
                {!file.isDir ? (
                    <Link href={`/books/${file.id}/pages/-1`} passHref>
                        {file.name}
                    </Link>
                ) : (
                    <a onClick={onClick}>{file.name}</a>
                )}
            </div>
            <div className={styles.property_name}>{file.ownerName || "-"}</div>
            <div className={styles.property_date}>{created}</div>
            <div className={styles.property_date}>{last_updated}</div>
            <div className={styles.property_num}>
                {file.numPages ? file.numPages : "-"}
            </div>
            <div className={styles.property_icon}>{edit}</div>
        </div>
    );
}

function FileGrid() {
    const { fileMapOrder, fileMap, currentFolder } = useBooksBrowserContext();
    const [, setDraggedFileId] = useState<FileID>();
    if (!fileMap) return null;

    // Construct an item for each file
    const items = fileMapOrder.map((fileId) => {
        const file = fileMap[fileId]!;
        return (
            <FileGridItem
                file={file}
                setDraggedFileId={setDraggedFileId}
                key={fileId}
            />
        );
    });

    return (
        <div className={styles.fileGrid}>
            {currentFolder?.parentId && (
                <FileGridItem
                    file={fileMap[currentFolder.parentId]!}
                    setDraggedFileId={setDraggedFileId}
                    key={currentFolder.parentId}
                    moveUp={true}
                />
            )}

            {items}
        </div>
    );
}

function FileGridItem({
    file,
    setDraggedFileId,
    moveUp = false,
}: {
    file: FileData;
    setDraggedFileId: Dispatch<SetStateAction<FileID | undefined>>;
    moveUp?: boolean;
}) {
    const { setCurrentFolderId, moveFileToFolder } = useBooksBrowserContext();
    const [isHovering, setIsHovering] = useState(false); // Whether the item is being hovered over
    const router = useRouter();

    const onClick = () => {
        if (file.isDir) {
            setCurrentFolderId(file.id);
        } else {
            //Clicking on a file opens it
            router.push(`/books/${file.id}`);
        }
    };

    /** Drag and drop handler
     *  - Only files can be dragged
     *  - Only folders can be dropped on
     *  - Changes the map of dragged file id to the folder it is being dragged over
     */
    const onDragStart: DragEventHandler<HTMLDivElement> = (e) => {
        e.dataTransfer.setData("text/plain", file.id);
        setDraggedFileId(file.id);
    };

    const onDragEnd = () => {
        setDraggedFileId(undefined);
    };
    const onDragOver: DragEventHandler<HTMLDivElement> = (e) => {
        e.preventDefault();
        // Change icon to indicate that the file can be dropped here
        setIsHovering(true);
    };
    const onDragLeave: DragEventHandler<HTMLDivElement> = (e) => {
        e.preventDefault();
        // Change icon back to normal
        setIsHovering(false);
    };

    const onDrop: DragEventHandler<HTMLDivElement> = (e) => {
        e.preventDefault();
        const droppedFileId = e.dataTransfer.getData("text/plain");
        moveFileToFolder(droppedFileId, file.id);
        setIsHovering(false);
    };

    let thumbnail = <Journal_svg />;
    if (file.thumbnail) {
        thumbnail = (
            <Image
                src={file.thumbnail}
                alt=""
                fill
                objectFit="contain"
                loader={({ src, width }) => {
                    // 200 hardcoded for now
                    return `/render/book_preview/${src}.jpeg?w=${width}`;
                }}
            />
        );
    } else if (file.isDir && isHovering) {
        thumbnail = <FolderOpen_svg />;
    } else if (file.isDir) {
        thumbnail = <Folder_svg />;
    } else {
        thumbnail = <Journal_svg />;
    }

    if (moveUp) {
        thumbnail = (
            <div style={{ width: "50%", height: "50%" }}>
                <GoUp_svg />
            </div>
        );
    }

    return (
        <div
            className={styles.item}
            draggable={!file.isDir}
            onDragStart={onDragStart}
            onDragEnd={onDragEnd}
            onDragOver={file.isDir ? onDragOver : undefined}
            onDragLeave={file.isDir ? onDragLeave : undefined}
            onDrop={file.isDir ? onDrop : undefined}
            data-hover={isHovering}
            onDoubleClick={onClick}
        >
            <div className={styles.thumbnail}>{thumbnail}</div>
            <div className={styles.name} onClick={onClick}>
                {!file.isDir ? (
                    <Link href={`/books/${file.id}`} passHref>
                        {file.name}
                    </Link>
                ) : (
                    <a>{file.name}</a>
                )}
            </div>
        </div>
    );
}

const Folder_svg = () => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-folder"
        viewBox="0 0 16 16"
    >
        <path d="M.54 3.87.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3h3.982a2 2 0 0 1 1.992 2.181l-.637 7A2 2 0 0 1 13.174 14H2.826a2 2 0 0 1-1.991-1.819l-.637-7a1.99 1.99 0 0 1 .342-1.31zM2.19 4a1 1 0 0 0-.996 1.09l.637 7a1 1 0 0 0 .995.91h10.348a1 1 0 0 0 .995-.91l.637-7A1 1 0 0 0 13.81 4H2.19zm4.69-1.707A1 1 0 0 0 6.172 2H2.5a1 1 0 0 0-1 .981l.006.139C1.72 3.042 1.95 3 2.19 3h5.396l-.707-.707z" />
    </svg>
);

const FolderOpen_svg = () => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-folder2-open"
        viewBox="0 0 16 16"
    >
        <path d="M1 3.5A1.5 1.5 0 0 1 2.5 2h2.764c.958 0 1.76.56 2.311 1.184C7.985 3.648 8.48 4 9 4h4.5A1.5 1.5 0 0 1 15 5.5v.64c.57.265.94.876.856 1.546l-.64 5.124A2.5 2.5 0 0 1 12.733 15H3.266a2.5 2.5 0 0 1-2.481-2.19l-.64-5.124A1.5 1.5 0 0 1 1 6.14V3.5zM2 6h12v-.5a.5.5 0 0 0-.5-.5H9c-.964 0-1.71-.629-2.174-1.154C6.374 3.334 5.82 3 5.264 3H2.5a.5.5 0 0 0-.5.5V6zm-.367 1a.5.5 0 0 0-.496.562l.64 5.124A1.5 1.5 0 0 0 3.266 14h9.468a1.5 1.5 0 0 0 1.489-1.314l.64-5.124A.5.5 0 0 0 14.367 7H1.633z" />
    </svg>
);

const Journal_svg = () => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-journal"
        viewBox="0 0 16 16"
    >
        <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z" />
        <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z" />
    </svg>
);
