export type ContextMenuState = {
    visible: boolean;
    x: number;
    y: number;
    file: FileData;
};

/** Context menu
 *  - does nothing at the moment
 *  - but stays here for future use (just in case)
 *
 */
export function BrowserContextMenu() {
    return <></>;
}
