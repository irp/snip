"use client";
import { BsFolderFill } from "react-icons/bs";

import { useBooksBrowserContext } from "components/context/useBooksBrowserContext";

import styles from "./BooksBrowser.module.scss";

export function BrowserNavbar() {
    const {
        ascendToParentFolder,
        currentPath,
        currentFolder,
        setCurrentFolderId,
        rootFolderId,
        isLoading,
    } = useBooksBrowserContext();

    return (
        <div className={styles.navbar}>
            <button
                id={styles.upButton}
                onClick={ascendToParentFolder}
                disabled={!currentFolder || currentFolder.id == rootFolderId}
            >
                <GoUp_svg />
            </button>
            <div id={styles.folderIcon}>
                <BsFolderFill />
            </div>
            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    {isLoading ? (
                        <li key={"root"} className={"breadcrumb-item"}>
                            <button data-active={true}>Home</button>
                        </li>
                    ) : (
                        currentPath.map((folder, i) => (
                            <li key={folder.id} className={"breadcrumb-item"}>
                                <button
                                    onClick={() => {
                                        setCurrentFolderId(folder.id);
                                    }}
                                    data-active={i == currentPath.length - 1}
                                >
                                    {folder.name}
                                </button>
                            </li>
                        ))
                    )}
                </ol>
            </nav>
        </div>
    );
}

export const GoUp_svg = () => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-arrow-90deg-up"
        viewBox="0 0 16 16"
        transform="scale(-1, 1)"
    >
        <path
            fillRule="evenodd"
            d="M4.854 1.146a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L4 2.707V12.5A2.5 2.5 0 0 0 6.5 15h8a.5.5 0 0 0 0-1h-8A1.5 1.5 0 0 1 5 12.5V2.707l3.146 3.147a.5.5 0 1 0 .708-.708l-4-4z"
        />
    </svg>
);
