import { FormEventHandler } from "react";

import { FormHeader } from "components/form";
import { SecondaryButton, SubmitButton } from "components/form/buttons";
import { LongTextInput, TextInput } from "components/form/inputs/text";
import ModalForm from "components/form/modal";

import styles from "./CreateBook.module.scss";

/** CreateFolder component
 * Primarily used in the books browser but can also be used in other places.
 * @param callback_submit Callback function to be called when the form is submitted
 * @param callback_cancel Callback function to be called when the form is cancelled
 */
export function CreateFolderForm({
    onSubmit,
    onCancel,
    // Modal specific
    show,
    onHide,
}: {
    onSubmit: (name: string, comment: string) => Promise<void> | void;
    onCancel?: () => Promise<void> | void;
    // Modal specific
    show: boolean;
    onHide: () => void;
}) {
    const resetForm = () => {
        (document.getElementById("newFolderForm") as HTMLFormElement).reset();
    };

    const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
        event.preventDefault();
        const form = event.target as HTMLFormElement;
        const formdata = new FormData(form);
        await onSubmit(
            formdata.get("name") as string,
            formdata.get("comment") as string,
        );
    };

    return (
        <ModalForm
            show={show}
            onHide={onHide}
            onSubmit={handleSubmit}
            id="newFolderForm"
        >
            <FormHeader>Create a folder</FormHeader>
            <div>
                <TextInput label="Folder name" name="name" />
                <div className={styles.description}>
                    The name of the folder you want to create. This can be
                    changed later.
                </div>
                <LongTextInput name="comment" label="Comment" />
                <div className={styles.description}>
                    The comment field can be used to add more information to the
                    book. It can be used to find the book more easily. It is not
                    required but recommended.
                </div>

                <div>
                    {
                        <SecondaryButton
                            onClick={async () => {
                                resetForm();
                                await onCancel?.();
                            }}
                        >
                            Cancel
                        </SecondaryButton>
                    }
                    <SubmitButton>Create folder</SubmitButton>
                </div>
            </div>
        </ModalForm>
    );
}
