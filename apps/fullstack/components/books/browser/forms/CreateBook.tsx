import { useState } from "react";
import { mutate } from "swr";

import { FormHeader } from "components/form";
import { FormError } from "components/form/alert";
import { SecondaryButton, SubmitButton } from "components/form/buttons";
import { LongTextInput, TextInput } from "components/form/inputs/text";
import ModalForm from "components/form/modal";

import styles from "./CreateBook.module.scss";

/** NewBookForm component
 * Primarily used in the books browser but can also be used in other places.
 * @param callback_submit Callback function to be called when the form is submitted
 * @param callback_cancel Callback function to be called when the form is cancelled
 */
export function CreateBookForm({
    callback_submit,
    callback_cancel = undefined,
    // Modal specific
    show,
    onHide,
}: {
    callback_submit: () => void;
    callback_cancel?: () => void;
    // Modal specific
    show: boolean;
    onHide: () => void;
}) {
    const [error, setError] = useState<string>();

    const resetForm = () => {
        (document.getElementById("newBookForm") as HTMLFormElement).reset();
        setError(undefined);
    };

    const handleSubmit = async (from: FormData) => {
        if (!from.get("title")) {
            setError("Title is required");
            return;
        }

        await create_new_book(
            from.get("title") as string,
            from.get("comment") as string,
        )
            .then(() => {
                resetForm();
                if (callback_submit) {
                    callback_submit();
                }
            })
            .catch((err) => {
                setError(err.message);
            });
    };

    const handleCancel = () => {
        // Reset form
        resetForm();
        if (callback_cancel) {
            callback_cancel();
        }
    };

    return (
        <ModalForm
            show={show}
            onHide={onHide}
            action={handleSubmit}
            id="newBookForm"
        >
            <FormHeader>Create a new book</FormHeader>
            <FormError>{error}</FormError>
            <div>
                <TextInput label="Book title" name="title" />
                <div className={styles.description}>
                    The title of the book is shown in the books browser and on
                    the books page. It can be changed later but should be set to
                    something meaningful which can be easily recognized.
                </div>

                <LongTextInput label="Comment" name="comment" />
                <div className={styles.description}>
                    The comment field can be used to add more information to the
                    book. It can be used to find the book more easily. It is not
                    required but recommended.
                </div>

                <div>
                    {callback_cancel && (
                        <SecondaryButton onClick={handleCancel}>
                            Cancel
                        </SecondaryButton>
                    )}
                    <SubmitButton>Create book</SubmitButton>
                </div>
            </div>
        </ModalForm>
    );
}

/** Post request to create a new book
 * @param title Title of the book
 * @param comment Comment for the book
 * @param collaborators List of collaborators
 */
async function create_new_book(title: string, comment?: string) {
    const response = await fetch("/api/books", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ title, comment }),
    });
    const data = await response.json();
    mutate("/api/folders");
    if (response.ok) {
        return data;
    }
    throw new Error(data.details);
}
