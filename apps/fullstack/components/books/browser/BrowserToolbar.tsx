"use client";
import { useDebounce } from "lib/hooks/useDebounce";
import { useEffect, useState } from "react";
import {
    BsChevronDown,
    BsFolderPlus,
    BsGrid3X3Gap,
    BsListUl,
} from "react-icons/bs";
import { usePopper } from "react-popper";
import { mutate } from "swr";

import { CreateBookForm } from "./forms/CreateBook";
import { CreateFolderForm } from "./forms/CreateFolder";

import { useBooksBrowserContext } from "components/context/useBooksBrowserContext";

import styles from "./BooksBrowser.module.scss";

export function BrowserToolbar() {
    return (
        <div className={styles.toolbar}>
            <Search />
            <CreateBook />
            <CreateFolder />
            <Layouts />
            <Options />
        </div>
    );
}

function Search() {
    const { setOrder, nHidden, fileMapOrder } = useBooksBrowserContext();
    const [search, setSearch] = useState("");
    const debounced_search = useDebounce(search, 500);
    useEffect(() => {
        setOrder((o) => {
            return { ...o, search: debounced_search };
        });
    }, [debounced_search, setOrder]);

    return (
        <div className={styles.search}>
            <input
                type="text"
                placeholder="Search"
                onChange={(e) => setSearch(e.target.value)}
                value={search}
            />
            <div className={styles.nItems}>
                <span>{fileMapOrder.length} items </span>
                <span>{nHidden > 0 ? ` (${nHidden} hidden)` : ""}</span>
            </div>
        </div>
    );
}

function Layouts() {
    const { view, setView } = useBooksBrowserContext();

    return (
        <div className={styles.layouts}>
            <button
                className={styles.layout}
                disabled={view === "list"}
                onClick={() => setView("list")}
            >
                <BsListUl />
            </button>

            <button
                className={styles.layout}
                disabled={view === "grid"}
                onClick={() => setView("grid")}
            >
                <BsGrid3X3Gap />
            </button>
        </div>
    );
}

function CreateFolder() {
    const { createFolder } = useBooksBrowserContext();
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <div className={styles.createFolder}>
            <button onClick={handleShow}>
                <BsFolderPlus />
                <span>Create folder</span>
            </button>

            <CreateFolderForm
                onSubmit={async (name, comment) => {
                    await createFolder(name, comment);
                    handleClose();
                    mutate("/api/folders/map"); // Might need to add optimistic updates at some point
                }}
                onCancel={handleClose}
                onHide={handleClose}
                show={show}
            />
        </div>
    );
}

export function CreateBook() {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <div className={styles.createBook}>
            <button onClick={handleShow}>
                <JournalPlus_svg />
                <span>Create book</span>
            </button>

            <CreateBookForm
                show={show}
                onHide={handleClose}
                callback_submit={() => {
                    handleClose();
                    mutate("/api/folders/map"); // Might need to add optimistic updates at some point
                }}
                callback_cancel={handleClose}
            />
        </div>
    );
}

/** Dropdown Menu that allows to sort by name, number of pages and dates  */
function Options() {
    const { order, setOrder } = useBooksBrowserContext();
    const [visible, setVisibility] = useState(false);

    const [referenceElement, setReferenceElement] =
        useState<HTMLButtonElement | null>(null);
    const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(
        null,
    );

    const { styles: popperStyles, attributes } = usePopper(
        referenceElement,
        popperElement,
        {
            placement: "bottom-end",
            modifiers: [
                {
                    name: "offset",
                    enabled: true,
                    options: {
                        offset: [0, -27.5],
                    },
                },
            ],
        },
    );

    useEffect(() => {
        const listener = (e: MouseEvent) => {
            if (
                popperElement &&
                !popperElement.contains(e.target as HTMLElement)
            ) {
                setVisibility(!visible);
            }
        };
        if (visible) {
            document.addEventListener("mousedown", listener);
        }
        return () => {
            document.removeEventListener("mousedown", listener);
        };
    }, [popperElement, visible]);

    const getDataSet = (n: string) => {
        if (order.by != n) return "false";
        return order.asc ? "asc" : "desc";
    };
    const clickHandlerFactory = (n: Order["by"]) => {
        return () => {
            if (order.by == n) {
                setOrder((o) => {
                    return {
                        ...o,
                        by: n,
                        asc: !order.asc,
                    };
                });
            } else {
                setOrder((o) => {
                    return {
                        ...o,
                        by: n,
                        asc: true,
                    };
                });
            }
        };
    };

    return (
        <div className={styles.options}>
            <button
                className={styles.optionsButton}
                ref={setReferenceElement}
                onClick={() => {
                    setVisibility(!visible);
                }}
            >
                <span>Options</span>
                <BsChevronDown />
            </button>
            <div
                ref={setPopperElement}
                style={{ ...popperStyles.popper, zIndex: 100 }}
                {...attributes.popper}
            >
                <div
                    style={{
                        display: visible ? "block" : "none",
                    }}
                    className={styles.popover}
                >
                    <ul>
                        <li>
                            <div
                                className={styles.sort_item}
                                data-set={getDataSet("name")}
                                onClick={clickHandlerFactory("name")}
                            >
                                Sort by name
                            </div>
                        </li>
                        <li>
                            <div
                                className={styles.sort_item}
                                data-set={getDataSet("last_updated")}
                                onClick={clickHandlerFactory("last_updated")}
                            >
                                Sort by last updated
                            </div>
                        </li>
                        <li>
                            <div
                                className={styles.sort_item}
                                data-set={getDataSet("created")}
                                onClick={clickHandlerFactory("created")}
                            >
                                Sort by created
                            </div>
                        </li>
                        <li>
                            <div
                                className={styles.sort_item}
                                data-set={getDataSet("numPages")}
                                onClick={clickHandlerFactory("numPages")}
                            >
                                Sort by number of pages
                            </div>
                        </li>
                        <li>
                            <div
                                className={styles.toggle_item}
                                data-active={order.foldersFirst}
                                onClick={() => {
                                    setOrder((o) => {
                                        return {
                                            ...o,
                                            foldersFirst: !o.foldersFirst,
                                        };
                                    });
                                }}
                            >
                                Show folders first
                            </div>
                        </li>
                        <li>
                            <div
                                className={styles.toggle_item}
                                data-active={order.showFinished}
                                onClick={() => {
                                    setOrder((o) => {
                                        return {
                                            ...o,
                                            showFinished: !o.showFinished,
                                        };
                                    });
                                }}
                            >
                                Show finished books
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

const JournalPlus_svg = () => {
    return (
        <svg
            className="bi bi-journal"
            fill="currentColor"
            version="1.1"
            viewBox="0 0 16 16"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path d="m10 16h-6c-1.1046 0-2-0.89543-2-2v-1h1v1c0 0.55228 0.44772 1 1 1h6m5-5v-8c0-0.55228-0.44772-1-1-1h-10c-0.55228 0-1 0.44772-1 1v1h-1v-1c0-1.1046 0.89543-2 2-2h10c1.1046 0 2 0.89543 2 2v8" />
            <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z" />
            <path d="m13.5 11a0.5 0.5 0 0 1 0.5 0.5v1.5h1.5a0.5 0.5 0 1 1 0 1h-1.5v1.5a0.5 0.5 0 1 1-1 0v-1.5h-1.5a0.5 0.5 0 0 1 0-1h1.5v-1.5a0.5 0.5 0 0 1 0.5-0.5z" />
        </svg>
    );
};
