type FileID = string;

/** FileData can represent both a book or a directory.
 */
interface FileData {
    id: string; // Unique ID, database key
    name: string; // Name of the file or directory
    comment?: string; // Comment for the file

    // Properties for files
    isDir?: boolean; // If true, this is a directory, default: false
    parentId?: string; // ID of parent directory
    childrenIds?: FileID[]; // IDs of children

    // Properties derived from the books
    numPages?: number; // Shows number of pages in the book
    thumbnail?: string; // Automatically load thumbnail from this URL
    created?: Date; // Date the book was created
    last_updated?: Date; // Date the book was last updated
    finished?: boolean; // If true, the book is finished, default: false
    ownerID?: int; // ID of the owner of the book
    ownerName?: string; // Username/email of the owner of the book
    url?: string; // URL to the book
    perms?: PermissionACLData; // Permissions for the book
}

// Map of file IDs to FileData objects
type FileMap = { [id: FileID]: FileData };

type FileMapOrder = FileData[];

type Order = {
    by: "name" | "created" | "last_updated" | "numPages";
    asc: boolean;
    foldersFirst: boolean;
    showFinished: boolean;
    search: string;
};

type View = "list" | "grid";
