import { BrowserNavbar } from "./BrowserNavbar";
import { BrowserToolbar } from "./BrowserToolbar";
import { FileView } from "./FileView";

import { BooksBrowserContextProvider } from "components/context/useBooksBrowserContext";

import styles from "./BooksBrowser.module.scss";

/** Wrapper that exports a full book browser with all the components.
 */
export function BooksBrowser({ rootFolderId }: { rootFolderId: FileID }) {
    return (
        <BooksBrowserContextProvider rootFolderId={rootFolderId}>
            <div className={styles.BooksBrowser}>
                <BrowserNavbar />
                <BrowserToolbar />
                <FileView />
            </div>
        </BooksBrowserContextProvider>
    );
}
