import React, { useMemo, useState } from "react";
import { Button, InputGroup } from "react-bootstrap";
import { TbLayoutGrid, TbLayoutList } from "react-icons/tb";

import { PageDataRet } from "@snip/database/types";

import { ImagePagePreview } from "components/editor/sidebar/previews/previewPages";
import { DropdownInput, DropdownItem } from "components/form/inputs/option";

import styles from "./booksSelector.module.scss";

/** Component to select pages from a book
 *  a standard dropdown and a page view.
 */
export function PageSelector({
    pages,
    currentId,
    setCurrentId,
    feedbackNone,
    ...props
}: {
    pages: PageDataRet[] | null;
    currentId: number | null;
    setCurrentId: (idx: number | null) => void;
    feedbackNone?: string;
} & React.HTMLAttributes<HTMLDivElement>) {
    const [view, setView] = useState<"list" | "grid">("list");

    // Compute current page
    const currentIdx = pages?.findIndex((p) => p.id === currentId);
    let currentPage: PageDataRet | null = null;
    if (currentIdx === -1 || currentIdx === undefined) {
        currentPage = null;
    } else {
        currentPage = pages?.[currentIdx] ?? null;
    }

    // Compute options
    const options: DropdownItem[] = useMemo(() => {
        const options: DropdownItem[] = [];

        // Add none option
        if (feedbackNone) {
            options.push({
                label: feedbackNone,
                eventKey: "-1",
            });
        }
        if (view == "grid") {
            options[options.length - 1]!.labelSelect = (
                <div className={styles.bookItem}>
                    <label>None</label>
                </div>
            );
        }

        // Add pages
        if (pages === null) return options;
        for (let i = 0; i < pages.length; i++) {
            const page_number = pages[i]!.page_number ?? i;

            options.push({
                label: (page_number + 1).toString(),
                eventKey: pages[i]!.id,
            });
            if (view == "grid") {
                options[options.length - 1]!.labelSelect = (
                    <PageDropDownItem page={pages[i]!} />
                );
            }
        }
        return options;
    }, [feedbackNone, pages, view]);

    return (
        <div {...props} className={styles.wrapper}>
            <DropdownInput
                type="button"
                label="Page"
                disabled={pages === null}
                value={currentPage?.id ?? "-1"}
                options={options}
                onSelect={(id) => {
                    const idn = id ? parseInt(id) : null;
                    setCurrentId(idn);
                }}
                menuProps={{
                    className:
                        styles.menu + " " + (view == "grid" ? styles.grid : ""),
                }}
            />
            <InputGroup className="w-auto">
                <Button
                    variant="outline-primary"
                    onClick={() => setView("list")}
                    active={view == "list"}
                >
                    <TbLayoutList />
                </Button>
                <Button
                    variant="outline-primary"
                    onClick={() => setView("grid")}
                    active={view == "grid"}
                >
                    <TbLayoutGrid />
                </Button>
            </InputGroup>
        </div>
    );
}

function PageDropDownItem({ page }: { page: PageDataRet }) {
    return (
        <div className={styles.bookItem}>
            <label>Page: {page.page_number! + 1}</label>
            <ImagePagePreview page={page} />
        </div>
    );
}
