import Image from "next/image";
import { useMemo, useState } from "react";
import { Button, InputGroup } from "react-bootstrap";
import { TbLayoutGrid, TbLayoutList } from "react-icons/tb";

import { BookDataRet } from "@snip/database/types";

import { DropdownInput, DropdownItem } from "components/form/inputs/option";

import styles from "./booksSelector.module.scss";

/** Component to select on of many books
 * implements to views, a standard dropdown
 * and an frontpage view.
 */
export function BookSelector({
    books,
    currentIdx,
    setCurrentIdx,
    allowNone = false,
}: {
    books: BookDataRet[];
    currentIdx: number | null;
    setCurrentIdx: (idx: number | null) => void;
    allowNone?: boolean;
}) {
    const [view, setView] = useState<"list" | "grid">("list");

    const [optionsList, optionsGrid] = useMemo(() => {
        const list: DropdownItem[] = [];
        const grid: DropdownItem[] = [];

        // Add none option
        if (allowNone) {
            list.push({
                label: "None selected!",
                eventKey: "-1",
            });
            grid.push({
                label: "None selected!",
                labelSelect: (
                    <div className={styles.bookItem}>
                        <label>None</label>
                    </div>
                ),
                eventKey: "-1",
            });
        }

        for (let i = 0; i < books.length; i++) {
            list.push({
                label: books[i]!.title,
                eventKey: books[i]!.id,
            });
            grid.push({
                label: books[i]!.title,
                labelSelect: <BookFrontpage book={books[i]!} />,
                eventKey: books[i]!.id,
            });
        }
        return [list, grid];
    }, [allowNone, books]);

    return (
        <div className={styles.wrapper}>
            <DropdownInput
                type="button"
                value={
                    books.length > 0 && currentIdx !== null
                        ? books[currentIdx]!.id
                        : "-1"
                }
                label="Book"
                options={view == "list" ? optionsList : optionsGrid}
                onSelect={(id: string | null) => {
                    if (id === null || id === "-1") {
                        setCurrentIdx(null);
                        return;
                    }
                    const idx = books.findIndex(
                        (book) => book.id === parseInt(id),
                    );
                    setCurrentIdx(idx);
                }}
                menuProps={{
                    className:
                        styles.menu + " " + (view == "grid" ? styles.grid : ""),
                }}
            />
            <InputGroup className="w-auto">
                <Button
                    variant="outline-primary"
                    onClick={() => setView("list")}
                    active={view == "list"}
                >
                    <TbLayoutList />
                </Button>
                <Button
                    variant="outline-primary"
                    onClick={() => setView("grid")}
                    active={view == "grid"}
                >
                    <TbLayoutGrid />
                </Button>
            </InputGroup>
        </div>
    );
}

function BookFrontpage({ book }: { book: BookDataRet }) {
    return (
        <div className={styles.bookItem}>
            <label>{book.title}</label>
            <Image
                src={book.id.toString()}
                alt="Page"
                width={150}
                height={150 * 1.41}
                loader={({ src, width }) => {
                    // 200 hardcoded for now
                    return `/render/book_preview/${src}.jpeg?w=${width}&date=${book.last_updated?.valueOf()}`;
                }}
            />
        </div>
    );
}
