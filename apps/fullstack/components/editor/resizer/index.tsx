"use client";

import { useEffect, useRef, useState } from "react";
import { BiCaretRight } from "react-icons/bi";

import { useEditorContext } from "../context";

import styles from "./resizer.module.scss";

export default function EditorResizer() {
    const { activePageIds } = useEditorContext();

    return (
        <GenericResizer
            snap_left={170}
            disabled_right={activePageIds.length === 0}
        />
    );
}

/** Resizer component allows to resize
 * two adjacent html elements to match the size of their parents.
 *
 * Additionally we can force the resizer to fully expand the previous or next element.
 * Also there is a snip function to snap the resizer to the left or right at a certain
 * breakpoint.
 *
 * @param snap_left The breakpoint to snap the resizer to the left
 * @param snap_right The breakpoint to snap the resizer to the right
 *
 * @param disabled_right If the next element is disabled the resizer shrinks
 * the next element and disables the resizer
 * @param disabled_left If the previous element is disabled the resizer
 * shrinks the previous element
 */
export function GenericResizer({
    snap_left = 100,
    snap_right = 100,
    disabled_left = false,
    disabled_right = false,
}: {
    snap_left?: number;
    snap_right?: number;
    disabled_left?: boolean;
    disabled_right?: boolean;
}) {
    const resizerRef = useRef<HTMLDivElement | null>(null);
    const previousRef = useRef<HTMLElement | null>(null);
    const nextRef = useRef<HTMLElement | null>(null);

    const [disabled, setDisabled] = useState(true);

    // For double click to have a previous state to go back to
    // same position
    const [stateElements, setStateElements] = useState<StateElements | null>(
        null,
    );
    const [previousState, setPreviousState] = useState<StateElements | null>(
        null,
    );

    // The current/mousedown position of the mouse and the previous element
    const pointer = { x: 0, y: 0 };

    // Get the elements surrounding the resizer
    useEffect(() => {
        if (!resizerRef.current) return;

        previousRef.current = resizerRef.current
            .previousElementSibling as HTMLElement;
        nextRef.current = resizerRef.current.nextElementSibling as HTMLElement;

        // Check if they are valid if not snap to the left or right
        if (previousRef.current && nextRef.current) {
            setDisabled(false);
        }
    }, [resizerRef]);

    function onDBClick(e: React.MouseEvent) {
        if (!previousRef.current || !nextRef.current) return;
        // Expand the previous element
        if (previousState) {
            setWidth(
                previousRef.current,
                nextRef.current,
                previousState.previous.width,
            );
            setPreviousState(null);
            return;
        }

        // Always snap to the left unless the cntrl or shift key is pressed
        if (!stateElements) return;
        setPreviousState(stateElements);
        if (e.ctrlKey || e.shiftKey) {
            console.log("snap to right");
            if (stateElements.next.width < snap_right) {
                setWidth(nextRef.current, previousRef.current, 2 * snap_left);
            } else {
                setWidth(nextRef.current, previousRef.current, 0);
            }
        } else {
            console.log("snap to left", stateElements.previous.width);
            if (stateElements.previous.width < snap_left) {
                setWidth(previousRef.current, nextRef.current, 2 * snap_left);
            } else {
                setWidth(previousRef.current, nextRef.current, 0);
            }
        }
    }

    function onPointerDown(e: React.PointerEvent) {
        pointer.x = e.clientX;
        pointer.y = e.clientY;

        // Attach the listeners to `document`
        document.addEventListener("pointermove", onPointerMove);
        document.addEventListener("pointerup", onPointerUp);
    }

    function onPointerMove(e: PointerEvent) {
        setPreviousState(null);

        if (!stateElements || !previousRef.current || !nextRef.current) return;

        const previous = stateElements.previous;
        const next = stateElements.next;
        const dx = e.clientX - pointer.x;
        const newPrevWidth = previous.width + dx;
        const newNextWidth = next.width - dx;

        setWidth(previousRef.current, nextRef.current, newPrevWidth);

        // Check snap
        if (newPrevWidth < snap_left) {
            setWidth(previousRef.current, nextRef.current, 0);
            e.preventDefault();
        }
        if (newNextWidth < snap_right) {
            setWidth(nextRef.current, previousRef.current, 0);
            e.preventDefault();
        }

        //select bug
        previousRef.current.style.userSelect = "none";
        previousRef.current.style.pointerEvents = "none";
        nextRef.current.style.userSelect = "none";
        nextRef.current.style.pointerEvents = "none";
        document.body.style.cursor = "col-resize";
    }

    function onPointerUp() {
        if (!previousRef.current || !nextRef.current) return;
        previousRef.current.style.removeProperty("user-select");
        previousRef.current.style.removeProperty("pointer-events");
        nextRef.current.style.removeProperty("user-select");
        nextRef.current.style.removeProperty("pointer-events");
        document.body.style.removeProperty("cursor");

        document.removeEventListener("pointermove", onPointerMove);
        document.removeEventListener("pointerup", onPointerUp);
    }

    // Use resize observer to update the state of the elements
    useEffect(() => {
        function observerCallback() {
            if (!previousRef.current || !nextRef.current) return;
            const previous = previousRef.current.getBoundingClientRect();
            const next = nextRef.current.getBoundingClientRect();
            setStateElements({ previous, next });
        }

        const observer = new ResizeObserver(observerCallback);
        if (previousRef.current) {
            observer.observe(previousRef.current);
        }
        return () => {
            observer.disconnect();
        };
    }, [previousRef]);

    /** Disable state
     * automatically snap to right if the next element is disabled
     */
    if (disabled_right && previousRef.current && nextRef.current) {
        setWidth(nextRef.current, previousRef.current, 0);
    }
    useEffect(() => {
        //If the disabled state changes
        if (!disabled_right && previousRef.current && nextRef.current) {
            setWidth(previousRef.current, nextRef.current, 350);
        }
    }, [disabled_right, snap_left]);

    return (
        <div
            ref={resizerRef}
            className={styles.resizer}
            data-disabled={disabled_right || disabled_left || disabled}
            data-mirror={
                stateElements && stateElements.previous.width > snap_left
                    ? true
                    : false
            }
            onDoubleClick={onDBClick}
            onPointerDown={onPointerDown}
        >
            <button>
                <BiCaretRight />
                <BiCaretRight />
                <BiCaretRight />
            </button>
        </div>
    );
}

interface StateElements {
    previous: DOMRect;
    next: DOMRect;
}

function setWidth(
    element: HTMLElement,
    otherElement: HTMLElement,
    width: number,
) {
    element.style.width = width + "px";
    // Resizer is 10px
    otherElement.style.width = `calc(100% - ${width}px - 10px)`;
}
