"use client";

import Link from "next/link";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import {
    TbBookDownload,
    TbBooks,
    TbEdit,
    TbMaximize,
    TbMinimize,
} from "react-icons/tb";

import type { BookDataRet } from "@snip/database/types";

import DownloadPdfForm from "./downloadForm";

import styles from "./navigation.module.scss";

/** The bottom navbar shows some
 * buttons for the user to interact with
 * e.g. go back to the bookshelf, download the book,
 * edit the book, etc.
 */
export function BottomNavBar({ book_data }: { book_data: BookDataRet }) {
    return (
        <div className={styles.bottomNavBar}>
            <Link
                href="/"
                passHref
                title="Back to bookshelf"
                aria-label="Back to bookshelf"
            >
                <Button>
                    <TbBooks size={35} />
                </Button>
            </Link>
            <Link
                href={`/books/${book_data.id}/edit`}
                passHref
                title="Edit book"
                aria-label="Edit book"
            >
                <Button>
                    <TbEdit size={35} />
                </Button>
            </Link>
            <DownloadBtn book_data={book_data} />
            <FullScreenButton />
        </div>
    );
}

export function DownloadBtn({ book_data }: { book_data: BookDataRet }) {
    const [show, setShow] = useState(false);

    return (
        <>
            <Button
                className={styles.downloadBtn}
                data-active={false}
                onClick={() => setShow(true)}
                title="Download"
                aria-label="Download as PDF"
                style={{ position: "relative" }}
            >
                <TbBookDownload size={35} />
            </Button>

            <DownloadPdfForm
                book_data={book_data}
                callback_submit={() => setShow(false)}
                callback_cancel={() => setShow(false)}
                show={show}
                onHide={() => setShow(false)}
            />
        </>
    );
}

function FullScreenButton() {
    const [isFullScreen, setIsFullScreen] = useState(false);

    const toggleFullScreen = () => {
        if (document.fullscreenElement) {
            document.exitFullscreen();
            setIsFullScreen(false);
            document.documentElement.dataset.fullscreen = "false";
        } else {
            document.getElementById("the_editor")?.requestFullscreen();
            setIsFullScreen(true);
            document.documentElement.dataset.fullscreen = "true";
        }
    };

    useEffect(() => {
        const fullScreenChange = () => {
            setIsFullScreen(!!document.fullscreenElement);
        };

        document.addEventListener("fullscreenchange", fullScreenChange);

        return () => {
            document.removeEventListener("fullscreenchange", fullScreenChange);
        };
    }, []);

    return (
        <Button
            onClick={toggleFullScreen}
            title="Fullscreen"
            aria-label="Toggle Fullscreen"
            className={styles.fullScreenBtn}
        >
            {isFullScreen ? <TbMinimize size={35} /> : <TbMaximize size={35} />}
        </Button>
    );
}
