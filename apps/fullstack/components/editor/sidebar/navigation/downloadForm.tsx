"use client";
import Loading from "app/(unprotected)/loading";
import { FormEventHandler, useRef, useState } from "react";

import type { BookDataRet } from "@snip/database/types";

import { usePagesContext } from "components/context/socket/usePagesContext";
import { FormHeader } from "components/form";
import { FormError } from "components/form/alert";
import ModalForm from "components/form/modal";

import styles from "components/form/Form.module.scss";

export default function DownloadPdfForm({
    book_data,
    callback_submit,
    callback_cancel = undefined,
    show,
    onHide,
}: {
    book_data: BookDataRet;
    callback_submit: () => void;
    callback_cancel?: () => void;
    show: boolean;
    onHide: () => void;
}) {
    const { pagesArray: pages } = usePagesContext();

    const [downloadToPdfState, setDownloadToPdfState] =
        useState<DownloadPdfParams>({
            downloadAll: true,
            selectedPages: pages.length > 1 ? `1-${pages.length}` : "1",
            pageNumberLocation: "none",
        });

    const [error, setError] = useState<string>();
    const [loading, setLoading] = useState<boolean>(false);
    const abortController = useRef<AbortController>(new AbortController());

    const resetForm = () => {
        (document.getElementById("DownloadPdfForm") as HTMLFormElement).reset();
        setError(undefined);
        setLoading(false);
    };

    const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
        event.preventDefault();
        setError(undefined);
        abortController.current = new AbortController();
        // Step 1: validate inputs

        if (!downloadToPdfState.downloadAll) {
            // Check if string contains ranges or single number
            try {
                parseSelectedPages(downloadToPdfState.selectedPages);
            } catch (error) {
                if (error instanceof Error) setError(error.message);
                console.error(error);
                return;
            }
        } else {
            downloadToPdfState.selectedPages = `1-${pages.length}`;
        }

        // Step 2: submit form and open new tab with pdf

        // Set loading state
        setLoading(true);
        const res = await fetch(`/render/download/${book_data.id}/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(downloadToPdfState),
            signal: abortController.current.signal,
        });

        if (!res.ok) {
            setLoading(false);
            setError(await res.text());
            return;
        }

        await res
            .blob()
            .then((blob) => {
                // Create a new blob with the preserved headers
                const blobWithHeaders = new Blob([blob], {
                    type: "application/pdf",
                });

                // Create a URL for the blob with preserved headers
                const contentDisposition = res.headers.get(
                    "content-disposition",
                );
                const url = URL.createObjectURL(blobWithHeaders);

                // Create a temporary anchor element
                const anchor = document.createElement("a");
                anchor.href = url;
                anchor.download =
                    getFilenameFromContentDisposition(contentDisposition) ||
                    `${book_data.title}.pdf`;
                document.body.appendChild(anchor);

                // Programmatically trigger a click event on the anchor to start the download
                anchor.click();
                document.body.removeChild(anchor);

                // Clean up the URL and temporary anchor
                URL.revokeObjectURL(url);
            })
            .catch((error) => {
                setLoading(false);
                setError(error.message);
                return false;
            });

        // Step 3: reset form and call callback
        resetForm();
        setLoading(false);

        if (callback_submit) {
            callback_submit();
        }
    };

    const handleCancel = () => {
        if (abortController) {
            abortController.current.abort();
        }
        // Reset form
        resetForm();
        if (callback_cancel) {
            callback_cancel();
        }
    };
    return (
        <ModalForm
            onSubmit={handleSubmit}
            id="DownloadPdfForm"
            show={show}
            onHide={onHide}
        >
            <FormHeader>Download book as PDF</FormHeader>
            <FormError>{error}</FormError>
            <PagesSelector
                downloadToPdfState={downloadToPdfState}
                setDownloadToPdfState={setDownloadToPdfState}
            />
            <PageNumbers
                downloadToPdfState={downloadToPdfState}
                setDownloadToPdfState={setDownloadToPdfState}
            />
            <div className="d-flex gap-4">
                {callback_cancel && (
                    <button
                        className="btn btn-outline-secondary w-100"
                        type="button"
                        onClick={handleCancel}
                    >
                        Cancel
                    </button>
                )}
                <button
                    className="btn btn-primary w-100"
                    type="submit"
                    form="DownloadPdfForm"
                    disabled={loading}
                >
                    {loading ? <Loading /> : "Download"}
                </button>
            </div>
        </ModalForm>
    );
}

/** Dropdown on the pages
 * - defaults to "All pages"
 * - if "Custom pages" is selected, show an input field
 *
 */
function PagesSelector({
    downloadToPdfState,
    setDownloadToPdfState,
}: {
    downloadToPdfState: DownloadPdfParams;
    setDownloadToPdfState: React.Dispatch<
        React.SetStateAction<DownloadPdfParams>
    >;
}) {
    return (
        <div className={styles.item}>
            <div className={"input-group mb-3"}>
                <label
                    className="input-group-text"
                    htmlFor="inputGroupSelect01"
                    style={{
                        borderTopLeftRadius: "var(--bs-border-radius)",
                        borderBottomLeftRadius: "var(--bs-border-radius)",
                    }}
                >
                    Selected pages
                </label>
                <select
                    className="form-select"
                    id="pagesToPdf"
                    value={downloadToPdfState.downloadAll ? "all" : "custom"}
                    onChange={(e) =>
                        setDownloadToPdfState((prev) => {
                            return {
                                ...prev,
                                downloadAll: e.target.value === "all",
                            };
                        })
                    }
                >
                    <option value="all">All</option>
                    <option value="custom">Custom</option>
                </select>
            </div>

            {!downloadToPdfState.downloadAll && (
                <>
                    <div className={styles.inputField}>
                        <input
                            className="w-100 form-control form__input"
                            type="text"
                            id="pages"
                            name="pages"
                            placeholder=" "
                            autoComplete="off"
                            required
                            value={downloadToPdfState.selectedPages}
                            onChange={(e) =>
                                setDownloadToPdfState((prev) => {
                                    return {
                                        ...prev,
                                        selectedPages: e.target.value,
                                    };
                                })
                            }
                        />
                        <label htmlFor="pages">Page numbers</label>
                    </div>
                    <span className={styles.description}>
                        You can specify a range of pages or single pages. Define
                        a range with a dash, e.g. 1-5. Define single pages with
                        a comma, e.g. 1,3,5. You can also combine both, e.g.
                        1-5,7,9-11.
                    </span>
                </>
            )}
        </div>
    );
}

/** Small checkbox to include page numbers
 *
 */
function PageNumbers({
    downloadToPdfState,
    setDownloadToPdfState,
}: {
    downloadToPdfState: DownloadPdfParams;
    setDownloadToPdfState: React.Dispatch<
        React.SetStateAction<DownloadPdfParams>
    >;
}) {
    return (
        <div className={styles.item}>
            <div className={"input-group mb-3"}>
                <label
                    className="input-group-text"
                    htmlFor="inputGroupSelect01"
                    style={{
                        borderTopLeftRadius: "var(--bs-border-radius)",
                        borderBottomLeftRadius: "var(--bs-border-radius)",
                    }}
                >
                    Show page numbers
                </label>
                <select
                    className="form-select"
                    id="pageNumberLocation"
                    value={downloadToPdfState.pageNumberLocation}
                    onChange={(e) =>
                        setDownloadToPdfState((prev) => {
                            return {
                                ...prev,
                                pageNumberLocation: e.target
                                    .value as PageNumberLocation,
                            };
                        })
                    }
                >
                    <option value="none">None</option>
                    <option value="br">Bottom right</option>
                    <option value="bl">Bottom left</option>
                    <option value="tr">Top right</option>
                    <option value="tl">Top left</option>
                </select>
            </div>
        </div>
    );
}

export type DownloadPdfParams = {
    downloadAll: boolean;
    selectedPages: string;
    pageNumberLocation: PageNumberLocation;
};

export type PageNumberLocation = "none" | "br" | "tl" | "tr" | "bl" | "custom";

export function parsePageNumberLocation(
    pageNumberLocation: string,
): PageNumberLocation {
    switch (pageNumberLocation) {
        case "none":
        case "br":
        case "tl":
        case "tr":
        case "bl":
            return pageNumberLocation;
        default:
            return "none";
    }
}

export type SelectedPage = [number, number] | number;

/** Validates and parses the selectedPages string
 *
 * throws an error if the string is invalid
 *
 */
export function parseSelectedPages(selectedPages: string): SelectedPage[] {
    const pages: SelectedPage[] = [];

    // Split by comma and parse items
    const items = selectedPages.split(",");

    for (const item of items) {
        if (item === "") {
            continue;
        }

        // Check if item is a range
        if (item.includes("-")) {
            const [start, end] = item.split("-");
            if (!start || !end) throw new Error(`Invalid range `);

            // check if start and end are numbers
            if (
                isNaN(parseInt(start)) ||
                isNaN(parseInt(end)) ||
                !/^\d+$/.test(start) ||
                !/^\d+$/.test(end)
            ) {
                throw new Error(`Invalid range in page numbers! "${item}"`);
            }

            // Check if start is smaller than end
            if (parseInt(start) > parseInt(end)) {
                throw new Error(`Invalid range in page numbers! "${item}"`);
            }

            // Start bigger than 0
            if (parseInt(start) < 1) {
                throw new Error(
                    `First item in range has to be bigger than 0! "${item}"`,
                );
            }
            pages.push([parseInt(start), parseInt(end)]);
        }

        // Check if item is a single page
        else {
            // check if item is a number
            if (isNaN(parseInt(item)) || !/^\d+$/.test(item)) {
                throw new Error(`Invalid page number! "${item}"`);
            }

            // Check if item is bigger than 0
            if (parseInt(item) < 1) {
                throw new Error(
                    `Page number has to be bigger than 0! "${item}"`,
                );
            }
            pages.push(parseInt(item));
        }
    }

    return pages;
}

// Function to extract the filename from the Content-Disposition header
function getFilenameFromContentDisposition(contentDisposition: string | null) {
    if (contentDisposition) {
        const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(contentDisposition);
        if (matches != null && matches[1]) {
            return matches[1].replace(/['"]/g, "");
        }
    }
    return null;
}
