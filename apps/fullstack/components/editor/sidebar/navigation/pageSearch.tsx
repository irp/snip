"use client";

import { ChangeEventHandler } from "react";

import { useSidebarContext } from "../context";

import styles from "./navigation.module.scss";

export default function PageSearch() {
    // Debounce search term so that it only gives us latest value ...
    // ... if searchTerm has not been updated within last 500ms.
    // The goal is to only have the API call fire when user stops typing ...
    // ... so that we aren't hitting our API rapidly.
    const { setSearchPages, isValidating } = useSidebarContext();

    return (
        <SearchBar
            onChange={(e) => setSearchPages(e.target.value)}
            isValidating={isValidating}
        />
    );
}

/** Simple searchbar only containing a search input
 * also some feedback for the user if a query is
 * currently being processed i.e. isValidating
 *  */
function SearchBar({
    onChange,
    isValidating = false,
}: {
    onChange: ChangeEventHandler<HTMLInputElement>;
    isValidating: boolean;
}) {
    return (
        <div className={styles.searchBar}>
            <input type="text" placeholder="Search" onInput={onChange} />
            {isValidating ? (
                <div className="loadingSpinner">
                    <span
                        className="spinner-grow spinner-grow-sm"
                        role="status"
                    ></span>
                </div>
            ) : null}
        </div>
    );
}
