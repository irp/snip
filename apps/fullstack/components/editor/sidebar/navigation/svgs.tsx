import { HTMLProps } from "react";

export const File_plus = (props: HTMLProps<SVGSVGElement>) => {
    return (
        <svg
            fill="currentColor"
            viewBox="0 0 16 16"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path d="M8.5 6a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V10a.5.5 0 0 0 1 0V8.5H10a.5.5 0 0 0 0-1H8.5V6z" />
            <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z" />
        </svg>
    );
};

export const File_right = (props: HTMLProps<SVGSVGElement>) => {
    return (
        <svg
            fill="currentColor"
            viewBox="0 0 16 16"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z" />
            <g transform="translate(-28.339 -.40678)">
                <path d="m34.025 7.9068c-0.66667 0-0.66667 1 0 1h3.4201l-2.147 2.146c-0.472 0.472 0.236 1.18 0.708 0.708l3-3c0.19586-0.19536 0.19586-0.51264 0-0.708l-3-3c-0.472-0.472-1.18 0.236-0.708 0.708l2.147 2.146z" />
            </g>
        </svg>
    );
};

export const File_left = (props: HTMLProps<SVGSVGElement>) => {
    return (
        <svg
            fill="currentColor"
            viewBox="0 0 16 16"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z" />
            <g transform="matrix(-1,0,0,1,44.338982,-0.40677966)">
                <path
                    id="path822"
                    d="m 34.024977,7.9067797 c -0.666667,0 -0.666667,1 0,1 h 3.420118 l -2.147,2.1460013 c -0.472,0.472 0.236,1.18 0.708,0.708 l 3,-3.0000013 c 0.195858,-0.195364 0.195858,-0.512636 0,-0.708 l -3,-2.9999998 c -0.472,-0.4719998 -1.18,0.2360002 -0.708,0.708 l 2.147,2.1459998 z"
                />
            </g>
        </svg>
    );
};
