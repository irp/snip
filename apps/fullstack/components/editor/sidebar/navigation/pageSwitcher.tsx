"use client";
import useMobileSafeContextMenu from "lib/hooks/useMobileSafeContextMenu";
import { useRouter } from "next/navigation";
import { lazy, Suspense, useCallback, useEffect, useState } from "react";
import { Button, ButtonGroup, Modal } from "react-bootstrap";

import { PageDataRet } from "@snip/database/types";

import { File_left, File_plus, File_right } from "./svgs";

import { usePagesContext } from "components/context/socket/usePagesContext";
import { useEditorContext } from "components/editor/context";
import Loading from "components/utils/Loading";

import styles from "./navigation.module.scss";

// Dynamic load advanced page creation form
const AdvancedPageCreationForm = lazy(() => import("./pageCreateForm"));

export default function PageSwitcher({ finished }: { finished: boolean }) {
    const { pagesArray: pages } = usePagesContext();
    const router = useRouter();
    const { activePageIds, bookData } = useEditorContext();

    const [boundaries, setBoundaries] = useState<{
        first: boolean;
        last: boolean;
    }>({ first: false, last: false });

    const updateBoundariesCheck = useCallback(
        (pagesData: PageDataRet | PageDataRet[]) => {
            if (!Array.isArray(pagesData)) {
                pagesData = [pagesData];
            }
            const page_numbers = pagesData.map((p) => p.page_number!);
            const first = page_numbers.some((n) => n <= 0);
            const last = page_numbers.some((n) => n >= pages.length - 1);
            setBoundaries({ first, last });
        },
        [pages.length],
    );

    /** Increment the active page index by the given amount
     * i.e. -1 for previous, 1 for next
     * @param increment
     */
    const incrementPage = useCallback(
        (increment: number) => {
            const currentPages = activePageIds
                .map((id) => pages.filter((p) => p.id === id))
                .flat();

            const next_page_nums = currentPages.map(
                (p) => p.page_number! + increment + 1, //offset by 1 to show [1,n]
            );

            router.push(
                `/books/${bookData.id}/pages/${next_page_nums.join("/")}`,
            );
        },
        [activePageIds, router, bookData.id, pages],
    );

    /** Initial check for boundaries */
    useEffect(() => {
        if (!pages) return;
        const pages_id = activePageIds
            .map((id) => pages.filter((p) => p.id === id))
            .flat();
        updateBoundariesCheck(pages_id);
    }, [activePageIds, pages, updateBoundariesCheck]);

    return (
        <div className={styles.pageSwitcher}>
            <ButtonGroup aria-label="Switcher">
                <Button
                    disabled={boundaries.first || activePageIds.length === 0}
                    onClick={() => incrementPage(-1)}
                    variant="outline-gray"
                >
                    <File_left width="25px" height="30px" alt="Prev" />
                </Button>
                <Button
                    disabled={boundaries.last || activePageIds.length === 0}
                    onClick={() => incrementPage(1)}
                    variant="outline-gray"
                >
                    <File_right width="25px" height="30px" alt="Next" />
                </Button>
            </ButtonGroup>
            <CreatePage finished={finished} />
        </div>
    );
}

function CreatePage({ finished }: { finished: boolean }) {
    const [showAdvanced, setShowAdvanced] = useState(false);

    const { createPage } = usePagesContext();
    const { setActivePageIds } = useEditorContext();

    const eleProps = useMobileSafeContextMenu((e) => {
        e.preventDefault();
        setDynComp(loadDynComp());
        setShowAdvanced(true);
    });

    // Dynamic component loading
    // we do not want to load all the data for
    // this component until it is needed
    const [dynComp, setDynComp] = useState(<div />);
    const loadDynComp = () => {
        return (
            <Suspense fallback={<Loading />}>
                <AdvancedPageCreationForm
                    onCreate={() => setShowAdvanced(false)}
                />
            </Suspense>
        );
    };

    return (
        <>
            <Button
                {...eleProps}
                style={{ marginLeft: "10px" }}
                variant="outline-gray"
                onClick={async () => {
                    const newPage = await createPage();
                    setActivePageIds((prevIds) => {
                        prevIds.pop();
                        prevIds.unshift(newPage.id);
                        return [...prevIds];
                    });
                }}
                disabled={finished}
            >
                <File_plus width="25px" height="30px" alt="+" />
            </Button>
            <Modal
                show={showAdvanced}
                centered
                size="xl"
                fullscreen="lg-down"
                backdrop="static"
                onHide={() => setShowAdvanced(false)}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Create a new page</Modal.Title>
                </Modal.Header>
                <Modal.Body
                    style={{
                        minHeight: "400px",
                        display: "flex",
                    }}
                >
                    {dynComp}
                </Modal.Body>
            </Modal>
        </>
    );
}
