// Create a context in react that provides the crate page state
import { useBackgroundType } from "app/(protected)/(generic_layout)/books/[id]/edit/other/backgroundTypeEditor";
import { BooksGetRet } from "app/api/books/route";
import { fetcher } from "lib/fetcher";
import Image from "next/image";
import React, {
    createContext,
    useContext,
    useEffect,
    useMemo,
    useState,
} from "react";
import FormCheck from "react-bootstrap/esm/FormCheck";
import useSWR from "swr";

import { BackgroundTypeData } from "@snip/database/types";

import { BookSelector } from "components/books/booksSelector";
import { PageSelector } from "components/books/pagesSelector";
import { usePagesContext } from "components/context/socket/usePagesContext";
import { useEditorContext } from "components/editor/context";
import { FormError } from "components/form/alert";
import { SubmitButton } from "components/form/buttons";
import { Item } from "components/form/items";
import Loading from "components/utils/Loading";
/* -------------------------------------------------------------------------- */
/*                              Context provider                              */
/* -------------------------------------------------------------------------- */

interface AdvancedPageCreationState {
    books: BooksGetRet["books"];
    pages: BooksGetRet["pages"];
    backgroundTypes?: BackgroundTypeData[];
    bookBackground?: BackgroundTypeData;
    isLoading: boolean;
    error: Error | null;
}

const AdvancedPageCreationContext =
    createContext<AdvancedPageCreationState | null>(null);

export const AdvancedPageCreationContextProvider = ({
    children,
}: {
    children: React.ReactNode;
}) => {
    // Fetch all available books
    const { bookData } = useEditorContext();
    const {
        data,
        isLoading: isLoadingBooks,
        error: errorBooks,
    } = useSWR<BooksGetRet>(`/api/books?include=pages`, fetcher);

    const {
        backgroundTypes,
        selected: selectedBackground,
        isLoading: isLoadingBackground,
        error: errorBackground,
    } = useBackgroundType(bookData.id);

    // Filter out the current book
    const [books, pages] = useMemo(() => {
        if (!data) return [[], []];
        const b = data.books.filter((b) => b.id !== bookData.id);
        const p = data.pages?.filter((p) => {
            const firstPage = p[0];
            if (!firstPage) return false;
            return firstPage.book_id !== bookData.id;
        });

        return [b, p];
    }, [data, bookData]);

    return (
        <AdvancedPageCreationContext.Provider
            value={{
                books,
                pages,
                backgroundTypes,
                bookBackground: selectedBackground,
                isLoading: isLoadingBooks || isLoadingBackground,
                error: errorBooks || errorBackground,
            }}
        >
            {children}
        </AdvancedPageCreationContext.Provider>
    );
};

export const useAdvancedPageCreation = (): AdvancedPageCreationState => {
    const context = useContext(AdvancedPageCreationContext);
    if (!context) {
        throw new Error(
            "useAdvancedPageCreation must be used within a PageCreateProvider",
        );
    }
    return context;
};

/* -------------------------------------------------------------------------- */

function BackgroundSelector({
    selectedBackgroundId,
    setSelectedBackgroundId,
}: {
    selectedBackgroundId: number | null;
    setSelectedBackgroundId: (id: number | null) => void;
}) {
    const {
        backgroundTypes,
        bookBackground: selectedBackground,
        isLoading,
    } = useAdvancedPageCreation();

    if (isLoading) {
        return <Loading />;
    }

    return (
        <div style={{ display: "flex", flexDirection: "row", gap: "2rem" }}>
            {backgroundTypes?.map((type) => {
                return (
                    <FormCheck
                        key={type.id}
                        type="radio"
                        label={type.name}
                        name="background_type"
                        id={`background_type_${type.id}`}
                        defaultChecked={selectedBackground?.id === type.id}
                        checked={
                            selectedBackgroundId
                                ? type.id === selectedBackgroundId
                                : undefined
                        }
                        onChange={() => {
                            setSelectedBackgroundId(type.id);
                        }}
                    />
                );
            })}
        </div>
    );
}

interface ReferencePage {
    bookId: number | null;
    pageId: number | null;
}

function ReferencePageSelector({
    pageRef,
    setPageRef,
}: {
    pageRef: ReferencePage;
    setPageRef: (page: ReferencePage) => void;
}) {
    const { books, pages, isLoading } = useAdvancedPageCreation();
    const bookIdx = books.findIndex((b) => b.id === pageRef.bookId);

    useEffect(() => {
        console.log("Page ref changed", pageRef);
        console.log("Book idx", bookIdx);
    }, [bookIdx, pageRef]);

    if (isLoading) {
        return <Loading />;
    }

    return (
        <div
            style={{ display: "flex", flexDirection: "column", gap: "0.5rem" }}
        >
            <BookSelector
                books={books}
                currentIdx={bookIdx !== -1 ? bookIdx : null}
                setCurrentIdx={(idx) => {
                    console.log("Selected book", idx);
                    if (idx === null || idx < 0) {
                        setPageRef({ bookId: null, pageId: null });
                        return;
                    }
                    const book = books[idx];
                    if (book) {
                        setPageRef({ bookId: book.id, pageId: null });
                    } else {
                        setPageRef({ bookId: null, pageId: null });
                    }
                }}
                allowNone
            />
            <PageSelector
                pages={pages?.[bookIdx] ?? null}
                currentId={pageRef.pageId}
                setCurrentId={(idx) => {
                    setPageRef({ ...pageRef, pageId: idx });
                }}
                feedbackNone={
                    pageRef.bookId ? "Select a page" : "Select a book first"
                }
            />
        </div>
    );
}

function PageCreationForm({ onCreate }: { onCreate?: () => void }) {
    const { createPage } = usePagesContext();
    const { isLoading, error, bookBackground } = useAdvancedPageCreation();
    const [actionError, setActionError] = useState<string>();
    const [selectedBackgroundId, setSelectedBackgroundId] = useState<
        number | null
    >(null);
    const [selectedPageRef, setSelectedPageRef] = useState<ReferencePage>({
        bookId: null,
        pageId: null,
    });

    async function formAction(_formData: FormData) {
        try {
            await createPage(selectedBackgroundId, selectedPageRef.pageId);
            setActionError(undefined);
            onCreate?.();
        } catch (e) {
            if (e instanceof Error) {
                setActionError(e.message);
            }
            console.error(e);
        }
    }

    if (isLoading) {
        return <Loading />;
    }

    if (error || actionError) {
        return <FormError>{error?.message || actionError}</FormError>;
    }

    return (
        <>
            <div
                style={{
                    display: "flex",
                    flex: "1 1 auto",
                    flexWrap: "wrap",
                }}
            >
                <div
                    style={{
                        maxWidth: "780px",
                        flexGrow: 1,
                        padding: "12px",
                        display: "flex",
                        flexDirection: "column",
                    }}
                >
                    <Item
                        title="Page background"
                        description="Select a background for the page. Defaults to the book background."
                    >
                        <BackgroundSelector
                            selectedBackgroundId={selectedBackgroundId}
                            setSelectedBackgroundId={setSelectedBackgroundId}
                        />
                    </Item>
                    <Item
                        title="Page as reference"
                        description="Allows you to link this page to another page from this or another book. If enabled you may not be able to edit this page but you may edit the origin page."
                    >
                        <ReferencePageSelector
                            pageRef={selectedPageRef}
                            setPageRef={setSelectedPageRef}
                        />
                    </Item>
                    <hr style={{ marginTop: "auto" }}></hr>
                    <div
                        style={{
                            display: "flex",
                            position: "relative",
                            height: "auto",
                            marginTop: "auto",
                            alignItems: "center",
                        }}
                    >
                        <form action={formAction}>
                            <FormError
                                style={{
                                    paddingInline: "12px",
                                    paddingBlock: "6px",
                                    margin: "4px",
                                    width: "100%",
                                }}
                            >
                                {actionError}
                            </FormError>
                            <SubmitButton style={{ minWidth: "180px" }}>
                                Create new page
                            </SubmitButton>
                        </form>
                    </div>
                </div>
                <div
                    style={{
                        padding: "12px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        flexGrow: 1,
                    }}
                >
                    <div style={{ position: "relative", border: "1px solid" }}>
                        <label
                            style={{
                                position: "absolute",
                                top: 0,
                                left: 0,
                                color: "var(--bs-gray-200)",
                                fontSize: "3rem",
                                textAlign: "center",
                                width: "100%",
                            }}
                        >
                            Preview
                        </label>
                        <Image
                            src={
                                selectedBackgroundId?.toString() ||
                                bookBackground?.id.toString() ||
                                "0"
                            }
                            alt="Background"
                            width={300}
                            height={300 * 1.41}
                            loader={({ src }) => {
                                return `/render/background/${src}.jpeg?w=${700}`;
                            }}
                        />
                        {selectedPageRef?.pageId && (
                            <Image
                                src={selectedPageRef.pageId.toString()}
                                alt="Page"
                                width={300}
                                height={300 * 1.41}
                                style={{
                                    position: "absolute",
                                    top: 0,
                                    left: 0,
                                }}
                                loader={({ src }) => {
                                    return `/render/${src}.jpeg?w=${700}&date=${Date.now()}`;
                                }}
                            />
                        )}
                    </div>
                </div>
            </div>
        </>
    );
}

export default function AdvancedPageCreationForm({
    onCreate,
}: {
    onCreate?: () => void;
}) {
    return (
        <AdvancedPageCreationContextProvider>
            <PageCreationForm onCreate={onCreate} />
        </AdvancedPageCreationContextProvider>
    );
}
