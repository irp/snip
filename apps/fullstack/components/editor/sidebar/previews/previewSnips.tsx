"use client";
import {
    Dispatch,
    ReactNode,
    SetStateAction,
    TableHTMLAttributes,
    useEffect,
    useRef,
    useState,
} from "react";
import { Button, Form, Modal } from "react-bootstrap";

import { localDateString } from "@snip/common/dates";
import { ID } from "@snip/database/types";
import { RenderContext } from "@snip/render/types";
import { BaseSnip } from "@snip/snips/general/base";

import UploadDropzone from "./uploadDropzone";

import { useEditorContext } from "components/editor/context";

import styles from "./preview.module.scss";

/** The snip preview components
 * shows a preview of all queued snips
 * in the book. (previously named
 * snipsToPlace)
 *
 * It allows to pass a filter to only show
 * a subset of the snips.
 *
 */
export function PreviewSnips({
    snips,
    filter_ids,
    book_id,
    notSeenSnipIds,
    setNotSeenSnipIds,
}: {
    snips: BaseSnip[];
    filter_ids?: ID[];
    book_id: ID;
    notSeenSnipIds?: ID[];
    setNotSeenSnipIds?: Dispatch<SetStateAction<ID[]>>;
}) {
    const { activeSnipId, setActiveSnipId } = useEditorContext();

    return (
        <div className={styles.wrapper}>
            {snips.toReversed().map((snip, i) => {
                return (
                    <PreviewSingleSnip
                        snip={snip}
                        key={i}
                        hidden={
                            filter_ids ? !filter_ids.includes(snip.id) : false
                        }
                        onClick={(snip, e) => {
                            e.preventDefault();
                            setActiveSnipId((prev) => {
                                if (prev === snip.id) {
                                    return null;
                                }
                                return snip.id;
                            });
                        }}
                        active={snip.id === activeSnipId}
                        newBadge={notSeenSnipIds?.includes(snip.id)}
                        resetNewBadge={() => {
                            setNotSeenSnipIds?.((prev) => {
                                return prev.filter((id) => id !== snip.id);
                            });
                        }}
                    />
                );
            })}
            <UploadDropzone
                book_id={book_id}
                setActiveSnipId={setActiveSnipId}
            />
        </div>
    );
}

import useMobileSafeContextMenu from "lib/hooks/useMobileSafeContextMenu";
import {
    TbCopyPlus,
    TbEdit,
    TbLink,
    TbLinkOff,
    TbTrash,
    TbWorldCheck,
    TbWorldUpload,
} from "react-icons/tb";

import { hasOwnProperty } from "@snip/common";
import { get_snip_from_data } from "@snip/snips";
import { ArraySnip } from "@snip/snips/general/array";
import { LinkSnip } from "@snip/snips/general/link";
import type { MarkdownSnip } from "@snip/snips/general/markdown";

import { HoverInfo } from "components/common/utils";
import { useQueuedSnipsContext } from "components/context/socket/useQueuedSnipsContext";
import { MarkdownModal } from "components/editor/markdown/modal";
import { ButtonWithConfirm } from "components/form/buttons";
import { TextInput } from "components/form/inputs/text";

/**
 * Renders a preview of a single snip.
 *
 * @param snipData - The data of the snip to be rendered.
 * @param active - Indicates whether the snip is active.
 * @param hidden - Indicates whether the snip is hidden.
 * @param onClick - The callback function to be called when the snip is clicked.
 * @param newBadge - Indicates whether the snip is new. Shows a badge if true.
 */
function PreviewSingleSnip({
    snip,
    active = false,
    hidden = false,
    onClick,
    newBadge,
    resetNewBadge,
}: {
    snip: BaseSnip;
    active?: boolean;
    hidden?: boolean;
    onClick?: (
        snip: BaseSnip,
        e: React.MouseEvent<
            HTMLDivElement | HTMLCanvasElement | HTMLSpanElement,
            MouseEvent
        >,
    ) => void;
    newBadge?: boolean;
    resetNewBadge?: () => void;
}) {
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const { events } = useQueuedSnipsContext();

    // Prevents hydration mismatch
    const [info, setInfo] = useState<Info>({
        width: 0,
        height: 0,
        created_at: "unknown",
        id: -1,
    });

    // Context menu
    const [showContext, setShowContext] = useState(false);

    const contextMenuProps = useMobileSafeContextMenu((e) => {
        e.preventDefault();
        setShowContext((p) => !p);
    });

    //Render snip
    useEffect(() => {
        const ctx = canvasRef.current!.getContext("2d")!;

        function renderSnip(snip: BaseSnip, ctx: RenderContext) {
            if (ctx) {
                ctx.save();
                ctx.resetTransform();
                ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
                ctx.restore();
                //Scale canvas to fit snip
                const scale = Math.min(
                    150 / snip.width,
                    (150 * 1.41) / snip.height,
                );
                ctx.setTransform(scale, 0, 0, scale, 0, 0);
                ctx.translate(-snip.x, -snip.y);
                snip.render(ctx);
            }
        }

        const helper = async () => {
            if (Object.hasOwn(snip, "ready")) {
                await snip.ready;
            }
            renderSnip(snip, ctx);
            setInfo({
                width: snip.width,
                height: snip.height,
                created_at: snip.created ? localDateString(snip.created) : "",
                id: snip.id,
            });
        };

        helper();

        // Render snip on update
        const controller = new AbortController();
        events?.addEventListener(
            "updated",
            (e) => {
                if (e.snip_id !== snip.id) return;
                helper();
            },
            { signal: controller.signal },
        );

        return () => {
            //clearInterval(intervalID);
            controller.abort();
            ctx?.clearRect(0, 0, 20000, 20000);
        };
    }, [snip, events]);

    const divRef = useRef<HTMLDivElement>(null);

    const outsideClickListener = (e: MouseEvent) => {
        if (!divRef.current) {
            return removeClickListener();
        }

        if (!divRef.current.contains(e.target as Node)) setShowContext(false);
        removeClickListener();
    };

    const removeClickListener = () => {
        document.removeEventListener("click", outsideClickListener);
    };

    return (
        <>
            <div
                ref={divRef}
                className={styles.snip}
                data-active={active}
                data-hidden={hidden}
                onPointerMove={() => {
                    if (newBadge) {
                        resetNewBadge?.();
                    }
                }}
                {...contextMenuProps}
            >
                <FlipCard
                    flipped={showContext}
                    backside={
                        <ContextMenu
                            snip={snip}
                            info={info}
                            onClick={onClick}
                        />
                    }
                >
                    <canvas
                        ref={canvasRef}
                        width={150}
                        height={150 * 1.41}
                        onClick={(e) => onClick?.(snip, e)}
                        style={{
                            width: "100%",
                        }}
                    />
                </FlipCard>
                <div
                    className={styles.topbar}
                    onClick={(e) => {
                        e.stopPropagation();
                        setShowContext(!showContext);
                    }}
                >
                    {newBadge && <div className={styles.newBadge}>New</div>}
                    <div className={styles.typeText}>{snip.type}</div>
                </div>
            </div>
        </>
    );
}
/** We need the info type as snips are not registered as state normally */
type Info = { width: number; height: number; created_at: string; id: number };

function InfoTable({
    collapsed,
    info,
    ...props
}: { collapsed: boolean; info: Info } & TableHTMLAttributes<HTMLTableElement>) {
    return (
        <table
            style={{
                display: collapsed ? "block" : "none",
            }}
            className={styles.info}
            {...props}
        >
            <tbody>
                <tr>
                    <th>Id</th>
                    <td>
                        {info.id > 0 ? info.id : "Not synced (" + info.id + ")"}
                    </td>
                </tr>
                <tr>
                    <th>Created</th>
                    <td>{info.created_at}</td>
                </tr>
                <tr>
                    <th>Width</th>
                    <td>{info.width.toFixed()}</td>
                </tr>
                <tr>
                    <th>Height</th>
                    <td>{info.height.toFixed()}</td>
                </tr>
            </tbody>
        </table>
    );
}

function FlipCard({
    flipped,
    children,
    backside,
}: {
    flipped: boolean;
    children: ReactNode;
    backside: ReactNode;
}) {
    return (
        <div className={styles.card} data-flip={flipped}>
            <div className={styles.front}>{children}</div>
            <div className={styles.back}>{backside}</div>
        </div>
    );
}

function ContextMenu({
    snip,
    info,
    onClick,
}: {
    snip: BaseSnip;
    info: Info;
    onClick?: (
        snip: BaseSnip,
        e: React.MouseEvent<
            HTMLDivElement | HTMLCanvasElement | HTMLSpanElement,
            MouseEvent
        >,
    ) => void;
}) {
    const { removeQueuedSnip } = useQueuedSnipsContext();

    let title = snip.type;
    if (hasOwnProperty(snip, "snips")) {
        title +=
            " (containing " +
            (snip as ArraySnip).snips.map((s) => s.type).join(", ") +
            ")";
    }
    if (hasOwnProperty(snip, "snip")) {
        title += " (containing " + (snip as LinkSnip).snip.type + ")";
    }

    // Is inserted
    const is_inserted = snip.id > 0;

    return (
        <div className={styles.contextMenu}>
            <span onClick={(e) => onClick?.(snip, e)} className="h-100">
                <div className={styles.header}>
                    <div className={styles.placeholder}></div>
                    <div className={styles.title}>{title}</div>
                </div>
                <InfoTable info={info} collapsed={true} />
            </span>

            {/* Bottom action items
                3 actions per row
            */}
            <div className={styles.actions}>
                {is_inserted ? (
                    <HoverInfo tooltip="Snippet synchronized with server">
                        <div
                            className={styles.button}
                            style={{
                                color: "var(--bs-secondary)",
                            }}
                        >
                            <TbWorldCheck />
                        </div>
                    </HoverInfo>
                ) : (
                    <HoverInfo tooltip="Upload Snippet">
                        <Button
                            variant="outline-secondary"
                            className={styles.button}
                            onClick={(e) => {
                                e.preventDefault();
                                e.stopPropagation();
                                alert(
                                    "WIP upload local snippet comes in a future version",
                                );
                            }}
                        >
                            <TbWorldUpload />
                        </Button>
                    </HoverInfo>
                )}
                {snip.type == "markdown" && (
                    <EditBtn snip={snip as MarkdownSnip} />
                )}
            </div>
            <div className={styles.actions}>
                <DuplicateSnip snip={snip} />
                <LinkModifier snip={snip} />
                <HoverInfo tooltip="Delete Snippet from Queue!">
                    <ButtonWithConfirm
                        className={styles.button}
                        onConfirmed={(e) => {
                            e.preventDefault();
                            e.stopPropagation();
                            removeQueuedSnip(snip.id, undefined, false);
                        }}
                    >
                        <TbTrash />
                    </ButtonWithConfirm>
                </HoverInfo>
            </div>
        </div>
    );
}

function EditBtn({ snip }: { snip: MarkdownSnip }) {
    const [show, setShow] = useState(false);

    return (
        <>
            <HoverInfo tooltip="Edit snippet">
                <Button
                    variant="outline-secondary"
                    className={styles.button}
                    onClick={(e) => {
                        setShow(true);
                        (e.target as HTMLElement).blur();
                    }}
                >
                    <TbEdit />
                </Button>
            </HoverInfo>
            <MarkdownModal show={show} setShow={setShow} snip={snip} />
        </>
    );
}

interface LinkModifierProps {
    snip: BaseSnip;
}

const LinkModifier: React.FC<LinkModifierProps> = ({ snip }) => {
    const [showModal, setShowModal] = useState(false);
    const [invalidFeedback, setInvalidFeedback] = useState<string | undefined>(
        undefined,
    );
    const [href, setHref] = useState("");
    const { transformSnip, removeQueuedSnip } = useQueuedSnipsContext();
    const { activeSnipId, setActiveSnipId } = useEditorContext();

    const is_link = snip instanceof LinkSnip;

    const handleCloseModal = () => setShowModal(false);
    const handleShowModal = () => setShowModal(true);

    const handleSubmit = async () => {
        if (is_link) {
            await convertLinkSnipToSnip(snip);
        } else {
            // check if model is show
            if (!showModal) {
                return handleShowModal();
            }

            //Validate if link using url mdn
            let url: URL;
            try {
                // Some quality of life additions
                // add https if not present
                if (!href.startsWith("http") && !href.startsWith("https")) {
                    url = new URL("https://" + href);
                } else {
                    url = new URL(href);
                }
            } catch (_e) {
                setInvalidFeedback("Url is invalid");
                setTimeout(() => setInvalidFeedback(undefined), 5000);
                return;
            }
            await convertSnipToLinkSnip(snip, url);
        }
        handleCloseModal();
    };

    async function convertSnipToLinkSnip(snip: BaseSnip, url: URL) {
        const n_snip = await transformSnip(snip.id, (snip) => {
            return new LinkSnip({
                snip,
                href: url.href,
                book_id: snip.book_id,
            });
        });

        // Select link snip in editor if active snipid
        if (snip.id === activeSnipId) {
            setActiveSnipId(n_snip.id);
        }
    }

    async function convertLinkSnipToSnip(snip: LinkSnip) {
        const n_snip = await transformSnip(snip.id, (snip) => {
            removeQueuedSnip(snip.id, undefined, false, false);
            return (snip as LinkSnip).snip;
        });

        // Select link snip in editor if active snipid
        if (snip.id === activeSnipId) {
            setActiveSnipId(n_snip.id);
        }
    }

    return (
        <>
            <HoverInfo
                tooltip={
                    is_link ? "Remove Link from Snippet" : "Add link to Snippet"
                }
            >
                <Button
                    variant="outline-secondary"
                    className={styles.button}
                    onClick={(e) => {
                        e.stopPropagation();
                        handleSubmit();
                    }}
                >
                    {is_link ? <TbLinkOff /> : <TbLink />}
                </Button>
            </HoverInfo>
            <Modal show={showModal} onHide={handleCloseModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Add Link to Snippet</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form
                        onSubmit={(e) => {
                            e.stopPropagation();
                            e.preventDefault();
                            handleSubmit();
                        }}
                        className="d-flex flex-column gap-2"
                    >
                        <Form.Group>
                            <TextInput
                                value={href}
                                name="url"
                                label="Link url"
                                isInvalid={invalidFeedback != undefined}
                                onInput={(
                                    e: React.ChangeEvent<HTMLInputElement>,
                                ) => {
                                    setHref(
                                        (e.target as HTMLInputElement).value,
                                    );
                                }}
                                invalidFeedback={invalidFeedback}
                                autoComplete="off"
                            />
                        </Form.Group>
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={(e) => e.stopPropagation()}
                        >
                            Convert to link snippet
                        </Button>
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    );
};

interface DuplicateSnipProps {
    snip: BaseSnip;
}

const DuplicateSnip: React.FC<DuplicateSnipProps> = ({ snip }) => {
    const { addTempSnip } = useQueuedSnipsContext();

    return (
        <HoverInfo tooltip="Duplicate Snippet">
            <Button
                variant="outline-secondary"
                className={styles.button}
                onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    const snip_copy = structuredClone(snip.to_data());
                    snip_copy.id = Math.floor(Math.random() * 1000000) * -1;
                    addTempSnip(get_snip_from_data(snip_copy));
                }}
            >
                <TbCopyPlus />
            </Button>
        </HoverInfo>
    );
};
