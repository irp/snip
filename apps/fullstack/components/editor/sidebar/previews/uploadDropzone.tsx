import { Dispatch, useCallback, useState } from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import { FileRejection, useDropzone } from "react-dropzone";
import { BsImage } from "react-icons/bs";

import styles from "./preview.module.scss";

/** This tool and the corresponding api endpoint
 * need major refactoring. It is currently a mess
 * and should be rewritten from scratch.
 * Desired features should be to upload snips via
 * their data object. Such that client and server
 * can easily sync snips.
 *
 * The full snip is send back in another place see book event listener
 *
 */
export default function UploadDropzone({
    book_id,
    setActiveSnipId,
}: {
    book_id: number;
    setActiveSnipId: Dispatch<number>;
}) {
    const [progress, setProgress] = useState<number>();
    const [error, setError] = useState<string>();

    const onLoad = useCallback(
        (snip_ids: number | number[]) => {
            // Set first active snip
            if (snip_ids instanceof Array) {
                setActiveSnipId && setActiveSnipId(snip_ids[0]!);
            } else {
                setActiveSnipId && setActiveSnipId(snip_ids);
            }
            setProgress(undefined);
        },
        [setActiveSnipId],
    );

    // Send file to server with xmlHttpRequest
    const onDrop = useCallback(
        (acceptedFiles: File[], fileRejections: FileRejection[]) => {
            // Do something with the files
            if (fileRejections.length > 0) {
                setError(fileRejections[0]!.errors[0]!.message);
                setTimeout(() => {
                    setError(undefined);
                }, 6000);
            } else {
                uploadFiles(
                    book_id,
                    acceptedFiles as unknown as FileList,
                    onLoad,
                    setProgress,
                );
            }
        },
        [book_id, onLoad],
    );

    // Configure dropzone
    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
        accept: {
            "image/*": [".jpeg", ".png"],
            "text/*": [".txt"],
        },
        multiple: true,
    });

    return (
        <div {...getRootProps()} className={styles.uploadForm}>
            <div className={styles.dropzone} data-drop-active={isDragActive}>
                <input {...getInputProps()} />
                <div className="p-2 w-100">
                    {isDragActive ? (
                        <div>Drop here! </div>
                    ) : (
                        <div>
                            Drag &apos;n&apos; drop files here, or click to
                            select files
                        </div>
                    )}
                </div>
                <div className="d-flex justify-content-center align-items-center flex-column">
                    {error ? (
                        <div className="alert alert-danger">{error}</div>
                    ) : null}
                    {<BsImage size={80} />}
                    {progress ? (
                        <ProgressBar
                            variant="primary"
                            now={progress}
                            label={`${progress.toFixed(2)}%`}
                        />
                    ) : null}
                </div>
            </div>
        </div>
    );
}

function uploadFiles(
    book_id: number,
    files: FileList,
    onLoad?: (snip_id: number | number[]) => void,
    onProgress?: (progress: number) => void,
): XMLHttpRequest {
    // create new XMLHttpRequest and add event listeners
    const req = new XMLHttpRequest();
    req.open("POST", `/api/books/${book_id}/upload`);
    req.addEventListener("load", () => {
        const snip_ids = JSON.parse(req.response)["ids"];
        onLoad && onLoad(snip_ids);
    });
    req.upload.addEventListener("progress", (e: ProgressEvent) => {
        onProgress && onProgress((e.loaded / e.total) * 100);
    });

    // create form data and append files
    const form = new FormData();
    for (let file = 0; file < files.length; file++) {
        form.append("file" + file, files[file]!, files[file]!.name);
    }

    // send post request see api/book/id/upload
    req.send(form);
    return req;
}
