"use client";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useEffect, useRef } from "react";

import { isInViewport } from "@snip/common";
import { PageDataRet } from "@snip/database/types";

import { useSidebarContext } from "../context";

import { usePagesContext } from "components/context/socket/usePagesContext";
import {
    OnlineUser,
    useSocketContext,
} from "components/context/useSocketContext";
import { useEditorContext } from "components/editor/context";

import styles from "./preview.module.scss";

/** The page previews component shows
 * a preview of all pages in the book.
 * Each page is an image as rendered
 * by the render service.
 *
 * On an update the
 *
 * It allows to pass a filter to only show
 * a subset of the pages.
 */
export function PreviewPages() {
    const router = useRouter();
    const { pagesArray: pages } = usePagesContext();
    const { filterPages } = useSidebarContext();
    const { activePageIds, bookData } = useEditorContext();
    const { pageToOnlineUsers } = useSocketContext();

    return (
        <div className={styles.wrapper}>
            {pages.map((page, i) => {
                return (
                    <PreviewSinglePage
                        page={page}
                        key={i}
                        hidden={
                            filterPages ? !filterPages.includes(page.id) : false
                        }
                        onClick={(page, e) => {
                            router.push(
                                `/books/${bookData?.id}/pages/${page.page_number! + 1}`,
                            );
                        }}
                        active={activePageIds?.includes(page.id)}
                        onlineUsers={pageToOnlineUsers.get(page.id) || []}
                    />
                );
            })}
        </div>
    );
}

/**
 * Renders a single page preview.
 *
 * @param page - The page data.
 * @param active - Indicates if the page is active.
 * @param hidden - Indicates if the page is hidden.
 * @param onClick - The function to call when the page is clicked.
 */
export function PreviewSinglePage({
    page,
    active = false,
    hidden = false,
    onClick,
    onlineUsers = [],
}: {
    page: PageDataRet;
    active?: boolean;
    hidden?: boolean;
    onClick?: (
        page: PageDataRet,
        e: React.MouseEvent<HTMLDivElement, MouseEvent>,
    ) => void;
    onlineUsers: OnlineUser[];
}) {
    // Scroll to page preview when active
    const ref = useRef<HTMLImageElement>(null);

    useEffect(() => {
        if (active && ref.current && !isInViewport(ref.current)) {
            ref.current.scrollIntoView({
                behavior: "smooth",
                block: "nearest",
                inline: "nearest",
            });
        }
    }, [active]);

    return (
        <div
            className={styles.page}
            data-active={active}
            data-hidden={hidden}
            onClick={(e) => onClick?.(page, e)}
            ref={ref}
        >
            <ImagePagePreview page={page} />
            <div className={styles.labels_top}>
                {page.referenced_page_id && (
                    <label
                        style={{ color: "#CF7843", borderColor: "#CF7843" }}
                        title="This page is a reference to another page!"
                    >
                        Ref
                    </label>
                )}
                <label>{page.page_number! + 1}</label>
            </div>
            {onlineUsers.length > 0 && (
                <div className={styles.onlineUsers}>
                    {onlineUsers.map((user, i) => {
                        if (i == 6) {
                            return (
                                <label
                                    key={i}
                                    title={
                                        "" + (onlineUsers.length - 6) + " more"
                                    }
                                >
                                    ..
                                </label>
                            );
                        }
                        if (i > 6) return;

                        return (
                            <label
                                key={i}
                                title={user?.email || ""}
                                style={{
                                    color: `rgba(${user.color.join(",")})`,
                                    borderColor: `rgba(${user.color.join(",")})`,
                                }}
                            >
                                {user.email[0]?.toLocaleUpperCase()}
                            </label>
                        );
                    })}
                </div>
            )}
        </div>
    );
}

export function ImagePagePreview({ page }: { page: PageDataRet }) {
    return (
        <Image
            src={page.id.toString()}
            alt="Page"
            width={150}
            height={150 * 1.41}
            loader={({ src, width }) => {
                // 200 hardcoded for now
                return `/render/${src}.jpeg?w=${width}&date=${page.last_updated?.valueOf()}`;
            }}
        />
    );
}
