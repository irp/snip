"use client";
import { useState } from "react";
import { Alert, Nav } from "react-bootstrap";
import Tab from "react-bootstrap/Tab";

import type { BookDataRet } from "@snip/database/types";

import { useEditorContext } from "../context";
import { SidebarContextProvider } from "./context";
import { BottomNavBar } from "./navigation/bottomNavBar";
import PageSearch from "./navigation/pageSearch";
import PageSwitcher from "./navigation/pageSwitcher";
import { PreviewPages } from "./previews/previewPages";
import { PreviewSnips } from "./previews/previewSnips";

import { useQueuedSnipsContext } from "components/context/socket/useQueuedSnipsContext";

import styles from "./sidebar.module.scss";

/** The sidebar shows all pages and a preview
 * of them in the book.
 */
export default function Sidebar({
    init_with_width,
}: {
    init_with_width: boolean;
}) {
    const { bookData } = useEditorContext();
    // Get all page ids for the book and request render
    const {
        snipsArray: snips,
        notSeenSnips,
        setNotSeenSnips,
    } = useQueuedSnipsContext();

    // ActiveTab
    const [activeTab, setActiveTab] = useState<string>("pages");

    return (
        <SidebarContextProvider book_id={bookData.id}>
            <div className={styles.sideBar} data-init-width={init_with_width}>
                <Tab.Container
                    activeKey={activeTab}
                    onSelect={(k) => {
                        if (k === null) return;
                        setActiveTab(k);
                    }}
                >
                    <div className={styles.tabBar}>
                        <Nav>
                            <Nav.Item>
                                <Nav.Link eventKey="pages" as="button">
                                    Pages
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="position-relative">
                                <Nav.Link eventKey="queued" as="button">
                                    Queue
                                </Nav.Link>
                                {activeTab === "pages" &&
                                notSeenSnips.length > 0 ? (
                                    <div className={styles.notificationBadge}>
                                        {notSeenSnips.length}
                                    </div>
                                ) : null}
                            </Nav.Item>
                        </Nav>
                        <TopNavigation activeTab={activeTab} />
                        <SidebarAlerts bookData={bookData} />
                    </div>
                    <Tab.Content>
                        <Tab.Pane eventKey="pages" className={styles.tabPane}>
                            <PreviewPages />
                        </Tab.Pane>
                        <Tab.Pane eventKey="queued" className={styles.tabPane}>
                            <PreviewSnips
                                snips={snips}
                                book_id={bookData.id}
                                notSeenSnipIds={notSeenSnips}
                                setNotSeenSnipIds={setNotSeenSnips}
                            />
                        </Tab.Pane>
                    </Tab.Content>
                    <BottomNavBar book_data={bookData} />
                </Tab.Container>
            </div>
        </SidebarContextProvider>
    );
}

function TopNavigation({ activeTab }: { activeTab: string }) {
    return (
        <div className={styles.topNavigation}>
            {activeTab === "pages" ? <PageSearch /> : null}
            <PageSwitcher finished={false} />
        </div>
    );
}

function SidebarAlerts({ bookData }: { bookData: BookDataRet }) {
    if (!bookData.finished) {
        return null;
    }

    return (
        <Alert className="my-2 mx-2 fs-6 fw-bold" variant="danger">
            This book is finished! It can no longer be edited. You may change
            the finished state in the book settings.
        </Alert>
    );
}
