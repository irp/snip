"use client";

/** Relative simple text markdown editor that shows a configurable preview.
 *
 * The preview is using our markdown snip class.
 */
import {
    ChangeEventHandler,
    createContext,
    Dispatch,
    forwardRef,
    KeyboardEventHandler,
    PointerEvent,
    SetStateAction,
    useContext,
    useEffect,
    useImperativeHandle,
    useRef,
    useState,
} from "react";
import {
    TbBold,
    TbCode,
    TbDots,
    TbEye,
    TbEyeOff,
    TbH1,
    TbH2,
    TbH3,
    TbH4,
    TbH5,
    TbHeading,
    TbItalic,
} from "react-icons/tb";
import { usePopper } from "react-popper";

import { MarkdownSnip } from "@snip/snips/general/markdown";

import { useDrag } from "../tools/common/drag";

import styles from "./textEditor.module.scss";

type Text = {
    value: string;
    caret: { start: number; end: number } | null;
    target: HTMLTextAreaElement | null;
};

interface MarkdownEditorContext {
    previewVisible: boolean;
    setPreviewVisible: Dispatch<SetStateAction<boolean>>;
    text: Text;
    setText: Dispatch<SetStateAction<Text>>;
}

const MarkdownEditorContext = createContext<MarkdownEditorContext | null>(null);

function useMarkdownEditorContext() {
    const ctx = useContext(MarkdownEditorContext);
    if (!ctx)
        throw new Error(
            "useMarkdownEditorContext must be used within a MarkdownEditorContext",
        );
    return ctx;
}

function MarkdownEditorProvider({
    children,
    initial_text,
}: {
    children: React.ReactNode;
    initial_text: string;
}) {
    const [previewVisible, setPreviewVisible] = useState(true);
    const [text, setText] = useState<Text>({
        value: initial_text,
        caret: null,
        target: null,
    });

    return (
        <MarkdownEditorContext.Provider
            value={{ previewVisible, setPreviewVisible, text, setText }}
        >
            {children}
        </MarkdownEditorContext.Provider>
    );
}

/* --------------------------- Toolbar and buttons -------------------------- */

function ToolBar() {
    const { previewVisible, setPreviewVisible } = useMarkdownEditorContext();

    return (
        <div className={styles.toolbar}>
            <Bold />
            <Italic />
            <Header />
            <Code />
            <div
                style={{
                    marginLeft: "auto",
                    fontSize: "0.8rem",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    color: "var(--bs-gray-600)",
                    textAlign: "center",
                }}
            >
                Markdown is still experimental, if you encounter any issues
                please let us know.
            </div>

            <button
                style={{ marginLeft: "auto" }}
                onClick={() => setPreviewVisible((prev) => !prev)}
            >
                {previewVisible ? <TbEye /> : <TbEyeOff />}
            </button>
        </div>
    );
}

function Header() {
    const { setText } = useMarkdownEditorContext();

    return (
        <__Header
            onHeaderClick={(e, level) => {
                e.preventDefault();
                setText((prev) => {
                    // Insert the # at the begin of the line
                    const val = prev.value;
                    const start = prev.target?.selectionStart ?? 0;
                    const end = prev.target?.selectionEnd ?? 0;

                    const line_start =
                        val.substring(0, start).lastIndexOf("\n") + 1;

                    const nVal =
                        val.substring(0, line_start) +
                        "#".repeat(level) +
                        " " +
                        val.substring(line_start);
                    return {
                        ...prev,
                        value: nVal,
                        caret: {
                            start: line_start + level + 1,
                            end: end + level + 1,
                        },
                    };
                });
            }}
        />
    );
}

function __Header({
    onHeaderClick,
}: {
    onHeaderClick?: (e: PointerEvent<HTMLButtonElement>, level: number) => void;
}) {
    const [visible, setVisibility] = useState(false);
    const [referenceElement, setReferenceElement] =
        useState<HTMLButtonElement | null>(null);
    const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(
        null,
    );

    const { styles: pStyles, attributes } = usePopper(
        referenceElement,
        popperElement,
        {
            placement: "bottom-start",
        },
    );

    function onClick(e: PointerEvent<HTMLButtonElement>, level: number) {
        setVisibility(false);
        onHeaderClick?.(e, level);
    }

    return (
        <>
            <button
                ref={setReferenceElement}
                onClick={() => {
                    setVisibility(!visible);
                }}
                className={styles.dropdownButton}
                data-active={visible}
            >
                <TbHeading />
            </button>

            <div
                ref={setPopperElement}
                style={{
                    ...pStyles.popper,
                    width: referenceElement?.offsetWidth + "px",
                }}
                {...attributes.popper}
            >
                <div
                    style={{
                        display: visible ? "flex" : "none",
                        flexDirection: "column",
                    }}
                    className={styles.dropdown}
                    title="Header"
                >
                    <button onPointerDown={(e) => onClick(e, 1)}>
                        <TbH1 />
                    </button>
                    <button onPointerDown={(e) => onClick(e, 2)}>
                        <TbH2 />
                    </button>
                    <button onPointerDown={(e) => onClick(e, 3)}>
                        <TbH3 />
                    </button>
                    <button onPointerDown={(e) => onClick(e, 4)}>
                        <TbH4 />
                    </button>
                    <button onPointerDown={(e) => onClick(e, 5)}>
                        <TbH5 />
                    </button>
                </div>
            </div>
        </>
    );
}

function Italic() {
    const { setText } = useMarkdownEditorContext();

    return (
        <button
            onPointerDown={(e) => {
                e.preventDefault();
                setText((prev) => {
                    // Insert the ** at the caret position
                    const val = prev.value;
                    const start = prev.target?.selectionStart ?? 0;
                    const end = prev.target?.selectionEnd ?? 0;

                    if (start == end) {
                        const nVal =
                            val.substring(0, start) + "**" + val.substring(end);
                        return {
                            ...prev,
                            value: nVal,
                            caret: {
                                start: start + 1,
                                end: end + 1,
                            },
                        };
                    }

                    // Wrap the selected text with **
                    const nVal =
                        val.substring(0, start) +
                        "*" +
                        val.substring(start, end) +
                        "*" +
                        val.substring(end);

                    return {
                        ...prev,
                        value: nVal,
                        caret: {
                            start: start + 1,
                            end: end + 1,
                        },
                    };
                });
            }}
        >
            <TbItalic />
        </button>
    );
}

function Bold() {
    const { setText } = useMarkdownEditorContext();

    return (
        <button
            onPointerDown={(e) => {
                e.preventDefault();
                setText((prev) => {
                    // Insert the ** at the caret position
                    const val = prev.value;
                    const start = prev.target?.selectionStart ?? 0;
                    const end = prev.target?.selectionEnd ?? 0;

                    if (start == end) {
                        const nVal =
                            val.substring(0, start) +
                            "****" +
                            val.substring(end);
                        return {
                            ...prev,
                            value: nVal,
                            caret: {
                                start: start + 2,
                                end: end + 2,
                            },
                        };
                    }

                    // Wrap the selected text with **
                    const nVal =
                        val.substring(0, start) +
                        "**" +
                        val.substring(start, end) +
                        "**" +
                        val.substring(end);

                    return {
                        ...prev,
                        value: nVal,
                        caret: {
                            start: start + 2,
                            end: end + 2,
                        },
                    };
                });
            }}
        >
            <TbBold />
        </button>
    );
}

function Code() {
    const { setText } = useMarkdownEditorContext();

    return (
        <button
            onPointerDown={(e) => {
                e.preventDefault();
                setText((prev) => {
                    // Inserts the ``` at the caret position
                    const val = prev.value;
                    const start = prev.target?.selectionStart ?? 0;
                    const end = prev.target?.selectionEnd ?? 0;

                    if (start == end) {
                        const nVal =
                            val.substring(0, start) +
                            "```\n\n```" +
                            val.substring(end);

                        //prev.target?.setSelectionRange(start + 4, end + 4);
                        return {
                            ...prev,
                            value: nVal,
                            caret: {
                                start: start + 4,
                                end: end + 4,
                            },
                        };
                    }

                    // Wrap the selected text with ```
                    const nVal =
                        val.substring(0, start) +
                        "```\n" +
                        val.substring(start, end) +
                        "\n```" +
                        val.substring(end);

                    return {
                        ...prev,
                        value: nVal,
                        caret: {
                            start: start + 4,
                            end: end + 4,
                        },
                    };
                });
            }}
        >
            <TbCode />
        </button>
    );
}

/* ---------------------------- Preview and input --------------------------- */

function InputAndPreview() {
    return (
        <ResizeWrapper>
            <Input />
            <Preview />
        </ResizeWrapper>
    );
}

const TabSpaces = 4;

function Input() {
    const ref = useRef<HTMLTextAreaElement>(null);
    const { text, setText } = useMarkdownEditorContext();

    useEffect(() => {
        if (text.caret && text.target) {
            if (
                ref.current?.selectionStart !== text.caret.start ||
                ref.current?.selectionEnd !== text.caret.end
            ) {
                ref.current?.setSelectionRange(
                    text.caret.start,
                    text.caret.end,
                );
            }
        }
    }, [text.caret, text.target]);

    const handleTab: KeyboardEventHandler<HTMLTextAreaElement> = (e) => {
        const target = e.target as HTMLTextAreaElement;
        const content = target.value;
        if (e.key === "Tab") {
            e.preventDefault();

            const newText =
                content.substring(0, target.selectionStart) +
                " ".repeat(TabSpaces) +
                content.substring(target.selectionStart);

            setText({
                value: newText,
                caret: {
                    start: target.selectionStart + TabSpaces,
                    end: target.selectionEnd + TabSpaces,
                },
                target: target,
            });
        }
    };

    const handleText: ChangeEventHandler<HTMLTextAreaElement> = (e) =>
        setText({
            value: e.target.value,
            caret: {
                start: e.target.selectionStart,
                end: e.target.selectionEnd,
            },
            target: e.target as HTMLTextAreaElement,
        });

    return (
        <div className={styles.input}>
            <textarea
                ref={ref}
                value={text.value}
                onKeyDown={handleTab}
                onChange={handleText}
            />
        </div>
    );
}

function Preview() {
    const snipRef = useRef<MarkdownSnip | null>(null);
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const { previewVisible, text } = useMarkdownEditorContext();

    // Create snip and change text on text change
    useEffect(() => {
        if (!canvasRef.current) return;
        const canvas = canvasRef.current;

        if (!snipRef.current && text.value.trim().length > 0) {
            snipRef.current = new MarkdownSnip({
                text: text.value,
                book_id: -1,
                lineWrap: 800,
            });
        }

        const context = canvas.getContext("2d")!;
        context.scale(5, 5);
        snipRef.current?.ready
            ?.then(async () => {
                await snipRef.current?.setText(text.value);
            })
            .then(() => {
                // Rendering twice is a workaround for the width and height not being set
                // before the first render
                context.setTransform(0.5, 0, 0, 0.5, 0, 0);
                snipRef.current?.render(context);

                // we scale the canvas to half the size to make it look better
                // in the editor ,will be twice the size in the actual page
                canvas.width = (snipRef.current!.width ?? 800) / 2;
                canvas.height = snipRef.current!.height / 2;

                context.setTransform(0.5, 0, 0, 0.5, 0, 0);
                snipRef.current?.render(context);
            });
    }, [text.value]);

    return (
        <div
            className={styles.preview}
            style={{
                display: previewVisible ? "block" : "none",
            }}
        >
            <canvas ref={canvasRef} className={styles.previewCanvas} />
        </div>
    );
}

/* --------------------------------- Resizer -------------------------------- */

function ResizeWrapper({ children }: { children: React.ReactNode }) {
    const refResizer = useRef<HTMLDivElement>(null);
    const refResizable = useRef<HTMLDivElement>(null);
    const [height, setHeight] = useState<number | null>(null);

    const [handlePointerDown] = useDrag({
        onDragMove: (_e, _offset, movement) => {
            if (height === null) return;
            setHeight((prev) => (prev ?? 0) + movement[1]);
        },
    });

    useEffect(() => {
        if (!refResizer.current) return;
        const el = refResizer.current;
        el.addEventListener("pointerdown", handlePointerDown!);

        return () => {
            el.removeEventListener("pointerdown", handlePointerDown!);
        };
    }, [handlePointerDown]);

    useEffect(() => {
        if (!refResizable.current) return;
        if (height === null) {
            setHeight(refResizable.current.parentElement!.clientHeight);
            return;
        }
        const minHeight =
            refResizable.current.parentElement!.parentElement!.style.minHeight.replace(
                "px",
                "",
            ) || "0";

        refResizable.current!.style.minHeight =
            Math.max(height, parseFloat(minHeight)) + "px";
    }, [height]);

    return (
        <div className={styles.resizeWrapper}>
            <div className={styles.wrap} ref={refResizable}>
                {children}
            </div>
            <div className={styles.resizeHandle} ref={refResizer}>
                <TbDots />
            </div>
        </div>
    );
}

/**
 * MarkdownEditor component serves as the main wrapper for the markdown editing UI.
 * It provides toolbar and input/preview areas within a context provider.
 */
const MarkdownEditor = forwardRef(function MarkdownEditor(
    {
        initial_text,
        ...props
    }: React.HTMLAttributes<HTMLDivElement> & { initial_text: string },
    ref: React.ForwardedRef<{ getText: () => string }>,
) {
    return (
        <MarkdownEditorProvider initial_text={initial_text}>
            <Wrapper {...props} ref={ref}>
                <ToolBar />
                <InputAndPreview />
            </Wrapper>
        </MarkdownEditorProvider>
    );
});

/**
 * Wrapper component is used to encapsulate children components
 * and provide an interface for accessing the current markdown text
 * through a forwarded ref.
 */
const Wrapper = forwardRef(function Wrapper(
    { children, ...props }: React.HTMLAttributes<HTMLDivElement>,
    ref: React.ForwardedRef<{ getText: () => string }>,
) {
    const { text } = useMarkdownEditorContext();

    // Allow the parent to access the current text
    useImperativeHandle(ref, () => {
        return {
            getText() {
                return text.value;
            },
        };
    }, [text.value]);

    return (
        <div className={styles.wrapper} {...props}>
            {children}
        </div>
    );
});

export default MarkdownEditor;
