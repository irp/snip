import { lazy, Suspense, useEffect, useRef, useState } from "react";
import { Button, Modal } from "react-bootstrap";

import type { BaseSnip } from "@snip/snips/general/base";
import type { MarkdownSnip } from "@snip/snips/general/markdown";

import { useQueuedSnipsContext } from "components/context/socket/useQueuedSnipsContext";
import Loading from "components/utils/Loading";

// Lazy load to decrease size of initial bundle
const MarkdownEditor = lazy(
    () => import("components/editor/markdown/textEditor"),
);

interface MarkdownModalProps {
    show: boolean;
    setShow: (show: boolean) => void;
    snip: MarkdownSnip;
    onApply?: (snip: BaseSnip) => void;
}

export function MarkdownModal({
    show,
    setShow,
    snip,
    onApply,
}: MarkdownModalProps) {
    const editorRef = useRef<{ getText: () => string }>(null);

    const { transformSnip, events } = useQueuedSnipsContext();
    const [initial_text, setInitialText] = useState<string>(snip.text);

    useEffect(() => {
        if (!snip) return;
        const abortController = new AbortController();
        events.addEventListener("updated", (e) => {
            console.log("Updated event", e);
            if (e.snip_id !== snip.id) return;
            setInitialText((snip as MarkdownSnip).text);
        });
        return () => {
            abortController.abort();
        };
    }, [events, snip]);

    function hide() {
        setShow(false);
    }

    return (
        <Modal
            show={show}
            centered
            size="xl"
            fullscreen="lg-down"
            backdrop="static"
            onHide={hide}
        >
            <Modal.Header closeButton>
                <Modal.Title>Edit Markdown Snip</Modal.Title>
            </Modal.Header>
            <Modal.Body
                style={{
                    minHeight: "400px",
                    height: "100%",
                    display: "flex",
                }}
            >
                {show && (
                    <Suspense fallback={<Loading />}>
                        <div
                            style={{
                                width: "100%",
                                height: "100%",
                                display: "flex",
                                flexDirection: "column",
                                gap: "1rem",
                            }}
                        >
                            <MarkdownEditor
                                initial_text={initial_text}
                                style={{
                                    width: "100%",
                                    height: "100%",
                                    minHeight: "400px",
                                }}
                                ref={editorRef}
                            />
                            <Button
                                variant="primary"
                                onClick={async (e) => {
                                    e.preventDefault();
                                    const text = editorRef.current?.getText();
                                    if (!text) {
                                        alert("No text. Couldn't not apply");
                                        return;
                                    }

                                    // This automatically updates the database
                                    await transformSnip(
                                        snip.id,
                                        (snip: BaseSnip) => {
                                            (snip as MarkdownSnip).setText(
                                                text,
                                            );
                                            return snip;
                                        },
                                        snip.id > 0, //Upsert if already in db
                                    )
                                        .then(() => {
                                            hide();
                                            onApply?.(snip);
                                        })
                                        .catch((e) => {
                                            alert(
                                                "Failed to update snip: " + e,
                                            );
                                        });
                                }}
                                style={{
                                    width: "100px",
                                    alignSelf: "flex-end",
                                    marginTop: "auto",
                                }}
                            >
                                Apply
                            </Button>
                        </div>
                    </Suspense>
                )}
            </Modal.Body>
        </Modal>
    );
}
