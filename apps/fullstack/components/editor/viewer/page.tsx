"use client";
import React, {
    CanvasHTMLAttributes,
    forwardRef,
    useEffect,
    useRef,
    useState,
} from "react";

import type { PageDataRet } from "@snip/database/types";

import { useEditorContext } from "../context";
import { RemotePointers } from "../remotePointers/component";
import { OverlayWorker } from "../worker/overlayWorker";
import { PageWorker } from "../worker/pageWorker";

import { useWorker } from "components/context/viewer/useWorkerContext";
import Loading from "components/utils/Loading";

import styles from "./viewer.module.scss";

type PageProps = {
    pageData: PageDataRet;
    idx?: number;
} & React.HTMLAttributes<HTMLDivElement>;

export function Page({ idx = 0, pageData, ...props }: PageProps) {
    const [transitioning, setTransitioning] = useState(false);
    /** We register each page to the editor if
     * it exists.
     */
    const { setEditorElementRef, editorElementRef, triggerScaleZoomUpdate } =
        useEditorContext();

    /** Each page needs
     * a worker to render it
     * and an overlay worker
     * to render the overlay
     *
     * To support multiple pages
     * we use an index to get the
     * correct worker(s)
     */
    const { worker, overlayWorker } = useWorker(idx);

    const firstMount = useRef(true);

    /**
     * Setup or change page
     * the setup function checks
     * if the page has changed and changes
     * the worker accordingly
     */
    useEffect(() => {
        if (!worker || !overlayWorker || !pageData) return;
        setTransitioning(true);
        Promise.all([
            worker.setupPage(pageData),
            overlayWorker.setupPage(pageData),
        ]).then(async () => {
            if (firstMount.current) {
                const rect = editorElementRef.current
                    .get(idx)!
                    .getBoundingClientRect();
                worker.viewport.resize(
                    rect.width * window.devicePixelRatio,
                    rect.height * window.devicePixelRatio,
                );
                worker.viewport.fitPage(true, true, [20, 20]);
                firstMount.current = false;
                triggerScaleZoomUpdate(idx, worker.viewport);
            }
            await worker.rerender();
            setTransitioning(false);
        });
    }, [
        worker,
        pageData,
        overlayWorker,
        idx,
        editorElementRef,
        triggerScaleZoomUpdate,
    ]);

    if (!worker) {
        return <></>;
    }

    return (
        <Canvases
            overlayWorker={overlayWorker}
            pageWorker={worker}
            ref={setEditorElementRef(idx)}
            className={styles.pageWrapper}
            {...props}
        >
            {transitioning && (
                <Loading
                    style={{
                        position: "absolute",
                        justifyContent: "center",
                        height: "100%",
                        backdropFilter: "blur(2px)",
                    }}
                />
            )}
            <RemotePointers overlayWorker={overlayWorker} />
        </Canvases>
    );
}

type WrapperProps = {
    overlayWorker: OverlayWorker;
    pageWorker: PageWorker;
} & CanvasHTMLAttributes<HTMLDivElement>;

/** Creates a canvas element bound to a worker
 * using offscreen canvas. This also
 * adds basic functionality like resizing
 * and visibility change
 *
 * @param worker - The workers reference
 * @param idx - The index of the worker/page
 */
export const Canvases = forwardRef<HTMLDivElement, WrapperProps>(
    function Canvas({ overlayWorker, children, pageWorker, ...props }, ref) {
        const mainCanvasRef = useRef<HTMLCanvasElement>(null);
        const overlayCanvasRef = useRef<HTMLCanvasElement>(null);

        /**
         * On mount we create a new offscreen canvas
         * and transfer it to the worker
         */
        useEffect(() => {
            if (
                !pageWorker ||
                !overlayWorker ||
                !mainCanvasRef.current ||
                !overlayCanvasRef.current
            )
                return;
            // Setup the canvas
            const mainCanvas = mainCanvasRef.current;
            const overlayCanvas = overlayCanvasRef.current;

            Promise.all([
                pageWorker.setupCanvas(mainCanvas),
                overlayWorker.setupCanvas(overlayCanvas),
            ]);

            //Attach visibility change
            function visibilityChange() {
                if (document.visibilityState === "visible") {
                    pageWorker.rerender();
                }
            }
            document.addEventListener("visibilitychange", visibilityChange);

            //Attach resize observer
            const observer = new ResizeObserver((entries) => {
                const { width, height } = entries[0]!.contentRect;
                pageWorker.resize(width, height);
            });
            observer.observe(mainCanvas);

            // Cleanup
            return () => {
                document.removeEventListener(
                    "visibilitychange",
                    visibilityChange,
                );
                observer.disconnect();
            };
        }, [overlayWorker, pageWorker]);

        return (
            <div ref={ref} {...props}>
                {children}
                <canvas
                    id="overlay"
                    ref={overlayCanvasRef}
                    style={{
                        width: "100%",
                        height: "100%",
                        position: "absolute",
                        pointerEvents: "none", // Are handled by the div!
                    }}
                />
                <canvas
                    id="main"
                    ref={mainCanvasRef}
                    style={{
                        zIndex: -1,
                        width: "100%",
                        height: "100%",
                        position: "absolute",
                        pointerEvents: "none", // Are handled by the div!
                    }}
                />
            </div>
        );
    },
);
