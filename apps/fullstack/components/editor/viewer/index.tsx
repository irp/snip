"use client";
import { Fragment } from "react";

import { useEditorContext } from "../context";
import { Page } from "./page";

import { usePagesContext } from "components/context/socket/usePagesContext";

import styles from "./viewer.module.scss";

export function Viewer() {
    const { activePageIds } = useEditorContext();
    const { pages } = usePagesContext();

    return (
        <div className={styles.viewer}>
            {activePageIds.map((id, i) => {
                const page = pages.get(id);
                return (
                    <Fragment key={i}>
                        {page ? <Page pageData={page} /> : null}
                    </Fragment>
                );
            })}
        </div>
    );
}
