import { TypedEventTarget } from "@snip/common/eventTarget";
import { ID, PageDataRet } from "@snip/database/types";
import { ViewPort } from "@snip/render/viewport";
import { BaseData, BaseView, SnipData } from "@snip/snips/general/base";

import { RenderWorker } from "./abc";
import {
    AllowedPointerEvent,
    EventMap,
    InteractionMessageEvent,
    InteractionResponseEvent,
    InteractionResponseMap,
    Message,
    Response,
    ResponseMap,
} from "./pageWorker/types";

export class PageWorker extends RenderWorker {
    worker: Worker;
    timeout: number;

    ready: Promise<ResponseMap<"init">>;

    /**
     * Creates a new instance of WorkerClass.
     * @param {number} timeout - Time (ms) after which the task will be rejected.
     */
    constructor(viewport: ViewPort, { timeout }: { timeout?: number } = {}) {
        super(viewport);
        this.timeout = timeout || 2000;
        this.worker = new Worker(
            new URL("./pageWorker/entrypoint.ts", import.meta.url),
        );

        this.worker.onerror = (evt) => {
            console.error(evt);
        };
        this.worker.onmessageerror = (evt) => {
            console.error(evt);
        };

        this.ready = this._sendMsg(
            {
                type: "init",
                data: {
                    render_timeout: this.timeout,
                },
            },
            [],
            5000,
        ).catch((e) => {
            console.debug(
                `Timeout might happen in dev mode because
                of multiple mounts`,
                e,
            );
            return undefined;
        });
    }

    public async terminate() {
        await this._sendMsg({
            type: "stop",
            data: undefined,
        });
        this.worker.terminate();
    }

    /* -------------------------------------------------------------------------- */
    /*                           send message to worker                           */
    /* -------------------------------------------------------------------------- */

    // Channel has no need for an id
    private _sendMsg<K extends keyof EventMap>(
        message: Message<K>,
        transfer?: Transferable[],
        timeout: number = 2000,
    ): Promise<ResponseMap<K>> {
        const worker = this.worker;
        if (!transfer) {
            transfer = [];
        }

        return new Promise((resolve, reject) => {
            const channel = new MessageChannel();
            channel.port1.onmessage = (evt) => {
                const data = evt.data as Response<K>;
                resolve(data.r);
                channel.port1.close();
            };

            // Send the canvas to the worker
            worker.postMessage(message, [channel.port2, ...transfer]);

            // Timeout reject
            if (timeout > 0) {
                setTimeout(() => {
                    reject(
                        new Error(
                            "Timeout of " +
                                (timeout || this.timeout) +
                                "ms exceeded!",
                        ),
                    );
                }, timeout || this.timeout);
            }
        });
    }
    public async sendMsg<K extends keyof EventMap>(
        message: Message<K>,
        transfer?: Transferable[],
        timeout?: number,
    ): Promise<ResponseMap<K>> {
        await this.ready;
        return await this._sendMsg(message, transfer, timeout);
    }

    /* -------------------------------------------------------------------------- */
    /*                      helpers for some common messages                      */
    /* -------------------------------------------------------------------------- */
    public async setupCanvas(canvas: HTMLCanvasElement) {
        if (canvas == this.canvas) {
            return;
        }
        const offscreen = canvas.transferControlToOffscreen();

        return await this.sendMsg(
            {
                type: "setupRender",
                data: {
                    canvas: offscreen,
                    viewPortBuffer: this.viewport.buffer.buffer,
                },
            },
            [offscreen],
        );
    }

    public async setupPage(page: PageDataRet) {
        await this.ready;
        this.viewport.resizePage(1400, 2000);
        return await this.sendMsg(
            {
                type: "setupPage",
                data: {
                    page,
                },
            },
            [],
            5000,
        );
    }

    public async resize(
        width: number,
        height: number,
        devicePixelRatio?: number,
    ) {
        await this.ready;

        const pixelRatio = devicePixelRatio || window.devicePixelRatio;
        this.viewport.resize(width * pixelRatio, height * pixelRatio);
        return;
    }

    public async insertSnip(snip: SnipData<BaseData, BaseView>) {
        await this.ready;
        return await this.sendMsg({
            type: "insertSnip",
            data: {
                snip,
            },
        });
    }

    public async placeOnPage(snip: SnipData<BaseData, BaseView>) {
        await this.ready;
        return await this.sendMsg({
            type: "placeOnPage",
            data: {
                snip,
            },
        });
    }

    public async rerender() {
        await this.ready;
        return await this.sendMsg({
            type: "render",
            data: undefined,
        });
    }

    /* -------------------------------------------------------------------------- */
    /*                              eraser specifics                              */
    /* -------------------------------------------------------------------------- */

    public async enableEraser() {
        await this.ready;
        return await this.sendMsg({
            type: "enableEraser",
            data: undefined,
        });
    }
    public async disableEraser() {
        await this.ready;
        return await this.sendMsg({
            type: "disableEraser",
            data: undefined,
        });
    }

    public async eraseSnip(id: ID) {
        await this.ready;
        return await this.sendMsg({
            type: "eraseSnip",
            data: {
                id,
            },
        });
    }

    /* -------------------------------------------------------------------------- */
    /*                                 interactions                               */
    /* -------------------------------------------------------------------------- */
    // you can use this to listen to events
    // e.g. worker.interactions.on("pointerdown", (e) => console.log(e));
    public readonly interactions = new TypedEventTarget<InteractionEventMap>();
    protected interactionPort?: MessagePort;
    public async setupInteraction(force = false) {
        if (!force && this.interactionPort != undefined) {
            return;
        }

        const messageChannel = new MessageChannel();
        this.interactionPort = messageChannel.port1;

        // Setup event emitter
        this.interactionPort.onmessage = (evt) => {
            const data = evt.data as InteractionResponseEvent<
                keyof InteractionResponseMap
            >;
            const type = data.type;
            const event = new InteractionEvent({
                type,
                ...data.r,
            });
            this.interactions.emit(type, event);
        };

        // Bind to worker
        await this.sendMsg(
            {
                type: "setupInteraction",
                data: undefined,
            },
            [messageChannel.port2],
        );
    }

    public teardownInteraction() {
        this.interactionPort?.close();
        this.interactionPort = undefined;
    }

    /** Attach element to interaction events
     * i.e. bind events of the element to the worker
     *
     * @param {HTMLElement} element - The element to attach the events to.
     * @returns {Function} - A function to remove the event listeners.
     */
    public attachElementEvents(element: HTMLElement): () => void {
        if (!this.interactionPort) return () => {};

        const events: AllowedPointerEvent[] = [
            "pointerenter",
            "pointerleave",
            "pointermove",
            "pointerdown",
            "pointerup",
            "pointerout",
            "dblclick",
        ];

        const functions = events.map((event) => {
            return (e: MouseEvent) => {
                const { offsetX, offsetY } = e;
                const msg: InteractionMessageEvent = {
                    type: event,
                    msg: {
                        mousePos: [offsetX, offsetY],
                        pixelRatio: window.devicePixelRatio || 1,
                    },
                };
                this.interactionPort?.postMessage(msg);
            };
        });

        for (const [i, event] of events.entries()) {
            element.addEventListener(event, functions[i]!, true);
        }

        return () => {
            for (const [i, event] of events.entries()) {
                element.removeEventListener(event, functions[i]!, true);
            }
        };
    }
}

export type InteractionEventMap<
    K extends keyof InteractionResponseMap = keyof InteractionResponseMap,
> = {
    [key in K]: InteractionEvent<K>;
};

export class InteractionEvent<
    K extends keyof InteractionResponseMap = keyof InteractionResponseMap,
> extends Event {
    // Target of the event
    snipData: InteractionResponseMap[K]["snipData"];

    data: Omit<InteractionResponseMap[K], "snipData">;
    constructor(response: InteractionResponseMap[K] & { type: K }) {
        const { type, snipData, ...data } = response;
        super(type);
        this.snipData = snipData;
        this.data = data as Omit<InteractionResponseMap[K], "snipData">;
    }
}
