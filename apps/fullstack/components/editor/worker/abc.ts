import { PageDataRet } from "@snip/database/types";
import { ViewPort } from "@snip/render/viewport";

export abstract class RenderWorker {
    public canvas?: HTMLCanvasElement;

    public readonly viewport: ViewPort;
    constructor(viewport: ViewPort) {
        this.viewport = viewport;
    }

    /** Setup the worker
     * by attaching the canvas
     */
    abstract setupCanvas(canvas: HTMLCanvasElement): Promise<void>;

    /** Setup a page for render */
    abstract setupPage(page: PageDataRet): void;

    /** Render the page */
    abstract rerender(): Promise<void> | void;

    /** Terminate the worker */
    abstract terminate(): void;

    /* -------------------------------------------------------------------------- */
    /*                                interactions                                */
    /* -------------------------------------------------------------------------- */

    /** Attach element to listener
     * i.e. bind events of the element to the worker
     *
     * @param {HTMLElement} element - The element to attach the events to.
     * @returns {Function} - A function to remove the event listeners.
     */
    abstract attachElementEvents(canvas: HTMLCanvasElement): () => void;

    // Can be overwritten but does nothing by default
    setupInteraction(): void {}
}
