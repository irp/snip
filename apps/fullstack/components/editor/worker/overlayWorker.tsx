/**
 * README:
 * -------
 *
 * The overlay "worker" is used in the main thread to render
 * the overlay canvas. This is done to avoid the overhead of
 * creating a webworker for the overlay.
 *
 * Thus it is not a real worker but a facade to the overlay
 * rendering functions.
 */
import { Socket } from "socket.io-client";

import type { PageDataRet } from "@snip/database/types";
import { Pipeline } from "@snip/render/pipeline";
import { RenderContext } from "@snip/render/types";
import { ViewPort } from "@snip/render/viewport";
import { get_snip_from_data_allow_unknown } from "@snip/snips";
import { BaseData, BaseView, SnipData } from "@snip/snips/general/base";
import { ClientToServerEvents, ServerToClientEvents } from "@snip/socket/types";

import { RemotePointerHandler } from "../remotePointers/remotePointerHandler";
import { RenderWorker } from "./abc";

const debug = (...args: unknown[]) => {
    console.debug("[OverlayWorker]", ...args);
};

export class OverlayWorker extends RenderWorker {
    // Render pipeline
    private pipeline?: Pipeline;
    private socket?: Socket<ServerToClientEvents, ClientToServerEvents>;
    public readonly remotePointerHandler?: RemotePointerHandler;

    constructor(viewport: ViewPort, socket?: Socket) {
        super(viewport);
        this.socket = socket;

        if (!socket) {
            return;
        }

        this.remotePointerHandler = new RemotePointerHandler(this, socket);
    }

    public async setupCanvas(canvas: HTMLCanvasElement) {
        debug("canvas setup");
        if (this.pipeline) {
            this.pipeline.stop();
        }
        this.canvas = canvas;
        this.pipeline = new Pipeline(
            this.canvas,
            this.viewport,
            "OverlayWorkerPipeline",
        );
        this.pipeline.start();
    }

    current_page_id?: number;
    public async setupPage(page: PageDataRet) {
        if (!this.pipeline) {
            throw new Error("Canvas has to be initialized before page setup!");
        }
        // Check if page is already setup
        if (this.current_page_id && this.current_page_id === page.id) {
            debug("[setupPage]", "Page already set");
            return;
        }
        //Leave page
        if (this.current_page_id) {
            // Leave room
            debug("[setupPage]", "Leaving old page");
            this.socket?.emit("page:leave", this.current_page_id, (cb) => {
                if (!cb.ok) {
                    console.error("Could not leave page", cb.error);
                }
            });
        }
        this.current_page_id = page.id;
        this.remotePointerHandler?.pageChanged(page.id);

        // Clear previous snip preview renders
        this.preview2render.clear();

        // Join room
        return new Promise<void>((resolve, reject) => {
            if (!this.socket) return resolve();
            this.socket.emit("page:join", page.id, (cb) => {
                if (cb.ok) {
                    resolve();
                } else {
                    reject(cb.error);
                }
            });
        });
    }

    public sendPreviewData(snipData: SnipData<BaseData, BaseView>) {
        this.socket?.volatile.emit("snip:preview", snipData);
    }

    private preview2render = new Map<number, (ctx: RenderContext) => void>();
    public receivePreviewData(snipData: SnipData<BaseData, BaseView>) {
        if (
            !this.current_page_id ||
            snipData.page_id !== this.current_page_id ||
            !snipData.id
        )
            return;

        // Check if snip is already rendered and remove it
        if (this.preview2render.has(snipData.id!)) {
            const renderfn = this.preview2render.get(snipData.id);
            this.remove(renderfn!);
        }

        // Add to stack
        const snip = get_snip_from_data_allow_unknown(snipData);
        this.preview2render.set(snipData.id, (ctx) => {
            snip.render(ctx);
        });
        this.add(this.preview2render.get(snipData.id)!);
    }

    public rerender() {
        this.pipeline?.rerenderStack();
    }

    public add(fn: (ctx: RenderContext) => void) {
        // Only allow function to be added once
        if (!this.pipeline) return;

        return this.pipeline.render(fn);
    }
    public remove = (fn: (ctx: RenderContext) => void) => {
        // Get all indexes of the function in the stack
        this.pipeline?.removeRender(fn).catch(console.warn);
    };

    public terminate() {
        this.pipeline?.stop();
    }

    /**
     * @deprecated Use viewport.getPosInPageCoords directly.
     */
    public canvasCoordsInPageCoords(xy: [number, number]) {
        return this.viewport.getPosInPageCoords(xy);
    }

    /**
     * @deprecated Use viewport.getPosInViewCoords directly.
     */
    public pageCoordsInCanvasCoords(xy: [number, number]) {
        return this.viewport.getPosInViewCoords(xy);
    }

    /**
     * @deprecated Use viewport.getPosInPageCoords directly.
     */
    public canvasCoords_2_dbCoords_het(xy: [number, number]) {
        return this.viewport.getPosInPageCoords(xy);
    }

    attachElementEvents(): () => void {
        //Does nothing for the overlay worker
        return () => {};
    }
}
