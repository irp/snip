import { PageWorker } from "./pageworker";
import { EventMap, Message, MessageMap } from "./types";

declare global {
    // eslint-disable-next-line no-var
    var worker: PageWorker;
}
globalThis.worker = PageWorker.instance;

/** The post message event handler
 * is relatively simple.
 * We get a channel port from the event
 * and we send a response back to the
 * main thread.
 *
 * The different events are handled in
 * the handleMessageEvent function.
 */
globalThis.onmessage = async (evt: MessageEvent<Message>) => {
    // Get the channel port
    const port = evt.ports[0];

    // Get the response from the message event
    const r = await handleMessageEvent(evt.data, evt.ports.at(1));

    // Send the response back to the main thread
    if (port) {
        port.postMessage({ r });
        port.close();
    }
};

/** See types for
 * the different events that can be sent
 * and received to the worker.
 *
 * See the pageworker.ts for the implementation
 */
async function handleMessageEvent<K extends keyof EventMap>(
    msg: Message<K>,
    secondaryPort?: MessagePort,
): Promise<EventMap[K]["response"]> {
    switch (msg.type) {
        case "render":
            return await globalThis.worker.rerenderPage(
                msg.data as MessageMap<"render">,
            );
        case "setupInteraction":
            // Special case for the interaction events
            // they open a channel and keep it open
            // until the interaction is done.
            return await globalThis.worker.setupInteraction(
                msg.data as MessageMap<"setupInteraction">,
                secondaryPort!,
            );
        case "teardownInteraction":
            return await globalThis.worker.teardownInteraction();
        case "insertSnip":
            return await globalThis.worker.insertSnip(
                msg.data as MessageMap<"insertSnip">,
            );
        case "placeOnPage":
            return await globalThis.worker.placeOnPage(
                msg.data as MessageMap<"placeOnPage">,
            );
        case "setupRender":
            return await globalThis.worker.setupRender(
                msg.data as MessageMap<"setupRender">,
            );
        case "setupPage":
            return await globalThis.worker.setupPage(
                msg.data as MessageMap<"setupPage">,
            );
        case "init":
            return await globalThis.worker.init(msg.data as MessageMap<"init">);
        case "stop":
            return await globalThis.worker.shutdown();
        case "enableEraser":
            return await globalThis.worker.enableEraser();
        case "disableEraser":
            return await globalThis.worker.disableEraser();
        case "eraseSnip":
            return await globalThis.worker.eraseSnip(
                msg.data as MessageMap<"eraseSnip">,
            );
        default:
            console.warn("Unknown message type", msg);
    }
    return assertUnreachable(msg.type);
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function assertUnreachable(x: never): never {
    throw new Error("Didn't expect to get here");
}
