import EventEmitter from "events";
import { Socket } from "socket.io-client";

import { render_background_by_id } from "@snip/render/backgrounds";
import { LocalPage } from "@snip/render/page";
import { ConditionalRender } from "@snip/render/types";
import { get_snip_from_data_allow_unknown } from "@snip/snips";
import {
    BaseData,
    BaseSnip,
    BaseView,
    SnipData,
} from "@snip/snips/general/base";
import { ClientToServerEvents, ServerToClientEvents } from "@snip/socket/types";

import { getSocket } from "components/context/socket";

interface PageEvents {
    inserted: [newSnip: BaseSnip];
    updated: [updatedSnip: BaseSnip];
    removed: [snipId: number];
}

export class Page extends LocalPage {
    public socket?: Socket<ServerToClientEvents, ClientToServerEvents>;

    /** Event emitter for all changes to the page.
     *
     * The idea is to e.g. subscribe to the change event and
     * rerender the page.
     */
    public readonly events = new EventEmitter<PageEvents>();

    async init(): Promise<void> {
        this.socket = await getSocket(this.data.book_id);
        if (!this.socket) {
            throw new Error("Socket not initialized");
        }
        if (!this.socket.connected) {
            await this.socket.connect();
        }
        await this.joinRoom();
        await this.subscribeToChanges();
    }

    /** Join room for the page */
    private async joinRoom(): Promise<void> {
        return await new Promise<void>((resolve, reject) => {
            if (!this.socket) return reject();
            this.socket.emit("page:join", this.data.id, (response) => {
                if (response.ok === false) {
                    return reject(new Error(response.error));
                }
                resolve();
            });
        });
    }

    /** Use socket to load all snips */
    public async loadAllSnips(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            if (!this.socket) return reject();
            this.socket.emit("page:getSnips", this.data.id, (response) => {
                if (response.ok === false) {
                    return reject(new Error(response.error));
                }
                this.set_snips(
                    response.data.map(get_snip_from_data_allow_unknown),
                );
                resolve();
            });
        });
    }

    /** Images need
     * to be loaded before they can be rendered.
     */
    public async allSnipsPrepared(): Promise<boolean> {
        const snips_with_ready = this.snips.filter((snip) => {
            return Object.hasOwn(snip, "ready");
        });
        return Promise.all(snips_with_ready.map((snip) => snip.ready)).then(
            () => true,
        );
    }

    /** Subscribe to changes */
    private subscribeToChanges(): void {
        if (!this.socket) {
            throw new Error("Socket not initialized");
        }
        this.socket.on(
            "snip:inserted",
            (newSnip: SnipData<BaseData, BaseView>) => {
                // Check if snip is on this page
                if (this.snipRelevant(newSnip)) {
                    const s = get_snip_from_data_allow_unknown(newSnip);
                    this.add_snip(s);
                    this.events.emit("inserted", s);
                }
            },
        );

        this.socket.on(
            "snip:updated",
            (updatedSnip: SnipData<BaseData, BaseView>) => {
                // Check if snip is on this page
                if (this.snipRelevant(updatedSnip)) {
                    const snip = get_snip_from_data_allow_unknown(updatedSnip);
                    const index = this.snips.findIndex((s) => s.id === snip.id);
                    if (index !== -1) {
                        this.snips[index] = snip;
                        this.events.emit("updated", snip);
                    } else {
                        this.add_snip(snip);
                        this.events.emit("inserted", snip);
                    }
                }
            },
        );

        this.socket.on("snip:removed", (snipId: number) => {
            // Check if snip is on this page
            const success = this.remove_snip(snipId);
            if (success) {
                this.events.emit("removed", snipId);
            }
        });

        this.socket.on("disconnect", async () => {
            const interval = setInterval(async () => {
                if (this.socket?.connected) {
                    clearInterval(interval);
                    return;
                }
                try {
                    await this.socket?.connect();
                    console.info("Socket reconnected successfully");
                } catch (error) {
                    console.error("Failed to reconnect socket:", error);
                    console.info("Retrying in 2 seconds");
                }
            }, 2000);
        });
    }

    /** Render a page
     *
     * @param ctx Context to render onto
     */
    public render(
        ctx: CanvasRenderingContext2D | OffscreenCanvasRenderingContext2D,
        conditionalRender?: ConditionalRender,
    ): void {
        // Render background
        render_background_by_id(this.data.background_type_id, ctx, this.size);

        const sorted_snips = this.snips.sort(
            (a, b) => a.last_updated.getTime() - b.last_updated.getTime(),
        );

        for (const snip of sorted_snips) {
            // Eraser mode decreases opacity of
            // snips older than x seconds
            if (conditionalRender && conditionalRender.condition(snip)) {
                conditionalRender.render(ctx, snip);
            } else {
                snip.render(ctx);
            }
        }
    }

    get id() {
        return this.data.id;
    }

    private snipRelevant(snip: { page_id?: number | null }): boolean {
        // check if a snip is relevant for this page
        return (
            this.data.id === snip.page_id ||
            this.data.referenced_page_id === snip.page_id
        );
    }
}
