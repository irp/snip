import { FONT_URLS } from "@snip/database/fonts/internal";
import { Pipeline as BasePipeline } from "@snip/render/pipeline";
import {
    ConditionalRender,
    MinimalPage,
    RenderContext,
} from "@snip/render/types";
import { ViewPortWorker } from "@snip/render/viewport";
import { get_snip_from_data_allow_unknown } from "@snip/snips";
import {
    BaseData,
    BaseSnip,
    BaseView,
    SnipData,
} from "@snip/snips/general/base";
import { ImageSnip } from "@snip/snips/general/image";

import { Page } from "./page";
import {
    InteractionMessageEvent,
    InteractionResponseEvent,
    InteractionResponseMap,
    MessageMap,
    ResponseMap,
} from "./types";

const debug = (...args: unknown[]) => {
    console.debug("[PageWorker]", ...args);
};

/** Basically a singleton which manages all worker logic
 */
export class PageWorker {
    static #instance: PageWorker;

    private constructor() {}

    public static get instance(): PageWorker {
        if (!PageWorker.#instance) {
            PageWorker.#instance = new PageWorker();
        }
        return PageWorker.#instance;
    }

    // Max time for a full page render (ms)
    render_timeout: number = 3000;

    /** Register fonts for the canvas/page
     * At the moment this just registers every font but
     * we might want to make this more dynamic in the future.
     */
    public async init(data: MessageMap<"init">): Promise<ResponseMap<"init">> {
        debug("init");
        this.render_timeout = data.render_timeout;
        const promises: Promise<void>[] = [];
        for (const [url, name] of FONT_URLS) {
            promises.push(
                fetch("/fonts/" + url)
                    .then(async (response) => {
                        return response.arrayBuffer();
                    })
                    .then((data) => {
                        return new FontFace(name, data);
                    })
                    .then((font) => {
                        globalThis.fonts.add(font);
                    }),
            );
        }
        return await Promise.all(promises).then(() => {
            debug("[init]", "Fonts loaded");
            return;
        });
    }

    pipeline?: Pipeline;
    /**
     * Initialize the rendering process with the specified canvas data.
     * If a renderer instance already exists, it will be stopped before creating a new one.
     *
     * @param data - Contains the canvas element to be used by the renderer.
     * @param viewportIPCport - An optional MessagePort for inter-process communication for viewport updates.
     *
     * @throws Error if the rendering process fails to initialize the new Renderer.
     */
    public async setupRender(data: MessageMap<"setupRender">) {
        debug("render setup");
        if (this.pipeline) {
            this.pipeline.stop();
        }
        const viewport = new ViewPortWorker(data.viewPortBuffer);
        this.pipeline = new Pipeline(
            data.canvas,
            viewport,
            "PageWorkerPipeline",
        );
        this.pipeline.start();
    }

    interactionHandler?: InteractionHandler;
    /**
     * Initialize the interaction handler for handling interaction messages.
     * Typically, interaction events include
     * mouse events such as pointer movements, clicks, etc., on the rendered page.
     *
     * @param msg - The setup interaction message. This typically contains configuration
     *              or state information required for initializing the interaction handler.
     * @param port - The MessagePort used for sending and receiving interaction messages
     *               between different contexts (e.g., between the worker and the main thread).
     *
     * @returns A promise that resolves when the interaction handler is successfully initialized.
     */
    public async setupInteraction(
        _msg: MessageMap<"setupInteraction">,
        port: MessagePort,
    ): Promise<ResponseMap<"setupInteraction">> {
        debug("interaction setup");
        this.interactionHandler = new InteractionHandler(this, port);
        return;
    }

    public async teardownInteraction(): Promise<
        ResponseMap<"teardownInteraction">
    > {
        debug("interaction teardown");
        this.interactionHandler?.port.close();
        this.interactionHandler = undefined;
        return;
    }

    page?: Page;
    /**
     * Sets up the page for rendering and interaction within the worker.
     *
     * This ensures that all snips are loaded and prepared before rendering.
     * It also handles switching from an old page if one is already set up, and subscribes
     * to events such as insert, remove, and placed to update the rendering dynamically.
     *
     * @param data - The setup page message, which includes the page data to be initialized.
     *
     * @returns A promise that resolves when the page is successfully set up.
     *
     * @throws Error if the canvas has not been initialized or if the page fails to initialize.
     */
    public async setupPage(
        data: MessageMap<"setupPage">,
    ): Promise<ResponseMap<"setupPage">> {
        if (!this.pipeline) {
            throw new Error("Canvas has to be initialized before page setup!");
        }

        const pageData = data.page;
        // Check if page is already set
        debug("[setupPage]", "Setting page", pageData.id);
        if (this.page && this.page.id === pageData.id) {
            debug("[setupPage]", "Page already set");
            return;
        }
        // Leave old page
        if (this.page) {
            debug("[setupPage]", "Leaving old page");
            this.page.socket?.emit("page:leave", this.page.id, (response) => {
                if (response.ok === false) {
                    console.error(response.error);
                }
            });
        }
        this.page = new Page(pageData);
        debug("[setupPage]", "Page created");
        await this.page.init().catch((e) => {
            console.error(e);
            throw new Error("Failed to initialize page");
        });
        debug("[setupPage]", "Page initialized");
        await this.page.loadAllSnips();

        debug("[setupPage]", "Snips loaded");
        await this.page.allSnipsPrepared();
        debug("[setupPage]", "Images prepared");

        await this.pipeline.renderPage(this.page);
        debug("[setupPage]", "Render completed");

        // Subscribe to changes
        const l = ["inserted", "removed", "updated"] as const;
        l.forEach((event) => {
            this.page?.events.addListener(event, () => {
                debug(`Got event ${event} for page ${pageData.id}`);
                if (!this.page) return;
                this.pipeline!.renderPage(this.page);
            });
        });

        return;
    }

    /** Insert a snip on the page
     */
    public async insertSnip(
        data: MessageMap<"insertSnip">,
    ): Promise<ResponseMap<"insertSnip">> {
        if (!this.pipeline) {
            throw new Error("Canvas needs to be initialized to insert snips!");
        }
        if (!this.page) {
            throw new Error("Page needs to be initialized to insert snips!");
        }
        debug("[insertSnip]", "Inserting snip", data.snip.id);
        const snip = await loadSnip(data.snip);
        await this.pipeline.renderSnipOnce(snip, this.render_timeout);

        // Insert db
        snip.socket = this.page.socket;
        snip.id = await snip.upsert();
        this.page.add_snip(snip);
    }

    public async placeOnPage(
        data: MessageMap<"placeOnPage">,
    ): Promise<ResponseMap<"placeOnPage">> {
        if (!this.pipeline) {
            throw new Error("Canvas needs to be initialized to place snips!");
        }
        if (!this.page) {
            throw new Error("Page needs to be initialized to place snips!");
        }
        debug("[placeOnPage]", "Placing snip", data.snip.id);
        data.snip.book_id = this.page.data.book_id;
        const snip = await loadSnip(data.snip);
        debug("[placeOnPage]", "Got snip", snip.type);
        snip.socket = this.page.socket;
        await Promise.all([
            this.pipeline.renderSnipOnce(snip, this.render_timeout),
            snip.place_on_page(this.page.id),
        ]).catch((e) => {
            console.error(e);
        });
        debug(snip);
        this.page.add_snip(snip);
    }

    public async rerenderPage(
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        data: MessageMap<"render">,
    ): Promise<ResponseMap<"render">> {
        if (!this.page || !this.pipeline) return;
        return await this.pipeline.renderPage(this.page, this.render_timeout);
    }

    private teardownPage(close_socket = false) {
        if (!this.page) return;
        this.page.socket?.emit("page:leave", this.page.id, (response) => {
            if (response.ok === false) {
                console.error(response.error);
            }
        });
        this.page.events.removeAllListeners();
        // We keep the socket open here! If you want to close
        // the socket too consider shutdown() function
        if (close_socket) {
            this.page.socket?.close();
        }
        this.page = undefined;
    }

    /** Set conditional render e.g. for the eraser tool
     */
    public async enableEraser() {
        if (!this.pipeline) {
            debug("Rendered not defined!");
            return;
        }

        // A bit of dynamic import voodo
        const Condition = await import(
            "components/editor/tools/eraser/condition"
        ).then((r) => r.default);
        const con = new Condition();
        this.pipeline.condition = con;

        // The condition is also the (inverse) filter for the
        // interaction event
        // i.e. interaction event should only trigger
        // on snips which can be erased
        if (this.interactionHandler)
            this.interactionHandler.snip_filter = (s) => !con.condition(s);

        if (this.page) this.pipeline.renderPage(this.page);
    }

    public async disableEraser() {
        if (!this.pipeline) {
            debug("Rendered not defined!");
            return;
        }
        this.pipeline.condition = undefined;
        if (this.interactionHandler)
            this.interactionHandler.snip_filter = undefined;
        if (this.page) this.pipeline.renderPage(this.page);
    }

    public async eraseSnip(
        data: MessageMap<"eraseSnip">,
    ): Promise<ResponseMap<"eraseSnip">> {
        if (!this.page) {
            throw Error("Page has to be set to use eraser tool!");
        }
        if (!this.pipeline) {
            throw Error(
                "Render pipeline has to be initialized to use eraser tool!",
            );
        }
        // Try to erase
        debug("ERASER");
        const snip = this.page.get_snip_by_id(data.id);

        // Check if user is allowed to delete it
        // (is also checked on the server side)
        if (
            !snip ||
            !this.pipeline.condition ||
            this.pipeline.condition?.condition(snip)
        ) {
            //Rerender
            await this.pipeline.renderPage(this.page);
            return {
                success: false,
                details: "Not allowed to delete this snip! Already too old",
            };
        }

        // Snip potentially can be deleted, try to propagate to server
        snip.socket = this.page.socket;
        try {
            debug(snip);
            await snip.remove();
            this.page.remove_snip(snip.id);
        } catch {
            return {
                success: false,
                details: "Not allowed to delete this snip!",
            };
        }

        // Rerender
        await this.pipeline?.renderPage(this.page);

        return {
            success: true,
        };
    }

    /** Shutdown the worker */
    public async shutdown() {
        this.teardownPage(true);
        delete this.page;
        this.teardownInteraction();
        this.pipeline?.stop();
    }
}

class InteractionHandler {
    worker: PageWorker;
    port: MessagePort;

    constructor(worker: PageWorker, port: MessagePort) {
        debug("Interaction handler created");
        this.worker = worker;
        this.port = port;
        this.port.onmessage = this.onEvent.bind(this);
    }

    /** This is triggered whenever an event is send
     * to the worker, this is a mousevent.
     */
    private async onEvent(evt: MessageEvent<InteractionMessageEvent>) {
        const { type, msg } = evt.data;

        let res_type: keyof InteractionResponseMap | null = null;
        let snip: BaseSnip | null = null;

        // Base move events parsing to snip events
        switch (type) {
            case "pointerenter":
                [res_type, snip] = this.onPointerEnter(msg);
                break;
            case "pointermove":
                [res_type, snip] = this.onPointerMove(msg);
                break;
            case "pointerleave":
                [res_type, snip] = this.onPointerLeave();
                break;
            case "dblclick":
                [res_type, snip] = this.onDblClick(msg);
                break;
            case "pointerdown":
                [res_type, snip] = this.onPointerDown(msg);
                break;
            case "pointerup":
            case "pointerout":
                console.warn("Not implemented", type);
                break;
            default:
                return assertUnreachable(type);
        }
        this.snip_under_cursor = snip;

        //early return if no event
        if (res_type == null || !snip) {
            return;
        }

        //return minified snipdata
        this._sendResponse(res_type, {
            snipData: snip.to_data(true),
        });

        // Trigger render event
        this.worker.pipeline?.renderInteractionEvent(
            res_type,
            snip,
            this.worker.page!,
        );

        if (res_type == "pointerleave") {
            this.snip_under_cursor = null;
        }
    }

    // At the moment we only have one snip under the cursor
    // we might want to change this in the future.
    // if we allow multitouch
    snip_under_cursor: BaseSnip | null = null;

    onPointerEnter(
        msg: InteractionMessageEvent["msg"],
    ): [keyof InteractionResponseMap, BaseSnip] | [null, null] {
        //Check if a snip is under the cursor on enter
        const coords = this.getCoords(msg);
        const snip = this.getSniptAt(coords);
        if (snip && snip.id !== this.snip_under_cursor?.id) {
            return ["pointerenter", snip];
        }
        return [null, null];
    }

    onPointerMove(
        msg: InteractionMessageEvent["msg"],
    ): [keyof InteractionResponseMap | null, BaseSnip | null] {
        const coords = this.getCoords(msg);
        const snip = this.getSniptAt(coords);
        if (snip?.id !== this.snip_under_cursor?.id) {
            if (this.snip_under_cursor) {
                return ["pointerleave", this.snip_under_cursor];
            }
            if (snip) {
                return ["pointerenter", snip];
            }
        }

        // Special case return active snip so it is not overwritten
        return [null, this.snip_under_cursor];
    }

    onPointerLeave(): [keyof InteractionResponseMap, BaseSnip] | [null, null] {
        if (this.snip_under_cursor) {
            return ["pointerleave", this.snip_under_cursor];
        }

        return [null, null];
    }

    onDblClick(
        msg: InteractionMessageEvent["msg"],
    ): [keyof InteractionResponseMap, BaseSnip] | [null, null] {
        const coords = this.getCoords(msg);
        const snip = this.getSniptAt(coords);
        if (snip) {
            return ["dblclick", snip];
        }
        return [null, null];
    }

    onPointerDown(
        msg: InteractionMessageEvent["msg"],
    ): [keyof InteractionResponseMap, BaseSnip] | [null, null] {
        const coords = this.getCoords(msg);
        const snip = this.getSniptAt(coords);
        if (snip) {
            return ["pointerdown", snip];
        }
        return [null, null];
    }

    private getCoords(msg: InteractionMessageEvent["msg"]): [number, number] {
        if (!this.worker.pipeline) {
            throw new Error("Canvas has to be initialized to getCoords");
        }
        const pos = this.worker.pipeline.viewport.getPosInPageCoords([
            msg.mousePos[0] * msg.pixelRatio,
            msg.mousePos[1] * msg.pixelRatio,
        ]);
        return pos;
    }

    public snip_filter?: (snip: BaseSnip) => boolean;
    private getSniptAt(coords: [number, number]): BaseSnip | null {
        if (!this.worker.page) {
            return null;
        }
        return this.worker.page.get_snip_at(coords, this.snip_filter) || null;
    }

    private _sendResponse<K extends keyof InteractionResponseMap>(
        type: K,
        data: InteractionResponseMap[K],
    ) {
        const response: InteractionResponseEvent<K> = {
            type,
            r: data,
        };
        this.port.postMessage(response);
    }
}

function assertUnreachable(x: never): never {
    throw new Error("Didn't expect to get here " + x);
}

async function loadSnip(snipData: SnipData<BaseData, BaseView>) {
    const snip = get_snip_from_data_allow_unknown(snipData);
    if (Object.hasOwn(snip, "ready")) {
        await (snip as ImageSnip).ready;
    }
    return snip;
}

/** Render with interaction events.
 */
class Pipeline extends BasePipeline {
    /**
     * Render interaction event with optional conditional rendering based on the event type.
     *
     * This method handles specific interaction events, such as pointer enter and pointer leave,
     * and triggers the appropriate rendering logic. If a "pointerleave" event is detected and there are
     * handlers for "pointerenter" without corresponding "pointerleave" cleanup handlers, a full page
     * rerender is initiated to ensure proper visual updates. Otherwise, a more specific rendering
     * function is constructed and executed to update only the necessary parts of the canvas.
     *
     * @template K - A key of the InteractionResponseMap representing the type of interaction event.
     *
     * @param type - The type of interaction event (e.g., "pointerenter", "pointerleave").
     * @param snip - The snip object associated with the interaction event.
     * @param page - The minimal representation of the page that is currently being rendered.
     *
     * @returns A promise that resolves when the interaction event rendering is complete.
     */
    public async renderInteractionEvent<K extends keyof InteractionResponseMap>(
        type: K,
        snip: BaseSnip,
        page: MinimalPage,
    ) {
        // Trigger page rerender if onleave is not defined as
        // a cleanup for on enter
        if (
            type === "pointerleave" &&
            (snip.on_pointerenter_worker ||
                this.condition?.on_pointerenter_worker) &&
            (!snip.on_pointerleave_worker ||
                !this.condition?.on_pointerleave_worker)
        ) {
            await this.renderPage(page);
        }

        const render_fkt = getRenderFunction(type, snip, this.condition);
        if (!render_fkt) return;
        await this.renderOnce(render_fkt);
    }
}

// Helper function to curry the render function
function getRenderFunction<K extends keyof InteractionResponseMap>(
    type: K,
    snip: BaseSnip,
    condition?: ConditionalRender,
): ((ctx: RenderContext) => void) | undefined {
    const conditionRenderFunction =
        condition?.[`on_${type}_worker`]?.bind(condition);
    const snipRenderFunction = snip[`on_${type}_worker`]?.bind(snip);

    return conditionRenderFunction
        ? (ctx: RenderContext) => conditionRenderFunction(ctx, snip)
        : snipRenderFunction;
}
