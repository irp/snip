import { ID, PageDataRet } from "@snip/database/types";
import { BaseData, BaseView, SnipData } from "@snip/snips/general/base";

export interface Message<K extends keyof EventMap = keyof EventMap> {
    type: K;
    data: EventMap[K]["message"];
}

export interface Response<K extends keyof EventMap = keyof EventMap> {
    r: EventMap[K]["response"];
}

/** Defines the types of all
 * the events that can be sent
 * and received by the worker.
 */
export interface EventMap {
    // The ready event is sent when the worker is ready
    // This is mainly used to load fonts etc
    init: {
        message: {
            render_timeout: number;
        };
        response: void;
    };

    // Stop/shutdown the worker
    stop: {
        message: void;
        response: void;
    };

    /** Setup the render pipeline and viewport interaction
     */
    setupRender: {
        message: {
            canvas: OffscreenCanvas;
            viewPortBuffer: SharedArrayBuffer;
        };
        response: void;
    };

    // Setup the page to render or change the page
    setupPage: {
        message: {
            page: PageDataRet;
        };
        response: void;
    };

    // Render the page
    render: {
        message: void;
        response: void;
    };

    // Insert a snip on the page
    insertSnip: {
        message: {
            snip: SnipData<BaseData, BaseView>;
        };
        response: void;
    };

    // Interactions are basically mouse events passed to the worker
    // via a messagechannel
    setupInteraction: {
        message: void;
        response: void;
    };
    teardownInteraction: {
        message: void;
        response: void;
    };

    // Place a snip on the page
    placeOnPage: {
        message: {
            snip: SnipData<BaseData, BaseView>;
        };
        response: void;
    };

    // Eraser tool
    enableEraser: {
        message: void;
        response: void;
    };
    disableEraser: {
        message: void;
        response: void;
    };
    eraseSnip: {
        message: {
            id: ID;
        };
        response:
            | {
                  success: true;
              }
            | {
                  success: false;
                  details: string;
              };
    };
}

// Helpers for the types
export type MessageMap<K extends keyof EventMap = keyof EventMap> =
    EventMap[K]["message"];
export type ResponseMap<K extends keyof EventMap = keyof EventMap> =
    EventMap[K]["response"];

/** Interaction events allow to send
 * mouse events to the worker thread
 * this also allows to return
 */

export type AllowedPointerEvent =
    | "pointerenter"
    | "pointerleave"
    | "pointermove"
    | "pointerdown"
    | "pointerup"
    | "pointerout"
    | "dblclick";

/**
 * Represents an interaction message event.
 * @template K - The type of interaction event.
 */
export interface InteractionMessageEvent {
    // event that triggered the interaction
    type: AllowedPointerEvent;
    msg: {
        mousePos: [number, number];
        pixelRatio: number;
    };
}

// These interaction events are on the level of the snip
// i.e. if the snip is clicked on
export interface InteractionResponseMap {
    pointerdown: {
        snipData: SnipData<BaseData, BaseView>;
    };
    dblclick: {
        snipData: SnipData<BaseData, BaseView>;
    };
    pointerenter: {
        snipData: SnipData<BaseData, BaseView>;
    };
    pointerleave: {
        snipData: SnipData<BaseData, BaseView>;
    };
    // FYI: enter and leave can be used to implement hover
}
/**
 * Represents an interaction response event.
 * @template K - The type of interaction event.
 */
export interface InteractionResponseEvent<
    K extends keyof InteractionResponseMap,
> {
    type: K;
    r: InteractionResponseMap[K];
}
