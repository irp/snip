"use client";
import useUser from "lib/hooks/useUser";
import {
    createContext,
    Dispatch,
    MutableRefObject,
    SetStateAction,
    useCallback,
    useContext,
    useEffect,
    useRef,
    useState,
} from "react";

import type {
    BookDataRet,
    ID,
    PageDataRet,
    UserConfigDataRet,
} from "@snip/database/types";
import { ViewPort } from "@snip/render/viewport";
import { BaseData, BaseView, SnipData } from "@snip/snips/general/base";

import { Rect, Tool } from "./tools/types";

import {
    PagesContextProvider,
    usePagesContext,
} from "components/context/socket/usePagesContext";
import { QueuedSnipsContextProvider } from "components/context/socket/useQueuedSnipsContext";
import { SocketContextProvider } from "components/context/useSocketContext";
import {
    useWorkerContext,
    WorkerContextProvider,
} from "components/context/viewer/useWorkerContext";

type PageIdx = number;
interface EditorContextI {
    /** Where to show the overlay
     * for tools in the editor
     *
     * Needed as state as we use a portal
     **/
    overlayContainer?: HTMLDivElement;
    setOverlayContainer: Dispatch<SetStateAction<HTMLDivElement | undefined>>;

    /** The current editor elements
     * map for multiple possible active editors
     */
    editorElementRef: MutableRefObject<Map<number, HTMLDivElement>>;
    setEditorElementRef: (idx: number) => (element: HTMLDivElement) => void;

    overlayRect: Rect;
    setOverlayRect: Dispatch<SetStateAction<Rect>>;
    overlayOnPageIdx: number;
    setOverlayOnPageIdx: Dispatch<SetStateAction<number>>;

    /** Active Page Ids and snip id
     */
    activePageIds: number[];
    setActivePageIds: Dispatch<SetStateAction<number[]>>;
    activeSnipId: ID | null;
    setActiveSnipId: Dispatch<SetStateAction<ID | null>>;

    //Current zoom and scale values
    scale: Map<PageIdx, number>;
    zoom: Map<PageIdx, number>;
    triggerScaleZoomUpdate: (page_idx: PageIdx, viewport?: ViewPort) => void;

    // User config
    userConfig: UserConfigDataRet;

    // Book data
    bookData: BookDataRet;

    enabledTools: Tool[];
}

const EditorContext = createContext<EditorContextI | null>(null);

export function useEditorContext() {
    const context = useContext(EditorContext);
    if (!context) throw new Error("Cant use EditorContext outside provider!");
    return context;
}

/**
 * Provides the context for the editor component. This also registers
 * the socket and the worker context.
 *
 *
 * @param {ReactNode} children - The child components to be wrapped by the context provider.
 * @param {number} book_id - The id of the book that the editor is editing, used for socket connection.
 * @param {PageData[]} pages - The pages of the book that the editor is allowed to edit.
 * @returns {JSX.Element} - The context provider component.
 */
function _EditorContextProvider({
    children,
    bookData,
    tools,
}: {
    children: React.ReactNode;
    bookData: BookDataRet;
    tools: Tool[];
}) {
    /** Currently shown page by its id
     */
    const [activePageIds, setActivePageIds] = useState<number[]>([]);

    /** In case of placement of an queued snip, the currently
     * active snip id.
     */
    const [activeSnipId, setActiveSnipId] = useState<ID | null>(null);

    /** The overlay allows to show a tool specific overlay
     */
    const [overlayOnPageIdx, setOverlayOnPageIdx] = useState<number>(0);
    const [overlayContainer, setOverlayContainer] = useState<HTMLDivElement>();

    /** User config */
    const { config: userConfig } = useUser(true);

    // Position relative to the page i.e. top left corner is
    // at the top left corner of the page (not the wrapper)
    // This normally mirrors the active snip position
    const [overlayRect, setOverlayRect] = useState<Rect>({
        x: 100,
        y: 400,
        width: 100,
        height: 100,
        rot: 0,
        mirror: false,
    });

    const editorElementRef = useRef<Map<number, HTMLDivElement>>(new Map());
    const setEditorElementRef = (idx: number) => {
        return (element: HTMLDivElement) => {
            if (!element) return;
            editorElementRef.current.set(idx, element);
        };
    };

    const { workers } = useWorkerContext();

    const [zoom, _setLocalZoom] = useState<Map<PageIdx, number>>(
        new Map().set(0, 1),
    );
    // Absolute scaling of the view matrix, this is not the zoom value!
    const [scale, _setScale] = useState<Map<PageIdx, number>>(
        new Map().set(0, 1),
    );

    const triggerScaleZoomUpdate = useCallback(
        (page_idx: PageIdx, viewport?: ViewPort) => {
            if (!viewport) {
                viewport = workers.get(page_idx!)?.main.viewport;
            }

            _setLocalZoom((prev) => {
                const next = new Map(prev);
                if (!viewport) {
                    return prev;
                }
                next.set(page_idx, viewport.getZoom());
                return next;
            });
            _setScale((prev) => {
                const next = new Map(prev);
                if (!viewport) {
                    return prev;
                }
                next.set(page_idx, viewport.getMatrix().a);
                return next;
            });
        },
        [workers],
    );

    return (
        <EditorContext.Provider
            value={{
                editorElementRef,
                setEditorElementRef,
                activePageIds,
                setActivePageIds,
                overlayContainer,
                setOverlayContainer,
                overlayRect,
                setOverlayRect,
                overlayOnPageIdx,
                setOverlayOnPageIdx,
                activeSnipId,
                setActiveSnipId,
                zoom,
                scale,
                triggerScaleZoomUpdate,
                userConfig: userConfig!,
                bookData,
                enabledTools: tools,
            }}
        >
            {children}
        </EditorContext.Provider>
    );
}

export function EditorContextProvider({
    bookData,
    pages,
    queuedSnips,
    tools,
    children,
}: {
    bookData: BookDataRet;
    pages: PageDataRet[];
    tools: Tool[];
    queuedSnips: SnipData<BaseData, BaseView>[];
    children: React.ReactNode;
}) {
    return (
        <SocketContextProvider book_id={bookData.id}>
            <PagesContextProvider init_pages={pages}>
                <WorkerContextProvider n={1}>
                    <_EditorContextProvider bookData={bookData} tools={tools}>
                        <QueuedSnipsContextProvider init_snips={queuedSnips}>
                            {children}
                        </QueuedSnipsContextProvider>
                    </_EditorContextProvider>
                </WorkerContextProvider>
            </PagesContextProvider>
        </SocketContextProvider>
    );
}
/**
 * Sets a reference to an element in a mutable ref object at the specified index.
 *
 * @template T - The type of the element.
 * @param ref - The mutable ref object to set the reference in.
 * @param idx - The index at which to set the reference.
 * @returns A function that sets the reference to the element.
 */
export function setRef<T>(ref: MutableRefObject<Map<number, T>>, idx: number) {
    return (element: T) => {
        if (!element) return;
        if (!ref.current) ref.current = new Map<number, T>();
        ref.current.set(idx, element);
    };
}
