import { forwardRef, LegacyRef, useEffect, useRef, useState } from "react";

import { PointerType } from "@snip/socket/types";

import { OverlayWorker } from "../worker/overlayWorker";
import { PointerI, useRemotePointers } from "./useRemotePointers";

import styles from "./pointer.module.scss";

export function RemotePointers({
    overlayWorker,
}: {
    overlayWorker: OverlayWorker;
}) {
    const ref = useRef<HTMLDivElement>(null);
    const { pointers } = useRemotePointers(ref, overlayWorker);

    const pointerElements: JSX.Element[] = [];

    for (const [id, pointer] of pointers) {
        pointerElements.push(<Pointer key={id} {...pointer} />);
    }

    return (
        <div
            ref={ref}
            style={{
                pointerEvents: "none",
            }}
        >
            {pointerElements}
        </div>
    );
}

function Pointer(pointer: PointerI) {
    const ref = useRef<HTMLSpanElement>(null);
    const { eType } = pointer;

    const [pointerDowns, setPointerDowns] = useState<
        Map<number, [number, number]>
    >(new Map());

    useEffect(() => {
        // Add element and remove it after animation is done
        if (eType === "down") {
            setPointerDowns((prev) => {
                prev.set(pointer.timestamp, [pointer.x, pointer.y]);
                return new Map(prev);
            });

            setTimeout(() => {
                setPointerDowns((prev) => {
                    prev.delete(pointer.timestamp);
                    return new Map(prev);
                });
            }, 1500); // see css animation duration
        }
    }, [eType, pointer.timestamp, pointer.x, pointer.y]);

    return (
        <>
            <__Pointer {...pointer} />
            {
                // Render the pointer down animation
                Array.from(pointerDowns).map(([timestamp, [x, y]]) => (
                    <span
                        key={timestamp}
                        ref={ref}
                        className={styles.pointerDown}
                        style={{
                            left: x,
                            top: y,
                            ["--color" as string]: pointer.color,
                        }}
                    />
                ))
            }
        </>
    );
}

function __Pointer({ pType, color, x, y, timestamp }: PointerI) {
    const timeoutRef = useRef<number | null>(null);
    const [opacity, setOpacity] = useState(1);

    const pointerProps = {
        className: styles.pointer,
        style: {
            left: x,
            top: y,
            opacity,
        },
        color,
    };

    useEffect(() => {
        if (timeoutRef.current) clearTimeout(timeoutRef.current);

        setOpacity(1);
        timeoutRef.current = window.setTimeout(
            () => {
                setOpacity(0);
                timeoutRef.current = null;
            },
            // timestamp is relative to timeOrigin
            window.performance.now() - timestamp + 1000,
        );

        return () => {
            if (timeoutRef.current) clearTimeout(timeoutRef.current);
        };
    }, [timestamp]);

    switch (pType) {
        case PointerType.PEN:
            return (
                <PenPointer
                    {...pointerProps}
                    // Move the pointer to the top to align the pen tip with the cursor
                    style={{
                        left: x,
                        top: y - 12,
                    }}
                />
            );
        default:
            return <MousePointer {...pointerProps} />;
    }
}

const MousePointer = forwardRef(function MousePointer(
    { color, ...props }: { color: string } & React.SVGProps<SVGSVGElement>,
    ref?: LegacyRef<SVGSVGElement>,
) {
    //https://tabler.io/icons/icon/pointer
    return (
        <svg
            ref={ref}
            xmlns="http://www.w3.org/2000/svg"
            width="15"
            height="15"
            viewBox="0 0 24 24"
            fill="none"
            stroke={color}
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
            {...props}
        >
            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
            <path d="M7.904 17.563a1.2 1.2 0 0 0 2.228 .308l2.09 -3.093l4.907 4.907a1.067 1.067 0 0 0 1.509 0l1.047 -1.047a1.067 1.067 0 0 0 0 -1.509l-4.907 -4.907l3.113 -2.09a1.2 1.2 0 0 0 -.309 -2.228l-13.582 -3.904l3.904 13.563z" />
        </svg>
    );
});

const PenPointer = forwardRef(function PenPointer(
    { color, ...props }: { color: string } & React.SVGProps<SVGSVGElement>,
    ref?: LegacyRef<SVGSVGElement>,
) {
    //https://tabler.io/icons/icon/pencile
    return (
        <svg
            ref={ref}
            xmlns="http://www.w3.org/2000/svg"
            width="15"
            height="15"
            viewBox="0 0 24 24"
            fill="none"
            stroke={color}
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
            {...props}
        >
            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
            <path d="M4 20h4l10.5 -10.5a2.828 2.828 0 1 0 -4 -4l-10.5 10.5v4" />
            <path d="M13.5 6.5l4 4" />
        </svg>
    );
});

// Not used anymore
const _Trail = ({
    points,
    color,
}: {
    points: Array<[number, number, number]>;
    color: string;
}) => {
    if (points.length < 2) return null;

    // Generate the path data
    const d = points
        .map(([x, y], index) => `${index === 0 ? "M" : "L"}${x},${y}`)
        .join(" ");

    return (
        <>
            {/* Render dots */}
            {points.map(([x, y], index) => (
                <circle
                    key={index}
                    cx={x}
                    cy={y}
                    r={3}
                    fill={color}
                    opacity={0.6}
                />
            ))}
            {/* Render the path */}
            <path
                d={d}
                stroke={color}
                strokeWidth="2"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                opacity={0.6}
            />
        </>
    );
};
