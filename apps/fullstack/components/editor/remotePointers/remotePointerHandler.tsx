import { Socket } from "socket.io-client";

import { TypedEventTarget } from "@snip/common/eventTarget";
import {
    ClientToServerEvents,
    PointerType,
    ServerToClientEvents,
} from "@snip/socket/types";

import type { OverlayWorker } from "../worker/overlayWorker";
import {
    PageChangeEvent,
    PointerEventTypeMap,
    RemotePointerEvent,
} from "./events";

/** Handler to communicate the current tool and pointer positions to
 * other clients.
 */
export class RemotePointerHandler {
    private socket: Socket<ServerToClientEvents, ClientToServerEvents>;
    private worker: OverlayWorker;

    private eventTarget = new TypedEventTarget<PointerEventTypeMap>();

    constructor(worker: OverlayWorker, socket: Socket) {
        this.socket = socket;
        this.worker = worker;
    }

    private pointerHandler(t: keyof ClientToServerEvents, e: PointerEvent) {
        if (!this.worker.current_page_id) {
            return;
        }

        let xy: [number, number] = [NaN, NaN];
        if (e.currentTarget !== e.target) {
            const rect = (
                e.currentTarget as HTMLElement
            ).getBoundingClientRect();
            const offsetX =
                e.clientX -
                rect.left -
                (e.currentTarget as HTMLElement).clientLeft;
            const offsetY =
                e.clientY -
                rect.top -
                (e.currentTarget as HTMLElement).clientTop;
            xy = this.worker.viewport.getPosInPageCoords([offsetX, offsetY]);
        } else {
            xy = this.worker.viewport.getPosInPageCoords([
                e.offsetX * window.devicePixelRatio,
                e.offsetY * window.devicePixelRatio,
            ]);
        }

        this.socket.volatile.emit(
            t,
            xy[0],
            xy[1],
            this.worker.current_page_id,
            getPointerType(e),
        );
    }

    private onRemotePointer(
        eType: keyof PointerEventTypeMap,
        x: number,
        y: number,
        color: [number, number, number, number],
        pType: PointerType,
    ) {
        if (color.length !== 4) {
            console.error("Invalid color array", color);
            return;
        }
        const xy = this.worker.viewport.getPosInViewCoords([x, y]);
        xy[0] /= window.devicePixelRatio;
        xy[1] /= window.devicePixelRatio;
        const e = new RemotePointerEvent(xy, color, pType, eType);
        this.eventTarget.dispatchEvent(e);
    }

    /**
     * Attaches pointer event listeners to the specified HTML element.
     * This method binds the `pointermove` event to the `pointerDownHandler` method,
     * allowing the PointerHandler to track and communicate pointer movements.
     *
     * We also subscribe to the pointer:move event to get updates from other clients here!
     *
     * @param element - The HTML element to which the pointer event listeners will be attached.
     * @returns A cleanup function that removes the event listeners when called.
     */
    public attachPointerEvents(element: HTMLElement): () => void {
        // Subscribe to remote pointer events
        const onRemotePointerMove = this.onRemotePointer.bind(this, "move");
        this.socket.on("pointer:move", onRemotePointerMove);
        const onRemotePointerDown = this.onRemotePointer.bind(this, "down");
        this.socket.on("pointer:down", onRemotePointerDown);

        // Bind pointermove event to pointerMoveHandler
        const onPointerMove = this.pointerHandler.bind(this, "pointer:move");
        element.addEventListener("pointermove", onPointerMove, {
            passive: true,
        });
        const onPointerDown = this.pointerHandler.bind(this, "pointer:down");
        element.addEventListener("pointerdown", onPointerDown, {
            passive: true,
        });

        return () => {
            element.removeEventListener("pointermove", onPointerMove);
            element.removeEventListener("pointerdown", onPointerDown);

            this.socket.off("pointer:move", onRemotePointerMove);
            this.socket.off("pointer:down", onRemotePointerDown);
        };
    }

    /**
     * Registers an event listener for a specific event type on the PointerHandler.
     * This allows external code to listen for custom events, such as `remotePointerMove`,
     * which are dispatched by the PointerHandler when remote pointer events are received.
     *
     * @param type - The type of event to listen for. Must be a key of `PointerEventMap`.
     * @param listener - The callback function to be invoked when the event is dispatched.
     *                  The listener will receive an event object of the corresponding type.
     */
    public addEventListener<K extends keyof PointerEventTypeMap>(
        type: K,
        listener: (e: PointerEventTypeMap[K]) => void,
    ) {
        this.eventTarget.addEventListener(type, listener);
    }

    /**
     * Removes an event listener previously registered with `addEventListener`.
     * This is useful for cleaning up listeners when they are no longer needed,
     * preventing potential memory leaks or unintended behavior.
     *
     * @param type - The type of event for which the listener was registered.
     * @param listener - The callback function to be removed from the event listener list.
     */
    public removeEventListener<K extends keyof PointerEventTypeMap>(
        type: K,
        listener: (e: PointerEventTypeMap[K]) => void,
    ) {
        this.eventTarget.removeEventListener(type, listener);
    }

    // Helper function to communicate page changes
    public pageChanged(pageId: number) {
        this.eventTarget.dispatchEvent(new PageChangeEvent(pageId));
    }
}

function getPointerType(event: PointerEvent): PointerType {
    switch (event.pointerType) {
        case "mouse":
            return PointerType.MOUSE;
        case "touch":
            return PointerType.TOUCH;
        case "pen":
            return PointerType.PEN;
        default:
            return PointerType.OTHER;
    }
}
