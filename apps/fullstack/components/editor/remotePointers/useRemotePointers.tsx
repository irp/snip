import { RefObject, useEffect, useState } from "react";

import { PointerType } from "@snip/socket/types";

import { useEditorContext } from "../context";
import { OverlayWorker } from "../worker/overlayWorker";
import { RemotePointerEvent } from "./events";

import { useSocketContext } from "components/context/useSocketContext";

export interface PointerI {
    color: string;
    x: number;
    y: number;
    pType: PointerType;
    eType: "move" | "down" | "up";
    timestamp: number;
}

/**
 * A custom React hook to manage and track remote pointers in a collaborative environment.
 *
 * This hook listens for remote pointer events (move, down) and updates the state of pointers
 * accordingly. It uses a WebSocket connection to receive real-time updates from other users. See
 * RemotePointerHandler for more details.
 *
 * @param {OverlayWorker} overlayWorker - An instance of OverlayWorker that handles remote pointer events.
 * @returns {Object} An object containing the current state of remote pointers.
 * @property {Map<string, Pointer>} pointers - A map of pointers, where the key is the pointer's color and the value is the pointer's data.
 */
export function useRemotePointers(
    ref: RefObject<HTMLDivElement>,
    overlayWorker: OverlayWorker,
) {
    const [pointers, setPointers] = useState<Map<string, PointerI>>(new Map());
    const { isConnected } = useSocketContext();

    /** Setup remote socket connection */
    useEffect(() => {
        if (!ref.current || !isConnected) return;
        const handler = overlayWorker.remotePointerHandler;

        function updatePointers(e: RemotePointerEvent<"move" | "down">) {
            setPointers((pointers) => {
                const color = e.hexColor();
                pointers.set(color, {
                    color,
                    x: e.x,
                    y: e.y,
                    pType: e.pType,
                    eType: e.eType,
                    timestamp: e.timeStamp,
                });
                return new Map(pointers);
            });
        }

        function removePointer() {
            setPointers(new Map());
        }

        // Attach the pointer events to send pointer events
        const cleanup = handler?.attachPointerEvents(
            ref.current.parentElement!,
        );

        // Subscribe to remote pointer events
        handler?.addEventListener("move", updatePointers);
        handler?.addEventListener("down", updatePointers);
        handler?.addEventListener("pageChange", removePointer);

        return () => {
            cleanup?.();

            handler?.removeEventListener("move", updatePointers);
            handler?.removeEventListener("down", updatePointers);
            handler?.removeEventListener("pageChange", removePointer);
        };
    }, [isConnected, overlayWorker.remotePointerHandler, ref]);

    return {
        pointers,
    };
}
