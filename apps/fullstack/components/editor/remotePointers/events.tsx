import { PointerType } from "@snip/socket/types";

export type PointerEventTypeMap = {
    move: RemotePointerEvent<"move">;
    down: RemotePointerEvent<"down">;
    up: RemotePointerEvent<"up">;
    pageChange: PageChangeEvent;
};

export class PageChangeEvent extends Event {
    public pageId: number;

    constructor(pageId: number) {
        super("pageChange");
        this.pageId = pageId;
    }
}

/**
 * Represents a custom event for remote pointer interactions, such as movement, down, or up events.
 * This class extends the native `Event` class to include additional properties like coordinates,
 * color, pointer type, and event type.
 *
 * @template T - The type of the pointer event, which must be one of the allowed `PointerEventType` values.
 */
export class RemotePointerEvent<
    T extends keyof PointerEventTypeMap,
> extends Event {
    /** The coordinates of the pointer event in the viewport. */
    public xy: [number, number];

    /** The color associated with the pointer event, represented as an RGBA array. */
    public color: [number, number, number, number];

    /** The type of pointer device used (e.g., mouse, touch, pen). */
    public pType: PointerType;

    /** The type of pointer event (e.g., move, down, up). */
    public eType: T;

    /**
     * Creates a new `RemotePointerEvent` instance.
     *
     * @param xy - The coordinates of the pointer event in the viewport.
     * @param color - The color associated with the pointer event, represented as an RGBA array.
     * @param pType - The type of pointer device used (e.g., mouse, touch, pen).
     * @param eType - The type of pointer event (e.g., move, down, up).
     */
    constructor(
        xy: [number, number],
        color: [number, number, number, number],
        pType: PointerType,
        eType: T,
    ) {
        super(eType);
        this.xy = xy;
        this.color = color;
        this.pType = pType;
        this.eType = eType;
    }

    /**
     * Gets the x-coordinate of the pointer event.
     *
     * @returns The x-coordinate of the pointer event.
     */
    get x() {
        return this.xy[0];
    }

    /**
     * Gets the y-coordinate of the pointer event.
     *
     * @returns The y-coordinate of the pointer event.
     */
    get y() {
        return this.xy[1];
    }

    /**
     * Converts the RGBA color array to a hexadecimal color string.
     *
     * @returns A hexadecimal color string representing the RGBA color.
     */
    public hexColor() {
        return `#${toHex(this.color[0]!)}${toHex(this.color[1]!)}${toHex(
            this.color[2]!,
        )}${toHex(this.color[3]!)}`;
    }

    /**
     * Returns a string representation of the pointer Type
     * @returns A string representation of the pointer Type
     */
    public pointerTypeString() {
        switch (this.pType) {
            case PointerType.MOUSE:
                return "mouse";
            case PointerType.TOUCH:
                return "touch";
            case PointerType.PEN:
                return "pen";
            default:
                return "other";
        }
    }
}

function toHex(value: number): string {
    const hex = value.toString(16);
    return hex.length === 1 ? "0" + hex : hex;
}
