import { usePreventSwipeGestures } from "lib/hooks/usePreventSwipeGestures";

import type { BookDataRet, PageDataRet } from "@snip/database/types";
import { BaseData, BaseView, SnipData } from "@snip/snips/general/base";

import { EditorContextProvider } from "./context";
import Resizer from "./resizer";
import Sidebar from "./sidebar";
import { Toolbar } from "./tools/toolbar";
import { Tool } from "./tools/types";
import { Viewer } from "./viewer";

import styles from "./editor.module.scss";

export function EditorSinglePageNoSidebar({
    pageData,
    bookData,
}: {
    pageData: PageDataRet;
    bookData: BookDataRet;
}) {
    return (
        <EditorContextProvider
            tools={["interaction", "movePage"]}
            bookData={bookData}
            pages={[pageData]}
            queuedSnips={[]}
        >
            <Toolbar />
            <Viewer />
        </EditorContextProvider>
    );
}

type Perms = { pRead?: boolean; pWrite?: boolean; pDelete?: boolean };

export function EditorSinglePage({
    bookData,
    pages,
    queuedSnips,
    perms,
}: {
    bookData: BookDataRet;
    pages: PageDataRet[];
    queuedSnips: SnipData<BaseData, BaseView>[];
    perms: Perms;
}) {
    // Parse tools based on the bookData
    const tools = permsToEnabledTools(perms);
    usePreventSwipeGestures();

    return (
        <EditorContextProvider
            bookData={bookData}
            pages={pages}
            queuedSnips={queuedSnips}
            tools={tools}
        >
            <EditorWrapper>
                <Sidebar init_with_width={false} />
                <Resizer />
                <ViewerWrapper>
                    <Toolbar />
                    <Viewer />
                </ViewerWrapper>
            </EditorWrapper>
        </EditorContextProvider>
    );
}

function EditorWrapper({ children }: { children: React.ReactNode }) {
    return (
        <div id="the_editor" className={styles.editor}>
            {children}
        </div>
    );
}

function ViewerWrapper({ children }: { children: React.ReactNode }) {
    return <div className={styles.viewer}>{children}</div>;
}

export function permsToEnabledTools(perms: Perms) {
    const tools: Tool[] = ["interaction", "movePage"];

    if (perms.pWrite) {
        tools.push("doodle");
        tools.push("text");
        tools.push("placement");
        tools.push("image");
        tools.push("eraser");
    }
    return tools;
}

// Allow to import from the index file
export { EditorContextProvider };
export { EditorWrapper };
export { Sidebar as EditorSidebar };
export { Viewer as EditorViewer };
export { Toolbar as EditorToolbar };
