"use client";
import Link from "next/link";
import {
    createContext,
    Dispatch,
    ReactNode,
    SetStateAction,
    useContext,
    useEffect,
    useMemo,
    useState,
} from "react";
import { Button } from "react-bootstrap";
import { createPortal } from "react-dom";
import { BsFonts } from "react-icons/bs";
import { TbArrowsMove, TbHelpSmall, TbPointer } from "react-icons/tb";

import { useEditorContext } from "../context";
import {
    Help as HelpWrapper,
    HelpDescription,
    HelpHeader,
} from "./common/help";
import { ToolsContextProvider } from "./context";
import { PenEmpty } from "./doodle/svgs";
import { getToolComponents } from "./toolsFactory";
import { Tool, ToolComponents } from "./types";

import { usePagesContext } from "components/context/socket/usePagesContext";

import styles from "./toolbar.module.scss";

export function Toolbar() {
    // Filter based on referenced page

    const { pages } = usePagesContext();
    const { activePageIds, enabledTools } = useEditorContext();

    const activePages = useMemo(() => {
        return activePageIds.map((id) => pages.get(id));
    }, [activePageIds, pages]);

    const toolsComponents = useMemo(() => {
        return enabledTools.map((tool) => getToolComponents(tool));
    }, [enabledTools]);

    const is_referenced_page = activePages.some((p) => p?.referenced_page_id);

    const filteredTools = useMemo(() => {
        if (is_referenced_page) {
            return toolsComponents.filter(
                (t) => t.name == "movePage" || t.name == "interaction",
            );
        }
        return toolsComponents;
    }, [is_referenced_page, toolsComponents]);

    return (
        <ToolbarContextProvider>
            <ToolsContextProvider
                contextProviders={toolsComponents.map((t) => t.ContextProvider)}
            >
                <div className={styles.toolbar} data-position="left">
                    <Menu>{filteredTools}</Menu>
                    <ToolOverlay>{filteredTools}</ToolOverlay>
                    <Help>{filteredTools}</Help>
                </div>
                {is_referenced_page && (
                    <div
                        style={{
                            position: "absolute",
                            top: 0,
                            left: 0,
                            width: "200px",
                            textAlign: "center",
                            zIndex: 1,

                            background: "var(--bs-gray-300)",
                            borderBottomRightRadius: "var(--bs-border-radius)",
                            overflow: "hidden",
                        }}
                    >
                        <span>
                            Referenced pages can&apos;t be edited! You can find
                            the original
                        </span>{" "}
                        <Link
                            href={`/books/${activePages[0]!.referenced_book_id!}/page_ids/${activePages[0]!.referenced_page_id}`}
                            target="_blank"
                        >
                            here
                        </Link>
                        .
                    </div>
                )}
            </ToolsContextProvider>
        </ToolbarContextProvider>
    );
}

/* -------------------------------------------------------------------------- */
/*                         The context for the toolbar                        */
/* -------------------------------------------------------------------------- */
// Tools might use this

interface ToolbarContext {
    activeTool: Tool | null;
    setActiveTool: Dispatch<SetStateAction<Tool | null>>;
    activeSubmenu: Tool | null;
    setActiveSubmenu: Dispatch<SetStateAction<Tool | null>>;
    showHelp: boolean;
    setShowHelp: Dispatch<SetStateAction<boolean>>;
}

const ToolbarContext = createContext<ToolbarContext | null>(null);

function ToolbarContextProvider({ children }: { children: ReactNode }) {
    const [showHelp, setShowHelp] = useState(false);
    const [activeTool, setActiveTool] = useState<Tool | null>("interaction");
    const [activeSubmenu, setActiveSubmenu] = useState<Tool | null>(null);

    const { activeSnipId } = useEditorContext();

    /** On queued snip click set specific tools
     * to be active or inactive
     */
    useEffect(() => {
        if (activeSnipId) {
            //setActiveTool("placement");
        } else {
            setActiveTool("interaction");
        }
    }, [activeSnipId]);

    useEffect(() => {
        console.log("activeTool", activeTool);
    }, [activeTool]);

    return (
        <ToolbarContext.Provider
            value={{
                showHelp,
                setShowHelp,
                activeTool,
                setActiveTool,
                activeSubmenu,
                setActiveSubmenu,
            }}
        >
            {children}
        </ToolbarContext.Provider>
    );
}

export function useToolbarContext() {
    const context = useContext(ToolbarContext);
    if (!context) throw new Error("Cant use ToolbarContext outside provider!");
    return context;
}

/* -------------------------------------------------------------------------- */
/*                   Helpers to wrap the tools and the help                   */
/* -------------------------------------------------------------------------- */

/**
 * Renders a menu component with toggleable tools and submenus.
 *
 * @param children - An array of tool components.
 * @returns The rendered menu component.
 */
function Menu({ children }: { children: ToolComponents[] }) {
    const { activeTool, setActiveTool, activeSubmenu, setActiveSubmenu } =
        useToolbarContext();

    function toggleTool(tool: Tool) {
        setActiveTool((prev) => (prev === tool ? null : tool));
    }

    function toggleSubmenu(submenu: Tool) {
        setActiveSubmenu((prev) => (prev === submenu ? null : submenu));
    }

    return (
        <div className={styles.menu}>
            <div className={styles.tools}>
                {children.map((tool) => {
                    return (
                        <tool.Toggle
                            key={tool.name}
                            isActive={activeTool === tool.name}
                            setActive={() => toggleTool(tool.name)}
                            isExpanded={activeSubmenu === tool.name}
                            setExpanded={() => toggleSubmenu(tool.name)}
                        />
                    );
                })}
            </div>
            <div className={styles.submenu} data-show={activeSubmenu !== null}>
                {children.map((tool) => {
                    if (!tool.Submenu) {
                        return null;
                    }
                    return (
                        <tool.Submenu
                            key={tool.name}
                            isExpanded={activeSubmenu === tool.name}
                            setExpanded={() => toggleSubmenu(tool.name)}
                        />
                    );
                })}
            </div>
        </div>
    );
}

/**
 * Renders the overlay component based on the active tool.
 *
 * @param children - An array of ToolComponents.
 * @param overlayContainer - Optional. The container element for the overlay.
 * @param overlayRect - The rectangle representing the overlay.
 * @param setOverlayRect - A function to update the overlay rectangle.
 * @returns The rendered overlay component or null if no overlay is found.
 */
function ToolOverlay({ children }: { children: ToolComponents[] }) {
    const { activeTool } = useToolbarContext();
    const { editorElementRef, overlayOnPageIdx } = useEditorContext();
    // Find the overlay component
    const Overlay = children.find((c) => c.name === activeTool)?.Overlay;

    if (!editorElementRef.current.get(overlayOnPageIdx) || !Overlay)
        return null;

    return createPortal(
        <Overlay />,
        editorElementRef.current.get(overlayOnPageIdx)!,
    );
}

function Help({ children }: { children: ToolComponents[] }) {
    const { showHelp, setShowHelp, activeTool } = useToolbarContext();

    // Find the help component
    const Help =
        children.find((c) => c.name === activeTool)?.Help || HelpToolbar;

    return (
        <div className={styles.help} data-expanded={showHelp}>
            <div className={styles.content}>
                <Help />
            </div>
            <Button
                variant="none"
                className={styles.expandBtn}
                onClick={() => setShowHelp(!showHelp)}
            >
                {showHelp ? <span>Hide</span> : <TbHelpSmall size={28} />}
            </Button>
        </div>
    );
}

function HelpToolbar() {
    return (
        <HelpWrapper>
            <HelpHeader>
                <span>Toolbar</span>
                <div className={styles.defHelpIcon}>
                    <TbPointer style={{ stroke: "var(--bs-secondary" }} />
                    <TbArrowsMove style={{ stroke: "var(--bs-secondary" }} />
                    <PenEmpty style={{ fill: "var(--bs-secondary" }} />
                    <BsFonts
                        style={{ fill: "var(--bs-secondary", stroke: "none" }}
                    />
                </div>
            </HelpHeader>
            <HelpDescription>
                The toolbar allows you to select different tools to interact
                with the page. Tools may have a submenu which can be opened by
                using the small arrow or by <kbd>RMB</kbd> click.
            </HelpDescription>
        </HelpWrapper>
    );
}
