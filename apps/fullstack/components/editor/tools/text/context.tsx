import useDevicePixelRatio from "lib/hooks/useDevicePixelRatio";
import {
    createContext,
    useCallback,
    useContext,
    useEffect,
    useMemo,
    useRef,
    useState,
} from "react";

import { AllowedFonts } from "@snip/database/fonts/internal";
import { TextSnip } from "@snip/snips/general/text";

import { useToolsContext } from "../context";
import { ToolContextProps } from "../types";

import { useWorkerContext } from "components/context/viewer/useWorkerContext";
import { useEditorContext } from "components/editor/context";

export interface TextStyles {
    /** The font family of the text.
     * this is extracted via the sql_to_ts script on
     * startup from the database. I.e. if a new font is added
     * to the database, the server needs to be restarted for it
     * to be available.
     */
    fontFamily: AllowedFonts;

    /** The font size of the text.
     * This is in db units.
     */
    fontSize: number;

    /** The line height of the text.
     * This is in db units.
     */
    lineHeight: number;

    /** The line wrap of the text. I.e. how many points
     * are allowed in one line before the text is wrapped.
     * This is in db units.
     *
     * This is parsed using the width state!
     */
    lineWrap: number;
}

export interface TextToolContextI {
    /** The text styles define the current text styles
     * that are applied to the text if it is created.
     * This is in db units i.e. what is used in the snip
     */
    textStyles: TextStyles;
    setTextStyles: (
        styles: Partial<TextStyles> | ((t: TextStyles) => Partial<TextStyles>),
        units?: "view" | "page",
    ) => void;

    /** Get text styles
     * in dom coordinates
     * i.e. to use in the textarea
     */
    getStylesInViewCoords: (
        page_idx: number,
        textStyles: TextStyles,
    ) => TextStyles;
    getStylesInPageCoords: (
        page_idx: number,
        textStyles: TextStyles,
    ) => TextStyles;

    /** The actual active text snip. Useful in some
     * cases, e.g. when the user wants to edit the text
     * snip before it is finally placed.
     */
    text: string;
    setText: (text: string) => void;

    incrementWrapByDrag: (x_mov: number) => void;

    activeTextSnip: TextSnip | null;
}

const TextToolContext = createContext<TextToolContextI | null>(null);

export function useTextToolContext() {
    const context = useContext(TextToolContext);
    if (!context) throw new Error("Cant use TextToolContext outside provider!");
    return context;
}

export function TextToolContextProvider({ children }: ToolContextProps) {
    const { workers } = useWorkerContext();
    const { activeSnip, placePageIdx } = useToolsContext();
    const { scale, userConfig } = useEditorContext();
    const pr = useDevicePixelRatio();

    // Get currently active text snip (if active)
    const activeTextSnip: TextSnip | null = useMemo(() => {
        if (activeSnip instanceof TextSnip) return activeSnip;
        return null;
    }, [activeSnip]);

    const [text, _setText] = useState<string>("");

    const setText = useCallback(
        (text: string) => {
            if (!activeTextSnip) return;

            // Update snip
            activeTextSnip.lineWrap = activeTextSnip.lineWrap || 0;
            activeTextSnip.text = text;

            // Set formatted text
            _setText(activeTextSnip.text);

            workers.get(placePageIdx)?.overlay.rerender();
        },
        [activeTextSnip, placePageIdx, workers],
    );

    /**
     *  Styles in db units
     */
    const [textStyles, _setTextStyles] = useState<TextStyles>({
        fontFamily: "Arial",
        fontSize: 12,
        lineHeight: 1.25,
        lineWrap: 400,
    });

    const setTextStyles = useCallback(
        (
            styles:
                | Partial<TextStyles>
                | ((t: TextStyles) => Partial<TextStyles>),
        ) => {
            if (!activeTextSnip) return;

            _setTextStyles((prev) => {
                if (typeof styles === "function") {
                    styles = styles(prev);
                }

                // Update snip
                if (styles.fontFamily) {
                    activeTextSnip.canvasFont.fontFamily = styles.fontFamily;
                }
                if (styles.fontSize) {
                    activeTextSnip.canvasFont.fontSize = styles.fontSize * 2.35;
                }
                if (styles.lineHeight) {
                    activeTextSnip.canvasFont.lineHeight = styles.lineHeight;
                }

                // Always reset linewrap to force text reformatting
                activeTextSnip.lineWrap =
                    styles.lineWrap ?? activeTextSnip.lineWrap;
                _setText(activeTextSnip.text);

                /** Not supported yet
                 * 
                activeTextSnip.canvasFont.fontStyle =
                styles.fontStyle ?? activeTextSnip.canvasFont.fontStyle;
                activeTextSnip.canvasFont.fontWeight =
                styles.fontWeight ?? activeTextSnip.canvasFont.fontWeight;
                */
                workers.get(placePageIdx)?.overlay.rerender();
                return {
                    ...prev,
                    ...styles,
                };
            });
        },
        [activeTextSnip, placePageIdx, workers],
    );

    /**
     * Converts text styles from page coordinates to view coordinates.
     *
     * @param page_idx - The index of the page.
     * @param styles - The text styles in page coordinates.
     * @returns The text styles converted to view coordinates.
     */
    const getStylesInViewCoords = useCallback(
        (page_idx: number, styles: TextStyles) => {
            const s = (2.35 / pr) * scale.get(page_idx)!;

            return {
                ...styles,
                fontSize: s * styles.fontSize,
                lineHeight: s * styles.lineHeight * styles.fontSize,
                lineWrap: (s * styles.lineWrap) / 2.35,
            };
        },
        [pr, scale],
    );

    const getStylesInPageCoords = useCallback(
        (page_idx: number, styles: TextStyles) => {
            const s = 2.35 * scale.get(page_idx)!;

            return {
                ...styles,
                fontSize: styles.fontSize / s,
                lineHeight: styles.lineHeight / styles.fontSize / s,
                lineWrap: styles.lineWrap / s,
            };
        },
        [scale],
    );

    const incrementWrapByDrag = useCallback(
        (x_mov: number) => {
            // TODO:
            // increment lineWrap
            // set minWidth
            setTextStyles({ lineWrap: textStyles.lineWrap + x_mov });
        },
        [setTextStyles, textStyles.lineWrap],
    );

    const mounted = useRef(false);

    //Initial sync from db
    useEffect(() => {
        if (mounted.current || !userConfig) return;
        mounted.current = true;

        setTextStyles((prev) => {
            if (userConfig.text_font) {
                prev.fontFamily = userConfig.text_font as AllowedFonts;
            }

            if (userConfig.text_fontSize) {
                prev.fontSize = userConfig.text_fontSize;
            }

            if (userConfig.text_lineHeight) {
                prev.lineHeight = userConfig.text_lineHeight;
            }

            if (userConfig.text_lineWrap) {
                prev.lineWrap = userConfig.text_lineWrap;
            }

            return { ...prev };
        });

        return () => {
            mounted.current = false;
        };
    }, [userConfig, setTextStyles]);

    return (
        <TextToolContext.Provider
            value={{
                textStyles,
                setTextStyles,
                getStylesInViewCoords,
                getStylesInPageCoords,
                text,
                setText,
                incrementWrapByDrag,
                activeTextSnip,
            }}
        >
            {children}
        </TextToolContext.Provider>
    );
}
