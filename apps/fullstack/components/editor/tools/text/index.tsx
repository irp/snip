import { useCallback, useEffect, useRef, useState } from "react";
import { Button, ButtonGroup } from "react-bootstrap";
import { BsFonts } from "react-icons/bs";
import { TbMarkdown } from "react-icons/tb";

import { AllowedFonts, FONT_URLS } from "@snip/database/fonts/internal";
import { MarkdownSnip } from "@snip/snips/general/markdown";
import { TextSnip } from "@snip/snips/general/text";

import DragMove from "../common/drag";
import {
    Help,
    HelpDescription,
    HelpHeader,
    HelpShortcuts,
} from "../common/help";
import {
    Overlay,
    OverlayContent,
    OverlayMover,
    OverlayOutline,
    OverlayPlaceButton,
    useSnipOverlay,
} from "../common/overlay";
import { ItemSmall, Submenu } from "../common/submenu";
import { Toggle } from "../common/toggle";
import { useToolsContext } from "../context";
import { usePlacementContext } from "../placement/context";
import { useToolbarContext } from "../toolbar";
import { ToolComponents, ToolSubmenuProps, ToolToggleProps } from "../types";
import { TextToolContextProvider, useTextToolContext } from "./context";
import { KeyHandler } from "./keyHandler";

import { useQueuedSnipsContext } from "components/context/socket/useQueuedSnipsContext";
import { useEditorContext } from "components/editor/context";
import { MarkdownModal } from "components/editor/markdown/modal";
import { NumberInput } from "components/form/inputs/number";
import { DropdownInput } from "components/form/inputs/option";

import styles from "./text.module.scss";

function TextToggle(props: ToolToggleProps) {
    const { activeSnip } = useToolsContext();
    const { textStyles, setTextStyles } = useTextToolContext();
    const { addTempSnip } = useQueuedSnipsContext();
    const { setActiveSnipId } = useEditorContext();
    const { setActiveTool } = useToolbarContext();

    // create a new snip on click if necessary
    const newSnip = useCallback(() => {
        if (!activeSnip || !(activeSnip instanceof TextSnip)) {
            // Create new temp text snip
            // and set styles
            const snip = new TextSnip({
                text: "",
                book_id: -1,
                x: 100,
                y: 100,
                ...textStyles,
            });
            addTempSnip(snip);
            setActiveSnipId(snip.id);
        } else {
            // Set styles to selected snip
            setTextStyles({
                fontFamily: activeSnip.canvasFont.fontFamily,
                fontSize: activeSnip.canvasFont.fontSize / 2.35,
                lineHeight: activeSnip.canvasFont.lineHeight,
                lineWrap: activeSnip.lineWrap,
            });
        }
    }, [activeSnip, addTempSnip, setActiveSnipId, setTextStyles, textStyles]);

    useEffect(() => {
        if (activeSnip && !(activeSnip instanceof TextSnip)) {
            setActiveTool("placement");
        }
    }, [activeSnip, setActiveTool]);

    return (
        <Toggle
            label="Move"
            icon={
                <BsFonts
                    style={{
                        stroke: "none",
                    }}
                />
            }
            onClick={newSnip}
            {...props}
        />
    );
}

function TextSubMenu(props: ToolSubmenuProps) {
    const { textStyles, setTextStyles } = useTextToolContext();

    useEffect(() => {
        const inp = document.getElementById(
            "font_size_text_input",
        ) as HTMLInputElement;
        if (inp) {
            inp.value = textStyles.fontSize.toString();
        }

        const inp2 = document.getElementById(
            "line_wrap_text_input",
        ) as HTMLInputElement;
        if (inp2) {
            inp2.value = textStyles.lineWrap.toFixed();
        }

        const inp3 = document.getElementById(
            "line_height_text_input",
        ) as HTMLInputElement;
        if (inp3) {
            inp3.value = textStyles.lineHeight.toString();
        }

        return () => {};
    }, [textStyles]);

    return (
        <Submenu {...props} className={styles.menu}>
            <ItemSmall className={styles.font_family} style={{ zIndex: 3 }}>
                <DropdownInput
                    label="Font Family"
                    value={textStyles.fontFamily}
                    options={FONT_URLS.map(([, name]) => ({
                        eventKey: name,
                        label: (
                            <span
                                style={{
                                    fontFamily: name,
                                }}
                            >
                                {name}
                            </span>
                        ),
                    }))}
                    onSelect={(name) => {
                        setTextStyles({
                            fontFamily: name as AllowedFonts,
                        });
                    }}
                />
            </ItemSmall>
            <ItemSmall className={styles.font_size}>
                <NumberInput
                    id="font_size_text_input"
                    label="Font Size"
                    min={6}
                    max={72}
                    step={1}
                    defaultValue={textStyles.fontSize}
                    onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                        setTextStyles({
                            fontSize: parseInt(e.target.value),
                        });
                    }}
                    onReset={() => {
                        setTextStyles({
                            fontSize: 12,
                        });
                    }}
                />
            </ItemSmall>
            <ItemSmall className={styles.line_wrap}>
                <NumberInput
                    id="line_wrap_text_input"
                    label="Line Wrap"
                    min={50}
                    max={1000}
                    step={1}
                    defaultValue={textStyles.lineWrap.toFixed()}
                    onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                        setTextStyles({
                            lineWrap: parseInt(e.target.value),
                        });
                    }}
                    onReset={() => {
                        setTextStyles({
                            lineWrap: 400,
                        });
                    }}
                />
            </ItemSmall>
            <ItemSmall className={styles.line_height}>
                <NumberInput
                    id="line_height_text_input"
                    label="Line Height"
                    min={0.5}
                    max={3}
                    step={0.1}
                    defaultValue={textStyles.lineHeight}
                    onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                        setTextStyles({
                            lineHeight: parseFloat(e.target.value),
                        });
                    }}
                    onReset={() => {
                        setTextStyles({
                            lineHeight: 1.25,
                        });
                    }}
                />
            </ItemSmall>
            <ItemSmall className={styles.markdown}>
                <MarkdownTextEditor />
            </ItemSmall>
        </Submenu>
    );
}

/** Button to access the markdowntexteditor */
function MarkdownTextEditor() {
    const [snipID, setSnipId] = useState<number | null>(null);
    const [show, setShow] = useState(false);
    const { addTempSnip, snips } = useQueuedSnipsContext();
    const { bookData, setActiveSnipId } = useEditorContext();

    let snip: MarkdownSnip | undefined = undefined;
    if (snipID !== null) {
        snip = snips.get(snipID) as MarkdownSnip;
    }

    return (
        <>
            <div style={{ color: "var(--bs-gray-800)" }}>
                Markdown editor (experimental)
            </div>
            <Button
                variant="none"
                onClick={(e) => {
                    // TODO: check for active snip
                    const snip = new MarkdownSnip({
                        text: "Placeholder",
                        book_id: bookData.id,
                    });
                    addTempSnip(snip);
                    setSnipId(snip.id);
                    //setActiveSnipId(snip.id);
                    setShow(true);
                    (e.target as HTMLElement).blur();
                }}
            >
                <TbMarkdown width={"100%"} height={"100%"} />
            </Button>
            {snip && snip.type == "markdown" && (
                <MarkdownModal
                    show={snip && show}
                    setShow={setShow}
                    snip={snip}
                    onApply={(s) => {
                        setActiveSnipId((_id) => s.id);
                    }}
                />
            )}
        </>
    );
}

function TextOverlay() {
    const areaRef = useRef<HTMLTextAreaElement>(null);
    const { activeSnip, placePageIdx, transformRelativeCoords } =
        useToolsContext();
    const { move, bindOverlayToRect } = usePlacementContext();
    const { text, textStyles, setTextStyles, getStylesInViewCoords, setText } =
        useTextToolContext();
    const [cursor, setCursor] = useState({
        beforeStart: 0,
        beforeEnd: 0,
    });

    useEffect(() => {
        if (!areaRef.current) return;
        const textArea = areaRef.current;

        // Hotfix to allow for smooth transitions
        // this disabled the overscroll effect on ios
        textArea.addEventListener("focus", () => {
            textArea.style.opacity = "0";
            setTimeout(() => (textArea.style.opacity = "1"));
        });
    }, []);

    useSnipOverlay(
        activeSnip instanceof TextSnip ? activeSnip : undefined,
        0,
        false,
    );
    useEffect(() => {
        if (
            !areaRef.current ||
            !activeSnip ||
            !(activeSnip instanceof TextSnip)
        )
            return;

        // Set styles
        const styles = getStylesInViewCoords(placePageIdx, textStyles);
        areaRef.current.style.fontFamily = styles.fontFamily;
        areaRef.current.style.fontSize =
            Math.round(styles.fontSize * 1000) / 1000 + "px";
        areaRef.current.style.lineHeight = styles.lineHeight + "px";

        // We also need to adjust the top value otherwise the text does not
        // align 100% as the baselines are computed differently in css
        // and in canvas

        // Overlay rect
        bindOverlayToRect({
            x: activeSnip.x,
            y: activeSnip.y,
            width: activeSnip.lineWrap,
            height: Math.max(
                activeSnip.canvasFont.lineHeight,
                activeSnip.height,
            ),
        });
        // Linewrap needs to be handeled manually as this is not a
        // css feature
        //areaRef.current.style.lin = styles.lineWrap + "px";
        areaRef.current.setSelectionRange(cursor.beforeStart, cursor.beforeEnd);
    }, [
        activeSnip,
        bindOverlayToRect,
        cursor.beforeEnd,
        cursor.beforeStart,
        getStylesInViewCoords,
        placePageIdx,
        text,
        textStyles,
    ]);

    return (
        <>
            <Overlay>
                <OverlayOutline>
                    <PlaceTextButton />
                    <OverlayMover
                        onDragMove={(_e, _offset, movement) => {
                            move(
                                movement![0]! * window.devicePixelRatio,
                                movement![1]! * window.devicePixelRatio,
                                "view",
                            );
                        }}
                    />
                    <DragMove
                        className={styles.resizerWrap}
                        onDragMove={(e) => {
                            const mov = transformRelativeCoords(
                                placePageIdx,
                                [e.movementX, e.movementY],
                                "page",
                            );

                            // Adjust wrapping
                            setTextStyles((prev) => {
                                return { lineWrap: prev.lineWrap + mov[0] };
                            });
                        }}
                    />
                </OverlayOutline>
                <OverlayContent>
                    <textarea
                        ref={areaRef}
                        className={styles.textarea}
                        onInput={(
                            e: React.ChangeEvent<HTMLTextAreaElement>,
                        ) => {
                            const start = e.target.selectionStart;
                            const end = e.target.selectionEnd;
                            setText(e.target.value);
                            setCursor({
                                beforeStart: start,
                                beforeEnd: end,
                            });
                        }}
                    />
                </OverlayContent>
            </Overlay>
            <KeyHandler />
        </>
    );
}

function PlaceTextButton() {
    const { placeOnPage } = usePlacementContext();

    const { setActiveTool } = useToolbarContext();
    const { activeTextSnip } = useTextToolContext();

    // TODO: somehow link with normal place tool
    // it is a bit inconsistent
    return (
        <OverlayPlaceButton
            onPlace={async () => {
                if (!activeTextSnip) return;
                // Only place if text is not empty (only contains spaces)
                if (
                    activeTextSnip.text === "" ||
                    activeTextSnip.text.trim() === ""
                )
                    return;

                await placeOnPage();
                setActiveTool("interaction");
            }}
        />
    );
}

function TextHelp() {
    return (
        <Help>
            <HelpHeader>
                <span>Text</span>
                <BsFonts style={{ stroke: "none" }} />
            </HelpHeader>
            <HelpDescription>
                The text tool allows you to add text to the page. It is
                relatively simple, but allows for some basic formatting options.
                Placement has to be confirmed!
            </HelpDescription>
            <HelpShortcuts
                shortcuts={[
                    ["t", "Toggle tool"],
                    ["o", "Font size down"],
                    ["i", "Font size up"],
                ]}
            />
        </Help>
    );
}

const TextTool: ToolComponents = {
    name: "text" as const,
    Toggle: TextToggle,
    Submenu: TextSubMenu,
    Help: TextHelp,
    Overlay: TextOverlay,
    ContextProvider: TextToolContextProvider,
};

export default TextTool;
