"use client";

import useDevicePixelRatio from "lib/hooks/useDevicePixelRatio";
import {
    createContext,
    ReactNode,
    useCallback,
    useContext,
    useMemo,
} from "react";

import { BaseSnip } from "@snip/snips/general/base";

import { useEditorContext } from "../context";
import { Rect, ToolComponents } from "./types";

import { useQueuedSnipsContext } from "components/context/socket/useQueuedSnipsContext";
import { useWorkerContext } from "components/context/viewer/useWorkerContext";

type CoordUnit = "page" | "view";

interface ToolsContextI {
    // Currently active snip
    // if one is active
    activeSnip: BaseSnip | null;

    // Page that should be used for current
    // edit/place operations
    // Not really used but hopefully
    // useful in the future
    placePageIdx: number;

    /**
     * Converts relative measures such as width and height into another coordinate system.
     *
     * @param page_idx - The index of the page.
     * @param from - The relative measures to transform, specified in the current coordinate system.
     * @param to_unit - The target coordinate system to transform the relative measures to. Defaults to the current coordinate system if not specified.
     *
     * @returns The transformed relative measures in the target coordinate system.
     */
    transformRelativeCoords: (
        page_idx: number,
        from: [number, number],
        to_unit?: CoordUnit,
        apply_pixel_ratio?: boolean,
    ) => [number, number];

    /**
     * Transforms the position vectors from one coordinate system to another.
     *
     * @param page_idx - The index of the page.
     * @param from - The position vector to transform, specified in the current coordinate system.
     * @param to_unit - The target coordinate system to transform the position vector to. Defaults to the current coordinate system if not specified.
     *
     * @returns The transformed position vector in the target coordinate system.
     */
    transformPosCoords: (
        page_idx: number,
        from: [number, number],
        to_unit?: CoordUnit,
        apply_pixel_ratio?: boolean,
    ) => [number, number];

    /**
     * Transforms the coordinates of a rectangle from one coordinate system to another.
     *
     * @param page_idx - The index of the page.
     * @param from - The rectangle to transform, specified in the current coordinate system.
     * @param to_unit - The target coordinate system to transform the rectangle to. Defaults to the current coordinate system if not specified.
     *
     * @returns The transformed rectangle in the target coordinate system.
     */
    transformRectCoords: (
        page_idx: number,
        from: Rect,
        to_unit?: CoordUnit,
    ) => Rect;
}

const ToolContext = createContext<ToolsContextI | null>(null);

/**
 * Provides the context for the a given
 * set of tools.
 *
 * For the tool specific context providers, see
 * the tool specific folder. E.g. `text`.
 *
 * @param contextProviders - An array of tool context providers.
 * @param children - The child components to render.
 *
 * @returns The context providers wrapped around the child components.
 */
export function ToolsContextProvider({
    contextProviders,
    children,
}: {
    contextProviders: ToolComponents["ContextProvider"][];
    children: ReactNode;
}) {
    // Create inner contexts
    const inner = useMemo(() => {
        return contextProviders.toReversed().reduce((acc, Provider, i) => {
            if (!Provider) {
                return acc;
            }
            return <Provider key={i}>{acc}</Provider>;
        }, children);
    }, [children, contextProviders]);

    const { workers } = useWorkerContext();
    const { snips } = useQueuedSnipsContext();
    const { activeSnipId, scale } = useEditorContext();

    // pixel ratio
    const pr = useDevicePixelRatio();

    // Get active snip
    const activeSnip: BaseSnip | null = useMemo(() => {
        if (!activeSnipId) return null;
        return snips.get(activeSnipId) ?? null;
    }, [activeSnipId, snips]);

    const transformRelativeCoords = useCallback(
        (
            page_idx: number,
            from: [number, number],
            to_units: "page" | "view" = "page",
            apply_pixel_ratio = false,
        ) => {
            // Small fix to force updates on scale change
            scale;
            pr;

            const viewport = workers?.get(page_idx)?.main.viewport;

            let ret;
            switch (to_units) {
                case "page":
                    if (apply_pixel_ratio) {
                        from[0] = from[0] * pr;
                        from[1] = from[1] * pr;
                    }
                    return viewport!.getRelativeInPageCoords(from);
                case "view":
                    ret = viewport!.getRelativeInViewCoords(from);
                    if (apply_pixel_ratio) {
                        ret[0] /= pr;
                        ret[1] /= pr;
                    }
                    return ret;
            }
            return assertUnreachable(to_units);
        },
        [pr, scale, workers],
    );

    const transformPosCoords = useCallback(
        (
            page_idx: number,
            from: [number, number],
            to_units: "page" | "view" = "page",
            apply_pixel_ratio = false,
        ) => {
            // Small fix to force updates on scale change
            scale;
            pr;

            const viewport = workers?.get(page_idx)?.main.viewport;

            let ret;
            switch (to_units) {
                case "page":
                    if (apply_pixel_ratio) {
                        from[0] = from[0] * pr;
                        from[1] = from[1] * pr;
                    }
                    return viewport!.getPosInPageCoords(from);
                case "view":
                    ret = viewport!.getPosInViewCoords(from);
                    if (apply_pixel_ratio) {
                        ret[0] /= pr;
                        ret[1] /= pr;
                    }
                    return ret;
            }
            return assertUnreachable(to_units);
        },
        [pr, scale, workers],
    );

    const transformRectCoords = useCallback(
        (page_idx: number, from: Rect, to_units: "page" | "view" = "page") => {
            // Small fix to force updates on scale change

            const xy = transformPosCoords(page_idx, [from.x, from.y], to_units);
            const wh = transformRelativeCoords(
                page_idx,
                [from.width, from.height],
                to_units,
            );

            return {
                x: xy[0],
                y: xy[1],
                width: wh[0],
                height: wh[1],
            };
        },
        [transformPosCoords, transformRelativeCoords],
    );

    return (
        <ToolContext.Provider
            value={{
                transformPosCoords,
                transformRelativeCoords,
                transformRectCoords,
                activeSnip,
                placePageIdx: 0,
            }}
        >
            {inner}
        </ToolContext.Provider>
    );
}

export function useToolsContext() {
    const context = useContext(ToolContext);
    if (!context) throw new Error("Cant use ToolContext outside provider!");
    return context;
}

function assertUnreachable(x: never): never {
    throw new Error("Didn't expect to get here");
}
