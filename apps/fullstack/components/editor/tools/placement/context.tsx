import { createContext, useCallback, useContext, useMemo } from "react";

import { BaseSnip } from "@snip/snips/general/base";

import { useToolsContext } from "../context";
import { Rect, ToolContextProps } from "../types";

import { useQueuedSnipsContext } from "components/context/socket/useQueuedSnipsContext";
import { useWorkerContext } from "components/context/viewer/useWorkerContext";
import { useEditorContext } from "components/editor/context";

type units = "view" | "page";

export interface PlacementContextI {
    setPos: (x: number, y: number, units?: units) => void;
    move: (movX: number, movY: number, units?: units) => void;
    resize: (width: number, height: number, units?: units) => void;

    activeSnip?: BaseSnip;
    posDB: [number, number];
    setRot: (rot: number, mirror?: boolean) => void;
    align: (args: {
        horizontal?: "left" | "center" | "right";
        vertical?: "top" | "center" | "bottom";
    }) => void;
    placeOnPage: () => Promise<void>;
    bindOverlayToRect: (rect: Rect) => void;
}

const PlacementContext = createContext<PlacementContextI | null>(null);

export function PlacementContextProvider({ children }: ToolContextProps) {
    const { overlayRect, setOverlayRect, activeSnipId, setActiveSnipId } =
        useEditorContext();
    const { transformPosCoords, transformRelativeCoords, placePageIdx } =
        useToolsContext();
    const { snips, removeQueuedSnip } = useQueuedSnipsContext();
    const { workers } = useWorkerContext();

    const activeSnip: BaseSnip | undefined = useMemo(() => {
        if (!activeSnipId) return;
        return snips.get(activeSnipId);
    }, [activeSnipId, snips]);

    /** Update the position of the
     * currently active snip
     *
     * This also updates the overlay rect.
     *
     * x,y in dom units
     */
    const setPos = useCallback(
        (x: number, y: number, units: "view" | "page" = "view") => {
            const oWorker = workers.get(placePageIdx)?.overlay;
            if (!oWorker || !activeSnip) return;
            setOverlayRect((prev) => {
                // Get coords in different systems
                const pageXY: [number, number] =
                    units === "page"
                        ? [x, y]
                        : transformPosCoords(placePageIdx, [x, y], "page");
                const viewXY: [number, number] =
                    units === "view"
                        ? [x, y]
                        : transformPosCoords(placePageIdx, [x, y], "view");

                //Update snip
                activeSnip.x = pageXY[0];
                activeSnip.y = pageXY[1];
                oWorker.rerender();

                // Update overlay rect
                return { ...prev, x: viewXY[0], y: viewXY[1] };
            });
        },
        [workers, placePageIdx, activeSnip, setOverlayRect, transformPosCoords],
    );

    const move = useCallback(
        (movX: number, movY: number, units: "view" | "page" = "view") => {
            const oWorker = workers.get(placePageIdx)?.overlay;
            if (!oWorker || !activeSnip) return;

            setOverlayRect((prev) => {
                // Check min/max bounds
                // Update activeSnip
                if (units === "page") {
                    [movX, movY] = transformRelativeCoords(
                        placePageIdx,
                        [movX, movY],
                        "view",
                    );
                }

                // Update in dom/view units
                const new_x = prev.x + movX;
                const new_y = prev.y + movY;

                const xy = transformPosCoords(
                    placePageIdx,
                    [new_x, new_y],
                    "page",
                );
                activeSnip.x = xy[0];
                activeSnip.y = xy[1];
                oWorker.rerender();

                return {
                    ...prev,
                    x: new_x,
                    y: new_y,
                };
            });
        },
        [
            activeSnip,
            placePageIdx,
            setOverlayRect,
            transformPosCoords,
            transformRelativeCoords,
            workers,
        ],
    );

    const resize = useCallback(
        (width: number, height: number, units: "view" | "page" = "view") => {
            const oWorker = workers.get(placePageIdx)?.overlay;
            if (!oWorker || !activeSnip) return;

            setOverlayRect((prev) => {
                let pageWH: [number, number];
                let viewWH: [number, number];
                if (units === "page") {
                    viewWH = transformRelativeCoords(
                        placePageIdx,
                        [width, height],
                        "view",
                    );
                    pageWH = [width, height];
                } else {
                    pageWH = transformRelativeCoords(
                        placePageIdx,
                        [width, height],
                        "page",
                    );
                    viewWH = [width, height];
                }

                activeSnip.width = pageWH[0];
                activeSnip.height = pageWH[1];
                oWorker.rerender();

                return {
                    ...prev,
                    width: viewWH[0],
                    height: viewWH[1],
                };
            });
        },
        [
            activeSnip,
            placePageIdx,
            setOverlayRect,
            transformRelativeCoords,
            workers,
        ],
    );

    /** Update the rotation
     * and mirror of the currently
     * active snip
     *
     * This also updates the overlay rect
     */
    const setRot = useCallback(
        (rot: number, mirror?: boolean) => {
            const oWorker = workers.get(placePageIdx)?.overlay;
            if (!oWorker || !activeSnip) return;

            setOverlayRect((prev) => {
                // Update activeSnip
                activeSnip.rot = rot;
                activeSnip.mirror = mirror ?? false;
                oWorker.rerender();
                // Update overlay rect
                return { ...prev, rot, mirror };
            });
        },
        [activeSnip, placePageIdx, setOverlayRect, workers],
    );

    const posDB = useMemo(() => {
        const oWorker = workers.get(placePageIdx)?.overlay;
        if (!oWorker) return [NaN, NaN] as [number, number];

        const xy = transformPosCoords(
            placePageIdx,
            [overlayRect.x, overlayRect.y],
            "page",
        );
        return xy;
    }, [
        workers,
        placePageIdx,
        transformPosCoords,
        overlayRect.x,
        overlayRect.y,
    ]);

    const align = useCallback(
        ({
            horizontal = "none",
            vertical = "none",
        }: {
            horizontal?: "left" | "center" | "right" | "none";
            vertical?: "top" | "center" | "bottom" | "none";
        }) => {
            const oWorker = workers.get(placePageIdx)?.overlay;
            if (!oWorker || !activeSnip) return;

            const padding = 10;
            const pageSize = oWorker.viewport.getPageSize();
            const snipSize = {
                width: activeSnip.width,
                height: activeSnip.height,
            };

            let x = activeSnip.x;
            let y = activeSnip.y;
            switch (horizontal) {
                case "left":
                    x = padding;
                    break;
                case "right":
                    x = pageSize.width - snipSize.width - padding;
                    break;
                case "center":
                    x = (pageSize.width - snipSize.width) / 2;
                    break;
            }

            switch (vertical) {
                case "top":
                    y = padding;
                    break;
                case "bottom":
                    y = pageSize.height - snipSize.height - padding;
                    break;
                case "center":
                    y = (pageSize.height - snipSize.height) / 2;
                    break;
            }

            setPos(x, y, "page");
        },
        [activeSnip, placePageIdx, setPos, workers],
    );

    const bindOverlayToRect = useCallback(
        (rect: Rect, units: "page" | "view" = "page") => {
            const oWorker = workers.get(placePageIdx)?.overlay;
            if (!oWorker || !activeSnip) return;

            // Adjust overlay to width and height
            if (units === "page") {
                console.log("Bind to rect", rect, oWorker.viewport);
                const wh = transformRelativeCoords(
                    placePageIdx,
                    [rect.width, rect.height],
                    "view",
                    true,
                );
                const xy = transformPosCoords(
                    placePageIdx,
                    [rect.x, rect.y],
                    "view",
                );
                console.log("Bind to rect", xy, wh);

                setOverlayRect(() => {
                    return {
                        x: xy[0],
                        y: xy[1],
                        width: wh[0],
                        height: wh[1],
                        rot: rect.rot,
                        mirror: rect.mirror,
                    };
                });
            } else {
                setOverlayRect(() => {
                    return rect;
                });
            }
        },
        [
            activeSnip,
            placePageIdx,
            setOverlayRect,
            transformPosCoords,
            transformRelativeCoords,
            workers,
        ],
    );

    /** Places the currently
     * active snip on the page
     * using the worker.
     *
     * I.e. moves the snip from the overlay
     * to the page.
     */
    async function placeOnPage() {
        if (!activeSnip) return;

        const w = workers.get(placePageIdx);
        await w?.main.placeOnPage(activeSnip.to_data());

        // Already remove the snip from the overlay
        // this does not wait for the socket to confirm
        removeQueuedSnip(activeSnip.id);
        setActiveSnipId(null);
    }

    return (
        <PlacementContext.Provider
            value={{
                activeSnip,
                posDB,
                setPos,
                setRot,
                align,
                move,
                placeOnPage,
                bindOverlayToRect,
                resize,
            }}
        >
            {children}
        </PlacementContext.Provider>
    );
}

export function usePlacementContext() {
    const context = useContext(PlacementContext);
    if (!context)
        throw new Error("Cant use PlacementContext outside provider!");
    return context;
}
