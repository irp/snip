import { HTMLProps } from "react";

const Icon = (props: HTMLProps<SVGSVGElement>) => {
    return (
        <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
            className="icon icon-tabler icons-tabler-outline icon-tabler-drag-drop"
            version="1.1"
            id="svg22"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <defs id="defs26" />
            <path stroke="none" d="M0 0h24v24H0z" fill="none" id="path2" />
            <g id="g1005">
                <path
                    d="M 18,10 V 8 A 2,2 0 0 0 16,6 H 8 A 2,2 0 0 0 6,8 v 8 a 2,2 0 0 0 2,2 h 2"
                    id="path4"
                />
                <path
                    d="m 12.009272,11.963396 9,3 -4,2 -2,4 -3,-9"
                    id="path6"
                />
            </g>
        </svg>
    );
};

Icon.displayName = "Icon";

export default Icon;
