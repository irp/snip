import DragMove from "../common/drag";
import { Help, HelpDescription, HelpHeader } from "../common/help";
import { useKeyPress } from "../common/keypress";
import {
    Overlay,
    OverlayContent,
    OverlayOutline,
    OverlayPlaceButton,
    useSnipOverlay,
} from "../common/overlay";
import { Toggle } from "../common/toggle";
import { ToolComponents, ToolToggleProps } from "../types";
import { PlacementContextProvider, usePlacementContext } from "./context";
import Icon from "./icon";
import PlacementSubMenu from "./submenu";

import stylesCommon from "../common/toggle.module.scss";

function PlacementToggle(props: ToolToggleProps) {
    const { activeSnip } = usePlacementContext();

    // this tool only works with an active image snip
    if (!activeSnip || activeSnip.width < 1 || activeSnip.height < 1) {
        return null;
    }
    return (
        <Toggle
            label="Placement"
            icon={<Icon className={stylesCommon.icon} />}
            {...props}
        />
    );
}

function PlacementHelp() {
    return (
        <Help>
            <HelpHeader>
                <span>Placement</span>
                <Icon
                    className={stylesCommon.icon}
                    style={{
                        fill: "transparent",
                    }}
                />
            </HelpHeader>
            <HelpDescription>
                <p>
                    Place, move, rotate, and mirror the currently selected
                    snippet. Use the <kbd>Place</kbd> button to permanently
                    place the snippet on the current page. Use the arrow keys{" "}
                    <kbd>↑</kbd> <kbd>↓</kbd> <kbd>→</kbd> <kbd>←</kbd> to move
                    the snippet. Hold <kbd>Shift</kbd> for smaller increments or{" "}
                    <kbd>Ctrl</kbd> for larger increments.
                </p>
            </HelpDescription>
        </Help>
    );
}

/** The overlay renders
 * a wrapper around the currently
 * selected snip. And allow to move
 * and rotate the snip.
 */
function PlacementOverlay() {
    const { activeSnip, move, placeOnPage } = usePlacementContext();

    useSnipOverlay(activeSnip, 0);

    useKeyPress((e, map) => {
        const tra: [number, number] = [0, 0];
        if (map.has("ArrowUp")) {
            tra[1]! -= 1;
        }
        if (map.has("ArrowDown")) {
            tra[1]! += 1;
        }
        if (map.has("ArrowLeft")) {
            tra[0]! -= 1;
        }
        if (map.has("ArrowRight")) {
            tra[0]! += 1;
        }

        // Default multiplier
        if (!e.ctrlKey) {
            tra[0]! *= 4;
            tra[1]! *= 4;
        }

        // Shift moves faster
        if (e.shiftKey) {
            tra[0]! *= 10;
            tra[1]! *= 10;
        }

        if (tra[0] == 0 && tra[1] == 0) {
            return;
        }

        move(tra[0], tra[1], "page");
    });

    return (
        <Overlay>
            <OverlayOutline>
                <OverlayPlaceButton onPlace={placeOnPage} />
            </OverlayOutline>
            <OverlayContent>
                <DragMove
                    className="w-100 h-100 position-absolute"
                    onDragMove={(_e, _o, m) => {
                        move(
                            m[0] * window.devicePixelRatio,
                            m[1] * window.devicePixelRatio,
                        );
                    }}
                ></DragMove>
            </OverlayContent>
        </Overlay>
    );
}

const PlacementTool: ToolComponents = {
    name: "placement" as const,
    Toggle: PlacementToggle,
    Help: PlacementHelp,
    Submenu: PlacementSubMenu,
    Overlay: PlacementOverlay,
    ContextProvider: PlacementContextProvider,
};

export default PlacementTool;
