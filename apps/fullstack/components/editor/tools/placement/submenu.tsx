import { useEffect } from "react";
import { Button, ButtonGroup } from "react-bootstrap";
import {
    TbFlipHorizontal,
    TbFlipVertical,
    TbLayoutAlignBottom,
    TbLayoutAlignCenter,
    TbLayoutAlignLeft,
    TbLayoutAlignMiddle,
    TbLayoutAlignRight,
    TbLayoutAlignTop,
    TbRotate2,
    TbRotateClockwise2,
} from "react-icons/tb";

import { ItemSmall, Submenu } from "../common/submenu";
import { ToolSubmenuProps } from "../types";
import { usePlacementContext } from "./context";

import { Input, InputProps } from "components/form/inputs/base";
import { JoinedNumbersInput } from "components/form/inputs/number";

import styles from "./placement.module.scss";

export default function PlacementSubMenu(props: ToolSubmenuProps) {
    const { activeSnip, setRot, posDB, setPos, align } = usePlacementContext();

    useEffect(() => {
        const x = document.getElementById(
            "x_input_placement",
        ) as HTMLInputElement;
        if (x) {
            x.value = Math.round(posDB[0]).toString();
        }
        const y = document.getElementById(
            "y_input_placement",
        ) as HTMLInputElement;
        if (y) {
            y.value = Math.round(posDB[1]).toString();
        }
    }, [posDB]);

    return (
        <Submenu {...props} className={styles.submenu}>
            <ItemSmall className={styles.place}>
                <JoinedNumbersInput
                    label="Position"
                    names={["x_input_placement", "y_input_placement"]}
                    onChange={(x, y) => {
                        if (!x) x = posDB[0];
                        if (!y) y = posDB[1];
                        setPos(x, y, "page");
                    }}
                    min={0}
                    max={10000}
                    className={styles.inputs}
                    onReset={() => {
                        align({
                            horizontal: "center",
                            vertical: "center",
                        });
                    }}
                />
            </ItemSmall>
            <ItemSmall className={styles.rotate}>
                <RotateAndMirrorButtons
                    className={styles.buttons}
                    onChangeRot={(change) => {
                        setRot(activeSnip!.rot + change);
                    }}
                    onChangeMirror={(mirror) => {
                        if (mirror === 1) {
                            setRot(activeSnip!.rot, !activeSnip!.mirror);
                        }
                        if (mirror === 2) {
                            setRot(activeSnip!.rot + 180, !activeSnip!.mirror);
                        }
                    }}
                    onReset={() => {
                        setRot(0, false);
                    }}
                ></RotateAndMirrorButtons>
            </ItemSmall>
            <ItemSmall className={styles.align}>
                <AlignButtons
                    name="align"
                    label="Alignment"
                    type="none"
                    as={"div"}
                    className={styles.buttons}
                />
            </ItemSmall>
        </Submenu>
    );
}

type RotateProps = {
    onChangeRot: (rot: number) => void;
    onChangeMirror: (mirror: 1 | 2) => void;
} & InputProps;

function RotateAndMirrorButtons({
    onChangeRot,
    onChangeMirror,
    ...props
}: RotateProps) {
    return (
        <Input label="Rotate & Mirror" type="none" as="div" {...props}>
            <ButtonGroup>
                <Button variant="none" onClick={() => onChangeRot(-90)}>
                    <TbRotate2 />
                </Button>
                <Button variant="none" onClick={() => onChangeRot(90)}>
                    <TbRotateClockwise2 />
                </Button>
            </ButtonGroup>
            <ButtonGroup>
                <Button variant="none" onClick={() => onChangeMirror(1)}>
                    <TbFlipVertical />
                </Button>
                <Button variant="none" onClick={() => onChangeMirror(2)}>
                    <TbFlipHorizontal />
                </Button>
            </ButtonGroup>
        </Input>
    );
}

function AlignButtons(props: InputProps) {
    const { align } = usePlacementContext();

    return (
        <Input {...props}>
            <ButtonGroup>
                <Button
                    variant="none"
                    onClick={() => align({ horizontal: "left" })}
                >
                    <TbLayoutAlignLeft />
                </Button>
                <Button
                    variant="none"
                    onClick={() => align({ horizontal: "right" })}
                >
                    <TbLayoutAlignRight />
                </Button>
                <Button
                    variant="none"
                    onClick={() => align({ vertical: "top" })}
                >
                    <TbLayoutAlignTop />
                </Button>
                <Button
                    variant="none"
                    onClick={() => align({ vertical: "bottom" })}
                >
                    <TbLayoutAlignBottom />
                </Button>
                <Button
                    variant="none"
                    onClick={() => align({ horizontal: "center" })}
                >
                    <TbLayoutAlignCenter />
                </Button>
                <Button
                    variant="none"
                    onClick={() => align({ vertical: "center" })}
                >
                    <TbLayoutAlignMiddle />
                </Button>
            </ButtonGroup>
        </Input>
    );
}
