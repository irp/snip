export type Tool =
    | "interaction"
    | "movePage"
    | "doodle"
    | "snipEditor"
    | "text"
    | "image"
    | "placement"
    | "eraser";

export interface ToolToggleProps {
    isActive: boolean;
    setActive: () => void;
    isExpanded: boolean;
    setExpanded: () => void;
}

export interface ToolSubmenuProps {
    isExpanded: boolean;
    setExpanded: (expanded: boolean) => void;
}

export interface Rect {
    x: number;
    y: number;
    width: number;
    height: number;
    rot?: number;
    mirror?: boolean;
}

/** Props for the context */
export interface ToolContextProps {
    children: React.ReactNode;
}

export type ToolComponents = {
    name: Tool;
    Toggle: React.ComponentType<ToolToggleProps>;
    Help: React.ComponentType;
    Submenu?: React.ComponentType<ToolSubmenuProps>;

    /** An overlay allows to render any
     * additional information if a tool is active.
     * E.g. for text
     *
     * Can only be shown on a single page
     * at the time.
     */
    Overlay?: React.ComponentType;
    ContextProvider?: React.ComponentType<ToolContextProps>;
};
