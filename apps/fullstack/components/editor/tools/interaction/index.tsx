import { useEffect, useState } from "react";
import { TbPointer, TbPointerFilled } from "react-icons/tb";

import { get_snip_from_data_allow_unknown } from "@snip/snips";
import { BaseSnip } from "@snip/snips/general/base";

import { Help, HelpDescription, HelpHeader } from "../common/help";
import { useInteractions } from "../common/interactions";
import { Toggle } from "../common/toggle";
import { ToolComponents, ToolToggleProps } from "../types";
import SnipToDom from "./snipToDom";

import { useWorkerContext } from "components/context/viewer/useWorkerContext";
import { type InteractionEvent } from "components/editor/worker/pageWorker";

function InteractionToggle(props: ToolToggleProps) {
    const icon = props.isActive ? (
        <TbPointerFilled
            style={{
                stroke: "none",
            }}
        />
    ) : (
        <TbPointer />
    );

    return (
        <Toggle label="Interact" icon={icon} expandable={false} {...props} />
    );
}

function InteractionHelp() {
    return (
        <Help>
            <HelpHeader>
                <span>Interaction</span>
                <TbPointer />
            </HelpHeader>
            <HelpDescription>
                <p>
                    Using this tool you may interact with the page. You may
                    select text, follow links or interact with interactive
                    snips.
                </p>
            </HelpDescription>
        </Help>
    );
}

/** The interaction overlay functions very differently to other
 * overlays!
 *
 * Instead of being used to place a snip on the page. It is used to
 * create html elements for text snips or other interactive components
 * on the page.
 *
 * Basically whenever a user clicks or hovers a snip the overlay will
 * create a html element for the snip and display it on the page.
 * This allows the user to interact with the snip in a more natural way.
 *
 */
function InteractionOverlay() {
    const { workers } = useWorkerContext();
    const [snip, setSnip] = useState<BaseSnip | null>(null);

    /** Setup worker interaction
     * events when the overlay is mounted
     * remove when unmounted
     */
    useInteractions();

    /** Register enter and leave events and
     * create elements for text snips
     */
    useEffect(() => {
        function enterSnip(e: InteractionEvent) {
            console.log(e);
            //TODO: Rework communication protocol
            // for interaction snip data
            if (e.snipData.type == "image") return;

            const snip = get_snip_from_data_allow_unknown(e.snipData);

            console.log("snip", snip);
            setSnip(snip);
        }

        const w = workers.get(0);
        if (!w) return;

        w.main.interactions.addEventListener("pointerenter", enterSnip);
        w.main.interactions.addEventListener("pointerdown", enterSnip);

        return () => {
            w.main.interactions.removeEventListener("pointerenter", enterSnip);
            w.main.interactions.removeEventListener("pointerdown", enterSnip);
        };
    }, [workers]);

    if (!snip) return <></>;

    return <SnipToDom snip={snip} />;
}

const InteractionTool: ToolComponents = {
    name: "interaction" as const,
    Toggle: InteractionToggle,
    Help: InteractionHelp,
    Overlay: InteractionOverlay,
};

export default InteractionTool;
