import { RefObject, useEffect, useRef, useState } from "react";

import { ArraySnip } from "@snip/snips/general/array";
import { BaseSnip } from "@snip/snips/general/base";
import { TextSnipLegacy } from "@snip/snips/general/legacy/text";
import { LinkSnip } from "@snip/snips/general/link";
import { TextSnip } from "@snip/snips/general/text";

import { useToolsContext } from "../context";

import styles from "./interaction.module.scss";

export default function SnipToDom({
    snip,
    offset,
}: {
    snip: BaseSnip;
    offset?: [number, number];
}) {
    if (snip instanceof TextSnip || snip instanceof TextSnipLegacy) {
        return <TextSnipAsFC snip={snip} offset={offset} />;
    }
    if (snip instanceof ArraySnip) {
        return <ArraySnipAsFC snip={snip} offset={offset} />;
    }
    if (snip instanceof LinkSnip) {
        return <LinkSnipAsFC snip={snip} offset={offset} />;
    }

    // TODO: add overlays for other snips
    // add right click menu for snips to view some information
    return null;
}

function ArraySnipAsFC({
    snip,
    offset = [0, 0],
}: {
    snip: ArraySnip;
    offset?: [number, number];
}) {
    let legacy_offset = [0, 0];
    const childs = snip.snips.map((s, i) => {
        const o: [number, number] = [
            snip.x + legacy_offset[0]! + offset[0],
            snip.y + legacy_offset[1]! + offset[1],
        ];
        const c = <SnipToDom key={i} snip={s} offset={o} />;
        if (snip.legacy) {
            legacy_offset = [legacy_offset[0]!, legacy_offset[1]! + s.height];
        }
        return c;
    });

    return <div>{childs}</div>;
}

/** Generate a text snip as a functional component
 * which can be used in the overlay
 *
 * @param {Snip} snip - The snip to render
 * @param {number} offsetDB - The offset of the snip in the database y coord
 * @returns {React.FC} - The functional component
 */
function TextSnipAsFC({
    snip,
    offset,
}: {
    snip: TextSnip | TextSnipLegacy;
    offset?: [number, number];
}) {
    const [parsedText, setParsedText] = useState<string>();
    const ref = useRef<HTMLTextAreaElement>(null);

    useOverlay(ref, snip, offset);

    useEffect(() => {
        // Parse and set text
        let text = snip.text;
        text = text
            .replaceAll(/\t/g, "        ")
            .replaceAll(/\n/g, "\\n")
            .replaceAll(/\r/g, "")
            .replaceAll(/\\n/g, String.fromCharCode(13, 10));
        setParsedText(text);
    }, [snip]);

    return (
        <textarea
            ref={ref}
            className={styles.textSnip}
            value={parsedText}
            autoCorrect="off"
            wrap="off"
            readOnly
        />
    );
}

function LinkSnipAsFC({
    snip,
    offset,
}: {
    snip: LinkSnip;
    offset?: [number, number];
}) {
    const ref = useRef<HTMLAnchorElement>(null);

    useOverlay(ref, snip, offset);
    // If no icon is set the full snip is interpreted
    // as an link
    if (!snip.show_icon) {
        return (
            <a
                href={snip.href.href}
                target="_blank"
                className={styles.linkSnip}
            >
                <SnipToDom snip={snip.snip} offset={offset} />
            </a>
        );
    }

    return (
        <>
            <a
                ref={ref}
                href={snip.href.href}
                target="_blank"
                className={styles.linkSnip}
                style={{
                    position: "absolute",
                }}
            >
                {/*Link anchor if icon*/}
            </a>
            <SnipToDom snip={snip.snip} offset={offset} />
        </>
    );
}

/** Binds the position of the snip to the overlay ref
 * also sets rotation and mirror if set.
 */
function useOverlay(
    ref: RefObject<HTMLElement>,
    snip: BaseSnip,
    offset?: [number, number],
) {
    const { transformPosCoords, transformRelativeCoords } = useToolsContext();

    useEffect(() => {
        const ele = ref.current;
        if (!ele) return;

        // transform coords to view coords
        // TODO: page_idx != 0
        let xy = transformPosCoords(0, [snip.x, snip.y], "view", true);
        let wh = transformRelativeCoords(
            0,
            [snip.width, snip.height],
            "view",
            true,
        );

        if (offset) {
            const o = transformRelativeCoords(0, offset, "view", true);
            xy[0] += o[0];
            xy[1] += o[1];
        }

        const adjust: [number, number] = [0, 0];
        // Special parsing for text styles
        if (snip instanceof TextSnip) {
            const lHfS = transformRelativeCoords(
                0,
                [
                    snip.canvasFont.lineHeight * snip.canvasFont.fontSize, //line height
                    snip.canvasFont.fontSize, //font size
                ],
                "view",
                true,
            );

            ele.style.fontFamily = snip.canvasFont.fontFamily;
            ele.style.lineHeight = lHfS[0] + "px";
            ele.style.fontSize = lHfS[1] + "px";
            adjust[1] = -(lHfS[0] - lHfS[1]) / 2;
        }
        if (snip instanceof TextSnipLegacy) {
            const lHfS = transformRelativeCoords(
                0,
                [
                    snip.lineHeight * 2.35, //line height
                    snip.fontSize * 2.35, //font size
                ],
                "view",
                true,
            );
            ele.style.fontFamily = snip.fontFamily;
            ele.style.lineHeight = lHfS[0] + "px";
            ele.style.fontSize = lHfS[1] + "px";
            adjust[1] = -(lHfS[0] - lHfS[1]) / 2;
        }

        // Set coords for anchor
        if (snip instanceof LinkSnip) {
            const xy_icon = transformPosCoords(
                0,
                [snip.icon_x - 6, snip.icon_y - 12], // 6 = fontsize/2 // middle align
                "view",
                true,
            );
            const wh_icon = transformRelativeCoords(
                0,
                [snip.icon_width + 12, snip.icon_height + 12],
                "view",
                true,
            );
            xy = xy_icon;
            wh = wh_icon;
        }
        // Set positions
        ele.style.left = xy[0] + adjust[0] + "px";
        ele.style.top = xy[1] + adjust[1] + "px";
        ele.style.width = wh[0] + "px";
        ele.style.height = wh[1] + "px";
        // Set rotation and mirror
        ele.style.transform = `rotate(${snip.rot ?? 0}deg) ${
            snip.mirror ? "scaleX(-1)" : ""
        }`;
    }, [snip, transformPosCoords, ref, offset, transformRelativeCoords]);
}
