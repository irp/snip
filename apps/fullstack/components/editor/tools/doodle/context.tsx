"use client";
import { usePersistentState } from "lib/hooks/usePersistentState";
import {
    createContext,
    Dispatch,
    SetStateAction,
    useContext,
    useEffect,
    useRef,
    useState,
} from "react";

import { ToolContextProps } from "../types";

import { useEditorContext } from "components/editor/context";

export interface DoodleContextI {
    // The current tool
    colorList: string[];
    setColorList: (colorList: string[]) => void;

    // Current color
    currentColor: string;
    currentColorIdx: number;
    setCurrentColorIdx: Dispatch<SetStateAction<number>>;
    // Pen size
    size: number;
    setSize: (size: number) => void;

    // Pen smoothing
    smoothing: number;
    setSmoothing: (smoothing: number) => void;

    // Touch events as move
    touchMove: boolean;
    setTouchMove: (touchMove: boolean) => void;
}

const DoodleToolContext = createContext<DoodleContextI | null>(null);

export function DoodleToolContextProvider({ children }: ToolContextProps) {
    const mounted = useRef(false);
    const { userConfig } = useEditorContext();

    const [colorList, setColorList] = useState([
        "#000000",
        "#0000ff",
        "#ff0000",
        "#00ff00",
    ]);
    const [currentColorIdx, setCurrentColorIdx] = useState(0);

    const [size, setSize] = useState(5);
    const [smoothing, setSmoothing] = useState(0.5);

    const [touchMove, setTouchMove] = usePersistentState("touchMove", false);

    // Sync local state from user defaults
    useEffect(() => {
        if (mounted.current || !userConfig) return;
        mounted.current = true;

        setColorList((prev) => {
            if (userConfig.pen_colors) {
                return userConfig.pen_colors;
            }
            return prev;
        });
        setCurrentColorIdx((prev) => {
            if (userConfig.pen_colors && userConfig.pen_color) {
                const idx = userConfig.pen_colors.indexOf(userConfig.pen_color);
                if (idx !== -1) return idx;
            }
            return prev;
        });

        setSize((prev) => {
            if (userConfig.pen_size) {
                return userConfig.pen_size;
            }
            return prev;
        });

        setSmoothing((prev) => {
            if (userConfig.pen_smoothing) {
                return userConfig.pen_smoothing;
            }
            return prev;
        });

        return () => {
            mounted.current = false;
        };
    }, [userConfig]);

    return (
        <DoodleToolContext.Provider
            value={{
                colorList,
                setColorList,
                currentColor: colorList[currentColorIdx]!,
                currentColorIdx,
                setCurrentColorIdx,
                size,
                setSize,
                smoothing,
                setSmoothing,
                touchMove,
                setTouchMove,
            }}
        >
            {children}
        </DoodleToolContext.Provider>
    );
}

export function useDoodleToolContext() {
    const context = useContext(DoodleToolContext);
    if (!context)
        throw new Error("Cant use DoodleToolContext outside provider!");
    return context;
}
