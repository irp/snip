import { ChangeEvent, useEffect } from "react";

import {
    Help,
    HelpDescription,
    HelpHeader,
    HelpShortcuts,
} from "../common/help";
import { ItemSmall, Submenu } from "../common/submenu";
import { Toggle } from "../common/toggle";
import { MoveHandler } from "../move/moveHandler";
import { ToolSubmenuProps, ToolToggleProps } from "../types";
import { DoodleToolContextProvider, useDoodleToolContext } from "./context";
import { DrawHandler } from "./drawHandler";
import { PenEmpty, PenFilled } from "./svgs";

import { MultiColorInput } from "components/form/inputs/color";
import { JoinedRangeNumberInput } from "components/form/inputs/number";
import { CheckInput } from "components/form/inputs/option";

import stylesCommon from "../common/toggle.module.scss";
import styles from "./doodle.module.scss";

function DoodleToggle(props: ToolToggleProps) {
    const { currentColor } = useDoodleToolContext();

    const icon = props.isActive ? (
        <PenFilled
            className={stylesCommon.icon + " " + stylesCommon.noStroke}
        />
    ) : (
        <PenEmpty className={stylesCommon.icon + " " + stylesCommon.noStroke} />
    );

    return (
        <Toggle
            label="Doodle"
            style={{
                ["--pen-color-active" as string]: currentColor,
                ["--pen-color-inactive" as string]: currentColor + "b0",
            }}
            icon={icon}
            {...props}
        />
    );
}

function DoodleSubMenu(props: ToolSubmenuProps) {
    const {
        currentColorIdx,
        setCurrentColorIdx,
        colorList,
        setColorList,
        size,
        setSize,
        smoothing,
        setSmoothing,
        touchMove,
        setTouchMove,
    } = useDoodleToolContext();

    return (
        <Submenu {...props} className={styles.menu}>
            <ItemSmall className={styles.adjust}>
                <MultiColorInput
                    label="Pen color"
                    currentColorIdx={currentColorIdx}
                    setCurrentColorIdx={setCurrentColorIdx}
                    colorList={colorList}
                    setColorList={setColorList}
                />
            </ItemSmall>
            <ItemSmall className={styles.adjust}>
                <JoinedRangeNumberInput
                    className={styles.rangeInput}
                    label="Pen size"
                    name="size"
                    min={1}
                    max={25}
                    step={1}
                    value={size}
                    onInput={(e: ChangeEvent<HTMLInputElement>) =>
                        setSize(parseInt(e.target.value))
                    }
                    onReset={() => setSize(4)}
                />
            </ItemSmall>
            <ItemSmall className={styles.adjust}>
                <JoinedRangeNumberInput
                    className={styles.rangeInput}
                    label="Pen smoothing"
                    name="smoothing"
                    min={0}
                    max={1}
                    step={0.01}
                    value={smoothing}
                    onInput={(e: ChangeEvent<HTMLInputElement>) =>
                        setSmoothing(parseFloat(e.target.value))
                    }
                    onReset={() => setSmoothing(0.5)}
                />
            </ItemSmall>
            <ItemSmall className={styles.adjust}>
                <CheckInput
                    label="Touch inputs move canvas"
                    checked={touchMove}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setTouchMove(e.target.checked);
                    }}
                />
            </ItemSmall>
        </Submenu>
    );
}

function Overlay() {
    const { setCurrentColorIdx, colorList, touchMove } = useDoodleToolContext();

    useEffect(() => {
        //Register keybindings to change color
        const keydown = (e: KeyboardEvent) => {
            if (e.key === "i") {
                // Prev color
                e.preventDefault();
                setCurrentColorIdx(
                    (prev) => (prev - 1 + colorList.length) % colorList.length,
                );
            } else if (e.key === "o") {
                // Next color
                e.preventDefault();
                setCurrentColorIdx((prev) => (prev + 1) % colorList.length);
            }
        };
        document.addEventListener("keydown", keydown);

        return () => {
            document.removeEventListener("keydown", keydown);
        };
    }, [colorList.length, setCurrentColorIdx]);

    return (
        <>
            {touchMove && <MoveHandler pointerTypes={["touch"]} />}
            <DrawHandler />
        </>
    );
}

function DoodleHelp() {
    return (
        <Help>
            <HelpHeader>
                <span>Doodle</span>
                <PenFilled
                    style={{ ["--pen-color" as string]: "black" }}
                    className={stylesCommon.icon + " " + stylesCommon.noStroke}
                />
            </HelpHeader>
            <HelpDescription>
                <p>
                    You may use the doodle tool to create free form annotations.
                </p>
            </HelpDescription>
            <HelpShortcuts
                shortcuts={[
                    ["p", "Toggle tool"],
                    ["i", "Prev color"],
                    ["o", "Next color"],
                ]}
            />
        </Help>
    );
}

const DoodleTool = {
    name: "doodle" as const,
    Toggle: DoodleToggle,
    Help: DoodleHelp,
    Submenu: DoodleSubMenu,
    Overlay,
    ContextProvider: DoodleToolContextProvider,
};

export default DoodleTool;
