"use client";
import { useCallback, useEffect, useRef } from "react";

import { PageDataRet } from "@snip/database/types";
import { RenderContext } from "@snip/render/types";
import { DoodleSnip, DoodleSnipArgs } from "@snip/snips/general/doodle";

import { useDoodleToolContext } from "./context";

import { usePagesContext } from "components/context/socket/usePagesContext";
import { useWorkerContext } from "components/context/viewer/useWorkerContext";
import { useEditorContext } from "components/editor/context";
import { OverlayWorker } from "components/editor/worker/overlayWorker";

type DrawState = {
    idx?: number;
    pageDiv?: HTMLDivElement;
    snip?: DoodleSnip;
    page?: PageDataRet;
};

/** This converts mouse down events
 * to lines drawn on the page.
 * I.e. doodle snips
 */
export function DrawHandler() {
    const { activePageIds, editorElementRef } = useEditorContext();
    const { pages } = usePagesContext();
    const { workers } = useWorkerContext();
    const { currentColor, smoothing, size, touchMove } = useDoodleToolContext();

    // We need to keep track of the current snip
    // to edit it in different event handlers
    const currentDrawState = useRef<DrawState>({});

    const render_doodle = useCallback((ctx: RenderContext) => {
        if (currentDrawState.current.snip) {
            currentDrawState.current.snip.render(ctx);
        }
    }, []);

    // Setup event listeners
    useEffect(() => {
        const pageElements: HTMLDivElement[] = [];
        editorElementRef.current.forEach((value) => {
            pageElements.push(value);
        });

        function pointerdownHandler(e: PointerEvent, idx: number) {
            e.stopImmediatePropagation();
            // Get all required values for drawing
            const pageDiv = editorElementRef.current.get(idx);
            const page = pages.get(activePageIds[idx]!);
            if (!pageDiv || !page) return;

            let snip;
            if (e.target === pageDiv) {
                const worker = workers.get(idx);
                if (!worker) return;
                snip = newDoodle(e, worker.overlay, {
                    page_id: page.id,
                    colour: currentColor,
                    lineWidth: size,
                    smoothing,
                    book_id: page.book_id,
                });
                worker.overlay.add(render_doodle);
            }

            // Set current state
            currentDrawState.current = {
                idx,
                snip,
                pageDiv,
                page,
            };
        }

        function pointerMoveHandler(e: PointerEvent) {
            e.stopImmediatePropagation();
            const { idx, pageDiv, snip, page } = currentDrawState.current;
            if (idx === undefined) return;
            const worker = workers.get(idx);
            if (!page || !worker) return;

            if (snip) {
                // Add edge to snip
                // If it is not on the page we interpolate the movement
                // To get the correct line
                const interpolate = !(e.target === pageDiv);
                addEdge(snip, e, worker.overlay, interpolate);
            } else {
                if (e.target === pageDiv) {
                    // We also add a previous edge to the snip i.e. interpolate
                    // the movement of the mouse
                    // this prevents the line from being cut off on the first
                    // mouse move event
                    const edges = [];

                    const edge = worker.overlay.canvasCoordsInPageCoords([
                        e.offsetX * window.devicePixelRatio,
                        e.offsetY * window.devicePixelRatio,
                    ]);
                    const prev = worker.overlay.canvasCoordsInPageCoords([
                        (e.offsetX - e.movementX) * window.devicePixelRatio,
                        (e.offsetY - e.movementY) * window.devicePixelRatio,
                    ]);
                    edges.push(prev);
                    edges.push(edge);

                    currentDrawState.current.snip = newDoodle(
                        e,
                        worker.overlay,
                        {
                            page_id: page.id,
                            colour: currentColor,
                            lineWidth: size,
                            smoothing,
                            book_id: page.book_id,
                        },
                        edges,
                    );
                    worker.overlay.add(render_doodle);
                }
            }
            worker.overlay.rerender();
        }

        function pointerUpHandler(e: PointerEvent) {
            e.stopImmediatePropagation();
            const { idx } = currentDrawState.current;
            if (idx === undefined) return;
            const w = workers?.get(idx);
            const doodle = currentDrawState.current.snip;
            document.removeEventListener("pointermove", pointerMoveHandler);
            document.removeEventListener("pointerup", pointerUpHandler);
            if (!w || !doodle) return;

            // Place snip and remove it from overlay
            w.main.insertSnip(doodle.to_data()).then(() => {
                w.overlay.remove(render_doodle);
                currentDrawState.current = {};
            });
        }

        /** Create handler for entry point
         * i.e. pointerdown
         */
        const handlers = pageElements.map((value, idx) => {
            // Styles
            value.style.cursor = "crosshair";
            // Handler
            return (e: PointerEvent) => {
                // Ignore if touchMove is set
                if (touchMove && e.pointerType === "touch") return;

                document.addEventListener("pointermove", pointerMoveHandler);
                document.addEventListener("pointerup", pointerUpHandler);
                pointerdownHandler(e, idx);
            };
        });

        // Add handlers to page elements
        handlers.forEach((handler, idx) => {
            pageElements[idx]!.addEventListener("pointerdown", handler);
        });

        return () => {
            handlers.forEach((handler, idx) => {
                document.removeEventListener(
                    "pointermove",
                    pointerMoveHandler,
                    true,
                );
                document.removeEventListener(
                    "pointerup",
                    pointerUpHandler,
                    true,
                );
                pageElements[idx]!.removeEventListener("pointerdown", handler);
                // Styles
                pageElements[idx]!.style.cursor = "default";
            });
        };
    }, [
        activePageIds,
        currentColor,
        editorElementRef,
        pages,
        render_doodle,
        size,
        smoothing,
        touchMove,
        workers,
    ]);

    return null;
}

function newDoodle(
    e: PointerEvent,
    worker: OverlayWorker,
    args: Omit<DoodleSnipArgs, "edges">,
    edges?: [number, number][],
) {
    if (!edges)
        edges = [
            worker.canvasCoordsInPageCoords([
                e.offsetX * window.devicePixelRatio,
                e.offsetY * window.devicePixelRatio,
            ]),
        ];

    const snip = new DoodleSnip({
        ...args,
        edges,
    });

    return snip;
}

function addEdge(
    snip: DoodleSnip,
    e: PointerEvent,
    worker: OverlayWorker,
    interpolate: boolean,
) {
    let edge;
    if (interpolate) {
        const prev = snip.get_edge(-1);
        const move = worker.canvasCoordsInPageCoords([
            e.movementX * window.devicePixelRatio,
            e.movementY * window.devicePixelRatio,
        ]);
        if (!prev) {
            return;
        }
        edge = [prev[0]! + move[0], prev[1]! + move[1]];
    } else {
        edge = worker.canvasCoordsInPageCoords([
            e.offsetX * window.devicePixelRatio,
            e.offsetY * window.devicePixelRatio,
        ]);
    }
    edge[0] = Math.round(edge[0]! * 100) / 100;
    edge[1] = Math.round(edge[1]! * 100) / 100;
    snip.add_edge(edge as [number, number]);
}
