import useDevicePixelRatio from "lib/hooks/useDevicePixelRatio";
import { ReactNode } from "react";

import DragMove from "../common/drag";
import { useKeyPress } from "../common/keypress";
import { OverlayHandles, OverlayOutline, Position } from "../common/overlay";
import { useImageEditorContext } from "./context";

import { useEditorContext } from "components/editor/context";

export function ResizeHandlerOverlayOutline({
    children,
}: {
    children?: ReactNode;
}) {
    const { setImgRect } = useImageEditorContext();
    function resizeHandles(e: MouseEvent, val: Position) {
        e.preventDefault();

        setImgRect((prev) => {
            const mY = e.movementY * window.devicePixelRatio;
            let mX = e.movementX * window.devicePixelRatio;

            // Modify the image resize logic when shift key is pressed
            // We want to keep the aspect ratio of the image
            if (!e.shiftKey) {
                const ar = prev.width / prev.height;
                mX = mY * ar;
                if (val === "bottom-left" || val === "top-right") {
                    mX = -mX;
                }
            }

            switch (val) {
                case "top-left": //f
                    return {
                        x: prev.x + mX,
                        y: prev.y + mY,
                        width: prev.width - mX,
                        height: prev.height - mY,
                    };
                case "top-right":
                    return {
                        x: prev.x,
                        y: prev.y + mY,
                        width: mX + prev.width,
                        height: prev.height - mY,
                    };
                case "bottom-left":
                    return {
                        x: prev.x + mX,
                        y: prev.y,
                        width: prev.width - mX,
                        height: mY + prev.height,
                    };
                case "bottom-right": //f
                    return {
                        x: prev.x,
                        y: prev.y,
                        width: prev.width + mX,
                        height: prev.height + mY,
                    };
                default:
                    return prev;
            }
        });
    }

    return (
        <OverlayOutline color="var(--bs-primary)" type="square">
            <OverlayHandles
                onMove={resizeHandles}
                positions={[
                    "top-left",
                    "top-right",
                    "bottom-left",
                    "bottom-right",
                ]}
            />
            {children}
        </OverlayOutline>
    );
}

export function KeyPressHandler() {
    const { setOverlayRect } = useEditorContext();
    useKeyPress((e, map) => {
        e.preventDefault();
        const move = [0, 0];
        if (map.has("ArrowUp")) {
            move[1]! -= 1;
        }
        if (map.has("ArrowDown")) {
            move[1]! += 1;
        }
        if (map.has("ArrowLeft")) {
            move[0]! -= 1;
        }
        if (map.has("ArrowRight")) {
            move[0]! += 1;
        }

        // Default multiplier
        if (!e.ctrlKey) {
            move[0]! *= 4;
            move[1]! *= 4;
        }

        // Shift moves faster
        if (e.shiftKey) {
            move[0]! *= 10;
            move[1]! *= 10;
        }

        if (move[0] == 0 && move[1] == 0) {
            return;
        }

        setOverlayRect((prev) => ({
            ...prev,
            x: prev.x + move[0]!,
            y: prev.y + move[1]!,
        }));
    });
    return null;
}

export function CutHandlersOverlayOutline({
    onDoubleClick,
    children,
}: {
    onDoubleClick: () => void;
    children?: ReactNode;
}) {
    const { subRect, setSubRect, imgRect } = useImageEditorContext();
    const pr = useDevicePixelRatio();

    function cutHandles(e: MouseEvent, val: Position) {
        e.preventDefault();
        /** We do boundary checks here because the direction
         * determines some boundary conditions
         */

        setSubRect((prev) => {
            const mX = e.movementX * window.devicePixelRatio;
            const mY = e.movementY * window.devicePixelRatio;
            const next = {
                ...prev,
            };
            switch (val) {
                case "left":
                    next.x = prev.x + mX;
                    next.width = prev.width - mX;
                    if (next.x < 0) {
                        next.x = 0;
                        next.width = prev.width;
                    }
                    if (next.width < 0) {
                        next.width = 0;
                        next.x = prev.x;
                    }

                    return next;
                case "top":
                    next.y = prev.y + mY;
                    next.height = prev.height - mY;
                    if (next.y < 0) {
                        next.y = 0;
                        next.height = prev.height;
                    }
                    if (next.height < 0) {
                        next.height = 0;
                        next.y = prev.y;
                    }

                    return next;
                case "bottom":
                    next.height = prev.height + mY;
                    if (next.height < 0) next.height = 0;
                    if (next.y + next.height > imgRect.height) {
                        next.height = prev.height;
                    }

                    return next;
                case "right":
                    next.width = prev.width + mX;
                    if (next.width < 0) next.width = 0;
                    if (next.x + next.width > imgRect.width) {
                        next.width = prev.width;
                    }
                    return next;
                default:
                    return next;
            }
        });
    }

    return (
        <>
            <OverlayOutline
                color="var(--bs-gray-400)"
                type="square"
            ></OverlayOutline>
            <div
                className="position-absolute w-100 h-100"
                style={{ zIndex: -1 }}
            ></div>
            <div
                style={{
                    top: subRect.y / pr,
                    left: subRect.x / pr,
                    width: subRect.width / pr,
                    height: subRect.height / pr,
                    position: "absolute",
                    zIndex: 1,
                }}
            >
                <OverlayOutline color="var(--bs-primary)" type="square">
                    <OverlayHandles
                        onMove={cutHandles}
                        positions={["left", "right", "bottom", "top"]}
                        type="square"
                    />
                    {children}
                </OverlayOutline>
                <DragMove
                    className="w-100 h-100 position-absolute"
                    onDragMove={(e) => {
                        setSubRect((prev) => {
                            let x = prev.x + e.movementX * pr;
                            let y = prev.y + e.movementY * pr;

                            if (x < 0) {
                                x = 0;
                            }
                            if (y < 0) {
                                y = 0;
                            }
                            if (x + prev.width > imgRect.width) {
                                x = prev.x;
                            }
                            if (y + prev.height > imgRect.height) {
                                y = prev.y;
                            }

                            return { ...prev, x, y };
                        });
                    }}
                    onDoubleClick={onDoubleClick}
                ></DragMove>
            </div>
        </>
    );
}
