"use client";
import {
    createContext,
    useCallback,
    useContext,
    useEffect,
    useMemo,
    useState,
} from "react";

import { ImageSnip } from "@snip/snips/general/image";

import { useToolsContext } from "../context";
import { usePlacementContext } from "../placement/context";
import { Rect, ToolContextProps } from "../types";

import { useWorkerContext } from "components/context/viewer/useWorkerContext";
import { useEditorContext } from "components/editor/context";

export interface ImageEditorContextI {
    /** Sub positions divide an image into a sub rectangle
     *
     * in dom units
     */
    subRect: Rect;
    setSubRect: (f: (prev: Rect) => Rect, units?: "view" | "page") => void;

    /** Position of the overlay in
     * in dom units
     */
    imgRect: Rect;
    setImgRect: (f: (prev: Rect) => Rect, units?: "view" | "page") => void;

    /** Currently active snip */
    activeImgSnip?: ImageSnip;
    resetImgSnip: () => void;
}

const ImageEditorContext = createContext<ImageEditorContextI | null>(null);

export function useImageEditorContext() {
    const context = useContext(ImageEditorContext);
    if (!context)
        throw new Error("Cant use ImageEditorContext outside provider!");
    return context;
}

export function ImageEditorContextProvider({ children }: ToolContextProps) {
    const { overlayRect, setOverlayRect } = useEditorContext();
    const { transformRectCoords, transformRelativeCoords, placePageIdx } =
        useToolsContext();
    const { activeSnip } = usePlacementContext();
    const { workers } = useWorkerContext();

    const activeImgSnip: ImageSnip | undefined = useMemo(() => {
        if (activeSnip instanceof ImageSnip) {
            return activeSnip;
        }
        return undefined;
    }, [activeSnip]);

    const [subRect, _setSubRect] = useState<Rect>({
        x: 0,
        y: 0,
        width: 10,
        height: 20,
    });

    const setImgRect = useCallback(
        (newRect: (a: Rect) => Rect, units: "view" | "page" = "view") => {
            const oWorker = workers.get(placePageIdx)?.overlay;
            if (!oWorker || !activeImgSnip) return;

            setOverlayRect((prev) => {
                let pageRect: Rect;
                let viewRect: Rect;
                if (units == "page") {
                    pageRect = newRect(
                        transformRectCoords(placePageIdx, prev, "page"),
                    );
                    viewRect = transformRectCoords(
                        placePageIdx,
                        pageRect,
                        "view",
                    );
                } else {
                    viewRect = newRect(prev);
                    // Check min/max values
                    if (viewRect.width < 5) viewRect.width = 5;
                    if (viewRect.height < 5) viewRect.height = 5;
                    pageRect = transformRectCoords(
                        placePageIdx,
                        viewRect,
                        "page",
                    );
                }

                activeImgSnip.x = pageRect.x;
                activeImgSnip.y = pageRect.y;
                activeImgSnip.width = pageRect.width;
                activeImgSnip.height = pageRect.height;
                oWorker.rerender();

                // We also need to set the subrect otherwise
                // there are some resizing issues
                const sxy = transformRelativeCoords(
                    placePageIdx,
                    [
                        (activeImgSnip.sx * activeImgSnip.width) /
                            activeImgSnip.img_width,
                        (activeImgSnip.sy * activeImgSnip.height) /
                            activeImgSnip.img_height,
                    ],
                    "view",
                );
                const swh = transformRelativeCoords(
                    placePageIdx,
                    [
                        (activeImgSnip.swidth * activeImgSnip.width) /
                            activeImgSnip.img_width,
                        (activeImgSnip.sheight * activeImgSnip.height) /
                            activeImgSnip.img_height,
                    ],
                    "view",
                );

                _setSubRect({
                    x: sxy[0],
                    y: sxy[1],
                    width: swh[0],
                    height: swh[1],
                });

                return viewRect;
            });
        },
        [
            workers,
            placePageIdx,
            activeImgSnip,
            setOverlayRect,
            transformRelativeCoords,
            transformRectCoords,
        ],
    );

    const setSubRect = useCallback(
        (newSubRect: (a: Rect) => Rect, units: "view" | "page" = "view") => {
            const oWorker = workers.get(placePageIdx)?.overlay;
            if (!oWorker || !activeImgSnip) return;

            _setSubRect((prev) => {
                let pageRect: Rect;
                let viewRect: Rect;
                if (units == "page") {
                    pageRect = newSubRect(
                        transformRectCoords(placePageIdx, prev, "page"),
                    );
                    viewRect = transformRectCoords(
                        placePageIdx,
                        pageRect,
                        "view",
                    );
                } else {
                    viewRect = newSubRect(prev);
                    pageRect = transformRectCoords(
                        placePageIdx,
                        viewRect,
                        "page",
                    );
                }

                // convert to page coords
                // use it is kinda its own coord system
                // 0,0 is aligned inside snip rect
                // And sx,sy,swidth,sheight us the original image coords
                const xy = transformRelativeCoords(
                    placePageIdx,
                    [viewRect.x, viewRect.y],
                    "page",
                );
                const wh = transformRelativeCoords(
                    placePageIdx,
                    [viewRect.width, viewRect.height],
                    "page",
                );

                activeImgSnip.sx =
                    (activeImgSnip.img_width / activeImgSnip.width) * xy[0];
                activeImgSnip.sy =
                    (activeImgSnip.img_height / activeImgSnip.height) * xy[1];
                activeImgSnip.swidth =
                    (activeImgSnip.img_width / activeImgSnip.width) * wh[0];
                activeImgSnip.sheight =
                    (activeImgSnip.img_height / activeImgSnip.height) * wh[1];
                oWorker.rerender();

                return viewRect;
            });
        },
        [
            activeImgSnip,
            placePageIdx,
            transformRectCoords,
            transformRelativeCoords,
            workers,
        ],
    );

    function resetImgSnip() {
        if (!activeImgSnip) return;

        // reset snip
        activeImgSnip.width = activeImgSnip.img_width;
        activeImgSnip.height = activeImgSnip.img_height;
        activeImgSnip.sx = 0;
        activeImgSnip.sy = 0;
        activeImgSnip.swidth = activeImgSnip.img_width;
        activeImgSnip.sheight = activeImgSnip.img_height;

        //update rect
        setImgRect((rect) => {
            return {
                ...rect,
                width: activeImgSnip.img_width ?? 0,
                height: activeImgSnip.img_height ?? 0,
                rot: 0,
            };
        }, "page");
    }

    /** Whenever the snip changes reset the
     * overlay rect to the snips width and height
     * also reset the subrect.
     */
    useEffect(() => {
        if (!activeImgSnip || !(activeImgSnip instanceof ImageSnip)) return;

        setImgRect(() => {
            return {
                x: activeImgSnip.x,
                y: activeImgSnip.y,
                width: activeImgSnip.width,
                height: activeImgSnip.height,
            };
        }, "page");
    }, [activeImgSnip, setImgRect]);

    return (
        <ImageEditorContext.Provider
            value={{
                subRect,
                setSubRect,
                imgRect: overlayRect,
                setImgRect,
                activeImgSnip,
                resetImgSnip,
            }}
        >
            {children}
        </ImageEditorContext.Provider>
    );
}
