import { useEffect, useState } from "react";
import { TbPhotoEdit } from "react-icons/tb";

import DragMove from "../common/drag";
import { Help, HelpDescription, HelpHeader } from "../common/help";
import {
    Overlay,
    OverlayContent,
    OverlayPlaceButton,
    useSnipOverlay,
} from "../common/overlay";
import { ItemSmall, Submenu } from "../common/submenu";
import { Toggle } from "../common/toggle";
import { useToolsContext } from "../context";
import { usePlacementContext } from "../placement/context";
import { ToolComponents, ToolSubmenuProps, ToolToggleProps } from "../types";
import { ImageEditorContextProvider, useImageEditorContext } from "./context";
import {
    CutHandlersOverlayOutline,
    ResizeHandlerOverlayOutline,
} from "./handlers";

import { LinkedNumbersInput } from "components/form/inputs/number";

import stylesCommon from "../common/toggle.module.scss";
import styles from "./imageEditor.module.scss";

function ImageToggle(props: ToolToggleProps) {
    const { activeImgSnip } = useImageEditorContext();

    // this tool only works with an active image snip
    if (!activeImgSnip) {
        return null;
    }

    return <Toggle label="Resize" icon={<TbPhotoEdit />} {...props} />;
}

function ImageOverlay() {
    const [state, setState] = useState<"resize" | "cut">("resize");
    const { move, placeOnPage } = usePlacementContext();
    const { activeImgSnip } = useImageEditorContext();

    useSnipOverlay(activeImgSnip, 0);

    function onDoubleClick() {
        if (!activeImgSnip) return;

        setState((prev) => (prev === "resize" ? "cut" : "resize"));
    }

    return (
        <Overlay>
            {state === "resize" ? (
                <ResizeHandlerOverlayOutline>
                    <OverlayPlaceButton
                        onPlace={placeOnPage}
                        style={{ zIndex: 1 }}
                    />
                </ResizeHandlerOverlayOutline>
            ) : (
                <CutHandlersOverlayOutline onDoubleClick={onDoubleClick}>
                    <OverlayPlaceButton
                        onPlace={placeOnPage}
                        style={{ zIndex: 1 }}
                    />
                </CutHandlersOverlayOutline>
            )}
            <OverlayContent>
                <DragMove
                    className="w-100 h-100 position-absolute"
                    onDragMove={function (e: PointerEvent): void {
                        if (state === "resize") {
                            move(
                                e.movementX * window.devicePixelRatio,
                                e.movementY * window.devicePixelRatio,
                                "view",
                            );
                        }
                    }}
                    onDoubleClick={onDoubleClick}
                />
            </OverlayContent>
        </Overlay>
    );
}

function ImageHelp() {
    return (
        <Help>
            <HelpHeader>
                <span>Image</span>
                <TbPhotoEdit
                    className={stylesCommon.icon}
                    style={{
                        fill: "transparent",
                    }}
                />
            </HelpHeader>
            <HelpDescription>
                Resize and cut the image. 2 x <kbd>LMB</kbd> to change between
                resizing and cutting. You may hold <kbd>Shift</kbd> to maintain
                the aspect ratio.
            </HelpDescription>
        </Help>
    );
}

function ImageSubMenu(props: ToolSubmenuProps) {
    const { resetImgSnip, imgRect, setImgRect, activeImgSnip } =
        useImageEditorContext();
    const { transformRelativeCoords, placePageIdx } = useToolsContext();

    useEffect(() => {
        if (!activeImgSnip) return;

        const widthEl = document.getElementById(
            "size_width",
        ) as HTMLInputElement;
        const heightEl = document.getElementById(
            "size_height",
        ) as HTMLInputElement;

        const wh = transformRelativeCoords(
            placePageIdx,
            [imgRect.width, imgRect.height],
            "page",
        );
        if (widthEl) {
            widthEl.value = Math.round(wh[0]).toString();
        }
        if (heightEl) {
            heightEl.value = Math.round(wh[1]).toString();
        }
    }, [activeImgSnip, imgRect, placePageIdx, transformRelativeCoords]);

    return (
        <Submenu {...props} className={styles.submenu}>
            <ItemSmall className={styles.wh}>
                <LinkedNumbersInput
                    label="Size"
                    names={["size_width", "size_height"]}
                    min={undefined}
                    max={undefined}
                    defaultLocked={true}
                    onBlur={(width, height, locked) => {
                        if ((!width && !height) || !activeImgSnip) return;

                        const ratio =
                            activeImgSnip.width / activeImgSnip.height;

                        if (!width) {
                            width = (
                                document.getElementById(
                                    "size_width",
                                )! as HTMLInputElement
                            ).valueAsNumber;
                            if (locked) {
                                width = height! * ratio;
                            }
                        }

                        if (!height) {
                            height = (
                                document.getElementById(
                                    "size_height",
                                )! as HTMLInputElement
                            ).valueAsNumber;

                            if (locked) {
                                height = width / ratio;
                            }
                        }
                        setImgRect((prev) => {
                            const next = {
                                ...prev,
                                width: width,
                                height: height,
                            };
                            return next;
                        }, "page");
                    }}
                    onReset={resetImgSnip}
                    className={styles.inputs}
                />
            </ItemSmall>
        </Submenu>
    );
}

const ImageTool = {
    name: "image" as const,
    Toggle: ImageToggle,
    Help: ImageHelp,
    Overlay: ImageOverlay,
    Submenu: ImageSubMenu,
    ContextProvider: ImageEditorContextProvider,
} as ToolComponents;

export default ImageTool;
