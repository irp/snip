"use client";
import { Fragment, ReactNode, useEffect, useState } from "react";
import { Button, ButtonGroup } from "react-bootstrap";
import {
    TbArrowAutofitHeight,
    TbArrowAutofitWidth,
    TbArrowsMove,
    TbLayoutAlignCenter,
    TbLayoutAlignMiddle,
} from "react-icons/tb";

import {
    Help,
    HelpDescription,
    HelpHeader,
    HelpShortcuts,
} from "../common/help";
import { ItemSmall, Submenu } from "../common/submenu";
import { Toggle } from "../common/toggle";
import { ToolComponents, ToolSubmenuProps, ToolToggleProps } from "../types";
import { MoveToolContextProvider, useMoveToolContext } from "./context";
import { MoveHandler } from "./moveHandler";

import { useEditorContext } from "components/editor/context";
import { JoinedRangeNumberInput } from "components/form/inputs/number";

import styles from "./move.module.scss";

function MoveToggle(props: ToolToggleProps) {
    const { activePageIds } = useEditorContext();
    const { fitPage, centerPage } = useMoveToolContext();
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        if (mounted || activePageIds.length < 1) return;
        // Only autofit if no page was set previously
        setMounted(true);
        fitPage(0, true, true);
        centerPage(0, true, true);
    }, [activePageIds.length, fitPage, centerPage, mounted]);

    return (
        <>
            <Toggle label="Move" icon={<TbArrowsMove />} {...props} />
        </>
    );
}

function MoveSubmenu(props: ToolSubmenuProps) {
    //placeholder
    const { zoom } = useEditorContext();
    const { setZoom, centerPage, fitPage } = useMoveToolContext();

    const zoomInputs: ReactNode[] = [];
    zoom.forEach((value, index) => {
        zoomInputs.push(
            <Fragment key={index}>
                <ItemSmall className={styles.zoom}>
                    <JoinedRangeNumberInput
                        name={"zoom" + index}
                        label={"Zoom " + (index + 1)}
                        key={index}
                        min={0.25}
                        max={4}
                        step={0.05}
                        value={value}
                        onReset={() => {
                            setZoom(index, 1);
                        }}
                        onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                            setZoom(index, e.target.valueAsNumber);
                        }}
                    />
                </ItemSmall>
                <ItemSmall className={styles.align} key={index + "align"}>
                    <ButtonGroup className="h-100">
                        <Button
                            variant="none"
                            onClick={() => {
                                centerPage(index, true, false);
                            }}
                        >
                            <TbLayoutAlignCenter />
                        </Button>
                        <Button
                            variant="none"
                            onClick={() => {
                                centerPage(index, false, true);
                            }}
                        >
                            <TbLayoutAlignMiddle />
                        </Button>
                    </ButtonGroup>
                    <ButtonGroup className="h-100">
                        <Button
                            variant="none"
                            onClick={() => {
                                fitPage(index, true, false);
                            }}
                        >
                            <TbArrowAutofitWidth />
                        </Button>
                        <Button
                            variant="none"
                            onClick={() => {
                                fitPage(index, false, true);
                            }}
                        >
                            <TbArrowAutofitHeight />
                        </Button>
                    </ButtonGroup>
                </ItemSmall>
            </Fragment>,
        );
    });

    return (
        <Submenu {...props} className={styles.menu}>
            {zoomInputs}
        </Submenu>
    );
}

function MoveHelp() {
    return (
        <Help>
            <HelpHeader>
                <span>Move &amp; Resize</span>
                <TbArrowsMove />
            </HelpHeader>
            <HelpDescription>
                <p>
                    Use this tool to move the page by dragging, zoom with pinch
                    gestures, the mouse wheel or by holding the pen&apos;s
                    barrel key.
                </p>
            </HelpDescription>
            <HelpShortcuts
                shortcuts={[
                    ["m", "Toggle tool"],
                    ["MMB", "Fit page"],
                ]}
            />
        </Help>
    );
}

/** In the overlay we register events
 * used to move the page as
 * they are dynamically created
 * based on the currently active tool.
 */
function MoveOverlay() {
    return (
        <>
            <MoveHandler />
        </>
    );
}

const MoveTool = {
    name: "movePage" as const,
    Toggle: MoveToggle,
    Submenu: MoveSubmenu,
    Help: MoveHelp,
    Overlay: MoveOverlay,
    ContextProvider: MoveToolContextProvider,
} as ToolComponents;

export default MoveTool;
