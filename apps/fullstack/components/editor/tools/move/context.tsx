import { createContext, useCallback, useContext } from "react";

import { ToolContextProps } from "../types";

import { useWorkerContext } from "components/context/viewer/useWorkerContext";
import { useEditorContext } from "components/editor/context";

type page_idx = number;

interface MoveContextI {
    setZoom: (page: page_idx, zoom: number) => void;

    // Helpers
    scalePage: (
        page_idx: page_idx,
        scale: number,
        origin: [number, number],
    ) => void;
    translatePage: (page_idx: page_idx, translation: [number, number]) => void;
    centerPage: (
        page_idx: page_idx,
        horizontally?: boolean,
        vertically?: boolean,
    ) => void;
    fitPage: (page_idx: page_idx, width?: boolean, height?: boolean) => void;
}

const MoveToolContext = createContext<MoveContextI | null>(null);

export function MoveToolContextProvider({ children }: ToolContextProps) {
    const { workers } = useWorkerContext();
    const { triggerScaleZoomUpdate } = useEditorContext();

    /* -------------------------------------------------------------------------- */
    /*                           evertyhing zoom related                          */
    /* -------------------------------------------------------------------------- */
    const setZoom = useCallback(
        (page_idx: page_idx, zoom: number, origin?: [number, number]) => {
            const viewport = workers?.get(page_idx)?.main.viewport;
            if (!viewport) return;
            viewport.setZoom(zoom, origin);
            triggerScaleZoomUpdate(page_idx, viewport);
        },
        [triggerScaleZoomUpdate, workers],
    );

    const getZoom = useCallback(
        (page_idx: page_idx) => {
            const viewport = workers?.get(page_idx)?.main.viewport;
            if (!viewport) return 1;
            return viewport.getZoom();
        },
        [workers],
    );

    /* -------------------------------------------------------------------------- */
    /*                                some helpers                                */
    /* -------------------------------------------------------------------------- */
    const translatePage = useCallback(
        (page_idx: page_idx, t: [number, number]) => {
            // We need to multiple the devicepixel ratio
            t = t.map((a) => a * window.devicePixelRatio || 1) as [
                number,
                number,
            ];
            workers?.get(page_idx)?.main.viewport.translate(t);
        },
        [workers],
    );
    const scalePage = useCallback(
        (page_idx: page_idx, s: number, origin: [number, number]) => {
            const viewport = workers?.get(page_idx)?.main.viewport;
            if (!viewport) return;
            // Min/max
            if (s < 1 && getZoom(page_idx) <= 0.25) {
                return setZoom(page_idx, 0.25, origin);
            }
            if (s > 1 && getZoom(page_idx) >= 4) {
                return setZoom(page_idx, 4, origin);
            }
            viewport.scale(s, origin);
            triggerScaleZoomUpdate(page_idx, viewport);
        },
        [getZoom, setZoom, triggerScaleZoomUpdate, workers],
    );

    const centerPage = useCallback(
        (
            page_idx: page_idx,
            horizontally: boolean = true,
            vertically: boolean = true,
        ) => {
            const viewport = workers?.get(page_idx)?.main.viewport;
            if (!viewport) return;

            viewport.centerPage(horizontally, vertically);
            triggerScaleZoomUpdate(page_idx, viewport);
        },
        [triggerScaleZoomUpdate, workers],
    );

    const fitPage = useCallback(
        (page_idx: page_idx, width: boolean = true, height: boolean = true) => {
            const viewport = workers?.get(page_idx)?.main.viewport;
            if (!viewport) return;

            viewport.fitPage(width, height, [20, 20]);
            triggerScaleZoomUpdate(page_idx, viewport);
        },
        [triggerScaleZoomUpdate, workers],
    );

    return (
        <MoveToolContext.Provider
            value={{
                setZoom,
                translatePage,
                scalePage,
                centerPage,
                fitPage,
            }}
        >
            {children}
        </MoveToolContext.Provider>
    );
}

export function useMoveToolContext() {
    const context = useContext(MoveToolContext);
    if (!context) throw new Error("Cant use MoveToolContext outside provider!");
    return context;
}
