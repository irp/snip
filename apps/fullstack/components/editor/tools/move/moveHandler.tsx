import { useEffect, useRef } from "react";

import { useMoveToolContext } from "./context";

import { useEditorContext } from "components/editor/context";

type PointerTypes = "mouse" | "touch" | "pen";

/**
 * This handler is responsible for moving the pages
 *
 * @returns
 */
export function MoveHandler({
    pointerTypes,
}: {
    pointerTypes?: PointerTypes[];
}) {
    const { editorElementRef } = useEditorContext();

    const { fitPage, scalePage, translatePage } = useMoveToolContext();

    const evCache = useRef<Map<number, PointerEvent[]>>(new Map());

    useEffect(() => {
        const removeHandlers: (() => void)[] = [];

        editorElementRef.current.forEach((div, i) => {
            const { registerContainer } = pointerEventFactory(
                evCache.current,
                pointerTypes,
                (scale, origin, _type) => {
                    scalePage(i, scale, origin);
                },
                (tra) => translatePage(i, tra),
                () => {
                    fitPage(i);
                },
            );
            removeHandlers.push(registerContainer(div));
        });

        return () => {
            removeHandlers.forEach((h) => h());
        };
    }, [fitPage, editorElementRef, pointerTypes, scalePage, translatePage]);

    return null;
}

/**
 * A custom hook for handling pointer events on a canvas element, specifically tailored for
 * operations such as pinch-zoom and move gestures. This hook supports multiple pointer types
 * including mouse, touch, and pen. The provided onZoom and onMove callbacks are invoked
 * when corresponding gestures are detected.
 *
 * The following features are currently supported:
 * - onTranslate:
 *      - move with standard left click, pointer down
 * - onScale:
 *      - move: holding rightclick or the eraser button allows to zoom
 *      - pinch: zoom with touch pinch gesture (2 fingers)
 *      - wheel: zoom with mouse wheel
 * - onFit:
 *      - middle/wheel click
 *
 * Pen eraser or barrel button allows to zoom. Wheel also zooms. Prevents context menu
 *
 * @param {MutableRefObject<HTMLCanvasElement>} canvasRef - A reference to the canvas element on which to handle pointer events.
 * @param {Array<PointerEvent["pointerType"]>} [allowedPointerTypes=["mouse", "touch", "pen"]] - Array of allowed pointer types. Defaults to "mouse", "touch", and "pen".
 * @param {Function} [onScale] - Optional callback function to handle the zoom event. Receives the zoom scale and origin as arguments.
 * @param {Function} [onMove] - Optional callback function to handle the move event. Receives the translation distance as arguments.
 */
export const pointerEventFactory = (
    evCache: Map<number, PointerEvent[]>,
    allowedPointerTypes: Array<PointerEvent["pointerType"]> = [
        "mouse",
        "touch",
        "pen",
    ],
    onScale?: (
        scale: number,
        origin: [number, number],
        type: "wheel" | "pinch" | "move",
    ) => void,
    onMove?: (translate: [number, number]) => void,
    onFit?: () => void,
) => {
    // We need to preserve pointer event state during event phase
    // to handle multitouch

    // The button is only added on the primary event for some reason
    let pressedButton: number | null = null;
    let prevDistance: number | null = null;
    const pinchZoom = () => {
        if (evCache.size !== 2) return;
        //Compute the distance of each tuple
        const [pointer1, pointer2] = Array.from(evCache.values()).map(
            (events) => events.at(-1),
        );
        if (!pointer1 || !pointer2) return;

        // Calculate the current distance between the two points
        const dx = pointer2.screenX - pointer1.screenX;
        const dy = pointer2.screenY - pointer1.screenY;
        const currentDistance = Math.sqrt(dx * dy + dy * dy);

        // Calculate the center point between the two pointers
        const centerX = (pointer1.layerX + pointer2.layerX) / 2;
        const centerY = (pointer1.layerY + pointer2.layerY) / 2;

        if (prevDistance !== null) {
            // Sometimes the distance jumps for some reason
            // I was not able to find the reason! Therefore we filter
            // too big of a scale
            const scale = currentDistance / prevDistance;
            if (scale < 1.25 && scale > 0.75) {
                onScale?.(
                    scale,
                    [
                        centerX * window.devicePixelRatio,
                        centerY * window.devicePixelRatio,
                    ],
                    "pinch",
                );
            }
        }
        prevDistance = currentDistance;
    };

    let prevPosition: [number, number] | null = null;
    const handleMove = () => {
        if (evCache.size !== 1) return;
        // @ts-expect-error dont know how to fix this type error
        const [[lastEvent, secondLastEvent]] = Array.from(evCache.values())
            .map((events) => [events.at(-1) ?? null, events.at(-2) ?? null])
            .filter(([lastEvent]) => lastEvent !== null); // Filter out invalid arrays

        //erase btn or right/pem barrel btn trigger zoom
        if (pressedButton == 5 || pressedButton == 2) {
            if (!secondLastEvent) return;
            // Calculate the current distance between the two points
            const dy =
                (secondLastEvent.screenY - lastEvent.screenY) /
                window.devicePixelRatio;
            onScale?.(
                1 + dy / 200,
                [
                    ((secondLastEvent.offsetX + lastEvent.offsetX) / 2) *
                        window.devicePixelRatio,
                    lastEvent.offsetY * window.devicePixelRatio,
                ],
                "move",
            );
        } else {
            if (prevPosition !== null) {
                // No device pixel ratio needed here as this is handled by the
                // width and height of the viewport
                onMove?.([
                    lastEvent.offsetX - prevPosition[0],

                    lastEvent.offsetY - prevPosition[1],
                ]);
            }
            prevPosition = [lastEvent.offsetX, lastEvent.offsetY];
        }
    };

    /* -------------------------------------------------------------------------- */
    /*                               pointer events                               */
    /* -------------------------------------------------------------------------- */
    // Wheel handler for mouse-based zoom

    /** Check if the pointer type is allowed, sets touch events
     * with an 1x1px rect as pen events!
     */
    const isAllowed = (type: string, id: number, ev?: PointerEvent) => {
        /**
             * 
            if (type === "touch" && width == 1 && height == 1) {
                //Sometimes pens are registered as touch with 1px area
                return allowedPointerTypes.includes("pen");
            }
            */

        if (!(allowedPointerTypes.includes(type) || evCache.has(id))) {
            return false;
        }
        //Check if ev width and height are bigger than 80
        // this roughtly corresponds to a palm touch
        //this is normally not wanted
        if (ev && ev.width > 80 && ev.height > 80) {
            return false;
        }
        return true;
    };

    const wheelHandler = (ev: WheelEvent) => {
        if (!isAllowed("mouse", -1)) return;
        ev.preventDefault();
        const scale = 1.0 + ev.deltaY / 1000;

        const originX = ev.offsetX * window.devicePixelRatio;
        const originY = ev.offsetY * window.devicePixelRatio;

        onScale?.(scale, [originX, originY], "wheel");
    };

    const pointermoveHandler = (ev: PointerEvent) => {
        //console.log("id", ev.pointerType, ev.pointerId, ev.button);
        if (!isAllowed(ev.pointerType, ev.pointerId)) return;
        // Find this event in the cache and update its record with this event
        if (!evCache.has(ev.pointerId)) return;
        evCache.get(ev.pointerId)!.push(ev);
        if (ev.button != -1) {
            pressedButton = ev.button;
        }

        //Handle gestures
        onScale && pinchZoom(); //2 pointers

        (onMove || onScale) && handleMove();
    };

    const pointerdownHandler = (ev: PointerEvent) => {
        if (!isAllowed(ev.pointerType, ev.pointerId)) return;

        evCache.set(ev.pointerId, [ev]);

        pressedButton = ev.button;
    };

    const pointerupHandler = (ev: PointerEvent) => {
        if (!isAllowed(ev.pointerType, ev.pointerId)) return;
        // Remove all pointer from the cache
        // this prevents unintended mouse move events

        evCache.delete(ev.pointerId);
        prevDistance = null;
        prevPosition = null;
        pressedButton = null;
    };
    const contextmenuHandler = (e: MouseEvent) => {
        e.preventDefault();
    };

    const dbclickHandler = (_e: MouseEvent) => {
        console.log("dblclick");
    };

    const auxclickHandler = (ev: MouseEvent) => {
        // middle click
        if (ev.button == 1) onFit?.();
    };

    // Helper to add all events
    function registerContainer(container: HTMLElement) {
        container.addEventListener("pointerdown", pointerdownHandler);
        container.addEventListener("pointermove", pointermoveHandler);
        container.addEventListener("wheel", wheelHandler);
        container.addEventListener("contextmenu", contextmenuHandler);
        container.addEventListener("dblclick", dbclickHandler);
        container.addEventListener("auxclick", auxclickHandler);

        // Use same handler for pointer{up,cancel,out,leave} events since
        // the semantics for these events are the same
        container.addEventListener("pointerup", pointerupHandler);
        container.addEventListener("pointercancel", pointerupHandler);
        container.addEventListener("pointerout", pointerupHandler);
        container.addEventListener("pointerleave", pointerupHandler);

        return () => {
            container.removeEventListener("pointerdown", pointerdownHandler);
            container.removeEventListener("pointermove", pointermoveHandler);
            container.removeEventListener("wheel", wheelHandler);
            container.removeEventListener("contextmenu", contextmenuHandler);
            container.removeEventListener("dblclick", dbclickHandler);
            container.removeEventListener("auxclick", auxclickHandler);

            container.removeEventListener("pointerup", pointerupHandler);
            container.removeEventListener("pointercancel", pointerupHandler);
            container.removeEventListener("pointerout", pointerupHandler);
            container.removeEventListener("pointerleave", pointerupHandler);
        };
    }

    return {
        registerContainer,
        pointerdownHandler,
        pointerupHandler,
        auxclickHandler,
        dbclickHandler,
        contextmenuHandler,
        wheelHandler,
        pointermoveHandler,
    };
};
