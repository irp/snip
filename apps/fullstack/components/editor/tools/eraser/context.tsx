import { createContext } from "react";

import { ToolContextProps } from "../types";

export interface EraserContextI {}

const EraserContext = createContext<EraserContextI>({});

export function EraserContextProvider({ children }: ToolContextProps) {
    return (
        <EraserContext.Provider value={{}}>{children}</EraserContext.Provider>
    );
}
