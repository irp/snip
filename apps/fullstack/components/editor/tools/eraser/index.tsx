import { useEffect } from "react";
import { TbEraser } from "react-icons/tb";

import { config } from "@snip/config/client";

import { Help, HelpDescription, HelpHeader } from "../common/help";
import { useInteractions } from "../common/interactions";
import { Toggle } from "../common/toggle";
import { ToolComponents, ToolToggleProps } from "../types";
import { EraserContextProvider } from "./context";

import { useWorkerContext } from "components/context/viewer/useWorkerContext";
import { useEditorContext } from "components/editor/context";

import stylesCommon from "../common/toggle.module.scss";
import stylesEraser from "./eraser.module.scss";

function EraserToggle(props: ToolToggleProps) {
    return (
        <Toggle
            icon={<TbEraser />}
            label="Eraser"
            {...props}
            expandable={false}
        />
    );
}

function EraserHelp() {
    return (
        <Help>
            <HelpHeader>
                <span>Eraser</span>
                <TbEraser
                    className={stylesCommon.icon}
                    style={{
                        fill: "transparent",
                    }}
                />
            </HelpHeader>
            <HelpDescription>
                <p>
                    The eraser tool allows you to delete recently placed snips.
                    You may only delete snips that you have placed in the last{" "}
                    {config.TOOLS.ERASER.TIMEOUT / 60} minutes.
                </p>
            </HelpDescription>
        </Help>
    );
}

function EraserOverlay() {
    /** Setup worker interaction
     * events when the overlay is mounted
     */
    const { workers } = useWorkerContext();
    const { editorElementRef } = useEditorContext();

    /** Setup worker interaction
     * events when the overlay is mounted
     * remove when unmounted
     */
    useInteractions();

    /** By design the eraser tool is relatively deeply baked into our worker pipeline
     * We basically apply a filter to all snips which cannot be erased on render and
     * overload the interaction methods
     */
    useEffect(() => {
        workers.forEach((w) => {
            w.main.enableEraser();
        });

        return () => {
            workers.forEach((w) => {
                w.main.disableEraser();
            });
        };
    });

    /** Register enter and leave events and
     * create elements for text snips
     */
    useEffect(() => {
        const remove: Array<() => void> = [];
        workers.forEach((w, i) => {
            function enterSnip() {
                editorElementRef
                    .current!.get(i)!
                    .classList.add(stylesEraser.cursor!);
            }

            function leaveSnip() {
                editorElementRef
                    .current!.get(i)!
                    .classList.remove(stylesEraser.cursor!);
            }

            function onClick(e: { snipData: { id?: number } }) {
                if (!e.snipData.id) return;
                w.main
                    .eraseSnip(e.snipData.id)
                    .then(console.error)
                    .finally(() => {
                        editorElementRef
                            .current!.get(i)!
                            .classList.remove(stylesEraser.cursor!);
                    });
            }

            w.main.interactions.addEventListener("pointerenter", enterSnip);
            w.main.interactions.addEventListener("pointerleave", leaveSnip);
            w.main.interactions.addEventListener("pointerdown", onClick);

            remove.push(() => {
                w.main.interactions.removeEventListener(
                    "pointerenter",
                    enterSnip,
                );
                w.main.interactions.removeEventListener(
                    "pointerleave",
                    leaveSnip,
                );
                w.main.interactions.removeEventListener("pointerdown", onClick);
            });
        });

        return () => {
            remove.forEach((fn) => fn());
        };
    }, [editorElementRef, workers]);

    /** Attach to page */
    return null;
}

const EraserTool: ToolComponents = {
    name: "eraser" as const,
    Toggle: EraserToggle,
    Help: EraserHelp,
    Overlay: EraserOverlay,
    ContextProvider: EraserContextProvider,
};

export default EraserTool;
