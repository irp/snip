import { config } from "@snip/config/client";
import { ConditionalRender, RenderContext } from "@snip/render/types";
import type { BaseSnip } from "@snip/snips/general/base";

export default class EraserConditionalRender extends ConditionalRender {
    condition(snip: BaseSnip): boolean {
        if (
            snip.last_updated.getTime() <
            Date.now() - config.TOOLS.ERASER.TIMEOUT * 1000
        ) {
            return true;
        }
        return false;
    }

    pre_render(ctx: RenderContext) {
        ctx.globalAlpha = 0.1;
    }
    post_render(ctx: RenderContext) {
        ctx.globalAlpha = 1.0;
    }

    on_pointerenter_worker(ctx: RenderContext, snip: BaseSnip) {
        ctx.save();
        ctx.shadowColor = "#000000";
        ctx.shadowBlur = 10;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
        snip.render(ctx);
        ctx.restore();
    }
}
