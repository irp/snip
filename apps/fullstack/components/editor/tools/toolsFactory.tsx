"use client";
import DoodleTool from "./doodle";
import EraserTool from "./eraser";
import ImageTool from "./imageEditor";
import InteractionTool from "./interaction";
import MoveTool from "./move";
import PlacementTool from "./placement";
import TextTool from "./text";
import { Tool, ToolComponents } from "./types";

export function getToolComponents(tool: Tool): ToolComponents {
    switch (tool) {
        case "movePage":
            return MoveTool;
        case "interaction":
            return InteractionTool;
        case "doodle":
            return DoodleTool;
        case "text":
            return TextTool;
        case "image":
            return ImageTool;
        case "placement":
            return PlacementTool;
        case "eraser":
            return EraserTool;
        default:
            throw new Error("Unknown tool");
    }
}
