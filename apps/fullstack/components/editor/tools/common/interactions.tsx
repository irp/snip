import { useEffect, useState } from "react";

import { useWorkerContext } from "components/context/viewer/useWorkerContext";
import { useEditorContext } from "components/editor/context";

/** The useInteraction hook attaches the pages event handler to
 * all the workers. It also unregister them on unmount.
 *
 * This allows to use the interactions events as follows:
 * const { worker } = useWorker(1);
 * worker.interactions.addEventListener("pointerenter",() => { console.log("Pointer entered snip area"); }
 *
 */
export function useInteractions() {
    const { workers } = useWorkerContext();
    const { editorElementRef } = useEditorContext();

    const [ready, setReady] = useState(false);

    /** Setup worker interaction
     * events when the overlay is mounted
     */
    useEffect(() => {
        if (ready) return;

        const w = Array.from(workers.values());

        Promise.all(
            w.map((w) => {
                w.main.setupInteraction();
            }),
        ).then(() => {
            setReady(true);
        });
    }, [ready, workers]);

    useEffect(() => {
        if (!ready) return;

        const w = Array.from(workers.values());

        const cleanup = w.map((w, i) => {
            const ele = editorElementRef.current.get(i);
            if (!ele) return () => {};
            return w.main.attachElementEvents(ele);
        });

        return () => {
            cleanup.forEach((c) => c());
        };
    });
}
