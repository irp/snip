import styles from "./help.module.scss";

export function Help({
    children,
    className,
    ...props
}: React.HTMLAttributes<HTMLDivElement>) {
    let cN = styles.wrapper;
    if (className) {
        cN += " " + className;
    }

    return (
        <div className={cN} {...props}>
            {children}
        </div>
    );
}

export function HelpHeader({
    children,
    className,
    ...props
}: React.HTMLAttributes<HTMLDivElement>) {
    let cN = styles.header;
    if (className) {
        cN += " " + className;
    }

    return (
        <div className={cN} {...props}>
            {children}
        </div>
    );
}

export function HelpDescription({
    children,
    className,
    ...props
}: React.HTMLAttributes<HTMLDivElement>) {
    let cN = styles.description;
    if (className) {
        cN += " " + className;
    }
    return (
        <div className={cN} {...props}>
            {children}
        </div>
    );
}

export function HelpShortcuts({
    shortcuts,
}: {
    shortcuts: [string | string[], string][];
}) {
    return (
        <div className={styles.shortcuts}>
            <div className={styles.title}>Shortcuts</div>
            <table>
                <thead>
                    <tr>
                        <th>Key</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {shortcuts.map(([key, description], i) => {
                        let kdb;
                        if (Array.isArray(key)) {
                            kdb = key.map((k) => <kbd key={k}>{k}</kbd>);
                        } else {
                            kdb = <kbd>{key}</kbd>;
                        }
                        return (
                            <tr key={i} style={{ textAlign: "left" }}>
                                <td
                                    style={{
                                        width: "50px",
                                    }}
                                >
                                    {kdb}
                                </td>
                                <td>{description}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
