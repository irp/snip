import { CSSProperties, ReactNode } from "react";
import { Button, ButtonProps } from "react-bootstrap";
import { IconContext } from "react-icons/lib";
import { TbCaretRightFilled } from "react-icons/tb";

import styles from "./toggle.module.scss";

/** Button to toggle a tool
 */
export function ToolBtn({ children, ...props }: ButtonProps) {
    let className = styles.toggleBtn;
    if (props.className) {
        className += " " + props.className;
    }

    return (
        <Button variant="none" className={className} {...props}>
            {children}
        </Button>
    );
}

export function ExpandBtn({ active, ...props }: ButtonProps) {
    let className = styles.expandBtn;
    if (props.className) {
        className += " " + props.className;
    }

    return (
        <Button variant="none" className={className} active={active} {...props}>
            <TbCaretRightFilled />
        </Button>
    );
}

interface ToggleProps {
    label: string;
    icon: ReactNode;
    isActive: boolean;
    setActive: (active: boolean) => void;
    isExpanded: boolean;
    setExpanded: (expanded: boolean) => void;
    expandable?: boolean;
    style?: CSSProperties;
    onClick?: React.MouseEventHandler<HTMLButtonElement>;
}

export function Toggle({
    label,
    icon,
    isActive,
    setActive,
    isExpanded,
    setExpanded,
    expandable = true,
    onClick,
    style = {},
}: ToggleProps) {
    return (
        <IconContext.Provider
            value={{
                className: styles.icon,
            }}
        >
            <div className={styles.wrapper} style={style} title={label}>
                <ToolBtn
                    active={isActive}
                    onClick={(e) => {
                        setActive(!isActive);
                        onClick?.(e);
                    }}
                    onContextMenu={(e) => {
                        e.preventDefault();
                        if (!expandable) return;
                        setExpanded(!isExpanded);
                    }}
                >
                    {icon}
                </ToolBtn>
                {expandable && (
                    <ExpandBtn
                        active={isExpanded}
                        onClick={() => setExpanded(!isExpanded)}
                        onContextMenu={(e) => {
                            e.preventDefault();
                            setExpanded(!isExpanded);
                        }}
                    />
                )}
            </div>
        </IconContext.Provider>
    );
}
