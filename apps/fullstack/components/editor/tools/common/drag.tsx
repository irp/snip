import { HTMLAttributes, useCallback, useEffect, useRef } from "react";

export interface DragMoveProps extends HTMLAttributes<HTMLDivElement> {
    onDragMove: (
        e: PointerEvent,
        offset: [number, number],
        movement: [number, number],
    ) => void;
}

export default function DragMove({
    onDragMove,
    children,
    ...props
}: DragMoveProps) {
    const ref = useRef<HTMLDivElement>(null);
    const [handlePointerDown] = useDrag({ onDragMove });

    useEffect(() => {
        if (!ref.current) return;
        const el = ref.current;
        el.addEventListener("pointerdown", handlePointerDown!);

        return () => {
            el.removeEventListener("pointerdown", handlePointerDown!);
        };
    }, [handlePointerDown, onDragMove]);

    return (
        <div ref={ref} {...props}>
            {children}
        </div>
    );
}

type PointerTypes = "mouse" | "touch" | "pen";

export function useDrag({
    pointerTypes,
    onDragMove,
    onPointerDown,
    onPointerUp,
}: {
    pointerTypes?: PointerTypes[];
    onDragMove: DragMoveProps["onDragMove"];
    onPointerDown?: (e: PointerEvent) => void;
    onPointerUp?: (e: PointerEvent) => void;
}) {
    const offset = useRef<[number, number]>([0, 0]);
    const isDragging = useRef(false);
    const screenPos = useRef<[number, number] | null>(null);
    const handlePointerMove = useCallback(
        (e: PointerEvent) => {
            e.preventDefault();
            // Check if the pointer type is allowed
            if (
                pointerTypes &&
                !pointerTypes.includes(e.pointerType as PointerTypes)
            )
                return;
            e.preventDefault();

            if (isDragging.current && screenPos.current) {
                const movement: [number, number] = [
                    e.clientX - screenPos.current[0],
                    e.clientY - screenPos.current[1],
                ];
                onDragMove(e, offset.current, movement);
                screenPos.current = [e.clientX, e.clientY];
            }
        },
        [onDragMove, pointerTypes],
    );

    const handlePointerUp = useCallback(
        (e: PointerEvent) => {
            isDragging.current = false;
            offset.current = [0, 0];

            // Check if the pointer type is allowed
            if (
                pointerTypes &&
                !pointerTypes.includes(e.pointerType as PointerTypes)
            )
                return;

            if (onPointerUp) onPointerUp(e);
            e.preventDefault();

            // Remove event listener for pointer move
            document.body.removeEventListener("pointermove", handlePointerMove);
            document.body.removeEventListener("pointerup", handlePointerUp);
        },
        [handlePointerMove, onPointerUp, pointerTypes],
    );

    const handlePointerDown = useCallback(
        (e: PointerEvent) => {
            offset.current = [e.layerX, e.layerY];
            screenPos.current = [e.pageX, e.pageY];
            isDragging.current = true;

            if (
                pointerTypes &&
                !pointerTypes.includes(e.pointerType as PointerTypes)
            )
                return;
            e.preventDefault();

            if (onPointerDown) onPointerDown(e);
            // Add event listener for pointer move
            document.body.style.touchAction = "none";
            document.body.addEventListener("pointermove", handlePointerMove);
            document.body.addEventListener("pointerup", handlePointerUp);
        },
        [handlePointerMove, handlePointerUp, onPointerDown, pointerTypes],
    );

    return [handlePointerDown, handlePointerUp, handlePointerMove];
}
