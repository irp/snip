"use client";
import { useCallback, useEffect, useRef } from "react";

export function useKeyPress(
    callback: (e: KeyboardEvent, map: Map<string, boolean>) => void,
) {
    const map = useRef(new Map<string, boolean>());
    const handleKeyDown = useCallback(
        (e: KeyboardEvent) => {
            const key = e.key;

            map.current.set(key, true);
            callback(e, map.current);
        },
        [callback],
    );

    const handleKeyUp = useCallback((e: KeyboardEvent) => {
        const key = e.key;
        map.current.delete(key);
    }, []);

    useEffect(() => {
        window.addEventListener("keydown", handleKeyDown);
        window.addEventListener("keyup", handleKeyUp);

        return () => {
            window.removeEventListener("keydown", handleKeyDown);
            window.removeEventListener("keyup", handleKeyUp);
        };
    }, [handleKeyDown, handleKeyUp]);
}
