import { HTMLAttributes } from "react";

import { ToolSubmenuProps } from "../types";

import styles from "./submenu.module.scss";

interface SubmenuProps extends ToolSubmenuProps {
    children: React.ReactNode;
    className?: string;
}

export function Submenu({ children, isExpanded, className }: SubmenuProps) {
    let cN = styles.wrapper;
    if (className) {
        cN += " " + className;
    }

    return (
        <div className={cN} data-show={isExpanded}>
            {children}
        </div>
    );
}

export function ItemFullSize({
    title,
    description,
    children,
}: {
    title: string;
    description: string;
    children: React.ReactNode;
}) {
    return (
        <div className={styles.itemFull}>
            {title || description ? (
                <div className={styles.header}>
                    <label>{title}</label>
                    <span>{description}</span>
                </div>
            ) : null}
            <div className={styles.content}>{children}</div>
        </div>
    );
}

export function ItemSmall({
    children,
    className,
    ...props
}: HTMLAttributes<HTMLDivElement>) {
    let cN = styles.itemSmall;
    if (className) {
        cN += " " + className;
    }
    return (
        <div className={cN} {...props}>
            {children}
        </div>
    );
}
