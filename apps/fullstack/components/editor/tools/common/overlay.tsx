import useDevicePixelRatio from "lib/hooks/useDevicePixelRatio";
import { HTMLAttributes, ReactNode, useEffect } from "react";
import { Button } from "react-bootstrap";
import { TbArrowsMove } from "react-icons/tb";

import { hasOwnProperty } from "@snip/common";
import { TypedEventListener } from "@snip/common/eventTarget";
import { RenderContext, RenderFunction } from "@snip/render/types";
import { BaseSnip } from "@snip/snips/general/base";
import { ImageSnip } from "@snip/snips/general/image";

import { usePlacementContext } from "../placement/context";
import { Rect } from "../types";
import DragMove from "./drag";

import { useQueuedSnipsContext } from "components/context/socket/useQueuedSnipsContext";
import { useWorkerContext } from "components/context/viewer/useWorkerContext";
import { useEditorContext } from "components/editor/context";

import styles from "./overlay.module.scss";

export function Overlay({
    children,
    className,
    ...props
}: HTMLAttributes<HTMLDivElement>) {
    const { overlayRect } = useEditorContext();
    const pr = useDevicePixelRatio();
    let cN = styles.wrapper;
    if (className) {
        cN += " " + className;
    }

    return (
        <div
            className={cN}
            style={{
                top: overlayRect.y / pr,
                left: overlayRect.x / pr,
                width: overlayRect.width / pr,
                minHeight: overlayRect.height / pr,
                transform: `rotate(${overlayRect.rot ?? 0}deg) ${
                    overlayRect.mirror ? "scaleX(-1)" : ""
                }`,
            }}
            {...props}
        >
            {children}
        </div>
    );
}

export function OverlayMover({
    onDragMove,
}: {
    onDragMove: (
        e: PointerEvent,
        offset?: [number, number],
        movement?: [number, number],
    ) => void;
}) {
    return (
        <DragMove className={styles.mover} onDragMove={onDragMove}>
            <TbArrowsMove />
        </DragMove>
    );
}

/**
 * Overlay outline
 *
 * @param type
 * @param margin
 * @returns
 */
export function OverlayOutline({
    children,
    inner,
    className,
    type = "round",
    color = "var(--bs-secondary)",
    style,
    ...props
}: HTMLAttributes<HTMLDivElement> & {
    margin?: string;
    type?: "round" | "square";
    inner?: ReactNode;
}) {
    let cN = styles.outline;
    if (className) {
        cN += " " + className;
    }

    style = style || {};

    if (color) {
        style["borderColor"] = color;
    }

    return (
        <>
            <div className={cN} {...props}>
                <div className={styles.accent} data-type={type} style={style}>
                    {inner}
                </div>
                {children}
            </div>
        </>
    );
}

export function OverlayContent({
    children,
    className,
    ...props
}: HTMLAttributes<HTMLDivElement>) {
    let cN = styles.content;
    if (className) {
        cN += " " + className;
    }

    return (
        <div className={cN} {...props}>
            {children}
        </div>
    );
}

export type Position =
    | "top"
    | "right"
    | "bottom"
    | "left"
    | "top-left"
    | "top-right"
    | "bottom-left"
    | "bottom-right";

export function OverlayHandles({
    positions,
    onMove,
    type,
    ...props
}: {
    positions: Position[];
    onMove: (e: MouseEvent, pos: Position) => void;
    type?: "round" | "square";
    subRect?: Rect;
} & HTMLAttributes<HTMLDivElement>) {
    return (
        <>
            {positions.map((pos) => {
                return (
                    <DragMove
                        key={pos}
                        onDragMove={(e) => {
                            onMove(e, pos);
                        }}
                        className={styles.handle}
                        data-position={pos}
                        data-type={type}
                        {...props}
                    ></DragMove>
                );
            })}
        </>
    );
}

/**
 * Custom hook that adds a snip overlay to the editor.
 * @param snip The snip to be rendered in the overlay.
 * @param overlayWorker The overlay worker responsible for managing the overlay.
 */
export function useSnipOverlay(
    snip?: BaseSnip,
    pageIdx: number = 0,
    bind_to_dom: boolean = true,
) {
    const { bindOverlayToRect } = usePlacementContext();
    const { events } = useQueuedSnipsContext();
    const { workers } = useWorkerContext();

    useEffect(() => {
        const oWorker = workers.get(pageIdx)?.overlay;
        if (!snip || !oWorker) return;

        const render_snip: RenderFunction = (ctx) => {
            ctx.save();

            // clip page coords
            ctx.beginPath();
            const pagesize = oWorker.viewport.getPageSize();
            ctx.rect(0, 0, pagesize.width, pagesize.height);
            ctx.clip();

            // render snip
            snip?.render(ctx);

            ctx.restore();
        };

        // Handle snip updates
        const controller = new AbortController();
        events?.addEventListener(
            "updated",
            (e) => {
                if (e.snip_id !== snip.id) return;
                render_snip.dirty = true;
            },
            { signal: controller.signal },
        );

        snip.overlay = true;
        let promise: Promise<void>;
        if (hasOwnProperty(snip, "ready")) {
            promise = (snip as ImageSnip).ready?.then(() => {
                oWorker.add(render_snip);
            });
        } else {
            oWorker.add(render_snip);
        }

        return () => {
            snip.overlay = false;
            controller.abort();
            if (promise) {
                promise
                    .then(() => {
                        oWorker.remove(render_snip);
                        oWorker.rerender();
                    })
                    .catch(console.error);
            } else {
                oWorker.remove(render_snip);
                oWorker.rerender();
            }
        };
    }, [events, pageIdx, snip, workers]);

    useEffect(() => {
        if (bind_to_dom && snip) {
            bindOverlayToRect(snip);
        }
    }, [snip, bindOverlayToRect, bind_to_dom]);
}

/**
 * Placement button for any snip overlay that may include
 * placement functionality.
 *
 * @param children - The content to be rendered inside the button.
 * @param onClick - The event handler for the button's click event.
 * @param className - Additional CSS class names for the button.
 * @param props - Additional HTML attributes for the button element.
 * @returns The rendered overlay place button component.
 */
export function OverlayPlaceButton({
    onPlace,
    className,
    ...props
}: HTMLAttributes<HTMLButtonElement> & {
    onPlace: () => void;
    onPlaceCopy?: () => void;
}) {
    let cN = styles.place;
    if (className) {
        cN += " " + className;
    }

    return (
        <Button className={cN} variant="primary" onClick={onPlace} {...props}>
            Place
        </Button>
    );
}
