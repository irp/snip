export default function Loading(props: React.HTMLAttributes<HTMLDivElement>) {
    return (
        <div
            className="d-flex justify-content-center align-items-center w-100"
            {...props}
        >
            <div className="spinner-grow" role="status"></div>
        </div>
    );
}
