// Function to logout user
export async function logoutUser() {
    const response = await fetch("/api/account/logout", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
    });
    if (response.status === 200) {
        return true;
    }
    return false;
}
