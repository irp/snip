"use client";
import { useEffect, useState } from "react";

export default function PrettyPrintJson({ ...props }) {
    const [data, setData] = useState<string>();
    const [copied, setCopied] = useState(false);

    useEffect(() => {
        setData(JSON.stringify(props, null, 2));
    }, [props]);

    return (
        <div style={{ position: "relative" }}>
            <code>
                <button
                    disabled={copied}
                    className="btn btn-outline-primary btn-sm"
                    onClick={() => {
                        if (!navigator.clipboard) {
                            alert("Cannot copy to clipboard");
                            return;
                        }
                        navigator.clipboard.writeText(data!);
                        setCopied(true);
                        setTimeout(() => setCopied(false), 10000);
                    }}
                    style={{ position: "absolute", right: "0", width: "80px" }}
                >
                    {copied ? "Copied" : "Copy"}
                </button>
                <pre>{data}</pre>
            </code>
        </div>
    );
}
