"use client";
import useFileMap from "lib/hooks/useFileMap";
import {
    createContext,
    Dispatch,
    SetStateAction,
    useContext,
    useMemo,
    useState,
} from "react";

import { ID } from "@snip/database/types";

import { ContextMenuState } from "components/books/browser/BrowserContextMenu";

export const BrowserContext = createContext<BrowserContext | null>(null);

interface BrowserContext {
    // Root folder and file map
    rootFolderId: FileID;
    fileMap?: FileMap;

    // Current folder
    currentPath: FileData[];
    currentFolder?: FileData;
    currentFolderId: FileID;
    setCurrentFolderId: Dispatch<SetStateAction<FileID>>;
    ascendToParentFolder: () => void;
    moveFileToFolder: (fileId: FileID, folderId: FileID) => void;

    // Order of the books
    order: Order;
    setOrder: Dispatch<SetStateAction<Order>>;
    fileMapOrder: FileID[];
    nHidden: number;

    // Current view
    view: View;
    setView: Dispatch<SetStateAction<View>>;

    // Context menu
    contextMenuState?: ContextMenuState;
    setContextMenuState: Dispatch<SetStateAction<ContextMenuState | undefined>>;
    onContextMenuFile: (
        e: React.MouseEvent<HTMLDivElement, MouseEvent>,
        file: FileData,
    ) => void;

    deleteFolder: (folderId: FileID) => Promise<void>;
    createFolder: (
        name: string,
        comment: string,
        parentId?: ID,
    ) => Promise<void>;

    isLoading: boolean;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    error: any;
}

/** BooksBrowser is a context provider for the general setup of the book browser.
 * It provides the root folder id, the file map, and the current folder id.
 * It also provides a function to set the current folder id and a way to ascend
 * to the parent folder.
 */
export function BooksBrowserContextProvider({
    rootFolderId = "root",
    children,
}: {
    rootFolderId: FileID;
    children: React.ReactNode;
}) {
    const {
        fileMap,
        delete_folder,
        move_file_to_folder,
        create_folder,
        isLoading,
        error,
    } = useFileMap();

    /** The current folder id is the id of the folder that is currently
     * being displayed.
     */
    const [currentFolderId, setCurrentFolderId] = useState(rootFolderId);
    const currentFolder = fileMap?.[currentFolderId];
    const currentPath = useMemo(() => {
        if (isLoading || !fileMap) return [];
        const path: FileData[] = [];
        let currentId = currentFolderId;
        while (currentId) {
            path.push(fileMap[currentId]!);
            if (currentId === rootFolderId) {
                break;
            }
            currentId = fileMap[currentId]!.parentId!;
        }
        return path.reverse();
    }, [currentFolderId, fileMap, rootFolderId, isLoading]);

    /** Current view (list,grid)
     */
    const [view, setView] = useState<View>("list");

    // ascendToParentFolder ascends to the parent folder of the current folder.
    const ascendToParentFolder = () => {
        if (isLoading || !fileMap) return;
        const currentFolder = fileMap[currentFolderId]!;
        if (currentFolder.parentId) {
            setCurrentFolderId(currentFolder.parentId);
        }
    };

    /** Sort the books depending on the state
     */
    const [order, setOrder] = useState<Order>({
        by: "last_updated",
        asc: false,
        foldersFirst: true,
        showFinished: true,
        search: "",
    });
    const { fileMapOrder, nHidden } = useMemo(() => {
        if (isLoading || !fileMap) return { fileMapOrder: [], nHidden: 0 };
        let fileOrder = Object.values(fileMap);

        // Filter out all files not in current folder
        fileOrder = fileOrder.filter(
            (file) =>
                file.parentId === currentFolderId ||
                currentFolder!.childrenIds!.includes(file.id),
        );

        // Filter out folders and finished books if necessary
        if (!order.showFinished) {
            fileOrder = fileOrder.filter((file) => !file.finished);
        }
        let folders: FileData[] = [];
        if (order.foldersFirst) {
            folders = fileOrder.filter((file) => file.isDir);
            fileOrder = fileOrder.filter((file) => !file.isDir);
        }

        // Filter out all files that don't match the search
        let nHidden = 0;
        if (order.search !== "") {
            nHidden = fileOrder.length;
            fileOrder = fileOrder.filter((file) =>
                file.name.toLowerCase().includes(order.search.toLowerCase()),
            );
            nHidden -= fileOrder.length;
        }

        fileOrder = fileOrder.sort(compareFunctionFactory(order));

        // Insert folders at the beginning if necessary
        if (order.foldersFirst) {
            folders = folders.sort(compareFunctionFactory(order));
            fileOrder = [...folders, ...fileOrder];
        }

        return {
            fileMapOrder: fileOrder.map((file) => file.id),
            nHidden,
        };
    }, [currentFolder, currentFolderId, fileMap, isLoading, order]);

    /** Context menu for the files
     *
     */
    const [contextMenuState, setContextMenuState] =
        useState<ContextMenuState>();
    const onContextMenuFile = (
        e: React.MouseEvent<HTMLDivElement, MouseEvent>,
        file: FileData,
    ) => {
        e.preventDefault();

        // TODO figure out way to hide context menu when clicking outside
        setContextMenuState({
            file,
            x: e.clientX,
            y: e.clientY,
            visible: true,
        });
    };

    const browserContext: BrowserContext = {
        rootFolderId,
        fileMap,

        currentPath,
        currentFolder,
        currentFolderId,
        setCurrentFolderId,
        ascendToParentFolder,
        moveFileToFolder: move_file_to_folder,

        order,
        setOrder,
        fileMapOrder,
        nHidden,

        view,
        setView,

        contextMenuState,
        setContextMenuState,
        onContextMenuFile,
        deleteFolder: delete_folder,
        createFolder: create_folder,
        isLoading,
        error,
    };

    return (
        <BrowserContext.Provider value={browserContext}>
            {children}
        </BrowserContext.Provider>
    );
}

function compareFunctionFactory(
    order: Order,
): (a: FileData, b: FileData) => number {
    return (a: FileData, b: FileData): number => {
        if (order.by == "name") {
            // Using localeCompare to sort by name
            const r = order.asc
                ? b.name.localeCompare(a.name, undefined, {
                      ignorePunctuation: true,
                      sensitivity: "base",
                  })
                : a.name.localeCompare(b.name, undefined, {
                      ignorePunctuation: true,
                      sensitivity: "base",
                  });
            return r;
        } else if (order.by == "created") {
            if (!a.created || !b.created) return 0;
            if (a.created < b.created!) {
                return order.asc ? 1 : -1;
            } else if (a.created > b.created) {
                return order.asc ? -1 : 1;
            } else {
                return 0;
            }
        } else if (order.by == "last_updated") {
            if (!a.last_updated || !b.last_updated) return 0;
            if (a.last_updated < b.last_updated) {
                return order.asc ? -1 : 1;
            } else if (a.last_updated > b.last_updated) {
                return order.asc ? 1 : -1;
            } else {
                return 0;
            }
        } else if (order.by == "numPages") {
            if (!a.numPages || !b.numPages) return 0;
            if (a.numPages < b.numPages) {
                return order.asc ? 1 : -1;
            } else if (a.numPages > b.numPages) {
                return order.asc ? -1 : 1;
            } else {
                return 0;
            }
        }
        return 0;
    };
}

export const useBooksBrowserContext = () => {
    const context = useContext(BrowserContext);
    if (!context) {
        throw new Error(
            "useBooksBrowserContext must be used within a BooksBrowserContextProvider",
        );
    }
    return context;
};
