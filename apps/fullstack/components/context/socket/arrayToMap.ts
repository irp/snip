"use client";
type Data<T> = T extends { id: number } ? T : never;

export function convertArrayToMap<T>(
    dataArray: Data<T>[],
): Map<number, Data<T>> {
    const dataMap: Map<number, Data<T>> = new Map();
    dataArray.forEach((item) => {
        dataMap.set(item.id, item);
    });
    return dataMap;
}
