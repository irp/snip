"use client";
import {
    createContext,
    ReactNode,
    SetStateAction,
    useContext,
    useEffect,
    useRef,
    useState,
} from "react";

import { ID, PageDataInsert, PageDataRet } from "@snip/database/types";

import { useSocketContext } from "../useSocketContext";
import { convertArrayToMap } from "./arrayToMap";

interface PagesContext {
    pages: Map<ID, PageDataRet>;
    pagesRef: React.MutableRefObject<Map<ID, PageDataRet>>;
    pagesArray: PageDataRet[];
    createPage: (
        background_type_id?: number | null,
        referenced_page_id?: number | null,
    ) => Promise<PageDataRet>;
    updatePage: (page: PageDataRet) => void;
    deletePage: (page_id: ID) => void;
}

const PagesContext = createContext<PagesContext | null>(null);

/** Has to be nested inside a socket context to work
 * properly.
 */
export function PagesContextProvider({
    init_pages,
    children,
}: {
    children: ReactNode;
    init_pages: PageDataRet[];
}) {
    const { socket, isConnected, book_id } = useSocketContext();

    /** The pages of the book
     * updates with creation or deletion from the server
     */
    const [pages, _setPages] = useState<Map<ID, PageDataRet>>(() =>
        convertArrayToMap(init_pages),
    );

    const pagesRef = useRef(pages);

    const setPages = (pages: SetStateAction<Map<ID, PageDataRet>>) => {
        if (pages instanceof Function) {
            pagesRef.current = pages(pagesRef.current);
        }
        _setPages(pagesRef.current);
    };
    // Register event listeners
    useEffect(() => {
        if (!isConnected || !socket) return;
        function onNewPage(page: PageDataRet) {
            page = parsePageData(page);
            setPages((pages) => {
                pages.set(page.id, page);
                return new Map(pages);
            });
        }
        function onDeletedPage(page_id: ID) {
            setPages((pages) => {
                pages.delete(page_id);
                return new Map(pages);
            });
        }
        function onUpdatedPage(page: PageDataRet) {
            page = parsePageData(page);
            setPages((pages) => {
                pages.set(page.id, page);
                return new Map(pages);
            });
        }
        function onUpdatedPagePreview(page_id: ID) {
            setPages((pages) => {
                const page = pages.get(page_id);
                if (!page) return pages;
                page.last_updated = new Date();
                return new Map(pages);
            });
        }

        // Page events
        socket.on("book:newPage", onNewPage);
        socket.on("book:deletedPage", onDeletedPage);
        socket.on("book:updatedPage", onUpdatedPage);
        socket.on("book:updatedPage:preview", onUpdatedPagePreview);

        return () => {
            socket.off("book:newPage", onNewPage);
            socket.off("book:deletedPage", onDeletedPage);
            socket.off("book:updatedPage", onUpdatedPage);
            socket.off("book:updatedPage:preview", onUpdatedPagePreview);
        };
    }, [socket, isConnected]);

    async function createPage(
        background_type_id: number | null = null,
        referenced_page_id: number | null = null,
    ) {
        if (!book_id || !socket) {
            throw new Error("Cant create page without book set!");
        }
        const newPageI: PageDataInsert = {
            book_id: book_id,
            created: new Date(),
            last_updated: new Date(),
            background_type_id,
            referenced_page_id,
        };

        // Send the new page to the server
        const newPage = await new Promise<PageDataRet>((resolve, reject) => {
            socket.emit("book:newPage", newPageI, (response) => {
                if (response.ok === false) {
                    reject(response.error);
                } else {
                    resolve(response.data);
                }
            });
        });
        setPages((pages) => {
            pages.set(newPage.id, newPage);
            return new Map(pages);
        });
        return newPage;
    }

    return (
        <PagesContext.Provider
            value={{
                pages,
                pagesArray: Array.from(pages.values()),
                createPage,
                updatePage: (page) => {
                    if (!socket) return;
                    setPages((pages) => {
                        pages.set(page.id, page);
                        return new Map(pages);
                    });
                    socket.emit("book:updatePage", page, (response) => {
                        if (response.ok === false) {
                            console.error(response.error);
                            return;
                        }
                        page = parsePageData(response.data);
                        setPages((pages) => {
                            pages.set(page.id, page);
                            return new Map(pages);
                        });
                    });
                },
                deletePage: (page_id) => {
                    if (!socket) return;
                    setPages((pages) => {
                        pages.delete(page_id);
                        return new Map(pages);
                    });
                    socket.emit("book:deletePage", page_id, (response) => {
                        if (response.ok === false) {
                            console.error(response.error);
                            return;
                        }
                    });
                },
                pagesRef,
            }}
        >
            {children}
        </PagesContext.Provider>
    );
}

export function usePagesContext() {
    const context = useContext(PagesContext);
    if (!context) throw new Error("Cant use PagesContext outside provider!");
    return context;
}

// Reducers
export function usePagesRef() {
    const { pagesRef } = usePagesContext();
    return pagesRef;
}

function parsePageData(page: PageDataRet) {
    page.created = page.created ? new Date(page.created) : new Date();
    page.last_updated = page.last_updated
        ? new Date(page.last_updated)
        : new Date();
    return page;
}
