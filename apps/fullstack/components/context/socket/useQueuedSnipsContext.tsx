"use client";

import {
    createContext,
    Dispatch,
    SetStateAction,
    useCallback,
    useContext,
    useEffect,
    useMemo,
    useState,
} from "react";

import { hasOwnProperty } from "@snip/common";
import { TypedEventTarget } from "@snip/common/eventTarget";
import { get_snip_from_data_allow_unknown } from "@snip/snips";
import type { ArraySnip } from "@snip/snips/general/array";
import {
    BaseData,
    BaseSnip,
    BaseView,
    SnipData,
} from "@snip/snips/general/base";
import { ImageSnip } from "@snip/snips/general/image";
import { ImageSnipLegacy } from "@snip/snips/general/legacy/image";
import { LinkSnip } from "@snip/snips/general/link";

import { useSocketContext } from "../useSocketContext";
import { convertArrayToMap } from "./arrayToMap";

import { useEditorContext } from "components/editor/context";

type ID = number;

interface QueuedSnipEventTypeMap {
    updated: QueuedSnipEvent<"updated">;
}

class QueuedSnipEvent<T extends keyof QueuedSnipEventTypeMap> extends Event {
    snip_id: ID;
    constructor(snip_id: ID, eType: T) {
        super(eType);
        this.snip_id = snip_id;
    }
}

export type QueuedSnipsEventTarget = TypedEventTarget<QueuedSnipEventTypeMap>;

interface QueuedSnipsContext {
    snips: Map<ID, BaseSnip>;
    snipsArray: BaseSnip[];
    addTempSnip: (snip: BaseSnip) => void;
    removeQueuedSnip: (
        snip_id: ID,
        page_id?: ID,
        only_local?: boolean,
        nested?: boolean,
    ) => void;
    transformSnip: (
        snip_id: ID,
        transform: (snip: BaseSnip) => BaseSnip,
        upsert?: boolean,
    ) => Promise<BaseSnip>;
    notSeenSnips: ID[];
    setNotSeenSnips: Dispatch<SetStateAction<ID[]>>;

    // Event target for snip events these includes both
    // local and remote events i.e. changes to a snip
    // if applied via the transform function
    events: QueuedSnipsEventTarget;
}

const QueuedSnipsContext = createContext<QueuedSnipsContext | null>(null);

export function QueuedSnipsContextProvider({
    init_snips,
    children,
}: {
    children: React.ReactNode;
    init_snips: SnipData<BaseData, BaseView>[];
}) {
    const { socket, isConnected } = useSocketContext();
    const { setActiveSnipId } = useEditorContext();

    const [snips, setSnips] = useState<Map<ID, BaseSnip>>(() =>
        convertArrayToMap(init_snips.map(loadSnip)),
    );

    const [events] = useState<QueuedSnipsEventTarget>(
        () => new TypedEventTarget<QueuedSnipEventTypeMap>(),
    );

    // Ids of snips that have not been seen
    // defaults to empty array
    // and is updated when snips are added
    // to the snips map via newQueuedSnip
    const [notSeenSnips, setNotSeenSnips] = useState<ID[]>([]);

    const deleteSnip = useCallback(
        (snips: Map<ID, BaseSnip>, snip_id: ID) => {
            snips.delete(snip_id);
            setActiveSnipId((activeSnipId) => {
                if (activeSnipId === snip_id) {
                    return null;
                }
                return activeSnipId;
            });
            setNotSeenSnips((seenSnips) => {
                return [...seenSnips.filter((id) => id !== snip_id)];
            });
        },
        [setActiveSnipId],
    );

    const newQueuedSnip = useCallback(
        (snipData: SnipData<BaseData, BaseView>) => {
            console.log("newQueuedSnip", snipData);
            if (snipData.page_id !== undefined && snipData.page_id !== null) {
                // skip placed snips, they should not end up here but
                // just in case
                return;
            }
            const snip = loadSnip(snipData);
            if (!snip) return;

            setSnips((snips) => {
                snips.set(snip.id, snip);
                setNotSeenSnips((seenSnips) => {
                    if (seenSnips.includes(snip.id)) {
                        return seenSnips;
                    }
                    return [...seenSnips, snip.id];
                });
                // Only keep parent snip if it there are nested snips
                if (hasOwnProperty(snip, "snips")) {
                    (snip as ArraySnip).snips.forEach((s) => {
                        deleteSnip(snips, s.id);
                    });
                }
                if (hasOwnProperty(snip, "snip")) {
                    deleteSnip(snips, (snip as LinkSnip).snip.id);
                }

                return new Map(snips);
            });
        },
        [deleteSnip],
    );

    const removeQueuedSnip = useCallback(
        (snip_id: ID, _page_id?: ID, only_local = true, nested = true) => {
            setSnips((snips) => {
                if (!snips.has(snip_id)) {
                    return snips;
                }
                if (!only_local) {
                    const s = snips.get(snip_id)!;
                    s.socket = socket;
                    s.remove(nested);
                }

                deleteSnip(snips, snip_id);
                return new Map(snips);
            });
        },
        [deleteSnip, socket],
    );

    const updateQueuedSnip = useCallback(
        (snipData: SnipData<BaseData, BaseView>) => {
            if (snipData.page_id !== undefined && snipData.page_id !== null) {
                return;
            }
            const snip = loadSnip(snipData);
            if (!snip) return;

            setSnips((snips) => {
                snips.set(snip.id, snip);

                // Only keep parent snip if it there are nested snips
                if (hasOwnProperty(snip, "snips")) {
                    (snip as ArraySnip).snips.forEach((s) => {
                        deleteSnip(snips, s.id);
                    });
                }
                if (hasOwnProperty(snip, "snip")) {
                    deleteSnip(snips, (snip as LinkSnip).snip.id);
                }
                return new Map(snips);
            });
            events.dispatchEvent(new QueuedSnipEvent(snip.id, "updated"));
        },
        [deleteSnip, events],
    );

    /**
     * Changes a snip by applying a transformation function to it. Deletes the old snip and adds the new one
     * returned by the transformation function. Also applies database/socket updates if upsert is true.
     *
     * @param snip_id - The ID of the snip to be changed.
     * @param transform - A function that takes the current snip and returns a new transformed snip.
     * @throws Will throw an error if the snip with the given ID is not found.
     * @returns void
     */
    const transformSnip = useCallback(
        async (
            snip_id: ID,
            transform: (snip: BaseSnip) => BaseSnip,
            upsert = true,
        ) => {
            const snip = snips.get(snip_id);
            if (!snip) {
                throw new Error(`Snip with id ${snip_id} not found`);
            }
            const new_snip = transform(snip);
            const id_after_transform = new_snip.id;
            setSnips((snips) => {
                if (id_after_transform !== snip_id) {
                    // This might remove the currently active snip
                    deleteSnip(snips, snip_id);
                }
                snips.set(new_snip.id, new_snip);
                return new Map(snips);
            });

            if (upsert) {
                new_snip.socket = socket;
                await new_snip.upsert();

                setSnips((snips) => {
                    if (id_after_transform === new_snip.id) {
                        return snips;
                    }
                    // id might change because of upsert
                    deleteSnip(snips, id_after_transform);
                    snips.set(new_snip.id, new_snip);
                    return new Map(snips);
                });
            }
            events.dispatchEvent(new QueuedSnipEvent(snip_id, "updated"));
            return new_snip;
        },
        [deleteSnip, events, snips, socket],
    );

    // Register event listeners
    // to sync snips with the server
    useEffect(() => {
        if (!isConnected || !socket) return;

        socket.on("snip:inserted", newQueuedSnip);
        socket.on("snip:removed", removeQueuedSnip);
        socket.on("snip:placed", removeQueuedSnip);
        socket.on("snip:updated", updateQueuedSnip);

        return () => {
            socket.off("snip:inserted", newQueuedSnip);
            socket.off("snip:removed", removeQueuedSnip);
            socket.off("snip:placed", removeQueuedSnip);
            socket.off("snip:updated", updateQueuedSnip);
        };
    }, [
        socket,
        isConnected,
        newQueuedSnip,
        removeQueuedSnip,
        updateQueuedSnip,
    ]);

    const snipsArray = useMemo(() => {
        const array = Array.from(snips.values());
        array
            .sort((a, b) => a.created.getTime() - b.created.getTime())
            .reverse();
        return [...array];
    }, [snips]);

    const addTempSnip = useCallback((snip: BaseSnip) => {
        console.log("addTempSnip", snip);
        if (snip.id > 0) {
            throw new Error("Cant add a temporary snip with a potential DB id");
        }
        setSnips((snips) => {
            snips.set(snip.id, snip);
            return new Map(snips);
        });
    }, []);

    return (
        <QueuedSnipsContext.Provider
            value={{
                snips,
                snipsArray,
                addTempSnip,
                transformSnip,
                removeQueuedSnip,
                notSeenSnips,
                setNotSeenSnips,
                events,
            }}
        >
            {children}
        </QueuedSnipsContext.Provider>
    );
}

export function useQueuedSnipsContext() {
    const context = useContext(QueuedSnipsContext);
    if (!context) {
        throw new Error(
            "useQueuedSnipsContext must be used within a QueuedSnipsContextProvider",
        );
    }
    return context;
}

function loadSnip(snipData: SnipData<BaseData, BaseView>) {
    let snip = get_snip_from_data_allow_unknown(snipData);
    if (snip instanceof ImageSnipLegacy) {
        snip = ImageSnip.from_legacy(snip);
    }
    return snip;
}
