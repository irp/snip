"use client";

import { io, Socket } from "socket.io-client";

import { isBrowser, isWorker } from "@snip/common/environment";
import { ID } from "@snip/database/types";
import type {
    ClientToServerEvents,
    ServerToClientEvents,
} from "@snip/socket/types";

// Global map of sockets to book_ids
const SOCKETS = new Map<ID, Socket>();

export async function getSocket(
    book_id: ID,
): Promise<Socket<ServerToClientEvents, ClientToServerEvents>> {
    if (SOCKETS.has(book_id)) {
        return SOCKETS.get(book_id)!;
    } else {
        if (isWorker() || isBrowser()) {
            //const token = await fetchToken(book_id);
            const socket = io("/book-" + book_id, {
                path: "/socket/",
                transports: ["websocket"],
                autoConnect: false,
                withCredentials: true,
            });

            socket.on("error", (error) => {
                console.error("Socket error:", error);
            });

            SOCKETS.set(book_id, socket);
            return socket;
        }
    }
    return io();
}
