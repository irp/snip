"use client";
import { usePathname, useSearchParams } from "next/navigation";
import {
    createContext,
    Dispatch,
    MutableRefObject,
    SetStateAction,
    useCallback,
    useContext,
    useEffect,
    useMemo,
    useRef,
    useState,
} from "react";

import { ID } from "@snip/database/types";

import { usePagesContext } from "./socket/usePagesContext";
import { WorkerContextProvider } from "./viewer/useWorkerContext";

interface CanvasRefs {
    // refs for the canvas wrappers
    // i.e. the pages
    pageRef: MutableRefObject<EleMap<HTMLDivElement>>;
}

interface ViewerContext extends CanvasRefs {
    // The active page ids
    activePageIds: ID[];
    setActivePageIds: Dispatch<SetStateAction<ID[]>>;

    // The active snip id (if in snip view mode)
    activeSnipId?: ID;
    setActiveSnipId: Dispatch<SetStateAction<ID | undefined>>;

    // The active book id
    book_id: ID;
}

const ViewerContext = createContext<ViewerContext | null>(null);

export type EleMap<T> = Map<number, T>;

export function ViewerContextProvider({
    children,
    book_id,
    initialActivePageIds,
}: {
    children: React.ReactNode;
    book_id: ID;
    initialActivePageIds: ID[];
}) {
    const [activePageIds, setActivePageIds] =
        useActivePageIds(initialActivePageIds);
    const [activeSnipId, setActiveSnipId] = useState<ID>();

    const pageRef = useRef<EleMap<HTMLDivElement>>(new Map());

    const ret: ViewerContext = useMemo(() => {
        return {
            book_id,
            activePageIds,
            setActivePageIds,
            activeSnipId,
            setActiveSnipId,
            pageRef,
        };
    }, [book_id, activePageIds, setActivePageIds, activeSnipId]);

    return (
        <ViewerContext.Provider value={ret}>
            <WorkerContextProvider n={activePageIds.length}>
                {children}
            </WorkerContextProvider>
        </ViewerContext.Provider>
    );
}

export function useViewerContext() {
    const context = useContext(ViewerContext);
    if (!context) {
        throw new Error(
            "useViewerContext must be used within a ViewerContextProvider",
        );
    }
    return context;
}

/** This wrapper handles the searchParams update for
 * the active page ids and allows to select a
 * page by searchParams
 */
function useActivePageIds(
    initialActivePageIds: ID[],
): [ID[], Dispatch<SetStateAction<ID[]>>] {
    const pathname = usePathname();
    const searchParams = useSearchParams();

    const { pages } = usePagesContext();

    const [activePageIds, setActivePageIds] =
        useState<ID[]>(initialActivePageIds);

    const setSearchParams = useCallback(
        (page_numbers: number[]) => {
            const search = new URLSearchParams(searchParams.toString());

            //Remove all page params
            search.delete("page");
            for (const no of page_numbers) {
                search.append("page", (no + 1).toString());
            }
            window.history.replaceState(
                {},
                "",
                `${pathname}?${search.toString()}`,
            );
        },
        [pathname, searchParams],
    );

    useEffect(() => {
        if (!pages || pages.size < 1) return;
        const pagenumbers: number[] = [];
        for (const id of activePageIds) {
            pagenumbers.push(pages.get(id)!.page_number!);
        }

        setSearchParams(pagenumbers);
    }, [activePageIds, pages, setSearchParams]);

    return [activePageIds, setActivePageIds];
}

/**
 * Sets a reference to an element in a mutable ref object at the specified index.
 *
 * @template T - The type of the element.
 * @param ref - The mutable ref object to set the reference in.
 * @param idx - The index at which to set the reference.
 * @returns A function that sets the reference to the element.
 */
export function setRef<T>(ref: MutableRefObject<EleMap<T>>, idx: number) {
    return (element: T) => {
        if (!element) return;
        if (!ref.current) ref.current = new Map<number, T>();
        ref.current.set(idx, element);
    };
}
