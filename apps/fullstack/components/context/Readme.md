# Context

We use the context to pass data to the components without having to pass it through the props. This is a typical react pattern that allows us to avoid prop drilling.
See the [official documentation](https://react.dev/learn/passing-data-deeply-with-context) for more information.


## Sockets

The socket context is used to pass the established client socket connection to the components. This is useful because we can have a single socket connection that is shared across the entire application. 

A socket connection is always bound to a book, and there is an additional contexts to bind page updates from
the socket to react state.

```tsx

const book_id = fetch_book_id()

<SocketContextProvider book_id={book_id}>
    <PageContextProvider>
    </PageContextProvider>
</SocketContextProvider>
```


## Viewer

The viewer context is used to manage the currently active page(s). Inside the viewer context we also have a sidebar, tool and worker context. The sidebar context is used to manage the state of the sidebar, the tool context is used to manage the state of the tools, and the worker context is used to manage the state of the page worker.

The viewer context wraps all the context providers that are used to manage the viewer state.

```tsx
<ViewerContextProvider>
</ViewerContextProvider>
```