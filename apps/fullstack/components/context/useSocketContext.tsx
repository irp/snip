"use client";
import { createContext, useContext, useEffect, useState } from "react";
import { Socket } from "socket.io-client";

import type { ID } from "@snip/database/types";
import type {
    ClientToServerEvents,
    ServerToClientEvents,
} from "@snip/socket/types";

import { getSocket } from "./socket";

import { LatencyModal } from "components/common/other/ConnetionIssues";

export interface OnlineUser {
    page_ids: Set<ID> | ID[];
    email: string;
    color: [number, number, number, number];
    id: ID;
    self?: boolean;
}

interface SocketContext {
    socket?: Socket<ServerToClientEvents, ClientToServerEvents>;
    book_id?: ID;
    isConnected: boolean | null;
    onlineUsers: Map<ID, OnlineUser>;
    pageToOnlineUsers: Map<ID, OnlineUser[]>;
}

const SocketContext = createContext<SocketContext | null>(null);

/** Creates a socket connection to the main
 * namespace of the book. This does not join
 * any pages i.e. rooms. This is intentional
 * as only the worker(s) should join the pages
 * directly.
 * @param book_id - The ID of the book to connect to
 */
export function useSocket(book_id: ID) {
    /** Socket variables
     * normally they are not used but
     * they are here mainly for debugging purposes
     */
    const [socket, setSocket] =
        useState<Socket<ServerToClientEvents, ClientToServerEvents>>();
    const [isConnected, setIsConnected] = useState<boolean | null>(null);

    useEffect(() => {
        // Create the socket
        getSocket(book_id).then((newSocket) => {
            setSocket((socket) => {
                if (socket) {
                    socket.close();
                }
                return newSocket;
            });
        });
        return () => {
            // Close the socket
            getSocket(book_id)?.then((s) => s.close());
        };
    }, [book_id]);

    // Register connection and disconnection events
    useEffect(() => {
        if (!socket) return;

        function onConnect() {
            console.log("[Socket] Connected to book", book_id);
            setIsConnected(true);
        }

        function onDisconnect() {
            console.log("[Socket] Disconnected from book", book_id);
            setIsConnected(false);
        }

        function reConnect() {
            if (socket?.connected !== true) {
                socket?.connect();
            }
        }

        socket.on("connect", onConnect);
        socket.on("disconnect", onDisconnect);
        socket.connect();
        window.addEventListener("online", reConnect);
        window.addEventListener("visibilitychange", reConnect);

        return () => {
            socket.off("connect", onConnect);
            socket.off("disconnect", onDisconnect);
            window.removeEventListener("online", reConnect);
            window.removeEventListener("visibilitychange", reConnect);
            socket.disconnect();
        };
    }, [book_id, socket]);

    return { socket, isConnected };
}

export function useOnlineUsers(
    isConnected: boolean | null,
    socket?: Socket<ServerToClientEvents, ClientToServerEvents>,
) {
    /** Other users currently online and viewing pages
     * of the book
     */
    const [onlineUsers, setOnlineUsers] = useState<Map<ID, OnlineUser>>(
        new Map(),
    );

    //Register online users after connection
    useEffect(() => {
        if (!isConnected || !socket) return;

        socket.emit("users:getOnline", (users) => {
            if (users.ok === false) {
                console.error(users.error);
                return;
            }

            console.log("[Socket] Online users", users);
            setOnlineUsers(
                users.data.reduce(
                    (map, user) => map.set(user.id, user),
                    new Map(),
                ),
            );
        });

        function onUserDisconnect(user_id: ID) {
            setOnlineUsers((users) => {
                users.delete(user_id);
                return new Map(users);
            });
        }

        function onUserUpdate(user: OnlineUser) {
            console.log("[Socket] User update", user);
            setOnlineUsers((users) => {
                //only update if the page_ids are different
                if (!users.has(user.id)) {
                    return new Map(users).set(user.id, user);
                }
                if (
                    !user.page_ids ||
                    users.get(user.id)?.page_ids !== user.page_ids
                ) {
                    return new Map(users).set(user.id, user);
                }
                return users;
            });
        }

        socket.on("users:disconnect", onUserDisconnect);
        socket.on("users:update", onUserUpdate);
        socket.on("users:connect", onUserUpdate);

        return () => {
            socket.off("users:disconnect", onUserDisconnect);
            socket.off("users:update", onUserUpdate);
            socket.off("users:connect", onUserUpdate);
        };
    }, [socket, isConnected]);

    return { onlineUsers };
}

export function SocketContextProvider({
    children,
    book_id,
}: {
    children: React.ReactNode;
    book_id: ID;
}) {
    const { socket, isConnected } = useSocket(book_id);
    const { onlineUsers } = useOnlineUsers(isConnected, socket);

    // Map page ids to online users
    const pageToOnlineUsers = new Map<ID, OnlineUser[]>();
    onlineUsers?.forEach((user) => {
        user?.page_ids?.forEach((page_id) => {
            let new_users = pageToOnlineUsers.get(page_id) || [];
            new_users.push(user);

            // filter out duplicates
            new_users = new_users.filter(
                (user, index, self) =>
                    index === self.findIndex((u) => u.id === user.id),
            );

            pageToOnlineUsers.set(page_id, new_users);
        });
    });

    return (
        <SocketContext.Provider
            value={{
                socket,
                isConnected,
                book_id,
                onlineUsers,
                pageToOnlineUsers,
            }}
        >
            <LatencyModal />
            {children}
        </SocketContext.Provider>
    );
}

export function useSocketContext() {
    const context = useContext(SocketContext);
    if (!context) throw new Error("Cant use SocketContext outside provider!");
    return context;
}
