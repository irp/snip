"use client";
import { createContext, useContext, useEffect, useState } from "react";

import { ViewPort } from "@snip/render/viewport";

import { useSocketContext } from "../useSocketContext";

import { OverlayWorker } from "components/editor/worker/overlayWorker";
import { PageWorker } from "components/editor/worker/pageWorker";

export type EleMap<T> = Map<number, T>;

interface WorkerContext {
    workers: EleMap<{
        main: PageWorker;
        overlay: OverlayWorker;
    }>;
}

const WorkerContext = createContext<WorkerContext>({
    workers: new Map(),
});

export function WorkerContextProvider({
    n,
    children,
}: {
    n: number;
    children: React.ReactNode;
}) {
    const { socket } = useSocketContext();

    const [workers, setWorkers] = useState<
        EleMap<{
            main: PageWorker;
            overlay: OverlayWorker;
        }>
    >(new Map());

    /* Scaling and create the workers dynamically */
    useEffect(() => {
        if (!socket) return;
        setWorkers((w) => {
            //Scale up
            if (w.size < n) {
                for (let i = w.size; i < n; i++) {
                    const viewport = new ViewPort();
                    w.set(w.size, {
                        main: new PageWorker(viewport),
                        overlay: new OverlayWorker(viewport, socket),
                    });
                }
                return new Map(w);
            }
            //Scale down
            else if (w.size > n) {
                for (let i = w.size - 1; i >= n; i--) {
                    const worker = w.get(i);
                    if (worker) {
                        Promise.all([
                            worker.main.terminate(),
                            worker.overlay.terminate(),
                        ]).then(() => {
                            w.delete(i);
                        });
                    }
                }
                return new Map(w);
            }
            return w;
        });
    }, [n, socket]);

    return (
        <WorkerContext.Provider value={{ workers }}>
            {children}
        </WorkerContext.Provider>
    );
}

export function useWorkerContext() {
    const context = useContext(WorkerContext);
    if (!context) {
        throw new Error(
            "useWorkerContext must be used within a WorkerContextProvider",
        );
    }
    return context;
}

export function useWorker(idx: number):
    | {
          worker: PageWorker;
          overlayWorker: OverlayWorker;
      }
    | {
          worker: undefined;
          overlayWorker: undefined;
      } {
    const { workers } = useWorkerContext();
    const w = workers?.get(idx);

    if (!w) {
        return {
            worker: undefined,
            overlayWorker: undefined,
        };
    }

    return {
        worker: w.main,
        overlayWorker: w.overlay,
    };
}

const OverlayWorkerContext = createContext<EleMap<OverlayWorker>>(new Map());

export function OverlayWorkerContextProvider({
    n,
    children,
}: {
    n: number;
    children: React.ReactNode;
}) {
    const [overlayWorker, setOverlayWorker] = useState<EleMap<OverlayWorker>>(
        new Map(),
    );

    useEffect(() => {
        setOverlayWorker((w) => {
            //Scale up
            if (w.size < n) {
                for (let i = w.size; i < n; i++) {
                    const viewport = new ViewPort();
                    w.set(w.size, new OverlayWorker(viewport, undefined));
                }
                return new Map(w);
            }
            //Scale down
            else if (w.size > n) {
                for (let i = w.size - 1; i >= n; i--) {
                    const worker = w.get(i);
                    if (worker) {
                        Promise.all([worker.terminate()]).then(() => {
                            w.delete(i);
                        });
                    }
                }
                return new Map(w);
            }
            return w;
        });
    }, [n]);

    return (
        <OverlayWorkerContext.Provider value={overlayWorker}>
            {children}
        </OverlayWorkerContext.Provider>
    );
}

export function useOverlayWorkerContext() {
    return useContext(OverlayWorkerContext);
}
