"use client";
import { fetcher } from "lib/fetcher";
import { useDebounce } from "lib/hooks/useDebounce";
import {
    createContext,
    Dispatch,
    SetStateAction,
    useContext,
    useState,
} from "react";
import useSWR from "swr";

import { ID } from "@snip/database/types";

import { useViewerContext } from "../useViewerContext";

interface SidebarContext {
    searchPages: string;
    setSearchPages: Dispatch<SetStateAction<string>>;
    filterPages: ID[];
    searchSnips: string;
    setSearchSnips: Dispatch<SetStateAction<string>>;
    filterSnips: ID[];
    isValidating: boolean;
}

const SidebarContext = createContext<SidebarContext | null>(null);

export function SidebarContextProvider({
    children,
}: {
    children: React.ReactNode;
}) {
    const { book_id } = useViewerContext();
    /** Logic for the search state
     * here we use debounce logic to delay inputs
     * -> better for performance and less requests
     */
    const [searchPages, setSearchPages] = useSearch();
    const [searchSnips, setSearchSnips] = useSearch();

    const { data: filterPages, isValidating: isValidatingPages } = useSWR(
        () => {
            return searchPages
                ? `/api/books/${book_id}/search/pages?search=${searchPages}`
                : undefined;
        },
        fetcher,
    );

    const { data: filterSnips, isValidating: isValidatingSnips } = useSWR(
        () => {
            return searchSnips
                ? `/api/snips/search?id=${book_id}&search=${searchSnips}`
                : undefined;
        },
        fetcher,
    );

    return (
        <SidebarContext.Provider
            value={{
                searchPages,
                setSearchPages,
                filterPages,
                searchSnips,
                setSearchSnips,
                filterSnips,
                isValidating: isValidatingPages || isValidatingSnips,
            }}
        >
            {children}
        </SidebarContext.Provider>
    );
}

export function useSidebarContext() {
    const context = useContext(SidebarContext);
    if (!context) throw new Error("Cant use SidebarContext outside provider!");
    return context;
}

/**
 * Custom hook for handling search functionality.
 * @param delay - The delay in milliseconds before performing the search.
 * @returns An array containing the debounced search value and a function to update the search value.
 */
function useSearch(delay = 500): [string, Dispatch<SetStateAction<string>>] {
    const [search, setSearch] = useState("");
    const debouncedSearch = useDebounce(search, delay);
    return [debouncedSearch, setSearch];
}
