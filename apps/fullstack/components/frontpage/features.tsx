import Section from "./section";

import styles from "./features.module.scss";

/** The features show cards with a short description of the features
 * of the application and a image.
 *
 */
export default function Features() {
    return (
        <Section>
            <div className={styles.wrapper}>
                <Card title="Seamless Integration">
                    Merge hand-crafted entries with computer-generated content
                    effortlessly, delivering a comprehensive overview of your
                    experiments. External software can be integrated to send
                    standardized &ldquo;snippets&rdquo; representing experiment
                    phases. You curate and annotate these snippets for a
                    complete record.
                </Card>
                <Card title="Intuitive User Interface">
                    Experience a user-friendly interface that simplifies
                    navigation and interaction. Whether you&apos;re new to
                    digital documentation or an experienced user, our intuitive
                    design ensures a seamless experience and a smooth transition
                    from analog to digital.
                </Card>
                <Card title="Efficient Experiment Queue">
                    Curate content in an organized queue, just like the
                    traditional printer output queue. Tailor your entries,
                    adjust formatting, and control details to maintain precision
                    and clarity.
                </Card>
                <Card title="Global Collaboration">
                    Our web-based platform enables real-time, distributed
                    collaboration for seamless teamwork and knowledge sharing.
                    Every person in your team can contribute to the same
                    project, and you can easily share your work with the world.
                </Card>
                <Card title="Transparent Design Philosophy">
                    <>
                        Rooted in openness, our philosophy facilitates content
                        preservation through individual hosting and deployment.
                        Our platform empowers you to take control of your data
                        and its longevity. You have the freedom to manage your
                        content, ensuring its availability for future reference.
                        Explore our open-source repository on{" "}
                        <a href="https://gitlab.gwdg.de/irp/snip">GitLab</a>.
                    </>
                </Card>
                <Card title="Continuous Evolution">
                    <>
                        Our platform evolves with your needs. Open-source
                        contributions and regular updates ensure that
                        you&apos;re equipped with the latest features and
                        enhancements to streamline your documentation process.
                        Stay informed about every change by exploring our{" "}
                        <a href="https://gitlab.gwdg.de/irp/snip/-/blob/main/CHANGELOG.md">
                            changelog
                        </a>
                        .
                    </>
                </Card>
            </div>
        </Section>
    );
}

interface CardProps {
    children: JSX.Element | JSX.Element[] | string;
    title?: string;
    img?: string;
}

function Card({ children, title }: CardProps) {
    return (
        <div className={styles.card_wrapper}>
            <div className={styles.card_content}>
                {title ? <h3>{title}</h3> : null}
                <p>{children}</p>
            </div>
        </div>
    );
}
