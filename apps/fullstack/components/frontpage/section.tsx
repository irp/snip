import { ReactNode } from "react";

import styles from "./frontpage.module.scss";

interface Props {
    children: ReactNode;
    accents?: boolean;
    fullsize?: boolean;
    className?: string;
}

/** A generic section which can be used in the frontpage
 * (Takes care of the styling and mobile responsiveness)
 */
export default function Section({
    children,
    accents,
    fullsize,
    className,
}: Props) {
    const accent_ele = accents ? (
        <>
            <div className={styles.accent1} />
            <div className={styles.accent2} />
        </>
    ) : null;

    return (
        <div
            className={
                styles.section_wrapper +
                ` ${accents ? styles.accented : ""}` +
                ` ${fullsize ? styles.fullsize : ""}`
            }
        >
            {accent_ele}
            <section
                className={styles.section + ` ${className ? className : ""}`}
            >
                {children}
            </section>
        </div>
    );
}
