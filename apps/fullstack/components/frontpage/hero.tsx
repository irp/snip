"use client";
import { logout } from "lib/actions/logout";
import Image from "next/image";
import Link from "next/link";

import Section from "./section";

import image_tablet from "@img/frontpage/tablet.webp";

import hero from "./hero.module.scss";

/** Hero section of the frontpage
 *
 */
export default function Hero({
    isLoggedIn,
    isVerified,
}: {
    isLoggedIn?: boolean;
    isVerified?: boolean;
}) {
    return (
        <div className={hero.wrapper}>
            <Section accents>
                <HeroTitle />
                <HeroContent />
                <HeroButtons
                    isLoggedIn={isLoggedIn ?? false}
                    isVerified={isVerified ?? false}
                />
            </Section>
        </div>
    );
}

/** The title of the hero section
 * This includes the title and the subtitle
 */
function HeroTitle() {
    return (
        <div className={hero.title}>
            <h1>Snip</h1>
            <h2>The user focused & open-source digital Lab Book</h2>
        </div>
    );
}

/** The content of the hero section
 * This includes the text and the image
 */
function HeroContent() {
    return (
        <div className={hero.content}>
            <div className={hero.text}>
                <p>
                    Our platform empowers scientists, researchers, and
                    innovators like you to record, organize, and share your
                    groundbreaking discoveries with precision and ease. With a
                    user-first interface and live collaboration support, we
                    offer the perfect tool for your documentation workflow.
                </p>
                <p>
                    Merge the charm of traditional analog documentation with
                    modern digital convenience. Our digital Lab Book creates a
                    bridge between past and future. Just as you would{" "}
                    <q>glue in</q> notes on paper, our platform effortlessly
                    integrates digital elements, preserving tactile familiarity
                    within a digital environment.
                </p>
            </div>
            <div className={hero.imgWrapper}>
                <Image
                    src={image_tablet}
                    alt="Snip running on a wacom tablet"
                    fill
                    style={{
                        objectFit: "cover",
                        objectPosition: "center",
                    }}
                />
            </div>
        </div>
    );
}

/** The Buttons are an extra component as this is
 * dependent if the user is logged in
 */
function HeroButtons({
    isLoggedIn,
    isVerified,
}: {
    isLoggedIn: boolean;
    isVerified: boolean;
}) {
    return (
        <div className={hero.buttonsWrapper}>
            <hr />
            <div className={hero.buttons}>
                {
                    <>
                        <Link
                            href="/books"
                            className={hero.login}
                            passHref
                            style={{
                                display:
                                    isLoggedIn && isVerified ? "flex" : "none",
                            }}
                        >
                            <button className="btn btn-primary">
                                Show me my books
                            </button>
                        </Link>

                        <Link
                            href="/signup"
                            className={hero.signup}
                            passHref
                            style={{
                                display: isLoggedIn ? "none" : "flex",
                            }}
                        >
                            <button className="btn btn-primary">Sign Up</button>
                        </Link>
                        <Link
                            href="/login"
                            className={hero.login}
                            passHref
                            style={{
                                display: isLoggedIn ? "none" : "flex",
                            }}
                        >
                            <button className="btn btn-outline-secondary">
                                Log In
                            </button>
                        </Link>
                        <Link
                            href="/verify"
                            className={hero.verify}
                            passHref
                            style={{
                                display:
                                    isLoggedIn && !isVerified ? "flex" : "none",
                            }}
                        >
                            <button className="btn btn-primary">
                                Verify your email
                            </button>
                        </Link>

                        <button
                            className="btn btn-outline-secondary"
                            onClick={async () => {
                                await logout();
                            }}
                            style={{
                                display: isLoggedIn ? "flex" : "none",
                                width: "30%",
                            }}
                        >
                            Logout
                        </button>
                    </>
                }
            </div>
        </div>
    );
}
