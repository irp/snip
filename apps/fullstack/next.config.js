import mdx from '@next/mdx';
import bundleAnalyzer from "@next/bundle-analyzer";
import CopyPlugin from "copy-webpack-plugin";

const withBundleAnalyzer = bundleAnalyzer({
    enabled: process.env.ANALYZE === "true",
});
const withMDX = mdx();



const filter_client = (resource, context) => {
    switch (resource) {
        case "sharp":
        case "canvas":
        case "mariadb":
        case "@snip/database":
        case "@snip/config/server":
            console.log("You are importing a dangerous module in (client-side)! Ignoring " + resource);
            return true;
        default:
            return false;
    }
};



function patchWasmModuleImport(config, dev, isServer) {
    config.experiments = {
        layers: true,
        asyncWebAssembly: true
    };

    config.output.environment = { ...config.output.environment, asyncFunction: true };
    if (!dev && isServer) {
        config.output.webassemblyModuleFilename = "./../server/chunks/[modulehash].wasm";

        const patterns = [];

        const destinations = [
            "../static/wasm/[name][ext]", // -> .next/static/wasm
            "./static/wasm/[name][ext]",  // -> .next/server/static/wasm
            "."                           // -> .next/server/chunks (for some reason this is necessary)
        ];
        for (const dest of destinations) {
            patterns.push({
                context: ".next/server/chunks",
                from: ".",
                to: dest,
                filter: (resourcePath) => resourcePath.endsWith(".wasm"),
                noErrorOnMissing: true
            });
        }

        config.plugins.push(new CopyPlugin({ patterns }));
    }

}


export default (phase, { defaultConfig }) => {
    /**
     * @type {import('next').NextConfig}
     */
    let nextConfig = {
        poweredByHeader: false,
        eslint: {
            dirs: [
                "lib",
                "components",
                "app",
            ],
        },
        output: "standalone",
        reactStrictMode: true,
        pageExtensions: ['js', 'jsx', 'mdx', 'ts', 'tsx'],
        webpack: (config, { dev, isServer, webpack }) => {
            /** Logging for webpack
             * 
            config.infrastructureLogging = {
                level: "verbose",
                stream: process.stderr,
                };
            */

            // Enable wasm loading
            patchWasmModuleImport(config, dev, isServer);

            if (!isServer) {
                // Ignore fs, path, ... on the client
                config.plugins.push(new webpack.IgnorePlugin({ checkResource: filter_client }));
                // Remove 'node:' from import specifiers, because Next.js does not yet support node: scheme
                // https://github.com/vercel/next.js/issues/28774
                config.plugins.push(
                    new webpack.NormalModuleReplacementPlugin(
                        /^node:/,
                        (resource) => {
                            resource.request = resource.request.replace(/^node:/, '');
                        },
                    ),
                );

                config.resolve.fallback = {
                    "fs": false,
                    "os": false,
                    "path": false,
                    "module": false,
                };

            }

            //config.externals.push({ "@snip/database": '@snip/database' });
            //config.externals.push({ "web-tree-sitter": 'web-tree-sitter' });
            return config;
        },
        experimental: {
            outputFileTracingExcludes: {
                "*": [
                    "node_modules/@swc/core-linux-x64-gnu",
                    "node_modules/@swc/core-linux-x64-musl",
                    "node_modules/@esbuild/linux-x64",
                ],
            },
        }
    }


    // Remove all console logs in production
    if (process.env.NODE_ENV === "production") {
        nextConfig = {
            ...nextConfig,
            compress: true,
            compiler: {
                removeConsole: {
                    exclude: ['error', 'warn'],
                },
            }
        }
    }


    return withBundleAnalyzer(withMDX(nextConfig));
}