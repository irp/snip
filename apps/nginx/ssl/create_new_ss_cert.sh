openssl genpkey -algorithm RSA -out certificate.key -pkeyopt rsa_keygen_bits:2048
openssl req -new -key certificate.key -out certificate.csr -config ./openssl.cnf
openssl x509 -req -days 365 -in certificate.csr -signkey certificate.key -out certificate.crt -extensions req_ext -extfile openssl.cnf
openssl x509 -outform der -in certificate.crt -out certificate.cer