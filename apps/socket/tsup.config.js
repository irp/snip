import { defineConfig } from 'tsup';

import { config } from "@snip/tsup-config/base";

export default defineConfig([
    {
        ...config,
        entry: [
            "./src/server.ts",
            "./src/cluster.ts",
            "./src/types.ts"
        ],
    }
]);