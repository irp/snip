import SQLService from "@snip/database";

import { setupBookPrefix } from "./book/socket";
import { setupPagesPrefix } from "./page/socket";
import { setupPointerPrefix } from "./pointer/socket";
import { setupSnipPrefix } from "./snip/socket";
import { setupUsersPrefix } from "./users/socket";

import { BookNamespace, PrefixData, Server, Socket } from "@/types";
import {
    addAllowedPage,
    authenticate_book,
    authenticate_user,
    authenticateApi,
    update_perms,
} from "@/utils/auth";
import { logAPI, logWS } from "@/utils/logging";

export function setupNamespaces(server: Server) {
    // Dynamic namespace creation for the books
    // All namespaces begin with "/book-"
    const book_nsp: BookNamespace = server.of(/^\/book-/);

    // Check for rights i.e if user is allowed to access book and adds perms to socket
    book_nsp.use(authenticate_user);
    book_nsp.use(authenticate_book);

    book_nsp.on("connection", async (socket) => {
        logWS(socket, "Client connected", socket.id);
        socket.on("disconnect", () => {
            logWS(socket, "Client disconnected", socket.id);
        });

        const data: PrefixData = {
            book_nsp,
            socket,
            server,
        };

        socket.on("ping", (callback) => {
            callback?.(Date.now());
        });

        /** Setup all function calls prefixed with pages
         * /book-[book_id]/pages:[function]/
         */
        setupPagesPrefix(data);

        /** Setup all function calls prefixed with book
         * /book-[book_id]/books:[function]/
         * /book-[book_id]/book:[function]/
         */
        setupBookPrefix(data);

        /** Setup all function calls prefixed with snip
         * /book-[book_id]/snip:[function]/
         */
        setupSnipPrefix(data);

        /** Setup all function calls prefixed with users
         * /book-[book_id]/users:[function]/
         */
        setupUsersPrefix(data);

        /** Setup all function calls prefixed with pointer
         * /book-[book_id]/pointer:[function]/
         * Allows to forward pointer movements to
         * all clients in the same page.
         */
        setupPointerPrefix(data);
    });

    setupServerSpace(server);

    setupApiSpace(server);
}

function setupApiSpace(server: Server) {
    const api_nsp = server.of("/api");

    api_nsp.use(authenticateApi);

    api_nsp.on("connection", async (socket: Socket) => {
        logAPI("Client connected", socket.id);

        socket.on("api:external_upload", async (snip_ids: number[]) => {
            logAPI("api:external_upload", snip_ids);
            const snips = await SQLService.snip.getByIds(snip_ids);

            snips.forEach((snip) => {
                const ns: BookNamespace = server.of("/book-" + snip.book_id);
                ns.emit("snip:inserted", snip);
            });
        });

        socket.on("resetSocketPerms", async (book_id: number) => {
            logAPI("resetSocketPerms", book_id);
            const ns: BookNamespace = server.of("/book-" + book_id);

            ns.sockets.forEach((socket) => {
                update_perms(socket as Socket);
            });
        });

        socket.on("disconnect", () => {
            logAPI("Client disconnected", socket.id);
        });
    });
}

function setupServerSpace(server: Server) {
    const server_nsp = server.of("/server");

    server_nsp.on("connection", async (socket: Socket) => {
        // Disconnect all clients that try to connect to the server namespace
        socket.disconnect(true);
    });

    server_nsp.on(
        "book:newPage:sync_allowedPages",
        (book_id: number, page_id: number, cb) => {
            addAllowedPage(server, book_id, page_id);
            cb?.({
                ok: true,
            });
        },
    );
}
