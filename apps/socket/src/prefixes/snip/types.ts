import { ID, SnipDataInsert, SnipDataRet } from "@snip/database/types";
import type { BaseData, BaseView, SnipData } from "@snip/snips/general/base";

import type { Callback } from "@/types";

export interface ClientToServerEvents_SnipPrefix {
    // Insert or update snip
    "snip:upsert": (snip_data: SnipDataInsert, callback: Callback<ID>) => void;
    // Remove snip
    "snip:remove": (
        snip_id: ID,
        nested: boolean,
        callback: Callback<true>,
    ) => void;

    "snip:preview": (snip_data: SnipData<BaseData, BaseView>) => void;
}
export interface ServerToClientEvents_SnipPrefix {
    // These can be called both in the page room and without a room
    "snip:updated": (updatedSnip: SnipDataRet) => void;
    "snip:removed": (snip_id: ID) => void;
    "snip:inserted": (snip: SnipDataRet) => void;
    "snip:placed": (snip_id: ID, page_id: ID) => void;
}
