import { hasOwnProperty } from "@snip/common";
import { config as client_config } from "@snip/config/client";
import SQLService from "@snip/database";
import service from "@snip/database";
import { ID, SnipDataInsert, SnipDataRet } from "@snip/database/types";
import { ArrayData } from "@snip/snips/general/array";
import { LinkData } from "@snip/snips/general/link";

import { PrefixData } from "@/types";
import { allowedPage } from "@/utils/auth";
import { logWS } from "@/utils/logging";
import { triggerRender } from "@/utils/triggerRender";
export function setupSnipPrefix({
    socket,
    book_nsp,
    server,
    ...props
}: PrefixData) {
    socket.on("snip:upsert", async (snip_data, callback) => {
        // Check if user has permission to write to the page
        if (!(socket.perms.pWrite === true)) {
            return callback({
                ok: false,
                error: "You do not have access to write to this book.",
            });
        }

        logWS(socket, "snip:upsert");

        try {
            const new_id = await upsert(
                { socket, book_nsp, server, ...props },
                snip_data,
            );
            callback({ ok: true, data: new_id });
        } catch (e) {
            if (e instanceof Error) {
                console.error(e);
                return callback({ ok: false, error: e.message });
            } else {
                console.error(e);
                return callback({
                    ok: false,
                    error: "Unknown Error upserting snip",
                });
            }
        }
    });

    // Remove snip from db
    socket.on("snip:remove", async (snip_id, nested, callback) => {
        // Check if user has permission to delete from the page
        if (!(socket.perms.pDelete === true)) {
            return callback({
                ok: false,
                error: "You do not have access to write to this book.",
            });
        }
        logWS(socket, "snip:remove " + snip_id);
        try {
            const snip = await SQLService.snip.getById(snip_id);
            await remove({ socket, book_nsp, server, ...props }, snip, nested);
            callback({ ok: true, data: true });
        } catch (e) {
            if (e instanceof Error) {
                console.error(e);
                return callback({ ok: false, error: e.message });
            } else {
                console.error(e);
                return callback({
                    ok: false,
                    error: "Unknown Error removing snip",
                });
            }
        }
    });

    socket.on("snip:preview", async (snipData) => {
        logWS(socket, "snip:preview");
        // TODO
    });
}

async function upsert(
    { socket, book_nsp, server }: PrefixData,
    snip_data: SnipDataInsert,
): Promise<ID> {
    // Sanity check for book_id and created by
    snip_data.book_id = socket.book_id;
    snip_data.created_by = socket.data.user_id;

    // Check for page_id
    const page_id = snip_data.page_id;
    if (page_id !== null && page_id !== undefined) {
        // check if page_id is allowed if set in update
        if (!allowedPage(socket, page_id, true, true)) {
            throw new Error("You do not have access to write to this page");
        }
    }

    // do not allow to update snips if they already exist and
    // are already placed
    try {
        if (!snip_data.id) {
            throw new Error("Snip does not have an id");
        }
        const old_snip = await SQLService.snip.getById(snip_data.id);
        if (old_snip.page_id !== undefined && old_snip.page_id !== null) {
            throw new Error("Snip is already placed! Cant update");
        }
    } catch (_e) {
        // Snip does not exist insert
    }
    const new_snipData = await SQLService.snip.upsert!(snip_data);

    if (page_id !== undefined && page_id !== null) {
        // Snip was placed! Emit to all clients in the page namespace
        socket.to("page-" + String(page_id)).emit("snip:updated", new_snipData);
        book_nsp.emit("snip:placed", new_snipData.id, page_id);

        // also emit to all clients in the referenced page if any
        service.page.getReferencedByPageId(page_id).then((pages) => {
            for (const page of pages) {
                server
                    .of("/book-" + String(page.book_id))
                    .to("page-" + String(page.id))
                    .emit("snip:updated", new_snipData);
                triggerRender(page.book_id, page.id, server);
            }
        });

        triggerRender(socket.book_id, page_id, server);
    } else {
        // updated queued snip
        book_nsp.emit("snip:updated", new_snipData);
    }

    return new_snipData.id;
}

/**
 * Updates a snip in the database and broadcasts the update to clients.
 *
 * @param {PrefixData} { socket, server } - The socket data containing the socket and server instances.
 * @param {SnipData<BaseData, BaseView>} snip_data - The data of the snip to be updated.
 * @throws {Error} If the socket book id does not match the book id of the snip being updated.
 * @throws {Error} If the user does not have access to write to the page.
 * @throws {Error} If the snip is already placed and cannot be updated.
 * @returns {Promise<void>}
 */
async function update(
    { socket, book_nsp, server }: PrefixData,
    snip_data: SnipDataRet,
) {
    if (!snip_data.id) {
        throw new Error("Snip does not have an id, cant be updated");
    }

    // Sanity check for book_id
    if (socket.book_id !== snip_data.book_id) {
        throw new Error(
            "Socket book id does not match the book id of the snip you are trying to upsert",
        );
    }
    // Check for page_id
    const page_id = snip_data.page_id;
    if (page_id !== null && page_id !== undefined) {
        // check if page_id is allowed if set in update
        if (!allowedPage(socket, page_id, true, true)) {
            throw new Error("You do not have access to write to this page");
        }
    }

    // Get old snip
    const old_snip = await SQLService.snip.getById(snip_data.id);
    if (old_snip.page_id !== undefined && old_snip.page_id !== null) {
        throw new Error("Snip is already placed! Cant update");
    }

    // Update snip
    const snip_data_n = await SQLService.snip.update(snip_data);

    if (snip_data_n.page_id !== undefined && snip_data_n.page_id !== null) {
        // Snip was placed!
        socket.to("page-" + String(page_id)).emit("snip:updated", snip_data_n);
        book_nsp.emit("snip:placed", snip_data_n.id, snip_data_n.page_id);

        triggerRender(socket.book_id, snip_data_n.page_id, server);
    } else {
        // updated queued snip
        book_nsp.emit("snip:updated", snip_data_n);
    }
}

/**
 * Inserts a new snip into the database and broadcasts the insertion to clients.
 *
 * @param {PrefixData} { socket, server } - The socket data containing the socket and server instances.
 * @param {SnipData<BaseData, BaseView>} snip_data - The data of the snip to be inserted.
 * @throws {Error} If the socket book id does not match the book id of the snip being inserted.
 * @throws {Error} If the user does not have access to write to the page.
 * @returns {Promise<ID>} The ID of the newly inserted snip.
 */
async function insert(
    { socket, book_nsp, server }: PrefixData,
    snip_data: SnipDataInsert,
): Promise<ID> {
    // Sanity check for book_id
    if (socket.book_id !== snip_data.book_id) {
        throw new Error(
            "Socket book id does not match the book id of the snip you are trying to upsert",
        );
    }

    // Check for page_id
    const page_id = snip_data.page_id;
    if (page_id !== null && page_id !== undefined) {
        // check if page_id is allowed if set in update
        if (!allowedPage(socket, page_id, false, true)) {
            throw new Error("You do not have access to write to this page");
        }
    }

    // Sanity
    snip_data.created_by = socket.perms.entity_id;

    // Insert snip
    const snip_data_n = await SQLService.snip.insert(snip_data);

    if (snip_data_n.page_id !== undefined && snip_data_n.page_id !== null) {
        // Send response to clients to update snip locally
        socket.to("page-" + String(page_id)).emit("snip:inserted", snip_data_n);
        // Trigger page preview update if page_id is set i.e. placed
        triggerRender(socket.book_id, snip_data_n.page_id, server);
    } else {
        // inserted new queued snip
        book_nsp.emit("snip:inserted", snip_data_n);
    }

    return snip_data_n.id;
}

async function remove(
    { socket, book_nsp, server }: PrefixData,
    snip: SnipDataInsert,
    nested = true,
) {
    if (!snip.id) {
        throw new Error("Snip does not have an id");
    }

    // Snips can only be removed/erased if one has created them oneself
    // and if they are placed in the last 5 mins
    //
    // If they are not placed yet anyone can remove them!

    const placed = snip.page_id !== undefined && snip.page_id !== null;
    const created_by_you = snip.created_by === socket.data.user_id;
    const timeout =
        snip.last_updated &&
        snip.last_updated.getTime() <
            Date.now() + client_config.TOOLS.ERASER.TIMEOUT * 1000;

    if (placed) {
        if (!created_by_you) {
            throw new Error("You can only remove snips you have created");
        }
        if (!timeout) {
            throw new Error(
                `You can only remove snips placed within the last ${client_config.TOOLS.ERASER.TIMEOUT} seconds`,
            );
        }
    }

    // Delete snip
    await SQLService.snip.delete(snip.id);

    // Delete nested if exists
    if (nested && hasOwnProperty(snip.data, "snips")) {
        for (const inner_snip of (snip.data as ArrayData)
            .snips as SnipDataInsert[]) {
            try {
                await remove({ socket, book_nsp, server }, inner_snip);
            } catch (e) {
                console.warn(e);
            }
        }
    }

    if (nested && hasOwnProperty(snip.data, "snip")) {
        try {
            await remove(
                { socket, book_nsp, server },
                (snip.data as LinkData).snip as SnipDataInsert,
            );
        } catch (e) {
            console.warn(e);
        }
    }

    // Trigger page preview update if page_id is set i.e. was placed
    if (snip.page_id !== undefined && snip.page_id !== null) {
        // Only emit to clients in the page namespace
        socket.to("page-" + String(snip.page_id)).emit("snip:removed", snip.id);

        // also emit to all clients in the referenced page if any
        service.page.getReferencedByPageId(snip.page_id).then((pages) => {
            for (const page of pages) {
                triggerRender(page.book_id, page.id, server);
                server
                    .of("/book-" + page.book_id)
                    .emit("snip:removed", snip.id!);
            }
        });

        // Trigger page preview update
        await triggerRender(socket.book_id, snip.page_id, server);
    } else {
        // removed queued snip
        book_nsp.emit("snip:removed", snip.id);
    }
}
