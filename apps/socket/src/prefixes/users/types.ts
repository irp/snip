import type { ID } from "@snip/database/types";

import type { Callback } from "@/types";

export interface OnlineUser {
    page_ids: Set<ID> | ID[];
    email: string;
    color: [number, number, number, number];
    id: ID;
    self?: boolean;
}

/** Typing for the socket io server and client
 * client files are copied for now to the fullstack
 * app
 *
 */
export interface ServerToClientEvents_UsersPrefix {
    // Online users in book
    "users:connect": (user: OnlineUser) => void;
    "users:disconnect": (user_id: number) => void;
    "users:update": (user: OnlineUser) => void;
}

export interface ClientToServerEvents_UsersPrefix {
    // Online users in book
    "users:getOnline": (callback: Callback<OnlineUser[]>) => void;
}
