import { Namespace } from "socket.io";

import { ID } from "@snip/database/types";

import { OnlineUser } from "./types";

import { PrefixData, RemoteSocket } from "@/types";
import { logWS } from "@/utils/logging";

export function setupUsersPrefix({ socket, server }: PrefixData) {
    /** Emit a user connect event if
     * the user freshly connects to the server
     * i.e. if this is the first socket connection
     * from the user
     */
    server
        .of("book-" + socket.book_id)
        .fetchSockets()
        .then((sockets) => {
            sockets = sockets.filter(
                (s) => s.data.user_id === socket.data.user_id,
            );
            if (sockets.length === 1) {
                //Emit user connect event
                const user: OnlineUser = {
                    id: socket.data.user_id,
                    email: socket.data.email,
                    color: socket.data.user_color,
                    page_ids: Array.from(roomsToPageIds(socket.rooms).values()),
                };
                socket.volatile.emit("users:connect", user);
            }
        });

    /** Allow clients to request all
     * online users
     */
    socket.on("users:getOnline", async (callback) => {
        logWS(socket, "Get online users");
        try {
            const sockets = await server
                .of("book-" + socket.book_id)
                .fetchSockets();

            let users: OnlineUser[] = socketsToUsers(sockets);

            users = users.map((user) => {
                if (user.id === socket.data.user_id) {
                    return {
                        ...user,
                        self: true,
                    };
                }
                return user;
            });
            callback?.({ ok: true, data: users });
        } catch (e) {
            console.error(e);
            callback?.({ ok: false, error: "Error fetching online users" });
        }
    });

    /** Emit proper user disconnect
     * event when user disconnects from all sockets
     */
    socket.on("disconnect", async () => {
        // Check if the user has disconnected with all sockets
        const sockets = await server
            .of("book-" + socket.book_id)
            .fetchSockets();

        for (const s of sockets) {
            if (s.data.user_id === socket.data.user_id) {
                return;
            }
        }
        logWS(socket, "User disconnected");
        socket.volatile.emit("users:disconnect", socket.data.user_id);
    });
}

function socketsToUsers(sockets: RemoteSocket[]): OnlineUser[] {
    const userMap: Map<ID, OnlineUser> = new Map();

    sockets.forEach((socket) => {
        if (!socket.data.user_id) {
            return;
        }
        if (!userMap.has(socket.data.user_id)) {
            // Add user to map
            userMap.set(socket.data.user_id, {
                id: socket.data.user_id,
                email: socket.data.email || "?",
                color: socket.data.user_color,
                page_ids: roomsToPageIds(socket.rooms),
            });
        } else {
            //Update page_ids
            const user = userMap.get(socket.data.user_id);
            if (user) {
                user.page_ids = new Set([
                    ...user.page_ids,
                    ...roomsToPageIds(socket.rooms),
                ]);
            }
        }
    });

    let users = Array.from(userMap.values());
    // map page ids set to array
    users = users.map((user) => ({
        ...user,
        page_ids: Array.from(user.page_ids.values()),
    }));
    // Map set to array
    return users;
}

export async function getAllSocketsById(
    namespace: Namespace,
    user_id: ID,
): Promise<RemoteSocket[]> {
    const sockets = await namespace.fetchSockets();
    return sockets.filter((s) => s.data.user_id === user_id);
}

function roomsToPageIds(rooms: Set<string>): Set<ID> {
    const pageIds: Set<ID> = new Set();
    rooms.forEach((room) => {
        const pageId = room.split("-")[1];
        if (pageId && !isNaN(parseInt(pageId))) {
            pageIds.add(parseInt(pageId));
        }
    });
    return pageIds;
}

export async function triggerUserUpdate(namespace: Namespace, user_id: ID) {
    // Get all sockets of the user
    const sockets = await getAllSocketsById(namespace, user_id);

    // convert to user
    const users = socketsToUsers(sockets);

    if (users.length > 1) {
        console.error(
            "Multiple sockets for user, should not happen here!",
            user_id,
        );
        return;
    }

    // Emit user update event
    // TODO: only trigger if user data has changed
    namespace.emit("users:update", users[0]);
}
