import type { ID } from "@snip/database/types";
import { BaseData, BaseView, SnipData } from "@snip/snips/general/base";

import type { Callback } from "@/types";

export interface ClientToServerEvents_PagePrefix {
    // Place a snip on the page
    "page:join": (page_id: ID, callback: Callback) => void;
    // Leave the page
    "page:leave": (page_id: ID, callback: Callback) => void;

    // Get all snips of a page
    "page:getSnips": (
        page_id: ID,
        callback: Callback<SnipData<BaseData, BaseView>[]>,
    ) => void;
}

export interface ServerToClientEvents_PagePrefix {}
