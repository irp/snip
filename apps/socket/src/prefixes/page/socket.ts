import SQLService from "@snip/database";

import { triggerUserUpdate } from "../users/socket";

import { PrefixData } from "@/types";
import { allowedPage } from "@/utils/auth";
import { logWS } from "@/utils/logging";

export function setupPagesPrefix({ socket, server }: PrefixData) {
    /** On join of page check if the
     * page exists and if the user has access
     * as in theory the user could use the
     * client to join any page
     */
    socket.on("page:join", async (page_id, callback) => {
        // No permission
        if (!allowedPage(socket, page_id)) {
            return callback?.({
                ok: false,
                error: "You do not have access to this page.",
            });
        }

        // Allowed
        logWS(socket, "page:join", "Join page " + page_id);
        await socket.join("page-" + String(page_id));
        callback?.({
            ok: true,
        });

        // We also trigger a user update
        triggerUserUpdate(
            server.of("book-" + socket.book_id),
            socket.data.user_id,
        );
    });

    /** Leave the page
     * @param page_id - The ID of the page to leave
     */
    socket.on("page:leave", async (page_id, callback) => {
        logWS(socket, "page:leave", "Leave page " + page_id);
        await socket.leave("page-" + String(page_id));

        callback?.({
            ok: true,
        });

        // We also trigger a user update
        triggerUserUpdate(
            server.of("book-" + socket.book_id),
            socket.data.user_id,
        );
    });

    /** Get all snips of a page
     * @param page_id - The ID of the page to get snips for
     */
    socket.on("page:getSnips", async (page_id, callback) => {
        if (!allowedPage(socket, page_id)) {
            return callback?.({
                ok: false,
                error: "You do not have access to this page.",
            });
        }

        const snips = await SQLService.snip.getByPageId(page_id);
        callback?.({
            ok: true,
            data: snips,
        });
    });
}
