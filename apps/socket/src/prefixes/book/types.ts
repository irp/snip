import type { Callback } from "types";

import type { ID, PageDataInsert, PageDataRet } from "@snip/database/types";

export interface ClientToServerEvents_BookPrefix {
    // Place a snip on the page
    "book:getPages": (callback: Callback<PageDataRet[]>) => void;
    "book:newPage": (
        pageData: PageDataInsert,
        callback: Callback<PageDataRet>,
    ) => void;
    "book:deletePage": (pageId: ID, callback: Callback) => void;
    "book:updatePage": (
        pageData: PageDataInsert & { id: ID },
        callback: Callback<PageDataRet>,
    ) => void;
}

export interface ServerToClientEvents_BookPrefix {
    "book:newPage": (pageData: PageDataRet) => void;
    "book:deletedPage": (pageId: ID) => void;
    "book:updatedPage": (pageData: PageDataRet) => void;
    "book:updatedPage:preview": (pageId: ID) => void;
}

export interface InterServerEvents_BookPrefix {
    "book:newPage:sync_allowedPages": (
        bookId: ID,
        pageId: ID,
        callback: Callback,
    ) => void;
}
