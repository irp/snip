import { getPermsPage } from "@snip/auth/page_permissions";
import service from "@snip/database";

import { PrefixData } from "@/types";
import { addAllowedPage, allowed, allowedPage } from "@/utils/auth";
import { logWS } from "@/utils/logging";

export function setupBookPrefix({ socket, server }: PrefixData) {
    /** Called on book init to get all page data
     *  - this does not transfer snip data
     */
    socket.on("book:getPages", async (callback) => {
        // Check if user has permission to view the page
        if (!allowed(socket)) {
            return callback?.({
                ok: false,
                error: "You do not have access to view this book",
            });
        }
        logWS(socket, "book:getPages", "Get pages");
        const pages = await service.page.getByBookId(socket.book_id);
        callback?.({
            ok: true,
            data: pages,
        });
    });

    /** Insert a new page into the database
     */
    socket.on("book:newPage", async (pageData, callback) => {
        // Check if user has permission to write to the page
        if (!allowed(socket, true, true, false)) {
            return callback?.({
                ok: false,
                error: "You do not have access to write to this book. Book might be finished!",
            });
        }
        logWS(socket, "book:newPage", "New page");

        try {
            // Add the page to the database
            pageData.book_id = socket.book_id;

            // Check if user is allowed to reference the page if any is set!
            if (
                pageData.referenced_page_id &&
                !isNaN(pageData.referenced_page_id)
            ) {
                const perms = await getPermsPage(pageData.referenced_page_id, {
                    user_id:
                        socket.data.user_id > 0
                            ? socket.data.user_id
                            : undefined,
                    ui_token_id:
                        socket.data.user_id < 0
                            ? socket.data.user_id * -1
                            : undefined,
                });
                if (!perms.resolved.pRead) {
                    return callback?.({
                        ok: false,
                        error: "You do not have access to reference this page!",
                    });
                }
            }

            const inserted_page = await service.page.insert(pageData);

            // Add the page to the allowedPages of all sockets
            // in the same book
            addAllowedPage(server, inserted_page.book_id, inserted_page.id);

            // emit to all other clusternodes (sync state)
            await server
                .of("/server")
                .serverSideEmitWithAck(
                    "book:newPage:sync_allowedPages",
                    inserted_page.book_id,
                    inserted_page.id,
                )
                .catch((e) => {
                    console.error(e);
                });

            callback?.({
                ok: true,
                data: inserted_page,
            });
            socket.broadcast.emit("book:newPage", inserted_page);
        } catch (e) {
            console.error(e);
            return callback?.({
                ok: false,
                error: "Error inserting new page",
            });
        }
    });

    socket.on("book:deletePage", async (page_id, callback) => {
        // Check if user has permission to write to the page
        if (!allowedPage(socket, page_id, true, true, true)) {
            return callback?.({
                ok: false,
                error: "You do not have access to delete from this book",
            });
        }

        logWS(socket, "book:deletePage", "Delete page " + page_id);
        try {
            // Delete the page from the database
            await service.page.delete(page_id);
            callback?.({
                ok: true,
            });

            // Emit to clients
            socket.broadcast.emit("book:deletedPage", page_id);
        } catch (e) {
            console.error(e);
            return callback?.({
                ok: false,
                error: "Error deleting page",
            });
        }
    });

    socket.on("book:updatePage", async (pageData, callback) => {
        // Check if user has permission to write to the page
        if (!allowedPage(socket, pageData.id, true, true, false)) {
            return callback?.({
                ok: false,
                error: "You do not have access to write to this book. Book might be finished!",
            });
        }
        logWS(socket, "book:updatePage", "Update page " + pageData.id);

        try {
            // Update the page in the database
            const updated_page = await service.page.update(pageData);
            callback?.({
                ok: true,
                data: updated_page,
            });

            // Emit to clients
            socket.broadcast.emit("book:updatedPage", updated_page);
        } catch (e) {
            console.error(e);
            return callback?.({
                ok: false,
                error: "Error updating page",
            });
        }
    });
}
