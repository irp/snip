import { PointerType } from "@/types";

export interface ClientToServerEvents_PointerPrefix {
    "pointer:move": (
        x: number,
        y: number,
        page_id: number,
        type: PointerType,
    ) => void;

    "pointer:down": (
        x: number,
        y: number,
        page_id: number,
        type: PointerType,
    ) => void;
}

export interface ServerToClientEvents_PointerPrefix {
    "pointer:move": (
        x: number,
        y: number,
        color: RGBA,
        type: PointerType,
    ) => void;

    "pointer:down": (
        x: number,
        y: number,
        color: RGBA,
        type: PointerType,
    ) => void;
}

export type RGBA = [number, number, number, number];
