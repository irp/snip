import { PrefixData } from "@/types";
import { logWSError } from "@/utils/logging";

export function setupPointerPrefix({ socket }: PrefixData) {
    /** Allows to forward pointer movements to
     * all clients in the same page.
     *
     * data: [x,y]
     * user: [user_id, page_id]
     */
    socket.on("pointer:move", (x, y, page_id, type) => {
        // Check if allowedPage is valid
        if (!socket.data.allowedPages.includes(page_id)) {
            logWSError(
                socket,
                "pointer:event",
                "User not allowed to send pointer events",
                page_id,
            );

            return;
        }

        const color = socket.data.user_color;

        // Send to all clients in the same page
        socket
            .to("page-" + String(page_id))
            .volatile.emit("pointer:move", x, y, color, type);
    });

    socket.on("pointer:down", (x, y, page_id, type) => {
        // Check if allowedPage is valid
        if (!socket.data.allowedPages.includes(page_id)) {
            logWSError(
                socket,
                "pointer:event",
                "User not allowed to send pointer events",
                page_id,
            );

            return;
        }

        const color = socket.data.user_color;

        // Send to all clients in the same page
        socket
            .to("page-" + String(page_id))
            .volatile.emit("pointer:down", x, y, color, type);
    });
}
