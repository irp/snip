import { Server as SocketIoServer } from "socket.io";
import { App } from "uWebSockets.js";

import { setupNamespaces } from "./prefixes/setupNamespace";

import { Server } from "@/types";
import { createAdapter } from "@socket.io/cluster-adapter";
import { setupWorker } from "@socket.io/sticky";

const app = App();
const server: Server = new SocketIoServer({
    maxHttpBufferSize: 1e8,
    pingTimeout: 25000,
    serveClient: false,
    path: "/socket/",
    cors: {
        credentials: true,
        origin: true,
        methods: ["GET", "POST"],
    },
});
server.attachApp(app);

// use the cluster adapter
server.adapter(createAdapter());

// setup connection with the primary process
setupWorker(server);

// setup namespaces
setupNamespaces(server);

/* c8 ignore start */
if (process.env.NODE_ENV !== "test") {
    app.listen(80, (listenSocket) => {
        if (!listenSocket) {
            console.warn("Failed to listen to port 80");
        }
    });
}
/* c8 ignore end */
