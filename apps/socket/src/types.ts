import type {
    Namespace,
    RemoteSocket as RemoteSocketIo,
    Server as SocketIoServer,
    Socket as SocketIoSocket,
} from "socket.io";

import type { ParsedPermissionACLData } from "@snip/auth/book_permissions";
import type { SnipSessionData } from "@snip/auth/types";

import type {
    ClientToServerEvents_BookPrefix,
    InterServerEvents_BookPrefix,
    ServerToClientEvents_BookPrefix,
} from "./prefixes/book/types";
import type {
    ClientToServerEvents_PagePrefix,
    ServerToClientEvents_PagePrefix,
} from "./prefixes/page/types";
import {
    ClientToServerEvents_PointerPrefix,
    ServerToClientEvents_PointerPrefix,
} from "./prefixes/pointer/types";
import type {
    ClientToServerEvents_SnipPrefix,
    ServerToClientEvents_SnipPrefix,
} from "./prefixes/snip/types";
import type {
    ClientToServerEvents_UsersPrefix,
    ServerToClientEvents_UsersPrefix,
} from "./prefixes/users/types";

type ID = number;

type ResponseData<T> = T extends void
    ? {
          ok: true;
      }
    : {
          ok: true;
          data: T;
      };

/** A bit of error handling
 * for the callbacks
 */
export type CBResponse<T> =
    | ResponseData<T>
    | {
          ok: false;
          error: string;
      };
export type Callback<T = void> = (res: CBResponse<T>) => void;

export type ClientToServerEvents = ClientToServerEvents_BookPrefix &
    ClientToServerEvents_UsersPrefix &
    ClientToServerEvents_SnipPrefix &
    ClientToServerEvents_PointerPrefix &
    ClientToServerEvents_PagePrefix & {
        ping: (cb: (utc: number) => void) => void;
        "api:external_upload": (snip_ids: ID[]) => void;
        resetSocketPerms: (book_id: ID) => void;
    };
export type ServerToClientEvents = ServerToClientEvents_BookPrefix &
    ServerToClientEvents_UsersPrefix &
    ServerToClientEvents_SnipPrefix &
    ServerToClientEvents_PointerPrefix &
    ServerToClientEvents_PagePrefix;

type InterServerEvents = InterServerEvents_BookPrefix;

type Data = {
    user_id: ID; //user id
    user_color: [number, number, number, number];
    auth_method: "user" | "ui_token";
    email: string;
    allowedPages: ID[];
};

export type Server = SocketIoServer<
    ClientToServerEvents,
    ServerToClientEvents,
    InterServerEvents,
    Data
>;

export type BookNamespace = Namespace<
    ClientToServerEvents,
    ServerToClientEvents,
    InterServerEvents,
    Data
>;

export interface Socket
    extends SocketIoSocket<
        ClientToServerEvents,
        ServerToClientEvents,
        InterServerEvents,
        Data
    > {
    book_id: ID;
    perms: ParsedPermissionACLData;
    session: SnipSessionData;
}

declare module "socket.io" {
    interface Socket {
        session: SnipSessionData;
        book_id: ID;
        perms: ParsedPermissionACLData;
    }
}

/** Internal parsing for prefixes */
export interface PrefixData {
    book_nsp: BookNamespace;
    socket: Socket;
    server: Server;
}

export type RemoteSocket = RemoteSocketIo<ServerToClientEvents, Socket["data"]>;

export enum PointerType {
    MOUSE = 0,
    TOUCH = 1,
    PEN = 2,
    OTHER = 3,
}
