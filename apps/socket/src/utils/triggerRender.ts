import { getCookieString } from "@snip/auth/session";
import { ID } from "@snip/database/types";

import { Server } from "@/types";

/** Trigger a render via an post request to the
 * render server.
 */
export async function triggerRender(book_id: ID, page_id: ID, server: Server) {
    await fetch(`http://snip_images/rerender/${page_id}`, {
        method: "POST",
        headers: {
            cookie: await getCookieString({
                api: {
                    identifier: "socket",
                },
            }),
        },
    }).catch((e) => {
        console.error(e);
    });
    server.of("/book-" + book_id).emit("book:updatedPage:preview", page_id);
}
