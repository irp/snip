import { type Socket } from "@/types";

export const log = (...args: unknown[]) => console.log("[Socket]", ...args);
export const debug = (...args: unknown[]) => console.debug("[Socket]", ...args);
export const warn = (...args: unknown[]) => console.warn("[Socket]", ...args);
export const logWS = (socket: Socket, ...args: unknown[]) =>
    console.log(
        `[Socket][Book:${socket.book_id}][User:${socket.data.user_id}]`,
        ...args,
    );

export const logAPI = (...args: unknown[]) =>
    console.log("[Socket][API]", ...args);

export const logWSError = (socket: Socket, ...args: unknown[]) =>
    console.error(
        `[Socket][Book:${socket.book_id}][User:${socket.data.user_id}]`,
        ...args,
    );
