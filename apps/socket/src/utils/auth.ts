import { ServerResponse } from "http";
import { Server } from "socket.io";

import { getPerms } from "@snip/auth/book_permissions";
import { getSession } from "@snip/auth/session";
import { verifyApiToken } from "@snip/auth/tokens/socket";
import service from "@snip/database";
import { getPrimaryEmail } from "@snip/database/services/user";
import { UserDataRet } from "@snip/database/types";

import { Socket } from "@/types";

type SocketNextFkt = (err?: Error) => void;

/** Check if the user is logged in
 * @param socket
 */
export async function authenticate_user(socket: Socket, next: SocketNextFkt) {
    // Verify session
    try {
        const session = await getSession(
            socket.request,
            new ServerResponse(socket.request),
        );

        if (!session.user && !session.anonymous) {
            console.log("Invalid session user has to be logged in");
            return next(new Error("Invalid session user has to be logged in"));
        }

        socket.session = session;
    } catch (error) {
        return next(error as Error);
    }
    return next();
}

/** Check if the user is allowed to view the book
 * @param socket
 */
export async function authenticate_book(socket: Socket, next: SocketNextFkt) {
    // Check if valid namespace (should be /book-[book_id])

    try {
        const book_id = socket.nsp.name.split("-")[1];
        const { user, anonymous } = socket.session;

        if (!book_id || isNaN(parseInt(book_id))) {
            throw new Error("Invalid namespace");
        }
        socket.book_id = parseInt(book_id);

        let ui_token_id: number | undefined;
        if (anonymous && anonymous.tokens.length > 0) {
            ui_token_id = anonymous.tokens.filter(
                (token) =>
                    token.book_id === socket.book_id &&
                    token.expires_at > Date.now(),
            )[0]?.id;
        }

        // Verify use perms
        const perms_promise = getPerms(socket.book_id, {
            user_id: user?.id,
            ui_token_id,
        });

        // Get allowed pages
        const allowedPages_promise = service.page
            .getByBookId(parseInt(book_id))
            .then((pages) => {
                return pages.map((page) => page.id);
            });

        // User data id is negative if anonymous i.e. ui_token
        let userData_promise: Promise<UserDataRet>;
        if (user) {
            userData_promise = service.user.getById(user.id);
        } else {
            userData_promise = Promise.resolve({
                id: ui_token_id! * -1,
                emails: ["anonymous"],
                credential_ids: [1],
                primary_credential_id: 1,
                created: new Date(),
                last_updated: new Date(),
                emails_verified: [false],
                credential_types: ["ui_token"],
                credential_provider_ids: [null],
            });
        }

        let userConfig_promise: Promise<{ user_color: string }> | undefined;
        if (user) {
            userConfig_promise = service.userConfig.getByUserId(user.id);
        } else {
            userConfig_promise = Promise.resolve({ user_color: "#512DA8" });
        }

        const [perms, allowedPages, u, uConfig] = await Promise.all([
            perms_promise,
            allowedPages_promise,
            userData_promise,
            userConfig_promise,
        ]);

        socket.perms = perms.resolved;
        socket.data = {
            user_id: u.id,
            user_color: hexToRGBA(uConfig.user_color),
            auth_method: ui_token_id ? "ui_token" : "user",
            email: getPrimaryEmail(u) || "anonymous",
            allowedPages,
        };

        if (
            !socket.perms.pRead &&
            !socket.perms.pWrite &&
            !socket.perms.pDelete
        ) {
            throw new Error("User not allowed to access book");
        }
    } catch (error) {
        return next(error as Error);
    }

    return next();
}

function hexToRGBA(hex: string): [number, number, number, number] {
    return [
        parseInt(hex.substring(1, 3), 16),
        parseInt(hex.substring(3, 5), 16),
        parseInt(hex.substring(5, 7), 16),
        255,
    ];
}

export async function update_perms(socket: Socket) {
    const user_id = socket.session.user?.id;
    const ui_tokens = socket.session.anonymous?.tokens;
    let ui_token_id: number | undefined;
    if (ui_tokens && ui_tokens.length > 0) {
        ui_token_id = ui_tokens.filter(
            (token) =>
                token.book_id === socket.book_id &&
                token.expires_at > Date.now(),
        )[0]?.id;
    }

    const perms = await getPerms(socket.book_id, {
        user_id,
        ui_token_id,
    });
    socket.perms = perms.resolved;
    return;
}

/** For api namespace only */
export function authenticateApi(socket: Socket, next: SocketNextFkt) {
    const token = socket.handshake.headers.authorization?.split(" ")[1];

    if (!token) {
        return next(new Error("No token provided"));
    }

    try {
        const decoded = verifyApiToken(token);

        if (!decoded.api) {
            throw new Error("Invalid token, not an api token");
        }
    } catch (error) {
        return next(error as Error);
    }
    return next();
}

export function allowedPage(
    socket: Socket,
    page_id: number,
    pRead = true,
    pWrite = false,
    pDelete = false,
) {
    if (!allowed(socket, pRead, pWrite, pDelete)) {
        return false;
    }
    return socket.data.allowedPages.includes(page_id);
}

export function allowed(
    socket: Socket,
    pRead = true,
    pWrite = false,
    pDelete = false,
) {
    if (!(socket.perms.pRead === true) && pRead) {
        return false;
    }
    if (!(socket.perms.pWrite === true) && pWrite) {
        return false;
    }
    if (!(socket.perms.pDelete === true) && pDelete) {
        return false;
    }
    return true;
}

export function addAllowedPage(
    server: Server,
    book_id: number,
    page_id: number,
) {
    const socketsLocal = server.of("book-" + book_id).sockets;
    socketsLocal.forEach((s) => {
        s.data.allowedPages.push(page_id);
    });
}
