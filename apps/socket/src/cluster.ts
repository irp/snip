import cluster, { Worker } from "node:cluster";
import { createServer } from "node:http";
import path, { join } from "node:path";

import { fileURLToPath } from "url";

import { config } from "@snip/config/server";

import { log } from "./utils/logging";

import { setupPrimary } from "@socket.io/cluster-adapter";
import { setupMaster } from "@socket.io/sticky";

const httpServer = createServer();

// setup sticky sessions
setupMaster(httpServer, {
    loadBalancingMethod: "least-connection",
});

// setup connections between the workers
setupPrimary();

// Setup the primary cluster i.e. worker file
const __filenameNew = fileURLToPath(import.meta.url);
const __dirnameNew = path.dirname(__filenameNew);
cluster.setupPrimary({
    exec: join(__dirnameNew, "server"),
    silent: false,
});

httpServer.listen(3000);

// Create workers
const workers: { [key: string]: Worker } = {};
for (let i = 0; i < config.socket.parse().num_processes; i++) {
    const worker = cluster.fork();
    workers[worker.process.pid!] = worker;
}
log(`Created ${config.socket.parse().num_processes} workers`);

// Some callbacks for the cluster
cluster.on("online", function (worker) {
    log("worker " + worker.process.pid + " is online");
});

cluster.on("exit", (worker, code, signal) => {
    console.log(
        `worker ${worker.process.pid} died with code: ${code}, and signal: ${signal}`,
    );
    const w = cluster.fork();
    workers[w.process.pid!] = w;
});

// Master can be terminated by either SIGTERM
// or SIGINT. The later is used by CTRL+C on console.

//Note for nodemon. Use nodemon --signal SIGTERM
const closeCluster = () => {
    log("Cluster Shutdown");
    for (const pid in workers) {
        workers[pid]!.destroy("SIGTERM");
    }
    process.exit(0);
};
process.on("SIGTERM", closeCluster);
process.on("SIGINT", closeCluster);
