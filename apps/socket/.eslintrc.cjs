// This configuration only applies to the package manager root.
/** @type {import("eslint").Linter.Config} */
module.exports = {
    root: true,
    extends: ["@snip/eslint-config/library"],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        project: true,
    },
};