// vitest.config.ts
import { resolve } from "path";
import { defineConfig } from "vitest/config";

export default defineConfig({
    test: {
        setupFiles: ["../fullstack/__mocks__/global_mocks.ts"],
        globals: true,
        coverage: {
            provider: "v8",
            include: ["src/**/*"],
        },
        alias: [
            {
                find: /^@snip\/database$/,
                replacement: resolve(
                    "../fullstack/__mocks__/@snip/database/index.ts",
                ),
            },
        ],
    },
    resolve: {
        alias: {
            "@/": new URL("./src/", import.meta.url).pathname,
        },
    },
});
