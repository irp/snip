import fs from "fs";
import { convert } from "html-to-text";
import { type MailOptions } from "nodemailer/lib/smtp-transport";
import path from "path";
import { fileURLToPath } from "url";

import { config } from "@snip/config/server";

const __filenameNew = fileURLToPath(import.meta.url);
const __dirnameNew = path.dirname(__filenameNew);

export class Email {
    // Path to template
    private templatePath: string;
    private replace: { [key: string]: string };
    private recipients: string[] = [];
    private subject: string;

    constructor(template: string, replace?: { [key: string]: string }) {
        this.templatePath = path.resolve(
            __dirnameNew, // running from build/server.js -> ../templates/
            "../templates/",
            template,
        );
        this.replace = replace || {};
        this.subject = "[Snip]";
    }

    /** Recipients
     * Add a recipient to the email
     */
    addRecipient(recipient: string) {
        //Parse email valid with zod
        this.recipients.push(recipient);
    }
    addRecipients(recipients: string[]) {
        for (const recipient of recipients) {
            this.addRecipient(recipient);
        }
    }
    getRecipients(): string[] {
        if (this.recipients.length === 0) {
            throw new Error(`No recipients set! Please add at least one recipient with 
            addRecipient() method.
            `);
        }
        return this.recipients;
    }

    /** Subject
     * Add a subject to the email
     */
    addSubject(subject: string) {
        this.subject = subject;
    }
    getSubject(): string {
        const subject = this.subject;
        if (!subject) {
            throw new Error(
                `No subject set! Please add a subject with addSubject() method.`,
            );
        }
        return subject;
    }

    /** Add template replacements
     * Add a key value pair to replace in the template
     */
    addReplace(key: string, value: string) {
        this.replace[key] = value;
    }

    getContent(): string {
        let template = fs.readFileSync(this.templatePath, "utf8");
        for (const key in this.replace) {
            const sanitizedValue = this.replace[key]!.replaceAll(
                /{{/g,
                "",
            ).replaceAll(/}}/g, "");
            template = template.replaceAll(`{{${key}}}`, sanitizedValue);
        }
        return template;
    }

    getContentAsText(): string {
        const htmlContent = this.getContent();
        return convert(htmlContent, {
            wordwrap: 130,
        });
    }

    toMailOptions(): MailOptions {
        const cnf = config.email.parse();

        // create mail options
        const mailOptions: MailOptions = {
            from: `SNIP Team <${cnf.options.from || cnf.transport.user}>`,
            bcc: cnf.options.bcc,
            to: this.getRecipients(),
            subject: this.getSubject(),
            html: this.getContent(),
            text: this.getContentAsText(),
        };
        return mailOptions;
    }
}

export class ForgotPasswordEmail extends Email {
    constructor(user_email: string, token: string) {
        super("forgotPassword.html");

        let resetLink = `${config.getServerUrl()}/resetPassword?token=${token}&email=${user_email}`;
        resetLink = formatLink(resetLink);
        let rawLink = `${config.getServerUrl()}/resetPassword`;
        rawLink = formatLink(rawLink);

        this.addSubject("[SNIP] Password reset request");
        this.addRecipient(user_email);
        this.addReplace("reset_link", resetLink);
        this.addReplace("raw_link", rawLink);
        this.addReplace("token", token);
    }
}

export class VerifyAccountEmail extends Email {
    constructor(user_email: string, token: string) {
        super("verifyAccount.html", {
            user_email,
            token,
        });
        let verify_link = `${config.getServerUrl()}/verify?token=${token}&email=${user_email}`;
        verify_link = formatLink(verify_link);

        let raw_link = `${config.getServerUrl()}/verify`;
        raw_link = formatLink(raw_link);

        this.addSubject("Welcome to SNIP - Please verify your email");
        this.addRecipient(user_email);
        this.addReplace("verify_link", verify_link);
        this.addReplace("raw_link", raw_link);
    }
}

export class ChangeEmailEmail extends Email {
    constructor(new_email: string, token: string, credential_id: number) {
        super("changeEmail.html", {
            new_email,
        });
        let change_link = `${config.getServerUrl()}/account/changeEmail?token=${token}&credential_id=${credential_id}`;
        change_link = formatLink(change_link);

        this.addSubject("[SNIP] Change email request");
        this.addReplace("change_link", change_link);
    }
}

export class GroupInviteEmail extends Email {
    constructor(
        group_name: string,
        group_id: number,
        token: string,
        invite_by: string | undefined,
    ) {
        super("groupInvite.html", {
            group_name,
            invite_by: invite_by || "Someone",
        });

        let invite_link = `${config.getServerUrl()}/account/groups/invitation?token=${token}&id=${group_id}`;
        invite_link = formatLink(invite_link);

        this.addSubject(`[SNIP] You have been invited to join ${group_name}`);
        this.addReplace("invite_link", invite_link);
    }
}

export class CollaboratorRequestEmail extends Email {
    constructor(
        book_title: string,
        book_id: number,
        inviter_email: string,
        token: string,
    ) {
        super("collaborationRequest.html", {
            book_title,
            inviter_email,
        });

        let invite_link = `${config.getServerUrl()}/books/invitation?token=${token}&id=${book_id}`;
        invite_link = formatLink(invite_link);

        this.addSubject(`[SNIP] Invitation to join ${book_title}`);
        this.addReplace("link", invite_link);
    }
}

function formatLink(link: string): string {
    if (!link.startsWith("https") || !link.startsWith("http")) {
        return "https://" + link;
    }
    return link;
}
