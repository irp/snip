import { createTransport } from "nodemailer";

import { config } from "@snip/config/server";

import { Email } from "./email";

export async function sendEmail(email: Email) {
    const options = email.toMailOptions();

    const cnf = config.email.parse();

    if (!cnf.enabled) {
        // Email sending not enabled in config
        // print email to console
        console.warn(Array(80).join("="));
        console.warn(
            "\x1b[0;31m[EMAIL] Email not sent, email sending is disabled in config\x1b[0m",
        );
        console.warn(options.text);
        console.warn(Array(80).join("="));
    } else {
        const transporter = createTransport({
            host: cnf.transport.host,
            port: cnf.transport.port,
            auth: {
                user: cnf.transport.user,
                pass: cnf.transport.password,
            },
        });

        // Send email
        const info = await transporter.sendMail(options);
        if (info.accepted.length === 0) {
            transporter.close();
            throw new Error("No email was accepted by the server");
        } else {
            console.log("[EMAIL] Email sent: " + info.response);
        }
        transporter.close();
    }
}
