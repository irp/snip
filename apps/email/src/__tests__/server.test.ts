import request from "supertest";
import { server } from "@/server";

vi.mock("@snip/auth/session", async () => {
    return {
        getSession: async () => {
            // @ts-expect-error mockConfig is not defined
            return globalThis.mockConfig.session;
        },
    };
});

describe("server", () => {
    let consoleSpy;
    beforeAll(() => {
        consoleSpy = vi.spyOn(console, "error").mockImplementation(() => {});
    });
    afterAll(() => {
        server.close();
    });

    it("should return 401 if no session is provided", async () => {
        // @ts-ignore
        delete globalThis.mockConfig.session;
        await request(server).get("/auth").expect(401);
    });

    it("should return 500 if internal error occurs", async () => {
        const res = await request(server)
            .get("/error")
            .expect(500)
            .expect("Content-Type", /json/);
        expect(res.body).toEqual({
            error: "Internal Server Error",
            details: "TestError",
        });
    });

    it("should return 200 if normal request", async () => {
        await request(server).get("/ping").expect(200).expect("pong");
        const res = await request(server).get("/auth").expect(200);
        expect(res.body).toEqual({
            message: "Authorized",
        });
    });
});
