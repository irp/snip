import express from "express";
import { createServer } from "http";

import { authMiddleWare } from "./common/auth";
import errorHandler from "./common/errorHandler";
import { accountRouter } from "./routes/account";
import { bookRouter } from "./routes/book";
import { groupRouter } from "./routes/group";

const app = express();
export const server = createServer(app);
// Auth middleware
app.get("/auth", authMiddleWare, (_, res) => {
    res.json({
        message: "Authorized",
    });
});

app.get("/ping", (_, res) => {
    res.send("pong");
});

app.get("/error", () => {
    const error = new Error("Test error");
    error.name = "TestError";
    throw error;
});

app.use("/email/account", accountRouter);
app.use("/email/group", groupRouter);
app.use("/email/book", bookRouter);

app.use(errorHandler);

/* c8 ignore start */
if (process.env.NODE_ENV !== "test") {
    const port = process.env.APP_PORT || "80";
    server.listen(parseInt(port));
}
/* c8 ignore end */
