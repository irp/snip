import { NextFunction, Request, Response } from "express";

import { getSession } from "@snip/auth/session";

// Auth middleware
export async function authMiddleWare(
    req: Request,
    res: Response,
    next: NextFunction,
) {
    const session = await getSession(req, res);
    /** We only allow users to use the email endpoint
     * no tokens allowed!
     */
    if (!session || !session.user) {
        res.status(401).json({
            error: "Unauthorized",
        });
        return;
    }

    req.session = session;

    next();
}

export async function authMiddleWareApiOnly(
    req: Request,
    res: Response,
    next: NextFunction,
) {
    const session = await getSession(req, res);
    if (!session || !session.api) {
        res.status(401).json({
            error: "Unauthorized",
        });
        return;
    }

    req.session = session;

    next();
}
