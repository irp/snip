import { NextFunction, Request, Response } from "express";

/** Rate limit middleware
 */
export function rateLimitAggressive(
    req: Request,
    res: Response,
    next: NextFunction,
) {
    // Rate limit logic here

    // Rate limiting minute
    const requestsLastMinute = numberOfRequest(req, 60);
    if (requestsLastMinute > 2) {
        res.status(429).json({
            error: "Too Many Requests",
            details: "Too many requests in the last minute",
        });
        return;
    }

    // Rate limiting hour
    const requestsLastHour = numberOfRequest(req, 3600);
    if (requestsLastHour > 5) {
        res.status(429).json({
            error: "Too Many Requests",
            details: "Too many requests in the last hour",
        });
        return;
    }

    const lastRequestTime = dateLastRequest(req);
    if (lastRequestTime) {
        const timeSinceLastRequest =
            new Date().getTime() - lastRequestTime.getTime();
        if (timeSinceLastRequest < 5000) {
            res.status(429).json({
                error: "Too Many Requests",
                details: "Please wait 5s between each request",
            });
            return;
        }
    }

    // Delete all requests older than 1 hour
    deleteAllRequestsOlderThanOneHour();

    // Add request to ipRequests
    addRequestToIpRequests(req);
    next();
}

// Track IP addresses and their request counts
const ipRequests = new Map<string, Date[]>();

function deleteAllRequestsOlderThanOneHour() {
    ipRequests.forEach((value, key) => {
        const now = new Date().getTime();
        ipRequests.set(
            key,
            value.filter(
                (timestamp) => timestamp.getTime() > now - 3600 * 1000,
            ),
        );
    });
}

function addRequestToIpRequests(req: Request) {
    const clientIp = getIp(req);

    if (ipRequests.has(clientIp)) {
        ipRequests.get(clientIp)!.push(new Date());
    } else {
        ipRequests.set(clientIp, [new Date()]);
    }
}

/** Get number of requests in the last x seconds
 * @param req
 * @param x number of seconds
 *
 * @returns number of requests in the last x seconds
 */
function numberOfRequest(req: Request, x: number): number {
    // Get ip  address
    // Get the client's IP address
    const clientIp = getIp(req);

    // Get the current time
    const now = new Date().getTime();

    // Get the number of requests made by this IP address in the last hour
    const requestsThisHour = ipRequests.has(clientIp)
        ? ipRequests
              .get(clientIp)!
              .filter((timestamp) => timestamp.getTime() > now - x * 1000)
              .length
        : 0;

    return requestsThisHour;
}

/** Get last timestamp of client
 * @param req
 */
function dateLastRequest(req: Request): Date | undefined {
    // Get ip  address
    // Get the client's IP address
    const clientIp = getIp(req);

    // Get the last request made by this IP address
    const lastRequest = ipRequests.has(clientIp)
        ? ipRequests.get(clientIp)!.at(-1)
        : undefined;

    return lastRequest;
}

function getIp(req: Request): string {
    const clientIp =
        req.headers["x-forwarded-for"] ||
        req.socket.remoteAddress ||
        Math.random().toString();
    if (Array.isArray(clientIp)) {
        return (clientIp as string[]).at(0)!;
    }
    return clientIp;
}
