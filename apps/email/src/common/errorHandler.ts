import { NextFunction, Request, Response } from "express";

const errorHandler = (
    err: Error,
    _req: Request,
    res: Response,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _next: NextFunction,
) => {
    console.error(err); // Log the error for debugging purposes

    // Return generic error message to the client
    res.header("Content-Type", "application/json");
    return res
        .status(500)
        .json({ error: "Internal Server Error", details: err.name });
};
export const asyncHandler =
    (
        fn: (
            req: Request,
            res: Response,
            next: NextFunction,
        ) => Promise<unknown>,
    ) =>
    (req: Request, res: Response, next: NextFunction) =>
        Promise.resolve(fn(req, res, next)).catch(next);

export default errorHandler;
