/** Account specific routers for the email service
 * in expressjs
 */
import express, { Router } from "express";
import { z } from "zod";

import { getPerms } from "@snip/auth/book_permissions";
import { createCollaborationRequestToken } from "@snip/auth/tokens/collaborationRequest";
import service from "@snip/database";
import { getPrimaryEmail } from "@snip/database/services/user";
import { ENTITY_USER, RESOURCE_BOOK, UserDataRet } from "@snip/database/types";

import { authMiddleWare } from "@/common/auth";
import { CollaboratorRequestEmail } from "@/email/email";
import { sendEmail } from "@/email/send";

export const bookRouter: Router = express.Router();

const InviteCollaboratorSchema = z.object({
    email: z.string().email(), //User to invite
    book_id: z.number().int(),
});

/** Invite a user to collaborate on a book
 */
bookRouter.post(
    "/inviteCollaborator",
    authMiddleWare,
    express.json(),
    async (req, res) => {
        const parsedBody = InviteCollaboratorSchema.safeParse(req.body);
        if (!parsedBody.success) {
            res.status(400).json({
                error: "Invalid request body",
                details: parsedBody.error.format(),
            });
            return;
        }
        const { email, book_id } = parsedBody.data;

        // Check if user has permission to invite
        const user_id = req.session!.user?.id;
        const { resolved: perms } = await getPerms(book_id, {
            user_id: user_id,
            //ui_token_id: req.session.anonymous?.tokens[0]?.id, //only by user!
        });
        if (!perms.pACL || !user_id) {
            res.status(400).json({
                error: "Not allowed to invite to this book",
            });
            return;
        }

        // Get potentialCollaborator and his status in the book
        let invitedUsers: UserDataRet[];
        try {
            invitedUsers = await service.user.getByEmail(email);
        } catch (error) {
            if (error instanceof Error && error.name === "NotFoundError") {
                res.status(400).json({
                    error: "User not found",
                });
                return;
            }
            res.status(500).json({
                error: "Failed to get user",
                details: (error as Error).message,
            });
            return;
        }

        // Remove all users that are already collaborators
        for (const invitedUser of invitedUsers) {
            if (await isAlreadyCollaborator(invitedUser.id, book_id)) {
                invitedUsers = invitedUsers.filter(
                    (user) => user.id !== invitedUser.id,
                );
            }
        }

        if (invitedUsers.length === 0) {
            res.status(400).json({
                error: "Invited user is already a collaborator",
            });
            return;
        }

        // Check if any standing invites
        let pendingInvites = (
            await Promise.all(
                invitedUsers.map((invitedUser) =>
                    service.collaboratorsInvite.getPendingByBookIdAndUserId(
                        book_id,
                        invitedUser.id,
                    ),
                ),
            )
        ).flat();

        //Check all invites older than 2 hours
        const now = new Date();
        const twoHoursAgo = new Date(now.getTime() - 1000 * 60 * 60 * 2);
        pendingInvites = pendingInvites.filter((invite) => {
            if (!invite.created_at) {
                return false;
            }
            return invite.created_at > twoHoursAgo;
        });

        // Filter all users that have a pending invite
        for (const invite of pendingInvites) {
            invitedUsers = invitedUsers.filter(
                (user) => user.id !== invite.user_id,
            );
        }

        if (invitedUsers.length === 0) {
            res.status(400).json({
                error: "Invite is still pending for this user you may try again later",
                details:
                    "Invite is still pending for this user you may try again later",
            });
            return;
        }

        // Send invite to all users
        const errors = [];
        const invites = [];
        for (const invitedUser of invitedUsers) {
            // Add db entry
            const invite = await service.collaboratorsInvite.insert({
                book_id,
                user_id: invitedUser.id,
                invited_by: user_id,
            });
            invites.push(invite);

            // Get book for email information
            const [book, user] = await Promise.all([
                service.book.getById(book_id),
                service.user.getById(user_id),
            ]);
            const token = await createCollaborationRequestToken(invite);
            const inviter_email = getPrimaryEmail(user);

            try {
                const emailToSend = new CollaboratorRequestEmail(
                    book.title,
                    book.id,
                    inviter_email || "unknown",
                    token,
                );
                emailToSend.addRecipients(invitedUser.emails);
                await sendEmail(emailToSend);
            } catch (error) {
                console.error(
                    `Failed to send "ForgotPasswordEmail" email to ${invitedUser.emails}`,
                );
                console.error(error);
                errors.push({
                    error: "Failed to send email",
                    details: (error as Error).message,
                });
            }
        }
        if (errors.length > 0) {
            res.status(500).json({
                errors,
                invites,
            });
            return;
        } else {
            res.json({
                success: true,
                invites,
            });
        }
    },
);

// check if row for user exist in permissions table
export async function isAlreadyCollaborator(user_id: number, book_id: number) {
    try {
        await service.permission.getByEntityAndResource(
            user_id,
            book_id,
            ENTITY_USER,
            RESOURCE_BOOK,
        );
    } catch (error) {
        if (error instanceof Error && error.message.includes("not found")) {
            return false;
        }
        throw error;
    }
    return true;
}
