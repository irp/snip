import request from "supertest";
import { server } from "../../server";
import { Email } from "@/email/email";

const config = {
    send_email: false,
    error_email: false,
};

const resetConfig = () => {
    config.send_email = false;
    config.error_email = false;
};

vi.mock("@/email/send", async (importActual) => {
    const actual = await importActual<typeof import("@/email/send")>();
    return {
        sendEmail: async (email: Email) => {
            if (config.error_email) {
                throw new Error("Failed to send email");
            }

            if (config.send_email) {
                return await actual.sendEmail(email);
            } else {
                //console.log("send email", email);
            }
            return;
        },
    };
});

vi.mock("@snip/auth/session", async () => {
    return {
        getSession: async () => {
            // @ts-expect-error mockConfig is not defined
            return globalThis.mockConfig.session;
        },
    };
});

vi.mock("@snip/auth/tokens/passwordReset", async (importActual) => {
    return {
        createPasswordResetToken: async (args: any) => {
            return "validtoken";
        },
        unsealPasswordResetToken: async (token: string) => {
            return {
                time: Date.now(),
                user_id: 1,
                type: "password_reset",
            };
        },
    };
});

describe("account router", () => {
    let consoleSpy;
    beforeAll(() => {
        consoleSpy = vi.spyOn(console, "error").mockImplementation(() => {});
    });

    beforeEach(() => {
        resetConfig();
    });

    afterAll(() => {
        server.close();
    });

    describe("POST /email/account/passwordReset/:email", () => {
        it("return 400 on invalid email", async () => {
            const res = await request(server)
                .post("/email/account/passwordReset/invalidemail")
                .set("X-Forwarded-For", "192.168.2.1")
                .expect(400)
                .expect("Content-Type", /json/);
            expect(res.body).toEqual({
                error: "Invalid email address",
            });
        });

        it("return 200 on valid email and send email", async () => {
            const res = await request(server)
                .post("/email/account/passwordReset/sebastian@test.de")
                .set("X-Forwarded-For", "192.168.2.2")
                .expect(200)
                .expect("Content-Type", /json/);

            expect(res.body).toEqual({
                success: true,
            });
        });

        it("handle error on send email", async () => {
            config.error_email = true;
            const res = await request(server)
                .post("/email/account/passwordReset/sebastian@test.de")
                .set("X-Forwarded-For", "192.168.2.6")
                .expect(500)
                .expect("Content-Type", /json/);

            expect(res.body).toMatchObject({
                error: "Failed to send email",
            });
        });

        it("should return 400 user not found", async () => {
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.service.not_found = true;
            const res = await request(server)
                .post("/email/account/passwordReset/sebastian@notfound.de")
                .set("X-Forwarded-For", "192.168.2.3")
                .expect(400)
                .expect("Content-Type", /json/);

            expect(res.body).toEqual({
                error: "Bad request",
                details: "User not found",
            });
        });

        it("should return 429 on rate limit", async () => {
            let res: any;
            for (let i = 0; i < 10; i++) {
                res = await request(server)
                    .post("/email/account/passwordReset/sebastian@notfound.de")
                    .set("X-Forwarded-For", "192.168.2.5")
                    .expect("Content-Type", /json/);
            }
            expect(res.body).toEqual({
                error: "Too Many Requests",
                details: "Please wait 5s between each request",
            });
        });
    });

    describe("POST /email/account/updatePassword", () => {
        it("return 400 on invalid body", async () => {
            const res = await request(server)
                .post("/email/account/updatePassword")
                .set("X-Forwarded-For", "192.168.100.1")
                .expect(400);
            expect(res.body).toMatchObject({
                error: "Invalid request",
            });
        });

        it("return 400 on invalid token", async () => {
            // @ts-ignore
            globalThis.mockConfig.service.throw_error =
                "user.getPasswordCredentialByEmail";
            const res = await request(server)
                .post("/email/account/updatePassword")
                .set("X-Forwarded-For", "192.168.100.2")
                .send({
                    email: "test@test.de",
                    token: "invalidtoken",
                })
                .expect(400)
                .expect("Content-Type", /json/);
            expect(res.body).toMatchObject({
                error: "Invalid token",
            });
        });

        it("valid token without password", async () => {
            const res = await request(server)
                .post("/email/account/updatePassword")
                .set("X-Forwarded-For", "192.168.100.3")
                .send({
                    email: "test@test.de",
                    token: "any",
                });

            expect(res.body).toMatchObject({
                valid: true,
            });
        });
    });

    describe("POST /email/account/verifyEmail", () => {
        it("return 200 on valid email and send email", async () => {
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.service.email_verified = false;
            const res = await request(server)
                .post("/email/account/verifyEmail")
                .set("X-Forwarded-For", "192.168.3.2");

            expect(res.body).toMatchObject({
                success: true,
            });
        });

        it("return 200 on already verified email", async () => {
            const res = await request(server)
                .post("/email/account/verifyEmail")
                .set("X-Forwarded-For", "192.168.3.22");

            expect(res.body).toMatchObject({
                success: true,
            });
        });

        it("should return 400 user not found", async () => {
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.service.not_found = true;

            const res = await request(server)
                .post("/email/account/verifyEmail")
                .set("X-Forwarded-For", "192.168.3.3")
                .expect(400)
                .expect("Content-Type", /json/);
            expect(res.body).toEqual({
                error: "User not found",
            });
        });

        it("should return 429 on rate limit", async () => {
            let res: any;
            for (let i = 0; i < 10; i++) {
                res = await request(server)
                    .post("/email/account/verifyEmail")
                    .set("X-Forwarded-For", "192.168.3.5")
                    .expect("Content-Type", /json/);
            }
            expect(res.body).toEqual({
                error: "Too Many Requests",
                details: "Please wait 5s between each request",
            });
        });
    });

    /** Change Email
     *
     */
    describe("POST /email/account/changeEmail", () => {
        it("return 400 on invalid body", async () => {
            const res = await request(server)
                .post("/email/account/changeEmail")
                .set("X-Forwarded-For", "192.168.4.1")
                .expect(400)
                .expect("Content-Type", /json/);
            expect(res.body).toEqual({
                error: "Invalid request body",
                details: "Valid Email, password and credential_id required",
            });
        });

        it("return 400 on invalid email", async () => {
            const res = await request(server)
                .post("/email/account/changeEmail")
                .set("X-Forwarded-For", "192.168.4.2")
                .send({
                    newEmail: "invalidemail",
                    password: "valid",
                    credential_id: 1,
                })
                .expect(400)
                .expect("Content-Type", /json/);
            expect(res.body).toEqual({
                error: "Invalid request body",
                details: "Valid Email, password and credential_id required",
            });
        });

        it("return 400 on invalid credential_id", async () => {
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.service.throw_error = "getByCredentialId";

            const res = await request(server)
                .post("/email/account/changeEmail")
                .set("X-Forwarded-For", "192.168.50.1")
                .send({
                    newEmail: "valid@email.de",
                    password: "valid",
                    credential_id: 1,
                })
                .expect(400);

            expect(res.body).toEqual({
                error: "Invalid credential",
                details: "Credential not found",
            });
        });

        it("return 400 if credentials fo not match user", async () => {
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.session.user.id = 2;

            const res = await request(server)
                .post("/email/account/changeEmail")
                .set("X-Forwarded-For", "192.168.50.2")
                .send({
                    newEmail: "valid@email.de",
                    password: "valid",
                    credential_id: 1,
                })
                .expect(401);

            expect(res.body).toEqual({
                error: "Unauthorized",
                details: "Credential does not belong to user",
            });
        });

        it("return 400 on invalid password", async () => {
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.service.throw_error = "loginWithPassword";
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.session.user.id = 1;
            const res = await request(server)
                .post("/email/account/changeEmail")
                .set("X-Forwarded-For", "192.168.4.3")
                .send({
                    newEmail: "test@test.de",
                    password: "does not match set pw",
                    credential_id: 1,
                })
                .expect(400)
                .expect("Content-Type", /json/);

            expect(res.body).toEqual({
                error: "Invalid password",
            });
        });
        it("return 400 if email is already registered", async () => {
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.session.user.id = 1;
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.service.throw_error =
                "getPasswordCredentialByEmails";
            const res = await request(server)
                .post("/email/account/changeEmail")
                .set("X-Forwarded-For", "192.168.4.4")
                .send({
                    newEmail: "test@test.de",
                    password: "valid",
                    credential_id: 1,
                });

            expect(res.body).toEqual({
                error: "Email already registered on other account!",
            });
        });

        it("should register a new email", async () => {
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.session.user.id = 1;
            // @ts-expect-error mockConfig is not defined
            globalThis.mockConfig.service.not_found =
                "getPasswordCredentialByEmail";
            const res = await request(server)
                .post("/email/account/changeEmail")
                .set("X-Forwarded-For", "192.168.4.7")
                .send({
                    newEmail: "test@test.de",
                    password: "valid",
                    credential_id: 1,
                });
            expect(res.body).toEqual({
                success: true,
            });
        });
    });

    /** Uncomment to send emails as test!
     *
    describe("SEND EMAILS", () => {
        const addr = "youremailhere@uni-goettingen.de";
        it("verifyEmail", async () => {
            send_email = true;
            user_email = addr;
            await request(server)
            .post("/email/account/verifyEmail/")
            .set("X-Forwarded-For", "192.168.99.1")
            .expect(200);
            user_email = "test@test.de";
        });
        it("passwordReset", async () => {
            send_email = true;
            await request(server)
            .post("/email/account/passwordReset/" + addr)
            .set("X-Forwarded-For", "192.168.99.2")
            .expect(200);
        });
        it("changeEmail", async () => {
            send_email = true;
            user_not_found = true;
            const res = await request(server)
            .post("/email/account/changeEmail")
            .set("X-Forwarded-For", "192.168.99.3")
            .send({
                email: addr,
                password: "valid",
            })
            .expect(200);
            
            expect(res.body).toEqual({
                success: true,
            });
            
            send_email = false;
            user_not_found = false;
        });
    });
    */
});
