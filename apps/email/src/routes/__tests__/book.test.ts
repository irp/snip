import request from "supertest";
import { server } from "../../server";

import { Email } from "@/email/email";
import { NotFoundError } from "@snip/database/errors";
import { CollaborationInviteDataRet, UserDataRet } from "@snip/database/types";
import service from "@snip/database";

const config = {
    send_email: false,
    allowedToInvite: false,
    isAlreadyCollaborator: false,
    invitedUserFound: false,
    inviteAlreadySent: false,
};

const resetConfig = () => {
    config.send_email = false;
    config.allowedToInvite = false;
    config.isAlreadyCollaborator = false;
    config.invitedUserFound = false;
    config.inviteAlreadySent = false;
};

vi.mock("@/email/send", async (importActual) => {
    const actual = await importActual<typeof import("@/email/send")>();
    return {
        sendEmail: async (email: Email) => {
            if (config.send_email) {
                return await actual.sendEmail(email);
            } else {
                //console.debug("send email", email);
            }
            return;
        },
    };
});

vi.mock("@snip/auth/session", async () => {
    return {
        getSession: async () => {
            // @ts-expect-error mockConfig is not defined
            return globalThis.mockConfig.session;
        },
    };
});

vi.mock("@snip/auth/book_permissions", async (importActual) => {
    return {
        getPerms: async (book_id: number, user: { user_id: number }) => {
            return {
                resolved: {
                    pACL: config.allowedToInvite,
                },
            };
        },
    };
});

vi.spyOn(service.user, "getByEmail").mockImplementation(async (_email) => {
    if (config.invitedUserFound) {
        return [
            {
                id: 1,
                emails: ["test@test.de"],
            } as UserDataRet,
        ];
    }
    throw new NotFoundError("user", 1);
});

vi.spyOn(service.permission, "getByEntityAndResource").mockImplementation(
    async (...args: any[]) => {
        if (config.isAlreadyCollaborator) {
            return {
                id: 1,
            } as any;
        } else {
            throw new NotFoundError("permission", 1);
        }
    },
);

vi.spyOn(
    service.collaboratorsInvite,
    "getPendingByBookIdAndUserId",
).mockImplementation(async (book_id, user_id) => {
    if (config.inviteAlreadySent) {
        return [
            {
                book_id: 1,
                user_id: 1,
                invited_by: 1,
                created_at: new Date(),
            } as CollaborationInviteDataRet,
        ];
    }
    return [];
});

describe("book router", () => {
    afterAll(() => {
        server.close();
    });

    beforeEach(() => {
        resetConfig();
    });

    describe("POST /email/book/inviteCollaborator", () => {
        describe("invalid requests", () => {
            it("400 if request body is invalid", async () => {
                const response = await request(server)
                    .post("/email/book/inviteCollaborator")
                    .send({ email: "test" });

                expect(response.status).toBe(400);
                expect(response.body).toMatchObject({
                    error: "Invalid request body",
                });
            });

            it("400 if user is not allowed to invite", async () => {
                const response = await request(server)
                    .post("/email/book/inviteCollaborator")
                    .send({ email: "test@test.de", book_id: 1 });

                expect(response.status).toBe(400);
                expect(response.body).toEqual({
                    error: "Not allowed to invite to this book",
                });
            });

            it("400 if invited user not found", async () => {
                config.allowedToInvite = true;

                const response = await request(server)
                    .post("/email/book/inviteCollaborator")
                    .send({ email: "test@test.de", book_id: 1 });

                expect(response.status).toBe(400);
                expect(response.body).toEqual({
                    error: "User not found",
                });
            });

            it("400 if user is already collaborator", async () => {
                config.allowedToInvite = true;
                config.invitedUserFound = true;
                config.isAlreadyCollaborator = true;

                const response = await request(server)
                    .post("/email/book/inviteCollaborator")
                    .send({ email: "test@test.de", book_id: 1 });

                expect(response.status).toBe(400);
                expect(response.body).toEqual({
                    error: "Invited user is already a collaborator",
                });
            });

            it("400 if invite already sent", async () => {
                config.allowedToInvite = true;
                config.invitedUserFound = true;
                config.inviteAlreadySent = true;

                const response = await request(server)
                    .post("/email/book/inviteCollaborator")
                    .send({ email: "test@test.de", book_id: 1 });

                expect(response.status).toBe(400);
                expect(response.body).toEqual({
                    error: "Invite is still pending for this user you may try again later",
                    details:
                        "Invite is still pending for this user you may try again later",
                });
            });
        });

        it("200 if invite sent", async () => {
            config.allowedToInvite = true;
            config.invitedUserFound = true;
            config.isAlreadyCollaborator = false;

            const response = await request(server)
                .post("/email/book/inviteCollaborator")
                .send({ email: "test@test.de", book_id: 1 });

            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty("success", true);
        });

        /** Uncomment to send emails as test!
         *
        describe("SEND EMAILS", () => {
            const addr = "sebastianbernd.mohr@uni-goettingen.de";
            it("collaborationInvite", async () => {
                config.allowedToInvite = true;
                config.invitedUserFound = true;
                config.send_email = true;
                
                const response = await request(server)
                    .post("/email/book/inviteCollaborator")
                    .send({
                        email: addr,
                        book_id: 1,
                    });

                expect(response.status).toBe(200);
                expect(response.body).toEqual({
                    success: true,
                    invite: {
                        book_id: 1,
                        user_id: 1,
                        invited_by: 1,
                    },
                });
            });
        });
        */
    });
});
