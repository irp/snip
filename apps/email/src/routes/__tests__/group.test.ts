import request from "supertest";
import { server } from "@/server";

import { Email } from "@/email/email";

import service from "@snip/database";

const config: any = {
    send_email: false,
    allowedToEdit: false,
    groupFound: false,
    invitedUserFound: false,
    memberAlreadyInGroup: "invited",
    joinDate: undefined,
};

const resetConfig = () => {
    config.send_email = false;
    config.allowedToEdit = false;
    config.groupFound = false;
    config.invitedUserFound = false;
    config.memberAlreadyInGroup = "invited";
    config.joinDate = undefined;
};

vi.mock("@/email/send", async (importActual) => {
    const actual = await importActual<typeof import("@/email/send")>();
    return {
        sendEmail: async (email: Email) => {
            if (config.send_email) {
                return await actual.sendEmail(email);
            } else {
                //console.debug("send email", email);
            }
            return;
        },
    };
});

vi.mock("@snip/auth/session", async () => {
    return {
        getSession: async () => {
            // @ts-expect-error mockConfig is not defined
            return globalThis.mockConfig.session;
        },
    };
});

vi.spyOn(service.group, "isAllowedToEdit").mockImplementation(
    async (user_id, group_id) => {
        return config.allowedToEdit;
    },
);

vi.spyOn(service.user, "getByEmail").mockImplementation(
    async (email: string) => {
        if (config.invitedUserFound) {
            return [
                {
                    id: 1,
                    emails: [email],
                    joined_at: config.joinDate || new Date(),
                },
            ] as any;
        } else {
            return [];
        }
    },
);

vi.spyOn(service.group, "getMember").mockImplementation(
    async (user_id, group_id) => {
        if (config.memberAlreadyInGroup) {
            return {
                user_id: 1,
                role: config.memberAlreadyInGroup,
                joined_at: config.joinDate,
            } as any;
        }
        throw new Error("not found");
    },
);

describe("group router", () => {
    afterAll(() => {
        server.close();
    });

    beforeEach(() => {
        resetConfig();
    });

    describe("POST /email/group/inviteUser", () => {
        describe("invalid requests", () => {
            it("400 if invalid request body", async () => {
                const response = await request(server)
                    .post("/email/group/inviteUser")
                    .send({
                        email: "invalid email",
                    })
                    .expect(400);

                expect(response.body).toEqual({
                    error: "Invalid request body",
                    details: "Valid Email required",
                });
            });

            it("400 if not allowed to edit", async () => {
                config.allowedToEdit = false;
                const response = await request(server)
                    .post("/email/group/inviteUser")
                    .send({
                        email: "test@test.de",
                        id: 1,
                    });
                expect(response.status).toBe(400);
            });

            it("400 if group not found", async () => {
                config.allowedToEdit = true;
                // @ts-ignore
                globalThis.mockConfig.service.not_found = "group.getById";

                const response = await request(server)
                    .post("/email/group/inviteUser")
                    .send({
                        email: "test@test.de",
                        id: 1,
                    });
                expect(response.status).toBe(400);
            });

            it("400 if invited user not found", async () => {
                config.allowedToEdit = true;
                config.invidedUserFound = false;

                const response = await request(server)
                    .post("/email/group/inviteUser")
                    .send({
                        email: "test@test.de",
                        id: 1,
                    });
                expect(response.status).toBe(400);
                expect(response.body).toEqual({
                    error: "User not found",
                });
            });

            it("400 if member already in group (owner moderator member)", async () => {
                config.allowedToEdit = true;
                config.groupFound = true;
                config.invitedUserFound = true;
                config.joinDate = new Date();
                for (const role of ["owner", "moderator", "member"]) {
                    config.memberAlreadyInGroup = role;
                    const response = await request(server)
                        .post("/email/group/inviteUser")
                        .send({
                            email: "test@test.de",
                            id: 1,
                        });
                    expect(response.status).toBe(400);
                    expect(response.body).toEqual({
                        error: "User already in group",
                    });
                }
            });

            it("400 if member already in group (invited)", async () => {
                config.allowedToEdit = true;
                config.groupFound = true;
                config.invitedUserFound = true;
                config.memberAlreadyInGroup = "invited";
                config.joinDate = new Date();
                const response = await request(server)
                    .post("/email/group/inviteUser")
                    .send({
                        email: "test@test.de",
                        id: 1,
                    });
                expect(response.status).toBe(400);
                expect(response.body).toMatchObject({
                    error: "Invite is still pending for this user!",
                });
            });
        });

        it("200 if successful (member not existing)", async () => {
            config.allowedToEdit = true;
            config.groupFound = true;
            config.invitedUserFound = true;
            config.memberAlreadyInGroup = undefined;
            const response = await request(server)
                .post("/email/group/inviteUser")
                .send({
                    email: "test@test.de",
                    id: 1,
                });
            expect(response.status).toBe(200);
        });

        it("200 if successful (member existing) allows resend email", async () => {
            config.allowedToEdit = true;
            config.groupFound = true;
            config.invitedUserFound = true;
            config.memberAlreadyInGroup = "invited";
            // 4 hours ago
            config.joinDate = new Date(Date.now() - 4 * 60 * 60 * 1000);
            const response = await request(server)
                .post("/email/group/inviteUser")
                .send({
                    email: "test@test.de",
                    id: 1,
                });
            expect(response.status).toBe(200);
        });
    });

    /** Uncomment to send emails as test!
     *
    describe("SEND EMAILS", () => {
        const addr = "testr@uni-goettingen.de";
        it("groupInvite", async () => {
            config.allowedToEdit = true;
            config.groupFound = true;
            config.invitedUserFound = true;
            config.memberAlreadyInGroup = undefined;
            config.send_email = true;
            await request(server).post("/email/group/inviteUser").send({
                email: addr,
                id: 1,
            });
        });
    });
    */
});
