/** Account specific routers for the email service
 * in expressjs
 */
import express, { Router } from "express";
import { z } from "zod";

import { createEmailChangeToken } from "@snip/auth/tokens/emailChange";
import { createEmailVerifyToken } from "@snip/auth/tokens/emailVerify";
import {
    createPasswordResetToken,
    unsealPasswordResetToken,
} from "@snip/auth/tokens/passwordReset";
import service from "@snip/database";
import { UserDataRet } from "@snip/database/types";

import { authMiddleWare, authMiddleWareApiOnly } from "@/common/auth";
import { rateLimitAggressive } from "@/common/rateLimit";
import {
    ChangeEmailEmail,
    ForgotPasswordEmail,
    VerifyAccountEmail,
} from "@/email/email";
import { sendEmail } from "@/email/send";

export const accountRouter: Router = express.Router();

const PasswordResetSchema = z.string().email();

accountRouter.post(
    "/passwordReset/:email",
    rateLimitAggressive,
    async (req, res) => {
        // Parse email
        const parsedEmail = PasswordResetSchema.safeParse(req.params.email);
        if (!parsedEmail.success) {
            res.status(400).json({
                error: "Invalid email address",
            });
            return;
        }

        // Get pw reset token
        let token: string;
        try {
            const credential = await service.user.getPasswordCredentialByEmail(
                parsedEmail.data,
            );
            token = await createPasswordResetToken(credential);
        } catch (error) {
            let message: string | undefined;
            if (error instanceof Error && error.message.includes("not found")) {
                message = "User not found";
            }

            // Invalid email
            res.status(400).json({
                error: "Bad request",
                details: message,
            });
            return;
        }

        // Create and send email
        // TODO: add cc
        try {
            const email = new ForgotPasswordEmail(parsedEmail.data, token);
            await sendEmail(email);
            res.status(200).json({
                success: true,
            });
        } catch (error) {
            console.error(
                `Failed to send "ForgotPasswordEmail" email to ${parsedEmail.data}`,
            );
            console.error(error);
            res.status(500).json({
                error: "Failed to send email",
                details: (error as Error).message,
            });
        }
    },
);

const UpdatePasswordSchema = z.object({
    email: z.string().email(),
    token: z.string(),
    password: z.string().optional(),
});

/** Given a token generates sets a new password
 */
accountRouter.post(
    "/updatePassword",
    rateLimitAggressive,
    express.json(),
    async (req, res) => {
        const parsedBody = UpdatePasswordSchema.safeParse(req.body);
        if (parsedBody.success == false) {
            res.status(400).json({
                error: "Invalid request",
                details: parsedBody.error.format(),
            });
            return;
        }

        const { email, token, password } = parsedBody.data;

        // Try to unseal token
        try {
            const credential =
                await service.user.getPasswordCredentialByEmail(email);
            await unsealPasswordResetToken(token, credential);
        } catch (_error) {
            res.status(400).json({
                error: "Invalid token",
            });
            return;
        }

        // If no password is set return valid (validation purpose)
        if (!password || password.length === 0) {
            res.status(200).json({
                valid: true,
            });
            return;
        }

        // Valid token
        try {
            await service.user.changePassword(email, password);
        } catch (_error) {
            console.error(_error);
            res.status(500).json({
                error: "Failed to reset password",
                details: (_error as Error).message,
            });
            return;
        }
        res.status(200).json({
            success: true,
        });
    },
);

/** Send email to user to request
 * verification of their email address
 * todo: handle multiple emails
 */
accountRouter.post(
    "/verifyEmail",
    authMiddleWare,
    rateLimitAggressive,
    async (req, res) => {
        if (!req.session?.user) {
            res.status(401).json({
                error: "Unauthorized",
            });
            return;
        }

        // Check if user exists
        let pw_cred;
        try {
            pw_cred = await service.user.getPasswordCredentialByUserId(
                req.session.user.id,
            );
        } catch (error) {
            if (error instanceof Error && error.message.includes("not found")) {
                res.status(400).json({
                    error: "User not found",
                });
                return;
            } else {
                throw error;
            }
        }

        if (pw_cred.email_verified) {
            res.status(200).json({
                success: true,
                error: "Email already verified!",
            });
            return;
        }

        const token = await createEmailVerifyToken(pw_cred);

        try {
            const email = new VerifyAccountEmail(pw_cred.email, token);
            await sendEmail(email);
            res.json({
                success: true,
            });
        } catch (error) {
            console.error(
                `Failed to send "VerifyAccountEmail" to ${pw_cred.email}`,
            );
            console.error(error);
            res.status(500).json({
                error: "Failed to send email",
                details: (error as Error).message,
            });
        }
    },
);

/** Send verify email by api endpoint
 * i.e. from other services (not the user)
 */
accountRouter.post(
    "/verifyEmail/:user_id",
    authMiddleWareApiOnly,
    async (req, res) => {
        if (!req.session?.api) {
            res.status(401).json({
                error: "Unauthorized",
            });
            return;
        }
        const user_id = parseInt(req.params.user_id || "nan");

        if (isNaN(user_id)) {
            res.status(400).json({
                error: "Invalid user id",
            });
            return;
        }

        // Check if user exists
        let pw_cred;
        try {
            pw_cred = await service.user.getPasswordCredentialByUserId(user_id);
        } catch (error) {
            if (error instanceof Error && error.message.includes("not found")) {
                res.status(400).json({
                    error: "User not found",
                });
                return;
            } else {
                throw error;
            }
        }

        if (pw_cred.email_verified) {
            res.status(200).json({
                success: true,
                error: "Email already verified!",
            });
            return;
        }

        const token = await createEmailVerifyToken(pw_cred);

        try {
            const email = new VerifyAccountEmail(pw_cred.email, token);
            await sendEmail(email);
            res.json({
                success: true,
            });
        } catch (error) {
            console.error(
                `Failed to send "VerifyAccountEmail" to ${pw_cred.email}`,
            );
            console.error(error);
            res.status(500).json({
                error: "Failed to send email",
                details: (error as Error).message,
            });
        }
    },
);

const ChangeEmailSchema = z.object({
    newEmail: z.string().email(),
    password: z.string(),
    credential_id: z.coerce.number(),
});

/** Change the email addr
 * of the user
 */
accountRouter.post(
    "/changeEmail",
    authMiddleWare,
    rateLimitAggressive,
    express.json(),
    async (req, res) => {
        if (!req.session?.user) {
            res.status(401).json({
                error: "Unauthorized",
            });
            return;
        }
        const user_id = req.session.user.id;

        // Parse body
        const parsedBody = ChangeEmailSchema.safeParse(req.body);
        if (!parsedBody.success) {
            res.status(400).json({
                error: "Invalid request body",
                details: "Valid Email, password and credential_id required",
            });
            return;
        }
        const { newEmail, password, credential_id } = parsedBody.data;

        // Check if password matches
        let user: UserDataRet;
        try {
            user = await service.user.getByCredentialId(credential_id);
        } catch (error) {
            console.error(error);
            res.status(400).json({
                error: "Invalid credential",
                details: "Credential not found",
            });
            return;
        }
        if (user.id !== user_id) {
            res.status(401).json({
                error: "Unauthorized",
                details: "Credential does not belong to user",
            });
            return;
        }

        // Check that only (local) password credentials are allowed
        const cIdx = user.credential_ids.findIndex(
            (cred) => cred === credential_id,
        );
        if (
            cIdx === -1 ||
            user.credential_types[cIdx] !== "password" ||
            !user.emails[cIdx]
        ) {
            res.status(400).json({
                error: "Invalid credential",
                details:
                    "Only password credentials are allowed to be changed manually.",
            });
            return;
        }

        // Login with password
        try {
            user = await service.user.loginWithPassword(
                user.emails[cIdx],
                password,
            );
        } catch (error) {
            console.error(error);
            res.status(400).json({
                error: "Invalid password",
            });
            return;
        }

        // Check if email already exists
        try {
            // should error if email is not found
            await service.user.getPasswordCredentialByEmail(newEmail);
            res.status(400).json({
                error: "Email already registered on other account!",
            });
            return;
        } catch (error) {
            if (error instanceof Error && error.message.includes("not found")) {
                // Do nothing
            } else {
                res.status(500).json({
                    error: "Internal server error",
                    details: (error as Error).message,
                });
                return;
            }
        }

        const token = await createEmailChangeToken(
            user,
            credential_id,
            newEmail,
        );

        try {
            const email = new ChangeEmailEmail(newEmail, token, credential_id);
            email.addRecipient(newEmail);
            await sendEmail(email);
            res.status(200).json({
                success: true,
            });
        } catch (error) {
            console.error(
                `Failed to send "ChangeEmailEmail" email to ${newEmail}`,
            );
            console.error(error);
            res.status(500).json({
                error: "Failed to send email",
                details: (error as Error).message,
            });
        }
    },
);
