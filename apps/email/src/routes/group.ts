/** Group specific routers for the email service
 * in expressjs
 */
import express, { Router } from "express";
import { z } from "zod";

import { createGroupInviteToken } from "@snip/auth/tokens/groupInvite";
import service from "@snip/database";
import { getPrimaryEmail } from "@snip/database/services/user";
import { GroupDataRet, MemberDataRet } from "@snip/database/types";

import { authMiddleWare } from "@/common/auth";
import { asyncHandler } from "@/common/errorHandler";
import { GroupInviteEmail } from "@/email/email";
import { sendEmail } from "@/email/send";

export const groupRouter: Router = express.Router();

const InviteUserSchema = z.object({
    email: z.string().email(), //User to invite
    id: z.number().int(), //Group id
});

groupRouter.post(
    "/inviteUser",
    authMiddleWare,
    express.json(),
    asyncHandler(async (req, res) => {
        const user_id = req.session!.user!.id;
        const parsedBody = InviteUserSchema.safeParse(req.body);
        if (!parsedBody.success) {
            res.status(400).json({
                error: "Invalid request body",
                details: "Valid Email required",
            });
            return;
        }
        const { email: invite_email, id: group_id } = parsedBody.data;

        // Check if user is allowed to invite
        // this also checks if the user is in group
        // i.e. in group and is owner/moderator
        const isAllowedToEdit = await service.group.isAllowedToEdit(
            user_id,
            group_id,
        );
        if (!isAllowedToEdit) {
            res.status(400).json({
                error: "Not allowed to invite",
            });
            return;
        }

        // Get group
        let group: GroupDataRet;
        try {
            group = await service.group.getById(group_id);
        } catch (error) {
            if (error instanceof Error && error.message.includes("not found")) {
                res.status(400).json({
                    error: "Group not found",
                });
                return;
            }
            throw error;
        }

        // Check if user exists
        // Shouldnt error if user is not found
        let invitedUsers = await service.user.getByEmail(invite_email);
        if (invitedUsers.length === 0) {
            res.status(400).json({
                error: "User not found",
            });
            return;
        }

        // Remove all users that are already in group
        const potentialMembers: Required<MemberDataRet>[] = [];
        for (const invitedUser of invitedUsers) {
            const pMember = await service.group
                .getMember(invitedUser.id, group_id)
                .catch((error) => {
                    if (
                        error instanceof Error &&
                        error.message.includes("not found")
                    ) {
                        return null;
                    }
                    throw error;
                });
            if (pMember && pMember.role !== "invited") {
                invitedUsers = invitedUsers.filter(
                    (user) => user.id !== invitedUser.id,
                );
            }
            if (pMember && pMember.role === "invited") {
                potentialMembers.push(pMember);
            }
        }

        if (invitedUsers.length === 0) {
            res.status(400).json({
                error: "User already in group",
            });
            return;
        }

        // Check if invite is still valid
        // Allow to resend every 2hours
        let try_again_in = 2 * 60 * 60 * 1000;
        for (const potentialMember of potentialMembers) {
            if (
                Date.now() - potentialMember.joined_at.getTime() <
                2 * 60 * 60 * 1000
            ) {
                try_again_in = Math.min(
                    try_again_in,
                    2 * 60 * 60 * 1000 -
                        (Date.now() - potentialMember.joined_at.getTime()),
                );
                invitedUsers = invitedUsers.filter(
                    (user) => user.id !== potentialMember.user_id,
                );
            }
        }

        if (invitedUsers.length === 0) {
            res.status(400).json({
                error: "Invite is still pending for this user!",
                details: `You may try again in ${try_again_in / 1000}s`,
            });
            return;
        }

        // Get user for email information
        const user = await service.user.getById(user_id);
        const primaryEmail = getPrimaryEmail(user);

        const members = [];
        const errors = [];
        for (const invitedUser of invitedUsers) {
            // Add db entry
            const member = await service.group.addMember(
                invitedUser.id,
                group_id,
                "invited",
            );
            members.push(member);

            // Generate token and send email
            const token = await createGroupInviteToken(
                invitedUser,
                member,
                user_id,
                group,
            );
            const email = new GroupInviteEmail(
                group.name,
                group.id,
                token,
                primaryEmail,
            );
            email.addRecipients(invitedUser.emails);
            try {
                await sendEmail(email);
            } catch (error) {
                console.error(
                    `Failed to send "GroupInviteEmail" email to ${invitedUser.emails}`,
                );
                errors.push({
                    error: "Failed to send email",
                    details: (error as Error).message,
                });
            }
        }

        if (errors.length > 0) {
            res.status(500).json({
                errors,
                members,
            });
            return;
        } else {
            res.json({
                members,
            });
        }
    }),
);
