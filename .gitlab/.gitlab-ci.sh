# Helper scripts for the .gitlab-ci.yml file


# Function to build and register a container image using Kaniko
#
# Parameters:
#   file: The path to the Dockerfile relative to the project directory.
#   target: The build target specified in the Dockerfile.
#   name: The name of the container image (optional, defaults to the target if not provided).
# Description:
# This function sets up the Docker authentication configuration and uses Kaniko to build
# the container image from the specified Dockerfile and target. It then registers the
# image in the container registry with multiple tags including 'latest', the short commit SHA,
# and the commit reference name.
function build_and_register_container() {
    local file=$1
    local target=$2
    local name=${3:-$target}  # Use target as default if name is not provided


    echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json

    echo "Building and registering container $name from $file with target $target"
    /kaniko/executor --cache=true --cache-dir="/cache" --cache-ttl=168h --cache-repo "$CI_REGISTRY_IMAGE/cache" \
        --single-snapshot \
        --context $CI_PROJECT_DIR \
        --dockerfile "$CI_PROJECT_DIR/$file" \
        --target $target \
        --destination $CI_REGISTRY_IMAGE/$name:latest \
        --destination $CI_REGISTRY_IMAGE/$name:$CI_COMMIT_SHORT_SHA \
        --destination $CI_REGISTRY_IMAGE/$name:$CI_COMMIT_REF_NAME \
        --no-push-cache
}


# Function to cache a container image using Kaniko
#
# Parameters:
#   file: The path to the Dockerfile relative to the project directory.
#   target: The build target specified in the Dockerfile.
#   name: The name of the container image (optional, defaults to the target if not provided).
# Description:
# This function sets up the Docker authentication configuration and uses Kaniko to build
# the container image from the specified Dockerfile and target. It caches the build layers
# in the container registry with a cache TTL of 168 hours (7 days). The cached layers are
# stored in a repository named 'cache' within the container registry. The function then
# registers the image in the container registry with the tag 'latest'.
function cache_container(){
    local file=$1
    local target=$2
    local name=${3:-$target}  # Use target as default if name is not provided

    echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    echo "Caching container $name from $file with target $target"
    /kaniko/executor --cache=true --cache-dir="/cache" --cache-ttl=168h --cache-repo "$CI_REGISTRY_IMAGE/cache" \
        --context $CI_PROJECT_DIR \
        --dockerfile "$CI_PROJECT_DIR/$file" \
        --target $target \
        --no-push \
        --no-push-cache
}



# Function to retag a container image
#
# Parameters:
#   name: The name of the container image.
#   tag: The new tag to apply to the container image.
# Description:
# This function retags the latest version of the specified container image with a new tag.
# It uses the 'crane' tool to copy the image from the 'latest' tag to the new tag in the container registry.
function retag_container() {
    local name=$1
    local tag=$2

    echo "Retagging container $name:$CI_COMMIT_SHORT_SHA as $name:$tag"
    crane cp "$CI_REGISTRY_IMAGE/$name:$CI_COMMIT_SHORT_SHA" $CI_REGISTRY_IMAGE/$name:$tag
}
