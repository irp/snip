#!/bin/bash
ARGS=$(getopt -a --options l: --long "last:,help" -- "$@")

eval set -- "$ARGS"

FOLDER_LOCAL="/tmp/backup/"
FOLDER_DOCKER="/backup/"

# Default values
last=$(ls ${FOLDER_LOCAL} | tail -n 1)

# Get args using shift
while true; do
    case "$1" in
        -l|--last)
            last="$2"
            echo "last: $last"
        shift 2;;
        --help)
            echo "Usage: $0 [--last <last-backup-to-use>] If none is given automatically uses the most resent one."
        exit 0;;
        --)
        break;;
    esac
done


# Case  1: Perform restore from incremental backup
incremental_backup(){
    echo "Incremental backup not yet implemented"
    echo "TODO"

    # Get last full backup
    for folder in $(ls ${FOLDER_LOCAL}); do
        if grep -q "backup_type = full" "${FOLDER_LOCAL}/${folder}/xtrabackup_checkpoints"; then
            full_backup_dir=$(basename "$folder")
            break
        fi
    done

    exit 1
}

# Case 2: Perform restore from full backup
restore_from_full(){
    # See https://mariadb.com/kb/en/full-backup-and-restore-with-mariabackup/
    # Prepare the backup
    docker exec mariadb mariabackup --prepare \
        --target-dir="${FOLDER_DOCKER}${last}"

    # Stop mariadb as the service is not allowed to run while restoring
    docker stop mariadb
    
    # Copy the folder to host
    docker run -v snip_database:/var/lib/mysql --name helper busybox rm -rf /var/lib/mysql/*
    docker cp "${FOLDER_LOCAL}${last}/" helper:/var/lib/mysql
    docker rm helper

    # Start mariadb
    docker start mariadb
}


# Check if the last one is an incremental update by
# checking if the "xtrabackup_checkpoints" contains "backup_type = incremental"
if grep -q "backup_type = incremental" "${FOLDER_LOCAL}${last}/xtrabackup_checkpoints"; then
    echo "Last backup is an incremental backup"
    incr_base_dir=$(basename "$last")
    incremental_backup
else
    echo "Last backup is a full backup"
    incr_base_dir=$(date +"%Y-%m-%d_%H-%M_incr")
    restore_from_full
fi

exit 0