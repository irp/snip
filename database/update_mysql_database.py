import mysql.connector
import json
import argparse
import re


def _parse_string(text):
    """
    Parse unclean text into clean text to use with json parser
    """
    return text.replace("\\n", "\n").replace("\n", "\\n")


def _get_book_id(page_id):
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(f"SELECT book_id FROM pages WHERE id = {page_id}")
    book_id = cursor.fetchone()["book_id"]
    cursor.close()
    return book_id


def create_trigger():
    cursor = mydb.cursor()

    # Create triggers
    for table in ["pages", "books"]:
        for event in ["UPDATE", "INSERT"]:
            evt_short = "ins" if event == "INSERT" else "upd"
            on_table = "snips" if table == "pages" else "pages"

            sql = f"""
            CREATE TRIGGER {evt_short}_last_updated_{table}
            AFTER {event}
            ON {on_table}
            FOR EACH ROW
            UPDATE {table}
            SET {table}.last_updated = CURRENT_TIMESTAMP()
            WHERE {table}.id = NEW.{table[:-1]}_id
            """
            cursor.execute(sql)

    # INSERT page trigger i.e. update page number
    sql = f"""
    CREATE TRIGGER ins_page_number
    BEFORE INSERT ON pages
    FOR EACH ROW 
    BEGIN
        SET NEW.page_number = (
            SELECT IFNULL(MAX(page_number),0) +1
            FROM pages
            WHERE book_id = NEW.book_id
        );
    END;
    """
    cursor.execute(sql)

    cursor.close()
    mydb.commit()


def doodles_to_snips():
    """Puts every row from the doodles table into the snips table
    as they share the same columns.
    """
    # Get doodle data
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(f"SELECT * FROM doodles")
    doodles = cursor.fetchall()

    for doodle in doodles:
        doodle_json = json.loads(doodle["json"])
        view_json = {
            "colour": doodle_json["colour"],
            "width": doodle_json["width"],
        }
        data_json = {
            "type": "doodle",
            "edges": doodle_json["edges"],
        }
        data_json = json.dumps(data_json)
        view_json = json.dumps([view_json])

        query = "INSERT INTO snips (id, last_updated, data_json, view_json,  page_id, type, book_id)"
        query += f" VALUES (NULL, '{doodle['last_updated']}', '{data_json}', '{view_json}', {doodle['page_id']}, 'doodle','{_get_book_id(doodle['page_id'])}');"
        cursor.execute(query)
    cursor.close()
    mydb.commit()

    # Delete doodles table
    cursor = mydb.cursor(raw=True)
    cursor.execute("DROP TABLE doodles")
    cursor.close()
    mydb.commit()


def extract_types_from_snips_data_json():
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(f"SELECT * FROM snips")
    snips = cursor.fetchall()

    for snip in snips:
        data_json = json.loads(snip["data_json"])

        # Check if data_json is array
        # If not, then it is a single snip
        if isinstance(data_json, list):
            snip_type = "array"
        else:
            snip_type = data_json["type"]

        # Update snip type
        query = f"UPDATE snips SET type = '{snip_type}' WHERE id = {snip['id']}"
        cursor.execute(query)

    cursor.close()
    mydb.commit()


def _insert_snips(
    type_, data_json_string, view_json_string, last_updated, page_id, book_id
):
    """Push each array snip inti the snips table individually"""
    # Query
    query = "INSERT INTO `snips` (`id`, `last_updated`, `data_json`, `type`, `view_json`, `page_id`, `book_id`) "
    if page_id is None:
        query += f" VALUES (NULL, '{last_updated}', '{data_json_string}', '{type_}', '{view_json_string}', NULL, '{book_id}');"
    else:
        query += f" VALUES (NULL, '{last_updated}', '{data_json_string}', '{type_}', '{view_json_string}', '{page_id}', '{book_id}');"

    # Execute
    cursor = mydb.cursor(raw=True)
    cursor.execute(query)
    mydb.commit()

    id = cursor.lastrowid
    cursor.close()

    return id


def some_strange_exceptions():

    cursor = mydb.cursor(dictionary=True)

    # Get all snips where the data_json contains "Westermeier, Fabian"
    cursor.execute(f"SELECT * FROM snips WHERE data_json LIKE '%Westermeier, Fabian%'")
    snips = cursor.fetchall()
    for snip in snips:
        snip[
            "data_json"
        ] = r'{"type": "text", "text": "From: Westermeier, Fabian <fabian.westermeier@desy.de>\\nTo: mosterh1@gwdg.de\\nSubject: RE: slow shutter\\nDate: Fri, 23 Apr 2021 13:01:38 +0200 (CEST)\\nMoin Markus,\\n> ku00f6nntest Du mir bitte die Register fu00fcr den slow shutter einmal\\n> elektrisch zusenden?\\nKlar:\\nSlow shutter:\\nhaspp10e2:10000/p10/register/e2.out04\\n.Value = 0 -> open\\n.Value = 1 -> close\\nGenau anders herum als beim fast shutter ...\\nSchu00f6nes Wochenende,\\nFabian"}'
        # Update
        query = f"UPDATE snips SET data_json = '{snip['data_json']}' WHERE id = {snip['id']}"
        cursor.execute(query)
        mydb.commit()
    cursor.close()

    # Drop all snips where the data_json contains "$blobid"
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(f"SELECT * FROM snips WHERE data_json LIKE '%$blobid%'")
    snips = cursor.fetchall()
    for snip in snips:
        # Drop snip
        query = f"DELETE FROM snips WHERE id = {snip['id']}"
        cursor.execute(query)
        mydb.commit()
    cursor.close()


def unfold_array_snips():
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(f"SELECT * FROM snips WHERE type = 'array'")
    snips = cursor.fetchall()

    for snip in snips:
        data_json = json.loads(snip["data_json"])
        if snip["view_json"] is None or snip["view_json"] == "":
            view_json = None
        else:
            view_json = json.loads(snip["view_json"])

        # Extract the type of the snip from the json data if length is 1
        if isinstance(data_json, list) and len(data_json) == 1:
            type_i = data_json[0]["type"]
            query = f"UPDATE snips SET type = '{type_i}', data_json = '{json.dumps(data_json[0])}' WHERE id = {snip['id']};"
        elif isinstance(data_json, list) and len(data_json) > 1:
            # Insert each snip in the array into the snips table individually
            ids = []
            for i in range(len(data_json)):
                type_i = data_json[i]["type"]
                data_json_i = json.dumps(data_json[i])
                if view_json is None or view_json[i] is None:
                    data_view_i = "NULL"
                else:
                    data_view_i = json.dumps([view_json[i]])

                id = _insert_snips(
                    type_i,
                    data_json_i,
                    data_view_i,
                    snip["last_updated"],
                    snip["page_id"],
                    snip["book_id"],
                )
                ids.append(id)

            # Define new array snip data and view
            data = {
                "type": "array",
                "snip_ids": ids,
                "legacy": True,
            }
            view = {
                "x": 0,
                "y": 0,
            }
            query = f"UPDATE snips SET type = 'array', data_json = '{json.dumps(data)}', view_json = '{json.dumps([view])}' WHERE id = {snip['id']};"
        else:
            print("Could not unfold snip:")
            print(snip)
            continue

        # Execute
        cursor.execute(query)
        mydb.commit()
    cursor.close()


def flatten_view_json():
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(f"SELECT * FROM snips WHERE view_json IS NOT NULL")
    snips = cursor.fetchall()

    for snip in snips:
        if snip["view_json"] is None or snip["view_json"] == "":
            continue

        if snip["view_json"] == "NULL":
            query = f"UPDATE snips SET view_json = NULL WHERE id = {snip['id']};"
            cursor.execute(query)
            mydb.commit()
            continue

        view_json = json.loads(snip["view_json"])

        if isinstance(view_json, list):
            view_json = view_json[0]

        query = f"UPDATE snips SET view_json = '{json.dumps(view_json)}' WHERE id = {snip['id']};"
        cursor.execute(query)
        mydb.commit()
    cursor.close()


def adjust_x_and_y_array():
    """
    Adjust x and y coordinates of the snips inside the array snips

    This is done because in the legacy version, the x and y coordinates were
    modified in the plotting.
    """

    cursor = mydb.cursor(dictionary=True)
    cursor.execute(f"SELECT * FROM snips WHERE type = 'array'")
    array_snips = cursor.fetchall()
    for array_snip in array_snips:
        if array_snip["view_json"] is None or array_snip["view_json"] == "":
            continue

        ids = json.loads(array_snip["data_json"])["snip_ids"]
        # Get all snips in array
        cursor.execute(f"SELECT * FROM snips WHERE id IN ({','.join(map(str, ids))})")
        snips = cursor.fetchall()
        for i in range(len(snips) - 1):
            if snips[i + 1]["view_json"] is None or snips[i + 1]["view_json"] == "":
                continue
            if snips[i]["view_json"] is None or snips[i]["view_json"] == "":
                continue

            # Load views
            view_next = json.loads(snips[i + 1]["view_json"])
            view = json.loads(snips[i]["view_json"])

            if snips[i]["type"] in ["text", "timestamp"]:
                # Adjust y coordinate

                text = json.loads(_parse_string(snips[i]["data_json"]))["text"]
                nLines = text.count("\n") + 1
                lineHeight = view["lheight"]
                fontSize = int(view["size"])
                view_next["y"] = view_next["y"] + nLines * lineHeight + 0.25 * fontSize

            if snips[i]["type"] == "image":
                # Adjust y coordinate
                view["rx"] = int(view["rx"])
                view["ry"] = int(view["ry"])
                view["rw"] = int(view["rw"])
                view["rh"] = int(view["rh"])
                view["width"] = int(view["width"])
                height = (view["rh"] * view["width"]) / view["rw"]
                view_next["y"] = view_next["y"] + height * 1.25

            # Reupload view_json
            snips[i + 1]["view_json"] = json.dumps(view_next)
            cursor.execute(
                f"UPDATE snips SET view_json = '{json.dumps(view_next)}' WHERE id = {snips[i+1]['id']};"
            )
            mydb.commit()

    cursor.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Update the snips database from legacy format to new format!"
    )
    """ MYSQL configs
    """
    parser.add_argument(
        "--host",
        help="The host of the database. Default: localhost",
        default="localhost",
        type=str,
    )
    parser.add_argument(
        "--user",
        help="The user of the database. Default: snip",
        default="snip",
        type=str,
    )
    parser.add_argument(
        "--password",
        help="The password of the database. Default: well_this_should_be_changed_as_well",
        default="well_this_should_be_changed_as_well",
        type=str,
    )
    parser.add_argument(
        "--database",
        help="The database of the database. Default: snip",
        default="snip",
        type=str,
    )

    """ Additional config
    parser.add_argument(
        "--force-new",
        help="Create a new database, this will overwrite the currently running database!",
    )
    """

    args = parser.parse_args()

    mydb = mysql.connector.connect(
        host=args.host,
        user=args.user,
        password=args.password,
        database=args.database,
    )

    """ --------------------------
        Books
    -------------------------- """
    queries = []

    # rename
    from_col = ["date1", "date2"]
    to_col = ["created", "finished"]
    for fr, to in zip(from_col, to_col):
        queries.append(f"ALTER TABLE books CHANGE {fr} {to} DATETIME")
    queries.append(
        "ALTER TABLE `books` CHANGE `created` `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;"
    )
    # drop year column
    queries.append(f"ALTER TABLE books DROP COLUMN year")

    # drop the index corresponding to the title column
    queries.append(f"ALTER TABLE books DROP INDEX title")

    # add last updated table
    queries.append(
        "ALTER TABLE `books` ADD `last_updated` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;"
    )

    """ --------------------------
        Pages
    -------------------------- """

    # Add created table
    queries.append(
        "ALTER TABLE `pages` ADD `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;"
    )
    queries.append("UPDATE `pages` SET `created`=`upd` WHERE `id`=`id`")

    # rename
    from_col = ["bookid", "pageno", "upd"]
    to_col = ["book_id", "page_number", "last_updated"]
    types_col = [
        "INT(10) UNSIGNED NULL DEFAULT NULL",
        "SMALLINT(6) NULL DEFAULT NULL",
        "TIMESTAMP(3) on update CURRENT_TIMESTAMP(3) NOT NULL DEFAULT  CURRENT_TIMESTAMP(3)",
    ]
    for fr, to, ty in zip(from_col, to_col, types_col):
        queries.append(f"ALTER TABLE pages CHANGE {fr} {to} {ty}")

    # drop ro column
    queries.append(f"ALTER TABLE pages DROP COLUMN ro")

    """ --------------------------
        Views
    -------------------------- """

    # rename
    from_col = ["upd", "snipid", "pageid"]
    to_col = ["last_updated", "snip_id", "page_id"]
    types_col = [
        "TIMESTAMP(3) on update CURRENT_TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3)",
        "INT(10) UNSIGNED NOT NULL",
        "SMALLINT(6) NULL DEFAULT NULL",
    ]
    for fr, to, ty in zip(from_col, to_col, types_col):
        queries.append(f"ALTER TABLE views CHANGE {fr} {to} {ty}")

    """ --------------------------
        Snips
    -------------------------- """
    # Add created table
    queries.append(
        "ALTER TABLE `snips` ADD `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;"
    )
    queries.append("UPDATE `snips` SET `created`=`upd` WHERE `id`=`id`")

    # rename
    from_col = ["upd", "bookid", "json"]
    to_col = ["last_updated", "book_id", "data_json"]
    types_col = [
        "TIMESTAMP(3) on update CURRENT_TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3)",
        "INT(10) UNSIGNED NOT NULL",
        "MEDIUMTEXT CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL",
    ]
    for fr, to, ty in zip(from_col, to_col, types_col):
        queries.append(f"ALTER TABLE snips CHANGE {fr} {to} {ty}")

    # add view_json col in snips table
    queries.append(
        f"ALTER TABLE `snips` ADD `view_json` MEDIUMTEXT CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL"
    )

    # set view_json col in snips table to json col in view table
    queries.append(
        f"""
    UPDATE
        snips
    INNER JOIN views ON snips.id = views.snip_id
    SET
        snips.view_json = views.json
    """
    )

    # add page_id col
    queries.append(f"ALTER TABLE `snips` ADD `page_id` SMALLINT(6) NULL DEFAULT NULL")

    # Copy page_id from views to snips
    queries.append(
        f"""
    UPDATE
        snips
    INNER JOIN views ON snips.id = views.snip_id
    SET
        snips.page_id = views.page_id
    """
    )

    # merge dates from views into snips taking the newest date
    queries.append(
        f"""
    UPDATE
        snips
    INNER JOIN views ON snips.id = views.snip_id
    SET
        snips.last_updated = IF(snips.last_updated > views.last_updated, snips.last_updated, views.last_updated)
    """
    )

    # drop views table
    queries.append(f"DROP TABLE views")

    """ --------------------------
        Users table
    -------------------------- """

    # create
    queries.append(
        f"""
    CREATE TABLE `snip`.`users` ( `id` INT NOT NULL AUTO_INCREMENT , `email` TEXT NOT NULL , `hashedPassword` TEXT NOT NULL , `emailValidated` BOOL DEFAULT 0, `group_ids` JSON NULL PRIMARY KEY (`id`)) ENGINE = InnoDB; 
    """
    )

    """ --------------------------
        Doodle table merge into snips table
    -------------------------- """

    # rename udp to last_updated
    queries.append(
        f"ALTER TABLE `doodles` CHANGE `upd` `last_updated` TIMESTAMP(3) on update CURRENT_TIMESTAMP(3) NOT NULL DEFAULT  CURRENT_TIMESTAMP(3)"
    )

    # rename pageid to page_id
    queries.append(
        f"ALTER TABLE `doodles` CHANGE `pageid` `page_id` SMALLINT(6) NULL DEFAULT NULL;"
    )

    for sql in queries:
        # Try query on error print error
        try:
            cursor = mydb.cursor(raw=True)
            cursor.execute(sql)
            cursor.close()
        except mysql.connector.Error as err:
            print("Error: {}".format(err))

    """ More complex operations
    """

    # Move doodles table into snips table
    try:
        doodles_to_snips()
    except mysql.connector.Error as err:
        print("Error: {}".format(err))

    # Extract the type of the snip from the json data
    # and put into a column in the snips table
    try:
        extract_types_from_snips_data_json()
    except mysql.connector.Error as err:
        print("Error: {}".format(err))

    # Unfold array snips
    try:
        unfold_array_snips()
    except mysql.connector.Error as err:
        print("Error: {}".format(err))

    # Flatten view json
    try:
        flatten_view_json()
    except mysql.connector.Error as err:
        print("Error: {}".format(err))
    try:
        create_trigger()
    except mysql.connector.Error as err:
        print("Error: {}".format(err))

    try:
        some_strange_exceptions()
    except mysql.connector.Error as err:
        print("Error: {}".format(err))

    """ Not working
    adding legacy into data_json
    try:
        #adjust_x_and_y_array()
    except mysql.connector.Error as err:
        print("Error: {}".format(err))
    """
