-- Simple initial setup

CREATE DATABASE IF NOT EXISTS snip_data;
CREATE DATABASE IF NOT EXISTS snip_perms;
CREATE DATABASE IF NOT EXISTS snip_dbinfo;



-- Create user
CREATE USER IF NOT EXISTS 'snip'@'%' IDENTIFIED BY 'snip';

-- Grant permissions to the snip user

GRANT ALL PRIVILEGES ON snip_data.* TO 'snip'@'%';
GRANT ALL PRIVILEGES ON snip_perms.* TO 'snip'@'%';
GRANT ALL PRIVILEGES ON snip_dbinfo.* TO 'snip'@'%';
FLUSH PRIVILEGES;

-- CREATE Tables for changelog and versioning
CREATE TABLE IF NOT EXISTS snip_dbinfo.changelog (
    `version_number` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `applied_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `applied_by` VARCHAR(32) NOT NULL,
    `description` VARCHAR(256) NOT NULL
) ENGINE=InnoDB;