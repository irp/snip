# Create backup as tar file
docker run --rm \
    -v snip_database:/backup-volume \
    -v $(pwd)/backup:/backup \
    ubuntu \
    tar -zcvf /backup/db_backup.tar.gz /backup-volume