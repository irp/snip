docker stop mariadb && \
docker run --rm \
    -v snip_database:/db \
    -v $(pwd)/backup:/backup \
    ubuntu \
    bash -c "cd /db && rm -rf ./* && tar -xvf /backup/db_backup.tar.gz --strip 1" && \
docker start mariadb