#!/usr/bin/env python3

import logging
import os
import re
import sys
from datetime import datetime
from time import sleep

import click
import mysql.connector
import sqlparse
from dotenv import load_dotenv

logging.basicConfig(level=logging.INFO, format="%(name)s - %(levelname)s - %(message)s")
logger = logging.getLogger("DB Migration")

FILE_DIR = os.path.dirname(os.path.realpath(__file__))


@click.command()
@click.option("--host", default="localhost", help="Database host. Default: localhost")
@click.option("--port", default=3306, help="Database port. Default: 3306")
@click.option("--user", default="root", help="Database user. Default: root")
@click.option(
    "--password",
    default=None,
    help="Database password. Defaults to the value defined in the ../.env file MYSQL_ROOT_PASSWORD",
)
def migrate(host, port, user, password):
    """
    Migrates the database to the latest version.

    How it works:

    The database store its changelog in a table `snip_dbinfo.changelog`. Each change
    applied to the database is represented by a row in this table.

    This script reads all sql files in the `migration` folder and executes them chronological. After
    each file is executed, the script adds a new row to the changelog table. The changelog table
    is used to determine which files have already been executed.

    """
    logger.warn(
        "Python migration script is deprecated, use the node migration script instead (see packages/scripts)"
    )
    load_dotenv("../.env")

    if password is None:
        password = os.getenv("MYSQL_ROOT_PASSWORD")

    # Get db connection
    db = mysql.connector.connect(
        host=host,
        user=user,
        port=port,
        password=password,
    )
    if db.is_connected():
        db_info = db.get_server_info()
        logger.info(f"Connected to MariaDB Server version {db_info}")
    else:
        logger.error("Could not connect to database")
        sys.exit(1)
    logger.info(f"Connected on {host}:{port}")

    # Get current version
    current_version = get_current_version(db)
    logger.info(f"Current database version: {current_version}")

    # Get all migration files and sort them by id
    migration_files = os.listdir(f"{FILE_DIR}/migration")
    migration_files.sort()

    # Execute all migration files older than the current version
    for file in migration_files:
        execute_file(db, file, int(current_version))
        sleep(0.1)

    new_version = get_current_version(db)

    logger.info(f"Migration up to {new_version} complete!")
    db.close()


def execute_file(db, file, current_version):
    """Run a migration sql file."""
    # Check if file matches the naming convention xxxx_name.sql
    if not file.split("_")[0].isdigit():
        logger.warning(
            f"Skipping file {file} because it does not match the naming convention"
        )
        return

    # Check if file is a sql file
    if not file.endswith(".sql"):
        logger.warning(f"Skipping file {file} because it is not a sql file")
        return

    # Get the version number from the filename and skip if the file is newer than the current version
    version_number = int(file.split("_")[0])

    if version_number <= current_version:
        logger.debug(
            f"Skipping file {file} because the server is already at version {current_version}"
        )
        return

    """ We execute the file here, first we parse it and encase it in an transaction such that
    if any of the statements fail, the whole transaction is rolled back and logged!
    """
    cursor = db.cursor()
    start_time = datetime.now()

    try:
        # Execute the file
        f = open(f"{FILE_DIR}/migration/{file}", "r")
        sql = f.read()
        parsed_sql = sqlparse.split(sql)
        for statement in parsed_sql:
            # Find special delimiters this is hacky
            delimiters = re.compile("DELIMITER *(\S*)", re.I)  # type: ignore
            result = delimiters.split(statement)
            result.insert(0, ";")
            delimiter = result[0::2]
            section = result[1::2]
            # Split queries on delimiters and execute
            for i in range(len(delimiter)):
                queries = section[i].split(delimiter[i])
                for query in queries:
                    if not query.strip():
                        continue
                    logger.debug(f"Executing query: {query}")
                    cursor.execute(query, multi=True)
        f.close()
        db.commit()
    except Exception as e:
        logger.error(f"Could not execute file {file}: {e}")
        logger.error("Rolling back transaction")
        db.rollback()
        raise e

    runtime = datetime.now() - start_time

    # Success
    logger.info(f"Executed file {file} in {runtime.total_seconds()}s")

    # Add a row to the changelog table
    cursor.execute(
        "INSERT INTO snip_dbinfo.changelog (version_number, applied_at, applied_by, description) VALUES (%s, %s, %s, %s)",
        (version_number, datetime.now(), "migration_script", file),
    )
    db.commit()
    cursor.close()


def get_current_version(db):
    """Get the current database version.

    First checks if the database has a changelog table.
    """
    cursor = db.cursor()

    # Check if the changelog table exists
    cursor.execute(
        """
        SELECT 
            TABLE_NAME
        FROM 
            information_schema.TABLES 
        WHERE 
            TABLE_SCHEMA LIKE 'snip_dbinfo' AND 
                TABLE_TYPE LIKE 'BASE TABLE' AND
                TABLE_NAME = 'changelog';
        """
    )
    table = cursor.fetchone()
    if table is None or "changelog" not in table:
        change_last = {
            "version_number": "0",
            "applied_at": datetime.now(),
            "applied_by": "unknown",
            "description": "No changelog table found",
        }
    else:
        """Get the last change
        `version_number` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        `applied_at` DATETIME NOT NULL,
        `applied_by` VARCHAR(32) NOT NULL,
        `description` VARCHAR(256) NOT NULL,
        """
        cursor = db.cursor(dictionary=True)
        cursor.execute(
            "SELECT * FROM snip_dbinfo.changelog ORDER BY version_number DESC LIMIT 1"
        )
        change_last = cursor.fetchone()
        logger.debug(f"Last change: {change_last}")
        cursor.nextset()

    cursor.close()
    if change_last is None:
        return 0
    return change_last["version_number"]


if __name__ == "__main__":
    migrate()
    exit(0)
