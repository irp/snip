#!/bin/bash

# This has to run on the same machine as the mariaDB server!
ARGS=$(getopt -a --options u:p: --long "user:,password:,help,full" -- "$@")

eval set -- "$ARGS"

# Get args using shift
while true; do
    case "$1" in
        -u|--user)
            user="$2"
            echo "user: $user"
        shift 2;;
        -p|--password)
            password="$2"
            echo "Password: set"
        shift 2;;
        --full)
            full=true
        shift;;
        --help)
            echo "Usage: $0 [--user <user>] [--password <password>]"
        exit 0;;
        --)
        break;;
    esac
done


FOLDER_LOCAL="/tmp/backup/"
FOLDER_DOCKER="/backup/"

# CASE 1: Perform full backup
full_backup () {
    foldername=$(date +"%Y-%m-%d_%H-%M_full")
    echo "Performing full backup to '${FOLDER_LOCAL}${foldername}'"
    mkdir -p "${FOLDER_LOCAL}${foldername}"
    docker exec mariadb mariabackup --backup \
    --user=$user \
    --password=$password \
    --target-dir="${FOLDER_DOCKER}${foldername}"
    
    status=$?
    if [ $status -eq 0 ]; then
        docker exec -u root mariadb chmod -R 755 "${FOLDER_DOCKER}${foldername}"
        exit 0
    else
        exit 1
    fi
}

# CASE 2: Perform incremental backup
incremental_backup () {
    foldername=$(date +"%Y-%m-%d_%H-%M_incr")
    
    # Iterate all folders by date
    for folder in $(ls -dr ${FOLDER_LOCAL}*/); do
        incr_base_dir=$(basename "$folder")
        # Check if folder contains "xtrabackup_checkpoints" file
        if [ -f ${FOLDER_LOCAL}${incr_base_dir}/xtrabackup_checkpoints ]; then
            echo "Performing incremental backup to '${FOLDER_LOCAL}${foldername}'"
            echo "Using base dir '${FOLDER_LOCAL}${incr_base_dir}'"
            mkdir -p "${FOLDER_LOCAL}${foldername}"
            docker exec mariadb mariabackup --backup \
            --user=$user \
            --password=$password \
            --target-dir="${FOLDER_DOCKER}$foldername"\
            --incremental-basedir="${FOLDER_DOCKER}/${incr_base_dir}"
            
            status=$?
            if [ $status -eq 0 ]; then
                docker exec -u root mariadb chmod -R 755 "${FOLDER_DOCKER}${foldername}"
                exit 0
            else
                exit 1
            fi
        fi
    done
    echo "Error: No incremental backup found!"
    exit 1
}

# Check if all args are set
if [ -z "$user" ] || [ -z "$password" ]; then
    echo "Please set all arguments"
    # Print usage
    echo "Usage: $0 [--user <user>] [--password <password>]"
    exit 1
fi

# Perform backup
if [ "$full" = true ]; then
    full_backup
else
    incremental_backup
fi
