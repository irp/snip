USE snip_perms;

-- Add the user configs table (api or ui)
ALTER TABLE tokens ADD COLUMN type VARCHAR(10) NOT NULL DEFAULT 'api' COMMENT 'The type of token (api or ui)';
-- Add token column to allow for plain text tokens (only ui)
ALTER TABLE tokens ADD COLUMN token TEXT NULL DEFAULT NULL COMMENT 'The token (only for ui tokens)';
-- Add expires_at column to allow for token expiration (only ui( for now))
ALTER TABLE tokens ADD COLUMN expires_at TIMESTAMP NULL DEFAULT NULL COMMENT 'The time the token expires (only for ui tokens)';