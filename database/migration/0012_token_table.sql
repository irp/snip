USE snip_perms;
CREATE TABLE IF NOT EXISTS tokens (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `hashed_token` TEXT NOT NULL,
    `book_id` INT NOT NULL,
    FOREIGN KEY (book_id) REFERENCES snip_data.books(id),
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT NOT NULL,
    FOREIGN KEY (created_by) REFERENCES users(id),
    `description` TEXT NULL DEFAULT NULL
);
