USE snip_perms;


-- add roles column to many-to-many table
ALTER TABLE users_groups 
    ADD COLUMN role ENUM('owner','moderator','member','invited') NOT NULL COMMENT 'The role of the user in the group' DEFAULT 'invited' AFTER group_id;

-- add joined_at column to many-to-many table
ALTER TABLE users_groups 
    ADD COLUMN joined_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The time the user joined the group if role is invited is the date the user was invited' AFTER role;


-- set all current users to member
UPDATE users_groups SET role = 'member';

-- add view to get members with email
CREATE OR REPLACE VIEW users_groups_with_email AS
SELECT 
    users_groups.user_id,
    users_groups.group_id,
    users_groups.role,
    users_groups.joined_at,
    users.email
FROM users_groups
JOIN users ON users.id = users_groups.user_id;

SET GLOBAL time_zone = 'Europe/Berlin';