/* Move width and height to view and rename to use mdn format */
USE snip_data;
UPDATE snips
SET view_json = JSON_SET(view_json, '$.width', JSON_EXTRACT(data_json, '$.width'))
WHERE view_json IS NOT NULL AND NOT JSON_CONTAINS(view_json, JSON_EXTRACT(data_json, '$.width')) AND type = "image";
UPDATE snips
SET view_json = JSON_SET(view_json, '$.height', JSON_EXTRACT(data_json, '$.height'))
WHERE view_json IS NOT NULL AND NOT JSON_CONTAINS(view_json, JSON_EXTRACT(data_json, '$.height')) AND type = "image";

UPDATE snips
SET view_json = JSON_INSERT(
                           JSON_REMOVE(view_json, '$.rx'), 
                           '$.sx', 
                           JSON_EXTRACT(view_json, '$.rx')
                          )
WHERE view_json IS NOT NULL AND NOT JSON_CONTAINS(view_json, JSON_EXTRACT(view_json, '$.rx')) AND type = "image";
UPDATE snips
SET view_json= JSON_INSERT(
                           JSON_REMOVE(view_json, '$.ry'), 
                           '$.sy', 
                           JSON_EXTRACT(view_json, '$.ry')
                          )
WHERE view_json IS NOT NULL AND NOT JSON_CONTAINS(view_json, JSON_EXTRACT(view_json, '$.ry')) AND type = "image";
UPDATE snips
SET view_json= JSON_INSERT(
                           JSON_REMOVE(view_json, '$.rw'), 
                           '$.swidth', 
                           JSON_EXTRACT(view_json, '$.rw')
                          )
WHERE view_json IS NOT NULL AND NOT JSON_CONTAINS(view_json, JSON_EXTRACT(view_json, '$.rw')) AND type = "image";
UPDATE snips
SET view_json= JSON_INSERT(
                           JSON_REMOVE(view_json, '$.rh'), 
                           '$.sheight', 
                           JSON_EXTRACT(view_json, '$.rh')
                          )
WHERE view_json IS NOT NULL AND NOT JSON_CONTAINS(view_json, JSON_EXTRACT(view_json, '$.rh')) AND type = "image";

/* Delete width and height from data_json */
UPDATE snips
SET data_json = JSON_REMOVE(JSON_REMOVE(data_json, '$.width'), '$.height')
WHERE data_json IS NOT NULL AND type = "image";