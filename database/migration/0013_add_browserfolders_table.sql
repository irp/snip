-- Add browser folder table
USE snip_data;

CREATE TABLE browserFolders (
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id INTEGER NOT NULL COMMENT 'The user that owns this folder',

    name TEXT NOT NULL,
    comment TEXT NULL DEFAULT NULL,
    created DATETIME NOT NULL DEFAULT current_timestamp(),

    parent_id INTEGER NULL COMMENT 'The parent folder of this folder, if null then this is in the users root folder',
    FOREIGN KEY (parent_id) REFERENCES browserFolders(id),
    UNIQUE KEY browserFolders_user_name (user_id, name),
    UNIQUE KEY browserFolders_user_id (user_id, id),
    INDEX browserFolders_user_id_idx (user_id)
);


CREATE TABLE browserFolders_books (
    user_id INTEGER NOT NULL,
    browserFolder_id INTEGER NOT NULL,
    book_id INTEGER NOT NULL,
    PRIMARY KEY (user_id, browserFolder_id, book_id),
    FOREIGN KEY (user_id, browserFolder_id) REFERENCES browserFolders(user_id, id),
    FOREIGN KEY (book_id) REFERENCES books(id)
) COMMENT 'The folders hold these books';


-- Create trigger to check if the user exists in the user table 
-- This has to be done because MySQL does not support foreign keys 
-- to be check multiple databases.
DELIMITER $$
CREATE TRIGGER check_user_exists_i AFTER INSERT ON browserFolders
FOR EACH ROW
BEGIN
    IF NOT EXISTS (SELECT 1 FROM snip_perms.users WHERE id = NEW.user_id) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'User does not exist';
    END IF;
END
$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER check_user_exists_u AFTER UPDATE ON browserFolders
FOR EACH ROW
BEGIN
    IF NOT EXISTS (SELECT 1 FROM snip_perms.users WHERE id = NEW.user_id) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'User does not exist';
    END IF;
END
$$
DELIMITER ;


-- Create a function to get the number of pages in a book
DELIMITER $$
CREATE FUNCTION getNumPages(id INT) RETURNS INT
BEGIN
    DECLARE numPages INT;
    SELECT COUNT(*) INTO numPages FROM pages WHERE book_id = id;
    RETURN numPages;
END
$$
DELIMITER ;


-- Not sure if this is needed
CREATE VIEW browserView AS
SELECT
    BF.user_id,
    CONCAT('book_', CAST(B.id AS CHAR)) as id,
    B.title as name,
    B.comment,

    false as isDir,
    CASE
        WHEN BF.browserFolder_id IS NOT NULL THEN CONCAT('folder_', CAST(BF.browserFolder_id AS CHAR))
        ELSE NULL
    END AS parentId,

    -- book specific --
    getNumPages(B.id) as numPages,
    B.created,
    B.last_updated,
    B.finished,
    B.owner_name as `owner`
FROM books_resolved as B
LEFT JOIN browserFolders_books as BF ON BF.book_id = B.id
UNION
SELECT
    F.user_id,
    CONCAT('folder_', CAST(F.id AS CHAR)) as id,
    F.name,
    F.comment,

    true as isDir,
    F.parent_id as parentId,
    
    -- book specific --
    NULL as numPages,
    F.created,
    NULL as last_updated,
    NULL as finished,
    NULL as `owner`
FROM browserFolders as F