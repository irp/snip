USE snip_perms;

-- Add the user configs table
CREATE TABLE user_config (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id),

    -- text editor
    text_font_id INT NOT NULL DEFAULT 1,
    FOREIGN KEY (text_font_id) REFERENCES fonts(id),
    text_fontSize DOUBLE NOT NULL DEFAULT 12 COMMENT 'The size of the text',
    text_fontColor VARCHAR(7) NOT NULL DEFAULT '#000000' COMMENT 'The color of the text',
    text_lineHeight DOUBLE NOT NULL DEFAULT 1.25 COMMENT 'The line height of the text',
    text_lineWrap DOUBLE NOT NULL DEFAULT 400 COMMENT 'The line wrap of the text editor (in pixels)',

    -- pen tool
    pen_color VARCHAR(7) NOT NULL DEFAULT '#000000' COMMENT 'The color of the pen',
    pen_size DOUBLE NOT NULL DEFAULT 4 COMMENT 'The size of the pen',
    pen_smoothing DOUBLE NOT NULL DEFAULT 0 COMMENT 'The smoothness of the pen',

    -- zoom
    zoom1 DOUBLE NOT NULL DEFAULT 1 COMMENT 'The zoom level for the first page',
    zoom2 DOUBLE NOT NULL DEFAULT 1 COMMENT 'The zoom level for the second page',
    
    -- utils
    last_updated timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) COMMENT 'The user configs for the app';

-- Create a view to resolve font names
CREATE OR REPLACE VIEW user_config_resolved AS
SELECT 
    UC.id,
    UC.user_id,
    UC.text_font_id,
    F.name AS text_font,
    UC.text_fontSize,
    UC.text_fontColor,
    UC.text_lineHeight,
    UC.text_lineWrap,
    UC.pen_color,
    UC.pen_size,
    UC.pen_smoothing,
    UC.zoom1,
    UC.zoom2,
    UC.last_updated
FROM snip_perms.user_config AS UC
LEFT JOIN snip_perms.fonts AS F ON UC.text_font_id = F.id;


-- select all users and add a config for each
INSERT INTO user_config (user_id) SELECT id FROM users;
