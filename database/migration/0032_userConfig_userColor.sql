USE snip_perms;



ALTER TABLE user_config
ADD COLUMN user_color VARCHAR(7) COMMENT 'The preferred color used to show the user. If NULL a random value should be generated.' NULL AFTER user_id;


-- Recreate a view to resolve font names
CREATE OR REPLACE VIEW user_config_resolved AS
SELECT 
    UC.id,
    UC.user_id,
    UC.user_color,
    UC.text_font_id,
    F.name AS text_font,
    UC.text_fontSize,
    UC.text_fontColor,
    UC.text_lineHeight,
    UC.text_lineWrap,
    UC.pen_color,
    UC.pen_colors,
    UC.pen_size,
    UC.pen_smoothing,
    UC.zoom1,
    UC.zoom2,
    UC.last_updated
FROM snip_perms.user_config AS UC
LEFT JOIN snip_perms.fonts AS F ON UC.text_font_id = F.id;
