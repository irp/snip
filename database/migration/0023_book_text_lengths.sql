USE snip_data;


-- allow 256 characters for the title
ALTER TABLE books MODIFY COLUMN title VARCHAR(256) NOT NULL COMMENT 'The title of the book';

-- Add cover_page_id to the view
ALTER VIEW books_resolved AS
SELECT 
    B.id,
    B.title,
    B.comment,
    B.created,
    B.finished,
    B.last_updated,
    -- resolve the owner
    CASE
        WHEN B.owner_user_id IS NULL THEN G.id
        ELSE U.id
    END AS owner_id,
    CASE
        WHEN B.owner_user_id IS NULL THEN G.name
        ELSE U.email
    END AS owner_name,
    CASE
        WHEN B.owner_user_id IS NULL THEN "group"
        ELSE "user"
    END AS owner_type,
    -- resolve the background type
    B.default_background_type_id,
    BG.name AS background_type_name,
    BG.description AS background_type_description,
    -- add the number of pages
    count(P.id) as num_pages,
    CASE
        WHEN B.cover_page_id IS NULL THEN (SELECT MIN(id) FROM snip_data.pages WHERE book_id = B.id)
        ELSE B.cover_page_id
    END AS cover_page_id
FROM snip_data.books as B 
LEFT JOIN snip_perms.users as U ON owner_user_id = U.id
LEFT JOIN snip_perms.groups as G ON owner_group_id = G.id
LEFT JOIN snip_data.background_types as BG ON B.default_background_type_id = BG.id
LEFT JOIN snip_data.pages as P ON B.id = P.book_id GROUP BY B.id;
