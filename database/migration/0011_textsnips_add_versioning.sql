/** Add version key into textsnips data json */
USE snip_data;
UPDATE snips
SET data_json = JSON_SET(data_json, '$.version', 1)
WHERE data_json IS NOT NULL
    AND type = "text";
UPDATE snips
SET data_json = JSON_SET(data_json, '$.version', 1)
WHERE data_json IS NOT NULL
    AND type = "uprp/spec/ginix/attct";
UPDATE snips
SET data_json = JSON_SET(data_json, '$.version', 1)
WHERE data_json IS NOT NULL
    AND type = "uprp/spec/logfile";
UPDATE snips
SET data_json = JSON_SET(data_json, '$.version', 1)
WHERE data_json IS NOT NULL
    AND type = "uprp/spec/macro.spec";
UPDATE snips
SET data_json = JSON_SET(data_json, '$.version', 1)
WHERE data_json IS NOT NULL
    AND type = "uprp/spec/timestamp";