-- Add a view for the user table which joins the user_config table and the groups
USE snip_perms;

ALTER TABLE users
ADD COLUMN last_login TIMESTAMP COMMENT 'The last time the user logged in' DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN last_login_ip VARCHAR(45) COMMENT 'The last IP address the user logged in from (ipv4 or ipv6)';