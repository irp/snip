USE snip_data;

ALTER TABLE pages
ADD COLUMN referenced_page_id INT DEFAULT NULL COMMENT 'The page that this page references.',
ADD CONSTRAINT fk_referenced_page
    FOREIGN KEY (referenced_page_id) REFERENCES pages(id);



-- Alter pageView to include referenced page
CREATE OR REPLACE VIEW pagesView AS
SELECT P.id,
    P.book_id,
    P.page_number,
    P.created,
    P.last_updated,
    -- resolve the background type
    CASE
        WHEN P.background_type_id IS NULL THEN B.default_background_type_id
        ELSE P.background_type_id
    END AS background_type_id,
    P.referenced_page_id,
    CASE
        WHEN P.referenced_page_id IS NULL THEN NULL
        ELSE RP.book_id
    END AS referenced_book_id
FROM snip_data.pages as P
    LEFT JOIN snip_data.books as B ON P.book_id = B.id
    LEFT JOIN snip_data.pages as RP ON P.referenced_page_id = RP.id;


