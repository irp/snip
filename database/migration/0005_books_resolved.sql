USE snip_data;
-- View to resolve the book owner on a book
CREATE OR REPLACE VIEW books_resolved AS
SELECT 
    B.id,
    B.title,
    B.comment,
    B.created,
    B.finished,
    B.last_updated,
    -- resolve the owner
    CASE
        WHEN B.owner_user_id IS NULL THEN G.id
        ELSE U.id
    END AS owner_id,
    CASE
        WHEN B.owner_user_id IS NULL THEN G.name
        ELSE U.email
    END AS owner_name,
    CASE
        WHEN B.owner_user_id IS NULL THEN "group"
        ELSE "user"
    END AS owner_type,
    -- resolve the background type
    B.background_type_id,
    BG.name AS background_type_name,
    BG.description AS background_type_description
FROM snip_data.books as B 
LEFT JOIN snip_perms.users as U ON owner_user_id = U.id
LEFT JOIN snip_perms.groups as G ON owner_group_id = G.id
LEFT JOIN snip_data.background_types as BG ON B.background_type_id = BG.id;