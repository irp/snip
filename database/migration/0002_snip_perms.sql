-- CREATES SNIP PERMS DATABASE
CREATE Database IF NOT EXISTS snip_perms;
USE snip_perms;

-- Create table to store user data
CREATE Table users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    -- login
    email text NOT NULL,
    hashedPassword text NOT NULL,
    -- permission system
    emailValidated boolean NOT NULL DEFAULT 0,
    -- utils
    created datetime NOT NULL DEFAULT current_timestamp(),
    last_updated timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Create table to store group data
CREATE Table groups (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name text NOT NULL,
    description text NULL,
    -- utils
    created datetime NOT NULL DEFAULT current_timestamp(),
    last_updated timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into groups (name) 
values 
    ('admin');

-- many to many relationship of users and groups i.e. junction table
CREATE Table users_groups (
    `user_id` INT NOT NULL,
    group_id INT NOT NULL,
    PRIMARY KEY (`user_id`, group_id),
    FOREIGN KEY (`user_id`) REFERENCES users(id),
    FOREIGN KEY (group_id) REFERENCES groups(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- Create table for entity types
CREATE TABLE entity_types (
    id INT PRIMARY KEY,
    entity_type varchar(20) not NULL UNIQUE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into entity_types (id, entity_type)
values
    (1, 'user'),
    (2, 'group');

-- Create table for resource types
CREATE TABLE resource_types (
    id int PRIMARY KEY,
    resource_type varchar(20) not NULL UNIQUE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into resource_types (id, resource_type)
values
    (1,'book'),
    (2,'instrument');

-- Create table for access control list
Create Table acl (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -- entity
    entity_id INT NOT NULL,
    entity_type INT NOT NULL DEFAULT 1,
    -- resource
    resource_id INT NOT NULL,
    resource_type INT NOT NULL DEFAULT 1,
    UNIQUE KEY entity_resource (entity_id, entity_type, resource_id, resource_type),
    -- permission(s)
    pRead boolean NOT NULL DEFAULT 0,
    pWrite boolean NOT NULL DEFAULT 0,
    pDelete boolean NOT NULL DEFAULT 0,
    pACL boolean NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Add constraint on types i.e. normalized enum and indexing
ALTER TABLE acl
    ADD FOREIGN KEY (entity_type) REFERENCES entity_types(id),
    ADD FOREIGN KEY (resource_type) REFERENCES resource_types(id);