-- Add unique constraint to email column
USE snip_perms;

ALTER TABLE user_config
ADD UNIQUE (user_id);
