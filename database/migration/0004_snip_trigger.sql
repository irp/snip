USE snip_data;
-- Trigger to normalize page number (always sets to new max)
-- (todo check on deletion)
DELIMITER $$
CREATE TRIGGER `ins_page_number` BEFORE INSERT ON `pages` FOR EACH ROW BEGIN
    IF NEW.page_number IS NULL THEN
	    SET NEW.page_number = (
        SELECT IFNULL(MAX(page_number),0) +1
        FROM pages
        WHERE book_id = NEW.book_id
      );
    END IF;
END
$$
DELIMITER ;

-- Create triggers for book>pages>snips hierachy
-- for insert and update
DELIMITER $$
CREATE TRIGGER `ins_last_updated_books` AFTER INSERT ON `pages` FOR EACH ROW UPDATE books
            SET books.last_updated = CURRENT_TIMESTAMP()
            WHERE books.id = NEW.book_id
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ins_last_updated_pages` AFTER INSERT ON `snips` FOR EACH ROW UPDATE pages
            SET pages.last_updated = current_timestamp()
            WHERE pages.id = NEW.page_id
$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER `upd_last_updated_books` AFTER UPDATE ON `pages` FOR EACH ROW UPDATE books
            SET books.last_updated = CURRENT_TIMESTAMP()
            WHERE books.id = NEW.book_id
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `upd_last_updated_pages` AFTER UPDATE ON `snips` FOR EACH ROW UPDATE pages
            SET pages.last_updated = current_timestamp() WHERE pages.id = NEW.page_id
$$
DELIMITER ;


-- Create trigger to add access rights for admins on new book create
-- e_type 2 --> group
-- r_type 1 --> book
DELIMITER $$
CREATE TRIGGER `ins_book_acl_add_admin` AFTER INSERT ON `books` FOR EACH ROW BEGIN
    INSERT INTO snip_perms.acl (entity_type, entity_id, resource_type, resource_id, pRead, pWrite, pDelete, pACL)
    VALUES (2, 1, 1, NEW.id, 1, 1, 1, 1);
END $$
DELIMITER ;

-- Trigger to update the permission on owner on books.owner_user_id change
DELIMITER $$
CREATE TRIGGER `ins_book_acl_update_owner` AFTER INSERT ON `books` FOR EACH ROW BEGIN
    INSERT INTO  snip_perms.acl (entity_type, entity_id, resource_type, resource_id, pRead, pWrite, pDelete, pACL)
    VALUES (IF(NEW.owner_user_id IS NULL,2,1), IFNULL(NEW.owner_group_id,NEW.owner_user_id), 1, NEW.id, 1, 1, 1, 1);
END $$
DELIMITER ;


-- Trigger to update the permission on owner on books.owner_user_id change
DELIMITER $$
CREATE TRIGGER `upd_book_acl_update_owner` AFTER UPDATE ON `books` FOR EACH ROW BEGIN
    REPLACE INTO  snip_perms.acl (entity_type, entity_id, resource_type, resource_id, pRead, pWrite, pDelete, pACL)
    VALUES (IF(NEW.owner_user_id IS NULL,2,1), IFNULL(NEW.owner_group_id,NEW.owner_user_id), 1, NEW.id, 1, 1, 1, 1);
END $$
DELIMITER ;