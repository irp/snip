USE snip_perms;

-- Add tokens to entity types
-- Add the description to the entity types
ALTER TABLE entity_types ADD COLUMN description VARCHAR(1024) NOT NULL;
UPDATE entity_types SET `description` = 'A user registered in the system, see users table for reference' WHERE entity_type = 'user';
UPDATE entity_types SET `description` = 'A group created within the system, see groups table for reference' WHERE entity_type = 'group';

-- create a view for the tokens with user joined
CREATE OR REPLACE VIEW tokens_users_resolved AS
SELECT 
    T.id,
    T.hashed_token,
    T.book_id,
    T.created_at,
    T.created_by,
    U.email AS created_by_email,
    T.description,
    T.type,
    T.token,
    T.expires_at
FROM tokens AS T
LEFT JOIN users AS U ON T.created_by = U.id;


-- remove token column from only use hashed_token for now 
ALTER TABLE tokens DROP COLUMN token;

-- Add comments to the hashed_token column
ALTER TABLE tokens MODIFY COLUMN hashed_token VARCHAR(1024) NOT NULL COMMENT 'Hashed token for api token, raw token for ui token';