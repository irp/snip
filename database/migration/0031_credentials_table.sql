USE snip_perms;

CREATE TABLE credentials (
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT "Unique identifier for the credential",
    user_id INT(11) NOT NULL COMMENT "The id of the user the credential belongs to",
    credential_type ENUM('password', 'sso') NOT NULL COMMENT "The type of the credential",

    -- password credentials
    password_hash TEXT COMMENT "The hashed password of the user. Including salt, for 'password' credentials only.",

    -- sso credentials
    provider_id VARCHAR(255) COMMENT "Identifier for the provider, NULL for 'password' credentials",
    provider_user_id VARCHAR(512) COMMENT "The unique id of the user at the provider. NULL for 'password' credentials.",

    -- shared
    email VARCHAR(255) NOT NULL COMMENT "The email of the user.",
    email_verified BOOLEAN NOT NULL DEFAULT FALSE COMMENT "Whether the email has been verified.",

    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    FOREIGN KEY (user_id) REFERENCES users(id),
    UNIQUE (provider_id, provider_user_id)
);

ALTER TABLE users ADD COLUMN primary_credential_id INT(11) NULL COMMENT "The id of the primary credential for the user"; 
ALTER TABLE users ADD FOREIGN KEY (primary_credential_id) REFERENCES credentials(id);



-- trigger to check the uniqueness of the email and format to lowercase
DELIMITER //
CREATE TRIGGER format_email_and_check_uniqueness
BEFORE INSERT ON credentials
FOR EACH ROW
BEGIN
    -- Remove spaces from the email
    SET NEW.email = REPLACE(NEW.email, ' ', '');

    -- Convert the email to lowercase
    SET NEW.email = LOWER(NEW.email);

    Set @error_message = CONCAT('The email must be unique for password credentials "', NEW.email, '" is already used.');

    -- Check if the email is already used for a password credential
    IF EXISTS (SELECT 1 FROM credentials WHERE email = NEW.email AND credential_type = 'password') THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = @error_message, 
        MYSQL_ERRNO = 3420;
    END IF;


    -- Check that provider_id and provider_user_id are not null for sso credentials
    IF NEW.credential_type = 'sso' AND (NEW.provider_id IS NULL OR NEW.provider_user_id IS NULL) THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'provider_id and provider_user_id must not be null for "sso" credentials.', 
        MYSQL_ERRNO = 3421;
    END IF;


    -- Check that provider_id and provider_user_id are null for password credentials
    IF NEW.credential_type = 'password' AND (NEW.provider_id IS NOT NULL OR NEW.provider_user_id IS NOT NULL) THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'provider_id and provider_user_id must be null for "password" credentials.', 
        MYSQL_ERRNO = 3421;
    END IF;

END //
DELIMITER ;



-- trigger to automatically set the primary_credential_id for a user when a new credential  
-- is added an the user does not have a primary credential
DELIMITER //
CREATE TRIGGER set_primary_credential_id
AFTER INSERT ON credentials
FOR EACH ROW
BEGIN
    -- Check if the user has a primary credential
    IF (SELECT primary_credential_id FROM users WHERE id = NEW.user_id) IS NULL THEN
        -- Set the primary_credential_id to the new credential id
        UPDATE users SET primary_credential_id = NEW.id WHERE id = NEW.user_id;
    END IF;
END //

CREATE TRIGGER update_primary_credential_id
AFTER UPDATE ON credentials
FOR EACH ROW
BEGIN
    -- Check if the user has a primary credential
    IF (SELECT primary_credential_id FROM users WHERE id = NEW.user_id) IS NULL THEN
        -- Set the primary_credential_id to the new credential id
        UPDATE users SET primary_credential_id = NEW.id WHERE id = NEW.user_id;
    END IF;
END //
DELIMITER ;


-- Procedure for merging users
DELIMITER //
CREATE OR REPLACE PROCEDURE merge_users(
    IN from_user_id INT(11),
    IN to_user_id INT(11)
)
BEGIN
    -- Declare variables
    DECLARE from_user_exists INT;
    DECLARE to_user_exists INT;

    -- cursor vars
    DECLARE tab_n varchar(64);
    DECLARE col_n varchar(64);
    DECLARE tab_s varchar(64);

    DECLARE done INT DEFAULT FALSE;

    DECLARE cur1 CURSOR FOR
        SELECT table_schema, table_name, column_name
        FROM information_schema.columns
        WHERE column_name IN ('user_id', 'created_by', 'owner_user_id')
        AND table_schema IN ('snip_perms', 'snip_data')
        AND table_name NOT IN (
            SELECT table_name
            FROM information_schema.views
            WHERE table_schema IN ('snip_perms', 'snip_data')
        ) 
        AND table_name NOT IN ("user_config");
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
        -- Rollback the transaction on error
        ROLLBACK;
        RESIGNAL;
    END;

    -- Check if the 'to_user_id' exists in the 'users' table
    SELECT COUNT(*) INTO to_user_exists FROM users WHERE id = to_user_id;

    IF to_user_exists = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The to user does not exist.', MYSQL_ERRNO = 3420;
    END IF;

    SELECT COUNT(*) INTO from_user_exists FROM users WHERE id = from_user_id;

    IF from_user_exists = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'The from user does not exist.', MYSQL_ERRNO = 3420;
    END IF;


    -- Merge the users
    -- dynamically update all user_id and created_by fields excluding views
    -- in snip_perms and snip_data
    START TRANSACTION;
    OPEN cur1;


    read_loop: LOOP
        FETCH cur1 INTO tab_s, tab_n, col_n;
        IF done THEN
            LEAVE read_loop;
        END IF;

        -- DEBUG: SELECT concat("Running update on ", tab_s, ".", tab_n, ".", col_n) as "** Debug";
        -- Check if the table has a 'last_updated' column
        SET @has_last_updated = FALSE;
        SET @check_last_updated_sql = CONCAT('SELECT COUNT(*) INTO @has_last_updated FROM information_schema.columns WHERE table_schema = "', tab_s, '" AND table_name = "', tab_n, '" AND column_name = "last_updated"');
        PREPARE check_stmt FROM @check_last_updated_sql;
        EXECUTE check_stmt;
        DEALLOCATE PREPARE check_stmt;

        -- Construct and execute dynamic SQL
        -- if is a bit hacky to not update the last_updated column if it exists
        IF @has_last_updated THEN 
            BEGIN
                DECLARE id_lu INT;
                DECLARE last_updated_value TIMESTAMP;
                DECLARE done BOOLEAN DEFAULT FALSE;
                -- Cursor definition
                DECLARE cur2 CURSOR FOR
                    SELECT id, last_updated
                    FROM snip_perms.temp_last_updated;
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                 -- Dynamic view definition and creation
                SET @create_temp_table_sql = CONCAT(
                    'CREATE TEMPORARY TABLE snip_perms.temp_last_updated AS SELECT id, last_updated FROM ', tab_s, '.', tab_n, ' WHERE ', col_n, ' = ', from_user_id
                );
                PREPARE create_temp_stmt FROM @create_temp_table_sql;
                EXECUTE create_temp_stmt;
                DEALLOCATE PREPARE create_temp_stmt;

                 -- Open cursor
                OPEN cur2;
                
                read_loop_last_updated: LOOP
                    FETCH cur2 INTO id_lu, last_updated_value;
                    IF done THEN
                        LEAVE read_loop_last_updated;
                    END IF;

                    -- Update the 'last_updated' column with its old value
                    SET @update_last_updated_sql = CONCAT(
                        'UPDATE ', tab_s, '.', tab_n, 
                        ' SET last_updated = "', last_updated_value, '"', 
                        ", ", col_n, ' = ', to_user_id,
                        ' WHERE id = ', id_lu
                    );

                    PREPARE update_stmt FROM @update_last_updated_sql;
                    EXECUTE update_stmt;
                    DEALLOCATE PREPARE update_stmt;
                END LOOP;
                CLOSE cur2;
                -- Drop the temporary table
                DROP TEMPORARY TABLE IF EXISTS temp_last_updated;
            END;
        ELSE
            SET @sql = CONCAT('UPDATE ', tab_s, '.', tab_n, ' SET ', col_n, ' = ', to_user_id, ' WHERE ', col_n, ' = ', from_user_id);
        END IF;

        PREPARE stmt FROM @sql;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END LOOP;
    CLOSE cur1;

    
    -- UPDATE ACLs and merge on duplicate key all pREad pWrite pDelete and pACL values
    INSERT INTO snip_perms.acl (entity_id, entity_type, resource_id, resource_type, pRead, pWrite, pDelete, pACL)
        SELECT to_user_id, ac.entity_type, ac.resource_id, ac.resource_type, ac.pRead, ac.pWrite, ac.pDelete, ac.pACL
        FROM snip_perms.acl as ac
        WHERE ac.entity_id = from_user_id AND ac.entity_type = 1
    ON DUPLICATE KEY UPDATE
        entity_id = to_user_id,
        pRead = IF(VALUES(pRead) = 1 OR acl.pRead = 1, 1, 0),
        pWrite = IF(VALUES(pWrite) = 1 OR acl.pWrite = 1, 1, 0),
        pDelete = IF(VALUES(pDelete) = 1 OR acl.pDelete = 1, 1, 0),
        pACL = IF(VALUES(pACL) = 1 OR acl.pACL = 1, 1, 0);

    -- Commit transaction
    COMMIT;

    -- Delete the 'from_user_id' from the 'users' table, shouldn't fail if all foreign keys are successfully updated
    DELETE FROM user_config WHERE user_id = from_user_id;
    DELETE FROM acl WHERE entity_id = from_user_id AND entity_type = 1;
    -- DELETE FROM users WHERE id = from_user_id;
END //
DELIMITER ;


-- CREATE a last logins table
CREATE TABLE last_logins (
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT "Unique identifier for the last login",
    user_id INT(11) NOT NULL COMMENT "The id of the user",
    `time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT "The last login time of the user",
    ip VARCHAR(45) COMMENT 'The last IP address the user logged in from (ipv4 or ipv6)',
    FOREIGN KEY (user_id) REFERENCES users(id)
);

-- auto truncate the last_logins table every 60days
CREATE EVENT truncate_last_logins
ON SCHEDULE EVERY 1 DAY
DO
    DELETE FROM last_logins WHERE `time` < NOW() - INTERVAL 60 DAY;



-- migrate data from users table to credentials table
CREATE TABLE temp_users AS SELECT * FROM users;
INSERT INTO credentials (user_id, credential_type, email, email_verified, created, last_updated, password_hash) SELECT id, 'password', email, emailValidated, created, last_updated, hashedPassword FROM temp_users;
DROP TABLE temp_users;

-- Migrate data from users table to last_logins table
INSERT INTO last_logins (user_id, `time`, ip) SELECT id, last_login, last_login_ip FROM users;

-- drop columns from users table
ALTER TABLE users DROP COLUMN email;
ALTER TABLE users DROP COLUMN emailValidated;
ALTER TABLE users DROP COLUMN hashedPassword;
ALTER TABLE users DROP COLUMN last_login;
ALTER TABLE users DROP COLUMN last_login_ip;


-- CREATE a view to get the user's emails
CREATE OR REPLACE VIEW users_with_emails AS
SELECT
    u.*,
    GROUP_CONCAT(c.email ORDER BY c.id SEPARATOR ',') AS `emails`,
    GROUP_CONCAT(c.email_verified ORDER BY c.id SEPARATOR ',') AS `emails_verified`,
    GROUP_CONCAT(c.id ORDER BY c.id SEPARATOR ',') AS `credential_ids`,
    GROUP_CONCAT(c.credential_type ORDER BY c.id SEPARATOR ',') AS `credential_types`,
    GROUP_CONCAT(c.provider_id ORDER BY c.id SEPARATOR ',') AS `credential_provider_ids`
FROM
   users AS u
LEFT JOIN credentials AS c ON
    u.id = c.user_id
GROUP BY
	u.id;

-- REPLACE the groups user view
DROP VIEW IF EXISTS users_groups_with_email;
CREATE OR REPLACE VIEW users_groups_with_emails AS
SELECT
    ug.*,
    u.primary_credential_id,
    u.emails,
    u.emails_verified,
    u.credential_ids,
    u.credential_types,
    g.name AS group_name,
    g.description AS group_description
FROM
    users_groups AS ug
    LEFT JOIN users_with_emails AS u ON
        ug.user_id = u.id
    LEFT JOIN groups AS g ON
        ug.group_id = g.id
GROUP BY
    ug.user_id, ug.group_id;

CREATE OR REPLACE VIEW users_with_primary_email AS
SELECT
    u.*,
    c.email AS primary_email,
    c.email_verified AS primary_email_verified
FROM
    users AS u
LEFT JOIN credentials AS c ON
    u.primary_credential_id = c.id;


-- REPLACE collaboration invites
DROP VIEW IF EXISTS collaboration_invites_users_resolved;
CREATE OR REPLACE VIEW collaboration_invites_with_emails AS
SELECT
    ci.*,
    u1.emails AS emails,
    u1.emails_verified AS emails_verified,
    u1.credential_ids AS credential_ids,
    u1.credential_types AS credential_types,
    u1.primary_credential_id AS primary_credential_id,
    u2.emails AS invited_by_emails,
    u2.emails_verified AS invited_by_emails_verified,
    u2.credential_ids AS invited_by_credential_ids,
    u2.credential_types AS invited_by_credential_types,
    u2.primary_credential_id AS invited_by_primary_credential_id
FROM collaboration_invites AS ci
LEFT JOIN users_with_emails AS u1 ON ci.user_id = u1.id
LEFT JOIN users_with_emails AS u2 ON ci.invited_by = u2.id
GROUP BY ci.id;




-- REPLACE the books resolved view
use snip_data;
CREATE OR REPLACE VIEW books_resolved AS
SELECT 
    B.id,
    B.title,
    B.comment,
    B.created,
    B.finished,
    B.last_updated,
    B.cover_page_id,
    -- resolve the owner
    CASE
        WHEN B.owner_user_id IS NULL THEN G.id
        ELSE U.id
    END AS owner_id,
    CASE
        WHEN B.owner_user_id IS NULL THEN G.name
        ELSE U.primary_email
    END AS owner_name,
    CASE
        WHEN B.owner_user_id IS NULL THEN "group"
        ELSE "user"
    END AS owner_type,
    -- resolve the background type
    B.default_background_type_id,
    BG.name AS background_type_name,
    BG.description AS background_type_description,
    count(P.id) as num_pages
FROM snip_data.books as B 
LEFT JOIN snip_perms.users_with_primary_email as U ON owner_user_id = U.id
LEFT JOIN snip_perms.groups as G ON owner_group_id = G.id
LEFT JOIN snip_data.background_types as BG ON B.default_background_type_id = BG.id
LEFT JOIN snip_data.pages as P ON B.id = P.book_id GROUP BY B.id;
