-- Create changelog DATABASE
CREATE DATABASE IF NOT EXISTS snip_dbinfo;
-- CREATE Tables for changelog and versioning
CREATE TABLE IF NOT EXISTS snip_dbinfo.changelog (
    `version_number` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `applied_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `applied_by` VARCHAR(32) NOT NULL,
    `description` VARCHAR(256) NOT NULL
) ENGINE=InnoDB;