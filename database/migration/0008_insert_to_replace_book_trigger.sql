USE snip_data;

DROP TRIGGER IF EXISTS `ins_book_acl_update_owner`;

DELIMITER $$
CREATE TRIGGER `ins_book_acl_update_owner` AFTER INSERT ON `books` FOR EACH ROW BEGIN
    REPLACE INTO  snip_perms.acl (entity_type, entity_id, resource_type, resource_id, pRead, pWrite, pDelete, pACL)
    VALUES (IF(NEW.owner_user_id IS NULL,2,1), IFNULL(NEW.owner_group_id,NEW.owner_user_id), 1, NEW.id, 1, 1, 1, 1);
END $$
DELIMITER ;

DROP TRIGGER IF EXISTS `ins_book_acl_add_admin`;

DELIMITER $$
CREATE TRIGGER `ins_book_acl_add_admin` AFTER INSERT ON `books` FOR EACH ROW BEGIN
    REPLACE INTO snip_perms.acl (entity_type, entity_id, resource_type, resource_id, pRead, pWrite, pDelete, pACL)
    VALUES (2, 1, 1, NEW.id, 1, 1, 1, 1);
END $$
DELIMITER ;
