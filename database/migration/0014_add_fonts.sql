USE snip_perms;

-- Fonts table holds the fonts that can be used in the text editor and for snips
CREATE TABLE fonts (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(1024) NOT NULL,
    fname VARCHAR(1024) NOT NULL DEFAULT '' COMMENT 'The file name of the font (in app/public/fonts)'
) COMMENT 'The fonts that can be used in the text editor and for snips';

-- Insert the default fonts
INSERT INTO fonts (name, fname) VALUES
('Arial', 'arial.ttf'),
('PlexMono', 'IBM_Plex_Mono.ttf'),
('Caveat','caveat.ttf');

