USE snip_perms;

ALTER TABLE user_config
ADD COLUMN pen_colors JSON COMMENT 'The colors of the pens the user can use in the quick select' DEFAULT '["#000000","#FF0000", "#00FF00", "#0000FF"]' AFTER pen_color;


-- Create a view to resolve font names
CREATE OR REPLACE VIEW user_config_resolved AS
SELECT 
    UC.id,
    UC.user_id,
    UC.text_font_id,
    F.name AS text_font,
    UC.text_fontSize,
    UC.text_fontColor,
    UC.text_lineHeight,
    UC.text_lineWrap,
    UC.pen_color,
    UC.pen_colors,
    UC.pen_size,
    UC.pen_smoothing,
    UC.zoom1,
    UC.zoom2,
    UC.last_updated
FROM snip_perms.user_config AS UC
LEFT JOIN snip_perms.fonts AS F ON UC.text_font_id = F.id;
