-- Add unique constraint to email column
USE snip_perms;

-- Delete duplicates
DELETE n1 FROM users n1, users n2 WHERE n1.id > n2.id AND n1.email = n2.email;

ALTER TABLE users
ADD UNIQUE (email);
