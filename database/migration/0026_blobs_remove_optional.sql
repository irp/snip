use snip_data;
-- Remove optional fields and set default values in a single pass
DELETE FROM blobs WHERE data IS NULL;
UPDATE blobs 
SET mime = COALESCE(mime, 'text/plain'),
    size = COALESCE(size, LENGTH(data)),
    created = COALESCE(created, NOW());

-- Alter table to set columns as NOT NULL with default values
ALTER TABLE blobs
MODIFY COLUMN data mediumblob NOT NULL,
MODIFY COLUMN mime VARCHAR(32) NOT NULL,
MODIFY COLUMN size INT UNSIGNED NOT NULL,
MODIFY COLUMN created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;