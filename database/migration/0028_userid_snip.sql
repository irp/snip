use snip_data;

-- add user_id column to snips table
ALTER TABLE snips ADD COLUMN IF NOT EXISTS created_by INT(11) NULL;
ALTER TABLE snips ADD FOREIGN KEY IF NOT EXISTS FK_created_by(created_by) REFERENCES snip_perms.users(id);


-- a trigger which does not allow to update or insert a snip without a user_id if page_id is not null
DROP TRIGGER IF EXISTS snip_user_id_check;
DELIMITER $$
CREATE TRIGGER snip_user_id_check
BEFORE INSERT ON snips
FOR EACH ROW
BEGIN
    IF NEW.page_id IS NOT NULL AND NEW.created_by IS NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Snip was not assigned a user_id', MYSQL_ERRNO = 3410;
    END IF;
END;
$$
DELIMITER ;


DROP TRIGGER IF EXISTS snip_user_id_check_update;
DELIMITER $$
CREATE TRIGGER snip_user_id_check_update
BEFORE UPDATE ON snips
FOR EACH ROW
BEGIN
    IF NEW.page_id IS NOT NULL AND NEW.created_by IS NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Snip was not assigned a user_id', MYSQL_ERRNO = 3410;
    END IF;
END;
$$
DELIMITER ;