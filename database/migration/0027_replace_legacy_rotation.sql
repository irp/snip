
USE snip_data;

-- drop update trigger
DROP TRIGGER IF EXISTS upd_last_updated_pages;

-- update all without mirror
UPDATE snips
SET view_json = JSON_SET(
    view_json,
    '$.rot',
    CAST(JSON_UNQUOTE(JSON_EXTRACT(view_json, '$.rot')) AS UNSIGNED)
),
last_updated=last_updated
WHERE JSON_UNQUOTE(JSON_EXTRACT(view_json, '$.rot')) IN ('0', '90', '180', '270');
-- update all with mirror
UPDATE snips
SET view_json = JSON_SET(
    view_json,
    '$.rot',
    CASE
        WHEN JSON_UNQUOTE(JSON_EXTRACT(view_json, '$.rot')) = '0m' THEN 0
        WHEN JSON_UNQUOTE(JSON_EXTRACT(view_json, '$.rot')) = '90m' THEN 90
        WHEN JSON_UNQUOTE(JSON_EXTRACT(view_json, '$.rot')) = '180m' THEN 180
        WHEN JSON_UNQUOTE(JSON_EXTRACT(view_json, '$.rot')) = '270m' THEN 270
    END,
    '$.mirror',
    true
),
last_updated=last_updated
WHERE JSON_UNQUOTE(JSON_EXTRACT(view_json, '$.rot')) IN ('0m', '90m', '180m', '270m');


-- enable triggers on snips table
DELIMITER $$
CREATE TRIGGER `upd_last_updated_pages` AFTER UPDATE ON `snips` FOR EACH ROW UPDATE pages
            SET pages.last_updated = current_timestamp() WHERE pages.id = NEW.page_id
$$
DELIMITER ;
