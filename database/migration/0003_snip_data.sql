-- CREATES SNIP DATA DATABASE
CREATE Database IF NOT EXISTS snip_data;
USE snip_data;
-- Create table for measuring instruments
Create table instruments (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(32) NOT NULL UNIQUE,
    description text NOT NULL,
    -- utils
    created datetime NOT NULL DEFAULT current_timestamp(),
    last_updated timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb3;
-- Background images for books i.e. checkered or lines 
CREATE Table background_types (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -- Background type
    name varchar(32) NOT NULL,
    description text NULL,
    -- utils
    created datetime DEFAULT current_timestamp()
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb3;
insert into background_types (id, name, description)
values (
        1,
        'blank',
        'Just your ordinary blank pages, nothing too special. Just a clean slate for you to write and draw onto.'
    ),
    (
        2,
        'checkered 4mm x 4mm',
        "Checkered background, the typical 4mm x 4mm grid you know and love?!."
    ),
    (
        3,
        'lined 7mm',
        "Simple lined pages, the lines are 7mm apart."
    );
-- Create table for books
CREATE Table books (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_german2_ci NOT NULL,
    comment mediumtext CHARACTER SET utf8mb3 COLLATE utf8mb3_german2_ci DEFAULT NULL,
    -- Ownership only one can be set!
    owner_user_id INT,
    owner_group_id INT,
    FOREIGN KEY (owner_user_id) REFERENCES snip_perms.users(id),
    FOREIGN KEY (owner_group_id) REFERENCES snip_perms.groups(id),
    constraint owner_unique check(
        (
            owner_user_id is null
            or owner_group_id is null
        )
        and not (
            owner_user_id is null
            and owner_group_id is null
        )
    ),
    -- Cover page
    cover_page_id INT,
    -- Added later FOREIGN KEY (cover_page_id) REFERENCES pages(id),
    -- Background type
    background_type_id INT NOT NULL DEFAULT 1,
    FOREIGN KEY (background_type_id) REFERENCES background_types(id),
    -- utils
    created datetime DEFAULT current_timestamp(),
    finished datetime DEFAULT NULL,
    last_updated timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb3;
-- many to many relationship of instruments and books i.e. junction table
CREATE Table books_instruments (
    book_id INT NOT NULL,
    instrument_id INT NOT NULL,
    PRIMARY KEY (book_id, instrument_id),
    FOREIGN KEY (book_id) REFERENCES books(id),
    FOREIGN KEY (instrument_id) REFERENCES instruments(id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb3;
-- Create table for pages
CREATE Table pages (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -- foreign constrain book_id
    book_id INT NOT NULL,
    FOREIGN KEY (book_id) REFERENCES books(id),
    -- page_number
    page_number smallint(6) DEFAULT NULL,
    UNIQUE KEY bp (book_id, page_number),
    -- utils
    created datetime DEFAULT current_timestamp(),
    last_updated timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb3;
-- Add foreign key to book table
ALTER TABLE books
ADD FOREIGN KEY (cover_page_id) REFERENCES pages(id);
-- Create table for snips
CREATE Table snips (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -- foreign constrain book_id
    book_id INT NOT NULL,
    FOREIGN KEY (book_id) REFERENCES books(id),
    -- foreign constrain page_id
    page_id INT NULL,
    FOREIGN KEY (page_id) REFERENCES pages(id),
    -- Data and view maybe create own table in the future (todo)
    -- also fully normalize type (todo)
    type varchar(32) DEFAULT NULL,
    data_json mediumtext DEFAULT NULL,
    view_json mediumtext DEFAULT NULL,
    hide boolean DEFAULT 0,
    -- utils
    created datetime DEFAULT current_timestamp(),
    last_updated timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb3;
-- Create table for blobs
-- size, data and mime could be set to NOT NULL (todo)
CREATE Table blobs (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -- foreign constrain snip_id
    snip_id INT,
    FOREIGN KEY (snip_id) REFERENCES snips(id),
    -- data
    mime varchar(32) DEFAULT NULL,
    size INT DEFAULT NULL,
    `data` mediumblob DEFAULT NULL,
    `hash` varchar(32) DEFAULT NULL,
    -- utils
    created datetime DEFAULT current_timestamp(),
    last_updated timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb3;
-- Grant snip user access to snip_* database (todo change to only allow localhost (node container))
GRANT ALL PRIVILEGES ON snip_data.* TO 'snip' @'%';
GRANT ALL PRIVILEGES ON snip_perms.* TO 'snip' @'%';
FLUSH PRIVILEGES;