-- alter snip table to include blob_id
USE snip_data;
ALTER TABLE snips
ADD COLUMN blob_id INT DEFAULT NULL;
ALTER TABLE snips
ADD FOREIGN KEY (blob_id) REFERENCES blobs(id);
-- JSON_VALUE data_json and add blob_id to snip table
UPDATE snips
SET blob_id = JSON_VALUE(data_json, '$.blobid')
WHERE JSON_VALUE(data_json, '$.blobid') IS NOT NULL;
-- Remove blobid from data_json
UPDATE snips
SET data_json = JSON_REMOVE(data_json, '$.blobid')
WHERE JSON_VALUE(data_json, '$.blobid') IS NOT NULL;
-- Remove snip_id from blobs table
ALTER TABLE blobs DROP FOREIGN KEY `blobs_ibfk_1`;
ALTER TABLE `blobs` DROP INDEX `snip_id`;
ALTER TABLE blobs DROP COLUMN snip_id;