-- It is a bit easier to manage the background type on a page level
-- we can just add a new column to the pages table and set the default
-- value to '1' i.e. no special background.
USE snip_data;
ALTER TABLE pages
ADD COLUMN background_type_id INT NULL DEFAULT NULL,
    ADD FOREIGN KEY (background_type_id) REFERENCES background_types(id);
-- Additionally we rename the same column on the book table to default_background_type_id
-- this is to make it clear that this is the default background type for the book.
-- It is used when a page does not have a background type set (see trigger below)
ALTER TABLE books
    RENAME COLUMN background_type_id TO default_background_type_id;
-- We also have to alter the view
ALTER VIEW `books_resolved` AS
SELECT B.id,
    B.title,
    B.comment,
    B.created,
    B.finished,
    B.last_updated,
    -- resolve the owner
    CASE
        WHEN B.owner_user_id IS NULL THEN G.id
        ELSE U.id
    END AS owner_id,
    CASE
        WHEN B.owner_user_id IS NULL THEN G.name
        ELSE U.email
    END AS owner_name,
    CASE
        WHEN B.owner_user_id IS NULL THEN "group"
        ELSE "user"
    END AS owner_type,
    -- resolve the background type
    B.default_background_type_id,
    BG.name AS default_background_type_name,
    BG.description AS default_background_type_description
FROM snip_data.books as B
    LEFT JOIN snip_perms.users as U ON owner_user_id = U.id
    LEFT JOIN snip_perms.groups as G ON owner_group_id = G.id
    LEFT JOIN snip_data.background_types as BG ON B.default_background_type_id = BG.id;
-- Add view to resolve the background type on a page
CREATE OR REPLACE VIEW pagesView AS
SELECT P.id,
    P.book_id,
    P.page_number,
    P.created,
    P.last_updated,
    -- resolve the background type
    CASE
        WHEN P.background_type_id IS NULL THEN B.default_background_type_id
        ELSE P.background_type_id
    END AS background_type_id
FROM snip_data.pages as P
    LEFT JOIN snip_data.books as B ON P.book_id = B.id;