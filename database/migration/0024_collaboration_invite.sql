USE snip_perms;


-- create a table to store collaboration invites
CREATE OR REPLACE TABLE collaboration_invites (
    id INT AUTO_INCREMENT PRIMARY KEY,
    book_id INT NOT NULL COMMENT 'The book the invite is for (might change to general resource in the future)',
    user_id INT NOT NULL COMMENT 'The user who is invited to collaborate',
    invited_by INT NOT NULL COMMENT 'The user who invited the user to collaborate',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT 'The time the invite was created',
    expire_at TIMESTAMP NOT NULL COMMENT 'The time the invite expires',
    accepted_at TIMESTAMP NULL COMMENT 'The time the invite was accepted, NULL if not accepted',
    FOREIGN KEY (book_id) REFERENCES snip_data.books(id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (invited_by) REFERENCES users(id)
) COMMENT 'Table to store collaboration invites';


-- create a view to resolve the users and the invited_by user
CREATE OR REPLACE VIEW collaboration_invites_users_resolved AS
SELECT 
    CI.id,
    CI.book_id,
    CI.user_id,
    U.email AS user_email,
    CI.invited_by,
    U2.email AS invited_by_email,
    CI.created_at,
    CI.expire_at,
    CI.accepted_at
FROM collaboration_invites AS CI
LEFT JOIN users AS U ON CI.user_id = U.id
LEFT JOIN users AS U2 ON CI.invited_by = U2.id;



-- create a trigger to set the expiration time of an invite
DELIMITER $$
CREATE OR REPLACE TRIGGER collaboration_invites_ins_trigger BEFORE INSERT ON collaboration_invites FOR EACH ROW
BEGIN
    -- Check if a custom expiration time is provided, otherwise set it to 24 hours from the current time
    IF NEW.expire_at IS NULL THEN
        SET NEW.expire_at = NOW() + INTERVAL 48 HOUR;
    END IF;
END $$
DELIMITER ;




-- create a procedure to update the status of an invite
DELIMITER $$
CREATE OR REPLACE PROCEDURE update_invite_status(IN invite_id INT)
BEGIN
    DECLARE invite_status SMALLINT;

    -- Check if the invite is expired or already accepted
    SELECT 
        CASE
            WHEN expire_at <= NOW() THEN 2 -- expired
            WHEN accepted_at IS NOT NULL THEN 1 -- accepted
            ELSE 0 -- valid
        END INTO invite_status
    FROM collaboration_invites WHERE id = invite_id;
    
    -- If the invite does not exist, set invite_status to -1
    IF invite_status IS NULL THEN
        SET invite_status = -1;
    END IF;

    -- Update invite status
    CASE invite_status
        WHEN 0 THEN
            UPDATE collaboration_invites SET accepted_at = CURRENT_TIMESTAMP WHERE id = invite_id;
        WHEN 1 THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invite has already been accepted', MYSQL_ERRNO = 3401;
        WHEN 2 THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invite has expired', MYSQL_ERRNO = 3402;
        WHEN -1 THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invite does not exist', MYSQL_ERRNO = 3403;
    END CASE;
END$$
DELIMITER ;

