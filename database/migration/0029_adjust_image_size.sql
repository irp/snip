-- Adjust the size of the image snips if they are not
-- placed yet to fit them into the page

USE snip_data;

UPDATE snips
SET view_json = JSON_SET(
    view_json, 
    '$.width', 
    1400, 
    '$.height', 
    CAST(1400 * (JSON_EXTRACT(view_json, '$.height') / JSON_EXTRACT(view_json, '$.width')) AS UNSIGNED)
)
WHERE type = 'image' AND page_id IS NULL AND JSON_UNQUOTE(JSON_EXTRACT(view_json, '$.width')) > 1400;