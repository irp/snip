-- Rename snip types into new format
USE snip_data;
UPDATE snips
SET type = 'uprp/spec/timestamp'
WHERE type = 'timestamp';
UPDATE snips
SET type = 'uprp/spec/motors'
WHERE type = 'motors';
UPDATE snips
SET type = 'uprp/spec/macro.spec'
WHERE type = 'macro.spec';
UPDATE snips
SET type = 'uprp/spec/logfile'
WHERE type = 'logfile';
UPDATE snips
SET type = 'image'
WHERE type = 'cplot';
UPDATE snips
SET type = 'image'
WHERE type = 'manta';
UPDATE snips
SET type = 'image'
WHERE type = 'matlab';
UPDATE snips
SET type = 'image'
WHERE type = 'scope';