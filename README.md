<p align="center">
    <h1 align="center">Snip</h1>
</p>
<p align="center">
    <em>Our digital lab book</em>
</p>


<p align="center">
<a href="https://gitlab.gwdg.de/irp/snip/-/commits/main">
    <img alt="pipeline status" src="https://gitlab.gwdg.de/irp/snip/badges/main/pipeline.svg?key_text=Build&style=flat-square" />
</a>
<a href="https://gitlab.gwdg.de/irp/snip/-/releases">
    <img alt="release" src="https://gitlab.gwdg.de/irp/snip/-/badges/release.svg?style=flat-square" />
</a>
<a href="https://gitlab.gwdg.de/irp/snip/-/blob/main/LICENSE">
    <img alt="License: GPL v3" src="https://img.shields.io/badge/License-GPL%20v3-blue.svg?style=flat-square" />
</a>
<a href="https://gitlab.gwdg.de/irp/snip/-/commits/main">
    <img alt="pipeline status" src="https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square" />
</a> 
</p>


## About

This is the monorepo for snip - our digital lab book. This repo includes the complete code for running the labbook app and the backend services.

Currently we run a public instance of snip at [snip.roentgen.physik.uni-goettingen.de](https://snip.roentgen.physik.uni-goettingen.de). Which can be used to test the application without having to install it locally. 

-   [Documentation](https://snip.roentgen.physik.uni-goettingen.de/docs/getting-started) (or see `docs` folder)
-   [Changelog](CHANGELOG.md)


## Getting started

The app is designed to be self-hosted with relative ease. We prebuild the services into docker containers that can be used to run the application. We provide you a [docker-compose](./docker/docker-compose.yml) file that contains all the services needed to run the application.

For a full guide on how to deploy snip, please visit our [documentation](https://snip.roentgen.physik.uni-goettingen.de/docs/deployment/getting-started) or see the `docs/deployment` folder in this repo.


## Repo overview

The repo is structured into packages, apps and assets. Apps can be run and are the entry points for the application. Packages are shared code that is used by the apps and other packages. Assets are static files that are used by the apps. 

```bash
apps
├── fullstack # next.js application
├── nginx # reverse proxy 
├── render # render server
├── email # email server
└── socket # socket server
```

```bash
packages
├── auth # authentication
├── configs # configuration for eslint, typescript, etc.
├── database # database service and types
├── snips # snip types
├── scripts # scripts for the project
└── utils # utility functions
```

## Development

We have a docker compose file that can be used to start a local development environment. This will start the frontend, all backend services and the database server locally and live mount the code into the containers.

```bash
git clone https://gitlab.gwdg.de/irp/snip.git
docker compose -f docker/docker-compose.dev.yml build
docker compose -f docker/docker-compose.dev.yml up
```

