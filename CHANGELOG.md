# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

## 1.12.0

### Fixed

- Snip preview images did not change when a snips content was changed. There is now an event target which handles snip content updates. 

### Added

- Experimental markdown support.
    - Introduced markdown snip type
    - Editor for basic markdown syntax
    - Thus for now only basic markdown syntax is supported
    - As of now the support for markdown is experimental and for instance highlighting is not working yet but may come in a future release.
    - Stay tuned
- Render engine: Allow render functions to set a dirty flag in render pipeline. This allows
to re-render a function if it is marked as dirty.


## 1.11.4

Hotfix for broken collaboration invitations

## [1.11.3]

### Fixed

- Connection issue modal pops up if a page transition is triggered while the connection is lost now has a 5s delay before showing the modal

### Added

- Transition between pages in the editor now has a loading indicator

### Changed

- Book pages are now a full page route instead of a page hash. I.e. /book/:id/page/:pageid instead of /book/:id#page=:pageid

## [1.11.2]

### Added

- Added a new disconnected modal which is shown once the server connection is lost

### Fixed

- Issues with reconnect of socket connection once it is lost

## [1.11.1]

### Fixed

- Fixed a small bug where the text initial position was not computed correctly on desktop devices

## [1.11.0]

### Added

- Added a advanced page creation tool. Allows to choose the page background and if the page should be a reference to another page. 
    - Can be accessed by right/context clicking on the normal page creation button, relatively hidden for now on purpose. If you read the changelog I guess you deserve to know about it.

### Fixed

- Sometimes the text placement tool would persist after the snip was placed

## Changed

- CI pipeline is now using the stage less backend this allows to resolve the dependencies
of all builds and tests as DAG. Should increase the speed of the pipeline significantly, on the other hand I have not figured out how to only run jobs if the dependencies have changed. It works but is not optimal yet. 

## [1.10.0]

### Added

Pointer positions are automatically transferred to other users on the same page, this can be used as a pointer tool to show other users where you are looking at. You may change your color in your settings.

## [1.9.3]

### Added
- New post-it note component
    - May be used to display notifications or other information
    - At the moment used for the update notification and mobile installation notification

## Fixed

- Fixed a bug where after a user visited a shared link, the user was not able to edit this book and view other books (redirected to not found)
    - Permissions where not resolved correctly, the first found set is used
- A minor SSO issue when the user is behind a reverse proxy
    - The redirect url was not correctly set to the server url (without the proxy)
- Placement of snips using pen too sensitive as the pen dpi was not correctly detected

## [1.9.2]

### Added

- Backref capabilities for inserted snips
    - On insert links are now returned to view the inserted snippet or retrieve it via the api
    - Added a page to view a snippet by its id
- Added a hash handler for the editor to allow for direct linking to page ids

## [1.9.1]

### Added

- Metadata for privacy policy route and schemas route
- Snip python package link snip type and guide for it
- Added padding to footer on devices with unsafe bottom area e.g. iphone, ipad...
- Duplicate button to snip context menu allows to duplicate a snip without the need to reupload it (by public demand).

### Changed

- Removed navbar in favour of simple back button to schemas, privacy policy and login pages
- Removed overlay trigger for tool labels as this way quite buggy in combination with the resizer,
the labels did not update correctly and were not removed correctly
- Touch move option for text tool is now persistent in the browser storage, i.e. reloads wil not reset the option
- Image resizer tool is now ratio locked by default (by public demand). This can be disabled by holding shift while resizing the image.

### Fixed

- Slight visual error in overflow of sidebar on mobile devices
- Scroll not disabled on doodle testing field in accounts page, also dpi was not respected in doodle testing field
- Issue with color picker on ios/webkit devices not being shown. For some reason hidden inputs do not trigger... apple doing apple things
- Inconsistent styling of books browser on mobile devices where default styling was applied to buttons.
- Back gesture on ios safari devices was preventing some buttons to be pressed.
- Text tool on ios or mobile device used to move the window of the page out of view, this is now fixed
- Unwanted ";" character in upload page
- Exit fullscreen button disappeared on desktop devices in fullscreen mode
- Formatting in password reset form was inconsistent with other forms
- Footer spacing would be set to zero if safe-area-inset-bottom was not available 
- New snips notification not disappearing from the not seen list once they are placed see [#100](https://gitlab.gwdg.de/irp/snip/-/issues/100)
- Sidebar not clickable in fullscreen on some mobile devices. Added safe area padding to the sidebar to avoid this issue.
- Reconnection issue on stale pages to websocket server see [#89](https://gitlab.gwdg.de/irp/snip/-/issues/89)
- Allow contextMenu click on mobile devices by holding the touch event for a longer time
- PWA padding was all over the place. Sometime this introduces a problem as on ios the top or bottom bar is not interactive (introduced safe area)

## [1.9.0]

### Added

- Added a second tab to api token copy page to allow for easier copying of the apitokens, allows to copy in ini format directly for use with `snip-python` package.
- Single Sign-On (SSO): Integrated SSO with credential management and account merging support.
- Progressive Web App (PWA): Initial PWA features, including installation and full-screen support.
- YAML Configuration System: Introduced modular YAML-based configuration for better flexibility and security.
- Optimized Deployment: Added an automated Nginx and mariadb container setup.
- Added ci pipelines to run package tests

### Fixed

- Fixed a bug where the cursor in the text tool was jumping to the end of the text when typing on multiline text areas see [#97](https://gitlab.gwdg.de/irp/snip/-/issues/97)
- Small optimization in book editor loading, should yield slightly shorter load times.
- Added stale revalidation to user config to avoid stale data see [#91](https://gitlab.gwdg.de/irp/snip/-/issues/91)
- Issues in verify emails not linking the correct verify page.
- Issues with user config not generated correctly for new users.

### Changed
- Dockerfile Rewrite: Optimized all containers, reducing sizes by ~20%. Frontend container size reduced from 350 MB to ~100 MB.
- Migration script is now migrated from python to nodejs


## [1.8.5]

### Added 
- Added a small notification badge on new snips in queue. Not synced between devices yet but shows if you haven't seen the snippet yet. Is set to seen once you hover over the snip or select it the first time. All snips are set to seen on page reload. This should at the moment only be used as an indicator for new snips in the queue. See [#98](https://gitlab.gwdg.de/irp/snip/-/issues/98)

### Fixed
- Minor inconveniences if the user tries to logout. Previously logout was only possible from the fontpage, now it is possible from the account page too and the branding (icon on the top right) redirects to the account page see [#96](https://gitlab.gwdg.de/irp/snip/-/issues/96).

### Changed
- Reversed order of snip queue items, now the newest is at the bottom to mirror the behavior of the page tab (by public demand).
- Scrollbar in the sidebar is now on the right side (by public demand).


## [1.8.4]

### Added

- New python package for interfacing with the snip api
    - Please see https://snip-python.readthedocs.io/en/latest/quickstart.html for more information
    
### Fixed
- Placeholder snip not parsed correctly 
- Interaction events now again work for the array snips
- TextSnip wrap on server side was not working correctly this yielded strange behavior for previews
- Small issue with shared link redirect not working as expected.

## [1.8.3]

### Added

- Enhanced docs
  - Added customization page on how to add fonts to a deployment
  - Refactored snips developer guide to be up to date
  - Refactored api docs to be up to date

### Fixed

- Placeholder snip not parsed correctly 
- Interaction events now again work for the array snips
- API schemas where not available because of static render issues 
- Invite via email not working correctly
- Placement button in image editor was not clickable see #90

## [1.8.2]

### Fixed

- Fixed a small issue with current users not being updated when a page is changed
- Added a placement button for the image editor tool to allow quick placement of the image on the page
- Small issue with the initial subrect not being set correctly for the image editor tool 

### Changed

- Internally snips are now parsed using the arktype library, this allows mainly for more useful error messages when parsing fails and the return of a jsonSchema

### Added

- Advanced Upload page allows to upload a snip using the json schema directly, this is useful for debugging and testing snips
- Schemas for all snip types are now available 

## [1.8.1]

### Added

- Favicons now are served in different sizes, light and dark mode to support different devices and themes
- Added some details to the developer docs on the database and authentication

### Changed

- Uploaded images are now adjusted to fit the page on initial upload this is also applied retrospectively to all existing (queued) snips
- Mobile layout now places the navigation bar inside a drawer which can be opened by clicking the hamburger icon on the top right of the screen this improves especially the documentation view on mobile devices

### Fixed

- Codeblocks formatting in the docs was broken, this is now fixed
- Userconfig is now loaded correctly for the tools

## [1.8.0]

### Added
- It is now possible to zoom while holding the barrel key on a pen or by using a pinch gesture on touch devices
- Snippet can now be moved using the arrow keys and shift, control to control the movement increment
- Readded ratio lock to manual image width and height input see [#80](https://gitlab.gwdg.de/irp/snip/-/issues/80) and [#85](https://gitlab.gwdg.de/irp/snip/-/issues/85)
- Mobile upload form now has a a grid selector, which allows to select a book by its frontpage/preview
- It is now possible to create snips with links from the ui, the snip type was existing for some time but without using the api directly there was no way to create them
    - RightClick on the queued snip or click the header of a queued snip to open the snip settings which allow to convert the snip to a link snip or delete the snip from the queue


### Changed
- Moving and zooming does not zoom the canvas element anymore but is changing the canvas view transformation
    - This increases performance significantly for high zoom values (runs perfectly fine with 60fps on my 6yo galaxy tablet)  
    - This also fixes [#84](https://gitlab.gwdg.de/irp/snip/-/issues/84)
    - The help description reflects this accordingly
- The render service was renamed image service and common render functions were moved to packages/render 
- Text linewrap now respects user linebreaks (for newly placed text snips only)
- Image backend is now using express for route parsing
- Backend render now uses skia-canvas for rendering (was node-canvas before), rendering is now done via gpu if available

### Fixed
- While resizing the page an currently actively placed snip is now resized according to the current transform without user interaction needed
- Moved to stricter type checking for the frontend app and fixed a big number of possible issues
- Touch inputs move canvas now also allow pinch zoom
- Downloaded PDFs did not show linked snips correctly

## [1.7.2]

### Fixed
- Upload size was restricted to 2MB by nginx, increase limit to 30MB

## [1.7.1]

### Fixed 
- Elements in the footer or generally on the bottom of the page were cutoff on mobile devices [#82](https://gitlab.gwdg.de/irp/snip/-/issues/82)
- Image rescaling by pressing shift does not work for top left and top right corner [#80](https://gitlab.gwdg.de/irp/snip/-/issues/80)
- Legacy snip mapping was missing see [#83](https://gitlab.gwdg.de/irp/snip/-/issues/83)

## [1.7.0]

### Added
- Dev guide on how to create a new tool
- Dev guide on how to create a new snip
- Wasm package for performance relevant code using rust->wasm
    - Used for boundary checks
- Allow for render specific interaction events

### Fixed
- Fixed small bug with snip render order of previously queued snips
- Interaction events always active i.e. teardown function was not executed
- Interaction event triggered twice
- changeEmail and verifyEmail did not work because of cookie parsing, the test cases catch that now
- Internal redirects to our server did not parse cookies correctly, see #77

### Changed 
- Switched from building cjs and esm to esm only for packages
    - interestingly this reduces the client bundle sizes by approx a factor 2
- Config is now its own package to allow for server and client side configuration

## [1.6.4]

## Added 
- Image editor now has number inputs again for setting the size of the image manually

## 1.6.3

## Fixed 
- Password reset did not work correctly, somehow one endpoint was missing

## 1.6.2

### Fixed
- Minor bug with blob parsing in legacy image snippets
- Internal redirects to our server did not parse cookies correctly, see #77

## [1.6.1]

### Added

- Readded hashtag handler for current page(s)
- The doodle tool now has toggle to allow touch events to be handled as move events instead of draw events see [#73](https://gitlab.gwdg.de/irp/snip/-/issues/73).

### Fixed

- (Legacy) Dada snip type was not parsed correctly 
- Database migration runs 26 is a bit faster now


### Changed

- Editor tools are now only shown based on the permission of the user currently editing the book
- Rollup options for subpackages optimized and streamlined which yields slightly smaller bundles


## [1.6.0]

### Added

- Frontpage now detects if the user is logged in.
  - If the user is logged in, the frontpage will show a user's books button instead of login.
- Added an indicator for currently online users.
  - The indicator is shown in the sidebar on the right side of each page preview if another user is currently viewing the page.
  - On hover, it shows the email of the user.
- Profile page has some functionality now.
  - User may change their email (requires email verification).
  - User may change their password (requires email verification).
  - User may set default values for the different tools.
- Removed the global navbar.
  - Opted to use the sidebar for main navigation.
- Clicking on the header of a snip now gives more information about the snip.
  - This includes: snip type, width, height, and creation date.
- Text tool now has an alignment submenu item.
  - Allows aligning the snippet to top, center, bottom, left, center, or right of the page.
- Image placement tool now has an alignment submenu item.
  - Allows aligning the snippet to top, center, bottom, left, center, or right of the page.
- Page move tool now has auto-fit width and height buttons.
  - Automatically fits the page to the width or height of your screen.
- Users are now notified on bad connections.
- Added validation for env parameters.
  - Catch missing env parameters early!
- Added BackgroundTypes database service.
- Added dedicated server for email sending.
- Maintenance page handler, if the app is down you will know now.

### Changed

- We now partially use app router instead of pages router.
  - Smaller bundle size.
  - Faster page transitions.
- Split up codebase into multiple independent packages.
  - Easier testing.
  - Easier development.
  - Significant change!
- Changelog now follows the Keep a Changelog format.

### Fixed

- Fixed canvas render width/height issue on devices with devicePixelRatio != 1.
  - See [#74](https://gitlab.gwdg.de/irp/snip/-/issues/74).
- Verify/Reset Password emails look better now (CSS added).
- Grid view overflow on long book names now shows as ellipsis.
- Fixed page flickering on resize.


## [1.5.3] 

### Changed

- Refactored email functions.
- Graceful quit for prebuild script.
- Upgraded from Buster to Bookworm Docker image.

### Fixed

- Fixed a bug in the server-side render pipeline (textBaseline was wrong).
  - See [#2343](https://github.com/Automattic/node-canvas/issues/2343).


## [1.5.2]

### Fixed

- Unauthorized screen layout was broken.
- Refactor upload endpoint.
- Updated API Usage docs.
  - Updated example scripts for upload.

### Added

- Test mode for testing uploads.


## [1.5.1]

### Added

- Page search in viewer is now fully functional.
  - Search is case insensitive.
  - Allows for modifiers (snip types, page numbers, etc.).
  - See docs for more info.

### Fixed

- Reset password screen alignment was broken.
- Verify email screen alignment was broken.
- Overflow was always shown in Chrome yielding a white line at the bottom of the page.

## 1.5.0

### Added

- Interaction mode (small eye icon).
  - Allows copying and pasting texts.
  - Allows interacting with snips that have event handlers.
    - E.g. on_click.
    - Currently, no snips have feedback, but the framework is there.

### Changed

- Refactor of overlay modules (related to above).
- Text edit tool uses the new overlay modules.


## [1.4.4]

### Changed

- Refactor of tools to reduce some code duplication.
- Split the developer docs into deployment and developer guides.
  - Also fixed some old docs.
  - Added to developer guides:
    - Snip documentation.
    - Tool documentation.
  - Parsing of links was broken if they ended with `.md` or `.mdx`.

### Added

- Docker entrypoint now automatically checks for database migration.

## [1.4.3]

### Fixed

- Fixed bug with draw interpolation.
  - On canvas leave, the line would be drawn to the left top corner due to inserting null values.
  - Do not draw null values.
- Logout not handled gracefully.


## [1.4.2] 

### Added

- Read-only mode.
  - Tools are dynamically picked by access level.
  - See new field in `/book/:id/edit/access`.

### Fixed

- Strange scroll behavior in edit page.
- Fixed a bug where the snips to place were not updated when a user adds a new snip.
- Overlay sometimes persisted on page change.
- Re-enabled old live updates for drawing (might be disabled again depending on performance).

## [1.4.1] 

### Fixed

- Fixed some alignment problems with the frontpage on mobile devices.

### Added

- Added pointer events instead of mouse events.
  - Should fix some problems with touch devices.
  - Applies to the resizer, move tool, draw tool, and text tool.

## [1.4.0] 

### Added

- We have a frontpage now! It explains the features of the app and helps new users get started.
- Pen line smoothing.
  - New slider to adjust smoothing.
  - Ramer-Douglas-Peucker algorithms for simplification of the polyline with additional splines for drawing.
- User settings.
  - Settings are saved for each user in the database.
  - Settings are synced across devices.
    - Sync happens on page reload or focus change to avoid confusion when using multiple tabs.
  - Settings include:
    - Zoom levels.
    - Font, font size, line height, line wrap.
    - Pen color, pen size.
- Added background gradient to page viewer.

### Fixed

- Fixed a bug where the page was setup multiple times by the worker.

### Changed

- Fonts are now dynamically injected into the page.
  - Makes the process of adding fonts easier.
  - Has no effect on the user.

## [1.3.4] 

### Fixed

- Chrome range slider looked different than other browsers.
- Zoom rounding error.
- Tab change sometimes produced an empty page, forcing rerender on activity change.

## [1.3.3]

### Added

- Added modal to configure PDF download.
  - Option to download with page numbers.
  - Option to only download selected pages.

### Fixed

- Adjusted styling of page number in the sidebar of the book viewer.
- Fixed bug with resizer using manual input or mouse wheel.
- Show finished books by default.
- Fixed tool not changing anymore in textarea on typing "p" or "m".

## [1.3.2]

### Added

- New book browser.
  - Folder sorting.
    - Drag and drop to sort folders.
  - Book sorting.
    - Sort by title, last modified, date added, and number of pages.

## [1.3.1]

### Added

- Image compression.
  - Added script to compress all images (see `scripts` folder).
  - Added auto image compression for file uploads.
  - WebP support for images.
- Maintenance mode.
  - Redirects all requests to `maintenance.html` page.
  - Started by putting `COMMAND=maintenance` in `.env` file.


## old

There are no changelogs for versions before 1.3.1.

[unreleased]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.11.0...HEAD
[1.11.0]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.10.0...v1.11.0
[1.10.0]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.9.3...v1.10.0
[1.9.3]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.9.2...v1.9.3
[1.9.2]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.9.1...v1.9.2
[1.9.1]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.9.0...v1.9.1
[1.9.0]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.8.5...v1.9.0
[1.8.5]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.8.4...v1.8.5
[1.8.4]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.8.3...v1.8.4
[1.8.3]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.8.2...v1.8.3
[1.8.2]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.8.1...v1.8.2
[1.8.1]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.8.0...v1.8.1
[1.8.0]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.7.2...v1.8.0
[1.7.1]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.7.1...v1.7.2
[1.7.1]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.7.0...v1.7.1
[1.7.0]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.6.4...v1.7.0
[1.6.4]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.6.1...v1.6.4
[1.6.1]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.6.0...v1.6.1
[1.6.0]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.5.3...v1.6.0
[1.5.3]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.5.2...v1.5.3
[1.5.2]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.5.1...v1.5.2
[1.5.1]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.5.0...v1.5.1
[1.5.0]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.4.4...v1.5.0
[1.4.4]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.4.3...v1.4.4
[1.4.3]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.4.2...v1.4.3
[1.4.2]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.4.1...v1.4.2
[1.4.1]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.4.0...v1.4.1
[1.4.0]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.3.4...v1.4.0
[1.3.4]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.3.3...v1.3.4
[1.3.3]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.3.2...v1.3.3
[1.3.2]: https://gitlab.gwdg.de/irp/snip/-/compare/v1.3.1...v1.3.2
