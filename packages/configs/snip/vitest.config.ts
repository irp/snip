// vitest.config.ts
import fs from "fs";
import { defineConfig } from "vitest/config";
export default defineConfig({
    test: {
        //setupFiles: ["./test_setup/global_mocks.ts"],
        globals: true,
        coverage: {
            provider: "v8",
            include: ["src/**/*"],
        },
    },
    resolve: {
        alias: {
            "@/": new URL("./src/", import.meta.url).pathname,
        },
    },
    plugins: [
        {
            name: "yamlastextloader",
            transform: (code, id) => {
                if (id.endsWith(".yaml")) {
                    const str = fs.readFileSync(id, "utf-8");
                    return `export default ${JSON.stringify(str)}`;
                }
            },
        },
    ],
});
