This small subpackages holds the default values for all configuration options of the application. There are two files here one for server side configuration and one for client side configuration. Most config options can be set via env. variables.

The server config options get filtered and never send to the client (see next config).