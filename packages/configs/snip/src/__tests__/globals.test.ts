import { MockInstance } from "vitest";

import { configTest } from "./config-test";

let consoleLogSpy: MockInstance;
let consoleErrorSpy: MockInstance;

beforeEach(() => {
    vi.resetModules();
    consoleLogSpy = vi.spyOn(console, "log").mockImplementation(() => {});
    consoleErrorSpy = vi.spyOn(console, "error").mockImplementation(() => {});
});

configTest("Create new config if none is provided", async () => {
    const { GLOBALS } = await import("../server/globals");
    expect(GLOBALS).toBeDefined();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
});

describe("Read env variables", () => {
    configTest("valid full config", async () => {
        process.env.SNIP_CONFIG_DIR = "/config";
        process.env.NODE_ENV = "production";
        process.env.MYSQL_HOST = "localhost";
        process.env.MYSQL_PORT = "3306";
        process.env.MYSQL_USER = "snip";
        process.env.MYSQL_PASSWORD = "well_this_should_be_changed_as_well";

        const { GLOBALS } = await import("../server/globals");

        expect(GLOBALS).toBeDefined();
        expect(consoleErrorSpy).not.toHaveBeenCalled();
    });

    configTest("invalid config", async () => {
        process.env.SNIP_PORT = "tests";

        try {
            await import("../server/globals");
        } catch (e) {
            expect(consoleErrorSpy).toHaveBeenCalled();
            expect(e).toBeDefined();
        }
    });
});
