import { MockInstance } from "vitest";
import { configTest } from "./config-test";
import fs from "node:fs";

let consoleLogSpy: MockInstance;
let consoleErrorSpy: MockInstance;

beforeEach(() => {
    vi.resetModules();
    consoleLogSpy = vi.spyOn(console, "log").mockImplementation(() => {});
    consoleErrorSpy = vi.spyOn(console, "error").mockImplementation(() => {});
});

configTest("Create new config if none is provided", async ({ tmpdir }) => {
    const { RENDER_CONFIG } = await import("../server/render");
    expect(RENDER_CONFIG).toBeDefined();
    expect(consoleLogSpy).toHaveBeenCalledWith(
        `File ${tmpdir}/render.yaml does not exist. Creating default file.`,
    );

    // Check if the file was created
    const fileExists = fs.existsSync(`${tmpdir}/render.yaml`);
    expect(fileExists).toBe(true);

    // Check if parsing the file works
    expect(RENDER_CONFIG.parse()).toBeDefined();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
});

describe("Read existing config", () => {
    configTest("valid full config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        num_processes: 1
        cache_dir: "/tmp"
        formats:
            - format: "jpeg"
              widths: [150, 720]
            - format: "pdf"
              widths: [720]
        `;

        fs.writeFileSync(`${tmpdir}/render.yaml`, EXAMPLE_FILE);

        const { RENDER_CONFIG } = await import("../server/render");
        expect(RENDER_CONFIG).toBeDefined();
        expect(RENDER_CONFIG.parse()).toEqual({
            num_processes: 1,
            cache_dir: "/tmp",
            formats: [
                {
                    format: "jpeg",
                    widths: [150, 720],
                },
                {
                    format: "pdf",
                    widths: [720],
                },
            ],
        });
        expect(consoleLogSpy).not.toHaveBeenCalled();
    });

    configTest("invalid config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        num_processes: what
            `;
        fs.writeFileSync(`${tmpdir}/render.yaml`, EXAMPLE_FILE);

        const { RENDER_CONFIG } = await import("../server/render");
        expect(RENDER_CONFIG).toBeDefined();

        // error on parse
        expect(() => RENDER_CONFIG.parse()).toThrowError();
    });
});
