import { MockInstance } from "vitest";
import { configTest } from "./config-test";
import fs from "node:fs";

let consoleLogSpy: MockInstance;
let consoleErrorSpy: MockInstance;

beforeEach(() => {
    vi.resetModules();
    consoleLogSpy = vi.spyOn(console, "log").mockImplementation(() => {});
    consoleErrorSpy = vi.spyOn(console, "error").mockImplementation(() => {});
});

configTest("Create new config if none is provided", async ({ tmpdir }) => {
    const { SOCKET_CONFIG } = await import("../server/socket");
    expect(SOCKET_CONFIG).toBeDefined();
    expect(consoleLogSpy).toHaveBeenCalledWith(
        `File ${tmpdir}/socket.yaml does not exist. Creating default file.`,
    );

    // Check if the file was created
    const fileExists = fs.existsSync(`${tmpdir}/socket.yaml`);
    expect(fileExists).toBe(true);

    // Check if parsing the file works
    expect(SOCKET_CONFIG.parse()).toBeDefined();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
});

describe("Read existing config", () => {
    configTest("valid full config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        num_processes: 1
        `;

        fs.writeFileSync(`${tmpdir}/socket.yaml`, EXAMPLE_FILE);

        const { SOCKET_CONFIG } = await import("../server/socket");
        expect(SOCKET_CONFIG).toBeDefined();
        expect(SOCKET_CONFIG.parse()).toEqual({
            num_processes: 1,
        });
        expect(consoleLogSpy).not.toHaveBeenCalled();
    });

    configTest("invalid config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        num_processes: what
            `;
        fs.writeFileSync(`${tmpdir}/socket.yaml`, EXAMPLE_FILE);

        const { SOCKET_CONFIG } = await import("../server/socket");
        expect(SOCKET_CONFIG).toBeDefined();

        // error on parse
        expect(() => SOCKET_CONFIG.parse()).toThrowError();
    });
});
