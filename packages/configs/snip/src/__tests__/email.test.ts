import { MockInstance } from "vitest";
import { configTest } from "./config-test";
import fs from "node:fs";

let consoleLogSpy: MockInstance;
let consoleErrorSpy: MockInstance;

beforeEach(() => {
    vi.resetModules();
    consoleLogSpy = vi.spyOn(console, "log").mockImplementation(() => {});
    consoleErrorSpy = vi.spyOn(console, "error").mockImplementation(() => {});
});

configTest("Create new config if none is provided", async ({ tmpdir }) => {
    const { EMAIL_CONFIG } = await import("../server/email");
    expect(EMAIL_CONFIG).toBeDefined();
    expect(consoleLogSpy).toHaveBeenCalledWith(
        `File ${tmpdir}/email.yaml does not exist. Creating default file.`,
    );

    // Check if the file was created
    const fileExists = fs.existsSync(`${tmpdir}/email.yaml`);
    expect(fileExists).toBe(true);

    // Check if parsing the file works
    expect(EMAIL_CONFIG.parse()).toBeDefined();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
});

describe("Read existing config", () => {
    configTest("valid full config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        enabled: true
        transport:
            host: "smtp.gmail.com"
            port: 465
            user: "user"
            password: "password"
        
        options:
            bcc: "copy@email.de"
            from: "from@email.de"
        `;

        fs.writeFileSync(`${tmpdir}/email.yaml`, EXAMPLE_FILE);

        const { EMAIL_CONFIG } = await import("../server/email");

        expect(EMAIL_CONFIG).toBeDefined();
        expect(EMAIL_CONFIG.parse()).toEqual({
            enabled: true,
            transport: {
                host: "smtp.gmail.com",
                port: 465,
                user: "user",
                password: "password",
            },
            options: {
                bcc: "copy@email.de",
                from: "from@email.de",
            },
        });
        expect(consoleLogSpy).not.toHaveBeenCalled();
    });

    configTest("invalid config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        transport:
            host: "smtp.gmail.com"
            port: 465
            `;
        fs.writeFileSync(`${tmpdir}/email.yaml`, EXAMPLE_FILE);

        const { EMAIL_CONFIG } = await import("../server/email");
        expect(EMAIL_CONFIG).toBeDefined();

        // error on parse
        expect(() => EMAIL_CONFIG.parse()).toThrowError();
    });
});
