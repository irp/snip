import { MockInstance } from "vitest";
import { configTest } from "./config-test";
import fs from "node:fs";

let consoleLogSpy: MockInstance;
let consoleErrorSpy: MockInstance;

beforeEach(() => {
    vi.resetModules();
    consoleLogSpy = vi.spyOn(console, "log").mockImplementation(() => {});
    consoleErrorSpy = vi.spyOn(console, "error").mockImplementation(() => {});
});

configTest("Create new config if none is provided", async ({ tmpdir }) => {
    const { SSO_CONFIG } = await import("../server/sso_provider");
    expect(SSO_CONFIG).toBeDefined();
    expect(consoleLogSpy).toHaveBeenCalledWith(
        `File ${tmpdir}/sso_provider.yaml does not exist. Creating default file.`,
    );

    // Check if the file was created
    const fileExists = fs.existsSync(`${tmpdir}/sso_provider.yaml`);
    expect(fileExists).toBe(true);

    // Check if parsing the file works
    expect(SSO_CONFIG.parse()).toBeDefined();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
});

describe("Read existing config", () => {
    configTest("valid full config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        enabled: true
        providers:
          - name: "Google"
            img: "google.png"
            description: "Login using Google"
            providerId: "google"
            type: "oidc"
            server: "https://google.com"
            clientId: "123"
            clientSecret: "456"
          - name: "Github" 
            img: "github.png"
            description: "Login using Github"
            providerId: "github"
            server: "https://github.com"
            type: "oidc"
            clientId: "789"
            clientSecret: "012"
        `;
        fs.writeFileSync(`${tmpdir}/sso_provider.yaml`, EXAMPLE_FILE);

        const { SSO_CONFIG } = await import("../server/sso_provider");
        expect(SSO_CONFIG).toBeDefined();
        expect(SSO_CONFIG.parse()).toEqual({
            enabled: true,
            providers: [
                {
                    name: "Google",
                    img: "google.png",
                    description: "Login using Google",
                    providerId: "google",
                    type: "oidc",
                    server: "https://google.com",
                    clientId: "123",
                    clientSecret: "456",
                },
                {
                    name: "Github",
                    img: "github.png",
                    description: "Login using Github",
                    providerId: "github",
                    server: "https://github.com",
                    type: "oidc",
                    clientId: "789",
                    clientSecret: "012",
                },
            ],
        });
        expect(consoleLogSpy).not.toHaveBeenCalled();
    });

    configTest("valid empty config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        enabled: true
        providers: []
        `;
        fs.writeFileSync(`${tmpdir}/sso_provider.yaml`, EXAMPLE_FILE);

        const { SSO_CONFIG } = await import("../server/sso_provider");
        expect(SSO_CONFIG).toBeDefined();

        expect(SSO_CONFIG.parse()).toEqual({
            enabled: true,
            providers: [],
        });
        expect(consoleLogSpy).not.toHaveBeenCalled();
    });

    configTest("reload config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        enabled: true
        providers:
          - name: "Google"
            img: "google.png"
            description: "Login using Google"
            providerId: "google"
            type: "oidc"
            server: "https://google.com"
            clientId: "123"
            clientSecret: "456"
        `;
        fs.writeFileSync(`${tmpdir}/sso_provider.yaml`, EXAMPLE_FILE);

        const { SSO_CONFIG } = await import("../server/sso_provider");
        expect(SSO_CONFIG).toBeDefined();
        const ex = SSO_CONFIG.parse();
        expect(ex).toEqual({
            enabled: true,
            providers: [
                {
                    name: "Google",
                    img: "google.png",
                    description: "Login using Google",
                    providerId: "google",
                    type: "oidc",
                    server: "https://google.com",
                    clientId: "123",
                    clientSecret: "456",
                },
            ],
        });

        // reload config
        SSO_CONFIG.reload();
        expect(SSO_CONFIG.parse()).toEqual(ex);

        // Second parse exits early
        expect(SSO_CONFIG.parse()).toEqual(ex);

        const EXAMPLE_FILE2 = `
        enabled: true
        providers:
          - name: "Github" 
            img: "github.png"
            description: "Login using Github"
            providerId: "github"
            server: "https://github.com"
            type: "oidc"
            clientId: "789"
            clientSecret: "012"
        `;
        fs.writeFileSync(`${tmpdir}/sso_provider.yaml`, EXAMPLE_FILE2);
        SSO_CONFIG.reload();

        expect(SSO_CONFIG.parse()).toEqual({
            enabled: true,
            providers: [
                {
                    name: "Github",
                    img: "github.png",
                    description: "Login using Github",
                    providerId: "github",
                    server: "https://github.com",
                    type: "oidc",
                    clientId: "789",
                    clientSecret: "012",
                },
            ],
        });
    });

    configTest("invalid config", async ({ tmpdir }) => {
        // should return process.exit(1)

        const EXAMPLE_FILE = `
        enabled: true
        providers:
            - name: "Google"
              invalid: "invalid"
        `;
        fs.writeFileSync(`${tmpdir}/sso_provider.yaml`, EXAMPLE_FILE);

        const { SSO_CONFIG } = await import("../server/sso_provider");
        expect(SSO_CONFIG).toBeDefined();

        // error on parse
        expect(() => SSO_CONFIG.parse()).toThrowError(Error);
    });
});
