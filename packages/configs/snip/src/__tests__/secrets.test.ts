import { MockInstance } from "vitest";
import { configTest } from "./config-test";
import fs from "node:fs";

let consoleLogSpy: MockInstance;
let consoleErrorSpy: MockInstance;

beforeEach(() => {
    vi.resetModules();
    consoleLogSpy = vi.spyOn(console, "log").mockImplementation(() => {});
    consoleErrorSpy = vi.spyOn(console, "error").mockImplementation(() => {});
});

configTest("Create new config if none is provided", async ({ tmpdir }) => {
    const { SECRETS_CONFIG } = await import("../server/secrets");
    expect(SECRETS_CONFIG).toBeDefined();
    expect(consoleLogSpy).toHaveBeenCalledWith(
        `File ${tmpdir}/secrets.yaml does not exist. Creating default file.`,
    );

    // Check if the file was created
    const fileExists = fs.existsSync(`${tmpdir}/secrets.yaml`);
    expect(fileExists).toBe(true);

    // Check if parsing the file works
    expect(SECRETS_CONFIG.parse()).toBeDefined();
    expect(consoleErrorSpy).not.toHaveBeenCalled();
});

describe("Parse config", () => {
    configTest("valid full config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        api_token: "${generateSecret()}"
        email_token: 
            key1: "${generateSecret()}"
        session: "${generateSecret()}"
        socket: 
            key1: "${generateSecret()}"
        `;

        fs.writeFileSync(`${tmpdir}/secrets.yaml`, EXAMPLE_FILE);

        const { SECRETS_CONFIG } = await import("../server/secrets");
        expect(SECRETS_CONFIG).toBeDefined();
        expect(SECRETS_CONFIG.parse()).toEqual({
            api_token: expect.any(String),
            email_token: expect.objectContaining({
                key1: expect.objectContaining({
                    active: true,
                    secret: expect.any(String),
                }),
            }),
            session: expect.any(String),
            socket: expect.objectContaining({
                key1: expect.objectContaining({
                    active: true,
                    secret: expect.any(String),
                }),
            }),
        });
        expect(consoleLogSpy).not.toHaveBeenCalled();
    });

    configTest("invalid config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        api_token: []
        email_token: "654321"
        session: "session"
        socket: "socket"
        `;
        fs.writeFileSync(`${tmpdir}/secrets.yaml`, EXAMPLE_FILE);

        const { SECRETS_CONFIG } = await import("../server/secrets");
        expect(SECRETS_CONFIG).toBeDefined();

        // error on parse
        expect(() => SECRETS_CONFIG.parse()).toThrowError();
    });

    configTest("Order of secrets is consistent", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        api_token: "${generateSecret()}"
        email_token:
            key1: "${generateSecret()}"
            a: "${generateSecret()}"
            1: "${generateSecret()}"
        session: "${generateSecret()}"
        socket:
            key1: "${generateSecret()}"
        `;
        fs.writeFileSync(`${tmpdir}/secrets.yaml`, EXAMPLE_FILE);

        const { SECRETS_CONFIG } = await import("../server/secrets");

        expect(SECRETS_CONFIG).toBeDefined();
        expect(SECRETS_CONFIG.parse().email_token["key1"]).toMatchObject({
            active: true,
            secret: expect.any(String),
        });
        expect(consoleLogSpy).not.toHaveBeenCalled();
    });

    configTest("active secrets", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        api_token: "${generateSecret()}"
        email_token:
            key1: "${generateSecret()}"
            key2: "${generateSecret()}"
        session: "${generateSecret()}"
        socket:
            active_key: "${generateSecret()}"
            key1: "${generateSecret()}"
        `;
        fs.writeFileSync(`${tmpdir}/secrets.yaml`, EXAMPLE_FILE);

        const { SECRETS_CONFIG } = await import("../server/secrets");
        expect(SECRETS_CONFIG).toBeDefined();

        // error on parse
        const active = SECRETS_CONFIG.activeEmailTokenSecret();
        expect(active).toMatchObject({
            active: true,
            secret: expect.any(String),
            id: "key1",
        });

        const activeSocket = SECRETS_CONFIG.activeSocketSecret();
        expect(activeSocket).toMatchObject({
            active: true,
            secret: expect.any(String),
            id: "active_key",
        });
    });
});

function generateSecret(): string {
    return Array.from({ length: 32 }, () =>
        Math.floor(Math.random() * 36).toString(36),
    ).join("");
}
