import { test } from "vitest";
import os from "node:os";
import fs from "node:fs/promises";
import path from "node:path";
import { vi } from "vitest";
interface ConfigFixture {
    tmpdir: string;
}

async function createTempDir() {
    const ostmpdir = os.tmpdir();
    const tmpdir = path.join(ostmpdir, "unit-test-");
    return await fs.mkdtemp(tmpdir);
}

export const configTest = test.extend<ConfigFixture>({
    tmpdir: async ({}, use) => {
        const directory = await createTempDir();
        vi.stubEnv("SNIP_CONFIG_DIR", directory);
        await use(directory);
        await fs.rm(directory, { recursive: true });
    },
});
