import { MockInstance } from "vitest";
import { configTest } from "./config-test";
import fs from "node:fs";

let consoleLogSpy: MockInstance;
let consoleErrorSpy: MockInstance;

beforeEach(() => {
    vi.resetModules();
    consoleLogSpy = vi.spyOn(console, "log").mockImplementation(() => {});
    consoleErrorSpy = vi.spyOn(console, "error").mockImplementation(() => {});
});

configTest("Create new config if none is provided", async ({ tmpdir }) => {
    const { DATABASE_CONFIG } = await import("../server/database");
    expect(DATABASE_CONFIG).toBeDefined();
    expect(consoleLogSpy).toHaveBeenCalledWith(
        `File ${tmpdir}/database.yaml does not exist. Creating default file.`,
    );

    // Check if the file was created
    const fileExists = fs.existsSync(`${tmpdir}/database.yaml`);
    expect(fileExists).toBe(true);

    // Check if parsing the file works
    expect(DATABASE_CONFIG.parse()).toBeDefined();
    expect(consoleErrorSpy).not.toHaveBeenCalled();

    // Check if password was generated
    const config = DATABASE_CONFIG.parse();
    expect(config.database.password).toBeDefined();
    expect(config.database.password).not.toBe("${__SECRET__}");
    expect(config.database.password.length).toBe(36);
});

describe("Read existing config", () => {
    configTest("valid full config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        database:
            host: "localhost"
            port: 5432
            user: "snip"
            password: "well"
        `;

        fs.writeFileSync(`${tmpdir}/database.yaml`, EXAMPLE_FILE);

        const { DATABASE_CONFIG } = await import("../server/database");
        expect(DATABASE_CONFIG).toBeDefined();
        expect(DATABASE_CONFIG.parse()).toEqual({
            database: {
                host: "localhost",
                port: 5432,
                user: "snip",
                password: "well",
            },
        });
        expect(consoleLogSpy).not.toHaveBeenCalled();
    });

    configTest("invalid config", async ({ tmpdir }) => {
        const EXAMPLE_FILE = `
        num_processes: what
            `;
        fs.writeFileSync(`${tmpdir}/database.yaml`, EXAMPLE_FILE);

        const { DATABASE_CONFIG } = await import("../server/database");
        expect(DATABASE_CONFIG).toBeDefined();

        // error on parse
        expect(() => DATABASE_CONFIG.parse()).toThrowError();
    });
});
