/* This file loads environment variables from .env file
 * and makes them available to the rest of the app. Please
 * use the .env file to configure your app.
 */

import { z } from "zod";

import { formatZodError } from "./parsing";

const configSchema = z.object({
    TOOLS: z.object({
        ERASER: z.object({
            TIMEOUT: z.number().int(),
        }),
    }),
});

const config_ = {
    TOOLS: {
        ERASER: {
            /** Time until a snippet cant be deleted anymore (with the eraser tool in seconds) */
            TIMEOUT: process.env.ERASER_TIMEOUT
                ? parseInt(process.env.ERASER_TIMEOUT)
                : 60 * 5,
        },
    },
};

// Validate the config
const parse = configSchema.safeParse(config_);

if (parse.success === false) {
    throw new Error(formatZodError(parse.error));
    process.exit(1);
}

export const config = parse.data as typeof config_;
