/** Render server configuration
 *
 * This file contains the configuration schema for the render service.
 */
import path from "path";
import { z } from "zod";

import default_ from "../../defaults/render.yaml";
import { Config } from "../config";
import { GLOBALS } from "./globals";

/* -------------------------------------------------------------------------- */
/*                              Schema Definition                             */
/* -------------------------------------------------------------------------- */

export const RENDER_SCHEMA = z.object({
    num_processes: z.coerce.number(),
    cache_dir: z.string(),
    formats: z.array(
        z.object({
            format: z.string(),
            widths: z.array(z.coerce.number()),
        }),
    ),
});

/* -------------------------------------------------------------------------- */
/*                           Create config class obj                          */
/* -------------------------------------------------------------------------- */

const source = path.join(GLOBALS.config_dir, "render.yaml");
export const RENDER_CONFIG = new Config(source, RENDER_SCHEMA, default_);
