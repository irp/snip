/** Secrets configuration
 *
 * This file contains the configuration schema for the secrets used
 * for token and socket encryption.
 */
import path from "path";
import { z } from "zod";

import default_ from "../../defaults/secrets.yaml";
import { Config } from "../config";
import { GLOBALS } from "./globals";

/* -------------------------------------------------------------------------- */
/*                              Schema Definition                             */
/* -------------------------------------------------------------------------- */
const secret_schema = z
    .string()
    .min(32, "Secrets have to be at least 32 characters long");

const key_rotation = z
    .map(z.coerce.string().min(1), secret_schema)
    .refine((val) => [...val].length > 0, {
        message: "At least one secret has to be defined",
    })
    .transform((val) => {
        const ret: Record<string, { secret: string; active: boolean }> = {};

        [...val].forEach(([key, secret], i) => {
            ret[key] = { secret, active: i === 0 };
        });

        return ret;
    });

export const SECRETS_SCHEMA = z.preprocess(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (val) => Object.fromEntries(val as any),
    z.object({
        api_token: secret_schema,
        email_token: key_rotation,
        session: secret_schema,
        socket: key_rotation,
    }),
);

/* -------------------------------------------------------------------------- */
/*                           Create config class obj                          */
/* -------------------------------------------------------------------------- */

export class SecretsStore extends Config<typeof SECRETS_SCHEMA> {
    activeEmailTokenSecret(): { secret: string; active: boolean; id: string } {
        return this._getActive(this.parse().email_token);
    }

    activeSocketSecret(): { secret: string; active: boolean; id: string } {
        return this._getActive(this.parse().socket);
    }

    _getActive(secrets: z.infer<typeof key_rotation>): {
        secret: string;
        active: boolean;
        id: string;
    } {
        for (const key in secrets) {
            if (secrets[key]!.active) {
                return { ...secrets[key]!, id: key };
            }
        }
        throw new Error("No active secret found");
    }
}

const source = path.join(GLOBALS.config_dir, "secrets.yaml");
export const SECRETS_CONFIG = new SecretsStore(
    source,
    SECRETS_SCHEMA,
    default_,
    {
        yamlParseAs: "JS",
    },
);
