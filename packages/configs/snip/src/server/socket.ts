/** Socket server configuration
 *
 * This file contains the configuration schema for the socket service.
 */
import path from "path";
import { z } from "zod";

import default_ from "../../defaults/socket.yaml";
import { Config } from "../config";
import { GLOBALS } from "./globals";

/* -------------------------------------------------------------------------- */
/*                              Schema Definition                             */
/* -------------------------------------------------------------------------- */

export const SOCKET_SCHEMA = z.object({
    num_processes: z.coerce.number(),
});

/* -------------------------------------------------------------------------- */
/*                           Create config class obj                          */
/* -------------------------------------------------------------------------- */

const source = path.join(GLOBALS.config_dir, "socket.yaml");
export const SOCKET_CONFIG = new Config(source, SOCKET_SCHEMA, default_);
