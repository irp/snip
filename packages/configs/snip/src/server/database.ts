import path from "path";
import { z } from "zod";

import default_ from "../../defaults/database.yaml";
import { Config, defaultFolder } from "../config";
import { GLOBALS } from "./globals";

/* -------------------------------------------------------------------------- */
/*                              Schema Definition                             */
/* -------------------------------------------------------------------------- */

export const DATABASE_SCHEMA = z.object({
    database: z.object({
        host: z.string(),
        port: z.coerce.number(),
        user: z.string(),
        password: z.string(),
    }),
});

/* -------------------------------------------------------------------------- */
/*                           Create config class obj                          */
/* -------------------------------------------------------------------------- */

const source = path.join(GLOBALS.config_dir, "database.yaml");
export const DATABASE_CONFIG = new Config(source, DATABASE_SCHEMA, default_);
