/** Global configuration options
 *
 * This file contains the global configuration options for the server.
 * This configuration is loaded first and might influence other
 * configurations. Globals are loaded from environment variables!
 */
import { z } from "zod";

/* -------------------------------------------------------------------------- */
/*                              Schema Definition                             */
/* -------------------------------------------------------------------------- */

export const GLOBALS_SCHEMA = z.object({
    config_dir: z.string(),
    NODE_ENV: z.enum(["development", "production", "test"]),
    hostname: z.string(),
    port: z.coerce.number(),
});

/* -------------------------------------------------------------------------- */
/*                             Parse values from env                          */
/* -------------------------------------------------------------------------- */

const raw = {
    /** Directory where the configuration files are stored */
    config_dir: process.env.SNIP_CONFIG_DIR || "/config",

    /** Environment */
    NODE_ENV: process.env.NODE_ENV || "development",

    /** Server configuration */
    // How the server is accessed
    // e.g. https://snip.example.com:1400
    hostname: process.env.SNIP_HOSTNAME || "localhost",
    port: process.env.SNIP_PORT || "443",
};

const config = GLOBALS_SCHEMA.safeParse(raw);

/* c8 ignore start */
if (!config.success) {
    console.error("Error parsing globals from environment variables!");
    console.error(JSON.stringify(config.error.errors, null, 2));
    console.error(process.env);
    throw new Error("Error parsing globals from environment variables!");
}
/* c8 ignore end */

export const GLOBALS = {
    ...config.data,
    getServerUrl: () => {
        if (config.data.port === 443) {
            return new URL(`https://${config.data.hostname}`);
        }
        return new URL(`https://${config.data.hostname}:${config.data.port}`);
    },
};
