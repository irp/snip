/** Email server configuration
 *
 * This file contains the configuration schema for the email server.
 * It allows to load an email server configuration from a yaml file.
 */
import path from "path";
import { z } from "zod";

import default_ from "../../defaults/email.yaml";
import { Config } from "../config";
import { GLOBALS } from "./globals";

/* -------------------------------------------------------------------------- */
/*                              Schema Definition                             */
/* -------------------------------------------------------------------------- */

export const EMAIL_SCHEMA = z.object({
    enabled: z.boolean(),
    transport: z.object({
        host: z.string(),
        port: z.coerce.number(),
        user: z.string(),
        password: z.string(),
    }),
    options: z.preprocess(
        (val) => (val === null ? {} : val),
        z.object({
            bcc: z.string().email().optional(),
            from: z.string().email().optional(),
        }),
    ),
});

/* -------------------------------------------------------------------------- */
/*                           Parse values from files                          */
/* -------------------------------------------------------------------------- */

const source = path.join(GLOBALS.config_dir, "email.yaml");
export const EMAIL_CONFIG = new Config(source, EMAIL_SCHEMA, default_);
