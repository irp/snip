import { DATABASE_CONFIG } from "./database";
import { EMAIL_CONFIG } from "./email";
import { GLOBALS } from "./globals";
import { RENDER_CONFIG } from "./render";
import { SECRETS_CONFIG } from "./secrets";
import { SOCKET_CONFIG } from "./socket";
import { SSO_CONFIG } from "./sso_provider";

export const config = {
    ...GLOBALS,
    render: RENDER_CONFIG,
    email: EMAIL_CONFIG,
    sso: SSO_CONFIG,
    secrets: SECRETS_CONFIG,
    socket: SOCKET_CONFIG,
    database: DATABASE_CONFIG,
};

export default config;
