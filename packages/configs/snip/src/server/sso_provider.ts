/* Configuration for SSO providers.
 *
 * This file contains the configuration for the SSO providers.
 * The configuration is loaded from the file `$SNIP_CONFIG_DIR/sso_provider.yaml`.
 */
import path from "path";
import { z } from "zod";

import default_ from "../../defaults/sso_provider.yaml";
import { Config } from "../config";
import { GLOBALS } from "./globals";

/* -------------------------------------------------------------------------- */
/*                              Schema Definition                             */
/* -------------------------------------------------------------------------- */

export const SSO_SCHEMA = z.object({
    enabled: z.boolean(),
    providers: z.preprocess(
        (val) => (val === null ? [] : val),
        z.array(
            z.object({
                name: z.string(),
                img: z.string(),
                description: z.string(),
                type: z.enum(["oidc"]),
                providerId: z.string(),
                server: z.string(),
                clientId: z.string(),
                clientSecret: z.string(),
            }),
        ),
    ),
});

/* -------------------------------------------------------------------------- */
/*                           Parse values from files                          */
/* -------------------------------------------------------------------------- */

const source = path.join(GLOBALS.config_dir, "sso_provider.yaml");
export const SSO_CONFIG = new Config(source, SSO_SCHEMA, default_);
