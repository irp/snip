import fs from "fs";
import path from "path";
import { Document, parseDocument } from "yaml";
import { z, ZodType } from "zod";

import { formatZodError } from "./parsing";

interface ConfigOptions {
    yamlParseAs: "JS" | "JSON";
}

export class Config<T extends ZodType> {
    public schema: T;
    private filePath: string;
    private lastModified: number = 0;

    private configData: Document | null = null;
    private defaultDoc: Document;

    private options: ConfigOptions;

    constructor(
        filePath: string,
        schema: T,
        defaultDoc: Document | string,
        options?: ConfigOptions,
    ) {
        this.schema = schema;
        this.filePath = filePath;
        this.options = options || { yamlParseAs: "JSON" };

        if (typeof defaultDoc === "string") {
            defaultDoc = this.readDefaultTemplate(defaultDoc);
        }
        this.defaultDoc = defaultDoc;
        this.loadConfig();
    }

    private loadConfig() {
        // Create default if file does not exist
        if (!fs.existsSync(this.filePath)) {
            console.log(
                `File ${this.filePath} does not exist. Creating default file.`,
            );
            fs.mkdirSync(path.dirname(this.filePath), { recursive: true });
            fs.writeFileSync(this.filePath, this.defaultDoc.toString());
        }

        // Check if file has been modified
        const stats = fs.statSync(this.filePath);
        if (stats.mtimeMs === this.lastModified) {
            return;
        }

        // Parse the file content
        const fileContent = fs.readFileSync(this.filePath, "utf-8");
        this.configData = parseDocument(fileContent);
        this.lastModified = stats.mtimeMs;
        this.parsedConfig = null;
    }

    private parsedConfig: z.infer<T> | null = null;

    /** Parse the config file and return the parsed object
     *
     * @throws Error if the config file could not be parsed
     */
    public parse(): z.infer<T> {
        if (this.configData === null) {
            throw new Error("Config data is not loaded");
        }
        // early return if already parsed
        if (this.parsedConfig !== null) {
            return this.parsedConfig;
        }

        //parsing options
        let raw;
        if (this.options.yamlParseAs === "JS") {
            raw = this.configData.toJS({
                mapAsMap: true,
            });
        } else {
            raw = this.configData.toJSON();
        }
        const parsed = this.schema.safeParse(raw);

        if (!parsed.success) {
            console.error(`Could not parse config file: ${this.filePath}!`);
            throw new Error(formatZodError(parsed.error));
        }

        this.parsedConfig = parsed.data;
        return parsed.data;
    }

    public reload() {
        this.loadConfig();
    }

    /** Reads a default yaml file and parses it a s document
     * replaces all secrets with generated ones
     *
     * @param fpath path to the default file
     */
    private readDefaultTemplate(yaml: string): Document {
        // Replace all
        while (yaml.includes("${__SECRET__}")) {
            const randomSecret = generateSecret();
            yaml = yaml.replace("${__SECRET__}", randomSecret);
        }
        return parseDocument(yaml);
    }
}

// secrets are generated on first start
function generateSecret(): string {
    const chars = "0123456789abcdefghijklmnopqrstuvwxyz";
    return Array.from({ length: 36 }, () =>
        chars.charAt(Math.floor(Math.random() * chars.length)),
    ).join("");
}

import { fileURLToPath } from "url";
const __filenameNew = fileURLToPath(import.meta.url);
const __dirnameNew = path.dirname(__filenameNew);
export const defaultFolder = path.join(__dirnameNew, "..", "defaults");
