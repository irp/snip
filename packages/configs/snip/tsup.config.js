
import { defineConfig } from "tsup";

import { config } from "@snip/tsup-config/base";


export default defineConfig([
    {
        ...config,
        entry: {
            client: "./src/client.ts",
            server: "./src/server/index.ts",
            "server/globals": "./src/server/globals.ts",
        },
        loader: {
            ".yaml": 'text'
        },
        minify: false,
    },
]);
