const nodeExternalsPlugin = require("esbuild-node-externals").nodeExternalsPlugin;


/** @type {import('tsup').Options} */
const config = {
    splitting: true,
    clean: true,
    treeshake: true,
    minify: true,
    outDir: "build",
    format: ["esm"],
    bundle: true,
    dts: true,
    target: "es2022",
    skipNodeModulesBundle: true,
    plugins: [nodeExternalsPlugin(
        {
            cwd: process.cwd(),
            allowWorkspaces: true
        }
    )],
}


module.exports.config = config;