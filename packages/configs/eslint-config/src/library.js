const { resolve } = require("node:path");

const project = resolve(process.cwd(), "tsconfig.json");

/** @type {import("eslint").Linter.Config} */
module.exports = {
    extends: [
        "eslint:recommended",
        "prettier",
        "plugin:@typescript-eslint/recommended"
    ],
    globals: {
        React: true,
        JSX: true,
    },
    env: {
        node: true,
    },
    settings: {
        "import/resolver": {
            typescript: {
                project,
            },
        },
    },
    ignorePatterns: [
        // Ignore dotfiles
        ".*.js",
        "node_modules/",
        "dist/",
        "**/__tests__/*",
        "benchmark",
        "test_setup/",
        "build/",
        "coverage/",
        "jest.config.*",
    ],
    plugins: ["simple-import-sort"],
    rules: {
        "simple-import-sort/imports": [
            "warn",
            {
                "groups": [
                    // Node.js built-in modules anything starting with node:
                    ["^\\u0000", "^node:"],
                    // External modules
                    ["^\\w"],
                    // Internal modules
                    ["^@snip/?[a-z]"], // workspace
                    ["^src"],
                    ["^\\."],
                    // All other imports
                    ["^"],
                    ["^snips|^controller|^database"],
                    ["^components"],
                    // Styles (ending with .css, .scss, .sass, or .less)
                    ["^.+\\.(css|scss|sass|less)$"]
                ]
            }
        ],
        "simple-import-sort/exports": "warn",
        "@typescript-eslint/no-unused-vars": [
            "warn",
            {
                "args": "all",
                "argsIgnorePattern": "^_",
                "caughtErrors": "all",
                "caughtErrorsIgnorePattern": "^_",
                "destructuredArrayIgnorePattern": "^_",
                "varsIgnorePattern": "^_",
                "ignoreRestSiblings": true
            }
        ]
    },
    overrides: [
        {
            "files": ["__tests__/*.ts"],
            "env": {
                "jest": true
            }
        }
    ],
};