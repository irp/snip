from .__main__ import app as app

if __name__ == "__main__":  # pragma: no cover
    app()
