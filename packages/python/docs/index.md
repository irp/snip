---
hide-toc: true
---

# Snip Python Package

Python Package of quality of life and helper functions to interface with the Snip Lab Book. Allows to create and upload snippets with relative ease. Store and retrieve api tokens, and more.


## Features

```{include} ../README.md
:start-after: <!-- start features -->
:end-before: <!-- end features -->
```



## Documentation

```{toctree}    
---
maxdepth: 1
---

quickstart
cli
snippets
tokens
references
```

