# API Reference

```{eval-rst}
.. autosummary::
    :toctree: _autosummary
    :nosignatures:
    :recursive:
    :template: module.rst

    snip.token
    snip.snippets
    snip.api
```