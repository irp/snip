
import { defineConfig } from 'tsup';

import { config } from "@snip/tsup-config/base";

export default defineConfig([
    {
        ...config,
        entry: [
            "./src/**/*.ts",
            "!./src/**/__tests__/*.ts",
            "!./src/**/strategies.ts"
        ],
    }
]);