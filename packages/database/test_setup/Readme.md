# Testing the database layer


To test the database and service layer, we start a test database similar to the production database and run the tests against it.

This is done semi automatically using the `docker compose` command locally. This starts a database on port `8652`!

For the ci pipeline we configured a service that starts a database. This is done in the `.gitlab-ci.yml` file. The service is started and the tests are run against it.


 The database
is started using the `docker-compose` command. The `docker-compose` command reads the `docker-compose.yml` file and starts the database. We wait for it to start and then run the migration scripts to create the tables, we than seed the database with some test data. We then run the tests and finally tear down the database.



## Requirements

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

- [Python](https://www.python.org/)