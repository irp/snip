import { exec } from "child_process";
import mariadb from "mariadb";

/** See https://vitest.dev/config/#globalsetup for exports */

export const setup = async () => {
    console.log("Starting up test database");
    await setupDatabase();
};

export const teardown = async () => {
    console.log("Stopping test database");
    await teardownDatabase();
};

async function setupDatabase() {
    // check if TEST_PORT variable is set
    let port = process.env.TEST_PORT;
    if (!port) {
        console.warn("TEST_PORT not set! Using default port 8652.");
        port = "8652";
    }

    // Check if database is already running by ping
    const pool = mariadb.createPool({
        host: "test_mariadb",
        user: "snip",
        password: "snip",
        port: parseInt(port),
        acquireTimeout: 1000,
    });

    // Ping the database
    try {
        await pool.getConnection();
        console.log("Database is running!");
        return;
    } catch (e) {
        //console.error(e);
        console.error("Database is not running.");
    }

    console.log("Starting database...");
    await execute(
        `docker compose -f docker-compose.yaml up -d`,
        import.meta.dirname + "/docker",
    );
    // Wait for 2 seconds to allow the database to start
    await new Promise((resolve) => setTimeout(resolve, 2000));

    // Wait for the migration container to finish
    await execute("docker wait test_migration");

    // Await delete of all data
}

export async function teardownDatabase() {
    // Stop your Docker containers or any other teardown logic
    await execute("docker compose down");
}

const execute = async (string: string, dir?: string) => {
    console.log("Executing: ", string + (dir ? " in " + dir : ""));
    await new Promise((resolve, reject) => {
        exec(string, { cwd: dir }, (error, stdout: string, stderr: string) => {
            if (error) {
                console.error(stderr);
                reject(error);
            } else {
                resolve(stdout);
            }
        });
    });
};
