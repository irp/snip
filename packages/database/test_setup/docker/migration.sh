#!/bin/bash
function database_ready() {
    mariadb --host="$MYSQL_HOST" --user="$MYSQL_USER" --password="$MYSQL_PASSWORD" -e "SELECT 1;" > /dev/null 2>&1
}

# Check that the database is available
if [ -n "$MYSQL_HOST" ]; then
    database=`echo $MYSQL_HOST | awk -F[@//] '{print $4}'`
    echo "[Entrypoint] Waiting for Database ..."
    while ! database_ready; do
        # Show some progress
        echo -n "."
        sleep 1;
    done
    echo ""
    echo "[Entrypoint] Database is ready"
    # Give it another second
    sleep 1;
fi

# Run migrations
echo "[Entrypoint] Running database migrations"
pnpm --filter scripts migrate /usr/src/app/database/migration --host "$MYSQL_HOST" --user "root" --password "$MYSQL_ROOT_PASSWORD"
echo "[Entrypoint] Database migrations complete";

mariadb --host="$MYSQL_HOST" --user="$MYSQL_USER" --password="$MYSQL_PASSWORD" < "/clear.sql" > /dev/null 2>&1
echo "[Entrypoint] Database cleared";

# Waita bit
sleep 4;

mariadb --host="$MYSQL_HOST" --user="$MYSQL_USER" --password="$MYSQL_PASSWORD" -e "USE snip_perms; INSERT INTO groups (name) VALUES ('admin');" > /dev/null 2>&1
echo "[Entrypoint] Groups initialized"

