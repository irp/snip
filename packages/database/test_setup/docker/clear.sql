
USE snip_data;
-- save current foreign key settings and disable foreign key checks
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE snips;
TRUNCATE TABLE blobs;
TRUNCATE TABLE books;
TRUNCATE TABLE pages;


USE snip_perms;
TRUNCATE TABLE acl;
TRUNCATE TABLE groups;
TRUNCATE TABLE tokens;
TRUNCATE TABLE users;
Truncate TABLE users_groups;
TRUNCATE TABLE user_config;
SET FOREIGN_KEY_CHECKS = 1;
