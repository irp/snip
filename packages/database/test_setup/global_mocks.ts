import { pool_data, pool_permissions } from "../src/sql.connection";

vi.mock("@snip/config/server", () => {
    return {
        config: {
            MYSQL: {
                HOST: "localhost",
                PORT: 8652,
                USER: "snip",
                PASSWORD: "snip",
            },
        },
    };
});

afterAll(() => {
    //Close all connections
    pool_data.end();
    pool_permissions.end();
});
