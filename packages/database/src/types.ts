/* eslint-disable @typescript-eslint/no-explicit-any */
import * as t from "./sql.typings";

/** !!README!!
 * ----------------
 * All types are from an insert perspective, meaning that all but
 * the primary key fields are optional.
 *
 * If you need a full type, you can use the `Required<T>` type.
 */

/** We overload some types from the automatically generated sql typing to
 * remove unneeded fields and add the id type.
 */
export type ID = number;

export type BlobData = t.blobsData & { id: ID };
// Same as book but joins the owner and the background type table
export type BookDataResolved = t.books_resolvedData & { id: ID };
export type BackgroundTypeData = t.background_typesData & { id: ID };
export type CredentialsData = t.credentialsData;

type FileID = string;
/** FileData can represent both a book or a directory.
 * This is an abstraction for the book and directory data.
 */
export interface FileData {
    id: FileID; // Unique ID, database key
    name: string; // Name of the file or directory
    comment?: string; // Comment for the file

    // Properties for files
    isDir?: boolean; // If true, this is a directory, default: false
    parentId?: FileID; // ID of parent directory
    childrenIds?: FileID[]; // IDs of children

    // Properties derived from the books
    numPages?: number; // Shows number of pages in the book
    thumbnail?: string; // Automatically load thumbnail from this URL
    created?: Date; // Date the book was created
    last_updated?: Date; // Date the book was last updated
    finished?: boolean; // If true, the book is finished, default: false
    ownerID?: ID; // ID of the owner of the book
    ownerName?: string; // Username/email of the owner of the book
    url?: string; // URL to the book
    perms?: PermissionACLData; // Permissions for the book
}

// The following types are used to define the Access Control List (ACL)
export const RESOURCE_BOOK = 1;
export const RESOURCE_INSTRUMENT = 2;
export type Resource = typeof RESOURCE_BOOK | typeof RESOURCE_INSTRUMENT;
export const ENTITY_USER = 1;
export const ENTITY_GROUP = 2;
export const ENTITY_TOKEN = -10;
export type Entity =
    | typeof ENTITY_USER
    | typeof ENTITY_GROUP
    | typeof ENTITY_TOKEN;
export type PermissionACLData = t.aclData & {
    id: ID;
    entity_type: Entity;
    resource_type: Resource;
};

// TODO:create database view for bookowner
export interface BookOwner {
    id: number;
    entity_name: string;
    entity_type: "user" | "group";
}

/* -------------------------------------------------------------------------- */
/*                                   folders                                  */
/* -------------------------------------------------------------------------- */

export type FolderDataInsert = t.browserFoldersData;
export type FolderDataRet = Required<t.browserFoldersData>;

/* -------------------------------------------------------------------------- */
/*                                books & pages                               */
/* -------------------------------------------------------------------------- */
export type PageDataInsert = t.pagesData;

// Background id is resolved to default
export type PageDataRet = Required<t.pagesViewData> & {
    background_type_id: number;
};

export type BookDataInsert = t.booksData;
export type BookDataRet = Required<t.books_resolvedData>;

/* -------------------------------------------------------------------------- */
/*                        snips and their serialized data                       */
/* -------------------------------------------------------------------------- */

// Snip data base type for the database services also extended
// by the different snips

export interface BaseDataIn {
    blob?: BlobData;
    snips?: _SnipDataInsert<BaseDataIn, BaseView>[];
    snip?: _SnipDataInsert<BaseDataIn, BaseView>;
    [key: string]: any;
}
export interface BaseDataRet {
    blob?: BlobData;
    snips?: _SnipDataRet<BaseDataRet, BaseView>[];
    snip?: _SnipDataRet<BaseDataRet, BaseView>;
    [key: string]: any;
}
export interface BaseView {
    x?: number | undefined;
    y?: number | undefined;
    rot?: number | undefined;
    mirror?: boolean | undefined;
    [key: string]: any;
}

interface _SnipDataInsert<D extends BaseDataIn, V extends BaseView>
    extends Omit<t.snipsData, "data_json" | "view_json"> {
    data: D;
    view: V;
    type: string;
}
interface _SnipDataRet<D extends BaseDataRet, V extends BaseView>
    extends Omit<Required<t.snipsData>, "data_json" | "view_json"> {
    data_json: string;
    view_json: string;
    data: D;
    view: V;
    type: string;
}
export type SnipDataInsert = _SnipDataInsert<BaseDataIn, BaseView>;
export type SnipDataRet = _SnipDataRet<BaseDataRet, BaseView>;

export const BackgroundsTypes = t.background_types;

export type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

export interface PermissionACLDataResolved
    extends Omit<PermissionACLData, "entity_type"> {
    entity_name: string;
    entity_type: "user" | "group";
}

/* -------------------------------------------------------------------------- */
/*                           users, groups & members                          */
/* -------------------------------------------------------------------------- */

// Just to be use remove the hashedPassword from the user data
export type UserDataInsert = t.usersData;
export type UserDataRet = Omit<
    Required<t.users_with_emailsData>,
    | "emails"
    | "emails_verified"
    | "credential_ids"
    | "credential_types"
    | "credential_provider_ids"
> & {
    emails: string[];
    emails_verified: boolean[];
    credential_ids: ID[];
    credential_types: string[];
    credential_provider_ids: (string | null)[];
};

export type LastLoginRet = Required<t.last_loginsData>;

// Groups
export type GroupDataInsert = t.groupsData;
export type GroupDataRet = Required<t.groupsData>;

// Members (users_groups)
export type MemberRole = Required<t.users_groups_with_emailsData>["role"];
export type MemberDataRet = Omit<
    Required<t.users_groups_with_emailsData>,
    "emails" | "emails_verified" | "credential_ids" | "credential_types"
> & {
    emails: string[];
    emails_verified: boolean[];
    credential_ids: ID[];
    credential_types: string[];
};

// Collaboration invites
export type CollaborationInviteDataInsert = t.collaboration_invitesData;
export type CollaborationInviteDataRet = Omit<
    Required<t.collaboration_invites_with_emailsData>,
    | "emails"
    | "emails_verified"
    | "credential_ids"
    | "credential_types"
    | "invited_by_emails"
    | "invited_by_emails_verified"
    | "invited_by_credential_ids"
    | "invited_by_credential_types"
> & {
    emails: string[];
    emails_verified: boolean[];
    credential_ids: ID[];
    credential_types: string[];
    invited_by_emails: string[];
    invited_by_emails_verified: boolean[];
    invited_by_credential_ids: ID[];
    invited_by_credential_types: string[];
};

// User config data
export type UserConfigDataInsert = Omit<
    t.user_config_resolvedData,
    "pen_colors"
> & {
    pen_colors?: string[];
};
export type UserConfigDataRet = Required<UserConfigDataInsert> & {
    user_color: string;
};

/* -------------------------------------------------------------------------- */
/*                          tokens and sharable links                         */
/* -------------------------------------------------------------------------- */

export type TokenData = t.tokensData;
export type APITokenData = TokenData & {
    type: "api";
};
export type UITokenData = TokenData & {
    type: "ui";
};
