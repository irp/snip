import { backgroundTypeService } from "./services/backgroundType";
import { bookService } from "./services/book";
import {
    CollaboratorsInviteService,
    collaboratorsInviteService,
} from "./services/collaborators";
import { folderService } from "./services/folder";
import { groupService } from "./services/group";
import { pageService } from "./services/page";
import { PermissionService, permissionService } from "./services/permission";
import { snipService } from "./services/snip";
import { TokenService, tokensService } from "./services/tokens";
import { userService } from "./services/user";
import { userConfigService } from "./services/userConfig";
import * as s from "./strategies";

interface ServiceInterface {
    user: s.UserStrategy;
    group: s.GroupStrategy;
    book: s.BookStrategy;
    page: s.PageStrategy;
    snip: s.SnipStrategy;
    permission: PermissionService;
    userConfig: s.UserConfigStrategy;
    folder: s.FolderStrategy;
    backgroundType: s.BackgroundTypeStrategy;
    collaboratorsInvite: CollaboratorsInviteService;
    token: TokenService;
}

const service: ServiceInterface = {
    user: userService,
    group: groupService,
    book: bookService,
    page: pageService,
    snip: snipService,
    permission: permissionService,
    userConfig: userConfigService,
    folder: folderService,
    backgroundType: backgroundTypeService,
    collaboratorsInvite: collaboratorsInviteService,
    token: tokensService,
};
export default service;
