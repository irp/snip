import { createHash } from "crypto";

import { registerService } from "@snip/common";

import { check_mariadb_warning } from "../errors";
import { pool_data as pool } from "../sql.connection";
import { BlobStrategy } from "../strategies";
import { BlobData, ID, PartialBy } from "../types";

export class BlobService implements BlobStrategy {
    /**
     * Retrieves all blobs from the database.
     * @returns A promise that resolves to an array of BlobData objects.
     */
    async getAll(): Promise<BlobData[]> {
        throw new Error("Method not implemented.");
    }
    async search(query: string): Promise<BlobData[]> {
        throw new Error("Method not implemented.");
    }

    async upsert(blob: BlobData): Promise<BlobData> {
        const sql =
            "INSERT INTO `blobs` (`id`, `mime`, `size`, `data`, `hash`) VALUES (?, ?, ?, FROM_BASE64(?), ?) ON DUPLICATE KEY UPDATE `mime` = VALUES(`mime`), `size` = VALUES(`size`), `data` = VALUES(`data`), `hash` = VALUES(`hash`) RETURNING `id`";

        // Compute hash if not given (md5)
        if (!blob.hash) {
            const hash = createHash("md5");
            hash.update(blob.data);
            blob.hash = hash.digest("hex");
        }

        // compute size if not given in bytes
        if (!blob.size) {
            blob.size = Buffer.byteLength(blob.data, "base64");
        }

        // Insert blob into database
        const blob_id = await pool
            .query(sql, [
                blob.id > 0 ? blob.id : null,
                blob.mime,
                blob.size,
                blob.data,
                blob.hash,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                return res[0].id;
            });

        // Add id to blob
        blob.id = blob_id;
        blob.last_updated = new Date();

        return blob as BlobData;
    }

    async exists(blob_id: ID): Promise<boolean> {
        const sql = "SELECT * FROM blobs WHERE id = ?";

        const exists = await pool
            .query(sql, [blob_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.length > 0;
            });

        return exists;
    }

    /**
     * Inserts a blob into the database. If it already exists, it will be updated.
     *
     * @param blob - The blob data to be inserted.
     * @returns A Promise that resolves to the inserted blob data.
     */
    async insert(blob: PartialBy<BlobData, "id">): Promise<BlobData> {
        if (blob.id) {
            console.warn("BlobService.insert: id is set, inserting new blob");
        }
        const sql =
            "INSERT INTO `blobs` (`mime`, `size`, `data`, `hash`) VALUES (?, ?, FROM_BASE64(?), ?)";

        // Compute hash if not given (md5)
        if (!blob.hash) {
            const hash = createHash("md5");
            hash.update(blob.data);
            blob.hash = hash.digest("hex");
        }

        // compute size if not given in bytes
        if (!blob.size) {
            blob.size = Buffer.byteLength(blob.data, "base64");
        }

        // Insert blob into database
        const blob_id = await pool
            .query(sql, [blob.mime, blob.size, blob.data, blob.hash])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.insertId;
            });

        // Add id to blob
        blob.id = blob_id;
        blob.created = new Date();
        blob.last_updated = new Date();

        return blob as BlobData;
    }

    /** Return blob from database via id
     *
     * @param blob_id
     * @returns
     */
    async getById(blob_id: ID): Promise<BlobData> {
        const sql = "SELECT * FROM blobs WHERE id = ?";

        const blob: BlobData = await pool
            .query(sql, [blob_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    return Error(
                        "Number of blobs returned not equal to 1, got " +
                            res.length,
                    );
                }
                return res[0];
            });

        //Convert blob data to base64
        blob.data = Buffer.from(blob.data).toString("base64");

        return blob;
    }

    /**
     * Retrieves blob data by their IDs.
     * @param blob_ids - An array of blob IDs.
     * @returns A promise that resolves to an array of BlobData objects.
     */
    async getByIds(blob_ids: ID[]): Promise<BlobData[]> {
        const sql = "SELECT * FROM blobs WHERE id IN (?)";

        const blobs = await pool
            .query(sql, [blob_ids])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != blob_ids.length) {
                    return Error(
                        "Number of blobs returned not equal to number of requested blobs, got " +
                            res.length,
                    );
                }
                return res;
            });

        //Convert blob data to base64
        for (const blob of blobs) {
            blob.data = Buffer.from(blob.data).toString("base64");
        }

        return blobs;
    }

    /**
     * Updates a blob in the database.
     *
     * @param blob - The updated blob data.
     * @returnsA promise that resolves to true if the update was successful.
     */
    async update(blob: Partial<BlobData> & { id: ID }): Promise<BlobData> {
        const sql_data =
            "UPDATE `blobs` SET `mime`=?,`size`=?,`data`=FROM_BASE64(?),`hash`=? WHERE `id`=?";

        await pool
            .query(sql_data, [
                blob.mime,
                blob.size,
                blob.data,
                blob.hash,
                blob.id,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
            });

        blob.last_updated = new Date();

        return blob as BlobData;
    }

    /**
     * Deletes a blob from the database.
     *
     * @param blob_id - The ID of the blob to delete.
     * @returns A promise that resolves to true if the deletion was successful.
     */
    async delete(blob_id: ID): Promise<boolean> {
        const sql = "DELETE FROM `blobs` WHERE `id`=?";

        const success = await pool
            .query(sql, [blob_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Delete failed, no rows affected");
                }
                return true;
            });

        return success;
    }
}

/** We need to globally register each service here
 * to prevent memory leaks in development.
 */
export const blobService = registerService("db_blob", () => new BlobService());
