import bcrypt from "bcryptjs";

import { registerService } from "@snip/common";

import { users_with_emailsData } from "src/sql.typings";

import { check_mariadb_warning, NotFoundError } from "../errors";
import { pool_permissions as pool, pool_permissions } from "../sql.connection";
import { UserStrategy } from "../strategies";
import {
    CredentialsData,
    ID,
    LastLoginRet,
    PartialBy,
    UserDataInsert,
    UserDataRet,
} from "../types";
import { groupService } from "./group";

/** Implements interactions with
 * the user and credentials table.
 */
export class UserService implements UserStrategy {
    /** Creates a new user with the given credentials.
     *
     * This will always try to create a new user.
     * Might throw an error if the credentials are not valid or already
     * exists.
     *
     * @raises Error if no credentials are provided
     * @raises ProviderNullError if the provider_id is null and the credential_type is 'sso'
     * @raises EmailExistsError if the email already exists and type is 'password'
     */
    private async create(
        credentials: PartialBy<CredentialsData, "user_id">[],
    ): Promise<{
        user: UserDataRet;
        credentials: CredentialsData[];
    }> {
        if (credentials.length == 0) {
            throw new Error("No credentials provided");
        }

        // Inserted data
        let user_id: ID;
        const cred_ids: ID[] = [];

        const con = await pool_permissions.getConnection();
        try {
            // Start transaction
            await con.beginTransaction();

            // Create user
            user_id = await con
                .query("INSERT INTO users (`id`) VALUES (NULL)")
                .then(check_mariadb_warning)
                .then((res) => {
                    return res.insertId;
                });

            // Create credentials
            for (const cred of credentials) {
                cred.user_id = user_id;
                const c_id = await con
                    .query(
                        "INSERT INTO credentials (`user_id`, `credential_type`, `email`, `email_verified`, `password_hash`, `provider_id`, `provider_user_id`) VALUES (?, ?, ?, ?, ?, ?, ?)",
                        [
                            cred.user_id,
                            cred.credential_type,
                            cred.email.toLowerCase(),
                            cred.email_verified,
                            cred.password_hash,
                            cred.provider_id,
                            cred.provider_user_id,
                        ],
                    )
                    .then(check_mariadb_warning)
                    .then((res) => {
                        return res.insertId;
                    });

                cred_ids.push(c_id);
            }

            // Commit transaction
            await con.commit();
        } catch (e) {
            await con.rollback();
            throw e;
        } finally {
            con.release();
        }

        const user_inserted = await this.getById(user_id);
        const credentials_inserted = await Promise.all(
            cred_ids.map((id) => this.getCredentialById(id)),
        );

        return {
            user: user_inserted,
            credentials: credentials_inserted,
        };
    }

    /**
     * Adds credentials for a user to the database.
     *
     * @param credentials - An array of credential data objects, with the `user_id` field being optional.
     * @param user_id - The ID of the user to whom the credentials belong.
     * @returns A promise that resolves to an array of the inserted credential data objects.
     * @throws Will throw an error if no credentials are provided or if there is an issue with the database transaction.
     */
    private async addCredentials(
        credentials: PartialBy<CredentialsData, "user_id">[],
        user_id: ID,
    ): Promise<Required<CredentialsData>[]> {
        if (credentials.length == 0) {
            throw new Error("No credentials provided");
        }

        const cred_ids: ID[] = [];

        const con = await pool_permissions.getConnection();
        try {
            // Start transaction
            await con.beginTransaction();

            // Create credentials
            for (const cred of credentials) {
                cred.user_id = user_id;
                const c_id = await con
                    .query(
                        "INSERT INTO credentials (`user_id`, `credential_type`, `email`, `email_verified`, `password_hash`, `provider_id`, `provider_user_id`) VALUES (?, ?, ?, ?, ?, ?, ?)",
                        [
                            cred.user_id,
                            cred.credential_type,
                            cred.email.toLowerCase(),
                            cred.email_verified ? 1 : 0,
                            cred.password_hash,
                            cred.provider_id || null,
                            cred.provider_user_id || null,
                        ],
                    )
                    .then(check_mariadb_warning)
                    .then((res) => {
                        return res.insertId;
                    });

                cred_ids.push(c_id);
            }

            // Commit transaction
            await con.commit();
        } catch (e) {
            await con.rollback();
            throw e;
        } finally {
            con.release();
        }

        const credentials_inserted = await Promise.all(
            cred_ids.map((id) => this.getCredentialById(id)),
        );

        return credentials_inserted;
    }

    /* -------------------------------------------------------------------------- */
    /*                                PASSWORD AUTH                               */
    /* -------------------------------------------------------------------------- */

    /** Signup a new user using an email
     * and password.
     * This requires a email verification step.
     *
     * @param email - The email of the user.
     * @param password - The password of the user.
     */
    public async signupWithPassword(
        email: string,
        password: string,
    ): Promise<UserDataRet> {
        const hashedPassword = await bcrypt.hash(password, 15);

        const { user } = await this.create([
            {
                credential_type: "password",
                email,
                password_hash: hashedPassword,
                email_verified: false,
            },
        ]);

        return user;
    }

    /**
     * Verifies the email of a user. I.e. updates the user's emailValidated field to true.
     *
     * @param email - The email of the user to verify.
     * @returns A Promise that resolves to a boolean indicating whether the email was successfully verified.
     */
    public async verifySignupWithPassword(email: string): Promise<boolean> {
        const sql =
            "UPDATE credentials SET `email_verified` = ? WHERE `email` = ? AND `credential_type` = ?";

        const verified = await pool
            .query(sql, [true, email.toLowerCase(), "password"])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
                return true;
            });

        return verified;
    }

    /** Login with password
     *
     * @param email - The email of the user.
     * @param password - The password of the user.
     * @returns A Promise that resolves to a boolean indicating whether the login was successful.
     */
    public async loginWithPassword(
        email: string,
        password: string,
    ): Promise<UserDataRet> {
        const sql =
            "SELECT `password_hash`, `user_id` FROM credentials WHERE `email` = ? AND `credential_type` = ?";

        const validated = await pool
            .query(sql, [email.toLowerCase(), "password"])
            .then(check_mariadb_warning)
            .then(async (res) => {
                if (res.length != 1) {
                    throw new NotFoundError("email", email);
                }
                const v = await bcrypt.compare(password, res[0].password_hash);
                if (!v) {
                    throw new Error("Invalid password");
                }
                return res[0];
            });

        return await this.getById(validated.user_id);
    }

    /**
     * Changes the password for a user identified by their email.
     *
     * @param email - The email of the user whose password needs to be changed.
     * @param newPassword - The new password to be set for the user.
     * @returns A Promise that resolves to a boolean indicating whether the password was successfully changed.
     * @throws Will throw an error if no rows are affected by the update operation.
     */
    public async changePassword(
        email: string,
        newPassword: string,
    ): Promise<boolean> {
        //Quick check if email exists
        const cred = await pool
            .query(
                "SELECT `id` FROM credentials WHERE `email` = ? AND `credential_type` = ?",
                [email, "password"],
            )
            .then(check_mariadb_warning);

        if (cred.length != 1) {
            throw new NotFoundError("email", email);
        }

        const hashedPassword = await bcrypt.hash(newPassword, 15);

        const sql =
            "UPDATE credentials SET `password_hash` = ? WHERE `email` = ? AND `credential_type` = ?";

        const changed = await pool
            .query(sql, [hashedPassword, email.toLowerCase(), "password"])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
                return true;
            });

        return changed;
    }

    public async changeEmail(
        credential_id: number,
        new_email: string,
    ): Promise<boolean> {
        const sql =
            "UPDATE credentials SET `email` = ? WHERE `id` = ? AND `credential_type` = ?";
        const updated = await pool
            .query(sql, [new_email.toLowerCase(), credential_id, "password"])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
                return true;
            });

        return updated;
    }

    /**
     * Adds a password credential to an already existing user.
     *
     * @param user_id - The ID of the user.
     * @param email - The email of the user.
     * @param password - The password of the user.
     *
     * @returns The ID of the inserted credential.
     * @throws Will throw an error if the credential was not inserted.
     */
    public async addPasswordCredential(
        user_id: ID,
        email: string,
        password: string,
    ): Promise<ID> {
        const hashedPassword = await bcrypt.hash(password, 15);

        const cred = await this.addCredentials(
            [
                {
                    credential_type: "password",
                    email,
                    password_hash: hashedPassword,
                    email_verified: false,
                },
            ],
            user_id,
        );

        if (cred.length != 1) {
            throw new Error("Credential not inserted");
        }
        return cred[0]!.id;
    }

    /* -------------------------------------------------------------------------- */
    /*                                  SSO AUTH                                  */
    /* -------------------------------------------------------------------------- */

    /** Signup a new user using a SSO provider.
     *
     * The SSO provider must provide a unique user id
     * and email must be verified.
     *
     */
    public async signupWithSSO(
        provider_id: string,
        provider_user_id: string,
        email: string,
        email_verified: boolean,
    ): Promise<UserDataRet> {
        if (email_verified !== true) {
            // Temporary?
            throw new Error("Email must be verified for SSO signup");
        }

        const { user } = await this.create([
            {
                credential_type: "sso",
                email,
                email_verified,
                provider_id,
                provider_user_id,
            },
        ]);

        return user;
    }

    /**
     * Logs in a user using an SSO provider.
     *
     * @param provider_id - The ID of the SSO provider.
     * @param provider_user_id - The ID of the user at the provider.
     * @returns A Promise that resolves to the user data if the login is successful.
     * @throws NotFoundError if the provider_user_id is not found.
     */
    public async loginWithSSO(
        provider_id: string,
        provider_user_id: string,
    ): Promise<UserDataRet> {
        const sql =
            "SELECT `user_id` FROM credentials WHERE `provider_id` = ? AND `provider_user_id` = ?";

        const user_id = await pool
            .query(sql, [provider_id, provider_user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError(
                        "provider_user_id",
                        provider_user_id,
                    );
                }
                return res[0].user_id;
            });

        return this.getById(user_id);
    }

    /** Adds a SSO credential to an
     * already existing user.
     *
     * @param user_id - The ID of the user.
     * @param provider_id - The ID of the SSO provider.
     * @param provider_user_id - The ID of the user at the provider.
     * @param email - The email of the user.
     * @param email_verified - Whether the email is verified.
     *
     * @returns The ID of the inserted credential.
     */
    public async addSSOCredential(
        user_id: ID,
        provider_id: string,
        provider_user_id: string,
        email: string,
        email_verified: boolean,
    ): Promise<ID> {
        if (email_verified !== true) {
            // Temporary?
            throw new Error("Email must be verified for SSO signup");
        }

        const cred = await this.addCredentials(
            [
                {
                    credential_type: "sso",
                    email,
                    email_verified,
                    provider_id,
                    provider_user_id,
                },
            ],
            user_id,
        );

        if (cred.length != 1) {
            throw new Error("Credential not inserted");
        }
        return cred[0]!.id;
    }

    /**
     * Retrieves a credential by its ID from the database.
     *
     * @param id - The ID of the credential to retrieve.
     * @returns A Promise that resolves to the credential data if found.
     * @throws NotFoundError if the credential with the given ID is not found.
     */
    private async getCredentialById(
        id: number,
    ): Promise<Required<CredentialsData>> {
        const sql = "SELECT * FROM credentials WHERE `id` = ?";
        const cred = await pool
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("credential_id", id);
                }
                return res[0] as Required<CredentialsData>;
            });

        return cred;
    }

    async setPrimaryCredential(
        user_id: number,
        cred_id: number,
    ): Promise<boolean> {
        const sql =
            "UPDATE users SET `primary_credential_id` = ? WHERE `id` = ?";
        const updated = await pool
            .query(sql, [cred_id, user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
                return true;
            });

        return updated;
    }

    /* -------------------------------------------------------------------------- */
    /*                                 other stuff                                */
    /* -------------------------------------------------------------------------- */

    async mergeUsers(from_id: number, to_id: number): Promise<boolean> {
        const sql = "call merge_users(?, ?)";
        const merged = await pool
            .query(sql, [from_id, to_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return true;
            });

        return merged;
    }

    async getById(id: number): Promise<UserDataRet> {
        const sql = "SELECT * FROM users_with_emails WHERE `id` = ?";
        const user: Required<users_with_emailsData> = await pool
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("user_id", id);
                }
                return res[0];
            });

        return this._parseEmailsRetData(user);
    }

    async getByIds(ids: number[]): Promise<UserDataRet[]> {
        const sql = "SELECT * FROM users_with_emails WHERE `id` IN (?)";
        const users: Required<users_with_emailsData>[] = await pool
            .query(sql, [ids])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        // Parse email string to array
        return users.map(this._parseEmailsRetData);
    }

    async getAll(): Promise<UserDataRet[]> {
        const sql = "SELECT * FROM users_with_emails";
        const users: Required<users_with_emailsData>[] = await pool
            .query(sql)
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        // Parse email string to array
        return users.map(this._parseEmailsRetData);
    }

    async search(search: string): Promise<UserDataRet[]> {
        const sql = `
        SELECT * FROM users_with_emails
            WHERE emails LIKE ? OR
            id LIKE ?
        `;

        const users = await pool
            .query(sql, [`%${search}%`, `%${search}%`])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        return users.map(this._parseEmailsRetData);
    }

    async insert(_data: UserDataInsert): Promise<UserDataRet> {
        throw new Error(
            "Method not implemented. User `signup` functions instead!",
        );
    }
    async update(_data: UserDataInsert & { id: ID }): Promise<UserDataRet> {
        throw new Error("Method not implemented.");
    }
    async delete(_id: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    async isAdmin(user_id: number) {
        const groups = await groupService.getGroupIdsByUserId(user_id);
        return groups.includes(1); //Admin group id = 1
    }

    async addLastLogin(user_id: number, ip: string): Promise<boolean> {
        const sql =
            "INSERT last_logins SET `time` = CURRENT_TIMESTAMP(), `ip` = ?, `user_id` = ?";
        const updated = await pool
            .query(sql, [ip, user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
                return true;
            });
        return updated;
    }

    /**
     * Retrieves the last login records for a user within a specified range of days.
     *
     * @param user_id - The ID of the user whose login records are to be retrieved.
     * @param range - The number of days to look back for login records. Defaults to 31 days.
     * @returns A Promise that resolves to an array of login records.
     */
    async getLastLogins(user_id: number, range = 31) {
        const sql = `
        SELECT * FROM last_logins
            WHERE user_id = ?
            AND time > DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL ? DAY)
        `;
        const logins = await pool
            .query(sql, [user_id, range])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        return logins as LastLoginRet[];
    }

    async getPasswordHash(user_id: number): Promise<string> {
        const sql =
            "SELECT `password_hash` FROM credentials WHERE `user_id` = ? AND `credential_type` = 'password'";
        const res = await pool
            .query(sql, [user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new Error("User not found");
                }
                return res[0].password_hash;
            });

        return res as string;
    }

    async getPasswordCredentialByEmail(
        email: string,
    ): Promise<Required<CredentialsData>> {
        const sql =
            "SELECT * FROM credentials WHERE `email` = ? AND `credential_type` = 'password'";
        const cred: Required<CredentialsData> = await pool
            .query(sql, [email.toLowerCase()])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("email", email);
                }
                return res[0];
            });

        return cred;
    }
    async getPasswordCredentialByUserId(
        user_id: number,
    ): Promise<Required<CredentialsData>> {
        const sql =
            "SELECT * FROM credentials WHERE `user_id` = ? AND `credential_type` = 'password'";
        const cred: Required<CredentialsData> = await pool
            .query(sql, [user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("user_id", user_id);
                }
                return res[0];
            });

        return cred;
    }

    async getByEmail(email: string): Promise<UserDataRet[]> {
        const sql = "SELECT * FROM credentials WHERE `email` = ?";

        const creds = await pool
            .query(sql, [email.toLowerCase()])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        // Unique user_ids
        const ids = new Set<number>();

        for (const c of creds) {
            ids.add(c.user_id);
        }

        // Get user data
        return await this.getByIds(Array.from(ids));
    }

    async getByCredentialId(credential_id: number): Promise<UserDataRet> {
        const sql = "SELECT * FROM credentials WHERE `id` = ?";
        const cred = await pool
            .query(sql, [credential_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("credential_id", credential_id);
                }
                return res[0];
            });

        return this.getById(cred.user_id);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private _parseEmailsRetData(
        data: Required<users_with_emailsData>,
    ): UserDataRet {
        return {
            ...data,
            emails: data.emails?.split(",") || [],
            emails_verified:
                data.emails_verified
                    ?.split(",")
                    .map((e: string) => (e === "1" ? true : false)) || [],
            credential_ids: data.credential_ids?.split(",").map(Number) || [],
            credential_types: data.credential_types?.split(",") || [],
            credential_provider_ids:
                data.credential_provider_ids?.split(",") || [],
        };
    }
}

export function getPrimaryEmail(user: UserDataRet): string | undefined {
    for (let i = 0; i < user.credential_ids.length; i++) {
        if (user.primary_credential_id === user.credential_ids[i]) {
            return user.emails[i];
        }
    }
    return undefined;
}

/** We need to globally register each service here
 * to prevent memory leaks in development.
 */
export const userService = registerService("db_user", () => new UserService());
