import { registerService } from "@snip/common";

import { check_mariadb_warning } from "../errors";
import { pool_data as pool } from "../sql.connection";
import { BackgroundTypeStrategy } from "../strategies";
import { BackgroundTypeData, ID, PartialBy } from "../types";

export class BackgroundService implements BackgroundTypeStrategy {
    /**
     * Get all allowed background types
     */
    async getAll() {
        const sql = "SELECT * FROM background_types";
        const backgroundTypes: BackgroundTypeData[] = await pool
            .query(sql)
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });
        return backgroundTypes;
    }

    async getById(id: number) {
        const sql = "SELECT * FROM background_types WHERE id = ?";
        const backgroundType: BackgroundTypeData = await pool
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw Error(
                        "Number of background types returned not equal to 1, got " +
                            res.length,
                    );
                }
                return res[0];
            });
        return backgroundType;
    }

    async getByIds(ids: number[]) {
        const sql = "SELECT * FROM background_types WHERE id IN (?)";
        const backgroundTypes: BackgroundTypeData[] = await pool
            .query(sql, [ids])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != ids.length) {
                    throw Error(
                        "Number of background types returned not equal to number of requested background types, got " +
                            res.length,
                    );
                }
                return res;
            });
        return backgroundTypes;
    }

    async getSelectedByBookId(book_id: ID) {
        const sql =
            "SELECT default_background_type_id FROM books_resolved WHERE id = ?";
        const backgroundType: ID = await pool
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw Error(
                        "Number of background types returned not equal to 1, got " +
                            res.length,
                    );
                }
                return res[0].default_background_type_id;
            });
        return backgroundType;
    }

    async setBookDefault(book_id: number, background_type_id: number) {
        const sql =
            "UPDATE books SET default_background_type_id = ? WHERE id = ?";
        await pool
            .query(sql, [background_type_id, book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
            });
        return true as const;
    }

    // We add book id here for security reasons
    async setPageById(
        book_id: number,
        page_id: number,
        background_type_id: number,
    ) {
        const sql =
            "UPDATE pages SET background_type_id = ? WHERE book_id = ? AND id = ?";
        await pool
            .query(sql, [background_type_id, book_id, page_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
            });
        return true as const;
    }

    async setPageByIds(
        book_id: number,
        page_ids: number[],
        background_type_id: number,
    ) {
        const sql =
            "UPDATE pages SET background_type_id = ? WHERE book_id = ? AND id IN (?)";
        await pool
            .query(sql, [background_type_id, book_id, page_ids])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
                if (res.affectedRows != page_ids.length) {
                    throw new Error(
                        "Number of pages updated not equal to number of requested pages, got " +
                            res.affectedRows,
                    );
                }
            });
        return true as const;
    }

    async setPageByNumber(
        book_id: number,
        page_number: number,
        background_type_id: number,
    ) {
        const sql =
            "UPDATE pages SET background_type_id = ? WHERE book_id = ? AND page_number = ?";
        await pool
            .query(sql, [background_type_id, book_id, page_number])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
            });
        return true as const;
    }

    async setPageByNumbers(
        book_id: number,
        page_numbers: number[],
        background_type_id: number,
    ) {
        const sql =
            "UPDATE pages SET background_type_id = ? WHERE book_id = ? AND page_number IN (?)";
        await pool
            .query(sql, [background_type_id, book_id, page_numbers])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
                if (res.affectedRows != page_numbers.length) {
                    throw new Error(
                        "Number of pages updated not equal to number of requested pages, got " +
                            res.affectedRows,
                    );
                }
            });
        return true as const;
    }

    async delete(_id: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    async insert(
        _data: PartialBy<BackgroundTypeData, "id">,
    ): Promise<BackgroundTypeData> {
        throw new Error("Method not implemented.");
    }

    async update(
        _data: Partial<BackgroundTypeData>,
    ): Promise<BackgroundTypeData> {
        throw new Error("Method not implemented.");
    }

    async search(_query: string): Promise<BackgroundTypeData[]> {
        throw new Error("Method not implemented.");
    }
}

export const backgroundTypeService = registerService(
    "db_backgroundType",
    () => new BackgroundService(),
);
