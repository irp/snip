import { registerService } from "@snip/common";
import { randomHexFromId } from "@snip/common/random";

import { check_mariadb_warning, NotFoundError } from "../errors";
import { pool_permissions as pool } from "../sql.connection";
import { user_configData } from "../sql.typings";
import { UserConfigStrategy } from "../strategies";
import { ID, UserConfigDataInsert, UserConfigDataRet } from "../types";
export class UserConfigService implements UserConfigStrategy {
    /** Gets the `text_font` as an id in the database.
     *
     * I.e. removes text_font from the data and adds text_font_id
     * if text_font is present.
     */
    private async fontToId(
        data: Partial<UserConfigDataInsert>,
    ): Promise<user_configData> {
        if (data.text_font !== undefined) {
            const font_id = await pool
                .query("SELECT id FROM fonts WHERE name = ?", [data.text_font])
                .then(check_mariadb_warning)
                .then((res) => {
                    if (res.length != 1) {
                        throw new Error(
                            "Font not found for name: " + data.text_font,
                        );
                    }
                    return res[0].id;
                });

            data.text_font_id = font_id;
            // Remove text_font from data
            delete data.text_font;
        }

        return data as user_configData;
    }

    // keys of configData
    allowedFields = [
        "user_id",
        "text_font_id",
        "text_fontSize",
        "text_fontColor",
        "text_lineHeight",
        "text_lineWrap",
        "pen_color",
        "user_color",
        "pen_colors",
        "pen_size",
        "pen_smoothing",
        "zoom1",
        "zoom2",
    ];

    private getFieldsAndValues(
        object: user_configData,
    ): [unknown[], unknown[]] {
        if (Object.keys(object).length == 0) {
            throw new Error("No fields to update");
        }
        const field_names = Object.keys(object).filter((key) =>
            this.allowedFields.includes(key),
        );
        const field_values = field_names.map(
            (key) => object[key as keyof user_configData],
        );

        //special case for pen_colors
        if (field_names.includes("pen_colors")) {
            field_values[field_names.indexOf("pen_colors")] = JSON.stringify(
                field_values[field_names.indexOf("pen_colors")],
            );
        }

        return [field_names, field_values];
    }

    /**
     * Inserts a new user configuration into the database.
     *
     * This method first resolves the `text_font` to `text_font_id` using the `fontToId` method.
     * It then constructs the SQL query to insert the resolved data into the `user_config` table.
     * After successfully inserting the data, it retrieves the newly inserted configuration by its ID.
     *
     * @param data - The user configuration data to insert.
     * @returns A promise that resolves to the newly inserted user configuration data.
     */
    async insert(data: UserConfigDataInsert): Promise<UserConfigDataRet> {
        // Resolve text_font to text_font_id and create sql
        const withId = await this.fontToId(data);

        // Create query dynamically
        const [field_names, vals] = this.getFieldsAndValues(withId);
        const sql =
            "INSERT INTO user_config (" +
            field_names.join(",") +
            ") VALUES (" +
            vals.map(() => "?").join(",") +
            ")";
        const configData_id = await pool
            .query(sql, vals)
            .then(check_mariadb_warning)
            .then((res) => {
                return res.insertId;
            });

        return await this.getById(configData_id);
    }

    /**
     * Updates a user configuration using partial data.
     * @param data The partial data to update, including the ID of the user configuration.
     * @returns A promise that resolves to the updated user configuration data.
     */
    async update(
        data: Partial<UserConfigDataInsert> & { id: ID },
    ): Promise<UserConfigDataRet> {
        if (data.id === undefined) {
            throw new Error("ID is required for updating user config");
        }
        const withId = await this.fontToId(data);

        // Create query dynamically
        const [field_names, vals] = this.getFieldsAndValues(withId);
        const sql = `UPDATE user_config SET ${field_names.map(
            (key) => `${key} = ?`,
        )} WHERE id = ?`;

        const configData_id = await pool
            .query(sql, [...vals, data.id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows != 1) {
                    throw new Error("Config not found for id:" + withId.id);
                }
                return data.id;
            });

        return await this.getById(configData_id);
    }

    /**
     * Deletes a user configuration by its ID.
     *
     * This method constructs a SQL query to delete the configuration from the `user_config` table.
     * After successfully deleting the configuration, it returns a boolean indicating the success of the operation.
     * If the configuration with the given ID is not found, it throws an error.
     *
     * @param id - The ID of the user configuration to delete.
     * @returns A promise that resolves to a boolean indicating whether the deletion was successful.
     * @throws {Error} If the configuration with the given ID is not found.
     */
    async delete(id: ID): Promise<boolean> {
        const sql = "DELETE FROM user_config WHERE id = ?";
        const deleted = await pool
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows != 1) {
                    throw new NotFoundError("user_config", id);
                }
                return true;
            });

        return deleted;
    }

    /**
     * Updates the user configuration data for a specific user ID.
     *
     * @param user_id - The ID of the user.
     * @param configUpd - The partial user configuration data to update.
     * @returns A promise that resolves to a boolean indicating whether the update was successful.
     */
    async updateByUserId(
        data: UserConfigDataInsert & { user_id: ID },
    ): Promise<UserConfigDataRet> {
        // Remove user_id from data
        const user_id = data.user_id;

        // Params for insert
        const non_resolved = await this.fontToId(data);
        const [field_names, vals] = this.getFieldsAndValues(non_resolved);

        //Remove user_id from field_names
        const idx = field_names.indexOf("user_id");
        if (idx == -1) {
            throw new Error("user_id is required for updating user config");
        }
        field_names.splice(idx, 1);
        vals.splice(idx, 1);

        const sql = `UPDATE user_config SET ${field_names.map(
            (key) => `${key} = ?`,
        )} WHERE user_id = ?`;

        const configData = await pool
            .query(sql, [...vals, user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows != 1) {
                    throw new Error(
                        "Config not found for user_id: " + data.user_id,
                    );
                }
            });

        return await this.getByUserId(user_id);
    }

    /* -------------------------------------------------------------------------- */
    /*                                   QUERIES                                  */
    /* -------------------------------------------------------------------------- */

    /**
     * Retrieves all user configuration data from the database.
     * @returns A promise that resolves to an array of UserConfigData objects.
     */
    async getAll(): Promise<UserConfigDataRet[]> {
        const sql = "SELECT * FROM user_config";
        const configData: UserConfigDataRet[] = await pool
            .query(sql)
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        return configData;
    }

    /**
     * Retrieves a user configuration by its ID.
     * @param id The ID of the user configuration.
     * @returns A promise that resolves to the user configuration data.
     */
    async getById(id: ID): Promise<UserConfigDataRet> {
        const sql = "SELECT * FROM user_config_resolved WHERE id = ?";
        const configData = await pool
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("user_config", id);
                }
                // As the database structure matches our UserConfigData interface
                return res[0];
            });

        return configData;
    }

    async getByIds(ids: ID[]): Promise<UserConfigDataRet[]> {
        throw new Error("Method not implemented.");
    }

    /**
     * Retrieves the user configuration data by user ID.
     * If the user ID is not provided or is -1, it returns the default configuration.
     * If the configuration is not found for the given user ID, it throws an error.
     *
     * @param user_id - The ID of the user.
     * @returns A promise that resolves to the user configuration data.
     */
    async getByUserId(user_id: ID): Promise<UserConfigDataRet> {
        const sql = "SELECT * FROM user_config_resolved WHERE user_id = ?";
        const configData = await pool
            .query(sql, [user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new Error("Config not found for user_id: " + user_id);
                }
                // As the database structure matches our UserConfigData interface
                return res[0];
            });

        // Create color if user_color is null by user_id hash
        if (configData.user_color === null) {
            configData.user_color = randomHexFromId(user_id);
        }

        return configData;
    }

    async search(query: string): Promise<UserConfigDataRet[]> {
        throw new Error("Method not implemented.");
    }
}

/** We need to globally register each service here
 * to prevent memory leaks in development.
 */
export const userConfigService = registerService(
    "db_userconfig",
    () => new UserConfigService(),
);
