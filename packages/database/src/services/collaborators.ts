import { SqlError } from "mariadb";

import { registerService } from "@snip/common";

import {
    catch_db_signals,
    check_mariadb_warning,
    NotFoundError,
} from "../errors";
import { pool_permissions } from "../sql.connection";
import {
    CollaborationInviteDataInsert,
    CollaborationInviteDataRet,
    ENTITY_USER,
    ID,
    RESOURCE_BOOK,
} from "../types";

/** Collaborators service allows to
 * add new collaborators or move
 * them from invited to
 * accepted.
 */
export class CollaboratorsInviteService {
    /**
     * Retrieves a collaboration invite by its ID.
     * @param id - The ID of the collaboration invite.
     * @returns A promise that resolves to the collaboration invite data.
     * @throws {NotFoundError} If the collaboration invite with the specified ID is not found.
     */
    async getById(id: ID): Promise<CollaborationInviteDataRet> {
        const sql = `SELECT * FROM collaboration_invites_with_emails WHERE id = ?`;

        const d = await pool_permissions
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length === 0) {
                    throw new NotFoundError("Collaboration Invite", id);
                }
                return res[0];
            });

        return this._parseEmailsRetData(d);
    }

    /**
     * Retrieves collaboration invites by book ID.
     * @param book_id - The ID of the book.
     * @returns A promise that resolves to an array of collaboration invite data.
     */
    async getByBookId(book_id: ID): Promise<CollaborationInviteDataRet[]> {
        const sql = `SELECT * FROM collaboration_invites_with_emails WHERE book_id = ?`;

        const d = await pool_permissions
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });
        return d.map(this._parseEmailsRetData);
    }

    async getPendingByBookId(
        book_id: ID,
    ): Promise<CollaborationInviteDataRet[]> {
        const sql = `SELECT * FROM collaboration_invites_with_emails WHERE book_id = ? AND accepted_at IS NULL`;

        const d = await pool_permissions
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });
        return d.map(this._parseEmailsRetData);
    }

    async getPendingByUserId(
        user_id: ID,
    ): Promise<CollaborationInviteDataRet[]> {
        const sql = `SELECT * FROM collaboration_invites_with_emails WHERE user_id = ? AND accepted_at IS NULL`;

        const d = await pool_permissions
            .query(sql, [user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });
        return d.map(this._parseEmailsRetData);
    }

    async getPendingByBookIdAndUserId(
        book_id: ID,
        user_id: ID,
    ): Promise<CollaborationInviteDataRet[]> {
        const sql = `SELECT * FROM collaboration_invites_with_emails WHERE book_id = ? AND user_id = ? AND accepted_at IS NULL`;

        const d = await pool_permissions
            .query(sql, [book_id, user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });
        return d.map(this._parseEmailsRetData);
    }

    /**
     * Retrieves collaboration invite data for a given user ID.
     * @param user_id - The ID of the user.
     * @returns A promise that resolves to an array of collaboration invite data.
     */
    async getByUserId(user_id: ID): Promise<CollaborationInviteDataRet[]> {
        const sql = `SELECT * FROM collaboration_invites_with_emails WHERE user_id = ?`;

        const d = await pool_permissions
            .query(sql, [user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });
        return d.map(this._parseEmailsRetData);
    }

    /**
     * Inserts a collaboration invite into the database.
     *
     * @param colab - The collaboration invite data to be inserted.
     * @returns A promise that resolves to the inserted collaboration invite data.
     */
    async insert(
        colab: CollaborationInviteDataInsert,
    ): Promise<CollaborationInviteDataRet> {
        const sql = `INSERT INTO collaboration_invites (book_id, user_id, invited_by, expire_at) VALUES (?, ?, ?, ?)`;

        const in_id = await pool_permissions
            .query(sql, [
                colab.book_id,
                colab.user_id,
                colab.invited_by,
                colab.expire_at,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.insertId;
            });

        return await this.getById(in_id);
    }

    /*
     * Deletes a collaboration invite from the database.
     *
     * @param id - The ID of the collaboration invite.
     */
    async delete(id: ID): Promise<boolean> {
        const sql = `DELETE FROM collaboration_invites WHERE id = ?`;

        return await pool_permissions
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Delete failed, no rows affected");
                }
                return true;
            });
    }

    /**
     * Accepts an invite for a specific book and user.
     * This also updates the acl table to give the user access to the book.
     *
     * @param book_id - The ID of the book.
     * @param user_id - The ID of the user.
     * @throws {ExpiredError} - If the invite has expired.
     */
    async acceptById(id: ID): Promise<CollaborationInviteDataRet> {
        const con = await pool_permissions.getConnection();
        try {
            //Begin transaction
            await con.beginTransaction();

            // Update invite
            // This might thow an error if the invite has expired
            // or if the invite has already been accepted
            await con
                .query(`CALL update_invite_status(?)`, [id])
                .then(check_mariadb_warning);

            // Add user to acl table
            await con
                .query(
                    `INSERT INTO acl (entity_id, entity_type, resource_id, resource_type, pRead, pWrite, pDelete, pAcl)
                    SELECT user_id, ${ENTITY_USER}, book_id, ${RESOURCE_BOOK}, 1 , 1, 0, 0
                    FROM collaboration_invites 
                    WHERE collaboration_invites.id = ?
                    ON DUPLICATE KEY UPDATE entity_type = VALUES(entity_type);`,
                    [id],
                )
                .then(check_mariadb_warning);
            await con.commit();
        } catch (e) {
            await con.rollback();
            // This reraises the error!
            catch_db_signals(e as SqlError);
        } finally {
            await con.release();
        }
        return await this.getById(id);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private _parseEmailsRetData(data: any): CollaborationInviteDataRet {
        data.emails = data.emails.split(",");
        data.invited_by_emails = data.invited_by_emails.split(",");
        data.emails_verified =
            data.emails_verified
                ?.split(",")
                .map((e: string) => (e === "1" ? true : false)) || [];
        data.invited_by_emails_verified =
            data.invited_by_emails_verified
                ?.split(",")
                .map((e: string) => (e === "1" ? true : false)) || [];

        data.credential_ids = data.credential_ids.split(",").map(Number);
        data.invited_by_credential_ids = data.invited_by_credential_ids
            .split(",")
            .map(Number);
        data.credential_types = data.credential_types.split(",");
        data.invited_by_credential_types =
            data.invited_by_credential_types.split(",");

        return data;
    }
}

/** We need to globally register each service here
 * to prevent memory leaks in development.
 */
export const collaboratorsInviteService = registerService(
    "db_collabInvite",
    () => new CollaboratorsInviteService(),
);
