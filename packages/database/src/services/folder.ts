import { registerService } from "@snip/common";

import { check_mariadb_warning, NotFoundError } from "../errors";
import { pool_data as pool, pool_data } from "../sql.connection";
import { FolderStrategy } from "../strategies";
import {
    FileData,
    FolderDataInsert,
    FolderDataRet,
    ID,
    RESOURCE_BOOK,
} from "../types";
import { permissionService } from "./permission";

export class FolderService implements FolderStrategy {
    async getAll(): Promise<FolderDataRet[]> {
        throw new Error("Method not implemented.");
    }
    async getById(id: number): Promise<FolderDataRet> {
        const sql = "SELECT * FROM browserFolders WHERE id = ?";

        const folder = await pool_data
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("folder", id);
                }
                return res[0];
            });
        return folder;
    }
    async getByIds(ids: number[]): Promise<FolderDataRet[]> {
        throw new Error("Method not implemented.");
    }
    async update(
        data: Partial<FolderDataInsert> & { id: ID },
    ): Promise<FolderDataRet> {
        throw new Error("Method not implemented.");
    }
    async search(query: string): Promise<FolderDataRet[]> {
        throw new Error("Method not implemented.");
    }

    /**
     * Creates a new folder.
     *
     * @param user_id - The ID of the user creating the folder.
     * @param name - The name of the folder to create.
     * @param parentId - The ID of the parent folder to create the new folder in.
     * @param comment - A comment to attach to the new folder.
     * @returns A Promise that resolves to the ID of the newly created folder.
     */
    async insert(folder: FolderDataInsert): Promise<FolderDataRet> {
        // Validate folder name
        await validateName(folder.name, folder.user_id);
        if (folder.parent_id)
            await validateItem("folder", folder.parent_id, folder.user_id);

        // Create folder
        const sql = `INSERT INTO browserFolders (name, parent_id, user_id, comment) VALUES (?, ?, ?, ?)`;
        const folder_id: ID = await pool_data
            .query(sql, [
                folder.name,
                folder.parent_id || null,
                folder.user_id,
                folder.comment,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.insertId;
            });

        return await this.getById(folder_id);
    }

    async delete(id: ID, user_id?: ID): Promise<boolean> {
        // Validate folder
        if (user_id) {
            await validateItem("folder", id, user_id);
        }

        // Move first
        const sql_folders = `
            UPDATE browserFolders SET parent_id = (
                SELECT parent_id FROM browserFolders WHERE id = ?
            ) WHERE parent_id = ?;
        `;
        const sql_books_del_null = `
            DELETE FROM browserFolders_books
            WHERE browserFolder_id = ?
            AND (
                SELECT parent_id FROM browserFolders WHERE id = ?
            ) IS NULL;
        `;
        const sql_books = `
            UPDATE browserFolders_books SET browserFolder_id = (
                SELECT parent_id FROM browserFolders WHERE id = ?
            ) WHERE browserFolder_id = ?;
        `;
        await pool_data.query(sql_folders, [id, id]);
        await pool_data.query(sql_books_del_null, [id, id]);
        await pool_data.query(sql_books, [id, id]);

        // Delete folder
        const sql2 = `
        DELETE FROM browserFolders WHERE id = ?;
    `;
        await pool_data.query(sql2, [id]);

        return true;
    }

    /**
     * Moves a folder or a book to a new location.
     *
     * @param type - The type of the item to move. Must be either "folder" or "book".
     * @param item - The ID of the item to move.
     * @param moveTo - The ID of the destination folder to move the item to, or "root" to move it to the root level.
     * @param user_id - The ID of the user performing the move operation.
     * @returns A Promise that resolves to `true` if the move operation is successful.
     * @throws An error if the type is invalid.
     */
    async move(
        type: "folder" | "book",
        item: ID,
        moveTo: ID | "root",
        user_id: ID,
    ): Promise<true> {
        //Validate item and moveTo
        await validateItem(type, item, user_id);
        if (moveTo !== "root") await validateItem(type, moveTo, user_id);

        // Move item
        if (moveTo === "root") {
            //@ts-expect-error needs null for insert
            moveTo = null;
        }
        if (type === "folder") {
            const sql = "UPDATE browserFolders SET parent_id = ? WHERE id = ?";
            await pool_data.query(sql, [moveTo, item]);
            return true;
        } else if (type === "book" && moveTo !== null) {
            const sql =
                "INSERT INTO browserFolders_books (user_id, browserFolder_id, book_id) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE browserFolder_id = ?";
            await pool_data.query(sql, [user_id, moveTo, item, moveTo]);
            return true;
        } else if (type === "book" && moveTo === null) {
            const sql =
                "DELETE FROM browserFolders_books WHERE user_id = ? AND book_id = ?";
            await pool_data.query(sql, [user_id, item]);
            return true;
        } else {
            throw new Error(`Invalid type, must be 'folder' or 'book'.`);
        }
    }

    /**
     * Retrieves the files associated with a given user ID.
     *
     * @param user_id - The ID of the user.
     * @returns A promise that resolves to an array of FileData objects representing the files associated with the user.
     */
    async getFilesByUserId(user_id: ID): Promise<FileData[]> {
        //Get books for user, i.e. all books the user has access to
        const acl = await permissionService.getByEntity(user_id);
        const books_with_pRead = acl.filter(
            (a) => a.resource_type == RESOURCE_BOOK && a.pRead,
        );
        const book_ids = books_with_pRead.map((a) => a.resource_id);
        if (book_ids.length == 0) {
            return [];
        }

        // Get book and folder data
        const sql = `
            SELECT 
                CAST(B.id AS CHAR) as id,
                B.title as name,
                B.comment,
            
                false as isDir,
                CASE
                    WHEN BF.browserFolder_id IS NOT NULL THEN CONCAT('folder_', CAST(BF.browserFolder_id AS CHAR))
                    ELSE NULL
                END AS parentId,
    
                getNumPages(B.id) as numPages,
                B.created,
                B.last_updated,
                B.finished,
                B.owner_name as ownerName,
                B.owner_id as ownerID
            FROM books_resolved as B
            LEFT JOIN browserFolders_books as BF ON BF.book_id = B.id AND BF.user_id = ? WHERE B.id IN (?) 
            UNION
            SELECT
                CONCAT('folder_', CAST(F.id AS CHAR)) as id,
                F.name,
                F.comment,
                true as isDir,
                F.parent_id as parentId,
    
                NULL as numPages,
                F.created,
                NULL as last_updated,
                NULL as finished,
                U.primary_email as ownerName,
                U.id as ownerID
            FROM browserFolders as F
            LEFT JOIN snip_perms.users_with_primary_email as U ON U.id = F.user_id
            WHERE F.user_id = ?;
        `;

        // Attach acl to books
        const file_data = await pool
            .query(sql, [user_id, book_ids, user_id])
            .then(check_mariadb_warning)
            .then((r) => r as FileData[]);

        // Attach acl to books
        const file_data_with_acl = file_data.map((f) => {
            const acl = books_with_pRead.find(
                (a) => a.resource_id == Number(f.id),
            );
            if (acl) {
                f.perms = acl;
            }

            if (f.ownerID == user_id) {
                f.perms = {
                    ...f.perms!,
                    pRead: true,
                    pWrite: true,
                    pDelete: true,
                    pACL: true,
                };
            }

            return f;
        });

        return file_data_with_acl;
    }
}

export const folderService = registerService(
    "db_folder",
    () => new FolderService(),
);

export async function validateItem(
    type: "folder" | "book",
    item: ID,
    user_id: ID,
) {
    if (type === "folder") {
        const sql = "SELECT 1 FROM browserFolders WHERE id = ? AND user_id = ?";
        const folder = await pool_data
            .query(sql, [item, user_id])
            .then(check_mariadb_warning);
        if (!folder) {
            throw new Error(`Folder does not exist`);
        }
        return;
    } else if (type === "book") {
        const sql = "SELECT 1 FROM books WHERE id = ?";
        const book = await pool_data
            .query(sql, [item])
            .then(check_mariadb_warning);
        if (!book) {
            throw new Error(`Book does not exist`);
        }
        return;
    } else {
        throw new Error(`Invalid type, must be 'folder' or 'book'.`);
    }
}

export async function validateName(
    name: string,
    user_id: number,
): Promise<true> {
    // Check if name is empty
    if (!name || name.length === 0) {
        throw new Error("Folder name is not set!");
    }
    if (name.length > 2048) {
        throw new Error("Folder name is too long, < 2048 chars!");
    }
    // Check if name exists
    const sql = `SELECT 1 FROM browserFolders WHERE name = ? AND user_id = ?`;
    const folder = await pool_data
        .query(sql, [name, user_id])
        .then((result) => {
            return result.length > 0;
        });

    if (folder) {
        throw new Error("Folder name already exists, choose another name!");
    }

    return true;
}
