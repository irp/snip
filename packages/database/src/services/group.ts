import { registerService } from "@snip/common";

import { users_groups_with_emailsData } from "src/sql.typings";

import { check_mariadb_warning, NotFoundError } from "../errors";
import { pool_permissions as pool } from "../sql.connection";
import { GroupStrategy } from "../strategies";
import {
    GroupDataInsert,
    GroupDataRet,
    ID,
    MemberDataRet,
    MemberRole,
} from "../types";
import { permissionService } from "./permission";

export class GroupService implements GroupStrategy {
    /**
     * Inserts a new group into the database.
     *
     * @param group - The group data to be inserted.
     * @returns A promise that resolves to the inserted group data.
     */
    async insert(group: GroupDataInsert): Promise<GroupDataRet> {
        const sql = `INSERT INTO groups 
                (name, description) VALUES 
                (?, ?)`;

        const group_id: ID = await pool
            .query(sql, [group.name, group.description])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.insertId;
            });

        return await this.getById(group_id);
    }

    /**
     * Updates a group in the database.
     *
     * @param group - The updated group data.
     * @param id - The ID of the group to update.
     * @returns A Promise that resolves to the updated group data.
     */
    async update(
        group: Partial<GroupDataInsert> & { id: ID },
    ): Promise<GroupDataRet> {
        if (
            group.id == undefined ||
            group.description == undefined ||
            group.name == undefined
        ) {
            //TODO: add dynamic updates
            throw new Error("Missing required fields for update");
        }
        const sql =
            "UPDATE `groups` SET name = ?, description = ? WHERE id = ?";

        await pool
            .query(sql, [group.name, group.description, group.id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
            });

        return await this.getById(group.id);
    }

    /**
     * Deletes a group from the database.
     * @param id - The ID of the group to delete.
     * @returns A promise that resolves to a boolean indicating whether the delete operation was successful.
     */
    async delete(id: number): Promise<boolean> {
        // Delete all acl entries for this group

        const actEntries = await permissionService.getByEntity(id, "group");
        await Promise.all(
            actEntries.map((entry) => permissionService.delete(entry.id)),
        );

        const sql = "DELETE FROM `groups` WHERE id = ?";

        const success = await pool
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Delete failed, no rows affected");
                }
                return true;
            });

        return success;
    }

    /* -------------------------------------------------------------------------- */
    /*                             QUERIES i.e. getBY                             */
    /* -------------------------------------------------------------------------- */

    /**
     * Retrieves a group by its ID.
     *
     * @param id - The ID of the group to retrieve.
     * @returns A promise that resolves to the group data.
     * @throws An error if the group is not found.
     */
    async getById(id: ID): Promise<GroupDataRet> {
        const sql = "SELECT * FROM groups WHERE id = ?";

        const groupData = await pool
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("group", id);
                }
                return res[0];
            });

        return groupData as GroupDataRet;
    }

    /**
     * Retrieves group data by their IDs.
     * @param ids - An array of group IDs.
     * @returns A promise that resolves to an array of GroupData objects.
     */
    async getByIds(ids: ID[]): Promise<GroupDataRet[]> {
        const sql = "SELECT * FROM groups WHERE id IN (?)";

        const groups = await pool
            .query(sql, [ids])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != ids.length) {
                    throw new NotFoundError("group", ids);
                }
                return res;
            });

        return groups as GroupDataRet[];
    }

    /**
     * Retrieves all groups associated with a given user ID.
     *
     * @param user_id - The ID of the user.
     * @returns A promise that resolves to an array of group data.
     */
    async getByUserId(user_id: ID): Promise<GroupDataRet[]> {
        const sql = `SELECT g.* FROM groups g
        JOIN users_groups ug ON g.id = ug.group_id
        WHERE ug.user_id = ?`;

        const groups: GroupDataRet[] = await pool
            .query(sql, [user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        return groups as GroupDataRet[];
    }

    /**
     * Retrieves all group IDs associated with a given user ID.
     *
     * @param user_id - The ID of the user.
     * @returns A promise that resolves to an array of group IDs.
     */
    async getGroupIdsByUserId(user_id: ID): Promise<ID[]> {
        // Use junction table to get groups of user
        const sql = "SELECT group_id FROM users_groups WHERE user_id = ?";

        const group_ids: ID[] = await pool
            .query(sql, [user_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.map((row: { group_id: number }) => row.group_id);
            });
        return group_ids;
    }

    async search(query: string): Promise<GroupDataRet[]> {
        const sql =
            "SELECT * FROM `groups` WHERE name LIKE ? OR description LIKE ?";

        const groups: GroupDataRet[] = await pool
            .query(sql, [`%${query}%`, `%${query}%`])
            .then(check_mariadb_warning);

        return groups;
    }

    async getAll(): Promise<GroupDataRet[]> {
        throw new Error("Method not implemented.");
    }

    /* -------------------------------------------------------------------------- */
    /*                                group members                               */
    /* -------------------------------------------------------------------------- */
    // This is the many-to-many relationship between users and groups
    // In the database the table is called users_groups_with_email

    async getMember(user_id: ID, group_id: ID): Promise<MemberDataRet> {
        const sql =
            "SELECT * FROM users_groups_with_emails WHERE user_id = ? AND group_id = ?";

        const member = await pool
            .query(sql, [user_id, group_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("group member", [
                        user_id,
                        group_id,
                    ]);
                }
                return res[0];
            });

        return this._parseMemberRetData(member);
    }

    private _parseMemberRetData(
        data: Required<users_groups_with_emailsData>,
    ): MemberDataRet {
        (data as unknown as MemberDataRet).emails =
            data.emails?.split(",") ?? [];
        (data as unknown as MemberDataRet).emails_verified =
            data.emails_verified?.split(",").map((v) => v == "1") ?? [];
        (data as unknown as MemberDataRet).credential_ids =
            data.credential_ids?.split(",").map(Number) ?? [];
        (data as unknown as MemberDataRet).credential_types =
            data.credential_types?.split(",") ?? [];

        return data as unknown as MemberDataRet;
    }

    /**
     * Retrieves the members of a group.
     *
     * @param group_id - The ID of the group.
     * @returns A promise that resolves to an array of group member data.
     */
    async getMembers(group_id: ID): Promise<MemberDataRet[]> {
        const sql = "SELECT * FROM users_groups_with_emails WHERE group_id = ?";

        const members = await pool
            .query(sql, [group_id])
            .then(check_mariadb_warning);

        return members.map(this._parseMemberRetData);
    }

    /** Add member to group
     * @param user_id - The ID of the user to add.
     * @param group_id - The ID of the group to add the user to.
     * @param role - The role of the user in the group.
     */
    async addMember(
        user_id: ID,
        group_id: ID,
        role: MemberRole = "invited",
    ): Promise<MemberDataRet> {
        const sql = `
            INSERT INTO users_groups (user_id, group_id, role) 
            VALUES (?, ?, ?) 
            ON DUPLICATE KEY UPDATE 
                role = IF(role = 'member', VALUES(role), ?), 
                joined_at = CASE
                    WHEN VALUES(role) = 'member' THEN NOW()
                    ELSE joined_at
                END
        `;

        return await pool
            .query(sql, [user_id, group_id, role, role])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Insert failed, no rows affected");
                }
            })
            .then(async () => {
                return await this.getMember(user_id, group_id);
            });
    }

    async updateMember(
        user_id: ID,
        group_id: ID,
        role: MemberRole,
    ): Promise<MemberDataRet> {
        const sql =
            "UPDATE users_groups SET role = ? WHERE user_id = ? AND group_id = ?";

        return await pool
            .query(sql, [role, user_id, group_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new NotFoundError("group member", [
                        user_id,
                        group_id,
                    ]);
                }
            })
            .then(() => {
                return this.getMember(user_id, group_id);
            });
    }

    async removeMember(user_id: ID, group_id: ID): Promise<boolean> {
        const sql =
            "DELETE FROM users_groups WHERE user_id = ? AND group_id = ?";

        return await pool
            .query(sql, [user_id, group_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new NotFoundError("group member", [
                        user_id,
                        group_id,
                    ]);
                }
                return true;
            });
    }

    /**
     * Checks if a user is allowed to edit a group.
     *
     * @param user_id - The ID of the user.
     * @param group_id - The ID of the group.
     * @returns A Promise that resolves to a boolean indicating whether the user is allowed to edit the group.
     */
    async isAllowedToEdit(user_id: ID, group_id: ID): Promise<boolean> {
        try {
            const member = await this.getMember(user_id, group_id);
            return member.role == "owner" || member.role == "moderator";
        } catch (error) {
            if (error instanceof NotFoundError) {
                return false;
            }
            throw error;
        }
    }

    /**
     * Checks if a user is a member of a group.
     *
     * member here entails any role! including invited
     *
     * @param user_id - The ID of the user.
     * @param group_id - The ID of the group.
     * @returns A Promise that resolves to the role of the user in the group. Undefined if the user is not a member.
     */
    async getRole(user_id: ID, group_id: ID): Promise<MemberRole | undefined> {
        const sql =
            "SELECT role FROM users_groups_with_emails WHERE user_id = ? AND group_id = ?";
        return await pool
            .query(sql, [user_id, group_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    return undefined;
                }
                return res[0].role;
            });
    }
}

/** We need to globally register each service here
 * to prevent memory leaks in development.
 */
export const groupService = registerService(
    "db_group",
    () => new GroupService(),
);
