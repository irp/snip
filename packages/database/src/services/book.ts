import { registerService } from "@snip/common";

import { check_mariadb_warning, NotFoundError } from "../errors";
import { pool_data as pool, pool_permissions } from "../sql.connection";
import { BookStrategy } from "../strategies";
import {
    BookDataInsert,
    BookDataRet,
    BookOwner,
    Entity,
    ID,
    PartialBy,
    RESOURCE_BOOK,
} from "../types";
import { permissionService } from "./permission";

export class BookService implements BookStrategy {
    /**
     * Inserts a new book into the database.
     *
     * @param bookData - The data of the book to be inserted.
     * @returns A Promise that resolves to the inserted book data.
     */
    async insert(bookData: Partial<BookDataInsert>): Promise<BookDataRet> {
        const sql = `
        INSERT INTO books 
            (title, comment, owner_user_id, owner_group_id, cover_page_id ) 
        VALUES 
            (?, ?, ?, ?, ?);
    `;
        const bookData_id: ID = await pool
            .query(sql, [
                bookData.title,
                bookData.comment ? bookData.comment : null,
                bookData.owner_user_id ? bookData.owner_user_id : null,
                bookData.owner_group_id ? bookData.owner_group_id : null,
                bookData.cover_page_id ? bookData.cover_page_id : null,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.insertId;
            });

        return await this.getById(bookData_id);
    }

    /* -------------------------------------------------------------------------- */
    /*                                   queries                                  */
    /* -------------------------------------------------------------------------- */

    /**
     * Retrieves all books from the database.
     * @returns A promise that resolves to an array of BookData objects.
     */
    async getAll(): Promise<BookDataRet[]> {
        const sql = "SELECT * FROM books_resolved";

        const books: BookDataRet[] = await pool
            .query(sql)
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        return books;
    }

    /**
     * Retrieves a book by its ID.
     *
     * @param book_id - The ID of the book to retrieve.
     * @returns A Promise that resolves to the BookData object representing the book.
     */
    async getById(book_id: ID): Promise<BookDataRet> {
        const sql = "SELECT * FROM books_resolved WHERE id = ?";

        const bookData: BookDataRet = await pool
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("book", book_id);
                }
                return res[0];
            });

        return bookData;
    }

    /**
     * Retrieves an array of books by their IDs.
     * @param book_ids - An array of book IDs.
     * @returns A promise that resolves to an array of BookData objects.
     */
    async getByIds(book_ids: ID[]): Promise<BookDataRet[]> {
        if (book_ids.length == 0) {
            return [];
        }

        const sql = "SELECT * FROM books_resolved WHERE id IN (?)";

        const books: BookDataRet[] = await pool
            .query(sql, [book_ids])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != book_ids.length) {
                    console.warn(
                        `Missing books! Found '${res.length}' books for lookup of '
                        ${book_ids.length}' book ids.`,
                    );
                }
                return res;
            });

        return books;
    }

    /**
     * Searches for books based on the given query.
     * @param query - The search query.
     * @returns A promise that resolves to an array of BookData objects.
     */
    async search(query: string): Promise<BookDataRet[]> {
        const sql = `
        SELECT * FROM books_resolved 
            WHERE title LIKE ? OR
            id LIKE ? OR
            comment LIKE ? OR
            created LIKE ? OR
            finished LIKE ? OR
            last_updated LIKE ?
        `;

        const books: BookDataRet[] = await pool
            .query(sql, [
                `%${query}%`,
                `%${query}%`,
                `%${query}%`,
                `%${query}%`,
                `%${query}%`,
                `%${query}%`,
            ])
            .then(check_mariadb_warning);

        return books;
    }

    /**
     * Updates a book in the database.
     *
     * @param bookData - The data of the book to be updated.
     * @returns promise that resolves to true if the update was successful.
     */
    async update(
        bookData: Partial<BookDataInsert> & { id: ID },
    ): Promise<BookDataRet> {
        if (!bookData.id) {
            // TODO: switch to dynamic update
            throw new Error("Invalid book data. Missing required fields.");
        }

        const sql = `
            UPDATE books SET 
                title=?, comment=?, owner_user_id=?, owner_group_id=?, cover_page_id=?
                WHERE id = ?;
            `;

        await pool
            .query(sql, [
                bookData.title,
                bookData.comment,
                bookData.owner_user_id ? bookData.owner_user_id : null,
                bookData.owner_group_id ? bookData.owner_group_id : null,
                bookData.cover_page_id ? bookData.cover_page_id : null,
                bookData.id,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
            });

        bookData.last_updated = new Date();

        return await this.getById(bookData.id);
    }

    /**
     * Deletes a book from the database.
     *
     * @param id - The ID of the book to delete.
     * @returns A promise that resolves to true if the deletion was successful.
     */
    async delete(id: number): Promise<boolean> {
        const sql = "DELETE FROM `books` WHERE id = ?";

        const success = await pool
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Delete failed, no rows affected");
                }
                return true;
            });

        return success;
    }

    /**
     * Sets the owner of a book entity.
     *
     * @param entity_id - The ID of the entity.
     * @param entity_type - The type of the entity (user or group).
     * @param book_id - The ID of the book.
     * @returns A promise that resolves to a boolean indicating whether the operation was successful.
     */
    async setOwner(
        entity_id: ID,
        entity_type: Entity | "user" | "group",
        book_id: ID,
    ): Promise<BookOwner> {
        if (!entity_id || !book_id || !entity_type) {
            throw new Error("Invalid entity or book id");
        }

        let sql;
        switch (entity_type) {
            case "user":
            case 1:
                sql =
                    "UPDATE `books` SET `owner_user_id`=?, `owner_group_id`=NULL WHERE id = ?";
                break;
            case "group":
            case 2:
                sql =
                    "UPDATE `books` SET `owner_group_id`=?, `owner_user_id`=NULL WHERE id = ?";
                break;
            default:
                throw new Error("Invalid entity type");
        }

        // The perms table is updated by a trigger
        const success = await pool
            .query(sql, [entity_id, book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Set owner failed, no rows affected");
                }
                return true;
            });

        return await this.getOwner(book_id);
    }

    async setOwnerByUser(user_id: ID, book_id: ID): Promise<BookOwner> {
        return this.setOwner(user_id, "user", book_id);
    }
    async setOwnerByGroup(group_id: ID, book_id: ID): Promise<BookOwner> {
        return this.setOwner(group_id, "group", book_id);
    }

    async getOwner(book_id: ID): Promise<BookOwner> {
        const sql = `
            SELECT 
                owner_id as id,
                owner_name as entity_name,
                owner_type as entity_type
            FROM books_resolved
            WHERE id = ?;
        `;

        const owner: BookOwner = await pool
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("book", book_id);
                }
                return res[0];
            });

        return owner;
    }

    /** Get all books by user id.
     * If pWrite is true, it returns only books where the user has write
     * permissions.
     *
     * @param user_id
     */
    async getByUserId(user_id: ID, pWrite = false): Promise<BookDataRet[]> {
        const acl = await permissionService.getByEntity(user_id);
        const book_ids = acl
            .filter(
                (a) =>
                    a.resource_type == RESOURCE_BOOK &&
                    (pWrite ? a.pWrite : a.pRead),
            )
            .map((a) => a.resource_id);
        const books = await this.getByIds(book_ids);
        return books;
    }
}

/** We need to globally register each service here
 * to prevent memory leaks in development.
 */
export const bookService = registerService("db_book", () => new BookService());
