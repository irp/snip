import { registerService } from "@snip/common";

import {
    check_mariadb_warning,
    NoRowsAffectedError,
    NotFoundError,
} from "../errors";
import { pool_data, pool_permissions as pool } from "../sql.connection";
import {
    Entity,
    ENTITY_GROUP,
    ENTITY_USER,
    ID,
    PartialBy,
    PermissionACLData,
    PermissionACLDataResolved,
    Resource,
    RESOURCE_BOOK,
} from "../types";

export const ACL_ACCESS_DENIED: Required<PermissionACLData> = {
    id: -1,
    entity_id: -1,
    resource_id: -1,
    entity_type: ENTITY_USER,
    resource_type: RESOURCE_BOOK,
    pRead: false,
    pWrite: false,
    pDelete: false,
    pACL: false,
};

export class PermissionService {
    /**
     * Inserts a new ACL entry into the database.
     * @param acl The ACL data to insert.
     * @returns A Promise that resolves to the PermissionACLData object if successful, or ACL_ACCESS_DENIED if not successful.
     * @throws Error if the ACL is not inserted.
     */
    async insert(
        acl: PartialBy<PermissionACLData, "id">,
    ): Promise<PermissionACLData> {
        const sql =
            "INSERT INTO acl (entity_id, entity_type, resource_id, resource_type, pRead, pWrite, pDelete, pACL) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        const acl_id: ID = await pool
            .query(sql, [
                acl.entity_id,
                acl.entity_type,
                acl.resource_id,
                acl.resource_type,
                acl.pRead,
                acl.pWrite,
                acl.pDelete,
                acl.pACL,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.insertId;
            });

        acl.id = acl_id;
        return acl as PermissionACLData;
    }

    /**
     * Retrieves all permission ACL data from the database.
     * @returns A promise that resolves to an array of PermissionACLData.
     */
    async getAll(): Promise<PermissionACLData[]> {
        const sql = "SELECT * FROM acl";
        const aclData: PermissionACLData[] = await pool
            .query(sql)
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        return aclData;
    }

    /**
     * Retrieves a permission ACL data by its ID.
     * @param acl_id The ID of the ACL to retrieve.
     * @returns A Promise that resolves to the PermissionACLData object if found, or ACL_ACCESS_DENIED if not found.
     */
    async getById(acl_id: ID): Promise<PermissionACLData> {
        const sql = "SELECT * FROM acl WHERE id = ?";

        const aclData: PermissionACLData = await pool
            .query(sql, [acl_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("PermissionACL", acl_id);
                }

                return res[0];
            });

        return aclData;
    }

    /**
     * Retrieves permission ACL data by ids
     *
     * @param acl_ids The IDs of the ACLs to retrieve.
     * @returns A Promise that resolves to an array of PermissionACLData objects.
     */
    async getByIds(acl_ids: ID[]): Promise<PermissionACLData[]> {
        const sql = "SELECT * FROM acl WHERE id IN (?)";

        const aclData: PermissionACLData[] = await pool
            .query(sql, [acl_ids])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != acl_ids.length) {
                    throw new NotFoundError("PermissionACL", acl_ids);
                }

                return res;
            });

        return aclData;
    }

    /**
     * Updates an existing ACL entry in the database. Only the pRead, pWrite, pDelete, and pACL fields are updated.
     *
     * @param acl The ACL data to update.
     * @returns A Promise that resolves to the PermissionACLData object if successful..
     * @throws Error if the ACL is not updated.
     *
     */
    async update(acl: PermissionACLData): Promise<PermissionACLData> {
        const sql =
            "UPDATE acl SET pRead = ?, pWrite = ?, pDelete = ?, pACL = ? WHERE id = ?";

        await pool
            .query(sql, [acl.pRead, acl.pWrite, acl.pDelete, acl.pACL, acl.id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new NoRowsAffectedError(
                        "update",
                        "PermissionACL",
                        acl.id,
                    );
                }
            });

        return acl;
    }

    /**
     * Deletes an existing ACL entry from the database.
     *
     * @param acl_id The ID of the ACL to delete.
     * @returns A Promise that resolves to true if the ACL is deleted. False if the ACL is not deleted.
     */
    async delete(acl_id: ID): Promise<boolean> {
        const sql = "DELETE FROM acl WHERE id = ?";

        const success: boolean = await pool
            .query(sql, [acl_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new NoRowsAffectedError(
                        "delete",
                        "PermissionACL",
                        acl_id,
                    );
                }
                return true;
            });

        return success;
    }

    /**
     * Retrieves the permission ACL data for a given entity and resource.
     *
     * @param entity_id - The ID of the entity.
     * @param resource_id - The ID of the resource.
     * @param entity_type - The type of the entity.
     * @param resource_type - The type of the resource.
     * @returns A Promise that resolves to the PermissionACLData object.
     */
    async getByEntityAndResource(
        entity_id: ID,
        resource_id: ID,
        entity_type: Entity,
        resource_type: Resource,
    ): Promise<PermissionACLData> {
        const sql = `
        SELECT * FROM acl WHERE 
            entity_id = ? AND entity_type = ? AND
            resource_id = ? AND
            resource_type = ?`;

        const aclData: PermissionACLData = await pool
            .query(sql, [entity_id, entity_type, resource_id, resource_type])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError(
                        "PermissionACL",
                        entity_id + " " + resource_id,
                    );
                }
                return res[0];
            });

        return aclData;
    }

    /**
     * Retrieves the permission ACL data for a given user and book.
     * This resolves the group permissions for the user.
     *
     * @param user_id - The ID of the user.
     * @param book_id - The ID of the book.
     * @returns A promise that resolves to an array of PermissionACLData objects.
     * @throws {NotFoundError} if no ACL data is found for the given user and book.
     */
    async getByUserAndBook(
        user_id: ID,
        book_id: ID,
    ): Promise<Required<PermissionACLData>[]> {
        // A bit more complex query which also checks for the users groups
        const sql = `
        SELECT * FROM acl WHERE (
            (entity_id = ? AND entity_type = ${ENTITY_USER}) OR 
            (entity_id IN (SELECT group_id FROM users_groups WHERE user_id = ?)) AND
            entity_type = ${ENTITY_GROUP}
        )
        AND resource_id = ?
        AND resource_type = ${RESOURCE_BOOK}`;

        let aclData: Required<PermissionACLData>[] = await pool
            .query(sql, [user_id, user_id, book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length < 1) {
                    throw new NotFoundError(
                        "PermissionACL",
                        user_id + " " + book_id,
                    );
                }
                return res;
            });

        // Check if book is finished
        const sql_finished = `SELECT finished FROM books WHERE id = ?`;
        const finished = await pool_data
            .query(sql_finished, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    return false;
                }
                return res[0].finished;
            });

        if (finished) {
            // If book is finished, we remove all write permissions
            aclData = aclData.map((acl) => {
                return {
                    ...acl,
                    pWrite: false,
                };
            });
        }

        return aclData;
    }

    /**
     * Retrieves the reduced permission ACL data for a specific user and book.
     * @param user_id - The ID of the user.
     * @param book_id - The ID of the book.
     * @returns A Promise that resolves to the reduced PermissionACLData object.
     */
    async getByUserAndBookReduced(
        user_id: ID,
        book_id: ID,
    ): Promise<Required<PermissionACLData>> {
        const all = await this.getByUserAndBook(user_id, book_id).catch(
            (error) => {
                if (error instanceof NotFoundError) {
                    return [
                        {
                            ...ACL_ACCESS_DENIED,
                            entity_id: user_id,
                            entity_type: ENTITY_USER as Entity,
                            resource_id: book_id,
                            resource_type: RESOURCE_BOOK as Resource,
                        },
                    ];
                }
                return [];
            },
        );

        const reduced = all.reduce(
            (prev: PermissionACLData, cur: PermissionACLData) => {
                return {
                    id: -2,
                    entity_id: user_id,
                    entity_type: ENTITY_USER as Entity,
                    resource_id: cur.resource_id,
                    resource_type: RESOURCE_BOOK as Resource,
                    pRead: prev.pRead || cur.pRead,
                    pWrite: prev.pWrite || cur.pWrite,
                    pDelete: prev.pDelete || cur.pDelete,
                    pACL: prev.pACL || cur.pACL,
                };
            },
            {
                ...ACL_ACCESS_DENIED,
                entity_id: user_id,
                entity_type: ENTITY_USER,
                resource_id: book_id,
                resource_type: RESOURCE_BOOK,
            },
        );

        // Convert pRead, pWrite, pDelete, pACL to boolean
        reduced.pRead = reduced.pRead ? true : false;
        reduced.pWrite = reduced.pWrite ? true : false;
        reduced.pDelete = reduced.pDelete ? true : false;
        reduced.pACL = reduced.pACL ? true : false;

        return reduced as Required<PermissionACLData>;
    }

    async getByUserAndPageId(
        user_id: ID,
        page_id: ID,
    ): Promise<PermissionACLData[]> {
        const sql = `SELECT * FROM acl WHERE (
            (entity_id = ? AND entity_type = ${ENTITY_USER}) OR 
            (entity_id IN (SELECT group_id FROM users_groups WHERE user_id = ?))
            AND entity_type = ${ENTITY_GROUP})
            AND resource_id = (
                SELECT book_id FROM snip_data.pages WHERE id = ?
            ) AND resource_type = ${RESOURCE_BOOK}`;

        const aclData: PermissionACLData[] = await pool
            .query(sql, [user_id, user_id, page_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length < 1) {
                    return ACL_ACCESS_DENIED;
                }
                return res;
            });

        return aclData;
    }

    async getByUserAndPageIdReduced(
        user_id: ID,
        page_id: ID,
    ): Promise<PermissionACLData> {
        const all = await this.getByUserAndPageId(user_id, page_id);
        const reduced = all.reduce(
            (prev: PermissionACLData, cur: PermissionACLData) => {
                return {
                    id: -2,
                    entity_id: user_id,
                    entity_type: ENTITY_USER,
                    resource_id: cur.resource_id,
                    resource_type: RESOURCE_BOOK,
                    pRead: prev.pRead || cur.pRead,
                    pWrite: prev.pWrite || cur.pWrite,
                    pDelete: prev.pDelete || cur.pDelete,
                    pACL: prev.pACL || cur.pACL,
                };
            },
            {
                ...ACL_ACCESS_DENIED,
                entity_id: user_id,
                entity_type: ENTITY_USER,
                resource_id: page_id,
                resource_type: RESOURCE_BOOK,
            },
        );
        return reduced;
    }

    /**
     * Retrieves the permission ACL data for a given entity.
     * @param entity_id The ID of the entity.
     * @param entity_type The type of the entity. Defaults to ENTITY_USER.
     * @returns A promise that resolves to an array of PermissionACLData objects.
     */
    async getByEntity(
        entity_id: ID,
        entity_type: Entity | "user" | "group" = ENTITY_USER,
    ): Promise<PermissionACLData[]> {
        // Get all books of a user/group which have read access
        let sql: string;
        let data = [];

        switch (entity_type) {
            case "user":
            case ENTITY_USER:
                sql = `
            SELECT * FROM acl 
                WHERE (
                    entity_id = ? AND entity_type = ${ENTITY_USER} AND pRead = 1
                ) OR (
                    entity_id IN(
                        SELECT group_id FROM users_groups WHERE user_id = ?
                    ) AND entity_type = ${ENTITY_GROUP} AND pRead = 1
                ); 
            `;
                data = [entity_id, entity_id];
                break;
            case "group":
            case ENTITY_GROUP:
                sql = `
            SELECT * FROM acl
                WHERE entity_id = ? AND entity_type = ${ENTITY_GROUP} AND pRead = 1;
            `;
                data = [entity_id];
                break;
            default:
                throw new Error("Invalid entity type");
        }

        const acl_pRead: PermissionACLData[] = await pool
            .query(sql, data)
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        return acl_pRead;
    }

    async getResolvedForBook(
        book_id: ID,
    ): Promise<PermissionACLDataResolved[]> {
        const sql_users = `
        SELECT 
            acl.pRead,
            acl.pWrite,
            acl.pDelete,
            acl.pACL,
            acl.resource_id,
            acl.resource_type,
            acl.entity_id,
            credentials.email AS entity_name,
            "user" AS entity_type
        FROM 
            acl 
        LEFT JOIN users ON acl.entity_id = users.id 
        LEFT JOIN credentials ON users.primary_credential_id = credentials.id
        WHERE 
            ( acl.entity_type = 1 AND acl.resource_id = ? AND acl.resource_type = 1 );
    `;
        const sql_groups = `
        SELECT 
            acl.pRead,
            acl.pWrite,
            acl.pDelete,
            acl.pACL,
            acl.resource_id,
            acl.resource_type,
            acl.entity_id,
            groups.name AS entity_name,
            "group" AS entity_type
        FROM 
            acl
        LEFT JOIN groups ON acl.entity_id = groups.id
        WHERE
            ( acl.entity_type = 2 AND acl.resource_id = ? AND acl.resource_type = 1 );
    `;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        function _resolve_response(res: any) {
            console;
            if (res.length < 1) {
                return [];
            }

            return res as PermissionACLDataResolved[];
        }

        // Parallel resolve both queries
        let acl = await Promise.all([
            pool.query(sql_users, [book_id, book_id]).then(_resolve_response),
            pool.query(sql_groups, [book_id, book_id]).then(_resolve_response),
        ]).then((values) => {
            return values[0].concat(values[1]);
        });

        // Filter admin group as it is always present and has full access
        acl = acl.filter(
            (acl) =>
                !(acl.entity_name == "admin" && acl.entity_type == "group"),
        );
        return acl;
    }

    /**
     * Updates the permission for a specific entity and resource.
     *
     * @param entity_id - The ID of the entity.
     * @param resource_id - The ID of the resource.
     * @param entity_type - The type of the entity.
     * @param resource_type - The type of the resource.
     * @param pRead - The permission for read access.
     * @param pWrite - The permission for write access.
     * @param pDelete - The permission for delete access.
     * @param pACL - The permission for ACL access.
     * @returns A promise that resolves to a boolean indicating whether the update was successful.
     */
    async updateByEntityAndResource(
        entity_id: ID,
        resource_id: ID,
        entity_type: Entity,
        resource_type: Resource,
        pRead: boolean,
        pWrite: boolean,
        pDelete: boolean,
        pACL: boolean,
    ): Promise<boolean> {
        // Insert and updated on duplicate key

        const sql = `
    INSERT INTO acl (entity_id, entity_type, resource_id, resource_type, pRead, pWrite, pDelete, pACL) 
    VALUES (?, ?, ?, ?, ?, ?, ?, ?) 
    ON DUPLICATE KEY UPDATE pRead = ?, pWrite = ?, pDelete = ?, pACL = ?`;

        const success: boolean = await pool
            .query(sql, [
                entity_id,
                entity_type,
                resource_id,
                resource_type,
                pRead,
                pWrite,
                pDelete,
                pACL,
                pRead,
                pWrite,
                pDelete,
                pACL,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
                return true;
            });

        return success;
    }

    async search(query: string): Promise<PermissionACLData[]> {
        throw new Error("Method not implemented.");
    }
}
/** We need to globally register each service here
 * to prevent memory leaks in development.
 */
export const permissionService = registerService(
    "db_permission",
    () => new PermissionService(),
);
