import { pool_data, pool_permissions } from "../../sql.connection";
import { ID, PageData, PartialBy } from "../../types";
import { PageService } from "../page";

describe("PageService", () => {
    let service: PageService;
    let defaultPageData: PartialBy<PageData, "id">;
    let book_ids: ID[] = [];
    let user_ids: ID[] = [];

    async function deleteCleanup() {
        //Cleanup ids {"table": [ids]}
        await pool_data.query(
            "DELETE FROM pages WHERE book_id = (SELECT id FROM books WHERE title = 'Test Book 1')",
        );
        await pool_data.query(
            "DELETE FROM pages WHERE book_id = (SELECT id FROM books WHERE title = 'Test Book 2')",
        );

        await pool_data.query("DELETE FROM books WHERE title = 'Test Book 1'");
        await pool_data.query("DELETE FROM books WHERE title = 'Test Book 2'");

        for (const id of user_ids) {
            await pool_permissions.query("DELETE FROM users WHERE id = ?", [
                id,
            ]);
        }
    }

    beforeAll(async () => {
        service = new PageService();

        //Insert two books
        await deleteCleanup();
        await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then((res) => {
                user_ids.push(res.insertId);
            });

        await pool_data
            .query(
                "INSERT INTO books (`title`, `comment`, `owner_user_id`) VALUES ('Test Book 1', 'Test description', ?)",
                [user_ids[0]],
            )
            .then((res) => {
                book_ids.push(res.insertId);
            });
        await pool_data
            .query(
                "INSERT INTO books (`title`, `comment`, `owner_user_id`) VALUES ('Test Book 2', 'Test description', ?)",
                [user_ids[0]],
            )
            .then((res) => {
                book_ids.push(res.insertId);
            });

        defaultPageData = {
            book_id: book_ids[0]!,
            page_number: 1,
            created: new Date(),
            last_updated: new Date(),
        };
    });

    afterAll(async () => {
        //Cleanup ids {"table": [ids]}
        await deleteCleanup();
    });

    describe("insert", () => {
        it("should insert a new page", async () => {
            const pageData = await service.insert(
                structuredClone(defaultPageData),
            );
            expect_same(pageData, defaultPageData);
            expect(pageData.id).toBeDefined();
        });

        it("should insert without page number", async () => {
            const pageData = await service.insert({
                ...structuredClone(defaultPageData),
                page_number: undefined,
            });
            expect(pageData.id).toBeDefined();
            expect(pageData.page_number).toBeDefined();
        });
    });

    describe("getAll", () => {
        it("should retrieve all pages", async () => {
            const pageData = await service.insert(
                structuredClone(defaultPageData),
            );
            const pageData2 = await service.insert(
                structuredClone(defaultPageData),
            );
            const retrievedPageData = await service.getAll();
            expect(retrievedPageData.length).toBeGreaterThanOrEqual(2);
        });
    });

    describe("getById|getByIds", () => {
        it("should retrieve a page by its ID(s)", async () => {
            const pageData = await service.insert(
                structuredClone(defaultPageData),
            );
            const pageData2 = await service.insert(
                structuredClone(defaultPageData),
            );
            const retrievedPageData = await service.getById(pageData.id);
            expect_same(retrievedPageData, pageData, true);

            const retrievedPageData2 = await service.getByIds([
                pageData.id,
                pageData2.id,
            ]);
            expect(retrievedPageData2.length).toBe(2);
            expect_same(retrievedPageData2[0]!, pageData, true);
            expect_same(retrievedPageData2[1]!, pageData2, true);
        });

        it("should throw an error if the page(s) not found", async () => {
            await expect(service.getById(-1)).rejects.toThrow();

            await expect(service.getByIds([-1])).rejects.toThrow();
        });
    });

    describe("getByBookId", () => {
        it("should retrieve pages by book_id", async () => {
            const pageData = await service.insert(
                structuredClone(defaultPageData),
            );
            const pageData2 = await service.insert(
                structuredClone(defaultPageData),
            );
            const retrievedPageData = await service.getByBookId(
                pageData.book_id,
            );
            expect(retrievedPageData.length).toBeGreaterThanOrEqual(2);

            // get first
            const firstPageData = await service.getFirstByBookId(
                pageData.book_id,
            );
            expect_same(firstPageData, retrievedPageData[0]!, true);

            // get last
            const lastPageData = await service.getLastByBookId(
                pageData.book_id,
            );
            expect_same(
                lastPageData,
                retrievedPageData[retrievedPageData.length - 1]!,
                true,
            );
        });

        it("should return empty array if no pages found", async () => {
            await expect(service.getByBookId(-1)).resolves.toEqual([]);

            await expect(service.getFirstByBookId(-1)).rejects.toThrow();

            await expect(service.getLastByBookId(-1)).rejects.toThrow();
        });
    });

    describe("update", () => {
        it("should update a page", async () => {
            const pageData = await service.insert(
                structuredClone(defaultPageData),
            );
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const updatedPageData = await service.update({
                ...pageData,
                background_type_id: 2,
            });
            expect_same(updatedPageData, {
                ...pageData,
                background_type_id: 2,
            });
            expect(updatedPageData.id).toBe(pageData.id);
            expect(updatedPageData.last_updated!.getTime()).toBeGreaterThan(
                pageData.last_updated!.getTime(),
            );
        });

        it("should throw an error if the page not found", async () => {
            await expect(
                service.update({
                    ...defaultPageData,
                    id: -1,
                }),
            ).rejects.toThrow();
        });
    });

    describe("delete", () => {
        it("should delete a page", async () => {
            const pageData = await service.insert(
                structuredClone(defaultPageData),
            );
            await service.delete(pageData.id);
            await expect(service.getById(pageData.id)).rejects.toThrow();
        });

        it("should throw an error if the page not found", async () => {
            await expect(service.delete(-1)).rejects.toThrow();
        });
    });

    describe("search", () => {
        test.todo("move from api to database package");
    });
});

function expect_same(
    a: PartialBy<PageData, "id">,
    b: PartialBy<PageData, "id">,
    time = false,
) {
    expect(a.book_id).toBe(b.book_id);
    expect(a.page_number).toBeDefined();
    expect(b.page_number).toBeDefined();
    expect(a.created).toBeDefined();
    expect(a.last_updated).toBeDefined();
    expect(b.created).toBeDefined();
    expect(b.last_updated).toBeDefined();

    if (time) {
        expect(a.created!.getTime()).toBeCloseTo(b.created!.getTime(), -4);
        expect(a.last_updated!.getTime()).toBeCloseTo(
            b.last_updated!.getTime(),
            -4,
        );
    }
}
