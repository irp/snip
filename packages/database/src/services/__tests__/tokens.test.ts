import { TokenService } from "../tokens";
import {
    APITokenData,
    UserDataRet as UserData,
    UITokenData,
    BookDataRet,
} from "../../types";
import { pool_permissions } from "../../sql.connection";
import { bookService } from "../book";
import { userService } from "../user";
import { NotFoundError } from "../../errors";

describe("TokenService", () => {
    let service: TokenService;
    let bookData: BookDataRet;
    let userData: UserData[];

    beforeAll(async () => {
        service = new TokenService();

        await pool_permissions.query("DELETE FROM tokens");

        // Insert two users
        const u1 = await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then(async (res) => await userService.getById(res.insertId));

        const u2 = await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then(async (res) => await userService.getById(res.insertId));
        userData = [u1, u2];

        // Insert a book
        bookData = await bookService.insert({
            title: "test",
            owner_user_id: u1.id,
        });
    });
    afterAll(async () => {
        if (bookData) {
            await bookService.delete(bookData.id);
        }
        await pool_permissions.query("DELETE FROM users WHERE id in (?, ?)", [
            userData[0]!.id,
            userData[1]!.id,
        ]);
    });

    describe("insert", () => {
        afterAll(async () => {
            await pool_permissions.query("DELETE FROM tokens");
        });

        it("insert", async () => {
            const tokendata: UITokenData = {
                type: "ui",
                book_id: bookData.id,
                created_by: userData[0]!.id,
                description: "foo bar",
                expires_at: new Date(),
                hashed_token: "token",
            };

            const token = await service.insert(tokendata);

            expect(token).toMatchObject({
                book_id: bookData.id,
                created_by: userData[0]!.id,
                description: "foo bar",
                type: "ui",
                expires_at: expect.any(Date),
                hashed_token: "token",
            });
        });
    });

    describe("getById", () => {
        let ui_token: UITokenData;
        let api_token: APITokenData;

        beforeAll(async () => {
            ui_token = (await service.insert({
                type: "ui",
                book_id: bookData.id,
                created_by: userData[0]!.id,
                description: "foo bar",
                expires_at: new Date(),
                hashed_token: "token",
            })) as UITokenData;
            api_token = (await service.insert({
                type: "api",
                book_id: bookData.id,
                created_by: userData[0]!.id,
                description: "foo bar",
                hashed_token: "hashed_token",
            })) as APITokenData;
        });

        afterAll(async () => {
            await service.delete(ui_token.id!);
            await service.delete(api_token.id!);
        });

        it("getById", async () => {
            const tokenData = await service.getById(ui_token.id!);

            expect(tokenData).toMatchObject(ui_token);
        });

        it("getById not found", async () => {
            await expect(service.getById(9999)).rejects.toThrow(NotFoundError);
        });

        it("getByBookId", async () => {
            const tokens = await service.getByBookId(bookData.id);

            expect(tokens).toHaveLength(2);
            expect(tokens[0]).toMatchObject(ui_token);
            expect(tokens[1]).toMatchObject(api_token);
        });
    });
});
