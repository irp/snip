import { MemberRole } from "src/types";
import { NotFoundError } from "../../errors";
import { pool_data, pool_permissions } from "../../sql.connection";
import { GroupService } from "../group";

describe("GroupService", () => {
    let service: GroupService = new GroupService();
    let defaultGroupData = {
        name: "test.group",
        description: "Test description",
    };
    let user_ids: number[] = [];

    async function clearLocalDBEntries() {
        await pool_permissions.query("DELETE FROM users_groups");
        await pool_data.query("DELETE FROM pages");
        await pool_permissions.query("DELETE FROM collaboration_invites");
        await pool_data.query("DELETE FROM books");
        await pool_permissions.query(
            "UPDATE users SET primary_credential_id = NULL",
        );
        await pool_permissions.query("DELETE FROM credentials");
        await pool_permissions.query("DELETE FROM users");
        user_ids = [];
        await pool_permissions.query(
            "DELETE FROM groups where name like 'test.group'",
        );
    }

    beforeAll(async () => {
        //Insert a user
        await clearLocalDBEntries();
        await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL);")
            .then((res) => {
                user_ids.push(res.insertId);
            });
    });

    afterAll(async () => {
        await clearLocalDBEntries();
    });

    describe("insert", () => {
        it("should insert a new group", async () => {
            const groupData = await service.insert(
                structuredClone(defaultGroupData),
            );
            expect_same(groupData, defaultGroupData);
            expect(groupData.id).toBeDefined();
            expect(groupData.created).toBeDefined();
            expect(groupData.last_updated).toBeDefined();
        });
    });

    describe("update", () => {
        it("should update a group", async () => {
            const groupData = await service.insert(
                structuredClone(defaultGroupData),
            );
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const updatedGroupData = await service.update({
                ...groupData,
                description: "Updated description",
            });
            expect_same(updatedGroupData, {
                ...groupData,
                description: "Updated description",
            });
            expect(updatedGroupData.id).toBe(groupData.id);
            expect(updatedGroupData.created.getTime()).closeTo(
                groupData.created.getTime(),
                1000,
            );
            expect(updatedGroupData.last_updated.getTime()).toBeGreaterThan(
                groupData.last_updated!.getTime(),
            );
        });

        it("should throw an error if the group not found", async () => {
            await expect(
                service.update({
                    ...defaultGroupData,
                    id: -1,
                }),
            ).rejects.toThrow();
        });
    });

    describe("delete", () => {
        it("should delete a group", async () => {
            const groupData = await service.insert(
                structuredClone(defaultGroupData),
            );
            await service.delete(groupData.id);
            await expect(service.getById(groupData.id)).rejects.toThrow();
        });

        it("should throw an error if the group not found", async () => {
            await expect(service.delete(-1)).rejects.toThrow();
        });
    });

    describe("queries", () => {
        let user_ids: number[] = [];
        let group_ids: number[] = [];

        beforeAll(async () => {
            await clearLocalDBEntries();
            for (let i = 0; i < 3; i++) {
                await pool_permissions
                    .query("INSERT INTO users (`id`) VALUES (NULL)")
                    .then((res) => {
                        user_ids.push(res.insertId);
                    });
            }

            for (let i = 0; i < 3; i++) {
                await service
                    .insert({
                        name: `test.group${i}`,
                        description: `Test description ${i}`,
                    })
                    .then((res) => {
                        group_ids.push(res.id);
                    });
            }
        });

        describe("getById|getByIds", () => {
            it("should retrieve a group by its ID(s)", async () => {
                let data = [];
                for (let i = 0; i < 3; i++) {
                    const retrievedGroupData = await service.getById(
                        group_ids[i]!,
                    );
                    expect_same(
                        retrievedGroupData,
                        {
                            name: `test.group${i}`,
                            description: `Test description ${i}`,
                        },
                        false,
                    );
                    data.push(retrievedGroupData);
                }

                const retrievedGroupData = await service.getByIds(group_ids);
                expect(retrievedGroupData.length).toBe(3);
                for (let i = 0; i < 3; i++) {
                    expect_same(retrievedGroupData[i]!, data[i]!, true);
                }
            });

            it("should throw an error if the group(s) not found", async () => {
                await expect(service.getById(-1)).rejects.toThrow();

                await expect(service.getByIds([-1])).rejects.toThrow();
            });
        });

        describe("getByUserID|getGroupIDsByUserID", () => {
            beforeAll(async () => {
                for (let i = 0; i < 3; i++) {
                    await service.addMember(user_ids[0]!, group_ids[i]!);
                }
            });

            it("should retrieve groups by user ID", async () => {
                const retrievedGroupData = await service.getByUserId(
                    user_ids[0]!,
                );
                expect(retrievedGroupData.length).toBe(3);
                expect_same(retrievedGroupData[0]!, {
                    name: "test.group0",
                    description: "Test description 0",
                });
            });

            it("should retrieve group IDs by user ID", async () => {
                const retrievedGroupIDs = await service.getGroupIdsByUserId(
                    user_ids[0]!,
                );
                expect(retrievedGroupIDs.length).toBe(3);
                expect(retrievedGroupIDs).toContain(group_ids[0]);
            });

            it("should return an empty array the user not found", async () => {
                const retrievedGroupData = await service.getByUserId(-1);
                expect(retrievedGroupData.length).toBe(0);

                const retrievedGroupIDs = await service.getGroupIdsByUserId(-1);
                expect(retrievedGroupIDs.length).toBe(0);
            });
        });

        afterAll(async () => {
            await clearLocalDBEntries();
        });
    });

    describe("search", () => {
        test.todo("add test");
    });

    /* -------------------------------------------------------------------------- */
    /*                                group members                               */
    /* -------------------------------------------------------------------------- */

    describe("members", () => {
        let user_ids: number[] = [];
        beforeAll(async () => {
            await clearLocalDBEntries();
            for (let i = 0; i < 3; i++) {
                await pool_permissions
                    .query("INSERT INTO users (`id`) VALUES (NULL)")
                    .then((res) => {
                        user_ids.push(res.insertId);
                    });
            }
        });

        describe("addMember", () => {
            it("should add members", async () => {
                const groupData = await service.insert(
                    structuredClone(defaultGroupData),
                );

                for (const role of [
                    "invited",
                    "member",
                    "owner",
                    "moderator",
                ]) {
                    const member = await service.addMember(
                        user_ids[0]!,
                        groupData.id,
                        role as MemberRole,
                    );
                    expect(member.group_id).toBe(groupData.id);
                    expect(member.user_id).toBe(user_ids[0]);
                    //Date
                    expect(member.joined_at).toBeDefined();
                    expect(member.joined_at).toBeInstanceOf(Date);
                    //Role
                    expect(member.role).toBe(role);
                }
            });

            it("should overwrite role if already exists", async () => {
                const groupData = await service.insert(
                    structuredClone(defaultGroupData),
                );

                // Inital add
                const member = await service.addMember(
                    user_ids[0]!,
                    groupData.id,
                );
                expect(member.group_id).toBe(groupData.id);
                expect(member.user_id).toBe(user_ids[0]!);
                expect(member.joined_at).toBeDefined();
                expect(member.joined_at).toBeInstanceOf(Date);
                expect(member.role).toBe("invited");

                await new Promise((resolve) => setTimeout(resolve, 1000));

                // Overwrite
                const member2 = await service.addMember(
                    user_ids[0]!,
                    groupData.id,
                    "member",
                );

                expect(member2.group_id).toBe(groupData.id);
                expect(member2.user_id).toBe(user_ids[0]!);
                expect(member2.joined_at).toBeDefined();
                expect(member2.joined_at).toBeInstanceOf(Date);
                expect(member2.role).toBe("member");
                //Date should update
                expect(member2.joined_at.getTime()).toBeGreaterThan(
                    member.joined_at.getTime(),
                );

                await new Promise((resolve) => setTimeout(resolve, 1000));

                // Overwrite
                const member3 = await service.addMember(
                    user_ids[0]!,
                    groupData.id,
                    "owner",
                );
                expect(member3.group_id).toBe(groupData.id);
                expect(member3.user_id).toBe(user_ids[0]!);
                expect(member3.joined_at).toBeDefined();
                expect(member3.joined_at).toBeInstanceOf(Date);
                expect(member3.role).toBe("owner");
                //Date should not update!
                expect(member3.joined_at.getTime()).toBe(
                    member2.joined_at.getTime(),
                );

                await new Promise((resolve) => setTimeout(resolve, 1000));

                // Overwrite
                const member4 = await service.addMember(
                    user_ids[0]!,
                    groupData.id,
                    "moderator",
                );
                expect(member4.group_id).toBe(groupData.id);
                expect(member4.user_id).toBe(user_ids[0]!);
                expect(member4.joined_at).toBeDefined();
                expect(member4.joined_at).toBeInstanceOf(Date);
                expect(member4.role).toBe("moderator");
                //Date should not update!
                expect(member4.joined_at.getTime()).toBe(
                    member3.joined_at.getTime(),
                );
            }, 10000);
        });

        describe("getMember(s)", () => {
            it("should retrieve a member by user ID and group ID", async () => {
                const groupData = await service.insert(
                    structuredClone(defaultGroupData),
                );

                const members = await service.getMembers(groupData.id);
                expect(members.length).toBe(0);

                const member = await service.addMember(
                    user_ids[0]!,
                    groupData.id,
                );
                expect(member.group_id).toBe(groupData.id);
                expect(member.user_id).toBe(user_ids[0]!);
                expect(member.joined_at).toBeDefined();
                expect(member.joined_at).toBeInstanceOf(Date);
                expect(member.role).toBe("invited");

                const members2 = await service.getMembers(groupData.id);
                expect(members2.length).toBe(1);
                expect(members2[0]!.group_id).toBe(groupData.id);
                expect(members2[0]!.user_id).toBe(user_ids[0]!);
                expect(members2[0]!.joined_at).toBeDefined();
                expect(members2[0]!.joined_at).toBeInstanceOf(Date);
                expect(members2[0]!.role).toBe("invited");

                //Overlap
                expect(members2[0]).toStrictEqual(member);
            });

            it("should throw not found error if member not found", async () => {
                await expect(service.getMember(-1, -1)).rejects.toThrow(
                    NotFoundError,
                );

                // Empty on getMembers
                const members = await service.getMembers(-1);
                expect(members.length).toBe(0);
            });
        });

        describe("isAllowedToEdit", () => {
            it("existing members", async () => {
                const groupData = await service.insert(
                    structuredClone(defaultGroupData),
                );

                const roles = {
                    name: ["invited", "member", "moderator", "owner"],
                    allowed: [false, false, true, true],
                };

                for (let i = 0; i < roles.name.length; i++) {
                    const role = roles.name[i];

                    const user_id = await pool_permissions
                        .query("INSERT INTO users (id) VALUES (NULL)")
                        .then((res) => res.insertId);

                    const member = await service.addMember(
                        user_id,
                        groupData.id,
                        role as any,
                    );
                    expect(member.group_id).toBe(groupData.id);
                    expect(member.user_id).toBe(user_id);
                    //Date
                    expect(member.joined_at).toBeDefined();
                    expect(member.joined_at).toBeInstanceOf(Date);
                    //Role
                    expect(member.role).toBe(role);

                    // Check if allowed to edit
                    const allowed = await service.isAllowedToEdit(
                        user_id,
                        groupData.id,
                    );
                    expect(allowed).toBe(roles.allowed[i]);
                }
            });
            it("not existing members", async () => {
                const groupData = await service.insert(
                    structuredClone(defaultGroupData),
                );

                const allowed = await service.isAllowedToEdit(-1, groupData.id);
                expect(allowed).toBe(false);
            });
        });

        describe("removeMember", () => {
            it("should remove a member", async () => {
                const groupData = await service.insert(
                    structuredClone(defaultGroupData),
                );

                const user_id = await pool_permissions
                    .query("INSERT INTO users (id) VALUES (NULL)")
                    .then((res) => res.insertId);

                const member = await service.addMember(user_id, groupData.id);
                expect(member.group_id).toBe(groupData.id);
                expect(member.user_id).toBe(user_id);
                expect(member.joined_at).toBeDefined();
                expect(member.joined_at).toBeInstanceOf(Date);

                await service.removeMember(user_id, groupData.id);
                await expect(
                    service.getMember(user_id, groupData.id),
                ).rejects.toThrow(NotFoundError);
            });

            it("should throw an error if the member not found", async () => {
                await expect(service.removeMember(-1, -1)).rejects.toThrow(
                    NotFoundError,
                );
            });
        });

        it("getRole", async () => {
            const groupData = await service.insert(
                structuredClone(defaultGroupData),
            );

            const user_id = await pool_permissions
                .query("INSERT INTO users (id) VALUES (NULL)")
                .then((res) => res.insertId);

            const member = await service.addMember(user_id, groupData.id);

            const getRole = await service.getRole(user_id, groupData.id);
            expect(getRole).toBe("invited");

            const getRole2 = await service.getRole(-1, -1);
            expect(getRole2).toBe(undefined);

            const getRole3 = await service.getRole(user_id, -1);
            expect(getRole3).toBe(undefined);

            const getRole4 = await service.getRole(-1, groupData.id);
            expect(getRole4).toBe(undefined);
        });
    });
});

function expect_same(A: any, B: any, times = false) {
    expect(A.name).toBe(B.name);
    expect(A.description).toBe(B.description);
    if (times) {
        expect(A.created!.getTime()).toBeCloseTo(B.created!.getTime(), -4);
        expect(A.last_updated!.getTime()).toBeCloseTo(
            B.last_updated!.getTime(),
            -4,
        );
    }
}
