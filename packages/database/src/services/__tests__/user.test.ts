import { getPrimaryEmail, UserService } from "../user";
import { pool_permissions } from "../../sql.connection";

describe(
    "UserService",
    {
        timeout: 20000, //login pw check is slow (hashing)
    },
    () => {
        const service = new UserService();

        const testUserEmail = {
            email: "testemail@users.de",
            password: "test",
        };
        const testUserSSO = {
            provider_id: "test",
            provider_user_id: "test",
            email: "testsso@users.de",
            email_verified: true,
        };
        afterAll(async () => {
            await clearLocalDBEntries();
        });

        describe("signup flow", () => {
            beforeEach(async () => {
                await clearLocalDBEntries();
            });

            it("using password", async () => {
                const userAfterSignup = await service.signupWithPassword(
                    testUserEmail.email,
                    testUserEmail.password,
                );

                // Check if the user was created (only one email)
                expect(userAfterSignup.emails[0]).toBe(testUserEmail.email);
                expect(userAfterSignup.emails_verified[0]).toBe(false);
                expect(userAfterSignup.id).toBeDefined();

                // Check if credentials were added
                const credentials = await pool_permissions.query(
                    `SELECT * FROM credentials WHERE email = ? AND credential_type = ?`,
                    [testUserEmail.email, "password"],
                );
                expect(credentials.length).toBe(1);
                expect(credentials[0].password_hash).toBeDefined();

                // Check if login with password works
                const userAfterLogin = await service.loginWithPassword(
                    testUserEmail.email,
                    testUserEmail.password,
                );
                expect(userAfterLogin.emails[0]).toBe(testUserEmail.email);
                expect(userAfterLogin.emails_verified[0]).toBe(false);

                // Check if the user can be verified
                await service.verifySignupWithPassword(testUserEmail.email);
                const userAfterVerify = await service.loginWithPassword(
                    testUserEmail.email,
                    testUserEmail.password,
                );
                expect(userAfterVerify.emails[0]).toBe(testUserEmail.email);
                expect(userAfterVerify.emails_verified[0]).toBe(true);
            });

            it("using SSO", async () => {
                const userAfterSignup = await service.signupWithSSO(
                    testUserSSO.provider_id,
                    testUserSSO.provider_user_id,
                    testUserSSO.email,
                    testUserSSO.email_verified,
                );

                // Check if the user was created
                expect(userAfterSignup.emails[0]).toBe(testUserSSO.email);
                expect(userAfterSignup.emails_verified[0]).toBe(
                    testUserSSO.email_verified,
                );
                expect(userAfterSignup.id).toBeDefined();

                // Check if login with SSO works
                const userAfterLogin = await service.loginWithSSO(
                    testUserSSO.provider_id,
                    testUserSSO.provider_user_id,
                );
                expect(userAfterLogin.emails[0]).toBe(testUserSSO.email);
                expect(userAfterLogin.emails_verified[0]).toBe(
                    testUserSSO.email_verified,
                );
            });

            it("login fails", async () => {
                await expect(
                    service.loginWithPassword("unknown", "unknown"),
                ).rejects.toThrow();

                await expect(
                    service.loginWithSSO("unknown", "unknown"),
                ).rejects.toThrow();
            });

            it("sso without verified email", async () => {
                await expect(
                    service.signupWithSSO(
                        testUserSSO.provider_id,
                        testUserSSO.provider_user_id,
                        testUserSSO.email,
                        false,
                    ),
                ).rejects.toThrow();
            });
        });

        describe("primary email", () => {
            let user_id: number;
            /** This test if the sql trigger
             * works correctly.
             * It should set the first credential
             * as primary email if no other credential
             * is available.
             */

            beforeAll(async () => {
                await clearLocalDBEntries();

                user_id = await pool_permissions
                    .query("INSERT INTO users (`id`) VALUES (NULL)")
                    .then((res) => res.insertId);
            });

            afterAll(async () => {
                await clearLocalDBEntries();
            });

            it("new credential is primary email if no other credential is available", async () => {
                // Add credential
                await service.addPasswordCredential(
                    user_id,
                    "1primary_email@users.de",
                    "some_password",
                );

                // Get credential
                const user = await service.getById(user_id);

                expect(getPrimaryEmail(user)).toBe("1primary_email@users.de");
            });
            it("set primary email removes old primary email from credential", async () => {
                // Add credential
                const c_id = await service.addSSOCredential(
                    user_id,
                    "provider",
                    "provider_user_id",
                    "2primary_email@users.de",
                    true,
                );

                // Get credentials
                const user = await service.getById(user_id);

                expect(getPrimaryEmail(user)).toBe("1primary_email@users.de");

                // Set primary
                await service.setPrimaryCredential(user_id, c_id);
                const user_new = await service.getById(user_id);

                // Check if the new credential is primary
                expect(getPrimaryEmail(user_new)).toBe(
                    "2primary_email@users.de",
                );
            });
        });

        describe("user actions", () => {
            let user_id_pw: number;
            let user_id_sso: number;
            beforeAll(async () => {
                await clearLocalDBEntries();
                const userData = await service.signupWithPassword(
                    testUserEmail.email,
                    testUserEmail.password,
                );
                user_id_pw = userData.id;

                const userDataSSO = await service.signupWithSSO(
                    testUserSSO.provider_id,
                    testUserSSO.provider_user_id,
                    testUserSSO.email,
                    testUserSSO.email_verified,
                );
                user_id_sso = userDataSSO.id;
            });

            describe("password change", () => {
                it("should successfully change password if user exists", async () => {
                    const newPassword = "newPassword";
                    const changed = await service.changePassword(
                        testUserEmail.email,
                        newPassword,
                    );

                    expect(changed).toBe(true);

                    const user = await service.loginWithPassword(
                        testUserEmail.email,
                        newPassword,
                    );
                    expect(user.id).toBe(user_id_pw);
                });

                it("should not change password if user does not exist", async () => {
                    const newPassword = "newPassword";

                    await expect(
                        service.changePassword("unknown@email.de", newPassword),
                    ).rejects.toThrow();
                });

                it("should fail if the user has no password credential", async () => {
                    const newPassword = "newPassword";

                    await expect(
                        service.changePassword(testUserSSO.email, newPassword),
                    ).rejects.toThrow();
                });
            });

            describe("add credential", () => {
                it("add a password credential to a user without one", async () => {
                    const id = await service.addPasswordCredential(
                        user_id_sso,
                        testUserSSO.email,
                        "aPassword",
                    );

                    expect(id).toBeDefined;

                    const user = await service.loginWithPassword(
                        testUserSSO.email,
                        "aPassword",
                    );
                    expect(user.id).toBe(user_id_sso);
                    expect(user.emails).toContain(testUserSSO.email);
                });

                it("add password credentials should fail if the email exists", async () => {
                    await expect(
                        service.addPasswordCredential(
                            user_id_sso,
                            testUserSSO.email,
                            "aPassword",
                        ),
                    ).rejects.toThrow();
                });

                it("add a sso credential to a user without one", async () => {
                    const id = await service.addSSOCredential(
                        user_id_pw,
                        testUserSSO.provider_id,
                        "n" + testUserSSO.provider_user_id,
                        "n" + testUserSSO.email,
                        testUserSSO.email_verified,
                    );

                    expect(id).toBeDefined();

                    const user = await service.loginWithSSO(
                        testUserSSO.provider_id,
                        "n" + testUserSSO.provider_user_id,
                    );
                    expect(user.id).toBe(user_id_pw);
                });

                it("add sso credentials should fail if the provider_user_id exists", async () => {
                    await expect(
                        service.addSSOCredential(
                            user_id_pw,
                            testUserSSO.provider_id,
                            "n" + testUserSSO.provider_user_id,
                            "n" + testUserSSO.email,
                            testUserSSO.email_verified,
                        ),
                    ).rejects.toThrow();
                });
                it("add sso credentials should fail if the email is not verified", async () => {
                    await expect(
                        service.addSSOCredential(
                            user_id_pw,
                            testUserSSO.provider_id,
                            "n" + testUserSSO.provider_user_id,
                            "n" + testUserSSO.email,
                            false,
                        ),
                    ).rejects.toThrow();
                });
            });

            describe("last_login", () => {
                it("should update the last login time", async () => {
                    const ip = "192.168.1.1";
                    await service.addLastLogin(user_id_pw, ip);
                });

                it("should get the last logins", async () => {
                    const logins = await service.getLastLogins(user_id_pw);
                    expect(logins.length).toBe(1);

                    // Test range
                    await pool_permissions.query(
                        `UPDATE last_logins SET time = time - INTERVAL 32 DAY`,
                    );

                    const logins2 = await service.getLastLogins(user_id_pw, 30);
                    expect(logins2.length).toBe(0);
                });
            });
        });

        describe("queries", () => {
            let user_id_pw: number;
            beforeAll(async () => {
                await clearLocalDBEntries();
                user_id_pw = await pool_permissions
                    .query("INSERT INTO users (`id`) VALUES (NULL)")
                    .then((res) => res.insertId);

                const c_id = await pool_permissions
                    .query(
                        "INSERT INTO credentials (`user_id`, `email`, `credential_type`, `password_hash`) VALUES (?, ?, ?, ?)",
                        [user_id_pw, testUserEmail.email, "password", "test"],
                    )
                    .then((res) => res.insertId);
            });

            it("getById should return the user by id", async () => {
                const userData = await service.getById(user_id_pw);
                expect(userData.id).toBe(user_id_pw);

                //emails array
                expect(userData.emails[0]).toBe(testUserEmail.email);
                expect(userData.emails_verified[0]).toBe(false);
            });
            it("getById should throw if the user by id does not exist", async () => {
                await expect(service.getById(-1)).rejects.toThrow();
            });

            it("getByIds should get by multiple id", async () => {
                const userData = await service.getByIds([user_id_pw]);
                expect(userData.length).toBe(1);
                expect(userData[0]!.id).toBe(user_id_pw);

                //emails array
                expect(userData[0]!.emails[0]).toBe(testUserEmail.email);
                expect(userData[0]!.emails_verified[0]).toBe(false);
            });
            it("getByIds should return empty array if the user by multiple id does not exist", async () => {
                const userData = await service.getByIds([-1, -2]);
                expect(userData.length).toBe(0);
            });
            it("getAll should return all users", async () => {
                const userData = await service.getAll();
                expect(userData.length).toBeGreaterThanOrEqual(1);
            });
            it("search should return an array of users that match the search query", async () => {
                const userData = await service.search("users");
                expect(userData.length).toBeGreaterThanOrEqual(1);

                const userData2 = await service.search("unknown");
                expect(userData2.length).toBe(0);
            });
            describe("isAdmin", () => {
                it("should return true if the user is an admin", async () => {
                    const userData = await service.getById(user_id_pw);

                    await pool_permissions.query(
                        `INSERT INTO users_groups (user_id, group_id) VALUES (${userData.id}, 1)`,
                    );
                    const isAdmin = await service.isAdmin(userData.id);
                    expect(isAdmin).toBe(true);
                });

                it("should return false if the user is not an admin", async () => {
                    const userData = await service.getById(user_id_pw);
                    await pool_permissions.query(
                        `DELETE FROM users_groups WHERE user_id = ${user_id_pw}`,
                    );
                    const isAdmin = await service.isAdmin(userData.id);
                    expect(isAdmin).toBe(false);
                });
                afterAll(async () => {
                    await pool_permissions.query(
                        `DELETE FROM users_groups WHERE user_id = ${user_id_pw}`,
                    );
                });
            });
        });

        describe("not implemented yet", () => {
            it("should throw an error", async () => {
                await expect(service.insert({})).rejects.toThrow();
                await expect(service.update({ id: 0 })).rejects.toThrow();
                await expect(service.delete(0)).rejects.toThrow();
                await expect(service.mergeUsers(0, 0)).rejects.toThrow();
            });
        });

        async function clearLocalDBEntries() {
            // Clear the tables
            const user_ids = await pool_permissions.query(
                "SELECT user_id FROM credentials where email like '%@users.de'",
            );
            await pool_permissions.query(
                "UPDATE users SET primary_credential_id = NULL",
            );
            await pool_permissions.query(
                "DELETE FROM credentials where email like '%@users.de'",
            );
            await pool_permissions.query("DELETE FROM last_logins");
            if (user_ids.length === 0) {
                return;
            }
            await pool_permissions.query("DELETE FROM users where id in (?)", [
                user_ids.map((x: any) => x.user_id),
            ]);
        }
    },
);
