import { FolderService } from "../folder";
import { userService } from "../user";
import { pool_data, pool_permissions } from "src/sql.connection";
import { FolderDataInsert } from "src/types";
import { bookService } from "../book";

describe("FolderService", () => {
    let folderService = new FolderService();
    let folder: any;
    let u_id: number;

    beforeAll(async () => {
        await cleanup();
        u_id = await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then((res) => res.insertId);

        folder = await folderService.insert({
            name: "Test Folder",
            user_id: u_id,
            parent_id: null,
            comment: "Test Comment",
        });
    });

    describe("getById", () => {
        it("should return a folder by id", async () => {
            const f = await folderService.getById(folder.id);
            expect(f).toEqual(folder);
        });

        it("should throw NotFoundError if folder does not exist", async () => {
            await expect(folderService.getById(-1)).rejects.toThrowError(
                "not found",
            );
        });
    });

    describe("insert", () => {
        it("should create a new folder", async () => {
            const folderData: FolderDataInsert = {
                name: "New Folder",
                user_id: u_id,
                parent_id: null,
                comment: "New Comment",
            };
            const newFolder = await folderService.insert(folderData);
            expect(newFolder.name).toBe("New Folder");
            expect(newFolder.user_id).toBe(u_id);
            expect(newFolder.parent_id).toBe(null);
            expect(newFolder.comment).toBe("New Comment");
        });

        it("should throw an error if folder name already exists", async () => {
            const folderData: FolderDataInsert = {
                name: "Existing Folder",
                user_id: u_id,
                parent_id: null,
                comment: "New Comment",
            };
            await folderService.insert(folderData);

            await expect(folderService.insert(folderData)).rejects.toThrow(
                "Folder name already exists, choose another name!",
            );
        });

        it("too long name", async () => {
            const folderData: FolderDataInsert = {
                name: "a".repeat(4000),
                user_id: u_id,
                parent_id: null,
                comment: "New Comment",
            };
            await expect(folderService.insert(folderData)).rejects.toThrow(
                "Folder name is too long",
            );
        });
        it("without name", async () => {
            const folderData: FolderDataInsert = {
                name: "",
                user_id: u_id,
                parent_id: null,
                comment: "New Comment",
            };
            await expect(folderService.insert(folderData)).rejects.toThrow(
                "Folder name is not set!",
            );
        });
    });

    describe("delete", () => {
        it("should delete a folder", async () => {
            const result = await folderService.delete(1, 1);
            expect(result).toBe(true);
        });
    });

    describe("move", () => {
        it("should move a folder to a new location", async () => {
            const result = await folderService.move("folder", 1, 2, 1);
            expect(result).toBe(true);
        });

        it("should throw an error if type is invalid", async () => {
            await expect(
                // @ts-ignore
                folderService.move("invalid", 1, 2, 1),
            ).rejects.toThrow("Invalid type, must be 'folder' or 'book'.");
        });
    });

    describe("getFilesByUserId", () => {
        beforeAll(async () => {
            await userService.addSSOCredential(
                u_id,
                "test",
                "testasdasdasd11",
                "test@folder.de",
                true,
            );

            await folderService.insert({
                name: "Test Book",
                user_id: folder.user_id,
                comment: "Test Comment",
            });
            const book = await bookService.insert({
                title: "Test Book",
                owner_user_id: u_id,
            });
        });

        it("should return files associated with a user", async () => {
            const files = await folderService.getFilesByUserId(u_id);
            expect(files.length).toBeGreaterThan(0);
            expect(files[0]!.name).toBe("Test Book");
        });
    });

    describe("not implemented methods", () => {
        it("should throw an error for not implemented methods", async () => {
            await expect(folderService.getAll()).rejects.toThrow(
                "Method not implemented.",
            );
            await expect(folderService.getByIds([1, 2])).rejects.toThrow(
                "Method not implemented.",
            );
            await expect(folderService.update({ id: 1 })).rejects.toThrow(
                "Method not implemented.",
            );
            await expect(folderService.search("query")).rejects.toThrow(
                "Method not implemented.",
            );
        });
    });

    async function cleanup() {
        await pool_permissions.query("DELETE FROM user_config");
        await pool_permissions.query(
            "UPDATE users SET primary_credential_id = NULL",
        );
        await pool_permissions.query("DELETE FROM credentials");
        await pool_permissions.query("DELETE FROM collaboration_invites");
        await pool_data.query("DELETE FROM pages");
        await pool_data.query("DELETE FROM books");
        await pool_permissions.query("DELETE FROM users");
        await pool_data.query("DELETE FROM browserFolders");
    }
});
