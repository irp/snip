import { BlobData } from "../../types";
import { BlobService } from "../blob";

describe("BlobService", () => {
    let blobService: BlobService;
    let consoleSpy: any;

    beforeEach(() => {
        if (typeof consoleSpy === "function") {
            consoleSpy.mockRestore();
        }
    });

    beforeAll(async () => {
        blobService = new BlobService();
    });

    describe("insert", () => {
        it("hash and no hash", async () => {
            // With hash
            const blob = {
                mime: "image/png",
                size: 1024,
                data: Buffer.from("base64data").toString("base64"),
                hash: "hash",
            };

            const result: BlobData = await blobService.insert(blob);

            // Number greater than 0 is the ID of the blob
            expect(result.id).toBeGreaterThan(0);

            // Without hash
            const blob2 = {
                mime: "image/png",
                size: 1024,
                data: Buffer.from("base64data").toString("base64"),
            };
            const result2 = await blobService.insert(blob2);
            expect(result2.id).toBeGreaterThan(0);
            expect(result2.hash).not.toBe(null);
        });
        it("no size given", async () => {
            const blob = {
                mime: "image/png",
                data: Buffer.from("base64data").toString("base64"),
                size: 0,
            };
            const result = await blobService.insert(blob);
            expect(result.size).toBe(10);
        });
    });

    describe("getById", () => {
        it("should return the same blob that was inserted", async () => {
            let blob = {
                mime: "image/png",
                size: 1024,
                data: Buffer.from("base64data").toString("base64"),
            };
            let blob_r = await blobService.insert(blob);

            const result = await blobService.getById((blob as BlobData).id);

            expect(result.created!.getTime()).toBeCloseTo(
                blob_r.created!.getTime(),
                -4,
            );
            expect(result.mime).toBe(blob_r.mime);
            expect(result.size).toBe(blob_r.size);
            expect(result.data).toBe(blob_r.data);
            expect(result.hash).toBe(blob_r.hash);
            expect(result.id).toBe(blob_r.id);
            expect(result.last_updated!.getTime()).toBeCloseTo(
                blob_r.last_updated!.getTime(),
                -4,
            );
        });

        it("should throw if the blob does not exist", async () => {
            consoleSpy = vi
                .spyOn(console, "error")
                .mockImplementation(() => {});
            await expect(blobService.getById(88888)).rejects.toThrow();
        });
    });

    describe("getByIds", () => {
        it("should return an array of blobs with the given IDs", async () => {
            const blob1 = {
                mime: "image/png",
                size: 1024,
                data: Buffer.from("base64data1").toString("base64"),
            };
            const blob2 = {
                mime: "image/jpeg",
                size: 2048,
                data: Buffer.from("base64data2").toString("base64"),
            };

            const insertedBlob1 = await blobService.insert(blob1);
            const insertedBlob2 = await blobService.insert(blob2);

            const result = await blobService.getByIds([
                insertedBlob1.id,
                insertedBlob2.id,
            ]);

            expect(result.length).toBe(2);
            expect(result[0]!.id).toBe(insertedBlob1.id);
            expect(result[1]!.id).toBe(insertedBlob2.id);
        });

        it("should throw if any of the blobs do not exist", async () => {
            await expect(
                blobService.getByIds([88888, 99999]),
            ).rejects.toThrow();
        });
    });

    describe("update", () => {
        it("should update the blob in the database", async () => {
            // Create a new blob
            const blob = {
                mime: "image/png",
                size: 1024,
                data: Buffer.from("base64data").toString("base64"),
                created: new Date(),
            };
            const insertedBlob = await blobService.insert(blob);

            // Update the blob data
            const updatedBlob = {
                ...insertedBlob,
                mime: "image/jpeg",
                size: 2048,
            };
            const result = await blobService.update(
                structuredClone(updatedBlob),
            );

            expect_same(result, updatedBlob, 6);

            // Retrieve the updated blob from the database
            const retrievedBlob = await blobService.getById(insertedBlob.id);

            // Check if the blob data has been updated correctly
            expect_same(result, retrievedBlob);
        });

        it("should throw if the blob does not exist", async () => {
            // Try to update a non-existent blob
            const blob = {
                id: 88888,
                mime: "image/png",
                size: 1024,
                data: Buffer.from("base64data").toString("base64"),
            };
            await expect(blobService.update(blob)).rejects.toThrow();
        });
    });

    describe("delete", () => {
        it("should delete the blob from the database", async () => {
            // Create a new blob
            const blob = {
                mime: "image/png",
                size: 1024,
                data: Buffer.from("base64data").toString("base64"),
            };
            const insertedBlob = await blobService.insert(blob);

            // Delete the blob from the database
            const result = await blobService.delete(insertedBlob.id);

            // Check if the deletion was successful
            expect(result).toBe(true);
        });

        it("should throw if the blob does not exist", async () => {
            // Try to delete a non-existent blob
            await expect(blobService.delete(88888)).rejects.toThrow();
        });
    });

    describe("getAll|search", () => {
        it("should return an array of blobs", async () => {
            await expect(blobService.getAll()).rejects.toThrow();
            await expect(blobService.search("asd")).rejects.toThrow();
        });
    });
});

function expect_same(A: BlobData, B: BlobData, time_diff = 4) {
    expect(A.mime).toBe(B.mime);
    expect(A.size).toBe(B.size);
    expect(A.data).toBe(B.data);
    expect(A.hash).toBe(B.hash);
    expect(A.last_updated!.getTime()).toBeCloseTo(
        B.last_updated!.getTime(),
        -1 * time_diff,
    );
    expect(A.created!.getTime()).toBeCloseTo(
        B.created!.getTime(),
        -1 * time_diff,
    );
}
