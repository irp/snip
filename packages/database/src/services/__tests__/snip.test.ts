import { SnipService } from "../snip";
import {
    BaseView,
    BlobData,
    BookDataRet as BookData,
    ID,
    PageData,
    UserDataRet as UserData,
    PartialBy,
    SnipDataInsert,
    SnipDataRet,
} from "../../types";
import { userService } from "../user";
import { bookService } from "../book";
import { blobService } from "../blob";
import { pageService } from "../page";
import { pool_data, pool_permissions } from "../../sql.connection";

describe("SnipService", () => {
    let service: SnipService = new SnipService();
    let user: UserData;
    let book: BookData;
    let blob: BlobData;
    let page: PageData;
    let consoleSpy: any;

    async function deleteCleanup() {
        await pool_data.query("DELETE FROM snips");
        await pool_data.query("DELETE FROM pages");
        await pool_permissions.query(
            "UPDATE users SET primary_credential_id = NULL",
        );
        await pool_permissions.query("DELETE FROM collaboration_invites");
        await pool_data.query("DELETE FROM books");
        await pool_permissions.query("DELETE FROM credentials");
        await pool_permissions.query("DELETE FROM users");
    }

    beforeAll(async () => {
        // Create a user
        await deleteCleanup();
        const user_id = await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then(async (res) => {
                return res.insertId;
            });
        user = await userService.getById(user_id);

        // Create a book
        book = await bookService.insert({
            title: "Test Book",
            comment: "Test description",
            owner_user_id: user.id,
        });
        // Create a page
        page = await pageService.insert({
            book_id: book.id,
        });

        //Create a blob
        blob = await blobService.insert({
            mime: "text/plain",
            data: "test",
            size: 4,
        });
    });

    beforeEach(async () => {
        if (typeof consoleSpy === "function") {
            consoleSpy.mockRestore();
        }

        // Delete all snips
        await pool_data.query("DELETE FROM snips");
    });

    describe("getAll", () => {
        it("not implemented", async () => {
            await expect(service.getAll()).rejects.toThrow(
                "Method not implemented.",
            );
        });
    });

    describe("insert", () => {
        it("basic snip", async () => {
            const snip = get_test_snip(book.id, user.id);

            const snipData = await service.insert(snip);

            expect_same(snipData, snip);
            expect(snipData.id).toBeDefined();
            expect(snipData.created).toBeDefined();
            expect(snipData.last_updated).toBeDefined();
        });

        it("with blob (initialized) in data", async () => {
            consoleSpy = vi.spyOn(console, "warn").mockImplementation(() => {});
            const snip = get_test_snip(book.id, user.id, blob);

            const snipData = await service.insert(structuredClone(snip));
            expect_same(snipData, snip);
            // do not have the same id
            expect_same_blob(snipData.data.blob!, blob, false);
            expect(snipData.data.blob!.id).not.toBe(blob.id);

            // should warn about blob id change
            expect(consoleSpy).toHaveBeenCalled();
        });
        it("with blob (uninitialized) in data", async () => {
            const snip = get_test_snip(book.id, user.id, {
                mime: "text/plain",
                data: "test",
                size: 4,
            });

            const snipData = await service.insert(snip);

            expect_same(snipData, snip);
            expect(snipData.data.blob!.id).toBeDefined();
        });

        it("with snips (uninitialized) in data", async () => {
            const inner_snip = get_test_snip(book.id, user.id);
            const snip = get_test_snip(book.id, user.id, undefined, [
                structuredClone(inner_snip),
                structuredClone(inner_snip),
            ]);

            const snipData = await service.insert(snip);

            expect_same(snipData, snip);

            expect(snipData.data.snips!.length).toBe(2);
            expect_same(snipData.data.snips![0]!, inner_snip);
            expect_same(snipData.data.snips![1]!, inner_snip);
            //Ids defined
            expect(snipData.data.snips![0]!.id).toBeDefined();
            expect(snipData.data.snips![1]!.id).toBeDefined();
        });
        it("with snips (initialized) in data", async () => {
            consoleSpy = vi.spyOn(console, "warn").mockImplementation(() => {});
            const inner_snip_1 = get_test_snip(book.id, user.id);
            const inner_snip_2 = get_test_snip(book.id, user.id);

            //Insert
            const inner_snip_1_data = await service.insert(inner_snip_1);
            const inner_snip_2_data = await service.insert(inner_snip_2);

            const snip = get_test_snip(book.id, user.id, undefined, [
                inner_snip_1_data,
                inner_snip_2_data,
            ]);

            const snipData = await service.insert(snip);

            expect_same(snipData, snip);
            expect_same(snipData.data.snips![0]!, inner_snip_1_data);
            expect_same(snipData.data.snips![1]!, inner_snip_2_data);
            //Ids defined
            expect(snipData.data.snips![0]!.id).toBeDefined();
            expect(snipData.data.snips![1]!.id).toBeDefined();
            // should warn about blob id change
            expect(consoleSpy).toHaveBeenCalledTimes(2);
        });

        it("with snip in data", async () => {
            const inner_snip = get_test_snip(book.id, user.id);
            const snip = get_test_snip(
                book.id,
                user.id,
                undefined,
                undefined,
                inner_snip,
            );

            const snipData = await service.insert(snip);

            expect_same(snipData, snip);
            expect_same(snipData.data.snip!, inner_snip);
            //Id defined
            expect(snipData.data.snip!.id).toBeDefined();
        });

        it("snip with rx,ry, rw, rh, width", async () => {
            const snip = get_test_snip(book.id, user.id);
            snip.view = {
                x: 0,
                y: 0,
                rx: 0,
                ry: 0,
                rw: 100,
                rh: 100,
                width: 100,
            } as BaseView & {
                rx: number;
                ry: number;
                rw: number;
                rh: number;
                width: number;
            };

            const snipData = (await service.insert(snip)) as SnipDataRet & {
                view: BaseView & {
                    rx: number;
                    ry: number;
                    rw: number;
                    rh: number;
                    width: number;
                };
            };

            const insertedSnip = (await service.getById(
                snipData.id,
            )) as SnipDataRet & {
                view: BaseView & {
                    rx: number;
                    ry: number;
                    rw: number;
                    rh: number;
                    width: number;
                };
            };

            expect_same(snipData, insertedSnip);

            expect(insertedSnip.view.x).toBe(snipData.view.x);
            expect(insertedSnip.view.y).toBe(snipData.view.y);
            expect(insertedSnip.view.rx).toBe(snipData.view.rx);
            expect(insertedSnip.view.ry).toBe(snipData.view.ry);
            expect(insertedSnip.view.rw).toBe(snipData.view.rw);
            expect(insertedSnip.view.rh).toBe(snipData.view.rh);
            expect(insertedSnip.view.width).toBe(snipData.view.width);
        });
    });

    describe("getById|getByIds|getByPageId", () => {
        it("should retrieve a snip by its ID(s)", async () => {
            const snip = get_test_snip(book.id, user.id);
            const snipData = await service.insert(structuredClone(snip));
            const retrievedSnipData = await service.getById(snipData.id);
            expect_same(retrievedSnipData, snipData, true);

            // by ids
            const retrievedSnipData2 = await service.getByIds([snipData.id]);
            expect(retrievedSnipData2.length).toBe(1);
            expect_same(retrievedSnipData2[0]!, snipData, true);

            // by page id
            const snipData2 = await service.insert({
                ...snip,
                page_id: page.id,
            });
            const retrievedSnipData3 = await service.getByPageId(page.id);
            expect_same(retrievedSnipData3[0]!, snipData2, true);
        });

        it("should get nested snips", async () => {
            const inner_snip = get_test_snip(book.id, user.id);
            const snip = get_test_snip(
                book.id,
                user.id,
                undefined,
                undefined,
                inner_snip,
            );

            const snipData = await service.insert(snip);
            const retrievedSnipData = await service.getById(snipData.id);

            expect_same(retrievedSnipData, snipData, true);
            expect_same(retrievedSnipData.data.snip!, inner_snip);

            // Multiple snips
            const inner_snip_1 = get_test_snip(book.id, user.id);
            const inner_snip_2 = get_test_snip(book.id, user.id);
            const snip_2 = get_test_snip(book.id, user.id, undefined, [
                inner_snip_1,
                inner_snip_2,
            ]);

            const snipData_2 = await service.insert(snip_2);
            const retrievedSnipData_2 = await service.getById(snipData_2.id);

            expect_same(retrievedSnipData_2, snipData_2, true);
            expect_same(retrievedSnipData_2.data.snips![0]!, inner_snip_1);
            expect_same(retrievedSnipData_2.data.snips![1]!, inner_snip_2);
        });

        it("should throw an error if the snip(s) not found", async () => {
            await expect(service.getById(-1)).rejects.toThrow();

            await expect(service.getByIds([-1])).rejects.toThrow();

            await expect(service.getByPageId(-1)).resolves.toEqual([]);
        });
    });

    describe("getQueuedByBookId", () => {
        it("should get the queued snips", async () => {
            const snip = get_test_snip(book.id, user.id);

            // Insert with page id
            const snipData_p = await service.insert({
                ...snip,
                page_id: page.id,
            });
            // Insert without page id
            const snipData = await service.insert(snip);

            const retrievedSnipData = await service.getQueuedByBookId(book.id);
            expect(retrievedSnipData.length).toBe(1);
            expect_same(retrievedSnipData[0]!, snipData, true);
        });
    });

    describe("update", () => {
        it("should update a snip", async () => {
            const inner_snip = get_test_snip(book.id, user.id);
            const snip = get_test_snip(
                book.id,
                user.id,
                { ...blob!, id: undefined },
                [structuredClone(inner_snip), structuredClone(inner_snip)],
                structuredClone(inner_snip),
            );

            const snipData = await service.insert(snip);

            const updatedSnipData = await service.update({
                ...snipData,
                data: {
                    ...snipData.data,
                    type: "updated",
                },
            });

            expect_same(updatedSnipData, {
                ...snipData,
                data: {
                    ...snipData.data,
                    type: "updated",
                },
            });
        });

        it("should throw an error if the snip does not exist", async () => {
            await expect(
                service.update({
                    ...get_test_snip(book.id, user.id, undefined, undefined),
                    id: -1,
                }),
            ).rejects.toThrow();
        });
    });

    describe("delete|nestedDelete", () => {
        it("should delete a snip", async () => {
            const snip = get_test_snip(book.id, user.id);
            const snipData = await service.insert(snip);

            await expect(service.delete(snipData.id)).resolves.toBe(true);
        });

        it("should throw an error if the snip does not exist", async () => {
            await expect(service.delete(-1)).rejects.toThrow();
        });

        it("should delete nested snips", async () => {
            const inner_snip = get_test_snip(book.id, user.id);
            const snip = get_test_snip(
                book.id,
                user.id,
                { ...blob!, id: undefined },
                [structuredClone(inner_snip), structuredClone(inner_snip)],
                structuredClone(inner_snip),
            );

            const snipData = await service.insert(snip);

            expect(snipData.data.snip!.id).toBeDefined();
            expect(snipData.data.snips![0]!.id).toBeDefined();
            expect(snipData.data.snips![1]!.id).toBeDefined();
            expect(snipData.data.blob!.id).toBeDefined();

            const success = await service.deleteNested(snipData.id!);
            expect(success).toBe(true);

            //Check if nested snip is deleted
            await expect(
                service.getById(snipData.data.snip!.id),
            ).rejects.toThrow();
            await expect(
                service.getByIds(snipData.data.snips!.map((s) => s.id)),
            ).rejects.toThrow();
            await expect(
                blobService.getById(snipData.data.blob!.id),
            ).rejects.toThrow();
        });
    });

    describe("search", () => {
        it("should return an array of snips", async () => {
            await expect(blobService.search("asd")).rejects.toThrow();
        });
    });
});

function get_test_snip(
    book_id: ID,
    user_id: ID,
    blob?: PartialBy<BlobData, "id">,
    snips?: SnipDataInsert[],
    snip?: SnipDataInsert,
): SnipDataInsert {
    const s: SnipDataInsert = {
        data: {},
        view: {
            x: 0,
            y: 0,
        },
        book_id: book_id,
        type: "test",
        created_by: user_id,
    };

    if (blob) {
        s.data.blob = blob as BlobData;
    }
    if (snips) {
        s.data.snips = snips;
    }
    if (snip) {
        s.data.snip = snip;
    }

    return s;
}

function expect_same(a: SnipDataInsert, b: SnipDataInsert, check_id = false) {
    if (check_id) {
        expect(a.id).toBe(b.id);
    }
    expect(a.book_id).toBe(b.book_id);
    expect(a.page_id == b.page_id).toBeTruthy();
}

function expect_same_blob(a: BlobData, b: BlobData, check_id = false) {
    if (check_id) {
        expect(a.id).toBe(b.id);
    }

    expect(a.mime).toBe(b.mime);
    expect(a.data).toBe(b.data);
    expect(a.created!.getTime()).toBeCloseTo(b.created!.getTime(), -3);
    expect(a.last_updated!.getTime()).toBeCloseTo(
        b.last_updated!.getTime(),
        -3,
    );
}
