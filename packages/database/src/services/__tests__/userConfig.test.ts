import { UserConfigService } from "../userConfig";
import { pool_data, pool_permissions } from "../../sql.connection";

describe("UserConfigService", () => {
    let service: UserConfigService = new UserConfigService();
    let user_id: number;
    let config_id: number;

    async function deleteCleanup() {
        await pool_permissions.query("DELETE FROM user_config");
        await pool_permissions.query(
            "UPDATE users SET primary_credential_id = NULL",
        );
        await pool_permissions.query("DELETE FROM credentials");
        await pool_permissions.query("DELETE FROM collaboration_invites");
        await pool_data.query("DELETE FROM pages");
        await pool_data.query("DELETE FROM books");
        await pool_permissions.query("DELETE FROM users");
    }

    beforeAll(async () => {
        await deleteCleanup();

        user_id = await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then((res) => {
                return res.insertId;
            });

        config_id = await service
            .insert({
                user_id,
            })
            .then((res) => {
                return res.id;
            });
    });

    afterAll(async () => {
        await deleteCleanup();
    });

    describe("queries", () => {
        it("getById", async () => {
            const userConfigData = await service.getById(config_id);
            expect(userConfigData.user_id).toBe(user_id);

            await expect(service.getById(-1)).rejects.toThrow();
        });

        it("getAll", async () => {
            const userConfigData = await service.getAll();
            expect(userConfigData.length).toBeGreaterThanOrEqual(1);
        });

        it("getByIds", async () => {
            await expect(service.getByIds([user_id])).rejects.toThrow(
                "Method not implemented.",
            );
        });

        it("getByUserId", async () => {
            const userConfigData = await service.getByUserId(user_id);
            expect(userConfigData.id).toBe(config_id);

            await expect(service.getByUserId(-1)).rejects.toThrow();
        });

        it("search", async () => {
            await expect(service.search("asd")).rejects.toThrow(
                "Method not implemented.",
            );
        });
    });

    describe("insert|delete", () => {
        it("should insert a new user configuration", async () => {
            const newUser = await pool_permissions
                .query("INSERT INTO users (`id`) VALUES (NULL)")
                .then((res) => {
                    return res.insertId;
                });

            const userConfigData = await service.insert({
                user_id: newUser,
            });
            expect(userConfigData.id).toBeDefined();

            //cleanup
            await expect(service.delete(userConfigData.id)).resolves.toBe(true);
        });
        it("should throw an error if the user does not exist", async () => {
            await expect(service.insert({ user_id: -1 })).rejects.toThrow();

            await expect(service.delete(-1)).rejects.toThrow();
        });
        it("should throw an error if the user configuration already exists", async () => {
            await expect(service.insert({ user_id })).rejects.toThrow();
        });
    });

    describe("update|updateByUserId", () => {
        it("should partially update a user configuration", async () => {
            //normal update by id
            const userConfigData = await service.update({
                text_font: "Caveat",
                id: config_id,
                pen_size: 20,
            });
            expect(userConfigData.text_font).toBe("Caveat");
            expect(userConfigData.pen_size).toBe(20);

            //update by user_id
            const userConfigData2 = await service.updateByUserId({
                text_font: "Arial",
                user_id: user_id,
                pen_size: 40,
            });

            expect(userConfigData2.text_font).toBe("Arial");
            expect(userConfigData2.pen_size).toBe(40);
        });

        it("should throw an error if the user configuration does not exist", async () => {
            await expect(service.update({ id: -1 })).rejects.toThrow();

            await expect(
                service.updateByUserId({
                    user_id: -1,
                }),
            ).rejects.toThrow();

            await expect(
                service.updateByUserId({
                    id: config_id,
                    text_font: "Arial",
                } as any),
            ).rejects.toThrow("user_id is required for updating user config");
        });

        it("should throw an error if no update fields are provided", async () => {
            await expect(service.update({ id: config_id })).rejects.toThrow();

            await expect(
                service.updateByUserId({ user_id: user_id }),
            ).rejects.toThrow();
        });

        it("should throw if the font does not exist", async () => {
            await expect(
                service.update({ id: config_id, text_font: "not_a_font" }),
            ).rejects.toThrow();

            await expect(
                service.updateByUserId({
                    user_id,
                    text_font: "not_a_font",
                }),
            ).rejects.toThrow();
        });
    });
});
