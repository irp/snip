import { CollaboratorsInviteService } from "../collaborators";
import {
    BookDataRet,
    CollaborationInviteDataRet,
    UserDataRet as UserData,
} from "../../types";
import { pool_data, pool_permissions } from "../../sql.connection";
import { bookService } from "../book";
import { userService } from "../user";
import {
    InviteAlreadyAcceptedError,
    InviteExpiredError,
    NotFoundError,
} from "../../errors";

describe("CollaboratorInviteService", () => {
    const service = new CollaboratorsInviteService();
    let consoleSpy;
    let bookData: BookDataRet;
    let userData: UserData[];

    async function cleanup() {
        await pool_permissions.query("DELETE FROM collaboration_invites");
        await pool_data.query("DELETE FROM books");
        await pool_permissions.query(
            "UPDATE users SET primary_credential_id = NULL",
        );
        await pool_permissions.query("DELETE FROM credentials");
        await pool_permissions.query("DELETE FROM users");
    }

    beforeAll(async () => {
        await cleanup();

        // Insert two users
        const u1_id = await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then((res) => res.insertId);
        await userService.addSSOCredential(
            u1_id,
            "test",
            "test_12312312",
            "test@collaborate.de",
            true,
        );
        const u1 = await userService.getById(u1_id);

        const u2_id = await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then((res) => res.insertId);

        await userService.addSSOCredential(
            u2_id,
            "test",
            "test_1231231a2",
            "test1@collaborate.de",
            true,
        );
        const u2 = await userService.getById(u2_id);
        userData = [u1, u2];

        bookData = await bookService.insert({
            title: "test",
            owner_user_id: u1.id,
        });
    });

    describe("insert", () => {
        it("insert", async () => {
            const invite = await service.insert({
                book_id: bookData.id,
                user_id: userData[1]!.id,
                invited_by: userData[0]!.id,
            });

            expect(invite.book_id).toBe(bookData.id);
            expect(invite.user_id).toBe(userData[1]!.id);
            expect(invite.invited_by).toBe(userData[0]!.id);

            //emails
            expect(invite.emails).toContain(userData[1]!.emails[0]);
            expect(invite.invited_by_emails).toContain(userData[0]!.emails[0]);

            // Check expire at is set and approx 48 hours in the future
            expect(invite.expire_at).toBeInstanceOf(Date);
            expect(invite.expire_at!.getTime()).toBeGreaterThan(Date.now());
            expect(invite.expire_at!.getTime()).toBeLessThan(
                Date.now() + 1000 * 60 * 60 * 48,
            );
            expect(invite.expire_at!.getTime()).toBeGreaterThan(
                Date.now() + 1000 * 60 * 60 * 47,
            );
        });
    });

    describe("get", () => {
        let pending: CollaborationInviteDataRet;
        let accepted: CollaborationInviteDataRet;

        beforeAll(async () => {
            pending = await service.insert({
                book_id: bookData.id,
                user_id: userData[1]!.id,
                invited_by: userData[0]!.id,
            });

            accepted = await service.insert({
                book_id: bookData.id,
                user_id: userData[0]!.id,
                invited_by: userData[1]!.id,
            });

            await pool_permissions.query(
                `UPDATE collaboration_invites SET accepted_at = NOW() WHERE id = ?`,
                [accepted.id],
            );
        });

        it("getById", async () => {
            const result = await service.getById(pending.id!);

            expect(result).toMatchObject(pending);

            expect(result.accepted_at).toBeNull();

            // throw on not found
            await expect(service.getById(-1)).rejects.toThrow(NotFoundError);
        });

        it("getByUserId", async () => {
            const result = await service.getByUserId(userData[1]!.id);
            for (const r of result) {
                expect(r.user_id).toBe(userData[1]!.id);
            }
        });

        it("getByBookId", async () => {
            const result = await service.getByBookId(bookData.id);

            for (const r of result) {
                expect(r.book_id).toBe(bookData.id);
            }
        });

        it("getPendingByBookId", async () => {
            const result = await service.getPendingByBookId(bookData.id);

            for (const r of result) {
                expect(r.book_id).toBe(bookData.id);
                expect(r.accepted_at).toBeNull();
            }
        });

        it("getPendingByUserId", async () => {
            const result = await service.getPendingByUserId(userData[1]!.id);

            for (const r of result) {
                expect(r.user_id).toBe(userData[1]!.id);
                expect(r.accepted_at).toBeNull();
            }
        });

        it("getPendingByBookIdAndUserId", async () => {
            const result = await service.getPendingByBookIdAndUserId(
                bookData.id,
                userData[1]!.id,
            );

            for (const r of result) {
                expect(r.book_id).toBe(bookData.id);
                expect(r.user_id).toBe(userData[1]!.id);
                expect(r.accepted_at).toBeNull();
            }
        });
    });

    describe("accept", () => {
        let pending: any;

        beforeAll(async () => {
            pending = await service.insert({
                book_id: bookData.id,
                user_id: userData[1]!.id,
                invited_by: userData[0]!.id,
            });
        });

        it("acceptById", async () => {
            const result = await service.acceptById(pending.id);

            expect(result).toMatchObject({
                ...pending,
                accepted_at: expect.any(Date),
            });
            expect(result.accepted_at!.getTime()).toBeGreaterThan(
                Date.now() - 1000,
            );
        });

        it("acceptById should throw if invite does not exist", async () => {
            await expect(service.acceptById(-1)).rejects.toThrow(NotFoundError);
        });

        it("acceptById should throw if invite is already accepted", async () => {
            await pool_permissions.query(
                `UPDATE collaboration_invites SET accepted_at = NOW() WHERE id = ?`,
                [pending.id],
            );

            await expect(service.acceptById(pending.id)).rejects.toThrow(
                InviteAlreadyAcceptedError,
            );
        });

        it("acceptById should throw if invite is expired", async () => {
            await pool_permissions.query(
                `UPDATE collaboration_invites SET expire_at = NOW() WHERE id = ?`,
                [pending.id],
            );

            await expect(service.acceptById(pending.id)).rejects.toThrow(
                InviteExpiredError,
            );
        });
    });
});
