import { BackgroundTypeData } from "../../types";
import { BackgroundService } from "../backgroundType";
import { bookService } from "../book";
import { pageService } from "../page";
import { pool_permissions } from "src/sql.connection";

describe("BackgroundService", () => {
    let bgService: BackgroundService;
    let book_id: number;
    let user_id: number;
    let page_ids: number[] = [];
    let consoleSpy: any;

    beforeEach(() => {
        if (typeof consoleSpy === "function") {
            consoleSpy.mockRestore();
        }
    });

    beforeAll(async () => {
        bgService = new BackgroundService();

        // Setup some data
        user_id = await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then((res) => res.insertId);
        const book = await bookService.insert({
            title: "book",
            comment: "description",
            owner_user_id: user_id,
        });
        book_id = book.id;

        for (let i = 1; i < 6; i++) {
            const page = await pageService.insert({
                book_id: book_id,
            });
            page_ids.push(page.id);
        }
    });
    afterAll(async () => {
        for (const pageId of page_ids) {
            await pageService.delete(pageId);
        }
        await bookService.delete(book_id);
        await pool_permissions.query("DELETE FROM users WHERE id = ?", [
            user_id,
        ]);
    });

    it("not implemented", async () => {
        await expect(
            bgService.insert({} as BackgroundTypeData),
        ).rejects.toThrow();
        await expect(bgService.delete(1)).rejects.toThrow();
        await expect(
            bgService.update({
                id: 1,
                name: "name",
                description: "description",
            }),
        ).rejects.toThrow();
        await expect(bgService.search("test")).rejects.toThrow();
    });

    describe("getAll", () => {
        it("should return an array of background types", async () => {
            const result = await bgService.getAll();
            expect(result.length).toBeGreaterThan(0);
        });
    });

    describe("getById", () => {
        it("should return a background type", async () => {
            const result = await bgService.getById(1);
            expect(result).toBeTruthy();
        });

        it("should throw an error if the background type is not found", async () => {
            await expect(bgService.getById(-1)).rejects.toThrow();
        });
    });

    describe("getByIds", () => {
        it("should return an array of background types", async () => {
            const result = await bgService.getByIds([1]);
            expect(result.length).toBe(1);
        });

        it("should throw an error if the background types are not found", async () => {
            await expect(bgService.getByIds([-1, -2, -3])).rejects.toThrow();
        });
    });

    describe("setBookDefault", () => {
        it("should update the default background type of a book", async () => {
            const backgroundTypeId = 2;
            const result = await bgService.setBookDefault(
                book_id,
                backgroundTypeId,
            );
            expect(result).toBe(true);
        });

        it("should throw an error if the book is not found", async () => {
            const bookId = -1;
            const backgroundTypeId = 2;
            await expect(
                bgService.setBookDefault(bookId, backgroundTypeId),
            ).rejects.toThrow();
        });
    });

    describe("setPageById", () => {
        it("should update the background type of a page", async () => {
            for (const pageId of page_ids) {
                const backgroundTypeId = 2;
                const result = await bgService.setPageById(
                    book_id,
                    pageId,
                    backgroundTypeId,
                );
                expect(result).toBe(true);
            }
        });

        it("should throw an error if the page is not found", async () => {
            const bookId = 1;
            const pageId = -1;
            const backgroundTypeId = 3;
            await expect(
                bgService.setPageById(bookId, pageId, backgroundTypeId),
            ).rejects.toThrow();
        });
    });

    describe("setPageByIds", () => {
        it("should update the background type of multiple pages", async () => {
            const backgroundTypeId = 2;
            const result = await bgService.setPageByIds(
                book_id,
                page_ids,
                backgroundTypeId,
            );
            expect(result).toBe(true);
        });

        it("should throw an error if any of the pages are not found", async () => {
            const bookId = 1;
            const pageIds = [2, -3, 4];
            const backgroundTypeId = 5;
            await expect(
                bgService.setPageByIds(bookId, pageIds, backgroundTypeId),
            ).rejects.toThrow();
        });
    });

    describe("setPageByNumber", () => {
        it("should update the background type of a page by page number", async () => {
            const pageNumber = 2;
            const backgroundTypeId = 2;
            const result = await bgService.setPageByNumber(
                book_id,
                pageNumber,
                backgroundTypeId,
            );
            expect(result).toBe(true);
        });

        it("should throw an error if the page is not found", async () => {
            const bookId = 1;
            const pageNumber = -2;
            const backgroundTypeId = 3;
            await expect(
                bgService.setPageByNumber(bookId, pageNumber, backgroundTypeId),
            ).rejects.toThrow();
        });
    });

    describe("setPageByNumbers", () => {
        it("should update the background type of multiple pages by page numbers", async () => {
            const pageNumbers = [2, 3, 4];
            const backgroundTypeId = 2;
            const result = await bgService.setPageByNumbers(
                book_id,
                pageNumbers,
                backgroundTypeId,
            );
            expect(result).toBe(true);
        });

        it("should throw an error if any of the pages are not found", async () => {
            const bookId = 1;
            const pageNumbers = [2, -3, 4];
            const backgroundTypeId = 5;
            await expect(
                bgService.setPageByNumbers(
                    bookId,
                    pageNumbers,
                    backgroundTypeId,
                ),
            ).rejects.toThrow();
        });
    });
});
