import { BookService } from "../book";
import {
    BookDataInsert,
    BookDataRet,
    BookOwner,
    ENTITY_GROUP,
    ENTITY_USER,
    Entity,
    ID,
    PartialBy,
} from "../../types";
import { pool_data, pool_permissions } from "../../sql.connection";
import { NotFoundError } from "../../errors";

describe("BookService", () => {
    let bookService = new BookService();
    let consoleSpy: any;
    let user_ids: ID[] = [];
    let group_ids: ID[] = [];
    let default_book = {
        title: "Test.book",
        comment: "This is a test book",
        finished: null,
        owner_user_id: user_ids[0],
        owner_group_id: null,
        cover_page_id: null,
        // Placeholder date to check if it is updated
        created: new Date(),
        last_updated: new Date(),
    };

    async function cleanup() {
        await pool_permissions.query("DELETE FROM collaboration_invites");
        await pool_permissions.query(
            "UPDATE users SET primary_credential_id = NULL",
        );
        await pool_permissions.query("DELETE FROM credentials");
        await pool_data.query("DELETE FROM pages");
        await pool_data.query("DELETE FROM books");
        await pool_permissions.query("DELETE FROM users");
    }

    beforeEach(() => {
        if (typeof consoleSpy === "function") {
            consoleSpy.mockRestore();
        }
        consoleSpy = vi.spyOn(console, "warn").mockImplementation(() => {});
    });

    beforeAll(async () => {
        await cleanup();
        // Insert user and group for testing otherwise
        // the foreign key constraints will fail

        await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then((res) => {
                user_ids.push(res.insertId);
            });
        await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then((res) => {
                user_ids.push(res.insertId);
            });
        await pool_permissions.query(
            "INSERT INTO credentials (`email`,`user_id`) VALUES ('test@book.de', ?)",
            [user_ids[0]],
        );
        await pool_permissions.query(
            "INSERT INTO credentials (`email`,`user_id`) VALUES ('test2@book.de', ?)",
            [user_ids[1]],
        );
        await pool_permissions
            .query(
                "INSERT INTO groups (`name`, `description`) VALUES ('book', 'test')",
            )
            .then((res) => {
                group_ids.push(res.insertId);
            });
        default_book.owner_user_id = user_ids[0];

        return;
    });

    afterAll(async () => {
        await cleanup();
    });

    describe("insert", () => {
        it("should insert a new book into the database", async () => {
            const result = await bookService.insert(
                structuredClone(default_book),
            );
            compare_insert(default_book, result);

            // Only with title and owner
            const result2 = await bookService.insert({
                title: "test2.book",
                owner_group_id: group_ids[0],
            });
            expect(result2.title).toBe("test2.book");
        });
    });

    describe("getById", () => {
        it("should return the same book that was inserted", async () => {
            const book = await bookService.insert(
                structuredClone(default_book),
            );

            const result = await bookService.getById(book.id);

            expect_same(result, book);
            expect(result.id).toBe(book.id);
        });

        it("should throw an error if the book does not exist", async () => {
            await expect(bookService.getById(-1)).rejects.toThrow(
                NotFoundError,
            );
        });
    });

    describe("getByIds", () => {
        it("should return the same books that were inserted", async () => {
            const book1 = await bookService.insert(
                structuredClone(default_book),
            );
            const book2 = await bookService.insert(
                structuredClone(default_book),
            );

            const result = await bookService.getByIds([book1.id, book2.id]);

            expect(result.length).toBe(2);
            expect_same(result[0]!, book1);
            expect_same(result[1]!, book2);
        });

        it("should gracefully handle empty arrays", async () => {
            const result = await bookService.getByIds([]);
            expect(result.length).toBe(0);
        });

        it("should return an partial array if any of the books do not exist", async () => {
            const books = await bookService.getByIds([88888, 99999]);
            expect(books.length).toBe(0);

            const book1 = await bookService.insert(
                structuredClone(default_book),
            );
            const books2 = await bookService.getByIds([book1.id, 99999]);
            expect(books2.length).toBe(1);
            // Expect warn
            expect(consoleSpy).toHaveBeenCalled();
        });
    });

    describe("getAll", () => {
        it("should return all books in the database", async () => {
            const book1 = await bookService.insert(
                structuredClone(default_book),
            );
            const book2 = await bookService.insert(
                structuredClone(default_book),
            );

            const result = await bookService.getAll();

            expect(result.length).toBeGreaterThanOrEqual(2);
        });
    });

    describe("update", () => {
        it("should update the book in the database", async () => {
            const book = await bookService.insert(
                structuredClone(default_book),
            );

            const updatedBook = {
                ...default_book,
                title: "test3.book",
                comment: "This is an updated test book",
                id: book.id,
            };
            await new Promise((resolve) => setTimeout(resolve, 1000));
            const result = await bookService.update(
                structuredClone(updatedBook),
            );

            compare_insert(updatedBook, result, 10);

            //Check last_updated changed
            expect(result.last_updated!.getTime()).toBeGreaterThan(
                book.last_updated!.getTime(),
            );
        });

        it("should throw an error if the book does not exist", async () => {
            const book = await bookService.insert(
                structuredClone(default_book),
            );

            await new Promise((resolve) => setTimeout(resolve, 700));
            const updatedBook = {
                ...book,
                title: "test4.book",
                comment: "This is an updated test book",
                id: -1,
            };

            await expect(
                bookService.update(structuredClone(updatedBook)),
            ).rejects.toThrow();
        });
    });

    describe("delete", () => {
        it("should delete the book from the database", async () => {
            const book = await bookService.insert(
                structuredClone(default_book),
            );

            await bookService.delete(book.id);

            await expect(bookService.getById(book.id)).rejects.toThrow();
        });

        it("should throw an error if the book does not exist", async () => {
            await expect(bookService.delete(-1)).rejects.toThrow();
        });
    });

    describe("setOwner|setOwnerByName", () => {
        it("should set the owner of the book", async () => {
            const book = await bookService.insert(
                structuredClone(default_book),
            );

            const users = ["user", ENTITY_USER];

            for (let i = 0; i < 2; i++) {
                const result = await bookService.setOwner(
                    user_ids[0]!,
                    users[i] as Entity,
                    book.id,
                );
                expect(result.entity_name).toBe("test@book.de");
                expect(result.entity_type).toBe("user");
                expect(result.id).toBe(user_ids[0]);
            }

            const groups = ["group", ENTITY_GROUP];
            for (let i = 0; i < 2; i++) {
                const result = await bookService.setOwner(
                    group_ids[0]!,
                    groups[i] as Entity,
                    book.id,
                );
                expect(result.entity_name).toBe("book");
                expect(result.entity_type).toBe("group");
                expect(result.id).toBe(group_ids[0]);
            }
        });

        it("should throw an error if the book does not exist", async () => {
            await expect(
                bookService.setOwner(user_ids[1]!, ENTITY_USER, -1),
            ).rejects.toThrow();
        });

        it("should throw an error if the user does not exist", async () => {
            const book = await bookService.insert(
                structuredClone(default_book),
            );

            await expect(
                bookService.setOwner(-1, ENTITY_USER, book.id),
            ).rejects.toThrow();

            await expect(
                bookService.setOwnerByUser(-1, book.id),
            ).rejects.toThrow();
        });
    });

    describe("search", () => {
        test.todo("add test");
    });
});

function expect_same(
    book1: PartialBy<BookDataRet, "id">,
    book2: PartialBy<BookDataRet, "id">,
    time_diff: number = 4,
) {
    expect(book1.title).toBe(book2.title);
    expect(book1.comment).toBe(book2.comment);
    expect(book1.finished).toBe(book2.finished);
    expect(book1.owner_id).toBe(book2.owner_id);
    expect(book1.owner_name).toBe(book2.owner_name);
    expect(book1.owner_type).toBe(book2.owner_type);
    expect(book1.cover_page_id).toBe(book2.cover_page_id);
    expect(book1.created!.getTime()).toBeCloseTo(
        book2.created!.getTime(),
        -time_diff,
    );
    expect(book1.last_updated!.getTime()).toBeCloseTo(
        book2.last_updated!.getTime(),
        -time_diff,
    );
}

function compare_insert(
    book1: BookDataInsert,
    book2: BookDataRet,
    time_diff = 4,
) {
    expect(book1.title).toBe(book2.title);
    expect(book1.comment).toBe(book2.comment);
    expect(book1.finished).toBe(book2.finished);
    if (book2.owner_type === "user") {
        expect(book1.owner_user_id).toBe(book2.owner_id);
    } else {
        expect(book1.owner_group_id).toBe(book2.owner_id);
    }
    expect(book1.cover_page_id).toBe(book2.cover_page_id);
    expect(book1.created!.getTime()).toBeCloseTo(
        book2.created!.getTime(),
        -time_diff,
    );
    expect(book1.last_updated!.getTime()).toBeCloseTo(
        book2.last_updated!.getTime(),
        -time_diff,
    );
}
