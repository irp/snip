import { NotFoundError } from "src/errors";
import { pool_data, pool_permissions } from "../../sql.connection";
import {
    ENTITY_GROUP,
    ENTITY_USER,
    ID,
    PartialBy,
    PermissionACLData,
    RESOURCE_BOOK,
} from "../../types";
import { PermissionService } from "../permission";

describe("PermissionService", () => {
    let service: PermissionService;
    let consoleSpy;

    let defaultPermissionData: PartialBy<PermissionACLData, "id">;
    let defaultPermissionData2: PartialBy<PermissionACLData, "id">;
    let user_ids: number[] = [];
    let book_ids: number[] = [];

    beforeAll(async () => {
        service = new PermissionService();

        // Add two books and a user
        const user_id = await pool_permissions
            .query("INSERT INTO users (`id`) VALUES (NULL)")
            .then((res) => {
                return res.insertId;
            });

        const book_id1 = await pool_data
            .query(
                "INSERT INTO books (title, comment, owner_user_id) VALUES (?, ?, ?)",
                ["test.perms", "Test description", user_id],
            )
            .then((res) => {
                return res.insertId;
            });

        book_ids.push(book_id1);
        user_ids.push(user_id);

        defaultPermissionData = {
            entity_id: 8888,
            resource_id: 9999,
            entity_type: ENTITY_USER,
            resource_type: RESOURCE_BOOK,
            pACL: true,
            pRead: true,
            pWrite: true,
            pDelete: true,
        };

        defaultPermissionData2 = {
            ...defaultPermissionData,
            entity_id: 1234,
        };
    });

    afterAll(async () => {
        //Cleanup ids {"table": [ids]}
        await pool_data.query(
            "DELETE FROM pages where book_id in (select book_id from books where title like '%.perms')",
        );
        await pool_data.query("DELETE FROM books where title like '%.perms'");
        for (const id of user_ids) {
            await pool_permissions.query("DELETE FROM users where id = ?", [
                id,
            ]);
        }
        await pool_permissions.query("Delete from acl");
    });

    beforeEach(async () => {
        await pool_permissions.query("Delete from acl where entity_id = 8888");
        await pool_permissions.query("Delete from acl where entity_id = 1234");
    });

    describe("insert", () => {
        it("should insert a new permission", async () => {
            const permission = await service.insert(
                structuredClone(defaultPermissionData),
            );
            expect_same(permission, defaultPermissionData);
        });
    });

    describe("getAll", () => {
        it("should retrieve all permissions", async () => {
            const permission = await service.insert(
                structuredClone(defaultPermissionData),
            );
            const permission2 = await service.insert(
                structuredClone(defaultPermissionData2),
            );
            const retrievedPermission = await service.getAll();
            expect(retrievedPermission.length).toBeGreaterThanOrEqual(2);
        });
    });

    describe("getById|getByIds", () => {
        it("should retrieve a permission by its ID(s)", async () => {
            const permission = await service.insert(
                structuredClone(defaultPermissionData),
            );

            const permission2 = await service.insert(
                structuredClone(defaultPermissionData2),
            );

            const retrievedPermission = await service.getById(permission.id);
            expect_same(retrievedPermission, permission, true);

            const retrievedPermission2 = await service.getByIds([
                permission.id,
                permission2.id,
            ]);
            expect(retrievedPermission2.length).toBe(2);
            expect_same(retrievedPermission2[0]!, permission, true);
            expect_same(retrievedPermission2[1]!, permission2, true);
        });

        it("should throw an error if the permission(s) not found", async () => {
            await expect(service.getById(-1)).rejects.toThrow();

            await expect(service.getByIds([-1])).rejects.toThrow();
        });
    });

    describe("update", () => {
        it("should update a permission", async () => {
            const permission = await service.insert(
                structuredClone(defaultPermissionData),
            );
            const updatedPermission = await service.update({
                ...permission,
                pACL: false,
                pRead: false,
                pWrite: false,
                pDelete: false,
            });
            expect_same(updatedPermission, {
                ...permission,
                pACL: false,
                pRead: false,
                pWrite: false,
                pDelete: false,
            });
        });

        it("should throw an error if the permission not found", async () => {
            await expect(
                service.update({
                    ...defaultPermissionData,
                    id: -1,
                }),
            ).rejects.toThrow();
        });
    });

    describe("delete", () => {
        it("should delete a permission", async () => {
            const permission = await service.insert(
                structuredClone(defaultPermissionData),
            );
            await service.delete(permission.id);
            await expect(service.getById(permission.id)).rejects.toThrow();
        });

        it("should throw an error if the permission not found", async () => {
            await expect(service.delete(-1)).rejects.toThrow();
        });
    });

    describe("getByEntityAndResource | getByEntityAndResourceReduced", () => {
        it("should retrieve a permission by its entity and resource", async () => {
            const permission = await service.insert(
                structuredClone(defaultPermissionData),
            );

            const retrievedPermission = await service.getByEntityAndResource(
                defaultPermissionData.entity_id,
                defaultPermissionData.resource_id,
                defaultPermissionData.entity_type,
                defaultPermissionData.resource_type,
            );
            expect_same(retrievedPermission, permission, true);
        });

        it("should retrieve a reduced permission by its entity and resource", async () => {
            const retrievedPermission = await service.getByEntityAndResource(
                user_ids[0]!,
                book_ids[0]!,
                ENTITY_USER,
                RESOURCE_BOOK,
            );
            expect(retrievedPermission.pACL).toBe(true);
            expect(retrievedPermission.pRead).toBe(true);
            expect(retrievedPermission.pWrite).toBe(true);
            expect(retrievedPermission.pDelete).toBe(true);
        });

        it("should return permission denied if the permission not found", async () => {
            expect(
                async () =>
                    await service.getByEntityAndResource(
                        -1,
                        -1,
                        ENTITY_USER,
                        RESOURCE_BOOK,
                    ),
            ).rejects.toThrowError();
        });
    });

    describe("getByUserAndBook | getByUserAndBooks | getByUserAndBookReduced", () => {
        it("should retrieve a permission by its user and book", async () => {
            // Insert user

            const retrievedPermission = await service.getByUserAndBook(
                user_ids[0]!,
                book_ids[0]!,
            );

            // book owner set automatically
            expect(retrievedPermission[0]!.resource_id).toBe(book_ids[0]);
            expect(retrievedPermission[0]!.entity_type).toBe(ENTITY_USER);
            expect(retrievedPermission[0]!.resource_type).toBe(RESOURCE_BOOK);
            expect(retrievedPermission[0]!.pACL).toBe(true);
            expect(retrievedPermission[0]!.pRead).toBe(true);
            expect(retrievedPermission[0]!.pWrite).toBe(true);
            expect(retrievedPermission[0]!.pDelete).toBe(true);

            // Insert book
            const book_id2 = await pool_data
                .query(
                    "INSERT INTO books (title, comment, owner_user_id) VALUES (?, ?, ?)",
                    ["test2.perms", "Test description", user_ids[0]],
                )
                .then((res) => {
                    return res.insertId;
                });

            const retrievedPermission3 = await service.getByUserAndBookReduced(
                user_ids[0]!,
                book_ids[0]!,
            );
            expect_same(retrievedPermission3, retrievedPermission[0]!);
        });

        it("should return permission denied if the permission not found", async () => {
            expect(
                async () => await service.getByUserAndBook(-1, -1),
            ).rejects.toThrowError();

            const r = await service.getByUserAndBookReduced(-1, -1);
            expect_denied(r);
        });
    });

    describe("getByEntity", () => {
        it("should retrieve all permissions by its entity", async () => {
            const permission = await service.insert(
                structuredClone(defaultPermissionData),
            );
            const permission2 = await service.insert(
                structuredClone({
                    ...defaultPermissionData,
                    resource_id: 8812,
                }),
            );
            ["user", ENTITY_USER].forEach(async (entity) => {
                const retrievedPermission = await service.getByEntity(
                    defaultPermissionData.entity_id,
                    entity as "user",
                );
                expect(retrievedPermission.length).toBeGreaterThanOrEqual(2);
            });
            //Same for groups
            const groupData = await service.insert(
                structuredClone({
                    ...defaultPermissionData,
                    entity_type: ENTITY_GROUP,
                }),
            );
            const groupData2 = await service.insert(
                structuredClone({
                    ...defaultPermissionData,
                    entity_type: ENTITY_GROUP,
                    resource_id: 8812,
                }),
            );
            [ENTITY_GROUP, "group"].forEach(async (entity) => {
                const retrievedGroupData = await service.getByEntity(
                    defaultPermissionData.entity_id,
                    entity as "group",
                );
                expect(retrievedGroupData.length).toBeGreaterThanOrEqual(2);
            });
        });

        it("should throw an error if invalid entity", async () => {
            await expect(
                service.getByEntity(-1, "invalid" as "user"),
            ).rejects.toThrow();
        });
    });

    describe("updateByEntityAndResource", () => {
        it("should update a permission by its entity and resource", async () => {
            const permission = await service.insert(
                structuredClone(defaultPermissionData),
            );
            const updatedPermission = await service.updateByEntityAndResource(
                defaultPermissionData.entity_id,
                defaultPermissionData.resource_id,
                defaultPermissionData.entity_type,
                defaultPermissionData.resource_type,
                false,
                false,
                false,
                false,
            );
            expect(updatedPermission).toBe(true);
        });

        it("should insert if it does not exist", async () => {
            const updatedPermission = await service.updateByEntityAndResource(
                111,
                22321,
                defaultPermissionData.entity_type,
                defaultPermissionData.resource_type,
                false,
                false,
                false,
                false,
            );
            expect(updatedPermission).toBe(true);
        });
    });

    describe("getResolvedForBook", () => {
        it("should retrieve all permissions for a book", async () => {
            const permission = await service.insert(
                structuredClone(defaultPermissionData),
            );
            const permission2 = await service.insert(
                structuredClone({
                    ...defaultPermissionData,
                    entity_id: 1234,
                }),
            );

            const retrievedPermission = await service.getResolvedForBook(
                defaultPermissionData.resource_id,
            );
            expect(retrievedPermission.length).toBeGreaterThanOrEqual(2);

            expect(retrievedPermission[0]!.entity_name).toBeDefined();
            expect(retrievedPermission[0]!.entity_type).toBeDefined();
        });

        it("should return empty array if none found", async () => {
            const retrievedPermission = await service.getResolvedForBook(-1);
            expect(retrievedPermission.length).toBe(0);
        });
    });

    describe("search", () => {
        it("should return an array of blobs", async () => {
            await expect(service.search("asd")).rejects.toThrow();
        });
    });
});

function expect_same(
    a: PartialBy<PermissionACLData, "id">,
    b: PartialBy<PermissionACLData, "id">,
    ignoreId = true,
) {
    if (!ignoreId) {
        expect(a.id).toBe(b.id);
    }
    expect(a.entity_id).toBe(b.entity_id);
    expect(b.resource_id).toBe(b.resource_id);
    expect(a.entity_type).toBe(b.entity_type);
    expect(b.resource_type).toBe(b.resource_type);
    expect(a.pACL).toBe(b.pACL);
    expect(a.pRead).toBe(b.pRead);
    expect(a.pWrite).toBe(b.pWrite);
    expect(a.pDelete).toBe(b.pDelete);
}

function expect_denied(permission: Partial<PermissionACLData>) {
    expect(permission.pACL).toBe(false);
    expect(permission.pRead).toBe(false);
    expect(permission.pWrite).toBe(false);
    expect(permission.pDelete).toBe(false);
}
