/* eslint-disable @typescript-eslint/no-explicit-any */
import { hasOwnProperty, registerService } from "@snip/common";

import { check_mariadb_warning } from "../errors";
import { pool_data as pool } from "../sql.connection";
import { snipsData } from "../sql.typings";
import { SnipStrategy } from "../strategies";
import {
    BaseDataIn,
    BaseDataRet,
    BaseView,
    ID,
    SnipDataInsert,
    SnipDataRet,
} from "../types";
import { blobService } from "./blob";

export class SnipService implements SnipStrategy {
    /**
     * Retrieves all snips from the database.
     * @returns A promise that resolves to an array of SnipData objects.
     */
    async getAll(): Promise<SnipDataRet[]> {
        throw new Error("Method not implemented.");
    }

    /**
     * Retrieves a snip by its ID.
     *
     * @param snip_id - The ID of the snip to retrieve.
     * @returns A Promise that resolves to the snip data.
     */
    public async getById(snip_id: ID): Promise<SnipDataRet> {
        const sql = "SELECT * FROM snips WHERE id = ?";

        let snipData = await pool
            .query(sql, [snip_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new Error("Snip not found");
                }
                return res[0];
            });

        snipData = await this.parseSnips([snipData]);
        return snipData[0];
    }

    /** Get multiple snips given their MARIADB id
     *
     * @param snip_ids
     */
    async getByIds(snip_ids: ID[]): Promise<SnipDataRet[]> {
        const sql = "SELECT * FROM snips WHERE id IN (?)";

        let snips = await pool
            .query(sql, [snip_ids])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != snip_ids.length) {
                    throw new Error(
                        "Number of snips found does not match number of snips requested! " +
                            res.length +
                            " != " +
                            snip_ids.length,
                    );
                }
                return res;
            });

        // We still need to parse the json from the database
        // This should run in parallel
        snips = this.parseSnips(snips);
        return snips;
    }

    async upsert(snipData: SnipDataInsert): Promise<SnipDataRet> {
        /** Handle nested snips first
         */
        let blob_id: number | null = null;
        if (hasOwnProperty(snipData.data, "blob")) {
            snipData.data.blob = await blobService.upsert(snipData.data.blob!);
            // Set blob id for sql insert
            blob_id = snipData.data.blob.id;
        }

        // Check if snip has nested snips
        if (hasOwnProperty(snipData.data, "snips")) {
            // Upsert nested snips
            snipData.data.snips = await Promise.all(
                snipData.data.snips!.map(async (snipd) => {
                    // Sanity overwrites
                    snipd.created_by = snipData.created_by;
                    snipd.book_id = snipData.book_id;
                    snipd.page_id = snipData.page_id;

                    return await this.upsert(snipd);
                }),
            );
        }

        //Check if snip has reference to other snips
        if (hasOwnProperty(snipData.data, "snip")) {
            // Sanity overwrites
            snipData.data.snip!.created_by = snipData.created_by;
            snipData.data.snip!.book_id = snipData.book_id;
            snipData.data.snip!.page_id = snipData.page_id;

            snipData.data.snip = await this.upsert(snipData.data.snip!);
        }

        // Check if exists in db
        const upsert_sql =
            "INSERT INTO `snips` (`id`, `page_id`, `book_id`, `type`, `data_json`, `view_json`, `blob_id`, `created_by`) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `page_id` = VALUES(`page_id`), `book_id` = VALUES(`book_id`), `type` = VALUES(`type`), `data_json` = VALUES(`data_json`), `view_json` = VALUES(`view_json`), `blob_id` = VALUES(`blob_id`), `created_by` = VALUES(`created_by`) RETURNING `id`";

        //Stringify json data adn view
        const [data_json, removed] = encodeData(snipData.data);
        const view_json = JSON.stringify(snipData.view);

        // Negative id not allowed
        let id: number | null = null;
        if (snipData.id && snipData.id > 0) {
            id = snipData.id;
        }

        const snip_id: ID = await pool
            .query(upsert_sql, [
                id,
                snipData.page_id,
                snipData.book_id,
                snipData.type,
                data_json,
                view_json,
                blob_id,
                snipData.created_by,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                return res[0].id;
            });

        // Update values
        snipData.id = snip_id;
        snipData.last_updated = new Date();

        // restore removed data
        snipData.data = restoreOptionalData(snipData.data, removed);
        return snipData as SnipDataRet;
    }

    /**
     * Inserts a snip into the database. If the snip data contains a blob or any nested snips,
     * they will be inserted as well. Even if they already exist in the database. This will
     * create duplicates if the snip data is not unique.
     *
     * If you want to insert a snip with dependencies, use the snip_ids, snip_id and blob_id
     * properties. i.e. insert nested snips and blobs first and then insert the snip with the
     * corresponding IDs.
     *
     *
     * @param snipData - The snip data to be inserted.
     * @param upsert_nested - Optional parameter to upsert nested snips. I.e. if a nested snip already exists, update it and do not reinsert it.
     * @returns A promise that resolves to the inserted snip data.
     */
    async insert(snipData: SnipDataInsert): Promise<SnipDataRet> {
        if (snipData.id && snipData.id > 0) {
            console.warn("SnipService.insert: id is set, inserting new snip");
        }

        if (!snipData.created_by) {
            console.warn(`Inserting without owner (created_by)!`);
        }

        // Check if snip has blob and insert
        let blob_id: number | null = null;

        if (hasOwnProperty(snipData.data, "blob")) {
            snipData.data.blob = await blobService.insert(snipData.data.blob!);
            // Set blob id for sql insert
            blob_id = snipData.data.blob.id;
        }

        // Check if snip has nested snips
        if (hasOwnProperty(snipData.data, "snips")) {
            // Upsert nested snips
            snipData.data.snips = await Promise.all(
                snipData.data.snips!.map(async (snipd) => {
                    // Sanity overwrites
                    snipd.created_by = snipData.created_by;
                    snipd.book_id = snipData.book_id;
                    snipd.page_id = snipData.page_id;

                    return await this.insert(snipd);
                }),
            );
        }

        //Check if snip has reference to other snips
        if (hasOwnProperty(snipData.data, "snip")) {
            // Sanity overwrite for book id and created by
            snipData.data.snip!.created_by = snipData.created_by;
            snipData.data.snip!.book_id = snipData.book_id;
            snipData.data.snip!.page_id = snipData.page_id;

            snipData.data.snip = await this.insert(snipData.data.snip!);
        }

        const snip_sql =
            "INSERT INTO `snips` (`page_id`, `book_id`, `type`, `data_json`, `view_json`, `blob_id`, `created_by`) VALUES (?, ?, ?, ?, ?, ?, ?)";

        //Stringify json data adn view
        const [data_json, removed] = encodeData(snipData.data);
        const view_json = JSON.stringify(snipData.view);

        // Insert data
        const snip_id: ID = await pool
            .query(snip_sql, [
                snipData.page_id,
                snipData.book_id,
                snipData.type,
                data_json,
                view_json,
                blob_id,
                snipData.created_by,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.insertId;
            });

        // Update values
        snipData.id = snip_id;
        snipData.created = new Date();
        snipData.last_updated = new Date();

        // restore removed data
        snipData.data = restoreOptionalData(snipData.data, removed);

        return snipData as SnipDataRet;
    }

    /**
     * Retrieves snips by page ID.
     * @param page_id The ID of the page.
     * @returns A promise that resolves to an array of SnipData objects.
     */
    async getByPageId(page_id: ID): Promise<SnipDataRet[]> {
        // Select snips also from referenced page, in theory
        // that kinda merges the pages
        const sql = `
        SELECT snips.*
        FROM snips
        WHERE snips.page_id = ?
        OR snips.page_id = (
            SELECT COALESCE(referenced_page_id, id)
            FROM pages
            WHERE id = ?
        );`;

        let snips = await pool
            .query(sql, [page_id, page_id, page_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        // We still need to parse the json from the database
        // This should run in parallel
        snips = await this.parseSnips(snips);

        return snips as SnipDataRet[];
    }

    /**
     * Retrieves queued snips by book ID. I.e. in not placed on a page yet
     *
     * @param book_id - The ID of the book.
     * @param show_hidden - Optional parameter to include hidden snips. Default is false.
     * @returns A promise that resolves to an array of SnipData objects.
     */
    async getQueuedByBookId(
        book_id: ID,
        show_hidden = false,
    ): Promise<SnipDataRet[]> {
        let sql =
            "SELECT * FROM `snips` WHERE `book_id` = ? AND `page_id` IS NULL";
        if (!show_hidden) {
            sql += " AND `hide` = 0";
        }

        let snips = await pool
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                // should only be the same format as SnipData
                return res;
            });

        // We still need to parse the json from the database
        snips = await this.parseSnips(snips);
        return snips;
    }

    /**
     * Retrieves queued snip ids by book ID. I.e. in not placed on a page yet
     *
     * @param book_id - The ID of the book.
     * @param show_hidden - Optional parameter to include hidden snips. Default is false.
     * @returns A promise that resolves to an array of snip ids.
     */
    async getQueuedIdsByBookId(
        book_id: ID,
        show_hidden = false,
    ): Promise<ID[]> {
        let sql =
            "SELECT id FROM `snips` WHERE `book_id` = ? AND `page_id` IS NULL";
        if (!show_hidden) {
            sql += " AND `hide` = 0";
        }

        const snipids = await pool
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.id;
            });

        return snipids;
    }

    /**
     * Updates a snip with the provided data.
     *
     * @param snipData - The data to update the snip with.
     * @returns A Promise that resolves to the updated snip data.
     */
    async update(
        snipData: Partial<SnipDataInsert> & { id: ID },
    ): Promise<SnipDataRet> {
        if (!snipData.id || !snipData.data || !snipData.view) {
            throw new Error("Missing required data");
        }

        // Update nested types
        if (hasOwnProperty(snipData.data, "snips")) {
            snipData.data.snips = await Promise.all(
                snipData.data.snips!.map(async (s: Partial<SnipDataInsert>) => {
                    // Sanity overwrite
                    s.created_by = snipData.created_by;
                    s.book_id = snipData.book_id;
                    s.page_id = snipData.page_id;
                    if (!s.id) {
                        throw new Error("Missing id for nested snip");
                    }
                    return await this.update(s as SnipDataInsert & { id: ID });
                }),
            );
        }

        if (hasOwnProperty(snipData.data, "snip")) {
            // Sanity overwrite
            snipData.data.snip!.created_by = snipData.created_by;
            snipData.data.snip!.book_id = snipData.book_id!;
            snipData.data.snip!.page_id = snipData.page_id;

            if (!snipData.data.snip!.id) {
                throw new Error("Missing id for nested snip");
            }

            snipData.data.snip = await this.update(
                snipData.data.snip! as SnipDataInsert & { id: ID },
            );
        }
        if (hasOwnProperty(snipData.data, "blob")) {
            snipData.data.blob = await blobService.update(snipData.data.blob!);
        }

        // Encode data
        const [data_json, removed] = encodeData(snipData.data);
        const view_json = JSON.stringify(snipData.view);

        // Upload
        const sql_data =
            "UPDATE `snips` SET `page_id`=?,`book_id`=?,`type`=?,`data_json`=?,`view_json`=?,`created_by`=? WHERE `id`=?";

        await pool
            .query(sql_data, [
                snipData.page_id,
                snipData.book_id,
                snipData.type,
                data_json,
                view_json,
                snipData.created_by,
                snipData.id,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows != 1) {
                    throw new Error("Update failed, no rows affected");
                }
            });

        // Update values
        snipData.last_updated = new Date();

        // restore removed data
        snipData.data = restoreOptionalData(snipData.data, removed);

        return snipData as SnipDataRet;
    }

    /**
     * Deletes a snip from the database.
     * If the snip has an associated blob or other snips, they will not
     * be deleted! Use deleteNested to delete all nested snips and blobs.
     *
     * @param snip_id The ID of the snip to delete.
     * @returns A promise that resolves to a boolean indicating whether the deletion was successful.
     */
    async delete(snip_id: number): Promise<boolean> {
        //Delete snip
        const sql = "DELETE FROM `snips` WHERE `id`=?";

        const success = await pool
            .query(sql, [snip_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows != 1) {
                    throw new Error(
                        "Delete failed, no rows affected, snip_id: " + snip_id,
                    );
                }
                return true;
            });

        return success;
    }

    /**
     * Deletes a snip and all nested snips and blobs from the database.
     * @param snip_id The ID of the snip to delete.
     * @returns A promise that resolves to a boolean indicating whether the deletion was successful.
     * @throws An error if the deletion was not successful, for instance if
     * the blob is still used by another snip.
     */
    async deleteNested(snip_id: number): Promise<boolean> {
        // Get nested snips
        const snips = await this.getById(snip_id);
        const snipData = snips.data;

        // Delete nested snips
        if (hasOwnProperty(snipData, "snips")) {
            await Promise.all(
                snipData.snips!.map(async (s) => {
                    return await this.deleteNested(s.id);
                }),
            );
        }
        if (hasOwnProperty(snipData, "snip")) {
            await this.deleteNested(snipData.snip!.id);
        }

        // Delete snip
        await this.delete(snip_id);

        // Delete blob last for foreign key constraints
        if (hasOwnProperty(snipData, "blob")) {
            await blobService.delete(snipData.blob!.id);
        }

        return true;
    }

    async placeOnPage(snip_id: number, page_id: number): Promise<SnipDataRet> {
        const snip = await this.getById(snip_id);
        snip.page_id = page_id;
        return await this.update(snip);
    }

    async search(query: string): Promise<SnipDataRet[]> {
        throw new Error("Method not implemented.");
    }

    /**
     * Parses an array of snips, transforming the data and view properties and removing nested array snips.
     * If an error occurs during parsing, a default value is assigned to the data and view properties.
     * @param snips - The array of snips to parse.
     * @returns A promise that resolves to an array of parsed snips.
     */
    private async parseSnips(snips: snipsData[]): Promise<SnipDataRet[]> {
        let snips_parsed = await Promise.all(
            snips.map(async (item) => {
                try {
                    const data = await this.parseSnipData(
                        item.data_json,
                        snips,
                    );
                    const view = await this.parseSnipView(item.view_json);
                    delete item.data_json;
                    delete item.view_json;

                    // Check for blob and get it
                    if (item.blob_id != null) {
                        data.blob = await blobService.getById(item.blob_id);
                    }
                    (item as SnipDataRet).data = data;
                    (item as SnipDataRet).view = view;
                } catch (err) {
                    // Just in case
                    console.log(
                        "[snip.service] Could not parse snip data, may need manual fix",
                    );
                    console.error(item);
                    console.error(err);
                    (item as SnipDataRet).data = {};
                    (item as SnipDataRet).view = {
                        x: 0,
                        y: 0,
                    };
                }
                return item as SnipDataRet;
            }),
        );
        // Remove nested array snips, is way faster than doing it using sql
        let to_remove: number[] = [];
        for (const snip of snips_parsed) {
            if (hasOwnProperty(snip.data, "snip_ids")) {
                to_remove = to_remove.concat(
                    (snip.data as BaseDataIn & { snip_ids: ID[] }).snip_ids,
                );
            }

            if (hasOwnProperty(snip.data, "snip_id")) {
                to_remove.push(
                    (snip.data as BaseDataIn & { snip_id: ID }).snip_id,
                );
            }
        }
        snips_parsed = snips_parsed.filter((snip) => {
            return !to_remove.includes(snip.id);
        });
        return snips_parsed;
    }

    /** Parses snip data if an snips array is given the array snips
     * are not retrieved from the database again but filtered from the snips
     *
     * @param data_string
     * @param snips
     * @returns
     */
    private async parseSnipData(
        data_string: string | null | undefined,
        snips?: snipsData[],
    ): Promise<BaseDataRet> {
        if (!data_string) {
            return {};
        }
        const data = JSON.parse(data_string);

        // Check for snip_ids
        // Check for snip_id
        if (hasOwnProperty(data, "snip_ids")) {
            data.snips = [];
            // Check if we have the snips already
            for (const id of data.snip_ids) {
                const found = snips?.find((snip) => snip.id == id);
                if (found) {
                    data.snips.push(found);
                } else {
                    data.snips.push(await this.getById(id));
                }
            }
        }
        if (hasOwnProperty(data, "snip_id")) {
            const id = data.snip_id;
            const found = snips?.find((snip) => snip.id == id);
            if (found) {
                data.snip = found;
            } else {
                data.snip = await this.getById(id);
            }
        }

        //Parse to int
        if (hasOwnProperty(data, "width")) {
            data.width = parseInt(data.width);
        }
        if (hasOwnProperty(data, "height")) {
            data.height = parseInt(data.height);
        }
        return data;
    }

    /**
     * Parses the snip view string and returns the parsed view object.
     *
     * @param view_string - The snip view string to parse.
     * @returns A promise that resolves to the parsed view object.
     */
    private async parseSnipView(
        view_string: string | null | undefined,
    ): Promise<BaseView> {
        // Parse to int
        if (!view_string) {
            return { x: 0, y: 0 };
        }
        const view = JSON.parse(view_string);
        if (hasOwnProperty(view, "rx")) {
            view.rx = parseInt(view.rx);
        }
        if (hasOwnProperty(view, "ry")) {
            view.ry = parseInt(view.ry);
        }
        if (hasOwnProperty(view, "rw")) {
            view.rw = parseInt(view.rw);
        }
        if (hasOwnProperty(view, "rh")) {
            view.rh = parseInt(view.rh);
        }
        if (hasOwnProperty(view, "width")) {
            view.width = parseInt(view.width);
        }
        return view as BaseView;
    }
}

function encodeData(data: BaseDataIn): [string, BaseDataIn] {
    // Check if blob
    const removed: BaseDataIn = {};
    if (hasOwnProperty(data, "blob")) {
        (data as unknown as any)["blob_id"] = data.blob!.id;
        removed["blob"] = data.blob;
        delete data.blob;
    }
    if (hasOwnProperty(data, "snips")) {
        (data as unknown as any)["snip_ids"] = data.snips!.map((s) => s.id);
        removed["snips"] = data.snips;
        delete data.snips;
    }
    if (hasOwnProperty(data, "snip")) {
        (data as unknown as any)["snip_id"] = data.snip!.id;
        removed["snip"] = structuredClone(data.snip);
        delete data.snip;
    }

    const encodedData = JSON.stringify(data);

    return [encodedData, removed];
}

function restoreOptionalData(
    data: BaseDataIn,
    removed: BaseDataIn,
): BaseDataIn {
    if (hasOwnProperty(removed, "blob")) {
        data.blob = removed.blob;
        //remove blob_id if present
        delete (data as unknown as any)["blob_id"];
    }

    if (hasOwnProperty(removed, "snips")) {
        data.snips = removed.snips;
        //remove snip_ids if present
        delete (data as unknown as any)["snip_ids"];
    }
    if (hasOwnProperty(removed, "snip")) {
        data.snip = removed.snip;
        //remove snip_id if present
        delete (data as unknown as any)["snip_id"];
    }
    return data;
}

/** We need to globally register each service here
 * to prevent memory leaks in development.
 */
export const snipService = registerService("db_snip", () => new SnipService());
