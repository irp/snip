import { registerService } from "@snip/common";

import { check_mariadb_warning, NotFoundError } from "../errors";
import { pool_permissions } from "../sql.connection";
import { APITokenData, ID, TokenData, UITokenData } from "../types";

export class TokenService {
    /**
     * Retrieves a token by its ID.
     * @param id - The ID of the token.
     * @returns A promise that resolves to the token data.
     * @throws {NotFoundError} If the token with the specified ID is not found.
     */
    async getById(id: ID): Promise<Required<UITokenData | APITokenData>> {
        const sql = `SELECT * FROM tokens WHERE id = ?`;

        return await pool_permissions
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length === 0) {
                    throw new NotFoundError("Token", id);
                }
                return res[0];
            });
    }

    /**
     * Retrieves tokens by book ID.
     * @param book_id - The ID of the book.
     * @returns A promise that resolves to an array of token data.
     */
    async getByBookId(
        book_id: ID,
    ): Promise<Required<UITokenData | APITokenData>[]> {
        const sql = `SELECT * FROM tokens WHERE book_id = ?`;

        return await pool_permissions
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });
    }

    async getUITokenByBookIdAndToken(
        book_id: ID,
        token: string,
    ): Promise<Required<UITokenData>> {
        const sql = `SELECT * FROM tokens WHERE book_id = ? AND hashed_token = ? AND type = "ui"`;

        return await pool_permissions
            .query(sql, [book_id, token])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length !== 1) {
                    throw new NotFoundError("Token", token);
                }
                return res[0];
            });
    }

    async getAPITokenByBookIdAndToken(
        book_id: ID,
        token: string,
    ): Promise<Required<APITokenData>> {
        const sql = `SELECT * FROM tokens WHERE book_id = ? AND hashed_token = ? AND type = "api"`;

        return await pool_permissions
            .query(sql, [book_id, token])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length !== 1) {
                    throw new NotFoundError("Token", token);
                }
                return res[0];
            });
    }

    async insert(data: TokenData): Promise<Required<TokenData>> {
        const sql = `
        INSERT INTO tokens 
            (hashed_token, book_id, created_by, description, type, expires_at)
        VALUES (?, ?, ?, ?, ?, ?);
        `;
        const result = await pool_permissions
            .query(sql, [
                data.hashed_token,
                data.book_id,
                data.created_by,
                data.description,
                data.type,
                data.expires_at,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                return this.getById(res.insertId) as Promise<
                    Required<UITokenData>
                >;
            });

        return result;
    }

    async delete(id: ID): Promise<boolean> {
        const sql = `DELETE FROM tokens WHERE id = ?`;

        return await pool_permissions
            .query(sql, [id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Delete failed, no rows affected");
                }
                return true;
            });
    }
}

/** We need to globally register each service here
 * to prevent memory leaks in development.
 */
export const tokensService = registerService(
    "db_tokens",
    () => new TokenService(),
);
