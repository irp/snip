import { registerService } from "@snip/common";

import { check_mariadb_warning, NotFoundError } from "../errors";
import { pool_data as pool } from "../sql.connection";
import { PageStrategy } from "../strategies";
import type { ID, PageDataInsert, PageDataRet } from "../types";

export class PageService implements PageStrategy {
    /**
     * Inserts a new page into the database.
     *
     * @param pageData - The data of the page to be inserted.
     * @returns A Promise that resolves to the inserted PageData.
     */
    async insert(pageData: PageDataInsert): Promise<PageDataRet> {
        // Count number of pages in book
        const sql2 =
            "SELECT COUNT(*) AS count FROM pagesView WHERE book_id = ?";

        const num_pages = await pool
            .query(sql2, [pageData.book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length == 0) {
                    return 0;
                }
                return res[0].count;
            });

        // Insert page
        const sql = `
            INSERT INTO pages
                (book_id, page_number, background_type_id, referenced_page_id)
            VALUES
                (?, ?, ?,?);
        `;

        const insert_id: ID = await pool
            .query(sql, [
                pageData.book_id,
                num_pages,
                pageData.background_type_id ?? null,
                pageData.referenced_page_id ?? null,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                return res.insertId;
            });

        return await this.getById(insert_id);
    }

    /**
     * Retrieves all pages from the database.
     * @returns A promise that resolves to an array of PageData objects.
     */
    async getAll(): Promise<PageDataRet[]> {
        const sql = "SELECT * FROM pagesView";
        const pages: PageDataRet[] = await pool
            .query(sql)
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        return pages;
    }

    /**
     * Retrieves a page by its ID.
     * @param page_id The ID of the page to retrieve.
     * @returns A Promise that resolves to the PageData object representing the page. Returns the default page data if the page is not found in the database.
     */
    async getById(page_id: ID): Promise<PageDataRet> {
        const sql = "SELECT * FROM pagesView WHERE id = ?";

        const pageData: PageDataRet = await pool
            .query(sql, [page_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("Page", page_id);
                }

                return res[0];
            });

        return pageData;
    }

    /**
     * Retrieves page data for the specified page IDs.
     * @param page_ids - An array of page IDs.
     * @returns A promise that resolves to an array of PageData objects.
     */
    async getByIds(page_ids: ID[]): Promise<PageDataRet[]> {
        const sql = "SELECT * FROM pagesView WHERE id IN (?)";

        const pages: PageDataRet[] = await pool
            .query(sql, [page_ids])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != page_ids.length) {
                    throw new Error(
                        "Number of pages does not match! length:" +
                            res.length +
                            " for page_ids: " +
                            page_ids,
                    );
                }
                return res;
            });

        return pages;
    }

    /**
     * Retrieves pages by book ID.
     * @param book_id - The ID of the book.
     * @returns A promise that resolves to an array of PageData objects.
     */
    async getByBookId(book_id: ID): Promise<PageDataRet[]> {
        const sql = "SELECT * FROM pagesView WHERE book_id = ?";

        const pages = await pool
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        return pages;
    }

    /**
     * Retrieves the first page by book ID.
     * @param book_id - The ID of the book.
     * @returns A Promise that resolves to the first page data.
     */
    async getFirstByBookId(book_id: ID): Promise<PageDataRet> {
        const sql =
            "SELECT * FROM pagesView WHERE book_id = ? ORDER BY page_number ASC LIMIT 1";

        const page = await pool
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("page", book_id);
                }
                // should only be the same format as PageData
                return res[0];
            });

        return page;
    }

    async getCoverByBookId(book_id: ID): Promise<PageDataRet> {
        const sql =
            "SELECT * FROM pagesView WHERE id = (SELECT cover_page_id FROM books_resolved WHERE id = ?)";

        const page = await pool
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("page", book_id);
                }
                // should only be the same format as PageData
                return res[0];
            });

        return page;
    }

    /**
     * Retrieves the last page by book ID.
     *
     * @param book_id - The ID of the book.
     * @returns A Promise that resolves to the last page data.
     */
    async getLastByBookId(book_id: ID): Promise<PageDataRet> {
        const sql =
            "SELECT * FROM pagesView WHERE book_id = ? ORDER BY page_number DESC LIMIT 1";

        const page = await pool
            .query(sql, [book_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.length != 1) {
                    throw new NotFoundError("Book", book_id);
                }
                // should only be the same format as PageData
                return res[0];
            });

        return page;
    }

    /**
     * Updates a page in the database.
     *
     * @param pageData - The data of the page to be updated.
     * @returns A Promise that resolves to the updated PageData object.
     */
    async update(
        pageData: Partial<PageDataInsert> & { id: ID },
    ): Promise<PageDataRet> {
        if (
            !pageData.id ||
            pageData.book_id == undefined ||
            pageData.page_number == undefined ||
            pageData.background_type_id == undefined ||
            pageData.referenced_page_id == undefined
        ) {
            //TODO: add dynamic updates
            throw new Error("Missing required fields for page update.");
        }

        const sql =
            "UPDATE `pages` SET `book_id` = ?, `page_number` = ?, `background_type_id`=? , `referenced_page_id` = ? WHERE `id` = ?";

        await pool
            .query(sql, [
                pageData.book_id,
                pageData.page_number,
                pageData.background_type_id,
                pageData.referenced_page_id,
                pageData.id,
            ])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Update failed, no rows affected");
                }
            });

        return this.getById(pageData.id);
    }

    /** Returns all references to this page
     * @param page_id
     */
    async getReferencedByPageId(page_id: ID): Promise<PageDataRet[]> {
        const sql = "SELECT * FROM pagesView WHERE referenced_page_id = ?";

        const pages = await pool
            .query(sql, [page_id])
            .then(check_mariadb_warning)
            .then((res) => {
                return res;
            });

        return pages;
    }

    /** Delete a page from the database, returns true if delete was a success.
     *
     * .. warning::
     *      This could be a problem since the page numbers are not updated!
     *
     * @param page_id
     * @returns A Promise that resolves to true if the delete was a success.
     */
    async delete(page_id: number): Promise<boolean> {
        const sql = "DELETE FROM `pages` WHERE `id` = ?";

        const success = await pool
            .query(sql, [page_id])
            .then(check_mariadb_warning)
            .then((res) => {
                if (res.affectedRows == 0) {
                    throw new Error("Delete failed, no rows affected");
                }
                return true;
            });

        //Update page number(s)
        //TODO
        return success;
    }

    async search(query: string): Promise<PageDataRet[]> {
        throw new Error("Method not implemented.");
    }
}
/** We need to globally register each service here
 * to prevent memory leaks in development.
 */
export const pageService = registerService("db_page", () => new PageService());
