/** We have two sql databases
 * and access them with two pools
 *
 * If the pool is not started we start it
 * otherwise we use the existing pool.
 */
import process from "node:process";

import mariadb from "mariadb";

import { registerService } from "@snip/common";
import { config } from "@snip/config/server";

// Small function to convert TINY(1) to boolean
const tinyToBoolean: mariadb.TypeCastFunction = (column, next) => {
    if (column.type == "TINY" && column.columnLength === 1) {
        const val = column.int();
        return val === null ? null : val === 1;
    }
    return next();
};

const pool_data: mariadb.Pool = registerService("pool_data", () => {
    const cnf = config.database.parse().database;
    return mariadb.createPool({
        host: cnf.host,
        user: cnf.user,
        password: cnf.password,
        port: cnf.port,
        database: "snip_data",
        //dateStrings: true,
        connectionLimit: 5,
        trace: config.NODE_ENV === "development", // Enable tracing for development
        bigIntAsNumber: true,
        decimalAsNumber: true,
        insertIdAsNumber: true,
        typeCast: tinyToBoolean,
        timezone: "Europe/Berlin",
    });
});

const pool_permissions: mariadb.Pool = registerService("pool_perms", () => {
    const cnf = config.database.parse().database;
    return mariadb.createPool({
        host: cnf.host,
        user: cnf.user,
        password: cnf.password,
        port: cnf.port,
        database: "snip_perms",
        //dateStrings: true,
        connectionLimit: 5,
        trace: config.NODE_ENV === "development", // Enable tracing for development
        bigIntAsNumber: true,
        decimalAsNumber: true,
        insertIdAsNumber: true,
        typeCast: tinyToBoolean,
        timezone: "Europe/Berlin",
    });
});

async function pingPool(pool: mariadb.Pool, name: string) {
    return await pool
        .getConnection()
        .then((conn) => {
            conn.ping();
            console.debug(
                `[Database][${name}] Connection established (PID:${process.pid})`,
            );
            conn.release();
            return true;
        })
        .catch((err) => {
            console.warn(err); // any of connection time or query time errors from above
            return false;
        });
}

export { pingPool, pool_data, pool_permissions, tinyToBoolean };

//Graceful shutdown
const shutdown = async () => {
    try {
        await pool_data.end();
        await pool_permissions.end();
    } catch (_err) {
        // Ignore errors
    }
};
process.on("SIGTERM", shutdown);
process.on("SIGINT", shutdown);
