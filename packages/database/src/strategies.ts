import {
    BackgroundTypeData,
    BlobData,
    BookDataInsert,
    BookDataRet,
    BookOwner,
    CredentialsData,
    FileData,
    FolderDataInsert,
    FolderDataRet,
    GroupDataInsert,
    GroupDataRet,
    ID,
    LastLoginRet,
    MemberDataRet,
    MemberRole,
    PageDataInsert,
    PageDataRet,
    SnipDataInsert,
    SnipDataRet,
    UserConfigDataInsert,
    UserConfigDataRet,
    UserDataInsert,
    UserDataRet,
} from "./types";

/** We more or less use the strategy pattern to
 * create a database strategy which allows to interact with
 * different tables.
 *
 *
 * see https://en.wikipedia.org/wiki/Strategy_pattern
 */

export interface TableStrategy<I, R extends I = I> {
    getAll: () => Promise<R[]>;
    getById: (id: ID) => Promise<R>;
    getByIds: (ids: ID[]) => Promise<R[]>;
    insert: (data: I) => Promise<R>;
    update: (data: Partial<I> & { id: ID }) => Promise<R>;
    upsert?: (data: I & { id?: ID }) => Promise<R>;
    delete: (id: ID) => Promise<boolean>;
    search: (query: string) => Promise<R[]>;
}

export interface UserStrategy
    extends Omit<TableStrategy<UserDataInsert, UserDataRet>, "search"> {
    // Password
    signupWithPassword: (
        email: string,
        password: string,
    ) => Promise<UserDataRet>;
    verifySignupWithPassword: (email: string) => Promise<boolean>;
    loginWithPassword: (
        email: string,
        password: string,
    ) => Promise<UserDataRet>;
    changePassword: (email: string, password: string) => Promise<boolean>;
    addPasswordCredential: (
        user_id: ID,
        email: string,
        password: string,
    ) => Promise<ID>;
    changeEmail: (credential_id: ID, newEmail: string) => Promise<boolean>;

    // SSO
    signupWithSSO: (
        provider_id: string,
        provider_user_id: string,
        email: string,
        email_verified: boolean,
    ) => Promise<UserDataRet>;
    loginWithSSO: (
        provider_id: string,
        provider_user_id: string,
    ) => Promise<UserDataRet>;
    addSSOCredential: (
        user_id: ID,
        provider_id: string,
        provider_user_id: string,
        email: string,
        email_verified: boolean,
    ) => Promise<ID>;

    isAdmin: (user_id: ID) => Promise<boolean>;

    // Login info
    addLastLogin: (user_id: ID, ip: string) => Promise<boolean>;
    getLastLogins: (user_id: ID, range?: number) => Promise<LastLoginRet[]>;

    // Other
    getPasswordHash: (user_id: ID) => Promise<string>;
    getPasswordCredentialByEmail: (
        email: string,
    ) => Promise<Required<CredentialsData>>;
    getPasswordCredentialByUserId: (
        user_id: ID,
    ) => Promise<Required<CredentialsData>>;
    getByEmail: (email: string) => Promise<UserDataRet[]>;
    getByCredentialId: (credential_id: ID) => Promise<UserDataRet>;
    mergeUsers: (from_id: ID, to_id: ID) => Promise<boolean>;
    setPrimaryCredential: (user_id: ID, credential_id: ID) => Promise<boolean>;

    search(search: string): Promise<UserDataRet[]>;
}

export interface GroupStrategy
    extends TableStrategy<GroupDataInsert, GroupDataRet> {
    getByUserId: (user_id: ID) => Promise<GroupDataRet[]>;
    getGroupIdsByUserId: (user_id: ID) => Promise<ID[]>;
    getMembers: (group_id: ID) => Promise<Required<MemberDataRet>[]>;
    getMember: (user_id: ID, group_id: ID) => Promise<Required<MemberDataRet>>;
    addMember: (
        user_id: ID,
        group_id: ID,
        role: MemberRole,
    ) => Promise<Required<MemberDataRet>>;
    updateMember: (
        user_id: ID,
        group_id: ID,
        role: MemberRole,
    ) => Promise<Required<MemberDataRet>>;
    removeMember: (user_id: ID, group_id: ID) => Promise<boolean>;
    isAllowedToEdit: (user_id: ID, group_id: ID) => Promise<boolean>;
    getRole: (user_id: ID, group_id: ID) => Promise<MemberRole | undefined>;
}

export interface PageStrategy
    extends TableStrategy<PageDataInsert, PageDataRet> {
    getByBookId: (book_id: ID) => Promise<PageDataRet[]>;
    getFirstByBookId: (book_id: ID) => Promise<PageDataRet>;
    getLastByBookId: (book_id: ID) => Promise<PageDataRet>;
    getCoverByBookId: (book_id: ID) => Promise<PageDataRet>;
    getReferencedByPageId: (page_id: ID) => Promise<PageDataRet[]>;
}

export interface BlobStrategy extends TableStrategy<BlobData> {}

export interface BookStrategy
    extends TableStrategy<BookDataInsert, BookDataRet> {
    getByUserId(user_id: ID, pWrite?: boolean): Promise<BookDataRet[]>;

    setOwner: (
        entity_id: ID,
        entity_type: "user" | "group",
        book_id: ID,
    ) => Promise<BookOwner>;
    setOwnerByUser: (user_id: ID, book_id: ID) => Promise<BookOwner>;
    setOwnerByGroup: (group_id: ID, book_id: ID) => Promise<BookOwner>;
    getOwner: (book_id: ID) => Promise<BookOwner>;
}

export interface SnipStrategy
    extends TableStrategy<SnipDataInsert, SnipDataRet> {
    getByPageId: (page_id: number) => Promise<SnipDataRet[]>;
    getQueuedByBookId: (book_id: number) => Promise<SnipDataRet[]>;
    getQueuedIdsByBookId: (book_id: number) => Promise<number[]>;
    placeOnPage: (snip_id: number, page_id: number) => Promise<SnipDataRet>;
    deleteNested: (snip_id: ID) => Promise<boolean>;
}

export interface UserConfigStrategy
    extends TableStrategy<UserConfigDataInsert, UserConfigDataRet> {
    getByUserId: (user_id: ID) => Promise<UserConfigDataRet>;
    updateByUserId: (
        data: UserConfigDataInsert & { user_id: ID },
    ) => Promise<UserConfigDataRet>;
}

export interface FolderStrategy
    extends TableStrategy<FolderDataInsert, FolderDataRet> {
    delete: (id: ID, user_id?: ID) => Promise<boolean>;
    move: (
        type: "folder" | "book",
        item: ID,
        moveTo: ID | "root",
        user_id: ID,
    ) => Promise<boolean>;
    getFilesByUserId: (user_id: ID) => Promise<FileData[]>;
}

export interface BackgroundTypeStrategy
    extends TableStrategy<BackgroundTypeData> {
    setBookDefault: (book_id: ID, background_type_id: ID) => Promise<true>;

    setPageById: (
        book_id: ID,
        page_id: ID,
        background_type_id: ID,
    ) => Promise<true>;
    setPageByIds: (
        book_id: ID,
        page_ids: ID[],
        background_type_id: ID,
    ) => Promise<true>;

    setPageByNumber: (
        book_id: ID,
        page_number: number,
        background_type_id: ID,
    ) => Promise<true>;

    setPageByNumbers: (
        book_id: ID,
        page_numbers: number[],
        background_type_id: ID,
    ) => Promise<true>;

    getSelectedByBookId: (book_id: ID) => Promise<ID>;
}
