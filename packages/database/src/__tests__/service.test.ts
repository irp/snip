import service from "../service";

describe("service", () => {
    it("should be able to create a new service", async () => {
        expect(service).toBeDefined();

        expect(service.user).toBeDefined();
        expect(service.group).toBeDefined();
        expect(service.page).toBeDefined();
        expect(service.book).toBeDefined();
        expect(service.snip).toBeDefined();
        expect(service.permission).toBeDefined();
        expect(service.userConfig).toBeDefined();
    });
});
