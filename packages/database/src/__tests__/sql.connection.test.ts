import { FieldInfo, createPool } from "mariadb";
import {
    pool_data,
    pool_permissions,
    pingPool,
    tinyToBoolean,
} from "../sql.connection";

describe("pingPool", () => {
    let consoleSpy: any;
    beforeEach(() => {
        if (typeof consoleSpy === "function") {
            consoleSpy.mockRestore();
        }
    });

    it("should be able to ping the database", async () => {
        // no error printed
        const p = await pingPool(pool_data, "pool_data");
        expect(p).toBe(true);

        const p2 = await pingPool(pool_permissions, "pool_permissions");
        expect(p2).toBe(true);
    });

    it("should return false when pinging a non-existing database", async () => {
        const bs_pool = createPool({
            host: "localhost",
            user: "root",
            password: "password",
            port: 3306,
            database: "non_existing_db",
            acquireTimeout: 1000,
        });
        consoleSpy = vi.spyOn(console, "warn").mockImplementation(() => {});
        const p = await pingPool(bs_pool, "non_existing_pool");
        expect(p).toBe(false);
        expect(consoleSpy).toHaveBeenCalled();
    }, 20000);
});

describe("tinyToBoolean", () => {
    it("should convert TINY column with value 1 to true", () => {
        const column = {
            type: "TINY",
            columnLength: 1,
            collation: "",
            columnType: "",
            scale: 0,
            flags: 0,
            int: vi.fn().mockReturnValue(1),
        } as unknown as FieldInfo;

        const result = tinyToBoolean(column, vi.fn());

        expect(result).toBe(true);
        expect(column.int).toHaveBeenCalled();
    });

    it("should convert TINY column with value 0 to false", () => {
        const column = {
            type: "TINY",
            columnLength: 1,
            collation: "",
            columnType: "",
            scale: 0,
            flags: 0,
            int: vi.fn().mockReturnValue(0),
        } as unknown as FieldInfo;

        const result = tinyToBoolean(column, vi.fn());

        expect(result).toBe(false);
        expect(column.int).toHaveBeenCalled();
    });

    it("should return null for null TINY column", () => {
        const column = {
            type: "TINY",
            columnLength: 1,
            collation: "",
            columnType: "",
            scale: 0,
            flags: 0,
            int: vi.fn().mockReturnValue(null),
        } as unknown as FieldInfo;

        const result = tinyToBoolean(column, vi.fn());

        expect(result).toBeNull();
        expect(column.int).toHaveBeenCalled();
    });

    it("should call the next type cast function for non-TINY columns", () => {
        const column = {
            type: "INT",
            columnLength: 11,
            collation: "",
            columnType: "",
            scale: 0,
            flags: 0,
            int: vi.fn().mockReturnValue(1),
        } as unknown as FieldInfo;

        const next = vi.fn().mockReturnValue("next result");

        const result = tinyToBoolean(column, next);

        expect(result).toBe("next result");
        expect(column.int).not.toHaveBeenCalled();
        expect(next).toHaveBeenCalled();
    });
});
