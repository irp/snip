/**
 * Checks for MariaDB error and throws if there is one.
 *
 * @param res
 */

import { SqlError } from "mariadb";

export function check_mariadb_warning<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    T extends { warningStatus: number } = any,
>(res: T): T {
    if (res.warningStatus > 0) {
        console.error(res);
        throw new MariaDBWarning("Status: " + res.warningStatus);
    }
    return res;
}

export class MariaDBWarning extends Error {
    constructor(message: string) {
        super(message);
        this.name = "MariaDBError";
    }
}

/**
 * Represents an error that occurs when a resource is not found in the database.
 */
export class NotFoundError extends Error {
    /**
     * Creates a new instance of the NotFoundError class.
     * @param resource - The name of the resource or an array of resource names.
     * @param id - The ID of the resource or an array of resource IDs.
     */
    constructor(resource: string | string[], id: unknown) {
        super(`Resource '${resource}' with id '${id}' not found in database`);
        this.name = "NotFoundError";
    }
}

/** No rows were affected by the query, i.e. no rows were updated or deleted
 *
 */
export class NoRowsAffectedError extends Error {
    type: "delete" | "update";
    constructor(type: "delete" | "update", resource: string, id: unknown) {
        super(`No rows affected for ${type} of ${resource} with id ${id}`);
        this.type = type;
        this.name = "NoRowsAffectedError";
    }
}
/* -------------------------------------------------------------------------- */
/*                 custom errors raised in the db by signals                  */
/* -------------------------------------------------------------------------- */

export function catch_db_signals(e: SqlError): void {
    if (e.sqlState && e.sqlState == "45000") {
        // We handle user defined errors here all other errors are rethrown
        switch (e.errno) {
            case 3401:
                throw new InviteAlreadyAcceptedError(e.sqlMessage);
            case 3402:
                throw new InviteExpiredError(e.sqlMessage);
            case 3403:
                throw new NotFoundError("Collaboration Invite", e.sqlMessage);
            case 3410:
                throw new SnipUserIdNotAssignedError(e.sqlMessage);
            case 3420:
                throw new EmailExistsError(e.sqlMessage);
            case 3421:
                throw new ProviderNullError(e.sqlMessage);
            default:
                console.error(e);
                throw e;
        }
    }
    throw e;
}

export class InviteExpiredError extends Error {
    constructor(msg: string | null) {
        super(msg || "Invite expired");
        this.name = "InviteExpiredError";
    }
}

export class InviteAlreadyAcceptedError extends Error {
    constructor(msg: string | null) {
        super(msg || "Invite already accepted");
        this.name = "InviteAlreadyAcceptedError";
    }
}

export class SnipUserIdNotAssignedError extends Error {
    constructor(msg: string | null) {
        super(msg || "Snip user id not assigned");
        this.name = "SnipUserIdNotAssignedError";
    }
}

export class EmailExistsError extends Error {
    constructor(msg: string | null) {
        super(msg || "Email already exists");
        this.name = "EmailAlreadyExistsError";
    }
}

export class ProviderNullError extends Error {
    constructor(msg: string | null) {
        super(msg || "Provider id is null");
        this.name = "ProviderNullError";
    }
}
