import { exec } from "child_process";
import { defineConfig } from "vitest/config";

// check if docker is installed
// if not, throw an error
await new Promise<{ stdout: string; stderr: string }>((resolve, reject) => {
    exec("docker -v", (error, stdout, stderr) => {
        if (error) {
            reject(error);
        } else {
            resolve({ stdout, stderr });
        }
    });
});

// setup global environment for vitest
export default defineConfig({
    test: {
        globalSetup: "./test_setup/global_setup.ts",
        setupFiles: ["./test_setup/global_mocks.ts"],
        globals: true,
        coverage: {
            provider: "v8",
            include: ["src/**/*"],
        },
        sequence: {
            concurrent: false,
        },
        fileParallelism: false,
        pool: "threads",
        poolOptions: {
            threads: {
                maxThreads: 1,
                minThreads: 1,
            },
        },
    },
    resolve: {
        alias: {
            "@/": new URL("./src/", import.meta.url).pathname,
        },
    },
});
