// vitest.config.ts
import { defineConfig } from "vitest/config";

export default defineConfig({
    test: {
        //setupFiles: ["./test_setup/global_mocks.ts"],
        globals: true,
        server: {
            deps: {
                inline: ["@snip/common"],
            },
        },
    },
    resolve: {
        alias: {
            "@/": new URL("./src/", import.meta.url).pathname,
        },
    },
});
