import { type } from "arktype";
import type {
    Code,
    Emphasis,
    Heading,
    InlineCode,
    List,
    Node,
    Paragraph,
    Parent,
    PhrasingContent,
    Root,
    Strong,
} from "mdast";
import { fromMarkdown } from "mdast-util-from-markdown";
import type { QueryCapture } from "web-tree-sitter";

import { hasOwnProperty } from "@snip/common";

import {
    BaseSnip,
    BaseSnipArgs,
    BaseViewSchema,
    RenderContext,
    SnipData,
    SnipDataSchema,
} from "./base";
import {
    getCodeCaptures,
    getContext,
    iterateSourceByColor,
    LangContext,
} from "./code";

/* -------------------------------------------------------------------------- */
/*                             MarkdownSnip typing                            */
/* -------------------------------------------------------------------------- */

export const MarkdownDataSchema = type({
    text: type("string").describe("The markdown text."),
    "version?": type("number.integer >= 0").describe(
        "The version of the markdown text, leave empty for latest version.",
    ),
});

export const MarkdownViewSchema = type(
    {
        "lineWrap?": type("number.integer >= 200").describe(
            "The width of the text wrap in pixels.",
        ),
    },
    "&",
    BaseViewSchema,
).describe(BaseViewSchema.description);

export const MarkdownSnipSchema = type(SnipDataSchema, "&", {
    data: MarkdownDataSchema,
    "view?": MarkdownViewSchema,
}).describe(
    "Represents markdown text on a page. The support for markdown at the moment is highly experimental.",
);

export type MarkdownData = typeof MarkdownDataSchema.infer;
export type MarkdownView = typeof MarkdownViewSchema.infer;

export type MarkdownSnipArgs = BaseSnipArgs &
    MarkdownView &
    Omit<MarkdownData, "version" | "type">;

type ExtCode = Code & { langContext?: LangContext; captures: QueryCapture[] };

/* -------------------------------------------------------------------------- */
/*                         MarkdownSnip implementation                        */
/* -------------------------------------------------------------------------- */
/** Markdown parsing is implemented using tokens
 * we parse a text as tokens and than allow to display it.
 */

export class MarkdownSnip extends BaseSnip {
    protected static _schema = MarkdownSnipSchema;
    public type = "markdown";

    public version: number;

    // Indicate if the code parser loaded
    public ready: Promise<boolean>;

    private _raw_text: string;
    private _mdast?: Root;

    private spacing: MarkdownSpacing;

    constructor({ text = "", lineWrap = 800, ...args }: MarkdownSnipArgs) {
        super(args);

        // Hardcoded version number
        this.version = 0;

        this._raw_text = text;
        this.spacing = new MarkdownSpacing(lineWrap);

        // Check if code parsing is needed an load deps
        // if needed
        this.ready = this.generate_mdast();
    }

    /** Generates the abstract syntax tree for the markdown text.
     * Also parses the code blocks in the markdown text using
     * tree-sitter parsers.
     *
     * This adds a `code_tree` property to the code nodes in the
     * mdast which contains the tree-sitter tree for the code block.
     */
    public generate_mdast() {
        const parse_code_block = async (node: Node) => {
            if (hasOwnProperty(node, "children")) {
                await Promise.all(
                    (node as Parent).children.map(parse_code_block),
                );
            }
            if (node.type === "code") {
                const context = await this.getContext((node as Code).lang);
                if (context) {
                    const captures = getCodeCaptures({
                        src: (node as Code).value,
                        context,
                    });
                    (node as ExtCode).langContext = context;
                    (node as ExtCode).captures = captures;
                }
            }
        };
        this._mdast = fromMarkdown(this._raw_text);

        return Promise.all(
            this._mdast.children.map(parse_code_block.bind(this)),
        )
            .then(() => {
                return true;
            })
            .catch((err) => {
                console.error("Failed to parse code block", err);
                return false;
            });
    }

    // Get context, mainly here to allow monkey patching
    // in the server side
    async getContext(lang: string | null | undefined) {
        return getContext(lang);
    }

    /** Construct a instance of the snip from a SnipData object. */
    static from_data(data: SnipData<MarkdownData, MarkdownView>): MarkdownSnip {
        return new MarkdownSnip({
            ...data.data,
            lineWrap: data.view?.lineWrap,
            // Base
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
            created_by: data.created_by,
            x: data.view?.x,
            y: data.view?.y,
            rot: data.view?.rot,
            mirror: data.view?.mirror,
        });
    }

    data(): MarkdownData {
        return {
            text: this.text,
            version: this.version,
        };
    }
    view(): MarkdownView {
        return {
            lineWrap: this.spacing.WRAP,
            ...super.view(),
        };
    }

    /** The raw text of the markdown snip. */
    get text(): string {
        return this._raw_text;
    }
    async setText(t: string) {
        if (this._raw_text === t) {
            return;
        }
        this._raw_text = t;
        this._height = null;
        await this.generate_mdast();
    }

    _height: number | null = null;
    get height(): number {
        if (this._height === null) {
            // We need to render the snip to get the height
            const c = this.createCanvas(10, 10);
            const ctx = c.getContext("2d")!;
            this._render(ctx); // this should set the height as side effect
        }

        if (this._height == null) {
            // Still null?
            console.error("Not able to calculate height");
            return 100;
        }

        return this._height;
    }

    get width(): number {
        // FIXME: Calculate width based on the content
        return this.spacing.WRAP;
    }

    get mdast(): Root {
        if (!this._mdast) {
            throw new Error("MDAST not generated yet.");
        }
        return this._mdast;
    }

    /** Render the ast to canvas
     * might only support limited syntax for now.
     */
    render(ctx: RenderContext): void {
        // Apply translate, rotate and mirror
        // Base transform in transform function
        // see base.ts
        ctx.textBaseline = "top";

        const transform = ctx.getTransform();
        this.transform(ctx);

        this._render(ctx);

        // Restore the transform
        ctx.setTransform(transform);
    }

    _render(ctx: RenderContext): void {
        const [_currentX, finalY] = this.renderNode(ctx, 0, 0, this.mdast);
        this._height = finalY;
    }

    private renderNode(
        context: RenderContext,
        x: number,
        y: number,
        node: Node | Root | Parent,
    ): [number, number] {
        let currentX = x;
        let currentY = y;
        switch (node.type) {
            case "root":
                for (const child of (node as Root).children) {
                    [currentX, currentY] = this.renderNode(
                        context,
                        currentX,
                        currentY,
                        child,
                    );
                }
                return [currentX, currentY] as const;
            case "heading":
                // Draw text heading
                return this._renderHeading(
                    context,
                    node as Heading,
                    currentX,
                    currentY,
                );
            case "paragraph":
                // Draw text paragraph
                return this._renderParagraph(
                    context,
                    node as Paragraph,
                    currentX,
                    currentY,
                );
            case "thematicBreak":
                // Draw a line takes up the full width of the canvas
                // and one line height
                context.beginPath();
                context.moveTo(0, currentY + this.spacing.LINE_HEIGHT / 2);
                context.lineTo(
                    this.spacing.WRAP,
                    currentY + this.spacing.LINE_HEIGHT / 2,
                );
                context.stroke();
                currentY += this.spacing.LINE_HEIGHT;
                return [currentX, currentY] as const;
            case "code":
                // Draw code block
                return this._renderCode(
                    context,
                    node as ExtCode,
                    currentX,
                    currentY,
                );
            case "list":
                // Draw list
                return this._renderList(
                    context,
                    node as List,
                    currentX,
                    currentY,
                );
            default:
                console.warn("Unknown node type", node);
                return [currentX, currentY] as const;
        }
    }

    /**
     * Renders a heading element by setting appropriate font size and style
     * and processing its inline children elements.
     *
     * @param {RenderContext} context - The rendering context that contains
     * necessary drawing settings like font and color.
     * @param {Heading} node - The heading node containing details like depth
     * and inline children to be rendered.
     */
    private _renderHeading(
        context: RenderContext,
        node: Heading,
        _x: number,
        y: number,
    ): [number, number] {
        const baseFontSize = this.spacing.headingFontSize(node.depth);

        // Save original context settings to restore them later.
        const originalFont = context.font;
        const originalFillStyle = context.fillStyle;

        context.font = `bold ${baseFontSize}px Arial`; //FIXME: Font
        context.fillStyle = "black";

        let currentX = 0; // Heading is always at the start of the line
        let currentY = y;

        // Render each inline child within the heading
        node.children.forEach((child) => {
            [currentX, currentY] = this._renderInline(
                context,
                child,
                currentX,
                currentY,
            );
        });

        // Advance currentY for subsequent content and apply heading spacing.
        currentY += baseFontSize + this.spacing.headingSpacing(node.depth);

        // Restore original context settings.
        context.font = originalFont;
        context.fillStyle = originalFillStyle;

        return [0, currentY] as const;
    }

    private _renderParagraph(
        context: RenderContext,
        node: Paragraph,
        x: number,
        y: number,
        level: number = 0,
        paddingBottom: number | null = null,
    ): [number, number] {
        // Save original context settings to restore them later.
        const originalFont = context.font;
        const originalFillStyle = context.fillStyle;

        // Set the paragraph style: regular font and black text.
        context.font = this.spacing.FONT;
        context.fillStyle = "black";

        let currentX = x;
        let currentY = y;
        node.children.forEach((child) => {
            [currentX, currentY] = this._renderInline(
                context,
                child,
                currentX,
                currentY,
                level,
            );
        });

        // Update the global Y position to be below the paragraph.
        if (paddingBottom !== null) {
            currentY += paddingBottom;
        } else {
            currentY += this.spacing.LINE_HEIGHT * 2;
            currentX = this.spacing.levelPadding(level);
        }
        // Restore the original canvas settings.
        context.font = originalFont;
        context.fillStyle = originalFillStyle;

        return [currentX, currentY] as const;
    }

    private _renderList(
        context: RenderContext,
        node: List,
        _x: number,
        y: number,
        level = 0,
    ): [number, number] {
        // Set the paragraph style: regular font and black text.
        context.font = this.spacing.FONT;
        context.fillStyle = "black";

        let currentX = this.spacing.levelPadding(level);
        let currentY = y;
        console.log("List", node);

        // Iterate over each list item in the list
        node.children.forEach((item, index) => {
            // Render ordered or unordered list item
            const listPrefix = node.ordered
                ? `${(node.start ?? 1) + index}. `
                : "• ";

            [currentX, currentY] = renderTextWithWrap(
                context,
                listPrefix,
                currentX,
                currentY,
                level,
                this.spacing,
            );

            item.children.forEach((child) => {
                if (child.type === "paragraph") {
                    // Render each piece of text in the paragraph
                    [currentX, currentY] = this._renderParagraph(
                        context,
                        child as Paragraph,
                        currentX,
                        currentY,
                        level + 1,
                        0,
                    );
                } else if (child.type === "list") {
                    // Handle nested list recursively
                    currentY += this.spacing.LINE_HEIGHT; // FIXME: Add extra spacing? before entering a new list
                    [currentX, currentY] = this._renderList(
                        context,
                        child as List,
                        currentX,
                        currentY,
                        level + 1,
                    );
                    currentY -= this.spacing.LINE_HEIGHT; // FIXME: Add extra spacing? after exiting a nested list
                }
            });
            // Move to the next list item (line)
            currentY += this.spacing.LINE_HEIGHT; // FIXME: ?Extra space between list items
            currentX = this.spacing.levelPadding(level);
        });

        if (level === 0) {
            // Add extra space after the list
            currentY += this.spacing.LINE_HEIGHT;
        }

        return [currentX, currentY] as const;
    }

    private _renderInline(
        context: RenderContext,
        node: PhrasingContent,
        x: number,
        y: number,
        level = 0,
    ): readonly [number, number] {
        switch (node.type) {
            case "text":
                // Render text content and apply word wrapping
                return renderTextWithWrap(
                    context,
                    node.value,
                    x,
                    y,
                    level,
                    this.spacing,
                );
            case "strong":
                return this.__renderStyled(context, node, x, y, level, "bold");
            case "emphasis":
                return this.__renderStyled(
                    context,
                    node,
                    x,
                    y,
                    level,
                    "italic",
                );
            case "inlineCode":
                // Render inline code block
                return this._renderInlineCode(context, node, x, y);
            default:
                // TODO: Implement other inline node types
                console.warn("Unknown inline node type", node.type);
                return [x, y] as const;
        }
    }

    private _renderCode(
        context: RenderContext,
        node: ExtCode,
        _x: number,
        y: number,
    ): [number, number] {
        const originalFont = context.font;
        const originalFillStyle = context.fillStyle;

        context.fillStyle = "black";
        context.font = this.spacing.FONT_CODE;

        let currentX = 0; // Code block is always at the start of the line
        let currentY = y;
        if (!node.langContext || !node.captures) {
            // Render as plain text
            const lines = node.value.split("\n");
            lines.forEach((line, i) => {
                if (i > 0) {
                    currentX = 0;
                    currentY += this.spacing.FONT_SIZE;
                }
                context.fillText(line, currentX, currentY);
            });
            currentY += this.spacing.FONT_SIZE;
        } else {
            // Render with syntax highlighting
            for (const token of iterateSourceByColor({
                src: node.value,
                captures: node.captures,
                context: node.langContext,
            })) {
                context.fillStyle = token.color;
                const t = token.text.split("\n");
                t.forEach((line, i) => {
                    if (i > 0) {
                        currentX = 0;
                        currentY += this.spacing.FONT_SIZE;
                    }
                    context.fillText(line, currentX, currentY);
                });
                currentX += context.measureText(t.at(-1)!).width;
            }
            currentY += this.spacing.FONT_SIZE;
        }
        currentY += this.spacing.FONT_SIZE;

        context.font = originalFont;
        context.fillStyle = originalFillStyle;

        return [0, currentY] as const;
    }

    private _renderInlineCode(
        context: RenderContext,
        node: InlineCode,
        x: number,
        y: number,
    ) {
        const originalFont = context.font;
        const originalFillStyle = context.fillStyle;

        context.fillStyle = "black";
        context.font = this.spacing.FONT_CODE;

        let currentX = x;
        let currentY = y;

        const lines = node.value.split("\n");
        for (let i = 0; i < lines.length; i++) {
            if (i > 0) {
                currentX = x;
                currentY += this.spacing.FONT_SIZE;
            }
            context.fillText(lines[i]!, currentX, currentY);
            currentX += context.measureText(lines[i]!).width;
        }

        context.font = originalFont;
        context.fillStyle = originalFillStyle;

        return [currentX, currentY] as const;
    }

    private __renderStyled(
        context: RenderContext,
        node: Strong | Emphasis,
        x: number,
        y: number,
        level: number,
        style: "bold" | "italic",
    ) {
        const originalFont = context.font;
        if (!originalFont.includes(style)) {
            context.font = `${style} ` + originalFont;
        }

        let currentX = x;
        let currentY = y;
        node.children.forEach((child) => {
            [currentX, currentY] = this._renderInline(
                context,
                child,
                currentX,
                currentY,
                level,
            );
        });
        context.font = originalFont;
        return [currentX, currentY] as const;
    }
}

/** Split a text by words
 * and wrap it to a given width
 * of the canvas.
 */
function renderTextWithWrap(
    ctx: RenderContext,
    text: string,
    x: number,
    y: number,
    level: number,
    spacing: MarkdownSpacing,
) {
    const parts = text.match(/\S+|\s+/g) || [];
    let currentX = x;
    let currentY = y;
    parts.forEach((part) => {
        const width = ctx.measureText(part).width;
        if (currentX + width > spacing.WRAP) {
            // Reset X position to the start of the line
            currentX = spacing.levelPadding(level);
            // Move Y position to the next line
            currentY += spacing.LINE_HEIGHT;
            part = part.trimStart();
        }

        ctx.fillText(part, currentX, currentY);
        currentX += ctx.measureText(part).width;
    });
    return [currentX, currentY] as const;
}

class MarkdownSpacing {
    readonly LINE_HEIGHT = 12 * 2.35 * 1.5;
    readonly FONT_SIZE = 12 * 2.35;
    WRAP = 400;

    constructor(wrap: number) {
        this.WRAP = wrap;
    }

    get FONT() {
        return `${this.FONT_SIZE}px Arial`;
    }
    get FONT_CODE() {
        return `${this.FONT_SIZE - 1}px PlexMono`;
    }

    readonly LEVEL_PADDING = 15;

    // Mostly applies to lists
    levelPadding(level: number) {
        return this.LEVEL_PADDING * level;
    }

    headingFontSize(depth: number) {
        return Math.max(32 - (depth - 1) * 4, this.FONT_SIZE);
    }
    headingSpacing(depth: number) {
        return Math.max(this.LINE_HEIGHT / 2 - (depth - 0.5) * 2, 0);
    }
}
