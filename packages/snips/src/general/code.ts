import {
    Language,
    Parser,
    Query,
    QueryCapture,
    type Tree,
} from "web-tree-sitter";

const parser_init = false;

// FIXME: add more languages no hardcoding
const supported_languages = ["python", "javascript"];

export interface LangContext {
    parser: Parser;
    query: Query;
    theme: Record<string, string>;
}
const contexts: Map<string, LangContext> = new Map();

/**
 * Retrieves or initializes the language context for a specified language.
 * If the parser has not been initialized, it initializes the parser first.
 * Then, if the language context does not exist in the contexts map and the language
 * is supported, it loads the necessary language wasm, parses the query string,
 * and the theme JSON, sets up the parser language, and stores the context in the map.
 *
 * @param lang - The language identifier for which the context is to be retrieved.
 * @returns A Promise that resolves to the LangContext for the specified language,
 *          or undefined if the language is not supported.
 */
export async function getContext(
    lang: string | null | undefined,
    c_fetch: typeof globalThis.fetch = globalThis.fetch,
): Promise<LangContext | undefined> {
    if (!lang) return undefined;

    if (parser_init === false) {
        await Parser.init();
    }

    if (!contexts.has(lang) && supported_languages.includes(lang)) {
        const parser = new Parser();
        const [lang_wasm, query_str, theme_json] = await Promise.all([
            c_fetch(`/grammars/tree-sitter-${lang}.wasm`)
                .then((res) => res.arrayBuffer())
                .then((buf) => Language.load(new Uint8Array(buf)))
                .catch((e) => {
                    console.error(`Failed to load wasm for ${lang}`);
                    console.error(e);
                }),
            c_fetch(`/grammars/${lang}.scm`)
                .then((res) => res.text())
                .catch(() => {
                    console.error(`Failed to load query for ${lang}`);
                    return "";
                }),
            c_fetch(`/grammars/${lang}-theme.json`)
                .then((res) => res.json())
                .catch(() => {
                    console.error(`Failed to load theme for ${lang}`);
                    return {};
                }),
        ]);
        if (!lang_wasm) return undefined;
        parser.setLanguage(lang_wasm);
        contexts.set(lang, {
            parser,
            query: new Query(lang_wasm, query_str),
            theme: theme_json,
        });
    }

    return contexts.get(lang);
}

// Priority map for captures
const priorityMap: Record<string, number> = {
    keyword: 1,
    function: 2,
    variable: 3,
    comment: 4,
    string: 5,
    number: 6,
    operator: 7,
    punctuation: 8,
};

function getPriority(name: string) {
    const s_name = name.split(".");
    let priority: number | undefined;
    for (let i = s_name.length; i > 0; i--) {
        priority = priority || priorityMap[s_name.slice(0, i).join(".")];
    }
    return priority || 99;
}

/**
 * Analyzes a source code string and returns the query captures based on a
 * language-specific query. It utilizes tree-sitter to parse the source code and
 * extract meaningful code captures, such as keywords, functions, variables, etc.,
 * based on a priority order.
 *
 * @param src - The source code string to analyze.
 * @param lang - The language identifier for the source code string.
 * @param old_tree - An optional existing Tree object to reuse when parsing the source code.
 * @returns A Promise that resolves to a sorted array of QueryCaptures, or undefined if
 *          the language is not supported or the parsing fails.
 */
export function getCodeCaptures({
    src,
    oldTree = undefined,
    context,
}: {
    src: string;
    oldTree?: Tree;
    context: LangContext;
}): QueryCapture[] {
    const tree = context.parser.parse(src, oldTree);

    if (!tree) return [];

    const matches = context.query.matches(tree.rootNode);

    return matches
        .flatMap((m) => m.captures)
        .sort((a, b) => {
            if (a.node.startIndex === b.node.startIndex) {
                return getPriority(a.name) - getPriority(b.name);
            }
            return a.node.startIndex - b.node.startIndex;
        });
}

/**
 * Determines the color associated with a given capture name in the context of
 * a specific language's theme. It resolves the most specific color available
 * in the theme for the hierarchical capture name, falling back to a default
 * color if none is found.
 *
 * @param name - The hierarchical capture name, typically delimited by dots,
 *               representing a structured identifier, like 'function.name'.
 * @param context - The language-specific context that includes the theme,
 *                  which maps capture names to their corresponding display colors.
 * @returns The color string associated with the capture name. If no matching
 *          color is found in the theme, it defaults to the theme's "default"
 *          color, or "black" if "default" is not specified.
 */
export function getCaptureColor(name: string, context: LangContext) {
    const s_name = name.split(".");
    let color;
    for (let i = s_name.length; i > 0; i--) {
        color = color || context.theme[s_name.slice(0, i).join(".")];
    }
    return color || context.theme["default"] || "black";
}

/** Iterate source by color
 */
export function* iterateSourceByColor({
    src,
    captures,
    context,
}: {
    src: string;
    captures: QueryCapture[];
    context: LangContext;
}) {
    let currentIndex = 0;
    for (const { node, name } of captures) {
        // Text before the node which was not captured yet
        const offset_str = src.slice(currentIndex, node.startIndex);
        if (offset_str.length > 0)
            yield {
                color: getCaptureColor("default", context),
                text: offset_str,
            };

        // Text of the node
        if (node.startIndex >= currentIndex) {
            yield {
                color: getCaptureColor(name, context),
                text: src.slice(node.startIndex, node.endIndex),
            };
        }
        currentIndex = node.endIndex;
    }
    const offset_str = src.slice(currentIndex);
    if (offset_str.length > 0)
        yield {
            color: getCaptureColor("default", context),
            text: offset_str,
        };
}
