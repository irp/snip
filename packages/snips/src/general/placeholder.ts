import { type } from "arktype";

import {
    BaseSnip,
    BaseSnipArgs,
    BaseViewSchema,
    SnipData,
    SnipDataSchema,
} from "./base";

import { DataValidationError } from "@//errors";

/** A placeholder snip can be used
 * in places where it might be too expensive to transfer a full
 * snip. This was mainly introduced to streamline the event
 * protocol.
 *
 * For instance instead of transferring image data we just transfer
 * a snip with the height, width precomputed.
 */
/* -------------------------------------------------------------------------- */
/*                           PlaceholderSnip typing                           */
/* -------------------------------------------------------------------------- */

const PlaceholderDataSchema = type({
    originalType: "string",
});

const PlaceholderViewSchema = type(
    {
        width: "number > 0",
        height: "number > 0",
    },
    "&",
    BaseViewSchema,
);

const PlaceholderSnipSchema = type(SnipDataSchema, "&", {
    data: PlaceholderDataSchema,
    "view?": PlaceholderViewSchema,
});

export type PlaceholderData = typeof PlaceholderDataSchema.infer;
export type PlaceholderView = typeof PlaceholderViewSchema.infer;

type PlaceholderSnipArgs = BaseSnipArgs &
    Omit<PlaceholderData, "type"> &
    PlaceholderView;

/* -------------------------------------------------------------------------- */
/*                       PlaceholderSnip Implementation                       */
/* -------------------------------------------------------------------------- */
export class PlaceholderSnip extends BaseSnip {
    type: string = "placeholder";

    height: number;
    width: number;
    originalType: string;

    constructor({
        originalType,
        height,
        width,
        ...baseArgs
    }: PlaceholderSnipArgs) {
        super({ ...baseArgs });
        this.width = width;
        this.height = height;
        this.originalType = originalType;
    }

    static from_data(
        data: SnipData<PlaceholderData, PlaceholderView>,
    ): PlaceholderSnip {
        const validation = PlaceholderSnipSchema(data);
        if (validation instanceof type.errors) {
            throw DataValidationError.from_ark(validation);
        }

        return new PlaceholderSnip({
            // placeholder
            originalType: validation.data.originalType,
            height: validation.view?.height || 0,
            width: validation.view?.width || 0,
            // Base
            x: validation.view?.x,
            y: validation.view?.y,
            rot: validation.view?.rot,
            mirror: validation.view?.mirror,
            id: validation.id,
            page_id: validation.page_id,
            book_id: validation.book_id,
            last_updated: validation.last_updated,
            created: validation.created,
        });
    }

    static from_snip(snip: BaseSnip): PlaceholderSnip {
        const data: SnipData<PlaceholderData, PlaceholderView> = {
            view: {
                x: snip.x,
                y: snip.y,
                width: snip.width,
                height: snip.height,
                rot: snip.rot,
                mirror: snip.mirror,
            },
            data: {
                originalType: snip.type,
            },
            type: "placeholder",
            id: snip.id,
            page_id: snip.page_id,
            book_id: snip.book_id,
            last_updated: snip.last_updated,
            created: snip.created,
        };

        return PlaceholderSnip.from_data(data);
    }

    data(): PlaceholderData {
        const d = super.data();
        return {
            ...d,
            originalType: this.originalType,
        };
    }

    view(): PlaceholderView {
        const v = super.view();
        return {
            ...v,
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
            rot: this.rot,
            mirror: this.mirror,
        };
    }

    public render(): void {
        throw new Error("Method not implemented.");
    }
}
