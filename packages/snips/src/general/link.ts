import { type } from "arktype";

import { isValidHttpUrl } from "@snip/common";

import {
    BaseSnip,
    BaseSnipArgs,
    BaseViewSchema,
    MultiPolygon,
    Polygon,
    PolygonI,
    RenderContext,
    SnipData,
    SnipDataSchema,
} from "./base";

import { DataValidationError } from "@//errors";

/* -------------------------------------------------------------------------- */
/*                               LinkSnip typing                              */
/* -------------------------------------------------------------------------- */
const PosYSchema = type('"top" | "bottom" | "center" | number')
    .describe(
        "Position of the icon on the y-axis, one of 'top', 'bottom', 'center'",
    )
    .default("top");
const PosXSchema = type('"right" | "left" | "center" | number')
    .describe(
        "Position of the icon on the x-axis, one of 'right', 'left', 'center'",
    )
    .default("right");

const LinkDataSchema = type({
    snip: type("object").describe("Inner snip data"),
    href: type(
        /(http(s)?:\/\/.)(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/,
    ).describe("The URL to use as link"),
}).describe(
    "The data of the link snippet, is basically a wrapper around any other snip",
);

const LinkViewSchema = type(
    {
        // Place an icon?
        "show_icon?": type("boolean")
            .describe("Show the clickable link icon")
            .default(true),
        // Position of icon
        // defaults to top right
        "icon_pos_y?": PosYSchema,
        "icon_pos_x?": PosXSchema,
    },
    "&",
    BaseViewSchema,
).describe(BaseViewSchema.description);

const LinkSnipSchema = type(SnipDataSchema, "&", {
    data: LinkDataSchema,
    "view?": LinkViewSchema,
}).describe("Represents a link to another external website or resource.");

export type LinkData = typeof LinkDataSchema.infer;
export type LinkView = typeof LinkViewSchema.infer;

export type LinkSnipArgs = LinkView &
    Omit<LinkData, "type"> &
    BaseSnipArgs & {
        snip: BaseSnip;
    };

export class LinkSnip extends BaseSnip {
    /** Basically a wrapper around any snip
     * which optionally places an icon.
     *
     * Icon placement can be configured.
     */

    protected static readonly _schema = LinkSnipSchema;
    public type = "link";

    // Indicate if bitmap was loaded

    public snip: BaseSnip;
    public href: URL;

    // tabler link icon
    public icon_paths = [
        "M9 15l6 -6",
        "M11 6l.463 -.536a5 5 0 0 1 7.071 7.072l-.534 .464",
        "M13 18l-.397 .534a5.068 5.068 0 0 1 -7.127 0a4.972 4.972 0 0 1 0 -7.071l.524 -.463",
    ];
    public show_icon: boolean;
    public icon_pos_y: "top" | "bottom" | "center" | number;
    public icon_pos_x: "right" | "left" | "center" | number;

    constructor({
        snip,
        href,
        show_icon,
        icon_pos_y,
        icon_pos_x,
        ...baseArgs
    }: LinkSnipArgs) {
        super({
            ...baseArgs,
        });

        this.snip = snip;
        // Check if the href is a valid URL
        if (!isValidHttpUrl(href)) {
            throw new Error("Invalid URL, must be a valid HTTP URL");
        }
        this.href = new URL(href);

        if (
            icon_pos_y !== undefined &&
            typeof icon_pos_y !== "number" &&
            !["top", "bottom", "center"].includes(icon_pos_y)
        ) {
            throw new Error(
                "Invalid icon_pos_y, must be 'top', 'bottom', 'center' or a number",
            );
        }

        if (
            icon_pos_x !== undefined &&
            typeof icon_pos_x !== "number" &&
            !["right", "left", "center"].includes(icon_pos_x)
        ) {
            throw new Error(
                "Invalid icon_pos_x, must be 'right', 'left', 'center' or a number",
            );
        }
        this.show_icon = show_icon ?? true;
        this.icon_pos_y = icon_pos_y ?? "top";
        this.icon_pos_x = icon_pos_x ?? "right";

        if (Object.hasOwn(this.snip, "ready")) {
            this.ready = this.snip.ready;
        }
    }

    /** Construct a instance of the snip from a
     * SnipData object. This is used to create a snip
     * object from the database data dict.
     *
     * @param {SnipData<ArrayData, ArrayView>} data - Data dict
     */
    static from_data(
        data: SnipData<LinkData, LinkView>,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        get_snip_from_data?: any,
    ): LinkSnip {
        // Validate outer first
        const validation = LinkSnipSchema(data);
        if (validation instanceof type.errors) {
            throw DataValidationError.from_ark(validation);
        }

        // Validate inner snip
        // This might also throw an error!
        let snip: BaseSnip;
        try {
            snip = get_snip_from_data(data.data.snip) as BaseSnip;
        } catch (e) {
            if (e instanceof DataValidationError) {
                // Adjust path
                e.path.unshift("data", "snip");
                e.message = "data.snip." + e.message;
            }
            throw e;
        }

        // check that book_id is same on inner snip
        if (snip.book_id !== data.book_id) {
            throw new DataValidationError(
                "Book id of inner snip does not match outer snip",
                ["book_id"],
            );
        }

        return new LinkSnip({
            snip,
            href: validation.data.href,
            show_icon: validation.view?.show_icon,
            icon_pos_y: validation.view?.icon_pos_y,
            icon_pos_x: validation.view?.icon_pos_x,

            // Base
            id: validation.id,
            page_id: data.page_id,
            book_id: validation.book_id,
            last_updated: validation.last_updated,
            created: validation.created,
            //x,y is not needed
        });
    }

    public to_data(
        minimal?: boolean | undefined,
    ): SnipData<LinkData, LinkView> {
        const data = super.to_data(minimal) as SnipData<LinkData, LinkView>;

        data.data.snip = this.snip.to_data(minimal);
        data.data.href = this.href.href;

        if (this.icon_pos_y !== "top") {
            data.view.icon_pos_y = this.icon_pos_y;
        }
        if (this.icon_pos_x !== "right") {
            data.view.icon_pos_x = this.icon_pos_x;
        }
        if (!this.show_icon) {
            data.view.show_icon = this.show_icon;
        }

        return data;
    }

    /** Overload getters and setters to use
     * the inner snip as the link does not
     * have any coordinates.
     */
    render(ctx: RenderContext): void {
        if (this.show_icon) {
            ctx.save();
            ctx.strokeStyle = "gray";
            ctx.lineJoin = "round";
            ctx.lineCap = "round";
            ctx.lineWidth = 2;
            ctx.translate(this.icon_x, this.icon_y);
            this.icon_paths.forEach((p) => {
                const path = new Path2D(p);
                ctx.stroke(path);
            });
            ctx.restore();
        }
        this.snip.render(ctx);
    }
    get x(): number {
        return this.snip.x;
    }
    get icon_x(): number {
        let x;
        switch (this.icon_pos_x) {
            case "right":
                x = this.snip.x + this.snip.width;
                break;
            case "left":
                x = this.snip.x - this.icon_width;
                break;
            case "center":
                x = this.snip.x + this.snip.width / 2 - this.icon_width / 2;
                break;
            default:
                x = this.icon_pos_x;
                break;
        }
        return x;
    }

    get icon_y(): number {
        let y;
        switch (this.icon_pos_y) {
            case "top":
                y = this.snip.y - this.icon_height;
                break;
            case "bottom":
                y = this.snip.y + this.snip.height;
                break;
            case "center":
                y = this.snip.y + this.snip.height / 2 - this.icon_height / 2;
                break;
            default:
                y = this.icon_pos_y;
                break;
        }
        return y;
    }

    get icon_width(): number {
        return 24;
    }

    get icon_height(): number {
        return 24;
    }

    set x(value: number) {
        this.snip.x = value;
    }
    get y(): number {
        return this.snip.y;
    }
    set y(value: number) {
        this.snip.y = value;
    }
    get height(): number {
        return this.snip.height;
    }
    get width(): number {
        return this.snip.width;
    }

    get polygon(): PolygonI {
        const icon_p = new Polygon([
            [this.icon_x, this.icon_y],
            [this.icon_x + this.icon_width, this.icon_y],
            [this.icon_x + this.icon_width, this.icon_y + this.icon_height],
            [this.icon_x, this.icon_y + this.icon_height],
        ]);
        const p = new MultiPolygon([this.snip.polygon, icon_p]);
        return p;
    }

    /** Setter for page id
     * allows for nested snips to be placed on
     * page.
     */
    public set page_id(value: number | undefined) {
        this._page_id = value;
        this.snip.page_id = value;
    }

    public get page_id(): number | undefined {
        return this._page_id;
    }

    public async place_on_page(page_id: number): Promise<void> {
        this.page_id = page_id;

        this.snip.socket = this.socket;
        await this.snip.upsert();

        await super.place_on_page(page_id);
    }
}
