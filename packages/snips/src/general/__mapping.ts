import { ArraySnip } from "./array";
import { BaseSnip } from "./base";
import { DoodleSnip } from "./doodle";
import { DummySnip } from "./dummy";
import { ImageSnip } from "./image";
import { LinkSnip } from "./link";
import { MarkdownSnip } from "./markdown";
import { PlaceholderSnip } from "./placeholder";
import { TextSnip } from "./text";

const TYPE_TO_SNIP = new Map<string, typeof BaseSnip>();
TYPE_TO_SNIP.set("base", BaseSnip);
TYPE_TO_SNIP.set("text", TextSnip);
TYPE_TO_SNIP.set("array", ArraySnip);
TYPE_TO_SNIP.set("doodle", DoodleSnip);
TYPE_TO_SNIP.set("image", ImageSnip);
TYPE_TO_SNIP.set("placeholder", PlaceholderSnip);
TYPE_TO_SNIP.set("legacy", ImageSnip);
TYPE_TO_SNIP.set("link", LinkSnip);
TYPE_TO_SNIP.set("dummy", DummySnip);
TYPE_TO_SNIP.set("markdown", MarkdownSnip);

export default TYPE_TO_SNIP;
