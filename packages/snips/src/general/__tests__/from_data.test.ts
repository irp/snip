import { TextSnip } from "../text";
import { ImageSnip } from "../image";
import { DoodleSnip } from "../doodle";
import { PlaceholderSnip } from "../placeholder";
import { DummySnip } from "../dummy";
import { LinkSnip } from "../link";
import { ArraySnip } from "../array";
import { get_snip_from_data } from "@//get_snip_from_data";

/** README:
 *
 * All of the following tests the basic validation
 * of the from_data method of the snips.
 *
 * As this method is primarily used to create snips
 * from user input, it is important to validate the
 * input data.
 *
 */

describe("from_data", () => {
    let base_data: any;
    beforeEach(() => {
        base_data = {
            //id: -1,
            type: "dummy",
            book_id: 1,
            data: {
                type: "dummy",
            },
            view: {
                x: 0,
                y: 0,
            },
            created: new Date(),
            last_updated: "2021-01-01T00:00:00.000Z",
        };
    });

    describe("BaseSnip", () => {
        it("create", async () => {
            const snip = DummySnip.from_data(base_data);

            // @ts-ignore - expectTypeOf is not typed
            expectTypeOf(snip).toEqualTypeOf<DummySnip>();

            expect(snip.type).toBe("dummy");
            expect(snip.height).toBe(0);
            expect(snip.width).toBe(0);
            expect(snip.book_id).toBe(1);
            expect(snip.id).toBeDefined();
        });
        it("type missing", async () => {
            // @ts-ignore
            delete base_data.type;

            expect(() => DummySnip.from_data(base_data)).toThrow(
                "type must be a string (was missing)",
            );
        });

        it("multiple validation errors", async () => {
            // @ts-ignore
            base_data.view.x = "invalid";
            base_data.view.y = "invalid";

            expect(() => DummySnip.from_data(base_data)).toThrow(
                "Multiple data validation errors",
            );
        });

        it.todo("additional field in data");
    });

    describe("TextSnip", () => {
        it("create", async () => {
            base_data.data.text = "test";
            expect(() => TextSnip.from_data(base_data)).not.toThrow();
        });
        it("data missing", async () => {
            // @ts-ignore
            delete base_data.data;
            expect(() => TextSnip.from_data(base_data)).toThrow(
                "data must be an object (was missing)",
            );
        });
        it("text missing", async () => {
            // @ts-ignore
            delete base_data.data.text;
            expect(() => TextSnip.from_data(base_data)).toThrow(
                "data.text must be a string (was missing)",
            );
        });

        it("invalid fontFamily", async () => {
            base_data.data.text = "test";
            base_data.view.font = "invalid";
            expect(() => TextSnip.from_data(base_data)).toThrow(
                /view\.font must be .* \(was "invalid"\)/,
            );
        });
    });

    describe("ImageSnip", () => {
        beforeEach(() => {
            base_data.data = {
                blob: {
                    mime: "image/png",
                    data: img_data,
                    size: 42,
                },
                version: 2,
            };
        });

        it("create", async () => {
            expect(() => ImageSnip.from_data(base_data)).not.toThrow();
        });

        it("blob missing", async () => {
            delete base_data.data.blob;
            expect(() => ImageSnip.from_data(base_data)).toThrow(
                "data.blob must be an object (was missing)",
            );
        });

        it("blob.mime missing", async () => {
            // @ts-ignore
            delete base_data.data.blob.mime;
            expect(() => ImageSnip.from_data(base_data)).toThrow(
                "data.blob.mime must be a string (was missing)",
            );
        });

        it("data.blob.data is invalid", async () => {
            // @ts-ignore
            base_data.data.blob.data = "invalid";
            expect(() => ImageSnip.from_data(base_data)).toThrow(
                /data\.blob\.data .*/,
            );
        });
    });

    describe("DoodleSnip", () => {
        beforeEach(() => {
            base_data.data.edges = [
                [0, 0],
                [10, 10],
            ];
            base_data.view.width = 100;
            base_data.view.smoothing = 0;
        });

        it("create", async () => {
            expect(() => DoodleSnip.from_data(base_data)).not.toThrow();
        });

        it("edges missing", async () => {
            // @ts-ignore
            delete base_data.data.edges;
            expect(() => DoodleSnip.from_data(base_data)).toThrow(
                /data.edges must be *./,
            );
        });

        it("edges invalid", async () => {
            // @ts-ignore
            base_data.data.edges = "invalid";
            expect(() => DoodleSnip.from_data(base_data)).toThrow(
                /data.edges must be *./,
            );

            base_data.data.edges = [1];
            expect(() => DoodleSnip.from_data(base_data)).toThrow(
                "data.edges[0] must be an array (was number)",
            );
        });

        it("smoothing invalid", async () => {
            // @ts-ignore
            base_data.view.smoothing = "invalid";
            expect(() => DoodleSnip.from_data(base_data)).toThrow(
                /view.smoothing must be *./,
            );
        });

        it("edges contain nulls", async () => {
            // @ts-ignore
            base_data.data.edges = [
                [null, null],
                [10, 10],
            ];
            expect(() => DoodleSnip.from_data(base_data)).not.toThrow();
        });
    });

    describe("PlaceholderSnip", () => {
        beforeEach(() => {
            base_data.data.originalType = "image";
            base_data.view.height = 100;
            base_data.view.width = 100;
        });

        it("create", async () => {
            expect(() => PlaceholderSnip.from_data(base_data)).not.toThrow();
        });

        it("originalType missing", async () => {
            // @ts-ignore
            delete base_data.data.originalType;
            expect(() => PlaceholderSnip.from_data(base_data)).toThrow(
                "data.originalType must be a string (was missing)",
            );
        });

        it("height missing", async () => {
            // @ts-ignore
            delete base_data.view.height;
            expect(() => PlaceholderSnip.from_data(base_data)).toThrow(
                "view.height must be a number (was missing)",
            );
        });

        it("width missing", async () => {
            // @ts-ignore
            delete base_data.view.width;
            expect(() => PlaceholderSnip.from_data(base_data)).toThrow(
                "view.width must be a number (was missing)",
            );
        });
    });

    describe("LinkSnip", () => {
        beforeEach(() => {
            base_data.data.href = "https://example.com";
            base_data.data.snip = JSON.parse(JSON.stringify(base_data));
        });

        it("create", async () => {
            expect(() =>
                LinkSnip.from_data(base_data, get_snip_from_data),
            ).not.toThrow();
        });

        it("href missing", async () => {
            // @ts-ignore
            delete base_data.data.href;
            expect(() =>
                LinkSnip.from_data(base_data, get_snip_from_data),
            ).toThrow("data.href must be a string (was missing)");
        });

        it("href invalid", async () => {
            base_data.data.href = "invalid";
            expect(() =>
                LinkSnip.from_data(base_data, get_snip_from_data),
            ).toThrow(/data.href must be The URL to use as link .*/);
        });
    });

    describe("ArraySnip", () => {
        beforeEach(() => {
            base_data.data.snips = [JSON.parse(JSON.stringify(base_data))];
        });

        it("create", async () => {
            expect(() =>
                ArraySnip.from_data(base_data, get_snip_from_data),
            ).not.toThrow();
        });

        it("snips missing", async () => {
            // @ts-ignore
            delete base_data.data.snips;
            expect(() =>
                ArraySnip.from_data(base_data, get_snip_from_data),
            ).toThrow("data.snips must be Array of snips (was missing)");
        });

        it("snips invalid", async () => {
            // @ts-ignore
            base_data.data.snips = "invalid";
            expect(() =>
                ArraySnip.from_data(base_data, get_snip_from_data),
            ).toThrow("data.snips must be Array of snips (was string)");
        });

        it("snips empty", async () => {
            // @ts-ignore
            base_data.data.snips = [];
            expect(() =>
                ArraySnip.from_data(base_data, get_snip_from_data),
            ).toThrow("data.snips must be Array of snips");
        });
    });
});

const img_data =
    "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAKNAAACjQFSfTLjAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAA7VJREFUWIWtl91vVFUUxX976HTaTilTqkXog0CJECBR8VtDBB9UwJBgYoL/gAkhhCcejA/6YkhMjP+AJvbN+BGNiC8mjQ9oCH6g1YANkNYBkqINBVJDKdDFw97j3OLczr0dV3LT03PPPnvtffde5wxkhKS1kt6R9I2kw5J6stq2DEnbJF2W9K2kS5J+ir+HJBVb2buQwXkvMAS8CXQCM0A5bF8FxiUdkGSLIdDUSNJxYClQDOefAd3hvBR73Ijl+83si/+NgKSvgaeBKjAKnADWAw8CfcA/wIoEEQP+BPaY2ZksBJp9gjbg9SAxBKyOpwRcj/fVcHoL6AA2AKcknZH0gqTSQg5SMyDpILAfmE2suwr8ChwDLuCpXwE8AzwFDMa6PvyTzQGnguiIme3NQ6AKDJrZzcTc/cBWYAuwHC/GSrzuiaeE18xSYBp4AM/0eWDAzC6n+bybwISktkyL/2tbkNQt6S1JJyQdlXRW0kCeTUYkbV4MgcQeexLjMUkdeYxPhwYgabuk1yTtzqqAkkqSxmLcJelso3UNUyypAhTMbCqm3gc+B7YBb0tqx4trCP+2Av4CLpqZwuYRoOZ0CzCWmUCgMzGeAj4ws9NBsB14EXgZb712vCgHJM0B+/DW/SXsnwzC2QiY2ZVwUkMlGYGZzQJfxjMPko4Bk8Ba4MeYfhz4JDOBcH4jxsvwdu2U1AnMmdnVFLsuoN/MRiSVgSvxaiUuVtkIAANArV+LeB9/HP+Xoz0rQfJ34Cs82s3AxVj3bxBhP5uHQA8utZjZJLAmon8X+Aj4zsxuR3a2AzuAV/A6qKW6gqtlbXwpD4HbEUESs7gEvwHcJ2km5rsiwmvAYTOr1UU/9bSX8brIRWAeIuJeXN9Hgd/w1qsC34fN9YRJNzApqQAUk5KehcAS/HS7G8N4ih/DD55VeItNmNnJ2qK4nCwxs1uS7sEPsYZII/B8RDUPNR0AjqZtGOignsWtuFA1RNp9YCfwaRMnC6FIveqFq2UuAiuB8RYIrAL+jvHDwA95CZTwql4sngD+iPGzNFDMVAJxZJqZTbdA4CXgSIyXAxOZCeDt0/S63gQl6pW/LBeBUL5yiwSOUK/8VA1oSCCgxf7QCDyKn4DQ5OqfpgMngeOSRoER/FyfAc4BkwtFFJgGChFEqgaksgvDjcC9wHN4W/XiBdUdxNvCfgq/+VwIoj/jOlDFv/+wmW3KRSAPJA3isrwOeAi/fvUB7+GXmH1mtrNVP3lJ7ZI0LOlDSf0Lrb0DD4WYYzdMyVsAAAAASUVORK5CYII=";
