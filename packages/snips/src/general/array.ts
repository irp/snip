import { type } from "arktype";

import {
    BaseSnip,
    BaseSnipArgs,
    BaseViewSchema,
    RenderContext,
    SnipData,
    SnipDataSchema,
} from "./base";

import { DataValidationError } from "@/errors";

/* -------------------------------------------------------------------------- */
/*                              ArraySnip typing                              */
/* -------------------------------------------------------------------------- */

const ArrayDataSchema = type({
    snips: type("object[] > 0").describe("Array of snips"),
    "legacy?": type("boolean")
        .describe("Legacy plotting places vertically below each other")
        .default(false),
});

const ArrayViewSchema = type(
    {
        "snip_order?": type("number[]").describe(
            "Draw order of the snips in the array",
        ),
    },
    "&",
    BaseViewSchema,
).describe(BaseViewSchema.description);

const ArraySnipSchema = type(SnipDataSchema, "&", {
    data: ArrayDataSchema,
    "view?": ArrayViewSchema,
}).describe(
    "Represents an grouping of snippets into one shared array snippet. Mainly useful for complex snippets or reusable templates.",
);

export type ArrayData = typeof ArrayDataSchema.infer;
export type ArrayView = typeof ArrayViewSchema.infer;

export type ArraySnipArgs = BaseSnipArgs &
    Omit<ArrayData, "type"> &
    ArrayView & {
        snips: BaseSnip[];
    };

/* -------------------------------------------------------------------------- */
/*                          ArraySnip Implementation                          */
/* -------------------------------------------------------------------------- */

export class ArraySnip extends BaseSnip {
    static readonly _schema = ArraySnipSchema;
    public type = "array";

    public snips: BaseSnip[];
    public legacy: boolean;
    public snip_order: number[];

    // Indicate if all snip images are loaded
    ready: Promise<boolean>;

    constructor({
        snips,
        legacy = false,
        snip_order,
        ...baseArgs
    }: ArraySnipArgs) {
        super({ ...baseArgs });
        this.snips = snips;
        this.legacy = legacy;

        // Set the snip order
        if (snip_order) {
            this.snip_order = snip_order;
        } else {
            this.snip_order = [...Array(this.snips.length).keys()];
        }

        this.ready = this.checkReady();
    }

    /** Check all snips loaded
     * e.g. images prepared
     */
    async checkReady(): Promise<boolean> {
        const promises: Promise<boolean>[] = [];
        for (const snip of this.snips) {
            if (Object.hasOwn(snip, "ready")) {
                promises.push((snip as ArraySnip).ready);
            }
        }
        await Promise.all(promises);
        return true;
    }

    /** Construct a instance of the snip from a
     * SnipData object. This is used to create a snip
     * object from the database data dict.
     *
     * @param {SnipData<ArrayData, ArrayView>} data - Data dict
     */
    static from_data(
        data: SnipData<ArrayData, ArrayView>,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        get_snip_from_data?: any,
    ): ArraySnip {
        // Validate outer first
        const validation = ArraySnipSchema(data);
        if (validation instanceof type.errors) {
            throw DataValidationError.from_ark(validation);
        }

        // Validate inner snips
        // This might also throw an error!
        const snips = data.data?.snips.map((snip_data, i) => {
            let snip: BaseSnip;
            try {
                snip = get_snip_from_data(snip_data);
            } catch (e) {
                if (e instanceof DataValidationError) {
                    // Adjust path
                    e.path.unshift("data", "snips[" + i + "]");
                    e.message = `data.snips[${i}].${e.message}`;
                }
                throw e;
            }
            return snip;
        });

        return new ArraySnip({
            //Array
            snips,
            legacy: data.data.legacy,
            snip_order: data.view?.snip_order,
            // Base
            x: data.view?.x,
            y: data.view?.y,
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
        });
    }

    /** Render an array snip
     *
     * This also renders the underlying snips these should not
     * be rendered separately!
     *
     * @param {RenderContext} ctx - The canvas context
     */
    render(ctx: RenderContext): void {
        const transform = ctx.getTransform();
        // Draw snips
        ctx.translate(this._x, this._y);
        if (this.legacy) {
            this.snip_order.forEach((index) => {
                this.snips[index]!.render(ctx);
                ctx.translate(0, this.snips[index]!.height);
            });
        } else {
            this.snip_order.forEach((index) => {
                this.snips[index]!.render(ctx);
            });
        }

        // Restore transform
        ctx.setTransform(transform);
    }

    to_data(minimal?: boolean): SnipData<ArrayData, ArrayView> {
        const data = super.to_data(minimal) as SnipData<ArrayData, ArrayView>;
        data.data.snips = [];
        for (const snip of this.snips) {
            data.data.snips.push(snip.to_data(minimal));
        }
        data.data.legacy = this.legacy;

        data.view.snip_order = this.snip_order;
        return data;
    }

    get height(): number {
        if (this.legacy) {
            let height = 0;
            for (const snip of this.snips) {
                height += snip.height + Math.max(snip.y, 0);
            }
            return height;
        }
        let y_min = this._y;
        let y_max = this._y;
        for (const snip of this.snips) {
            y_min = Math.min(y_min, this._y + snip.y);
            y_max = Math.max(y_max, this._y + snip.y + snip.height);
        }
        return Math.abs(y_max - y_min);
    }
    get width(): number {
        let x_min = this._x;
        let x_max = 0;
        for (const snip of this.snips) {
            x_min = Math.min(x_min, this._x + snip.x);
            x_max = Math.max(x_max, this._x + snip.x + snip.width);
        }
        return x_max - x_min;
    }

    /** Returns the offset of the snip if the plotting type is legacy
     *
     */
    offset_snip(snip_id: number | null): number {
        if (this.legacy && snip_id !== null) {
            let offset = 0;
            for (const i of this.snip_order) {
                if (this.snips[i]!.id === snip_id) {
                    return offset;
                }
                offset += this.snips[this.snip_order[i]!]!.height;
            }
        }
        return 0;
    }

    /** Setter for page id
     * allows for nested snips to be placed on
     * page.
     */
    public set page_id(value: number | undefined) {
        this._page_id = value;
        for (const snip of this.snips) {
            snip.page_id = value;
        }
    }

    public get page_id(): number | undefined {
        return this._page_id;
    }

    public async place_on_page(page_id: number): Promise<void> {
        this.page_id = page_id;

        for (const snip of this.snips) {
            snip.socket = this.socket;
            await snip.upsert();
        }
        await super.place_on_page(page_id);
    }
}
