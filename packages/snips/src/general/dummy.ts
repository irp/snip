import { BaseSnip } from "./base";

export class DummySnip extends BaseSnip {
    public type = "dummy";
    width: number = 0;
    height: number = 0;

    render(): void {
        // Do nothing
    }
}
