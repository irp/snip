import { type } from "arktype";
import { sync as probe_sync } from "probe-image-size";

import {
    BaseData,
    BaseSnip,
    BaseSnipArgs,
    BaseView,
    BaseViewSchema,
    DateSchema,
    IdSchema,
    RenderContext,
    SnipData,
    SnipDataSchema,
} from "./base";
import { ImageSnipLegacy } from "./legacy/image";
import { PlaceholderSnip } from "./placeholder";

import { DataValidationError } from "@//errors";

/* -------------------------------------------------------------------------- */
/*                              ImageSnip typing                              */
/* -------------------------------------------------------------------------- */
const BlobSchema = type({
    "data?": "unknown",
    mime: "string",
    size: "number > 0",

    "id?": IdSchema,
    "hash?": "string",
    "created?": DateSchema,
    "last_updated?": DateSchema,
}).describe("Represents an image blob i.e. the image data");

const ImageDataSchema = type({
    blob: BlobSchema,
    "version?": type("number.integer >= 0").describe(
        "The version of the image snippet, empty for legacy version.",
    ),
}).describe("The data of the image snippet.");

const ImageViewSchema = type(
    {
        "sx?": type("number").describe(
            "The x-axis coordinate of the top left corner of the sub-rectangle of the source image to draw into the destination context.",
        ),
        "sy?": type("number").describe(
            "The y-axis coordinate of the top left corner of the sub-rectangle of the source image to draw into the destination context.",
        ),
        "swidth?": type("number").describe(
            "The width of the sub-rectangle of the source image to draw into the destination context.",
        ),
        "sheight?": type("number").describe(
            "The height of the sub-rectangle of the source image to draw into the destination context.",
        ),
        "width?": type("number").describe(
            "The desired width of the snip in pixels. Defaults to the image width or page width if image width is larger.",
        ),
        "height?": type("number").describe(
            "The desired height of the snip in pixels. Defaults to the image height or scaled by page width if image width is larger.",
        ),
    },
    "&",
    BaseViewSchema,
).describe(BaseViewSchema.description);

const ImageSnipSchema = type(SnipDataSchema, "&", {
    data: ImageDataSchema,
    "view?": ImageViewSchema,
}).describe("Represents an image with options for cropping and scaling.");

export type ImageData = typeof ImageDataSchema.infer;
export type ImageView = typeof ImageViewSchema.infer;
export type BlobData = typeof BlobSchema.infer;
export type ImageSnipArgs = ImageView &
    Omit<ImageData, "version"> &
    BaseSnipArgs;

/* -------------------------------------------------------------------------- */
/*                          ImageSnip implementation                          */
/* -------------------------------------------------------------------------- */

export class ImageSnip extends BaseSnip {
    static _schema = ImageSnipSchema;
    public type = "image";

    public version: number = 2;
    public blob: BlobData;

    public bitmap?: CanvasImageSource;

    // Image dimensions in pixels (probed from the image (blob))
    public img_width: number;
    public img_height: number;

    // Size of the sub-rectangle of the source image
    // i.e. the cropped image
    public sx: number;
    public sy: number;
    public swidth: number;
    public sheight: number;

    // Size of the image in the destination context
    // i.e. the rendered image on the canvas
    // base: x,y
    public dwidth: number;
    public dheight: number;

    // Indicate if bitmap was loaded
    public ready: Promise<boolean>;

    // Overlay mode
    public overlay: boolean = false;

    constructor({
        blob,
        sx,
        sy,
        swidth,
        sheight,
        width,
        height,
        ...baseArgs
    }: ImageSnipArgs) {
        super({ ...baseArgs });

        // Set default values
        this.blob = blob;

        // Probe image dimensions

        const prob_res = probe_sync(Buffer.from(blob.data as string, "base64"));

        if (!prob_res) {
            throw new DataValidationError(
                "data.blob.data is not a valid image data (base64)",
                ["data", "blob", "data"],
            );
        }

        this.img_width = prob_res.width;
        this.img_height = prob_res.height;

        // If crop is not defined
        // we use the full image
        this.sx = sx || 0;
        this.sy = sy || 0;
        this.swidth = swidth || this.img_width;
        this.sheight = sheight || this.img_height;

        // Width and height on the canvas
        this.dwidth = width || Math.min(this.img_width, 1400);
        this.dheight =
            height || this.dwidth * (this.img_height / this.img_width);

        // Convert string to base64
        this.ready = this.get_bitmap();
    }

    public get_bitmap() {
        return fetch(`data:${this.blob.mime};base64,${this.blob.data}`)
            .then((response) => response.blob())
            .then((blob) => {
                try {
                    return createImageBitmap(blob);
                } catch (_e) {
                    throw new Error("No ImageBitmap support");
                }
            })
            .then((bitmap) => {
                this.bitmap = bitmap;
                return true;
            })
            .catch((_e) => {
                return false;
            });
    }

    /** Construct a instance of the snip from a
     * SnipData object. This is used to create a snip
     * object from the database data dict.
     *
     * @param {SnipData<ImageData, ImageView>} data - Data dict
     */
    static from_data(
        data: SnipData<ImageData, ImageView>,
    ): ImageSnip | ImageSnipLegacy {
        const validation = ImageSnipSchema(data);
        if (validation instanceof type.errors) {
            throw DataValidationError.from_ark(validation);
        }

        // TODO: migrate legacy data
        if (!validation.data.version) {
            return ImageSnipLegacy.from_data(data);
        }

        return new ImageSnip({
            // Image
            blob: validation.data.blob,
            sx: validation.view?.sx,
            sy: validation.view?.sy,
            swidth: validation.view?.swidth,
            sheight: validation.view?.sheight,
            width: validation.view?.width,
            height: validation.view?.height,
            //Base
            x: validation.view?.x,
            y: validation.view?.y,
            rot: validation.view?.rot,
            mirror: validation.view?.mirror,
            id: validation.id,
            page_id: validation.page_id,
            book_id: validation.book_id,
            last_updated: validation.last_updated,
            created: validation.created,
        });
    }

    static from_legacy(data: ImageSnipLegacy): ImageSnip {
        // Parse rot string "90m" -> 90 true

        return new ImageSnip({
            // Image
            blob: data.blob as BlobData,
            sx: data.sx,
            sy: data.sy,
            swidth: data.swidth,
            sheight: data.sheight,
            width: data.width,
            height: data.height,
            //Base
            x: data.x,
            y: data.y,
            rot: data.rot,
            mirror: data.mirror,
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
        });
    }

    /** Renders the simple text to the canvas function
     *
     * @param {RenderContext} ctx - Canvas context
     */
    render(ctx: RenderContext): void {
        if (!this.bitmap) {
            console.error("No bitmap loaded!");
            return;
        }

        // Apply translate, rotate and mirror
        // see base.ts
        const transform = ctx.getTransform();
        this.transform(ctx);

        // Draw the image in the right scale
        const scale_x = this.width / this.img_width;
        const scale_y = this.height / this.img_height;
        ctx.scale(scale_x, scale_y);
        ctx.drawImage(
            this.bitmap,
            // In image coordinates
            this.sx,
            this.sy,
            this.swidth,
            this.sheight,
            // In canvas coordinates
            this.sx,
            this.sy,
            this.swidth,
            this.sheight,
        );

        // If we are in overlay mode we want to draw the full image too
        if (this.overlay) {
            ctx.globalAlpha = 0.1;
            ctx.scale(1 / scale_x, 1 / scale_y);
            ctx.drawImage(this.bitmap, 0, 0, this.width, this.height);
            ctx.globalAlpha = 1;
        }
        ctx.setTransform(transform);
    }

    /** Create a dictionary in data standard format
     *
     * @returns {Dictionary}
     */
    data(): ImageData {
        return {
            blob: this.blob,
            version: this.version,
        };
    }
    view(): ImageView {
        const view = super.view() as ImageView;
        view.sx = this.sx;
        view.sy = this.sy;
        view.swidth = this.swidth;
        view.sheight = this.sheight;
        view.width = this.width;
        view.height = this.height;
        return view;
    }

    /** Overload data to allow interaction events */
    public to_data(minimal?: boolean): SnipData<BaseData, BaseView> {
        if (minimal) {
            return PlaceholderSnip.from_snip(this).to_data();
        } else {
            // for some reason the super to_data function does not apply
            // the local data view versions of the img class
            const data = super.to_data();
            data.data = this.data();
            data.view = this.view();
            return data;
        }
    }

    get width(): number {
        return this.dwidth;
    }
    set width(width: number) {
        this.dwidth = width;
    }
    get height(): number {
        return this.dheight;
    }
    set height(height: number) {
        this.dheight = height;
    }

    public static get jsonSchema() {
        if (!this.schema) {
            return undefined;
        }

        let schema = this.schema.merge({
            "created?": "string.date.iso.parse",
            "last_updated?": "string.date.iso.parse",
        });
        const BlobSchemaDate = BlobSchema.merge({
            "created?": "string.date.iso.parse",
            "last_updated?": "string.date.iso.parse",
        });

        // We also need to parse the blob data
        schema = schema.merge({
            data: ImageDataSchema.merge({
                blob: BlobSchemaDate,
            }),
        });

        return schema.in.toJsonSchema();
    }
}
