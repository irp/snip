import { Type, type } from "arktype";

import type { Socket } from "../socket_types";

import { DataValidationError } from "@/errors";

export type RenderContext =
    | CanvasRenderingContext2D
    | OffscreenCanvasRenderingContext2D;

/* -------------------------------------------------------------------------- */
/*                               BaseSnip typing                              */
/* -------------------------------------------------------------------------- */

export const IdSchema = type("number.integer").describe(
    "Unique identifier, should normally not be pre set!",
);
export const DateSchema = type("string.date.iso.parse | Date").describe(
    "Date should be in in ISO8601 format.",
);

export const BaseViewSchema = type({
    "x?": type("number").describe(
        `The vertical position of the snippet. This may be adjusted until the snippet is placed on a page. Coordinates are relative to the top left corner of the page.`,
    ),
    "y?": type("number").describe(
        `The horizontal position of the snippet. This may be adjusted until the snippet is placed on a page. Coordinates are relative to the top left corner of the page.`,
    ),

    "rot?": type("-360 <= number <= 360")
        .describe(
            `The rotation of the snippet in degrees. Positive values rotate the snippet clockwise. The rotation origin is the 
            center of the snippet.`,
        )
        .default(0),
    "mirror?": type("boolean")
        .describe(
            `If true the snippet is mirrored on the x-axis. This is applied after the rotation.`,
        )
        .default(false),
}).describe(
    `The visual representation of the snippet. This may be adjusted until the snippet is placed on a page and is not required to be set on creation.`,
);

export const SnipDataSchema = type({
    type: type("string").describe("Type of the snippet"),

    book_id: IdSchema.describe(
        "Reference to the book to which the snip belongs. Once set can't be changed!",
    ),

    "id?": IdSchema.describe(
        `The Unique identifier of the snippet in the database. Is set during the first
        insert and does not changed afterwards. ${IdSchema.description}`,
    ),

    "page_id?": type("number.integer | null").describe(
        "The page id on which the snip is placed. If not set the snip is not placed on any page. You can't upload a snip with a page_id set! This has to be done via the UI!",
    ),

    "created?": DateSchema.describe(
        `The date when the snippet was created. This is set during the first insert of the snippet and can't be changed after afterwards. ${DateSchema.description}`,
    ).or(type("null")),

    "last_updated?": DateSchema.describe(
        `The date when the snippet was last updated. This is set during every change of the snippet via a db trigger. Manual changes are not reflected in the database. ${DateSchema.description}`,
    ).or(type("null")),

    "created_by?": type("number.integer | null").describe(
        `The user id of the user who created the snippet. This is set during the first insert of the snippet and can't be changed after afterwards. ${IdSchema.description}`,
    ),
});
type _SnipData = typeof SnipDataSchema.infer;

export const BaseSnipSchema = type(SnipDataSchema, "&", {
    "view?": BaseViewSchema,
});

export interface BaseData {}
export type BaseView = typeof BaseViewSchema.infer;
export type SnipData<B extends BaseData, V extends BaseView> = _SnipData & {
    data: B;
    view: V;
};

// Args is flattened
export type BaseSnipArgs = Omit<_SnipData, "type"> &
    BaseView & {
        /** Socket
         * This is the socket that the snip is associated with. Can be parsed if
         * the socket is already created.
         */
        socket?: Socket;
    };

/* -------------------------------------------------------------------------- */
/*                           BaseSnip Implementation                          */
/* -------------------------------------------------------------------------- */

/** This class is an abstract base class and should be extended by
 *  other snip classes via inheritance. It implements some features
 *  that every snip shares.
 */
export abstract class BaseSnip {
    static description = "Base Snip";

    public socket: Socket | undefined;

    // Parents have to set type string
    abstract type: string;

    /** Variables
     * see getters/setters below for protected
     */
    protected _x: number;
    protected _y: number;
    protected _rot: number;
    protected _mirror: boolean;

    public id: number;
    protected _page_id: number | undefined;
    public readonly book_id: number;

    public readonly created: Date;
    public last_updated: Date;

    // Ready state in case the snip needs to load
    public ready?: Promise<boolean>;

    // Overlay draw mode
    public overlay: boolean = false;

    constructor({
        x = 0,
        y = 0,
        rot = 0,
        mirror = false,
        id,
        page_id,
        book_id,
        last_updated = new Date(),
        created = new Date(),
        socket,
    }: BaseSnipArgs) {
        // Parse default values
        this._x = x;
        this._y = y;
        this._rot = rot;
        this._mirror = mirror;

        // Negative id means that the snip is not saved in the db
        this.id = id || -1 * Math.floor(Math.random() * 100000);

        this._page_id = page_id || undefined;
        this.book_id = book_id;
        this.last_updated = last_updated ? new Date(last_updated) : new Date();
        this.created = created ? new Date(created) : new Date();
        this.socket = socket;
    }

    /** Construct a instance of the snip from a
     * SnipData object. This is used to create a snip
     * object from the database data dict.
     *
     * @param {SnipData<BaseData, BaseView>} _data - Data dict
     */
    static from_data(
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        _data: SnipData<BaseData, BaseView>,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars, @typescript-eslint/no-explicit-any
        _get_snip_from_data?: any,
    ): BaseSnip {
        // Date validation
        // TODO find a better way to validate dates
        const validation = BaseSnipSchema(_data);

        if (validation instanceof type.errors) {
            throw DataValidationError.from_ark(validation);
        }

        // @ts-expect-error this in abstract class
        return new this({
            x: validation.view?.x,
            y: validation.view?.y,
            rot: validation.view?.rot,
            mirror: validation.view?.mirror,
            id: validation.id,
            page_id: validation.page_id,
            book_id: validation.book_id,
            last_updated: validation.last_updated,
            created: validation.created,
        });
    }

    /** Render function, should be used by parent classes
     * This is the most important function of any snip class
     * as it is used to render the snip on the page.
     *
     * The base snip class does not have much rendering functionality
     * so it is left to the child classes to implement this.
     *
     * This class only implements the basic bounding box plotting.
     *
     * @param {RenderContext} ctx - Canvas context
     */
    public abstract render(ctx: RenderContext): void;

    /** Apply the placement transformation to the snip
     * This is used to move the snip to the correct position
     * can also be used to rotate the snip.
     *
     * @param ctx
     */
    public transform(ctx: RenderContext): void {
        const w2 = this.width >> 1;
        const h2 = this.height >> 1;

        ctx.translate(this._x, this._y);
        ctx.translate(w2, h2);
        ctx.rotate((this._rot * Math.PI) / 180);
        if (this._mirror) {
            ctx.scale(-1, 1);
        }
        ctx.translate(-w2, -h2);
    }

    /** Get SnipData object from Snip, this should be used
     * to send the snip to other clients. I.e. it is the
     * serialization function.
     *
     * @param minimal Further remove binary files, this can be used for interaction
     * events.
     */
    public to_data(_minimal?: boolean): SnipData<BaseData, BaseView> {
        const ret: SnipData<BaseData, BaseView> = {
            type: this.type,
            book_id: this.book_id,
            id: this.id,
            data: this.data(),
            view: this.view(),
            created: this.created,
            last_updated: this.last_updated,
        };

        if (this._page_id !== undefined) {
            ret.page_id = this._page_id;
        }

        return ret;
    }

    /** Corresponds to data object in the database */
    protected data(): BaseData {
        return {};
    }
    /* Corresponds to view object in the database */
    protected view(): BaseView {
        const ret: BaseView = {
            x: this._x,
            y: this._y,
        };
        if (this._rot !== 0) {
            ret.rot = this._rot;
        }
        if (this._mirror) {
            ret.mirror = this._mirror;
        }

        return ret;
    }

    /** All snips have to implement
     * an height and width property
     */
    abstract height: number;
    abstract width: number;

    /** Returns polygon of snip
     *
     * might be overwritten by child classes
     * and can be used to check if a point is inside
     * the snip.
     *
     * I.e. for hitbox detection.
     *
     */
    get polygon(): PolygonI {
        return new Polygon([
            [this.x, this.y],
            [this.x + this.width, this.y],
            [this.x + this.width, this.y + this.height],
            [this.x, this.y + this.height],
        ]);
    }

    /**
     * Upserts the snip data to the database.
     *
     * This method sends the snip data to the server for either insertion or update.
     * If the snip already exists in the database, it will be updated; otherwise,
     * it will be inserted. Matching is done based on the snip ID.
     *
     * @param timeout - The maximum time to wait for a response from the server, in milliseconds. Defaults to 3000ms.
     * @returns A promise that resolves with the ID of the snip in the database.
     * @throws An error if the server response indicates a failure or if the operation times out.
     */
    public async upsert(timeout: number = 3000): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            if (this.socket === undefined) {
                return reject(new Error("Socket is not defined!"));
            }
            const timeoutT = setTimeout(() => {
                reject(new Error(`Failed to upsert snip! Timeout exceeded!`));
            }, timeout);

            this.socket.emit("snip:upsert", this.to_data(), (response) => {
                clearTimeout(timeoutT);
                if (response.ok === false) {
                    return reject(new Error(response.error));
                }
                this.id = response.data;
                resolve(response.data);
            });
        });
    }

    /** Remove a snip from the database, this should normally not be done
     * as it will remove the snip from all pages and books. It might also
     * be not possible to do this depending on the users permissions.
     */
    public async remove(
        nested = true,
        timeout: number = 3000,
    ): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            if (this.socket === undefined) {
                return reject(new Error("Socket is not defined!"));
            }
            const timeoutT = setTimeout(() => {
                reject(new Error(`Failed to remove snip!  Timeout exceeded!`));
            }, timeout);

            this.socket.emit("snip:remove", this.id, nested, (response) => {
                clearTimeout(timeoutT);
                if (response.ok === false) {
                    return reject(new Error(response.error));
                } else {
                    resolve(true);
                }
            });
        });
    }

    /** Place the snip on the page
     * This method can be used to place the snip on a given page of
     * previously no page was set.
     *
     * It updates the page_id and emits a socket event to the server
     */
    public async place_on_page(page_id: number): Promise<void> {
        this._page_id = page_id;
        this.last_updated = new Date();

        await this.upsert();
    }

    /** If a snip has a preview state, e.g. while drawing but not placed yet.
     * The snip can be send to other clients with this method.
     */
    public send_preview(): void {
        this.socket?.volatile.emit("snip:preview", this.to_data(true));
    }

    /** Handler for interactions
     *
     * If handlers are defined these snips are interactive, the can be
     * clicked on and react to mouse events. This is very basic and
     * should be implemented by child classes.
     *
     * At the moment the following events are supported:
     * - on_pointerdown
     *  This is called when the snip is clicked on
     * - on_dblclick
     *  This is called when the snip is double clicked on
     * - on_pointerenter / on_pointerleave
     *  This is called when the mouse first enters the snip and when it leaves
     *
     * The base snip is not interactive and does not implement any
     * of these handlers as if they are defined an additional
     * transfer of the snip data is required. Which might impact
     * performance.
     *
     * The most complex interaction is implemented by the
     * textSnip which allows to copy text from the snip.
     *
     */
    public on_pointerdown?: InteractionFunction;
    public on_dblclick?: InteractionFunction;
    public on_pointerenter({ setActiveSnip }: EventContext) {
        // We dont wont to import react that's why we
        // use this approach to create a overlay
        // in the tool component.
        // See interaction.tool.tsx
        setActiveSnip(this);
        return;
    }
    public on_pointerleave = ({ setActiveSnip }: EventContext) => {
        setActiveSnip(undefined);
        return;
    };

    /** Further if they are subfixed with the "worker" tag. The event
     * is called in the worker thread and not in the main thread. This is
     * useful if you for instance want to change the drawing of the snip on
     * hover. After the event is called in the worker thread the page
     * is rerendered and the snip is redrawn.
     *
     */
    public on_pointerdown_worker?(ctx: RenderContext): void;
    public on_pointerenter_worker?(ctx: RenderContext): void;
    public on_pointerleave_worker?(ctx: RenderContext): void;
    public on_dblclick_worker?(ctx: RenderContext): void;

    /** Page id can be updated to e.g. place a snip on a page
     */
    get page_id(): number | undefined {
        return this._page_id;
    }
    set page_id(value: number | undefined) {
        this._page_id = value;
    }

    /** x coordinate of the snip
     */
    get x(): number {
        return this._x;
    }
    set x(value: number) {
        this._x = value;
    }
    get y(): number {
        return this._y;
    }
    set y(value: number) {
        this._y = value;
    }
    set rot(value: number) {
        this._rot = value;
    }
    get rot(): number {
        return this._rot;
    }
    set mirror(value: boolean) {
        this._mirror = value;
    }
    get mirror(): boolean {
        return this._mirror;
    }

    /** Needed for server side overrides
     *  i.e. in the images service we override
     * this with a nodejs canvas implementation.
     */
    createCanvas(width: number, height: number) {
        return new OffscreenCanvas(width, height);
    }

    /** You may add a schema to the snip if you have created
     * a custom snip. This is used to validate the snip data
     * before it is send to the server.
     */
    protected static readonly _schema: Type<object> = BaseSnipSchema;
    public static get schema() {
        // try to access the schema from only the current class
        if (Object.getOwnPropertyNames(this).includes("_schema")) {
            return this._schema;
        } else {
            return undefined;
        }
    }
    public static get jsonSchema() {
        if (!this.schema) {
            return undefined;
        }

        const schema = this.schema.merge({
            "created?": "string.date.iso.parse",
            "last_updated?": "string.date.iso.parse",
        });
        return schema.in.toJsonSchema();
    }
}

export interface EventContext {
    canvas: HTMLCanvasElement;
    container: HTMLDivElement;
    setActiveSnip: (snip?: BaseSnip) => void;
    page_idx: number;
}

export type InteractionFunction = (event: EventContext) => void;
export type Point = [number, number];

export abstract class PolygonI {
    protected _isInside(polygon: Array<Point>, point: Point) {
        let inside = false;
        const [x, y] = point;
        const num_vertices = polygon.length;

        let [x1, y1]: Point = polygon.at(0)!;
        let [x2, y2]: Point = [0, 0];

        for (let i = 1; i <= num_vertices; i++) {
            [x2, y2] = polygon.at(i % num_vertices)!;

            if (y > Math.min(y1, y2)) {
                if (y <= Math.max(y1, y2)) {
                    if (x <= Math.max(x1, x2)) {
                        const x_intersection =
                            ((y - y1) * (x2 - x1)) / (y2 - y1) + x1;

                        if (x1 === x2 || x <= x_intersection) {
                            inside = !inside;
                        }
                    }
                }
            }

            [x1, y1] = [x2, y2];
        }
        return inside;
    }
    abstract isInside(point: Point): boolean;
}

export class Polygon extends PolygonI {
    polygon: Array<Point>;

    constructor(points: Array<Point>) {
        super();
        this.polygon = points;
    }
    isInside(p: Point): boolean {
        return super._isInside(this.polygon, p);
    }
}

export class MultiPolygon extends PolygonI {
    polygons: Array<PolygonI>;

    constructor(polygons: Array<PolygonI>) {
        super();
        this.polygons = polygons;
    }

    isInside(point: Point): boolean {
        for (const polygon of this.polygons) {
            if (polygon.isInside(point)) {
                return true;
            }
        }
        return false;
    }
}
