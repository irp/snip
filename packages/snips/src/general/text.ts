import { type } from "arktype";

import { isBrowser, isWorker } from "@snip/common/environment";

import {
    BaseSnip,
    BaseSnipArgs,
    BaseViewSchema,
    RenderContext,
    SnipData,
    SnipDataSchema,
} from "./base";
import { TextSnipLegacy } from "./legacy/text";

import { DataValidationError } from "@/errors";
import { AllowedFonts, FONT_URLS } from "@/fonts/internal";

/* -------------------------------------------------------------------------- */
/*                               TextSnip typing                              */
/* -------------------------------------------------------------------------- */
const allowedFont = type(`'${FONT_URLS.map((f) => f[1]).join("'|'")}'`)
    .describe("The font family to use for the text.")
    .default("Arial");

export const TextDataSchema = type({
    text: type("string").describe("The text to display"),
    "version?": type("number.integer >= 0").describe(
        "The version of the text snippet, leave empty for latest version.",
    ),
}).describe("The data of the text snippet");

export const TextViewSchema = type(
    {
        "size?": type("number > 0")
            .describe("The font size of the text in pixel")
            .default(12),

        /** The distance between the baseline of
         * two lines of text
         */
        "lheight?": type("number > 0").describe(
            "The line height of the text in pixels. Defaults to 1.25 * size",
        ),

        "font?": allowedFont,
        "wrap?": type("number")
            .describe("The linewrap of the text in pixels")
            .default(400),

        "colour?": type("string")
            .describe("The colour of the text")
            .default("black"),

        "baseline?": type(
            "'alphabetic' | 'bottom' | 'hanging' | 'ideographic' | 'middle' | 'top'",
        )
            .describe("The baseline of the text")
            .default("middle"),
    },
    "&",
    BaseViewSchema,
).describe(BaseViewSchema.description);

export const TextSnipSchema = type(SnipDataSchema, "&", {
    data: TextDataSchema,
    "view?": TextViewSchema,
}).describe(
    "Represents some text on the page, allows to define different fonts, sizes and colours.",
);

export type TextData = typeof TextDataSchema.infer;
export type TextView = typeof TextViewSchema.infer;

//rename some of the properties
type t1 = RenameByT<
    {
        font: "fontFamily";
        size: "fontSize";
        lheight: "lineHeight";
        wrap: "lineWrap";
    },
    TextView
>;

export type TextSnipArgs = t1 & Omit<TextData, "type"> & BaseSnipArgs;

/* -------------------------------------------------------------------------- */
/*                          TextSnip implementation                          */
/* -------------------------------------------------------------------------- */

/**
 * Represents a font used in a canvas.
 */
class CanvasFont {
    fontStyle: string;
    fontWeight: string;
    fontFamily: AllowedFonts;
    fontSize: number; //in px
    lineHeight: number; //relative

    /**
     * Creates a new instance of the CanvasFont class.
     * @param options - The font options.
     */
    constructor({
        fontStyle = "normal",
        fontWeight = "normal",
        fontSize = 12,
        lineHeight = 1.25,
        fontFamily = "Arial",
    }: {
        fontStyle?: string;
        fontWeight?: string;
        fontSize?: number;
        lineHeight?: number;
        fontFamily: AllowedFonts;
    }) {
        this.fontStyle = fontStyle;
        this.fontWeight = fontWeight;
        // Im not sure anymore where the 2.35 comes from but it is needed
        this.fontSize = fontSize * 2.35;
        this.lineHeight = lineHeight;
        this.fontFamily = fontFamily;
    }

    /**
     * Gets the font string representation.
     * @returns The font string.
     */
    get fontStr(): string {
        return `${this.fontStyle} ${this.fontWeight} ${this.fontSize}px ${this.fontFamily}`;
    }
}

export class TextSnip extends BaseSnip {
    protected static _schema = TextSnipSchema;
    public type = "text";

    public canvasFont: CanvasFont;
    public baseline: CanvasTextBaseline;
    public colour: string;

    public version: number;

    constructor({
        text = "",
        fontFamily = "PlexMono",
        fontSize = 12,
        colour = "black",
        lineHeight = undefined,
        lineWrap = undefined,
        baseline = "middle",
        ...baseArgs
    }: TextSnipArgs) {
        super({ ...baseArgs });

        // Hardcoded version number
        this.version = 4;

        // Set font
        this.canvasFont = new CanvasFont({
            fontSize,
            fontFamily: fontFamily as AllowedFonts,
            lineHeight,
        });

        // Set colour
        this.colour = colour;

        this._lineWrap = lineWrap || 400;
        this._raw_text = text;
        // text is formatted (wrapped) on first access of .text

        // Set baseline (should be middle from 1.6 onwards)
        this.baseline = baseline;
    }

    /** Renders the simple text to the canvas function
     *
     * @param {RenderContext} ctx - Canvas context
     */
    render(ctx: RenderContext): void {
        // Set font and style
        // 2480/1400  (TODO put into database update script)
        ctx.font = this.canvasFont.fontStr;
        ctx.fillStyle = this.colour;
        ctx.textAlign = "left";
        ctx.textBaseline = this.baseline;

        // Split text in lines
        // For now only newline is supported
        // linewrap has to be done manually
        const line = this.text.split("\\n");

        // Apply translate, rotate and mirror
        // Base transform in transform function
        // see base.ts
        const transform = ctx.getTransform();
        this.transform(ctx);

        let y = 0;
        if (this.baseline === "middle") {
            // offset to keep the text inside the snip
            // x and y coords
            y += (this.canvasFont.lineHeight * this.canvasFont.fontSize) / 2;
        }

        // Draw text
        line.forEach((text: string) => {
            ctx.fillText(text, 0, y);
            y += this.canvasFont.lineHeight * this.canvasFont.fontSize;
        });

        ctx.setTransform(transform);
    }

    /** Construct a instance of the snip from a
     * SnipData object. This is used to create a snip
     * object from the database data dict.
     *
     * @param {SnipData<TextData, TextView>} data - Data dict
     */
    static from_data(
        data: SnipData<TextData, TextView>,
    ): TextSnip | TextSnipLegacy {
        const validation = TextSnipSchema(data);
        if (validation instanceof type.errors) {
            throw DataValidationError.from_ark(validation);
        }

        if (validation.data.version && validation.data.version < 3) {
            return new TextSnipLegacy({
                // Text
                text: validation.data.text,
                fontFamily: validation.view?.font as AllowedFonts,
                fontSize: validation.view?.size,
                colour: validation.view?.colour,
                lineHeight: validation.view?.lheight,
                lineWrap: validation.view?.wrap,
                version: validation.data.version,
                // baseline ignored in earlier versions

                //Base
                id: validation.id,
                page_id: validation.page_id,
                book_id: validation.book_id,
                last_updated: validation.last_updated,
                created: validation.created,
                x: validation.view?.x,
                y: validation.view?.y,
                rot: validation.view?.rot,
                mirror: validation.view?.mirror,
            });
        }

        return new TextSnip({
            // Text
            text: validation.data.text,
            version: validation.data.version,

            fontFamily: validation.view?.font as AllowedFonts,
            fontSize: validation.view?.size,
            colour: validation.view?.colour,
            lineHeight: validation.view?.lheight,
            lineWrap: validation.view?.wrap,
            baseline: validation.view?.baseline,
            //Base
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
            x: data.view?.x,
            y: data.view?.y,
            rot: data.view?.rot,
            mirror: data.view?.mirror,
        });
    }

    /** Create a json string for mysql from a text snip
     *
     * @returns {Dictionary}
     */
    data(): TextData {
        return {
            text: this._raw_text,
            version: this.version,
        };
    }
    view(): TextView {
        const view = super.view() as TextView;
        view.font = this.canvasFont.fontFamily;
        view.size = this.canvasFont.fontSize / 2.35;
        view.lheight = this.canvasFont.lineHeight;
        view.colour = this.colour;
        view.wrap = this.lineWrap;
        view.baseline = this.baseline;
        return view;
    }

    public getNumLines(): number {
        return this.text.split("\\n").length;
    }

    get height(): number {
        return (
            this.canvasFont.lineHeight *
            this.canvasFont.fontSize *
            this.getNumLines()
        );
    }
    get width(): number {
        const canvas = this.createCanvas(1, 1);
        const ctx = canvas.getContext(
            "2d",
        ) as OffscreenCanvasRenderingContext2D;
        ctx.font = this.canvasFont.fontStr;
        let maxWidth = 0;
        const lines = this.text.split("\\n");
        for (let i = 0; i < lines.length; i++) {
            maxWidth = Math.max(maxWidth, ctx.measureText(lines[i]!).width);
        }
        return maxWidth;
    }

    private _lineWrap: number;
    get lineWrap(): number {
        return this._lineWrap;
    }
    set lineWrap(lineWrap: number) {
        this._lineWrap = lineWrap;
        if (this.version < 4) {
            this._text = this.formatText_v3(this._raw_text);
        } else {
            this._text = this.formatText(this._raw_text);
        }
    }

    private _text: string | undefined;
    private _raw_text: string;
    get text(): string {
        if (!this._text) {
            this._text = this.formatText(this._raw_text);
        }
        return this._text;
    }
    set text(text: string) {
        this._raw_text = text;
        if (this.version < 4) {
            this._text = this.formatText_v3(text);
        } else {
            this._text = this.formatText(this._raw_text);
        }
    }

    private formatText(inputString: string): string {
        // Replace tabs with 8 spaces
        inputString = inputString.replaceAll(/\t/g, "        ");
        // Replace newlines with \n
        inputString = inputString.replaceAll(/\n/g, "\\n");

        // split text in (presplit)lines
        const lines = inputString.split("\\n");

        // Measure text width for each line and get
        // wrap points
        const font = this.canvasFont.fontStr;
        const output: string[] = [];
        for (let i = 0; i < lines.length; i++) {
            const subsStrings = this.splitStringAtWrap(
                lines[i]!,
                this.lineWrap,
                font,
            );
            output.push(subsStrings.join("\\n"));
        }
        return output.join("\\n");
    }

    /**
     * Splits a string into segments based on a maximum width constraint.
     *
     * This method measures the width of the string as it would be rendered
     * using the specified font. It then splits the string into segments where
     * each segment's width does not exceed the given maximum width.
     *
     * @param str - The input string to be split.
     * @param maxWidth - The maximum width in pixels for each segment.
     * @param font - The font string used for measuring the text width.
     * @returns An array of string segments, each fitting within the specified width.
     */
    splitStringAtWrap(str: string, maxWidth: number, font: string): string[] {
        if (maxWidth == -1) {
            return [str];
        }
        const canvas = this.createCanvas(1, 1);
        const ctx = canvas.getContext(
            "2d",
        ) as OffscreenCanvasRenderingContext2D;
        ctx.font = font;

        let width = 0;
        const splits = [];
        for (let c = 0; c < str.length; c++) {
            const char = str.charAt(c);
            const cWidth = ctx.measureText(char).width;
            width += cWidth;
            if (width >= maxWidth) {
                splits.push(c);
                width = cWidth;
            }
        }

        // split into segments
        const segments: string[] = [];
        let start = 0;

        for (const split of splits) {
            segments.push(str.slice(start, split));
            start = split;
        }

        // Add the last segment if there's any remaining part of the string
        if (start < str.length) {
            segments.push(str.slice(start));
        }
        return segments;
    }

    /** Formats the text to a usable format
     * - Replaces tabs with 8 spaces
     * - Replaces newlines with \n
     * - Calculate width and further split text if
     *  lineWrap is set and text width is too long
     *  @deprecated
     */
    formatText_v3(inputString: string): string {
        // Replace tabs with 8 spaces
        inputString = inputString.replaceAll(/\t/g, "        ");
        // Replace newlines with \n
        inputString = inputString.replaceAll(/\n/g, "\\n");

        // split text in (presplit)lines
        const lines = inputString.split("\\n");

        // Measure text width
        const font = this.canvasFont.fontStr;
        for (let i = 0; i < lines.length; i++) {
            const o = this.get_overlap_index_v3(lines[i]!, this.lineWrap, font);
            if (o !== undefined && o > 0) {
                // Split line and add to next line
                if (lines[i + 1] === undefined) lines[i + 1] = "";
                lines[i + 1] = lines[i]!.substring(o) + lines[i + 1];
                lines[i] = lines[i]!.substring(0, o);
            }
            if (o == 0) {
                // each character on a new line
                lines[i] = lines[i]!.split("").join("\\n");
            }
        }

        // Join lines
        return lines.join("\\n");
    }

    /** Returns index of character which
     * overlaps the lineWrap limit.
     *
     * Returns undefined if no character overlaps
     * @deprecated
     */
    get_overlap_index_v3(str: string, maxWidth: number, font: string) {
        if (maxWidth == -1) {
            return undefined;
        }
        if (!isBrowser() && !isWorker()) {
            return undefined;
        }
        const canvas = this.createCanvas(1, 1);
        const ctx = canvas.getContext(
            "2d",
        ) as OffscreenCanvasRenderingContext2D;
        ctx.font = font;

        let width = 0;
        for (let c = 0; c < str.length; c++) {
            const char = str.charAt(c);
            width += ctx.measureText(char).width;
            if (width > maxWidth) {
                return c;
            }
        }
        return undefined;
    }
}
