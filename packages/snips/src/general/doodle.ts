import { type } from "arktype";
import assert from "assert";

import {
    BaseSnip,
    BaseSnipArgs,
    BaseViewSchema,
    PolygonI,
    RenderContext,
    SnipData,
    SnipDataSchema,
} from "./base";
import { simplify } from "./simplify";

import { DataValidationError } from "@//errors";

// Dynamic import for wasm
let wasm: Promise<typeof import("@snip/wasm")>;

/* -------------------------------------------------------------------------- */
/*                              DoodleSnip typing                             */
/* -------------------------------------------------------------------------- */

const EdgeSchema = type(["number", "number"]);
type Edge = typeof EdgeSchema.infer;

const DoodleDataSchema = type({
    edges: type([["number|null", "number|null"], "[]"]).describe(
        `The edges of the doodle which are connected in order they are drawn as a line.
        E.g. [[0, 0], [1, 1], [2, 2]] would draw a line from (0, 0) to (1, 1) to (2, 2). Null values are ignored.`,
    ),
}).describe(`The data of a doodle snippet.`);

const DoodleViewSchema = type(
    {
        "colour?": type("string")
            .describe(
                "The colour of the drawn lines, accepts any valid CSS color string.",
            )
            .default("black"),

        "width?": type("number > 0")
            .describe(`The width of the drawn lines in pixels.`)
            .default(4),

        "smoothing?": type("0 <= number <= 1")
            .describe(
                `Smoothing factor which determines how much smoothing is applied to the edges.
            A value of 0 corresponds to no smoothing while a value of 1 means maximum smoothing is applied.`,
            )
            .default(0),
    },
    "&",
    BaseViewSchema,
).describe(BaseViewSchema.description);

const DoodleSnipSchema = type(SnipDataSchema, "&", {
    data: DoodleDataSchema,
    "view?": DoodleViewSchema,
}).describe(`Represents a freehand drawing on a page.`);

export type DoodleData = typeof DoodleDataSchema.infer;
export type DoodleView = typeof DoodleViewSchema.infer;

export type DoodleSnipArgs = BaseSnipArgs &
    DoodleData &
    Rename<DoodleView, "width", "lineWidth">;

/* -------------------------------------------------------------------------- */
/*                          DoodleSnip Implementation                         */
/* -------------------------------------------------------------------------- */

export class DoodleSnip extends BaseSnip {
    static readonly _schema = DoodleSnipSchema;
    public type = "doodle";

    private edges: Edge[];
    public points: Float32Array;
    public colour: string;
    public lineWidth: number;
    public smoothing: number;

    constructor({
        edges = [],
        colour = "black",
        lineWidth = 4,
        smoothing = 0,
        ...baseArgs
    }: DoodleSnipArgs) {
        super({ ...baseArgs });

        // Vars specific to text snip
        this.edges = edges as Edge[];

        // Remove null/undefined entries i.e.
        // [[0, 0], [null,null], [1, 1]] -> [[0, 0], [1, 1]]
        for (let i = 0; i < this.edges.length; i++) {
            if (isNaN(this.edges[i]![0]) || isNaN(this.edges[i]![1])) {
                this.edges.splice(i, 1);
                i--;
            }
        }

        this.colour = colour;
        this.lineWidth = lineWidth;
        this.smoothing = smoothing;
        // PreCompute curve points
        if (this.smoothing > 0 && this.edges.length > 1) {
            const simpl_points = simplify(
                this.edges as [number, number][],
                4 * this.smoothing,
                false,
            ).flat();
            this.points = curve(simpl_points, this.smoothing * 0.5, 15);
            //this.points = curve(this.edges.flat(), this.smoothing, 5, false); // [x1, y1, x2, y2, ...]
        } else {
            this.points = new Float32Array(this.edges.flat());
        }
    }

    /** Add an edge to the snip, this function has to be called as
     * the smoothing is not recalculated automatically
     */
    add_edge(edge: Edge) {
        this.edges.push(edge);

        // Update points relative arbitrarily cutoff
        if (this.smoothing > 0 && this.edges.length > 1) {
            const simp_points = simplify(
                this.edges,
                4 * this.smoothing,
                false,
            ).flat();
            this.points = curve(simp_points, this.smoothing * 0.5, 15);
            //this.points = curve(this.edges.flat(), this.smoothing, 5, false); // [x1, y1, x2, y2, ...]
        } else {
            this.points = new Float32Array(this.edges.flat());
        }
    }

    get_edge(idx: number): number[] | undefined {
        return this.edges.at(idx);
    }

    get_points(): number[] | Float32Array {
        return this.points;
    }

    /** Construct a instance of the snip from a
     * SnipData object. This is used to create a snip
     * object from the database data dict.
     *
     * @param {SnipData<DoodleData, DoodleView>} data - Data dict
     */
    static from_data(data: SnipData<DoodleData, DoodleView>): DoodleSnip {
        const validation = DoodleSnipSchema(data);
        if (validation instanceof type.errors) {
            throw DataValidationError.from_ark(validation);
        }

        return new DoodleSnip({
            edges: validation.data.edges,
            colour: validation.view?.colour,
            lineWidth: validation.view?.width,
            smoothing: validation.view?.smoothing,

            //Base
            id: validation.id,
            page_id: validation.page_id,
            book_id: validation.book_id,
            last_updated: validation.last_updated,
            created: validation.created,
            x: validation.view?.x,
            y: validation.view?.y,
            rot: validation.view?.rot,
            mirror: validation.view?.mirror,
        });
    }

    /** Renders the edges array using predefined color and line width whereby
     * linejoin and linecap are set to round.
     *
     * @param {RenderContext} ctx - Canvas context
     */
    render(ctx: RenderContext): void {
        //Set color & lineWidth
        ctx.strokeStyle = this.colour;
        ctx.lineWidth = this.lineWidth;
        ctx.lineJoin = "round";
        ctx.lineCap = "round";

        // Draw the edges
        if (this.edges.length > 1) {
            ctx.beginPath();
            ctx.moveTo(this.points[0]!, this.points[1]!);
            for (let i = 0; i < this.points.length - 1; i += 2) {
                ctx.lineTo(this.points[i]!, this.points[i + 1]!);
            }
            ctx.stroke();
        }
    }

    get height(): number {
        if (this.edges.length === 0) return 0;
        // Get the max and min y values
        let min = 9999,
            max = -9999;
        for (const edge of this.edges) {
            if (edge[1]! < min) min = edge[1];
            if (edge[1]! > max) max = edge[1];
        }
        return max - min;
    }
    get width(): number {
        if (this.edges.length === 0) return 0;
        // Get the max and min x values
        let min = 9999,
            max = -9999;
        for (const edge of this.edges) {
            if (edge[0]! < min) min = edge[0];
            if (edge[0]! > max) max = edge[0];
        }
        return max - min;
    }

    /** We overload the data and view methods to
     * include the edges array.
     */

    data(): DoodleData {
        return {
            edges: this.edges,
        };
    }
    view(): DoodleView {
        const view = super.view() as DoodleView;
        view.colour = this.colour;
        view.width = this.lineWidth;
        view.smoothing = this.smoothing;
        return view;
    }

    /** Hitbox detection for lines
     *  we expand the line by width*2.
     *
     *
     */
    get polygon() {
        if (!wasm) {
            wasm = import("@snip/wasm");
        }

        if (!this._poly_cache) {
            wasm.then(({ offset_lines_simd }) => {
                this._poly_cache = new MultiRectPolygon(
                    offset_lines_simd(this.points, this.lineWidth * 2),
                );
            });
            return super.polygon;
        }
        return this._poly_cache;
    }
    _poly_cache: MultiRectPolygon | undefined;
}

class RectArray {
    // A flat array of rects [c1,c2,c3,c4,k1,k2,k3,k4]
    data: Float32Array;
    constructor(data: Float32Array) {
        assert(
            data.length % 8 == 0,
            "Length has to correspond to 2(d)*4(corners)*n ",
        );
        this.data = data;
    }

    /** Get rect as float array with stride 2 for
     * position vectors.
     */
    at(i_rect: number): Float32Array | undefined {
        const idx = i_rect * 8;
        const ret = this.data.slice(idx, idx + 8);
        if (ret.length != 8) return undefined;
        return ret;
    }
    /** Get point with rect and poly index */
    at_at(i_rect: number, i_poly: number): Float32Array | undefined {
        const idx = i_rect * 8 + i_poly * 2;
        const ret = this.data.slice(idx, idx + 2);
        if (ret.length != 2) return undefined;
        return ret;
    }
    get length(): number {
        return this.data.length / 8;
    }

    *iter_polys(): Iterable<Float32Array> {
        for (let i = 0; i < this.length; i++) {
            yield this.at(i)!;
        }
    }
}

/** A polygon which holds multiple rectangles
 */
class MultiRectPolygon extends PolygonI {
    flatPolygons: RectArray;
    constructor(data: Float32Array) {
        super();
        this.flatPolygons = new RectArray(data);
    }

    private __isInside(
        polygon: Float32Array,
        point: [number, number],
    ): boolean {
        let inside = false;
        const [x, y] = point;
        const numVertices = polygon.length / 2; // Each vertex has 2 coordinates (x, y)

        let x1 = polygon[0]!;
        let y1 = polygon[1]!;
        let x2 = 0,
            y2 = 0;

        for (let i = 1; i <= numVertices; i++) {
            x2 = polygon[(i % numVertices) * 2]!;
            y2 = polygon[(i % numVertices) * 2 + 1]!;

            if (y > Math.min(y1, y2)) {
                if (y <= Math.max(y1, y2)) {
                    if (x <= Math.max(x1, x2)) {
                        const xIntersection =
                            ((y - y1) * (x2 - x1)) / (y2 - y1) + x1;

                        if (x1 === x2 || x <= xIntersection) {
                            inside = !inside;
                        }
                    }
                }
            }

            x1 = x2;
            y1 = y2;
        }

        return inside;
    }

    isInside(point: [number, number]): boolean {
        for (const poly of this.flatPolygons.iter_polys()) {
            //early return
            if (this.__isInside(poly, point)) return true;
        }
        return false;
    }
}

/** Spline drawing is done with the curves prototype which implements
 * cardinal spline through a given point array.
 *
 * @param {Array} points - Array of points [x1, y1, x2, y2, ..., xn, yn]
 * @param {Number} [tension=0.5] - Curve tension (0...1)
 * @param {Number} [numSeg=10] - Number of segments between two points i.e. resolution (>=1)
 * @param {Boolean} [close=false] - Close the ends making the line continuous
 * @returns {Float32Array} - Array of cubic bezier curves
 **/
function curve(
    points: number[],
    tension = 0.5,
    numSeg = 10,
    close = false,
): Float32Array {
    let pts,
        i = 1,
        l = points.length,
        rPos = 0,
        cachePtr = 4;
    const rLen = (l - 2) * numSeg + 2 + (close ? 2 * numSeg : 0),
        res = new Float32Array(rLen),
        cache = new Float32Array((numSeg + 2) * 4);

    pts = points.slice(0);

    if (close) {
        pts.unshift(points[l - 1]!); // insert end point as first point
        pts.unshift(points[l - 2]!);
        pts.push(points[0]!, points[1]!); // first point as last point
    } else {
        pts.unshift(points[1]!); // copy 1. point and insert at beginning
        pts.unshift(points[0]!);
        pts.push(points[l - 2]!, points[l - 1]!); // duplicate end-points
    }

    // cache inner-loop calculations as they are based on t alone
    cache[0] = 1; // 1,0,0,0

    for (; i < numSeg; i++) {
        const st = i / numSeg,
            st2 = st * st,
            st3 = st2 * st,
            st23 = st3 * 2,
            st32 = st2 * 3;

        cache[cachePtr++] = st23 - st32 + 1; // c1
        cache[cachePtr++] = st32 - st23; // c2
        cache[cachePtr++] = st3 - 2 * st2 + st; // c3
        cache[cachePtr++] = st3 - st2; // c4
    }

    cache[++cachePtr] = 1; // 0,1,0,0

    // calc. points
    parse(pts, cache, l);

    if (close) {
        //l = points.length;
        pts = [];
        pts.push(
            points[l - 4]!,
            points[l - 3]!,
            points[l - 2]!,
            points[l - 1]!, // second last and last
            points[0]!,
            points[1]!,
            points[2]!,
            points[3]!,
        ); // first and second
        parse(pts!, cache, 4);
    }

    function parse(pts: number[], cache: Float32Array, l: number) {
        for (let i = 2, t; i < l; i += 2) {
            const pt1 = pts[i]!,
                pt2 = pts[i + 1]!,
                pt3 = pts[i + 2]!,
                pt4 = pts[i + 3]!,
                t1x = (pt3 - pts[i - 2]!) * tension,
                t1y = (pt4 - pts[i - 1]!) * tension,
                t2x = (pts[i + 4]! - pt1) * tension,
                t2y = (pts[i + 5]! - pt2) * tension;

            for (t = 0; t < numSeg; t++) {
                const c = t << 2, //t * 4;
                    c1 = cache[c]!,
                    c2 = cache[c + 1]!,
                    c3 = cache[c + 2]!,
                    c4 = cache[c + 3]!;

                res[rPos++] = c1 * pt1 + c2 * pt3 + c3 * t1x + c4 * t2x;
                res[rPos++] = c1 * pt2 + c2 * pt4 + c3 * t1y + c4 * t2y;
            }
        }
    }

    // add last point
    l = close ? 0 : points.length - 2;
    res[rPos++] = points[l]!;
    res[rPos] = points[l + 1]!;

    return res;
}
