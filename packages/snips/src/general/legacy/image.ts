import { sync as probe_sync } from "probe-image-size";

import { BaseSnip, RenderContext, SnipData } from "../base";
import type { BlobData, ImageData, ImageSnipArgs, ImageView } from "../image";
import { PlaceholderSnip } from "../placeholder";

export class ImageSnipLegacy extends BaseSnip {
    public type = "image";
    public version: number = 1;
    public blob: BlobData;

    public img_width: number;
    public img_height: number;
    public sx: number;
    public sy: number;
    public swidth: number;
    public sheight: number;
    public dwidth: number;
    public dheight: number;

    public bitmap?: CanvasImageSource;
    public ready: Promise<boolean>;

    constructor({
        blob,
        sx,
        sy,
        swidth,
        sheight,
        width,
        height,
        ...baseArgs
    }: ImageSnipArgs) {
        super({ ...baseArgs });

        // Set default values
        this.blob = blob;

        // Probe image dimensions
        const prob_res = probe_sync(Buffer.from(blob.data as string, "base64"));
        if (!prob_res) {
            throw new Error("Could not probe image dimensions");
        }
        this.img_width = prob_res.width;
        this.img_height = prob_res.height;

        // If crop is not defined
        // we use the full image
        this.sx = sx || 0;
        this.sy = sy || 0;
        this.swidth = swidth || this.img_width;
        this.sheight = sheight || this.img_height;

        // Width and height on the canvas
        this.dwidth = width || this.img_width;
        this.dheight = height || this.img_height;

        // Convert string to base64
        this.ready = this.get_bitmap();
    }

    public get_bitmap() {
        return fetch(`data:${this.blob.mime};base64,${this.blob.data}`)
            .then((response) => response.blob())
            .then((blob) => {
                try {
                    return createImageBitmap(blob);
                } catch (_e) {
                    throw new Error("No ImageBitmap support");
                }
            })
            .then((bitmap) => {
                this.bitmap = bitmap;
                return true;
            })
            .catch((_e) => {
                return false;
            });
    }

    /** Construct a instance of the snip from a
     * SnipData object. This is used to create a snip
     * object from the database data dict.
     *
     * @param {SnipData<ImageData, ImageViewLegacy>} data - Data dict
     */
    static from_data(data: SnipData<ImageData, ImageView>): ImageSnipLegacy {
        if (!data.data.blob) {
            throw new Error("Blob is null");
        }
        if (!data.data.blob.mime) {
            throw new Error("Mime is not set!");
        }

        return new ImageSnipLegacy({
            // Image
            blob: data.data.blob,
            sx: data.view?.sx,
            sy: data.view?.sy,
            swidth: data.view?.swidth,
            sheight: data.view?.sheight,
            width: data.view?.width,
            height: data.view?.height,
            //Base
            x: data.view?.x,
            y: data.view?.y,
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
        });
    }

    /** Renders the simple text to the canvas function
     *
     * @param {RenderContext} ctx - Canvas context
     */
    render(ctx: RenderContext): void {
        if (!this.bitmap) {
            console.error("No bitmap loaded!");
            return;
        }
        const transform = ctx.getTransform();
        this.rotateContext(ctx);
        if (this.created < new Date("2023-04-08T00:00:00.000Z")) {
            ctx.drawImage(
                this.bitmap,
                this.sx,
                this.sy,
                this.swidth,
                this.sheight,
                this.sx,
                this.sy,
                this.width,
                this.height,
            );
        } else {
            ctx.drawImage(
                this.bitmap,
                // In image coordinates
                this.sx,
                this.sy,
                this.swidth,
                this.sheight,
                // In canvas coordinates
                this.sx * (this.width / this.img_width),
                this.sy * (this.height / this.img_height),
                (this.swidth * this.width) / this.img_width,
                (this.sheight * this.height) / this.img_height,
            );
        }
        ctx.setTransform(transform);
    }

    //Callback can be overwritten if one wants to plot the image inside an array snip
    public callbackLoaded: (ctx: RenderContext) => void = this.render;

    /** The rotation of the image
     * Legacy only allows increments of 90 and mirroring
     *
     * Values: "0", "90", "180", "270", "m0", "m90", "m180", "m270"
     * default: "0"
     */
    private rotateContext(ctx: RenderContext): void {
        const x = this._x;
        const y = this._y;

        const rot_str = this.rot + (this.mirror ? "m" : "");

        switch (rot_str) {
            default:
            case "0":
                ctx.translate(x, y);
                break;
            case "90":
                ctx.translate(this.height + x, y);
                ctx.rotate((1 * Math.PI) / 2);
                break;
            case "180":
                ctx.translate(this.width + x, this.height + y);
                ctx.rotate((2 * Math.PI) / 2);
                ctx.scale(1, 1);
                break;
            case "270":
                ctx.translate(x, this.width + y);
                ctx.rotate((3 * Math.PI) / 2);
                ctx.scale(1, 1);
                break;
            case "0m":
                ctx.translate(this.width + x, y);
                ctx.scale(-1, 1);
                break;
            case "90m":
                ctx.translate(x + this.height, y + this.width);
                ctx.rotate((1 * Math.PI) / 2);
                ctx.scale(-1, 1);
                break;
            case "180m":
                ctx.translate(x, this.height + y);
                ctx.rotate((2 * Math.PI) / 2);
                ctx.scale(-1, 1);
                break;
            case "270m":
                ctx.translate(x + this.height, y + this.width);
                ctx.rotate((3 * Math.PI) / 2);
                ctx.scale(1, -1);
                break;
        }
    }

    get width(): number {
        return this.dwidth;
    }
    set width(width: number) {
        this.dwidth = width;
    }
    get height(): number {
        // Legacy support
        if (this.created < new Date("2023-04-08T00:00:00.000Z")) {
            return (this.sheight * this.width) / this.swidth;
        }
        return this.dheight;
    }
    set height(height: number) {
        this.dheight = height;
    }

    /** Create a dictionary in data standard format
     *
     * @returns {Dictionary}
     */
    data(): ImageData {
        return {
            blob: this.blob,
            version: this.version,
        };
    }
    view(): ImageView {
        const view = super.view() as ImageView;
        view.sx = this.sx;
        view.sy = this.sy;
        view.swidth = this.swidth;
        view.sheight = this.sheight;
        view.width = this.width;
        view.height = this.height;
        return view;
    }

    /** Overload data to allow interaction events */
    public to_data(minimal?: boolean) {
        if (minimal) {
            return PlaceholderSnip.from_snip(this).to_data();
        } else {
            // for some reason the super to_data function does not apply
            // the local data view versions of the img class
            const data = super.to_data();
            data.data = this.data();
            data.view = this.view();
            return data;
        }
    }
}
