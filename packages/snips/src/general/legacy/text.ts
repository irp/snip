import { isBrowser, isWorker } from "@snip/common/environment";

import { BaseSnip, RenderContext, SnipData } from "../base";
import type { TextData, TextSnipArgs, TextView } from "../text";

/** Old text snip
 *
 */
export class TextSnipLegacy extends BaseSnip {
    public type = "text";

    public fontFamily: string;
    public fontSize: number;
    public colour: string;
    public lineHeight: number;
    public version: number;

    constructor({
        text = "",
        fontFamily = "PlexMono",
        fontSize = 12,
        colour = "black",
        lineHeight = undefined,
        lineWrap = undefined,
        version = 2,
        ...baseArgs
    }: TextSnipArgs) {
        super({ ...baseArgs });

        this.version = version;

        this.fontFamily = fontFamily;
        this.fontSize = fontSize;
        this.colour = colour;
        this.lineHeight = lineHeight || fontSize * 1.25;
        // Set default vars
        if (version < 2) {
            this._lineWrap = lineWrap || 78;
            this._text = wrap_text(text, this._lineWrap);
        } else {
            this._lineWrap = lineWrap || 400;
            this._text = this.formatText(text);
        }
        this._height =
            this.lineHeight * 2.35 * this.getNumLines() +
            0.25 * this.fontSize * 2.35;
    }

    /** Renders the simple text to the canvas function
     *
     * @param {RenderContext} ctx - Canvas context
     */
    render(ctx: RenderContext): void {
        // Set font and style
        // 2480/1400  (TODO put into database update script)
        ctx.font = this.fontSize * 2.35 + "px " + this.fontFamily;
        ctx.fillStyle = this.colour;
        ctx.textAlign = "left";
        ctx.textBaseline = "top";

        // Split text in lines
        // For now only newline is supported
        // linewrap has to be done manually
        const line = this.text.split("\\n");
        let y = this._y;

        // Draw text
        line.forEach((text: string) => {
            ctx.fillText(text, this._x, y);
            y += this.lineHeight * 2.35;
        });
    }

    /** Construct a instance of the snip from a
     * SnipData object. This is used to create a snip
     * object from the database data dict.
     *
     * @param {SnipData<TextData, TextView>} data - Data dict
     */
    static from_data(data: SnipData<TextData, TextView>): TextSnipLegacy {
        return new TextSnipLegacy({
            // Text
            text: data.data.text,
            fontFamily: data.view.font,
            fontSize: data.view.size,
            colour: data.view.colour,
            lineHeight: data.view.lheight,
            lineWrap: data.view.wrap,
            version: data.data.version,
            //Base
            x: data.view.x,
            y: data.view.y,
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
        });
    }

    /** Create a json string for mysql from a text snip
     *
     * @returns {Dictionary}
     */
    data(): TextData {
        return {
            text: this.text,
            version: this.version,
        };
    }
    view(): TextView {
        const view = super.view() as TextView;
        view.font = this.fontFamily;
        view.size = this.fontSize;
        view.colour = this.colour;
        view.lheight = this.lineHeight;
        view.wrap = this.lineWrap;
        return view;
    }

    public getNumLines(): number {
        return this.text.split("\\n").length;
    }

    private _height: number;
    get height(): number {
        return (
            this.lineHeight * 2.35 * this.getNumLines() +
            0.25 * this.fontSize * 2.35
        );
    }
    get width(): number {
        if (this.version < 2) {
            return getWidth(this.text, this.fontSize, this.fontFamily);
        }
        const canvas = new OffscreenCanvas(1, 1);
        const ctx = canvas.getContext(
            "2d",
        ) as OffscreenCanvasRenderingContext2D;
        ctx.font = this.fontSize * 2.35 + "px " + this.fontFamily;
        let maxWidth = 0;
        const lines = this.text.split("\\n");
        for (let i = 0; i < lines.length; i++) {
            maxWidth = Math.max(maxWidth, ctx.measureText(lines[i]!).width);
        }

        return maxWidth;
    }

    private _lineWrap: number;
    get lineWrap(): number {
        return this._lineWrap;
    }
    set lineWrap(lineWrap: number) {
        this._lineWrap = lineWrap;
        if (this.version < 2) {
            this.text = wrap_text(this.text, this.lineWrap);
        } else {
            this.text = this.formatText(this.text);
        }
    }

    private _text: string;
    get text(): string {
        return this._text;
    }
    set text(text: string) {
        // format text
        if (this.version < 2) {
            this._text = wrap_text(text, this.lineWrap);
        } else {
            this._text = this.formatText(text);
        }
    }
    /** Formats the text to a usable format
     * - Replaces tabs with 8 spaces
     * - Replaces newlines with \n
     * - Calculate width and further split text if
     *  lineWrap is set and text width is too long
     *
     */
    formatText(inputString: string): string {
        // Replace tabs with 8 spaces
        inputString = inputString.replaceAll(/\t/g, "        ");
        // Replace newlines with \n
        inputString = inputString.replaceAll(/\n/g, "\\n");

        // split text in lines
        const lines = inputString.split("\\n");

        // Measure text width
        const font = this.fontSize * 2.35 + "px " + this.fontFamily;
        for (let i = 0; i < lines.length; i++) {
            const o = this.get_overlap_index(lines[i]!, this.lineWrap, font);
            if (o !== undefined && o > 0) {
                // Split line and add to next line
                if (lines[i + 1] === undefined) lines[i + 1] = "";
                lines[i + 1] = lines[i]!.substring(o) + lines[i + 1];
                lines[i] = lines[i]!.substring(0, o);
            }
            if (o == 0) {
                // each character on a new line
                lines[i] = lines[i]!.split("").join("\\n");
            }
        }

        // Join lines
        return lines.join("\\n");
    }

    /** Returns index of character which
     * overlaps the lineWrap limit.
     *
     * Returns undefined if no character overlaps
     */
    get_overlap_index(str: string, maxWidth: number, font: string) {
        if (maxWidth == -1) {
            return undefined;
        }
        if (!isBrowser() && !isWorker()) {
            return undefined;
        }
        const canvas = new OffscreenCanvas(1, 1);
        const ctx = canvas.getContext(
            "2d",
        ) as OffscreenCanvasRenderingContext2D;
        ctx.font = font;

        let width = 0;
        for (let c = 0; c < str.length; c++) {
            const char = str.charAt(c);
            width += ctx.measureText(char).width;
            if (width > maxWidth) {
                return c;
            }
        }
        return undefined;
    }
}

/** Wrap text
 * keeps all defined line breaks but adds
 * newlines at the defined wrap points
 * i.e. this.lineWrap
 *
 * @deprecated
 *
 * http://web.archive.org/web/20200925112638/https://j11y.io/snippets/wordwrap-for-javascript/
 *
 * @param {string} text The text to be wrapped
 * @param {boolean} cut If the cut is set to TRUE, the string is always
 * wrapped at or before the specified width. So if you have a word that is larger than the given width,
 * it is broken apart. When FALSE the function does not split the word even if the width is smaller than the word width.
 * @param {number} lineWrap The width of the line in characters.
 */
export function wrap_text(text: string, lineWrap: number): string {
    //?
    const str = text.replaceAll(/\t/g, "        ").replaceAll(/\\n/g, "\n");

    //TODO measure test here!
    if (lineWrap < 0 || text.length == 0) {
        return text;
    }

    const regex = String.raw`/.{1,${lineWrap}}(s|$)|.{${lineWrap}}|.+$`;

    const match = str.match(RegExp(regex, "gm")) || [];
    return match.join("\\n") || str;
}

/**
 * @deprecated
 */
export function getWidth(
    text: string,
    fontSize: number,
    fontFamily: string,
): number {
    if (isBrowser()) {
        const canvas = new OffscreenCanvas(1, 1);
        const ctx = canvas.getContext(
            "2d",
        ) as OffscreenCanvasRenderingContext2D;
        ctx.font = fontSize * 2.35 + "px " + fontFamily;
        let width = 0;
        text.split("\\n").forEach((line: string) => {
            const _width = ctx?.measureText(line).width || 0;
            if (_width > width) width = _width;
        });
        return width;
    } else {
        return 0;
    }
}
