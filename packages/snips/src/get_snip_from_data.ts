import { ArkErrors, type } from "arktype";

import {
    DataValidationError,
    UnknownNamespaceError,
    UnknownSnipTypeError,
} from "./errors";
import BASE_TYPE_TO_SNIP from "./general/__mapping";
import { ArraySnip } from "./general/array";
import { BaseData, BaseSnip, BaseView, SnipData } from "./general/base";
import { DummySnip } from "./general/dummy";
import UPRP_TYPE_TO_SNIP from "./uprp/__mapping";

// Register namespace here
export const NAMESPACE_TO_SNIP_TYPE = new Map<
    string,
    typeof BASE_TYPE_TO_SNIP
>();
NAMESPACE_TO_SNIP_TYPE.set("general", BASE_TYPE_TO_SNIP);
NAMESPACE_TO_SNIP_TYPE.set("uprp", UPRP_TYPE_TO_SNIP);

//import EXAMPLE_TYPE_TO_SNIP from "./example/__mapping";
//NAMESPACE_TO_SNIPTYPEMAP.set("example", EXAMPLE_TYPE_TO_SNIP);

const TypeSchema = type({
    type: "string",
});

/** This function parses the data of a snip and returns
 * a snip object.
 *
 * This can be seen as main entry point to convert a snip from
 * a data dict to a snip object.
 *
 * @param {SnipData} data - The data of the snip
 */
export function get_snip_from_data<
    s extends BaseSnip,
    d extends BaseData,
    v extends BaseView,
>(snip_data: SnipData<d, v>): s | BaseSnip {
    // Validate data
    const validation = TypeSchema(snip_data);
    if (validation instanceof ArkErrors) {
        throw DataValidationError.from_ark(validation);
    }

    const type = validation.type;

    // Parse Snip and return the right object
    // The type name is prefixed with
    // "[folder]/.../[file]" to avoid name clashes
    // This is not the case for the general snips!
    const type_split = type.split("/");
    if (type_split.length - 1 === 0) {
        // General snips i.e. no namespace
        const snip_type = get_snip_type_from_typemap(BASE_TYPE_TO_SNIP, type);
        if (
            snip_type == ArraySnip ||
            snip_type.prototype instanceof ArraySnip
        ) {
            return snip_type.from_data(snip_data, get_snip_from_data);
        }
        return snip_type.from_data(snip_data, get_snip_from_data);
    }
    const namespace = type_split[0];
    if (!namespace) {
        throw new UnknownNamespaceError(
            "namespace is empty for type_split: " + type_split.join("/"),
        );
    }

    if (NAMESPACE_TO_SNIP_TYPE.has(namespace)) {
        const mapping = NAMESPACE_TO_SNIP_TYPE.get(namespace)!;
        const snip_type = get_snip_type_from_typemap(mapping, type);
        if (
            snip_type == ArraySnip ||
            snip_type.prototype instanceof ArraySnip
        ) {
            return snip_type.from_data(snip_data, get_snip_from_data);
        }
        return snip_type.from_data(snip_data, get_snip_from_data);
    } else {
        throw new UnknownNamespaceError(namespace);
    }
}

export function get_snip_from_data_allow_unknown(
    snip_data: SnipData<BaseData, BaseView>,
) {
    try {
        return get_snip_from_data(snip_data);
    } catch (e) {
        if (e instanceof UnknownNamespaceError) {
            console.warn("Unknown namespace", e.message);
            return new DummySnip(snip_data);
        } else if (e instanceof UnknownSnipTypeError) {
            console.warn("Unknown snip type", e.message);
            return new DummySnip(snip_data);
        } else {
            console.error(snip_data);
            throw e;
        }
    }
}

export function get_snip_type_from_typemap(
    typemap: Map<string, typeof BaseSnip>,
    type: string,
) {
    if (typemap.has(type)) {
        return typemap.get(type)!;
    } else {
        throw new UnknownSnipTypeError(type);
    }
}
