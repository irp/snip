import type { ArkErrors } from "arktype";

export class UnknownNamespaceError extends Error {
    constructor(namespace: string) {
        super(`Unknown namespace: ${namespace}`);
        this.name = "UnknownNamespaceError";
    }
}

export class UnknownSnipTypeError extends Error {
    constructor(type: string) {
        super(`Unknown snip type: ${type}`);
        this.name = "UnknownSnipTypeError";
    }
}

export class FieldNotFoundError extends Error {
    field: string;
    constructor(field: string) {
        super(`Field not found: ${field}`);
        this.name = "FieldNotFoundError";
        this.field = field;
    }
}

export class DataValidationError extends Error {
    path: PropertyKey[];

    constructor(message: string, path: PropertyKey[]) {
        super(message);
        this.path = path;
        this.name = "DataValidationError";
    }

    static from_ark(error: ArkErrors) {
        const errors = [];
        for (const e of error) {
            errors.push(e);
        }
        if (errors.length > 1) {
            return new DataValidationErrorArray(
                errors.map((e) => new DataValidationError(e.message, e.path)),
            );
        } else if (errors.length === 1) {
            return new DataValidationError(errors[0]!.message, errors[0]!.path);
        } else {
            return new DataValidationError("Unknown error", []);
        }
    }
}

export class DataValidationErrorArray extends Error {
    errors: DataValidationError[];

    constructor(errors: DataValidationError[]) {
        super("Multiple data validation errors");
        this.errors = errors;
        this.name = "DataValidationErrorArray";
    }

    get message() {
        return (
            super.message + "\n" + this.errors.map((e) => e.message).join("\n")
        );
    }
}
