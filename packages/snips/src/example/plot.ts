import {
    BaseData,
    BaseSnip,
    BaseSnipArgs,
    BaseView,
    RenderContext,
    SnipData,
} from "../general/base";

export interface PlotData extends BaseData {
    xarray: number[];
    yarray: number[];
}

export interface PlotView extends BaseView {
    vwidth: number;
    vheight: number;
    xrange: [number, number];
    yrange: [number, number];
}
export interface ImageSnipArgs extends BaseSnipArgs {
    xarray: number[];
    yarray: number[];
    vwidth: number;
    vheight: number;
    xrange: [number, number];
    yrange: [number, number];
}

export class PlotSnip extends BaseSnip {
    public type = "example/plot";

    private xarray: number[];
    private yarray: number[];
    private vwidth: number;
    private vheight: number;
    private xrange: [number, number];
    private yrange: [number, number];

    // We can define default values here
    constructor({
        xarray,
        yarray,
        vwidth,
        vheight,
        xrange,
        yrange,
        ...baseArgs
    }: ImageSnipArgs) {
        super({ ...baseArgs });
        console.log("creating plot snip");

        if (xarray.length !== yarray.length) {
            throw new Error("xarray and yarray must have the same length");
        }
        this.xarray = xarray;
        this.yarray = yarray;

        if (vwidth < 0) {
            throw new Error("width must be positive");
        }
        this.vwidth = vwidth;

        if (vheight < 0) {
            throw new Error("height must be positive");
        }
        this.vheight = vheight;

        this.xrange = xrange;
        this.yrange = yrange;
    }

    /** Construct a instance of the snip from a
     * SnipData object. This is used to create a snip
     * object from the database data dict.
     *
     * @param {SnipData<PlotData, PlotData>} data - Data dict
     */
    static from_data(data: SnipData<PlotData, PlotView>): PlotSnip {
        return new PlotSnip({
            //Plot
            xarray: data.data.xarray,
            yarray: data.data.yarray,
            vwidth: data.view.vwidth,
            vheight: data.view.vheight,
            xrange: data.view.xrange,
            yrange: data.view.yrange,
            //Base
            x: data.view.x,
            y: data.view.y,
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
        });
    }

    // As we extend the base snip we do not have to define the to_data method
    // but we have to define the view and data methods
    public view(): PlotView {
        const view = super.view() as PlotView;
        return {
            ...view,
            vwidth: this.vwidth,
            vheight: this.vheight,
            xrange: this.xrange,
            yrange: this.yrange,
        };
    }

    public data(): PlotData {
        return {
            xarray: this.xarray,
            yarray: this.yarray,
        };
    }

    /** Renders the plot on the page
     *
     * @param {RenderContext} ctx - Canvas context
     */
    render(ctx: RenderContext): void {
        //Scale x and y range to width and height
        const xScale = this.width / (this.xrange[1] - this.xrange[0]);
        const yScale = this.height / (this.yrange[1] - this.yrange[0]);
        const xShift = -this.xrange[0] * xScale;
        const yShift = -this.yrange[0] * yScale;
        const xarray = this.xarray.map((x) => x * xScale + xShift);
        const yarray = this.yarray.map((y) => y * yScale + yShift);

        // Render axis
        ctx.beginPath();
        ctx.moveTo(this._x, this._y);
        ctx.lineTo(this._x, this._y + this.height);
        ctx.lineTo(this._x + this.width, this._y + this.height);
        ctx.stroke();

        // Render data
        ctx.beginPath();
        for (let i = 0; i < xarray.length; i++) {
            const x = xarray[i]!;
            const y = yarray[i]!;
            ctx.moveTo(this._x + x, this._y + y);
            //Draw points
            ctx.arc(this._x + x, this._y + y, 5, 0, 2 * Math.PI);
        }
        ctx.stroke();
    }

    get width(): number {
        return this.vwidth;
    }
    get height(): number {
        return this.vheight;
    }
}
