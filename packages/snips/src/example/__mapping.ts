import { BaseSnip } from "../general/base";
import { PlotSnip } from "./plot";

const TYPE_TO_SNIP: Map<string, typeof BaseSnip> = new Map();
TYPE_TO_SNIP.set("example/plot", PlotSnip);

export default TYPE_TO_SNIP;
