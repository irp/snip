import { BaseSnip } from "../general/base";
import { ImageSnip } from "../general/image";
import { AttCtSnip } from "./spec/ginix/attct";
import { LogfileSnip } from "./spec/logfile";
import { MacroSpecSnip } from "./spec/macrospec";
import { MotorsSnip } from "./spec/motors";
import { TimestampSnip } from "./spec/timestamp";

const TYPE_TO_SNIP: Map<string, typeof BaseSnip> = new Map();

// SPEC mapping
TYPE_TO_SNIP.set("uprp/spec/logfile", LogfileSnip);
TYPE_TO_SNIP.set("uprp/spec/macro.spec", MacroSpecSnip);
TYPE_TO_SNIP.set("uprp/spec/motors", MotorsSnip);
TYPE_TO_SNIP.set("uprp/spec/ginix/attct", AttCtSnip);
TYPE_TO_SNIP.set("uprp/spec/timestamp", TimestampSnip);

TYPE_TO_SNIP.set("uprp/spec/cplot", ImageSnip);
TYPE_TO_SNIP.set("uprp/spec/livedada", ImageSnip);
TYPE_TO_SNIP.set("uprp/spec/dada", ImageSnip);
TYPE_TO_SNIP.set("uprp/spec/manta", ImageSnip);
TYPE_TO_SNIP.set("uprp/spec/scope", ImageSnip);
TYPE_TO_SNIP.set("uprp/spec/matlab", ImageSnip);

// SPOC mapping
// TODO:@Markus

export default TYPE_TO_SNIP;
