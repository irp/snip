import { SnipData } from "../../general/base";
import { BlobData } from "../../general/image";
import { TextSnipLegacy } from "../../general/legacy/text";
import { TextData, TextSnipArgs, TextView } from "../../general/text";

export interface LogfileData extends TextData {
    blob: BlobData;
}

export interface LogfileSnipArgs extends TextSnipArgs {
    blob: BlobData;
}

/** Logfile Snip
 *
 * simple logfile snip implementation
 */
export class LogfileSnip extends TextSnipLegacy {
    type = "uprp/spec/logfile";
    blob: BlobData;

    constructor({
        blob,
        // Override default values for text snip
        fontSize = 7,
        lineHeight = 8.75,
        lineWrap = -1,
        ...textArgs
    }: LogfileSnipArgs) {
        super({
            lineHeight,
            fontSize,
            lineWrap,
            ...textArgs,
        });

        // Vars specific to logfile snip
        this.blob = blob;
    }

    /** Create logfile snip from a data dict
     *
     * @param data
     * @returns {LogfileSnip}
     */
    static from_data(data: SnipData<LogfileData, TextView>): LogfileSnip {
        let text = Buffer.from(
            data.data.blob.data as string,
            "base64",
        ).toString("utf8");
        const splt = text.split("\n");
        let t = "";
        splt.forEach(function (l) {
            t += l + "\\n";
        });
        text = t;
        return new LogfileSnip({
            blob: data.data.blob,
            // Text
            text: data.data.text,
            fontFamily: data.view.font,
            fontSize: data.view.size,
            colour: data.view.colour,
            lineHeight: data.view.lheight,
            lineWrap: data.view.wrap,
            //Base
            x: data.view.x,
            y: data.view.y,
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
            version: data.data.version,
        });
    }

    /** Create a data json string for mysql from a text snip
     *
     * @returns {Dictionary}
     */
    data(): LogfileData {
        return {
            blob: this.blob,
            text: this.text,
            version: this.version,
        };
    }
}
