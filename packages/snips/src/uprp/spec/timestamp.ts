import { SnipData } from "../../general/base";
import { TextSnipLegacy } from "../../general/legacy/text";
import { TextData, TextSnipArgs, TextView } from "../../general/text";

export interface TimestampData extends TextData {
    epoch: string;
}

export interface TimestampSnipArgs extends TextSnipArgs {
    /** TODO docs*/
    epoch: string;
}

export class TimestampSnip extends TextSnipLegacy {
    public type = "uprp/spec/timestamp";
    epoch: string;

    constructor({
        epoch,
        // Overwrite default values for text snip
        fontSize = 7,
        lineHeight = 8.75,
        ...args
    }: TimestampSnipArgs) {
        super({ fontSize, lineHeight, ...args });

        // Vars specific to timestamp snip
        this.epoch = epoch;
    }

    /** Create timestamp snip from a data dict
     *
     */
    static from_data(data: SnipData<TimestampData, TextView>): TimestampSnip {
        return new TimestampSnip({
            //Timestamp
            epoch: data.data.epoch,
            //Text
            text: data.data.text,
            fontFamily: data.view.font,
            fontSize: data.view.size,
            colour: data.view.colour,
            lineHeight: data.view.lheight,
            lineWrap: data.view.wrap,
            version: data.data.version,
            //Base
            x: data.view.x,
            y: data.view.y,
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
        });
    }

    /** Create a json string for mysql from a text snip
     *
     * @returns {Dictionary}
     */
    data(): TimestampData {
        return {
            epoch: this.epoch,
            text: this.text,
            version: this.version,
        };
    }
}
