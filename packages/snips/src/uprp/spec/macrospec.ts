import { SnipData } from "../../general/base";
import { BlobData } from "../../general/image";
import { TextSnipLegacy } from "../../general/legacy/text";
import { TextData, TextSnipArgs, TextView } from "../../general/text";
/** MacroSpec Snip
 *
 * simple macro snip implementation (SPEC macros)
 */
export interface MacroSpecData extends TextData {
    blob: BlobData;
}

export type MacroSpecSnipArgs = TextSnipArgs & {
    blob: BlobData;
};

export class MacroSpecSnip extends TextSnipLegacy {
    public type = "uprp/spec/macro.spec";
    blob: BlobData;

    constructor({
        blob,
        // Overwrite default values for text snip
        lineHeight = 8.75,
        fontSize = 7,
        lineWrap = -1,
        ...args
    }: MacroSpecSnipArgs) {
        super({ lineHeight, fontSize, lineWrap, ...args });

        // Vars specific to MacroSpec snip
        this.blob = blob;
    }

    /** Create MacroSpec snip from a data dict
     *
     * @param data
     * @returns {MacroSpecSnip}
     */
    static from_data(data: SnipData<MacroSpecData, TextView>): MacroSpecSnip {
        let text = Buffer.from(
            data.data.blob.data as string,
            "base64",
        ).toString("utf8");
        const splt = text.split("\n");
        let t = "";
        splt.forEach(function (l) {
            t += l + "\\n";
        });
        text = t;
        return new MacroSpecSnip({
            // MacroSpec
            blob: data.data.blob,
            // Text
            text: text,
            fontFamily: data.view.font,
            fontSize: data.view.size,
            colour: data.view.colour,
            lineHeight: data.view.lheight,
            version: data.data.version,
            //Base
            x: data.view.x,
            y: data.view.y,
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
        });
    }

    /** Create a data json string for mysql from a text snip
     *
     * @returns {Dictionary}
     */
    data(): MacroSpecData {
        return {
            blob: this.blob,
            text: this.text,
            version: this.version,
        };
    }
}
