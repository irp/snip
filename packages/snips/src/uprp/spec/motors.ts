import { sprintf } from "sprintf-js";

import {
    BaseData,
    BaseSnip,
    BaseSnipArgs,
    BaseView,
    RenderContext,
    SnipData,
} from "../../general/base";
import { getWidth } from "../../general/legacy/text";

interface MotorData {
    name: string;
    value: number; // "users" coordinate, i.e. "value"
    raw_value: number; // "dial" coordinate, i.e. "raw_value"
    limit_min: number;
    limit_max: number;
    raw_limit_min: number;
    raw_limit_max: number;
    unit: string;
}

export interface MotorsData extends BaseData {
    layout: string;
    motors: MotorData[];
    show: string;
}

export interface MotorsView extends BaseView {
    layout: string;
    size: number;
    lheight: number;
    font: string;
    nocols: number;
    wrap: number;
}

export interface MotorsSnipArgs extends BaseSnipArgs {
    /** The layout of the motors, either "wa" or "?"
     */
    layout: string;

    /** The motors to be displayed
     */
    motors: MotorData[];

    /** show string */
    show: string;

    /** nocols
     *
     * Number of columns to display
     *
     * default: if layout == "wa" then 12 else 6
     */
    numberColumns?: number;

    /** The font family to use for the text, this
     * must be available on the client and the server
     * for the text to be displayed correctly.
     *
     * default: "PlexMono"
     */
    fontFamily?: string;

    /** The font size of the text
     *
     * See https://gitlab.gwdg.de/irp/snip/-/blob/legacy/public/assets/js/common.js#L1159 for defaults
     *
     * default: 7
     */
    fontSize?: number;

    /** The line height of the text in pixels
     * (i.e. the distance between the baseline of
     * two lines of text)
     *
     * default: fontSize*1.25
     */
    lineHeight?: number;

    /** The number of characters to wrap the text at
     *
     * This should not be used if possible as it is
     * kinda buggy and not very useful atm.
     *
     * default: 200
     */
    lineWrap?: number;
}

function chunkArrayInGroups<T>(arr: Array<T>, size: number): Array<Array<T>> {
    return arr.length > size
        ? [arr.slice(0, size), ...chunkArrayInGroups(arr.slice(size), size)]
        : [arr];
}

export class MotorsSnip extends BaseSnip {
    public type = "uprp/spec/motors";

    //Data
    layout: string;
    motors: MotorData[];
    show: string;

    //View
    fontFamily: string;
    fontSize: number;
    lineHeight: number;
    numberColumns: number;

    //Other
    text: string;

    constructor({
        layout,
        motors,
        show,
        fontFamily = "PlexMono",
        fontSize = 7,
        numberColumns = layout == "wa" ? 12 : 6,
        lineHeight = 8.75,
        lineWrap = 200,
        ...baseArgs
    }: MotorsSnipArgs) {
        super({ ...baseArgs });

        // Parse default values
        this.layout = layout;
        this.motors = motors;
        this.show = show;
        this.fontFamily = fontFamily;
        this.numberColumns = numberColumns;
        this.fontSize = fontSize;
        this.lineHeight = lineHeight;
        this._lineWrap = lineWrap;
        this.text = this.createText();

        // Set dimensions
        this._height = this._height =
            this.lineHeight * 2.35 * this.getNumLines() +
            0.25 * this.fontSize * 2.35;
        this._width = getWidth(this.text, this.fontSize, this.fontFamily);
    }

    /** Create motors snip from a data dict
     * @param data The data dict containing MotorsData and MotorsView
     */
    static from_data(data: SnipData<MotorsData, MotorsView>): MotorsSnip {
        return new MotorsSnip({
            // Motor
            layout: data.view.layout,
            motors: data.data.motors,
            show: data.data.show,
            fontFamily: data.view.font,
            fontSize: data.view.size,
            lineHeight: data.view.lheight,
            lineWrap: data.view.wrap,
            //Base
            x: data.view.x,
            y: data.view.y,
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
        });
    }

    //Dimensions
    private _lineWrap: number;
    private _height: number;
    private _width: number;
    get width(): number {
        return this._width;
    }
    get height(): number {
        return this._height;
    }
    get lineWrap(): number {
        return this._lineWrap;
    }
    set lineWrap(value: number) {
        this._lineWrap = value;
        this.text = this.createText();
    }

    createText(): string {
        // Select showed motors
        let sel_motors;
        if (this.show != "*" && this.show != "") {
            const splt = this.show.split(",");
            sel_motors = this.motors.filter((motor: MotorData) => {
                for (let i = 0; i < splt.length; i++) {
                    if (motor.name == splt[i]) {
                        return true;
                    }
                }
                return false;
            });
        } else {
            sel_motors = this.motors.filter((motor: MotorData) => {
                return motor.name != "dummy";
            });
        }

        // Chunk into groups of nocols
        const groups = chunkArrayInGroups(sel_motors, this.numberColumns);

        // Create text
        let t = "";
        if (!this.motors) return t;

        if (this.layout == "wa") {
            // "wa" layout
            t += "Current Positions (user, dial)\\n"; // + timestamp
            groups.forEach((motors: MotorData[]) => {
                let names = "";
                let values = ""; // "users" coordinate system
                let raw_values = ""; // "dial" coordinate system
                motors.forEach((motor: MotorData) => {
                    // At least 9 long
                    names += sprintf("%9s ", motor.name);
                    // At least 9 long & 4 decimals
                    values += sprintf("%9.4f ", motor.value);
                    // At least 9 long & 4 decimals
                    raw_values += sprintf(
                        "%9.4f ",
                        motor.raw_value ? motor.raw_value : 0,
                    );
                });
                t += names + "\\n" + values + "\\n" + raw_values + "\\n";
            });
        } else {
            // "wm" layout
            groups.forEach((motors: MotorData[]) => {
                t += "           ";
                motors.forEach((motor: MotorData) => {
                    t += sprintf("%12s", motor.name);
                });
                t += "\\n";
                t += "User\\n";
                t += "  High     ";
                motors.forEach((motor: MotorData) => {
                    t += motor.limit_max
                        ? sprintf("%12.4f", motor.limit_max)
                        : "            ";
                });
                t += "\\n";
                t += "  Current  ";
                motors.forEach((motor: MotorData) => {
                    t += sprintf("%12.4f", motor.value);
                });
                t += "\\n";
                t += "  Low      ";
                motors.forEach((motor: MotorData) => {
                    t += motor.limit_min
                        ? sprintf("%12.4f", motor.limit_min)
                        : "            ";
                });
                t += "\\n";
                t += "Dial\\n";
                t += "  High     ";
                motors.forEach((motor: MotorData) => {
                    t += motor.raw_limit_max
                        ? sprintf("%12.4f", motor.raw_limit_max)
                        : "            ";
                });
                t += "\\n";
                t += "  Current  ";
                motors.forEach((motor: MotorData) => {
                    t += motor.raw_value
                        ? sprintf("%12.4f", motor.raw_value)
                        : "            ";
                });
                t += "\\n";
                t += "  Low      ";
                motors.forEach((motor: MotorData) => {
                    t += motor.raw_limit_min
                        ? sprintf("%12.4f", motor.raw_limit_min)
                        : "            ";
                });
                t += "\\n";
            });
        }

        //Apply lineWrap
        //t = wrap_text(t, this.lineWrap);
        return t;
    }

    data(): MotorsData {
        return {
            motors: this.motors,
            layout: this.layout,
            show: this.show,
        };
    }
    view(): MotorsView {
        const view = super.view() as MotorsView;
        view.layout = this.layout;
        view.size = this.fontSize;
        view.lheight = this.lineHeight;
        view.font = this.fontFamily;
        view.nocols = this.numberColumns;
        return view;
    }

    public getNumLines(): number {
        return this.text.split("\\n").length;
    }

    render(ctx: RenderContext): void {
        // We want all mono fonts to be PlexMono
        if (this.fontFamily == "mono" || this.fontFamily == "monospace")
            this.fontFamily = "PlexMono";

        // Set font and style
        // 2480/1400  (TODO put into database update script)
        ctx.font = this.fontSize * 2.35 + "px " + this.fontFamily;
        ctx.fillStyle = "black";
        ctx.textAlign = "left";
        ctx.textBaseline = "top";

        // Split text in lines
        // For now only newline is supported
        // TODO: lineWrap is not implemented
        const line = this.text.split("\\n");
        let y = this._y;

        // Draw text
        line.forEach((text: string) => {
            ctx.fillText(text, this._x, y);
            y += this.lineHeight * 2.35;
        });
    }
}
