import { sprintf } from "sprintf-js";

import { SnipData } from "../../../general/base";
import { TextSnipLegacy } from "../../../general/legacy/text";
import { TextData, TextSnipArgs, TextView } from "../../../general/text";

export interface AttCtData extends TextData {
    att: number;
    factor: number;
    ct: number;
}

export interface AttCtArgs extends TextSnipArgs {
    /** TODO docs*/
    att: number;
    factor: number;
    ct: number;
}

/** spec: attenuator + count Snip
 *
 * simple snip implementation for spec's counting time + attenuator setting
 */
export class AttCtSnip extends TextSnipLegacy {
    public type = "uprp/spec/ginix/attct";
    att: number;
    factor: number;
    ct: number;

    constructor({
        att,
        factor,
        ct,
        // Overwrite default values for text snip
        lineHeight = 8.75,
        fontSize = 7,
        lineWrap = -1,
        ...textArgs
    }: AttCtArgs) {
        super({ lineHeight, fontSize, lineWrap, ...textArgs });

        // Vars specific to attct snip
        this.att = att;
        this.factor = factor;
        this.ct = ct;
    }

    /** Create attct snip from a data dict
     *
     */
    static from_data(data: SnipData<AttCtData, TextView>): AttCtSnip {
        const text = sprintf(
            "att: %d, factor: %d, ct %g",
            data.data.att,
            data.data.factor,
            data.data.ct,
        );
        return new AttCtSnip({
            //AttCt
            att: data.data.att,
            factor: data.data.factor,
            ct: data.data.ct,
            //Text
            text: text,
            fontFamily: data.view.font,
            fontSize: data.view.size,
            colour: data.view.colour,
            lineHeight: data.view.lheight,
            lineWrap: -1,
            //Base
            x: data.view.x,
            y: data.view.y,
            id: data.id,
            page_id: data.page_id,
            book_id: data.book_id,
            last_updated: data.last_updated,
            created: data.created,
        });
    }

    /** Create a json string for mysql from a text snip
     *
     * @returns {Dictionary}
     */
    data(): AttCtData {
        return {
            att: this.att,
            factor: this.factor,
            ct: this.ct,
            text: this.text,
            version: this.version,
        };
    }
}
