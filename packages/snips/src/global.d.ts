type Rename<T, K extends keyof T, N extends string> = Pick<
    T,
    Exclude<keyof T, K>
> & { [P in N]: T[K] };

type RenameByT<T, U> = {
    [K in keyof U as K extends keyof T
        ? T[K] extends string
            ? T[K]
            : never
        : K]: K extends keyof U ? U[K] : never;
};
