import type { Socket as SocketIoSocket } from "socket.io-client";

import type { BaseData, BaseView, SnipData } from "./general/base";

type ID = number;

// Copied from @snip/socket
type ResponseData<T> = T extends void
    ? {
          ok: true;
      }
    : {
          ok: true;
          data: T;
      };

/** A bit of error handling
 * for the callbacks
 */
type CBResponse<T> =
    | ResponseData<T>
    | {
          ok: false;
          error: string;
      };
type Callback<T = void> = (res: CBResponse<T>) => void;

interface ClientToServerEvents {
    // Insert or update snip
    "snip:upsert": (
        snip_data: SnipData<BaseData, BaseView>,
        callback: Callback<ID>,
    ) => void;
    // Remove snip
    "snip:remove": (
        snip_id: ID,
        nested: boolean,
        callback: Callback<true>,
    ) => void;

    "snip:preview": (snip_data: SnipData<BaseData, BaseView>) => void;
}
interface ServerToClientEvents {
    // These can be called both in the page room and without a room
    "snip:updated": (updatedSnip: SnipData<BaseData, BaseView>) => void;
    "snip:removed": (snip_id: ID) => void;
    "snip:inserted": (snip: SnipData<BaseData, BaseView>) => void;
    "snip:placed": (snip_id: ID) => void;
}

export type Socket = SocketIoSocket<ServerToClientEvents, ClientToServerEvents>;
