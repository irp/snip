import { offset_line_simd, offset_lines_simd } from "../build/snips_wasm";

function offset_line(
    p1: number[] | Float32Array,
    p2: number[] | Float32Array,
    padding: number,
) {
    const [x1, y1] = p1;
    const [x2, y2] = p2;
    // Calculate direction vector
    const dx = x2 - x1;
    const dy = y2 - y1;

    // Calculate perpendicular vector
    const length = Math.sqrt(dx * dx + dy * dy);
    const ux = (-dy / length) * padding;
    const uy = (dx / length) * padding;

    // Calculate direction vector with padding
    const px = (-dx / length) * padding;
    const py = (-dy / length) * padding;

    return [
        [x1 + ux + px, y1 + uy + py],
        [x1 - ux + px, y1 - uy + py],
        [x2 - ux - px, y2 - uy - py],
        [x2 + ux - px, y2 + uy - py],
    ];
}

function offset_lines(points: Float32Array, offset: number) {
    const rects: number[][] = [];
    for (let i = 0; i < points.length - 1; i++) {
        const idx1 = i * 2;
        const idx2 = (i + 1) * 2;
        rects.concat(
            offset_line(
                [points[idx1], points[idx1 + 1]],
                [points[idx2], points[idx2 + 1]],
                offset,
            ),
        );
    }
    return rects;
}

// Function to measure execution time of a function
function measureExecutionTime(func: Function) {
    const startTime = performance.now();
    func();
    const endTime = performance.now();
    return endTime - startTime;
}

// Define points p1 and p2 as Float32Array
const p1 = new Float32Array([-1, 0]);
const p2 = new Float32Array([1, 0]);

// Define offset value
const offset = 0.5;

// Perform the test 100 times for offset_line
console.log("Testing offset_line...");
let normalTimes: number[] = [];
for (let i = 0; i < 1000; i++) {
    const time = measureExecutionTime(() => offset_line(p1, p2, offset));
    normalTimes.push(time);
}
console.log(
    "Normal Execution Times (ms):",
    normalTimes.reduce((a, b) => a + b, 0) / normalTimes.length,
);
// Perform the test 100 times for offset_line_simd
console.log("Testing offset_line_simd...");
let simdTimes: number[] = [];
for (let i = 0; i < 1000; i++) {
    const time = measureExecutionTime(() => offset_line_simd(p1, p2, offset));
    simdTimes.push(time);
}
console.log(
    "SIMD Execution Times (ms):",
    simdTimes.reduce((a, b) => a + b, 0) / simdTimes.length,
);

// same for lines
const line_ps: number[] = [];
for (let i = 0; i < 100; i++) {
    line_ps.push(i * 0.85, i * 0.76);
}

const array = new Float32Array(line_ps);
console.log("Testing offset_lines...");
normalTimes = [];
for (let i = 0; i < 1000; i++) {
    const time = measureExecutionTime(() => offset_lines(array, offset));
    normalTimes.push(time);
}
console.log(
    "Normal Execution Times (ms):",
    normalTimes.reduce((a, b) => a + b, 0) / normalTimes.length,
);
// Perform the test 100 times for offset_line_simd
console.log("Testing offset_line_simd...");
simdTimes = [];
for (let i = 0; i < 1000; i++) {
    const time = measureExecutionTime(() => offset_lines_simd(array, offset));
    simdTimes.push(time);
}
console.log(
    "SIMD Execution Times (ms):",
    simdTimes.reduce((a, b) => a + b, 0) / simdTimes.length,
);
