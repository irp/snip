//! Test suite for the Web and headless browsers.
extern crate wasm_bindgen_test;

use wasm_bindgen_test::*;
wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn pass() {
    assert_eq!(1 + 1, 2);
}
// Define macro for approximate equality assertion with epsilon
macro_rules! assert_approx_eq {
    ($a:expr, $b:expr) => {
        let epsilon = 1e-2;
        let (a, b) = ($a, $b);
        assert!((a - b).abs() < epsilon, "Expected {}, got {}", b, a);
    };
}

extern crate snips_wasm;
use snips_wasm::offset_line_simd;
#[wasm_bindgen_test]
fn test_offset_line_simd() {
    let p1 = [-1., 0.];
    let p2 = [1., 0.];
    let rect = offset_line_simd(&p1, &p2, 0.5);

    println!("{:?}", rect);
    // Assert that the result has exactly 8 elements (4 corners * 2 coordinates each)
    assert_eq!(rect.len(), 8);

    // Assert specific corner values based on expected calculations
    assert_approx_eq!(rect[0], -1.5); // x-coordinate of corner 1
    assert_approx_eq!(rect[1], 0.5); // y-coordinate of corner 1

    assert_approx_eq!(rect[2], -1.5); // x-coordinate of corner 2
    assert_approx_eq!(rect[3], -0.5); // y-coordinate of corner 2

    assert_approx_eq!(rect[4], 1.5); // x-coordinate of corner 1
    assert_approx_eq!(rect[5], -0.5); // y-coordinate of corner 1

    assert_approx_eq!(rect[6], 1.5); // x-coordinate of corner 2
    assert_approx_eq!(rect[7], 0.5); // y-coordinate of corner 2
}
use snips_wasm::offset_lines_simd;
#[wasm_bindgen_test]
fn test_offset_lines_simd() {
    const NUM_POINTS: usize = 4;
    let mut p = [0.0; NUM_POINTS * 2];
    for i in (0..p.len()).step_by(4) {
        p[i] = -1.;
        p[i + 1] = 0.;
        p[i + 2] = 1.;
        p[i + 3] = 0.;
    }

    let rect = offset_lines_simd(&p, 0.5);

    // Assert that the result has exactly 8 elements (4 corners * 2 coordinates each)
    assert_eq!(rect.len(), 8 * (NUM_POINTS - 1));

    // Assert specific corner values based on expected calculations
    assert_approx_eq!(rect[0], -1.5); // x-coordinate of corner 1
    assert_approx_eq!(rect[1], 0.5); // y-coordinate of corner 1

    assert_approx_eq!(rect[2], -1.5); // x-coordinate of corner 2
    assert_approx_eq!(rect[3], -0.5); // y-coordinate of corner

    assert_approx_eq!(rect[4], 1.5); // x-coordinate of corner 1
    assert_approx_eq!(rect[5], -0.5); // y-coordinate of corner 1

    assert_approx_eq!(rect[6], 1.5); // x-coordinate of corner 2
    assert_approx_eq!(rect[7], 0.5); // y-coordinate of corner 2

    assert_approx_eq!(rect[8], 1.5); // x-coordinate of corner 1
    assert_approx_eq!(rect[9], -0.5); // y-coordinate of corner 1
    assert_approx_eq!(rect[10], 1.5); // x-coordinate of corner 2
    assert_approx_eq!(rect[11], 0.5); // y-coordinate of corner 2
}

#[wasm_bindgen_test]
fn test_offset_lines_simd_minimal() {
    let p = [0.0, 0.0, 1.0, 1.0];

    let rect = offset_lines_simd(&p, 0.5);

    // Assert that the result has exactly 4 elements i.e. one rect
    assert_eq!(rect.len(), 8);
}
