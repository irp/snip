#![feature(portable_simd)]
use std::simd::*;

use num::SimdFloat;
use wasm_bindgen::prelude::*;

extern crate wee_alloc;

// Use `wee_alloc` as the global allocator.
// This reduces the size by ~ a factor 10
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub fn offset_line_simd(p1: &[f32], p2: &[f32], offset: f32) -> Vec<f32> {
    assert!(p1.len() == 2);
    assert!(p2.len() == 2);

    let v1 = Simd::from_slice(p1);
    let v2 = Simd::from_slice(p2);

    let direction = v2 - v1;
    let norm = (direction * direction).reduce_sum().inv_sqrt();

    let p = direction * Simd::splat(offset * norm);
    // ux = py*-1 || uy = dx*1
    let u = p.rotate_elements_left::<1>() * Simd::from_array([-1., 1.]);

    let c1 = v1 - p + u;
    let c2 = v1 - p - u;
    let c3 = v2 + p - u;
    let c4 = v2 + p + u;
    // Return corners as a Vec<f32>
    let mut corners = Vec::with_capacity(8);
    corners.extend_from_slice(&c1.to_array());
    corners.extend_from_slice(&c2.to_array());
    corners.extend_from_slice(&c3.to_array());
    corners.extend_from_slice(&c4.to_array());

    corners
}

// Trait defining the inv_sqrt method
trait InvSqrt {
    fn inv_sqrt(self) -> Self;
}
impl InvSqrt for f32 {
    fn inv_sqrt(self) -> Self {
        let x = self;
        let i = x.to_bits();
        let i = 0x5f3759df - (i >> 1);
        let y = f32::from_bits(i);
        y * (1.5 - 0.5 * x * y * y)
    }
}

#[wasm_bindgen]
pub fn offset_lines_simd(points: &[f32], offset: f32) -> Vec<f32> {
    assert!(points.len() % 2 == 0);

    let num_segments = points.len() / 2;

    let mut corners = Vec::with_capacity(num_segments * 8);
    const INV: Simd<f32, 2> = Simd::from_array([-1., 1.]);

    for i in 0..num_segments - 1 {
        let idx1 = i * 2;
        let idx2 = (i + 1) * 2;

        let p1 = Simd::from_array([points[idx1], points[idx1 + 1]]);
        let p2 = Simd::from_array([points[idx2], points[idx2 + 1]]);

        let direction = p2 - p1;
        let norm = (direction * direction).reduce_sum().inv_sqrt();

        let p = direction * Simd::splat(offset * norm);
        // ux = py*-1 || uy = dx*1
        let u = p.rotate_elements_left::<1>() * INV;

        corners.extend_from_slice(&(p1 - p + u).to_array());
        corners.extend_from_slice(&(p1 - p - u).to_array());
        corners.extend_from_slice(&(p2 + p - u).to_array());
        corners.extend_from_slice(&(p2 + p + u).to_array());
    }

    corners
}
