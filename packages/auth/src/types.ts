// We use the same session data in the frontend and backend
// this makes it a bit easier to keep track of who is
// authenticated and what they are allowed to do

import type { IronSession } from "iron-session";

export interface SessionData {
    user?: UserSessionData;
    anonymous?: TokenSessionData;
    api?: ApiSessionData;
}

/** For user sessions
 * they create a session for a user
 * e.g. login
 */
interface UserSessionData {
    id: number;
    email_verified: boolean;
    created: number;
}

/** For internal request
 * they create a session for an api user
 * e.g. pdf render in parallel
 */
interface ApiSessionData {
    // Identifier for the api user
    identifier: string;
}

/** For ui tokens
 * they also create a session
 * for the (anonymous) user
 * e.g. sharable links
 * we allow multiple ui tokens
 * to be active at the same time
 */
export interface TokenSessionData {
    tokens: {
        id: number;
        book_id: number;
        expires_at: number;
    }[];
}

type Prettify<T> = {
    [K in keyof T]: T[K];
} & object;

export type SnipSessionData = Prettify<IronSession<SessionData>>;
