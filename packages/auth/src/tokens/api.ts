import { createHmac, randomBytes } from "crypto";

import { config } from "@snip/config/server";
import service from "@snip/database";
import { APITokenData } from "@snip/database/types";

export const API_TOKEN_LENGTH_IN_CHARS = 128;

export async function generateAPITokenTuple(): Promise<{
    token: string;
    hashed_token: string;
}> {
    const token = randomBytes(
        Math.ceil((API_TOKEN_LENGTH_IN_CHARS / 4) * 3),
    ).toString("hex");
    const hashed_token = hash(token);
    return { token, hashed_token };
}

export async function verifyAPIToken(
    token: string,
    book_id: number,
): Promise<Required<APITokenData>> {
    // try to unseal token
    const hashed_token = hash(token);

    let tokenData: Required<APITokenData>;
    try {
        tokenData = await service.token.getAPITokenByBookIdAndToken(
            book_id,
            hashed_token,
        );
    } catch (_error) {
        throw new Error("Invalid token");
    }

    return tokenData;
}

function hash(token: string): string {
    const hmac = createHmac("sha256", config.secrets.parse().api_token);
    hmac.update(token);
    return hmac.digest("hex");
}
