import jwt from "jsonwebtoken";

import { config } from "@snip/config/server";

/**
 * Encrypts and seals the provided data into a token using JWT.
 *
 * @param data - The data to be sealed. Can be a Buffer, string, or object.
 * @param salt - An optional salt to be appended to the secret key.
 * @param options - Optional JWT signing options.
 * @returns A promise that resolves to a hexadecimal string representing the sealed token.
 */
export async function sealData<T extends Buffer | string | object>(
    data: T,
    salt?: string,
    options?: jwt.SignOptions,
): Promise<string> {
    const active_secret = config.secrets.activeEmailTokenSecret();
    let secret = active_secret.secret;
    if (salt) {
        secret += salt;
    }
    const str = await jwt.sign(data, secret, {
        ...options,
        keyid: active_secret.id,
    });
    return Buffer.from(str).toString("hex");
}

/**
 * Decrypts and unseals the provided token back into its original data using JWT.
 *
 * @param token - The hexadecimal string representing the sealed token.
 * @param salt - An optional salt to be appended to the secret key.
 * @param options - Optional JWT verification options.
 * @returns A promise that resolves to the original data.
 */
export function unsealToken<T extends Buffer | string | object>(
    token: string,
    salt?: string,
    options?: jwt.VerifyOptions,
): T {
    return jwt_verify(
        Buffer.from(token, "hex").toString(),
        config.secrets.parse().email_token,
        salt,
        options,
    ) as T;
}

/** Helper function to allow for secret rotation.
 */
export function jwt_verify(
    token: string,
    secret_store: Record<
        string,
        {
            secret: string;
            active: boolean;
        }
    >,
    salt?: string,
    options?: jwt.VerifyOptions,
) {
    const decoded = jwt.decode(token, { complete: true });
    if (!decoded || typeof decoded === "string") {
        throw new Error("Invalid token");
    }

    let secret: string;
    if (!decoded.header.kid || secret_store[decoded.header.kid] === undefined) {
        const active_secret = config.secrets._getActive(secret_store);
        secret = active_secret.secret;
    } else {
        secret = secret_store[decoded.header.kid]!.secret;
    }

    if (salt) {
        secret += salt;
    }

    return jwt.verify(token, secret, options);
}
