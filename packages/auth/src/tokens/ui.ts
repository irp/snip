import { randomBytes } from "crypto";

import service from "@snip/database";
import { UITokenData } from "@snip/database/types";

export const UI_TOKEN_LENGTH_IN_CHARS = 64;

/** UI tokens
 * are smaller and can be are always checked against the db for validity
 * therefore we do not need the typical sign/verify pattern.
 *
 * Additional to the hashed_token we also store the plain token in the db
 *
 */
export async function generateUIToken(): Promise<string> {
    const token = randomBytes(
        Math.ceil((UI_TOKEN_LENGTH_IN_CHARS / 4) * 3),
    ).toString("base64");
    return token;
}

/**
 * Verifies a UI token for a specific book. Does not check if the token
 * is expired!
 * @param token - The UI token to verify.
 * @param book_id - The ID of the book.
 * @returns A promise that resolves to the verified token data.
 * @throws An error if the token is invalid.
 */
export async function verifyUIToken(
    token: string,
    book_id: number,
): Promise<Required<UITokenData>> {
    let tokenData: Required<UITokenData>;
    try {
        tokenData = await service.token.getUITokenByBookIdAndToken(
            book_id,
            token,
        );
    } catch (_error) {
        throw new Error("Invalid token");
    }

    return tokenData;
}
