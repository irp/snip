import { CollaborationInviteDataRet } from "@snip/database/types";

import { sealData, unsealToken } from "./common";

export interface CollaborationRequestToken {
    time: number;
    invite_id: number;
    type: "collaboration_request";
}

/**
 * Unseal a collaboration request token.
 *
 * @param user_id The user that wants to accept the collaboration request.
 * @param token
 * @returns The unsealed token.
 * @raises Error if the token is invalid.
 */
export async function unsealCollaborationRequestToken(
    token: string,
    user_id: number,
): Promise<CollaborationRequestToken> {
    // try to unseal token
    let tokenData: CollaborationRequestToken;
    try {
        tokenData = await unsealToken<CollaborationRequestToken>(
            token,
            user_id.toFixed(),
        );
    } catch (_error) {
        throw new Error("Invalid token");
    }
    if (tokenData.type !== "collaboration_request") {
        throw new Error("Invalid token");
    }
    if (!("invite_id" in tokenData)) {
        throw new Error("Invalid token");
    }

    return tokenData;
}

/**
 * Create a token to accept a collaboration request.
 *
 * @param invite The collaboration invite data.
 * @returns The generated token string
 * @raises Error if the invite is invalid.
 */
export async function createCollaborationRequestToken(
    invite: CollaborationInviteDataRet,
): Promise<string> {
    if (!invite.id || !invite.user_id) {
        throw new Error("Invalid invite");
    }

    const data: CollaborationRequestToken = {
        time: Date.now(),
        invite_id: invite.id,
        type: "collaboration_request",
    };

    // If none given 96h

    if (!invite.expire_at) {
        invite.expire_at = new Date(Date.now() + 96 * 60 * 60 * 1000);
    }

    return await sealData(data, invite.user_id.toFixed(), {
        expiresIn: invite.expire_at.getTime() - Date.now(),
    });
}
