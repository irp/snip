import jwt from "jsonwebtoken";

import { config } from "@snip/config/server";
import { ID } from "@snip/database/types";

import { jwt_verify } from "./common";

export interface SocketTokenPayload extends jwt.JwtPayload {
    book_id: ID;
    user_id?: ID;
    token?: string;
}
export interface SocketTokenPayloadApi extends jwt.JwtPayload {
    api: boolean;
    date: number;
}

export function signToken(payload: SocketTokenPayload): string {
    const active_secret = config.secrets.activeSocketSecret();
    return jwt.sign(payload, active_secret.secret, {
        expiresIn: "24h",
        keyid: active_secret.id,
    });
}

/**
 * Verifies a socket token.
 * @param token - The token to verify.
 * @returns The payload of the verified token, including the book_id, user_id, and token.
 * @throws Error if the token is invalid or missing required properties.
 */
export function verifyToken(token: string): SocketTokenPayload {
    const payload = jwt_verify(
        token,
        config.secrets.parse().socket,
    ) as SocketTokenPayload;

    //Check if payload is valid
    if (!payload || typeof payload !== "object") {
        throw new Error("Invalid token" + JSON.stringify(payload));
    }

    // Check if book_id is present
    if (!payload.book_id) {
        throw new Error("Invalid token no book_id!");
    }
    // Either user_id or token must be present
    if (!payload.user_id && !payload.token) {
        throw new Error("Invalid token no user_id or token!");
    }
    // Add undefined to user_id and token
    if (!payload.user_id) {
        payload.user_id = undefined;
    }
    if (!payload.token) {
        payload.token = undefined;
    }

    return payload as SocketTokenPayload;
}

export function signApiToken(payload: SocketTokenPayloadApi): string {
    const active_secret = config.secrets.activeSocketSecret();
    return jwt.sign(payload, active_secret.secret, {
        expiresIn: "2h",
        keyid: active_secret.id,
    });
}

export function verifyApiToken(token: string): SocketTokenPayloadApi {
    const payload = jwt_verify(
        token,
        config.secrets.parse().socket,
    ) as SocketTokenPayloadApi;

    //Check if payload is valid
    if (!payload || typeof payload !== "object") {
        throw new Error("Invalid token" + JSON.stringify(payload));
    }

    // Check if api is present
    if (!payload.api) {
        throw new Error("Invalid token no api!");
    }

    return payload as SocketTokenPayloadApi;
}
