import {
    MemberDataRet as MemberData,
    UserDataRet as UserData,
} from "@snip/database/types";

import { sealData, unsealToken } from "./common";

export interface GroupInviteToken {
    time: number;
    type: "group_invite";
    // group that user is invited to
    group_id: number;
    group_name: string;
    // user who invited other user
    by_id: number;
    //user who was invited
    user_id: number;
}

export async function unsealGroupInviteToken(
    token: string,
    potentialMember: MemberData,
) {
    // try to unseal token
    let tokenData: GroupInviteToken;
    try {
        tokenData = await unsealToken(
            token,
            potentialMember.user_id.toFixed() +
                potentialMember.joined_at.getTime(),
        );
    } catch (_error) {
        throw new Error("Invalid token");
    }

    if (
        !tokenData.user_id ||
        !tokenData.group_id ||
        tokenData.user_id !== potentialMember.user_id ||
        tokenData.group_id !== potentialMember.group_id ||
        tokenData.type !== "group_invite"
    ) {
        throw new Error("Invalid token");
    }

    return tokenData;
}

export async function createGroupInviteToken(
    user: UserData,
    potentialMember: MemberData,
    invited_by_id: number,
    group: { id: number; name: string },
): Promise<string> {
    const data: GroupInviteToken = {
        time: Date.now(),
        type: "group_invite",
        //group
        group_id: group.id,
        group_name: group.name,
        //inviter
        by_id: invited_by_id,
        //user
        user_id: user.id,
    };

    return await sealData(
        data,
        potentialMember.user_id.toFixed() + potentialMember.joined_at.getTime(),
        {
            expiresIn: "96h",
        },
    );
}
