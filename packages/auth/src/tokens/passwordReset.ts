import { CredentialsData } from "@snip/database/types";

import { sealData, unsealToken } from "./common";

export interface PasswordResetToken {
    time: number;
    user_id: number; // We only have one password per user so we don't need to specify the credential
    type: "password_reset";
}
/** Create a token to reset the password of a user.
 *
 * @param user_id
 * @returns
 * @raises Error if the user_id is invalid.
 */
export async function createPasswordResetToken(
    credentials: Required<CredentialsData>,
): Promise<string> {
    if (
        credentials.credential_type !== "password" ||
        !credentials.password_hash
    ) {
        throw new Error("Invalid credential type");
    }

    const data: PasswordResetToken = {
        time: Date.now(),
        user_id: credentials.user_id,
        type: "password_reset",
    };

    // seal the data
    return await sealData(data, credentials.password_hash, {
        expiresIn: "1h",
    });
}

/** Unseal a password reset token.
 *
 * @param token The token to unseal.
 * @param user_id The user that wants to reset their password.
 * @returns The unsealed token.
 * @raises Error if the token is invalid.
 *
 */
export async function unsealPasswordResetToken(
    token: string,
    credentials: Required<CredentialsData>,
): Promise<PasswordResetToken> {
    if (!credentials.password_hash) {
        throw new Error("Invalid user_id");
    }
    // try to unseal token
    let tokenData: PasswordResetToken;
    try {
        tokenData = await unsealToken<PasswordResetToken>(
            token,
            credentials.password_hash,
        );
    } catch (_error) {
        throw new Error("Invalid token");
    }
    if (tokenData.type !== "password_reset") {
        throw new Error("Invalid token");
    }
    if (tokenData.user_id !== credentials.user_id) {
        throw new Error("Invalid token");
    }

    return tokenData;
}
