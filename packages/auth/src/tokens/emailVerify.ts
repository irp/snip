import { CredentialsData } from "@snip/database/types";

import { sealData, unsealToken } from "./common";

export interface VerifyEmailToken {
    time: number;
    email: string;
    user_id: number;
    credential_id: number;
    type: "verify_email";
}

export async function unsealEmailVerifyToken(
    token: string,
    user_id: number,
): Promise<VerifyEmailToken> {
    // try to unseal token
    let tokenData: VerifyEmailToken;
    try {
        tokenData = await unsealToken<VerifyEmailToken>(
            token,
            user_id.toFixed(),
        );
    } catch (_error) {
        throw new Error("Invalid token");
    }
    if (tokenData.type !== "verify_email") {
        throw new Error("Invalid token");
    }
    if (tokenData.user_id !== user_id) {
        throw new Error("Invalid token");
    }

    return tokenData;
}

/**
 * Creates a Token for email verification.
 *
 * @param credential - The credentials data containing the user's email and user ID.
 * @returns A promise that resolves to the generated JWT.
 * @raises Error if the token is invalid.
 */
export async function createEmailVerifyToken(
    credential: CredentialsData,
): Promise<string> {
    const data = {
        time: Date.now(),
        email: credential.email,
        user_id: credential.user_id,
        credential_id: credential.id,
        type: "verify_email",
    };

    return await sealData(data, credential.user_id.toFixed(), {
        expiresIn: "2h",
    });
}
