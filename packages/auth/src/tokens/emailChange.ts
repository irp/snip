import { UserDataRet } from "@snip/database/types";

import { sealData, unsealToken } from "./common";
export interface EmailChangeToken {
    time: number;
    from_credential_id: number;
    to_email: string;
    user_id: number;
    type: "change_email";
}

/** Create a token to change the email of a user.
 *
 *  The token is signed with the old email of the user.
 *
 * @param user The user that wants to change their email.
 * @param from_credential_id The id of the credential that will be changed.
 * @param new_email The new email of the user.
 * @returns
 * @raises Error if the credential id is invalid.
 */
export async function createEmailChangeToken(
    user: UserDataRet,
    from_credential_id: number,
    new_email: string,
): Promise<string> {
    const data: EmailChangeToken = {
        time: Date.now(),
        from_credential_id: from_credential_id,
        to_email: new_email,
        user_id: user.id,
        type: "change_email",
    };
    const old_email = getOldEmail(user, from_credential_id);
    // seal the data
    return await sealData(data, old_email);
}

/** Unseal an email change token.
 *
 * Only works once since the token uses the old
 * email as part of the secret.
 *
 * @param token The token to unseal.
 * @param user The user that wants to change their email.
 * @param from_credential_id The id of the credential that will be changed.
 * @returns The unsealed token.
 * @raises Error if the credential id is invalid.
 * @raises Error if the token is invalid.
 *
 */
export async function unsealEmailChangeToken(
    token: string,
    user: UserDataRet,
    from_credential_id: number,
): Promise<EmailChangeToken> {
    // try to get old email
    const old_email = getOldEmail(user, from_credential_id);

    // try to unseal token
    let tokenData: EmailChangeToken;
    try {
        tokenData = await unsealToken<EmailChangeToken>(token, old_email);
    } catch (_error) {
        throw new Error("Invalid token");
    }

    // Short validity check
    if (tokenData.type !== "change_email") {
        throw new Error("Invalid token");
    }

    return tokenData;
}

function getOldEmail(user: UserDataRet, credential_id: number) {
    // check if the credential id is valid
    const idx = user.credential_ids.findIndex((c) => c === credential_id);
    if (idx === -1) {
        throw new Error("Invalid credential id");
    }
    const old_email = user.emails[idx];
    if (!old_email) {
        throw new Error("Invalid credential id");
    }
    return old_email;
}
