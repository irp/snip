import service from "@snip/database";
import {
    APITokenData,
    ENTITY_TOKEN,
    PermissionACLData,
    Resource,
    RESOURCE_BOOK,
} from "@snip/database/types";
import { UITokenData } from "@snip/database/types";

import { verifyAPIToken } from "./tokens/api";

type AuthMethod = "bearer" | "user" | "ui_token" | "unknown" | "combined";
export interface ParsedPermissionACLData extends PermissionACLData {
    auth_method: AuthMethod;
    denied_reason?: string;
    expires_at?: Date;
}

/**
 * Get the users permission for a given book
 * this function can e.g. be used to check if a user
 * has read access to a book.
 *
 * Optionally you can pass a token to verify the user with. An ui token
 * will only give the user read access, while an api token will give the
 * user read, write access.
 *
 * @param book_id - The ID of the book.
 * @param user_id - The ID of the user.
 * @param ui_token_id - Optional token id for authentication using ui tokens.
 * @param bearer_token - Optional token for authentication Only used for
 * api tokens.
 * @returns The permissions of the user for the book.
 */
export async function getPerms(
    book_id: number,
    {
        user_id,
        ui_token_id,
        bearer_token,
    }: {
        user_id?: number;
        ui_token_id?: number | number[];
        bearer_token?: string;
    },
): Promise<{
    resolved: ParsedPermissionACLData;
    all: Array<ParsedPermissionACLData>;
}> {
    // Accumulate all given permissions
    const promises: Array<Promise<ParsedPermissionACLData>> = [];
    if (bearer_token) {
        promises.push(perms_by_bearer_token(book_id, bearer_token));
    }

    if (ui_token_id) {
        if (Array.isArray(ui_token_id)) {
            promises.push(
                ...ui_token_id.map((id) => perms_by_ui_token(book_id, id)),
            );
        } else {
            promises.push(perms_by_ui_token(book_id, ui_token_id));
        }
    }

    if (user_id) {
        promises.push(perms_by_user(book_id, user_id));
    }

    // Combine/Reduce perms
    const permsArray = await Promise.all(promises);
    const resolved = permsArray.reduce(
        (prev: ParsedPermissionACLData, cur: ParsedPermissionACLData) => {
            let denied_reason = prev.denied_reason || "";
            denied_reason += cur.denied_reason ? `; ${cur.denied_reason}` : "";

            return {
                ...prev,
                id: -2,
                auth_method: "combined",
                resource_id: book_id,
                resource_type: RESOURCE_BOOK as Resource,
                pRead: prev.pRead || cur.pRead,
                pACL: prev.pACL || cur.pACL,
                pWrite: prev.pWrite || cur.pWrite,
                pDelete: prev.pDelete || cur.pDelete,
                denied_reason: denied_reason,
            };
        },
        accessDenied(book_id, "unknown", "No authentication method provided"),
    );

    if (resolved.pRead) {
        resolved.denied_reason = undefined;
    }

    return {
        resolved,
        all: permsArray,
    };
}

export function accessDenied(
    book_id: number,
    type?: AuthMethod,
    reason?: string,
): ParsedPermissionACLData {
    return {
        id: -1,
        entity_id: -1,
        entity_type: ENTITY_TOKEN,
        resource_id: book_id,
        resource_type: RESOURCE_BOOK,
        pACL: false,
        pDelete: false,
        pRead: false,
        pWrite: false,
        auth_method: type || "unknown",
        denied_reason: reason,
    };
}

async function perms_by_user(
    book_id: number,
    user_id: number,
): Promise<ParsedPermissionACLData> {
    const perms = await service.permission
        .getByUserAndBookReduced(user_id, book_id)
        .catch(() => {
            return accessDenied(
                book_id,
                "user",
                "User is not allowed to access this book",
            );
        });

    return {
        ...perms,
        auth_method: "user",
    };
}

export async function perms_by_ui_token(
    book_id: number,
    token_id: number,
): Promise<ParsedPermissionACLData> {
    let tokenData: Required<UITokenData>;
    try {
        tokenData = (await service.token.getById(
            token_id,
        )) as Required<UITokenData>;
    } catch (_error) {
        return accessDenied(book_id, "ui_token", "Invalid token");
    }

    if (tokenData.type !== "ui") {
        return accessDenied(book_id, "ui_token", "Invalid token type");
    }

    if (tokenData.book_id !== book_id) {
        return accessDenied(
            book_id,
            "ui_token",
            "Token is not allowed for this book",
        );
    }

    // Should always have an expires_at!
    if (tokenData.expires_at && tokenData.expires_at < new Date()) {
        return accessDenied(book_id, "ui_token", "Token has expired");
    }

    return {
        id: tokenData.id,
        entity_id: tokenData.created_by,
        entity_type: ENTITY_TOKEN,
        resource_id: tokenData.book_id,
        resource_type: RESOURCE_BOOK,
        pACL: false,
        pDelete: false,
        pWrite: false,
        pRead: true,
        auth_method: "ui_token",
        expires_at: tokenData.expires_at || undefined,
    };
}

export async function perms_by_bearer_token(
    book_id: number,
    token: string,
): Promise<ParsedPermissionACLData> {
    let tokenData: Required<APITokenData>;
    try {
        // check if token can be verified
        tokenData = await verifyAPIToken(token, book_id);
    } catch (_error) {
        return accessDenied(book_id, "bearer", "Invalid token");
    }

    if (tokenData.book_id !== book_id) {
        return accessDenied(
            book_id,
            "bearer",
            "Token is not allowed for this book",
        );
    }

    if (tokenData.expires_at && tokenData.expires_at < new Date()) {
        return accessDenied(book_id, "bearer", "Token has expired");
    }

    return {
        id: tokenData.id,
        entity_id: tokenData.created_by,
        entity_type: ENTITY_TOKEN,
        resource_id: tokenData.book_id,
        resource_type: RESOURCE_BOOK,
        pACL: false,
        pDelete: false,
        pWrite: true,
        pRead: true,
        auth_method: "bearer",
        expires_at: tokenData.expires_at || undefined,
    };
}
