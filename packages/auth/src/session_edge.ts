import { IncomingMessage, ServerResponse } from "http";
import { getIronSession, SessionOptions } from "iron-session";

import { GLOBALS } from "@snip/config/server/globals";

import { SnipSessionData } from "./types";

export const sessionOptions: Omit<SessionOptions, "password"> = {
    cookieName: "snip_labbook",
    // secure: true should be used in production (HTTPS) but can't be used in development (HTTP)
    cookieOptions: {
        domain: GLOBALS.hostname,
        secure: true,
        httpOnly: true,
    },
};

/** For the edge runtime we need a
 * different implementation of getSession
 * as path and fs are not available!
 */
export async function getSession(
    arg1: IncomingMessage | Request,
    arg2?: Response | ServerResponse,
): Promise<SnipSessionData> {
    const secret = process.env.SNIP_SESSION_SECRET;
    if (!secret) {
        throw new Error("SNIP_SESSION_SECRET is not defined");
    }

    if (arg2) {
        // If the second argument is present, assume the first one is Request and the second one is Response
        const req = arg1 as IncomingMessage;
        const res = arg2 as Response;
        const session = await getIronSession<SnipSessionData>(req, res, {
            ...sessionOptions,
            password: secret,
        });
        return session;
    } else {
        // If the second argument is not present, assume the first one is cookies
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const cookies = arg1 as any;
        const session = await getIronSession<SnipSessionData>(cookies, {
            ...sessionOptions,
            password: secret,
        });
        return session;
    }
}
