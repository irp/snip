import { IncomingMessage, ServerResponse } from "http";
import { getIronSession, sealData, SessionOptions } from "iron-session";

import { config } from "@snip/config/server";

import { SessionData, SnipSessionData } from "./types";

export const sessionOptions: Omit<SessionOptions, "password"> = {
    cookieName: "snip_labbook",
    cookieOptions: {
        domain: config.hostname,
        secure: true,
        httpOnly: true,
    },
};

/** Get session in the route handlers
 * @param req
 * @param res
 */
export async function getSession(
    req: IncomingMessage | Request,
    res: Response | ServerResponse,
): Promise<SnipSessionData>;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function getSession(cookies: any): Promise<SnipSessionData>;

// Combined implementation of getSession
export async function getSession(
    arg1: IncomingMessage | Request,
    arg2?: Response | ServerResponse,
): Promise<SnipSessionData> {
    if (arg2) {
        // If the second argument is present, assume the first one is Request and the second one is Response
        const req = arg1 as IncomingMessage;
        const res = arg2 as Response;
        const session = await getIronSession<SnipSessionData>(req, res, {
            ...sessionOptions,
            password: config.secrets.parse().session,
        });
        return session;
    } else {
        // If the second argument is not present, assume the first one is cookies
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const cookies = arg1 as any;
        const session = await getIronSession<SnipSessionData>(cookies, {
            ...sessionOptions,
            password: config.secrets.parse().session,
        });
        return session;
    }
}

/** Get the session string for an internal api request
 * @param data
 *
 * For example useful for an internal api request
 * ```typescript
 * const session = await getCookieString({ user_api: "socket" });
 * fetch("http://localhost:4000/api/v1/snip", {
 *    headers: {
 *       "Content-Type": "application/json",
 *       "cookie": `snip_labbook=${session}`
 *   }
 * });
 */
export async function getCookieString(data: SessionData): Promise<string> {
    return await sealData(data, {
        ...sessionOptions,
        password: config.secrets.parse().session,
    }).then((data) => {
        return `${sessionOptions.cookieName}=${data}`;
    });
}
