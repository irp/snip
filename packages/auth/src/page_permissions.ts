import service from "@snip/database";

import { getPerms, ParsedPermissionACLData } from "./book_permissions";

/**
 * Get the users permission for a given page
 * this function can e.g. be used to check if a user
 * has read access to a specific page without needing the book
 * id.
 *
 * Optionally you can pass a token to verify the user with. An ui token
 * will only give the user read access, while an api token will give the
 * user read, write access.
 *
 * @param page_id - The ID of the book.
 * @param user_id - The ID of the user.
 * @param ui_token_id - Optional token id for authentication using ui tokens.
 * @param bearer_token - Optional token for authentication Only used for
 * api tokens.
 * @returns The permissions of the user for the book.
 */
export async function getPermsPage(
    page_id: number,
    props: {
        user_id?: number;
        ui_token_id?: number | number[];
        bearer_token?: string;
    },
): Promise<{
    resolved: ParsedPermissionACLData;
    all: Array<ParsedPermissionACLData>;
}> {
    const book_id = await service.page
        .getById(page_id)
        .then((page) => page.book_id);

    return await getPerms(book_id, props);
}
