import { defineConfig } from 'tsup';

import { config } from "@snip/tsup-config/base";


export default defineConfig([
    {
        ...config,
        dts: true,
        sourcemap: true,
        clean: true,
        entry: [
            "./src/**/*.ts", "!./src/**/__tests__/**"
        ],
    }
]);