import { PageDataInsert, PageDataRet } from "@snip/database/types";
import { ArraySnip } from "@snip/snips/general/array";
import { BaseSnip } from "@snip/snips/general/base";

import { render_background_by_id } from "./backgrounds";
import { MinimalPage, Size } from "./types";

/** A small local page implementation without sockets or any communication. Can render a predefined pageData
 * no dynamic content is used. See extended pages in e.g. fullstack for dynamic content
 * using a socket connection.
 */
export class LocalPage extends MinimalPage {
    size: Size = {
        width: 1400,
        height: 2000,
    };

    data: PageDataRet;
    snips: Array<BaseSnip>;

    constructor({
        id,
        book_id,
        page_number,
        created = new Date(),
        last_updated = new Date(),
        snips = [],
        ...props
    }: PageDataRet & { snips?: Array<BaseSnip> }) {
        super();
        this.data = {
            id,
            book_id,
            page_number,
            created,
            last_updated,
            ...props,
        };
        this.snips = snips;
    }
    static from_partial(
        data: Partial<PageDataInsert> & { snips?: Array<BaseSnip> },
    ): LocalPage {
        return new LocalPage({
            id: data.id ?? -1,
            book_id: data.book_id ?? -1,
            page_number: data.page_number ?? -1,
            created: data.created ?? new Date(),
            last_updated: data.last_updated ?? new Date(),
            background_type_id: data.background_type_id ?? 0,
            referenced_page_id: data.referenced_page_id ?? null,
            referenced_book_id: null,
            snips: data.snips ?? [],
        });
    }

    public to_data(): PageDataRet {
        return this.data;
    }

    public set_snips(snips: BaseSnip[]): void {
        this.snips = snips;
    }

    public add_snip(snip: BaseSnip): void {
        /** If array snip remove all
         * nested snips and add them to the arraysnip
         */
        if (snip instanceof ArraySnip) {
            snip.snips.forEach((s) => {
                this.remove_snip(s.id);
            });
        }

        /** Get all array snips and check if they contain the snip
         */
        const array_snips = this.snips.filter(
            (s) => s instanceof ArraySnip,
        ) as ArraySnip[];
        for (const array_snip of array_snips) {
            for (let s of array_snip.snips) {
                if (s.id === snip.id) {
                    s = snip;
                    return;
                }
            }
        }

        this.snips.push(snip);
    }

    public remove_snip(snip_id: number): boolean {
        const index = this.snips.findIndex((s) => s.id === snip_id);
        if (index !== -1) {
            this.snips.splice(index, 1);
            return true;
        }
        return false;
    }

    /**
     * Get a snip at a position using their polygon,
     * filters doodles by default.
     *
     * @param coords
     * @param filter
     * @returns
     */
    public get_snip_at(
        coords: [number, number],
        snip_filter = (snip: BaseSnip) => snip.type != "doodle",
    ): BaseSnip | undefined {
        // Check if snips are loaded
        if (this.snips === undefined) {
            return undefined;
        }

        // Filter all snips
        // reverse seems correct
        const snips = this.snips.filter(snip_filter).reverse();

        return snips.find((s) => s.polygon.isInside(coords));
    }

    public get_snip_by_id(snip_id: number): BaseSnip | null {
        const snip = this.snips.find((s) => s.id === snip_id);
        if (snip) {
            return snip;
        }
        return null;
    }

    public update_snip(snip: BaseSnip): void {
        const index = this.snips.findIndex((s) => s.id === snip.id);
        if (index !== -1) {
            this.snips[index] = snip;
        } else {
            this.add_snip(snip);
        }
    }

    public render(
        ctx: CanvasRenderingContext2D | OffscreenCanvasRenderingContext2D,
    ): void {
        // Render background
        render_background_by_id(this.data.background_type_id, ctx, this.size);

        const sorted_snips = this.snips.sort(
            (a, b) => a.created.getTime() - b.created.getTime(),
        );

        for (const snip of sorted_snips) {
            snip.render(ctx);
        }
    }
}
