import { type BaseSnip } from "@snip/snips/general/base";

export type Surface = HTMLCanvasElement | OffscreenCanvas;
export type RenderContext =
    | CanvasRenderingContext2D
    | OffscreenCanvasRenderingContext2D;

export const debug = (...args: unknown[]) => {
    console.debug("[Render]", ...args);
};

export type Size = {
    width: number;
    height: number;
};

export abstract class MinimalPage {
    //max width and height of a page
    abstract size: Size;

    /** Render the page, clipping is applied automatically
     */
    abstract render(
        ctx: RenderContext,
        conditionalRender?: ConditionalRender,
    ): void;

    /**
     * Renders the page with optional conditional rendering and page rendering.
     *
     * This function sets up the rendering context with shadows and fill style,
     * applies clipping to ensure the rendering is contained within the page dimensions,
     * and then calls the render method to perform the actual rendering of the content.
     *
     * @param ctx - The rendering context, either a CanvasRenderingContext2D or OffscreenCanvasRenderingContext2D.
     * @param conditionalRender - An optional ConditionalRender parameter that could affect the rendering logic.
     * @param renderPage - A boolean flag indicating whether the page should be rendered. Defaults to true.
     */
    _render(
        ctx: RenderContext,
        conditionalRender?: ConditionalRender,
        renderFkt?: (
            ctx: RenderContext,
            conditionalRender?: ConditionalRender,
        ) => void,
    ) {
        ctx.save();
        ctx.shadowColor = "rgba(0, 0, 0, 0.2)";
        ctx.fillStyle = "white";
        ctx.shadowBlur = 15;
        ctx.shadowOffsetX = 2;
        ctx.shadowOffsetY = 2;
        ctx.fillRect(0, 0, this.size.width, this.size.height);
        ctx.restore();

        // Apply clipping
        ctx.save();
        ctx.beginPath();
        ctx.rect(0, 0, this.size.width, this.size.height);
        ctx.clip();

        // render page
        if (renderFkt) {
            renderFkt(ctx, conditionalRender);
        } else {
            this.render(ctx, conditionalRender);
        }

        // Cleanup
        ctx.restore();
    }
}

/** An render condition can be used to
 * apply specific styling to snips
 * based on a given condition.
 */
export abstract class ConditionalRender {
    abstract condition(snip: BaseSnip): boolean;
    abstract pre_render(ctx: RenderContext): void;
    abstract post_render(ctx: RenderContext): void;

    render(ctx: RenderContext, snip: BaseSnip) {
        if (this.condition(snip)) {
            this.pre_render(ctx);
            snip.render(ctx);
            this.post_render(ctx);
        } else {
            snip.render(ctx);
        }
    }

    /** Overrides for default on pointer events */
    public on_pointerdown_worker?(ctx: RenderContext, snip: BaseSnip): void;
    public on_pointerenter_worker?(ctx: RenderContext, snip: BaseSnip): void;
    public on_pointerleave_worker?(ctx: RenderContext, snip: BaseSnip): void;
    public on_dblclick_worker?(ctx: RenderContext, snip: BaseSnip): void;
}

export interface RenderFunction {
    // Callback function that renders the item
    (ctx: RenderContext): void;

    // Boolean that implies that the render function
    // is dirty and needs to be re-rendered
    dirty?: boolean;
    is_dirty?: () => boolean;
}
