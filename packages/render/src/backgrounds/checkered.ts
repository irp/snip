import type { RenderContext, Size } from "../types";

/** Renders a checkered background onto a canvas given a
 * ctx.
 *
 * @param ctx The canvas context to render onto.
 * @param width The width of a box in mm
 * @param height The height of a box in mm.
 *
 */
export function render_checkered(
    ctx: RenderContext,
    pageSize: Size,
    lwidth = 4,
    lheight = 4,
) {
    // Scale to db coordinates
    const scale = pageSize.height / pageSize.width;

    // Assuming it is an a4 page the height is 210mm
    const mm = pageSize.height / 210;

    const delta_x = lwidth * mm;
    const delta_y = lheight * mm;
    const padding_x = (delta_x / 2) * scale;
    const padding_y = (delta_y / 2) * scale;

    ctx.strokeStyle = "#d3d3d3"; // Light grey
    ctx.lineWidth = 1;

    //Determine how many boxes can fit on the canvas
    const num_vertical = Math.floor((pageSize.width - padding_x) / delta_x);
    const num_horizontal = Math.floor((pageSize.height - padding_y) / delta_y);

    // Calc starting point such that it is centered
    const start_x = (pageSize.width - num_vertical * delta_x) / 2;
    const start_y = (pageSize.height - num_horizontal * delta_y) / 2;

    // Draw vertical lines
    for (let i = 0; i < num_vertical + 1; i++) {
        ctx.beginPath();
        ctx.moveTo(start_x + i * delta_x, start_y);
        ctx.lineTo(start_x + i * delta_x, pageSize.height - start_y);
        ctx.stroke();
    }

    // Draw horizontal lines
    for (let i = 0; i < num_horizontal + 1; i++) {
        ctx.beginPath();
        ctx.moveTo(start_x, start_y + i * delta_y);
        ctx.lineTo(pageSize.width - start_x, start_y + i * delta_y);
        ctx.stroke();
    }

    return;
}
