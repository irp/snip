export const cm2px = (cm: number) => {
    if (
        typeof WorkerGlobalScope !== "undefined" &&
        self instanceof WorkerGlobalScope
    ) {
        return (cm * 96) / 2.54;
    }
    return (cm / window.devicePixelRatio / 2.54) * 96;
};
