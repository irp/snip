import { Canvas } from "skia-canvas";
import { render_background_by_id } from "..";

describe("backgrounds", () => {
    test.each([1, 2, 3, undefined, null])(
        "should render background %i",
        (id) => {
            // Create a mock canvas context
            const canvas = new Canvas(800, 600);
            const ctx = canvas.getContext("2d");

            // Set up the canvas size
            canvas.width = 800;
            canvas.height = 600;

            // Call the render_checkered function
            render_background_by_id(
                id,
                ctx as unknown as CanvasRenderingContext2D,
                {
                    width: 100,
                    height: 100,
                },
            );

            // Verify the checkered pattern is rendered
            // You can add your own assertions here based on your expected output
            // For example, you can check the color of specific pixels or the number of lines drawn

            // Cleanup
            ctx.clearRect(0, 0, canvas.width, canvas.height);
        },
    );
});
