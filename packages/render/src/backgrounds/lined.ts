import type { RenderContext, Size } from "../types";

export function render_lined(ctx: RenderContext, pageSize: Size, lwidth = 7) {
    // Scale to db coordinates
    const scale = pageSize.height / pageSize.width;

    // Assuming it is an a4 page the height is 210mm
    const mm = pageSize.height / 210;

    // Padding and line delta
    const delta_y = lwidth * mm;
    //const padding_x = (delta_y / 2) * scale;
    const padding_y = (delta_y / 2) * scale;

    // Visual properties
    ctx.strokeStyle = "#d3d3d3"; // Light grey
    ctx.lineWidth = 1;

    // Determine how many lines can fit on the canvas
    const num_horizontal = Math.floor((pageSize.height - padding_y) / delta_y);

    // Calc starting point such that it is centered
    const start_x = 0;
    const start_y = (pageSize.height - num_horizontal * delta_y) / 2;

    // Draw horizontal lines
    for (let i = 0; i < num_horizontal + 1; i++) {
        ctx.beginPath();
        ctx.moveTo(start_x, start_y + i * delta_y);
        ctx.lineTo(pageSize.width - start_x, start_y + i * delta_y);
        ctx.stroke();
    }
    return;
}
