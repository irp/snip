import { background_types } from "@snip/database/sql.typings";

import type { RenderContext, Size } from "../types";
import { render_checkered } from "./checkered";
import { render_lined } from "./lined";

export function render_background_by_id(
    id: number | null | undefined | "none",
    ctx: RenderContext,
    pageSize: Size,
) {
    switch (id) {
        case null:
        case undefined:
        case background_types["blank"]:
            // White page
            ctx.fillStyle = "white";
            ctx.fillRect(0, 0, pageSize.width, pageSize.height);
            break;
        case background_types["checkered 4mm x 4mm"]:
            render_checkered(ctx, pageSize, 4, 4);
            break;
        case background_types["lines 7mm" as never]:
        case background_types["lined 7mm" as never]:
            render_lined(ctx, pageSize, 7);
            break;
        case "none":
            break;
        default:
            console.error("Invalid background id: " + id);
            break;
    }
}
