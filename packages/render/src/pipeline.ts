import { AbstractViewport } from "viewport";

import Queue from "@snip/common/queue";
import { type BaseSnip } from "@snip/snips/general/base";

import {
    ConditionalRender,
    debug,
    MinimalPage,
    RenderContext,
    RenderFunction,
    Surface,
} from "./types";

export class Pipeline {
    // Items that are drawn are removed from the queue
    private queue = new Queue<RenderFunction>();

    // Items that are drawn every time the viewport changes
    // i.e. the size or the viewmatrix
    private stack = new Set<RenderFunction>();

    private surface: Surface;

    private abortController = new AbortController();

    public viewport: AbstractViewport;

    // TODO: We might want to move this from the pipeline
    public condition?: ConditionalRender;
    name: string;

    constructor(canvas: Surface, viewport: AbstractViewport, name?: string) {
        this.surface = canvas;
        this.viewport = viewport;
        this.name = name || "Pipe" + Math.round(Math.random() * 1000);
    }

    private forceRedraw = true;
    public start() {
        this.abortController = new AbortController();

        let lastTime = performance.now();
        let frameCount = 0;
        const ctx = this.surface.getContext("2d") as CanvasRenderingContext2D;

        // We have to opt for a draw loop
        // as firefox has some problems with dynamic
        // updates to the canvas.
        const drawLoop = (currentTime: number) => {
            if (lastTime + 3000 < currentTime) {
                debug(
                    this.name,
                    "FPS:",
                    Math.round((frameCount * 1000) / (currentTime - lastTime)),
                );

                lastTime = currentTime;
                frameCount = 0;
            }

            // We check if viewport values changed using a simple xor hash
            const matrixChanged = this.viewport.buffer.hasViewMatrixChanged();
            const sizeChanged =
                this.viewport.buffer.hasSurfaceSizeChanged() ||
                this.viewport.buffer.hasPageSizeChanged();

            // Check if render functions are dirty
            for (const fn of this.stack) {
                if (fn.dirty || fn.is_dirty?.()) {
                    this.forceRedraw = true;
                    break;
                }
            }

            // Set new size if needed
            if (this.forceRedraw || sizeChanged) {
                const new_size = this.viewport.buffer.surfaceSizeView;
                // Set new width
                this.surface.width = new_size[0]!;
                this.surface.height = new_size[1]!;
            }

            // Set current transform
            ctx.setTransform(this.viewport.getMatrix());

            // Redraw if needed
            if (this.forceRedraw || matrixChanged || sizeChanged) {
                this.clearCanvas(ctx);
                for (const fn of this.stack) {
                    fn(ctx);
                }
                this.forceRedraw = false; // Reset the force redraw flag
            }

            let item;
            while ((item = this.queue.dequeue()) !== undefined) {
                item(ctx);
            }

            if (!this.abortController.signal?.aborted) {
                globalThis.requestAnimationFrame(drawLoop);
            }
            frameCount++;
        };

        globalThis.requestAnimationFrame(drawLoop);
    }

    public stop() {
        this.abortController.abort();
    }

    /**
     * Renders a single function once using the queue.
     *
     * @param fn - The function to be rendered. This function takes a RenderContext as its parameter.
     * @param timeout - The timeout duration in milliseconds. If the rendering does not complete within this time, the promise will be rejected.
     * @returns A promise that resolves when the rendering is complete, or rejects if the rendering times out.
     */
    public async renderOnce(
        fn: RenderFunction,
        timeout: number = 5000,
    ): Promise<void> {
        return await new Promise<void>((resolve, reject) => {
            const timeoutT = setTimeout(
                () => reject(new Error("Render failed! Timeout!")),
                timeout,
            );
            this.queue.enqueue((ctx) => {
                fn(ctx);
                clearTimeout(timeoutT);
                resolve();
            });
        });
    }

    /**
     * Renders a function using the stack and then executes it once.
     *
     * @param fn - The function to be rendered. This function takes a RenderContext as its parameter.
     * @param timeout - The timeout duration in milliseconds. If the rendering does not complete within this time, the promise will be rejected.
     * @returns A promise that resolves when the rendering is complete, or rejects if the rendering times out.
     */
    public async render(fn: RenderFunction, timeout: number = 5000) {
        if (this.stack.has(fn)) return;
        this.stack.add(fn);
        return this.renderOnce(fn, timeout);
    }

    /**
     * Removes a rendering function from the stack and forces a re-render of the stack.
     *
     * @param fn - The rendering function to be removed from the stack. This function takes a RenderContext as its parameter.
     * @throws {Error} If the function is not found in the render stack.
     * @returns A promise that resolves when the stack has been re-rendered.
     */
    public async removeRender(fn: RenderFunction) {
        if (!this.stack.has(fn)) {
            throw new Error("Function not found in render stack!");
        }
        this.stack.delete(fn);
        return this.rerenderStack();
    }

    /**
     * Forces a re-render of the stack by setting the forceRedraw flag to true.
     * This will trigger a redraw on the next draw loop iteration.
     */
    public rerenderStack() {
        this.forceRedraw = true;
    }

    protected clearCanvas(ctx: RenderContext) {
        ctx.save();
        // Use the identity matrix while clearing the canvas
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        // Restore the transform
        ctx.restore();
    }

    /** Perform a full render of a page, this clears
     * the page and then fully redraws it.
     */
    public async renderPage(page: MinimalPage, timeout: number = 5000) {
        //Clear stack
        this.stack.clear();

        return this.render((ctx) => {
            this.clearCanvas(ctx);
            page._render(ctx, this.condition);
        }, timeout);
    }

    /** Render a single snip once
     */
    public async renderSnipOnce(snip: BaseSnip, timeout: number = 5000) {
        return this.renderOnce((ctx) => {
            if (this.condition && this.condition.condition(snip)) {
                this.condition.render(ctx, snip);
            } else {
                snip.render(ctx);
            }
        }, timeout);
    }
}
