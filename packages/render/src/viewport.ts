import { Size } from "./types";

export abstract class AbstractViewport {
    readonly buffer: SharedDataBuffer;

    constructor(buffer: SharedDataBuffer) {
        this.buffer = buffer;
    }

    /** Get the current view matrix */
    getMatrix() {
        // need to cast to float64 to copy the array
        return DOMMatrixReadOnly.fromFloat64Array(
            new Float64Array(this.buffer.viewMatrixView),
        );
    }

    /** Get the current surface size */
    getSurfaceSize(): Size {
        return {
            width: this.buffer.surfaceSizeView[0]!,
            height: this.buffer.surfaceSizeView[1]!,
        };
    }

    /**
     * Converts coordinates from the current view's coordinate system
     * to the page's coordinate system.
     *
     * This method uses the inverse of the current view matrix to transform
     * the given coordinates.
     *
     * @param pos - The [x, y] coordinates in the view's coordinate system.
     * @returns A tuple containing the [x, y] coordinates in the page's coordinate system.
     */
    getPosInPageCoords(pos: [number, number]): [number, number] {
        const invertedMatrix = this.getMatrix().inverse();
        return [
            pos[0] * invertedMatrix.a +
                pos[1] * invertedMatrix.c +
                invertedMatrix.e,
            pos[0] * invertedMatrix.b +
                pos[1] * invertedMatrix.d +
                invertedMatrix.f,
        ];
    }

    /**
     * Converts relative coordinates from the current view's coordinate system
     * to the page's coordinate system. E.g. width and height or other distances
     *
     * This method uses the inverse of the current view matrix to transform
     * the given coordinates.
     *
     * @param pos - The [x, y] coordinates in the view's coordinate system.
     * @returns A tuple containing the [x, y] coordinates in the page's coordinate system.
     */
    getRelativeInPageCoords(pos: [number, number]): [number, number] {
        const invertedMatrix = this.getMatrix().inverse();
        const scaleX = invertedMatrix.a;
        const scaleY = invertedMatrix.d;
        return [pos[0] * scaleX, pos[1] * scaleY];
    }

    /**
     * Converts coordinates from the page's coordinate system
     * to the current view's coordinate system (canvas).
     *
     * This method uses the current view matrix to transform
     * the given coordinates.
     *
     * @param x - The x coordinate in the page's coordinate system.
     * @param y - The y coordinate in the page's coordinate system.
     * @returns A tuple containing the [x, y] coordinates in the view's coordinate system.
     */
    getPosInViewCoords(pos: [number, number]): [number, number] {
        const matrix = this.getMatrix();
        return [
            pos[0] * matrix.a + pos[1] * matrix.c + matrix.e,
            pos[0] * matrix.b + pos[1] * matrix.d + matrix.f,
        ];
    }

    /**
     * Converts relative coordinates from the current page's coordinate system
     * to the views's coordinate system. E.g. width and height or other distances
     *
     * This method uses the current view matrix to transform
     * the given coordinates.
     *
     * @param pos - The [x, y] coordinates in the pages's coordinate system.
     * @returns A tuple containing the [x, y] coordinates in the page's coordinate system.
     */
    getRelativeInViewCoords(pos: [number, number]): [number, number] {
        const matrix = this.getMatrix();
        const scaleX = matrix.a;
        const scaleY = matrix.d;
        return [pos[0] * scaleX, pos[1] * scaleY];
    }
}

class SharedDataBuffer {
    public readonly buffer: SharedArrayBuffer;

    viewMatrixView: Float64Array;
    surfaceSizeView: Float64Array;
    pageSizeView: Float64Array;

    /**
     * Initializes a new instance of the SharedDataBuffer class.
     *
     * This constructor sets up a SharedArrayBuffer to facilitate shared state
     * between the main thread and a worker (if used). The shared state comprises
     * a redraw indicator, a view matrix, and surface size data. The buffer is divided
     * and views are created for these different pieces of shared data.
     *
     * The layout of the buffer is as follows:
     * - The next 16 Float64 elements (128 bytes) are reserved for the DOMMatrix.
     * - The last 2 Float64 elements (16 bytes) are reserved for surface width and height.
     * - The first 2 bytes (Int8Array) are reserved for the redraw indicator.
     *
     * If a buffer is provided it is used, else a new one is created.
     */
    constructor(buffer?: SharedArrayBuffer) {
        // Size for the different shared data
        const len_dommatrix = 16 * Float64Array.BYTES_PER_ELEMENT;
        const len_size = 2 * Float64Array.BYTES_PER_ELEMENT;

        // create buffer
        if (buffer) {
            this.buffer = buffer;
        } else {
            this.buffer = new SharedArrayBuffer(len_dommatrix + len_size * 2);
        }

        // Define views on the buffer
        this.viewMatrixView = new Float64Array(this.buffer, 0, 16);
        this.surfaceSizeView = new Float64Array(this.buffer, len_dommatrix, 2);
        this.pageSizeView = new Float64Array(
            this.buffer,
            len_dommatrix + len_size,
            2,
        );

        // Initialize the hashes
        this.previousViewMatrixHash = this.computeXorHash(0, 16);
        this.previousSurfaceSizeHash = this.computeXorHash(16, 2);
        this.previousPageSizeHash = this.computeXorHash(18, 2);
    }

    private computeXorHash(start: number, length: number): number {
        const view = new DataView(this.buffer);
        let hash = 0;

        for (let i = start; i < start + length; i++) {
            const lowBits = view.getUint32(i * 8, true);
            const highBits = view.getUint32(i * 8 + 4, true);

            hash ^= lowBits;
            hash ^= highBits;
        }

        return hash;
    }

    previousViewMatrixHash: number;
    public hasViewMatrixChanged(): boolean {
        const currentHash = this.computeXorHash(0, 16);
        if (this.previousViewMatrixHash !== currentHash) {
            this.previousViewMatrixHash = currentHash;
            return true;
        }
        return false;
    }

    previousSurfaceSizeHash: number;
    public hasSurfaceSizeChanged(): boolean {
        const currentHash = this.computeXorHash(16, 2);
        if (this.previousSurfaceSizeHash !== currentHash) {
            this.previousSurfaceSizeHash = currentHash;
            return true;
        }
        return false;
    }

    previousPageSizeHash: number;
    public hasPageSizeChanged(): boolean {
        const currentHash = this.computeXorHash(18, 2);
        if (this.previousPageSizeHash !== currentHash) {
            this.previousPageSizeHash = currentHash;
            return true;
        }
        return false;
    }
}

/** Represents the current view onto the page
 * allows to center, resize and lock the screen
 * to the current page.
 */
export class ViewPort extends AbstractViewport {
    private viewMatrix: DOMMatrix;

    private surfaceSize: Size;
    private pageSize: Size;

    /**
     * Initializes a new instance of the ViewPort class.
     *
     * @param {Size} surface - The initial size of the surface.
     * @param {Size} page - The initial size of the page.
     * @param {SharedArrayBuffer} [sharedBuffer] - An optional shared buffer for worker communication.
     */
    constructor(surface?: Size, page?: Size) {
        const buffer = new SharedDataBuffer();
        super(buffer);
        this.viewMatrix = new DOMMatrix();

        this.surfaceSize = surface || { width: 1, height: 1 };
        this.pageSize = page || { width: 1, height: 1 };

        this._updateSharedMatrix();
        this._updateSize();
    }

    /** Resize the current surface,
     * has to be called to update the size
     * in the center and fit functions.
     *
     */
    public resize(width: number, height: number) {
        this.surfaceSize = {
            width,
            height,
        };
        this._updateSize();
    }

    /** Resize the page
     */
    public resizePage(width: number, height: number) {
        this.pageSize = {
            width,
            height,
        };
        this._updateSize();
    }

    getSurfaceSize(): Size {
        return this.surfaceSize;
    }
    getSurfaceSizeArray(): Float64Array {
        return this.buffer.surfaceSizeView;
    }
    getPageSize(): Size {
        return this.pageSize;
    }

    /** Center the page in the viewport, keeps zoom levels
     * constant
     *
     * @param c_x: Center the x coords of the page in the viewport
     * @param c_y: Center the y coords
     */
    public centerPage(c_x = true, c_y = true) {
        if (c_x) {
            this.viewMatrix.e =
                (this.surfaceSize.width -
                    this.viewMatrix.a * this.pageSize.width) /
                2;
        }
        if (c_y) {
            this.viewMatrix.f =
                (this.surfaceSize.height -
                    this.viewMatrix.d * this.pageSize.height) /
                2;
        }
        this._updateSharedMatrix();
    }

    /** Fit a page in the viewport this modifies position and
     * current zoom values to fit the page into the viewport
     *
     * @param fit_w: Fit the width
     * @param fit_h: Fit the height
     * @param padding: Optional padding to keep some space around the page
     */
    public fitPage(
        fit_w: boolean = true,
        fit_h: boolean = true,
        padding?: [number, number],
    ) {
        if (!padding) {
            padding = [0, 0];
        }

        const surfaceWidth = this.surfaceSize.width - padding[0];
        const surfaceHeight = this.surfaceSize.height - padding[1];

        // Compute new scale
        let scale = 1;
        if (fit_w && fit_h) {
            scale = Math.min(
                surfaceWidth / this.pageSize.width,
                surfaceHeight / this.pageSize.height,
            );
        } else if (fit_w) {
            scale = surfaceWidth / this.pageSize.width;
        } else if (fit_h) {
            scale = surfaceHeight / this.pageSize.height;
        }

        // Compute fit position
        const newWidth = this.pageSize.width * scale;
        const newHeight = this.pageSize.height * scale;

        // compute center positions
        const overhang_y = surfaceHeight - newHeight;
        const overhang_x = surfaceWidth - newWidth;

        // translate and account for padding
        const tx = overhang_x / 2 + padding[0] / 2;
        const ty = overhang_y / 2 + padding[1] / 2;

        // same x and y scale
        this.viewMatrix = new DOMMatrix().translate(tx, ty).scale(scale, scale);
        this._updateSharedMatrix();
    }

    /** Zoom is relative to the height of the viewport
     * i.e. if the page height fits it is zoom=1
     *
     * @param z: The zoom value
     * @param origin - The [x, y] coordinates in the current view's coordinate system where the scaling transformation anchors.
     */
    public setZoom(z: number, origin?: [number, number]) {
        if (!origin) {
            origin = [this.surfaceSize.width / 2, this.surfaceSize.height / 2];
        }
        origin = this.getPosInPageCoords(origin);

        const surfaceHeight = this.surfaceSize.height;
        const baseline = surfaceHeight / this.pageSize.height;

        // Calculate the new scale factor
        const newScale = z * baseline;

        // Calculate the scaling factor relative to the current zoom
        const scaleFactor = newScale / this.viewMatrix.a;

        // Apply scaling keeping the centerX and centerY as fixed points

        this.viewMatrix.scaleSelf(
            scaleFactor,
            scaleFactor,
            undefined,
            ...origin,
        );
        this._updateSharedMatrix();
    }

    public getZoom(): number {
        const surfaceHeight = this.surfaceSize.height;
        const baseline = surfaceHeight / this.pageSize.height;

        // Assuming viewMatrix is uniform
        const scale = this.viewMatrix.a; // Returns the scale factor

        return scale / baseline;
    }

    /**
     * Scales the viewing area by the specified scale factor, anchoring the scaling
     * transformation at a given origin point.
     *
     * The origin point is provided in the current coordinate system of the view
     * and will be converted to the page coordinate system before applying the scaling.
     *
     * @param scale - The scaling factor. A value greater than 1 will zoom in, and a value less than 1 will zoom out.
     * @param origin - The [x, y] coordinates in the current view's coordinate system where the scaling transformation anchors.
     */
    public scale(scale: number, origin: [number, number]) {
        origin = this.getPosInPageCoords(origin);

        this.viewMatrix.scaleSelf(scale, scale, undefined, ...origin);
        this._updateSharedMatrix();
    }

    /**
     * Translates the viewing area by the specified x and y distances.
     *
     * The translation is in the current coordinate system of the view
     * and will be converted to the page coordinate system before applying the scaling.
     *
     * @param trans - The [x, y] translation distance along in pixels.
     */
    public translate(translation: [number, number]) {
        const invertedMatrix = this.viewMatrix.inverse();
        //translation = this.getPosInPageCoords(translation);
        this.viewMatrix.translateSelf(
            translation[0] * invertedMatrix.a,
            translation[1] * invertedMatrix.d,
        );
        this._updateSharedMatrix();
    }

    public resetMatrix() {
        this.viewMatrix = new DOMMatrix();
        this._updateSharedMatrix();
    }

    public getMatrix(): DOMMatrix {
        return this.viewMatrix;
    }

    /** This class can be used with pipelines
     * not on the main thread. Here we basically set an
     * shared buffer which is read by the viewportWorker class.
     *
     * This is ~ 10 times faster than using ipc with channels.
     */
    private _updateSharedMatrix() {
        this.buffer.viewMatrixView.set(this.viewMatrix.toFloat64Array());
    }

    private _updateSize() {
        this.buffer.surfaceSizeView[0] = this.surfaceSize.width;
        this.buffer.surfaceSizeView[1] = this.surfaceSize.height;
        this.buffer.pageSizeView[0] = this.pageSize.width;
        this.buffer.pageSizeView[1] = this.pageSize.height;
    }
}

/** Can be used on workers in combination with the class above to
 * receive viewport updates and process them accordingly.
 */
export class ViewPortWorker extends AbstractViewport {
    /** Pointer to the shared float64 array
     * even though it is a shared array
     * we parse it as float64array, as we cant
     * convert sharedarrays or views to dommatrix
     * directly.
     * We also need to case the shared array before
     * every read for some reason.
     * likely because the underlying atomic parsing
     * implements some kind of dirty state which
     * converts the array back to an shared array.
     */
    constructor(sharedArray: SharedArrayBuffer) {
        const buffer = new SharedDataBuffer(sharedArray);
        super(buffer);
    }

    getMatrix() {
        // need to cast to float64 to copy the array
        return DOMMatrixReadOnly.fromFloat64Array(
            new Float64Array(this.buffer.viewMatrixView),
        );
    }

    getSurfaceSizeArray(): Float64Array {
        return this.buffer.surfaceSizeView;
    }
}

export class ImageViewport {
    private imageElement: HTMLImageElement;
    private viewMatrix: DOMMatrix;
    private viewportSize: Size;

    constructor(imageElement: HTMLImageElement, viewPortSize: Size) {
        this.imageElement = imageElement;
        this.viewMatrix = new DOMMatrix();
        this.viewportSize = viewPortSize;

        // Fit the image initially
        this.fitImage();
    }

    /**
     * Fit the image within the viewport, maintaining aspect ratio.
     */
    public fitImage() {
        const imageWidth = this.imageElement.clientWidth;
        const imageHeight = this.imageElement.clientHeight;

        const viewportWidth = this.viewportSize.width;
        const viewportHeight = this.viewportSize.height;

        const scale = Math.min(
            viewportWidth / imageWidth,
            viewportHeight / imageHeight,
        );

        this.viewMatrix = new DOMMatrix().scale(scale, scale);
    }

    /**
     * Get the current view matrix.
     */
    getMatrix(): DOMMatrix {
        return this.viewMatrix;
    }

    /**
     * Converts coordinates from the current view's coordinate system
     * to the page's coordinate system.
     *
     * This method uses the inverse of the current view matrix to transform
     * the given coordinates.
     *
     * @param pos - The [x, y] coordinates in the view's coordinate system.
     * @returns A tuple containing the [x, y] coordinates in the page's coordinate system.
     */
    getPosInPageCoords(pos: [number, number]): [number, number] {
        const invertedMatrix = this.getMatrix().inverse();
        return [
            pos[0] * invertedMatrix.a +
                pos[1] * invertedMatrix.c +
                invertedMatrix.e,
            pos[0] * invertedMatrix.b +
                pos[1] * invertedMatrix.d +
                invertedMatrix.f,
        ];
    }

    /**
     * Converts relative coordinates from the current view's coordinate system
     * to the page's coordinate system. E.g. width and height or other distances
     *
     * This method uses the inverse of the current view matrix to transform
     * the given coordinates.
     *
     * @param pos - The [x, y] coordinates in the view's coordinate system.
     * @returns A tuple containing the [x, y] coordinates in the page's coordinate system.
     */
    getRelativeInPageCoords(pos: [number, number]): [number, number] {
        const invertedMatrix = this.getMatrix().inverse();
        const scaleX = invertedMatrix.a;
        const scaleY = invertedMatrix.d;
        return [pos[0] * scaleX, pos[1] * scaleY];
    }

    /**
     * Handle image resizing (e.g., when the image is loaded or resized).
     */
    public onImageResize() {
        this.fitImage();
    }
}
