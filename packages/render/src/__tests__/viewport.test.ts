//import "@vitest/web-worker";

import { ViewPort } from "../viewport";
import { Size } from "../types";
describe("ViewPort and ViewPortWorker Communication Test", () => {
    let viewPort: ViewPort;
    let worker: Worker;

    beforeAll(async () => {
        // Define initial sizes
        const surfaceSize: Size = { width: 800, height: 600 };
        const pageSize: Size = { width: 1000, height: 1000 };

        // Initialize ViewPort in main thread
        viewPort = new ViewPort(surfaceSize, pageSize);

        // Create the worker
        worker = new Worker(new URL("./worker.ts", import.meta.url), {
            type: "module",
        });

        // Create a promise to wait for worker result
        const workerPromise = new Promise<void>((resolve) => {
            worker.onmessage = (e) => {
                if (e.data.type === "init") {
                    resolve();
                }
            };
        });

        // Send shared buffer to worker
        worker.postMessage({
            type: "init",
            buffer: viewPort.buffer.buffer,
        });

        // Wait for worker to finish processing
        await workerPromise;
    });

    afterAll(() => {
        // Clean up
        worker.terminate();
    });
    beforeEach(() => {
        viewPort.buffer.hasViewMatrixChanged();
        viewPort.buffer.hasSurfaceSizeChanged();
    });

    it("updated shared viewMatrix", async () => {
        // Create a promise to wait for worker result
        const workerPromise = new Promise<void>((resolve) => {
            worker.onmessage = (e) => {
                if (e.data.type === "setMatrix") {
                    resolve();
                }
            };
        });

        const newMatrix = new Float64Array([
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
        ]);
        worker.postMessage({
            type: "setMatrix",
            array: newMatrix,
        });

        await workerPromise;

        expect(viewPort.buffer.hasViewMatrixChanged()).toBe(true);
        expect(viewPort.buffer.hasViewMatrixChanged()).toBe(false);
        expect(viewPort.buffer.hasSurfaceSizeChanged()).toBe(false);

        // Validate all elements in the view matrix
        for (let i = 0; i < newMatrix.length; i++) {
            expect(viewPort.buffer.viewMatrixView[i]).toEqual(newMatrix[i]);
        }
    });
    it("updated shared surface size", async () => {
        // Create a promise to wait for worker result
        const workerPromise = new Promise<void>((resolve) => {
            worker.onmessage = (e) => {
                if (e.data.type === "setSize") {
                    resolve();
                }
            };
        });

        const newMatrix = new Float64Array([1000, 1000]);
        worker.postMessage({
            type: "setSize",
            array: newMatrix,
        });

        await workerPromise;

        expect(viewPort.buffer.hasViewMatrixChanged()).toBe(false);
        expect(viewPort.buffer.hasSurfaceSizeChanged()).toBe(true);
        expect(viewPort.buffer.hasSurfaceSizeChanged()).toBe(false);

        // Validate all elements in the view matrix
        for (let i = 0; i < newMatrix.length; i++) {
            expect(viewPort.buffer.surfaceSizeView[i]).toEqual(newMatrix[i]);
        }
    });
});
