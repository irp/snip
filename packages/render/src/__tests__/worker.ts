import { ViewPortWorker } from "../viewport";

let viewport: ViewPortWorker;
self.onmessage = function (e) {
    switch (e.data.type) {
        case "init":
            viewport = new ViewPortWorker(e.data.buffer);
            self.postMessage({ type: "init" });
            return;
        case "setMatrix":
            viewport.buffer.viewMatrixView.set(e.data.array);
            self.postMessage({ type: "setMatrix" });
            return;
        case "setSize":
            viewport.buffer.surfaceSizeView.set(e.data.array);
            self.postMessage({ type: "setSize" });
            return;
    }
};
