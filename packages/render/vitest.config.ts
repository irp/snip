// vitest.config.ts
import { defineConfig } from "vitest/config";

export default defineConfig({
    server: {
        headers: {
            "Cross-Origin-Embedder-Policy": "require-corp",
            "Cross-Origin-Opener-Policy": "same-origin",
        },
    },
    test: {
        globals: true,
        //pool: "forks",
        //environment: "jsdom",
        browser: {
            provider: "webdriverio", // or 'webdriverio'
            enabled: true,
            name: "chrome", // browser name is required,
            headless: false,
        },
    },
});
