const worker1 = new Worker("worker_1.js");

const numUpdates = 1000;
const matrix = new DOMMatrix();

function updateAndSend() {
    matrix.translateSelf(10, 20);
    const floatArray = matrix.toFloat64Array();
    worker1.postMessage(floatArray.buffer);
}

console.time("sendBuffer");
for (let i = 0; i < numUpdates; i++) {
    updateAndSend();
}
console.timeEnd("sendBuffer");
