let sharedArray;

onmessage = (event) => {
    if (event.data instanceof SharedArrayBuffer) {
        // Initialize the sharedArray
        sharedArray = new Float64Array(event.data);
        // Start checking for updates
        checkForUpdates();
    }
};


let previousView = null;
function checkForUpdates() {
    // Perform operations or calculations with sharedArray
    console.log("Checking for updates...");

    // We create a copy every render 
    const matrix = DOMMatrixReadOnly.fromFloat64Array(new Float64Array(sharedArray));
    // check equality
    // if not equal trigger update
    console.log(matrix.e)

    // Request next animation frame
    requestAnimationFrame(checkForUpdates);
}
