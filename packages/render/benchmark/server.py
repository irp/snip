# server.py

from http.server import SimpleHTTPRequestHandler, HTTPServer
from socketserver import BaseServer
import ssl


class CustomRequestHandler(SimpleHTTPRequestHandler):
    def end_headers(self):
        self.send_header("Cross-Origin-Opener-Policy", "same-origin")
        self.send_header("Cross-Origin-Embedder-Policy", "require-corp")
        super().end_headers()


def run(server_class=HTTPServer, handler_class=CustomRequestHandler):
    server_address = ("", 8000)
    httpd = server_class(server_address, handler_class)
    httpd.socket = ssl.wrap_socket(
        httpd.socket,
        certfile="../../../../../../nginx/ssl/certificate.crt",
        keyfile="../../../../../../nginx/ssl/certificate.key",
        server_side=True,
    )
    return httpd


if __name__ == "__main__":
    httpd = run()
    print("Serving HTTPS on port 8000...")
    httpd.serve_forever()
