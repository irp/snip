/**
 * Represents a generic Node with a value and an optional reference to the next Node.
 */
export type Node<T> = {
    value: T;
    next?: Node<T>;
};

/**
 * A generic Queue implementation.
 *
 * Provides basic queue operations: enqueue, dequeue, and peek.
 *
 * @typeParam T - The type of elements held in this queue.
 */
export default class Queue<T> {
    public length: number;
    private head?: Node<T>;
    private tail?: Node<T>;

    constructor() {
        this.head = this.tail = undefined;
        this.length = 0;
    }

    /**
     * Adds an element to the end of the queue.
     *
     * Increases the length of the queue by one. If the queue is empty,
     * the new node becomes both the head and the tail. Otherwise, the new
     * node is added at the end of the queue and becomes the new tail.
     *
     * @param value - The value to be added to the queue.
     */
    enqueue(value: T): void {
        const node = { value };

        this.length++;

        if (!this.tail) {
            this.head = this.tail = node;
            return;
        }

        this.tail.next = node;
        this.tail = node;
    }

    /**
     * Removes and returns the element at the front of the queue.
     *
     * Decreases the length of the queue by one. If the queue becomes empty after
     * the operation, the tail is also set to undefined. If the queue is already
     * empty, the method returns undefined.
     *
     * @returns The value of the element removed from the front of the queue,
     *          or undefined if the queue is empty.
     */
    dequeue(): T | undefined {
        if (!this.head) {
            return undefined;
        }

        this.length--;

        const head = this.head;
        this.head = this.head.next;

        if (this.length === 0) {
            this.tail = undefined;
        }

        return head.value;
    }

    /**
     * Returns the value of the element at the front of the queue without removing it.
     *
     * If the queue is empty, the method returns undefined.
     *
     * @returns The value of the element at the front of the queue,
     *          or undefined if the queue is empty.
     */
    peek(): T | undefined {
        return this.head?.value;
    }
}
