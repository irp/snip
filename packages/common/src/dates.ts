export type DateFormat = "raw" | "compact" | "full";

/**
 * Converts a Date object to a localized string representation.
 * If the date is the current date, it returns the time in a 12-hour format.
 * Otherwise, it returns the date and time in a localized format, including the year, month, day, and time zone.
 * @param date - The Date object to convert.
 * @param format - The format of the date string to return.
 * @returns A string representation of the date in the local time zone.
 */
export function localDateString(
    date: Date | string,
    format: DateFormat = "compact",
): string {
    date = new Date(date);

    switch (format) {
        case "raw":
            return date.toString();
        case "compact":
            return getCompactDate(date);
        case "full":
            return getFullDate(date);
    }
}

function getCompactDate(date: Date): string {
    let options;
    if (date.toDateString() === new Date().toDateString()) {
        // On the same day, only show the time
        options = {
            hour: "numeric",
            minute: "numeric",
            hour12: false,
        };
    }
    // on the same year, show the day and time
    else if (date.getFullYear() === new Date().getFullYear()) {
        options = {
            month: "short",
            day: "numeric",
            hour: "numeric",
            minute: "numeric",
            hour12: false,
        };
    } else {
        // show the full date
        options = {
            year: "numeric",
            month: "short",
            day: "numeric",
            hour: "numeric",
            minute: "numeric",
            hour12: false,
        };
    }

    return new Intl.DateTimeFormat("de-DE", options).format(date);
}

function getFullDate(date: Date): string {
    return new Intl.DateTimeFormat("de-DE", {
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        hour12: false,
    }).format(date);
}

export function isDate(date: string | Date | undefined) {
    if (date === undefined) return false;
    return !isNaN(Date.parse(date as string));
}
