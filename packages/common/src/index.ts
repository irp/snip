/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Register service.
 * @description Stores instances in `global` to prevent memory leaks in development.
 * @arg {string} name Service name.
 * @arg {function} initFn Function returning the service instance.
 * @return {*} Service instance.
 */
export function registerService<T>(name: string, initFn: () => T): T {
    if (!globalThis.services) {
        globalThis.services = new Map<string, unknown>();
    }

    if (!globalThis.services.has(name)) {
        globalThis.services.set(name, initFn());
    }

    return globalThis.services.get(name) as T;
}

/**
 * Checks if an object has a specific property.
 *
 * @param object - The object to check.
 * @param property - The property to check for.
 * @returns `true` if the object has the property, `false` otherwise.
 */
export function hasOwnProperty(object: unknown, property: string): boolean {
    return Object.prototype.hasOwnProperty.call(object, property);
}

/**
 * Debounces a function by delaying its execution until a certain amount of time has passed without any further calls.
 * @param func The function to debounce.
 * @param timeout The debounce timeout in milliseconds (default: 300).
 * @returns A debounced version of the function.
 */
export function debounce(func: (...args: any[]) => void, timeout = 300) {
    let timer: NodeJS.Timeout;
    return (...args: any[]) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this as any, args);
        }, timeout);
    };
}

//Debounce function with return value using promises
export function debounceWithReturn<A, R>(
    callback: (...args: A[]) => Promise<R>,
    delay: number,
) {
    let timer;

    return (...args) => {
        return new Promise<R>((resolve, reject) => {
            clearTimeout(timer);
            timer = setTimeout(() => {
                try {
                    const output = callback(...args);
                    resolve(output);
                } catch (err) {
                    reject(err);
                }
            }, delay);
        });
    };
}

/**
 * Checks if a given string is a valid HTTP URL.
 *
 * @param {string} string - The string to be checked.
 * @returns {boolean} - Returns true if the string is a valid HTTP URL, otherwise returns false.
 */
export function isValidHttpUrl(string: string): boolean {
    let url;

    try {
        url = new URL(string);
    } catch (_) {
        return false;
    }

    return url.protocol === "http:" || url.protocol === "https:";
}

/**
 * Checks if an element is currently in the viewport.
 * @param {Element} element - The element to check.
 * @returns {boolean} - True if the element is in the viewport, false otherwise.
 */
export function isInViewport(element: Element): boolean {
    const rect = element.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <=
            (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <=
            (window.innerWidth || document.documentElement.clientWidth)
    );
}
