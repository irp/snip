/**
 * Checks if the code is running in a browser environment.
 * @returns {boolean} True if running in a browser, false otherwise.
 */
export function isBrowser(): boolean {
    return typeof window !== "undefined";
}

/**
 * Checks if the code is running in a worker environment.
 * @returns {boolean} True if running in a worker environment, false otherwise.
 */
export function isWorker(): boolean {
    return (
        typeof WorkerGlobalScope !== "undefined" &&
        self instanceof WorkerGlobalScope
    );
}
