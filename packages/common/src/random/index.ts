import { adjectiveList } from "./adjectives";
import { animaList } from "./animals";
import { colorList } from "./colors";

export function randomAnimal() {
    return rand_value_from_array(animaList);
}
export function randomAdjective() {
    return rand_value_from_array(adjectiveList);
}
export function randomColor() {
    return rand_value_from_array(colorList);
}

export function randomHexFromId(id: number) {
    // Simple hash function to create variation
    function simpleHash(n) {
        n = ((n >> 16) ^ n) * 0x45d9f3b;
        n = ((n >> 16) ^ n) * 0x45d9f3b;
        n = (n >> 16) ^ n;
        return n;
    }

    // Apply the hash function to the number
    const hashed = simpleHash(id);

    // Extract RGB components from the hashed value
    const r = (hashed >> 16) & 0xff;
    const g = (hashed >> 8) & 0xff;
    const b = hashed & 0xff;

    // Convert to hex color
    const hexColor = `#${r.toString(16).padStart(2, "0")}${g.toString(16).padStart(2, "0")}${b.toString(16).padStart(2, "0")}`;

    return hexColor;
}

const rand_value_from_array = (data: string[]): string => {
    return data[Math.floor(Math.random() * data.length)];
};
