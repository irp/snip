/* Compresses all images in the database to webp format */
import { get_pool } from "../migrate_legacy/src/connection";
import { progressOptions } from "../migrate_legacy/src/utils";
import { createHash } from "crypto";
import { default as sharp } from "sharp";
import cliProgress from "cli-progress"

// Keep track of sizes
const id_sizes = new Map<number, [number, number]>();


async function compress_images() {

    const start_time = new Date();

    console.log("Starting image compression in");
    // Wait for 5 seconds and print each second
    for (let i = 5; i > 0; i--) {
        console.log(i);
        await new Promise(resolve => setTimeout(resolve, 1000));
    }

    // Get total number of blobs which are images
    const pool = await get_pool("mariadb", "snip_data", 3306);
    const sql = `
        SELECT COUNT(*) AS count FROM blobs WHERE mime LIKE 'image%' AND mime NOT LIKE 'image/webp';
    `;
    const count = await pool.query(sql).then((res) => {
        return res[0].count;
    });
    console.log(`Found ${count} images to compress`);

    // Create progress bar
    const bar = new cliProgress.SingleBar(progressOptions, cliProgress.Presets.shades_classic);
    bar.start(count, 0);

    // Prepare stream
    const connection = await pool.getConnection();
    const stream = connection.queryStream("SELECT * FROM `blobs` WHERE mime LIKE 'image%' AND mime NOT LIKE 'image/webp'");

    // Iterate blobs
    for await (const blob of stream) {
        // Compress image
        try {
            await compress_image(blob, pool);
        } catch (e) {
            console.log(`Failed to compress image ${blob.id}`);
            console.log(e);
        }


        // Update progress bar
        bar.increment();
    }
    connection.release();

    bar.stop();


    const end_time = new Date();
    const time_diff = end_time.getTime() - start_time.getTime();
    console.log(`Finished image compression in ${time_diff / 1000} seconds`);


    // Compute total size difference
    let old_size = 0;
    let new_size = 0;
    for (const [_, [old, new_]] of id_sizes) {
        old_size += old;
        new_size += new_;
    }
    const size_diff = old_size - new_size;
    const size_diff_percent = (size_diff / old_size) * 100;

    const size_human_readable = (size: number) => {
        if (size < 1000) {
            return `${size} B`;
        }
        if (size < 1000000) {
            return `${(size / 1000).toFixed(2)} kB`;
        }
        if (size < 1000000000) {
            return `${(size / 1000000).toFixed(2)} MB`;
        }
        return `${(size / 1000000000).toFixed(2)} GB`;
    }

    console.log(`Total size difference: ${size_human_readable(size_diff)} (${size_diff_percent.toFixed(2)}%)`);


    // Save as json
    const fs = require("fs");
    const path = require("path");
    const dir = path.join(__dirname, "../mappings");
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    const size_change = path.join(dir, "size_change.json");

    fs.writeFileSync(size_change, JSON.stringify([...id_sizes]));

    return
}


async function compress_image(blob: any, pool: any) {

    const old_size = blob.size;

    // Compress data

    const data = await sharp(blob.data).webp({
        quality: 80,
        effort: 6,
        force:true
    }).toBuffer().catch((e) => {
        console.log(`Failed to compress image ${blob.id}`);
        console.log(e);
        return undefined;
    });
    if (data === undefined) {
        return
    }


    // Get new size in bytes
    const new_size = data.length;


    // Update map
    id_sizes.set(blob.id, [old_size, new_size]);

    // Update hash
    const hash = createHash("md5");
    hash.update(data);
    const new_hash = hash.digest("hex");

    // Update database
    pool.query("UPDATE blobs SET data = ?, size = ?, hash = ?, mime = ?, last_updated = ? WHERE id = ?", [data, new_size, new_hash, "image/webp", blob.last_updated, blob.id]);

    return
}

// Start the main function
(async () => {
    await compress_images();
})();
