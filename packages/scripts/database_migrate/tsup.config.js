import { defineConfig } from 'tsup';

import { config } from "@snip/tsup-config/base";

export default defineConfig([
    {
        ...config,
        entry: [
            "./src/main.ts"
        ],
        target: "node20",
        splitting: false,
    }
]);