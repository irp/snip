/* eslint-disable @typescript-eslint/no-explicit-any */
const FgRed = "\x1b[31m";
const FgYellow = "\x1b[33m";
const Reset = "\x1b[0m";

export const log = {
    debug: (...args: any[]) => console.debug("[Migrate]", ...args),
    info: (...args: any[]) => console.info("[Migrate]", ...args),
    warn: (...args: any[]) => {
        args = args.map((arg) => {
            return FgYellow + arg + Reset;
        });
        console.warn("[Migrate]", ...args);
    },
    error: (...args: any[]) => {
        args = args.map((arg) => {
            return FgRed + arg + Reset;
        });
        console.error("[Migrate]", ...args);
    },
};
