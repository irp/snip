import mariadb from "mariadb";

import { SQLFile } from "./fileParsing";
import { log } from "./logging";

let CREDS: Database | undefined = undefined;

type Database = {
    host: string;
    port: string | number;
    user: string;
    password: string;
};

export async function createPool({ host, port, user, password }: Database) {
    CREDS = { host, port, user, password };
    const pool = mariadb.createPool({
        host: host,
        user: user,
        password: password,
        port: parseInt(port as string),
        connectionLimit: 5,
        connectTimeout: 2500,
        initializationTimeout: 2500,
        acquireTimeout: 2500,
        multipleStatements: true,
        ssl: false,
    });
    try {
        await test_connection(pool);
        log.info("Database connection established.");
    } catch (err) {
        log.error("Could not connect to database", err);
    }
    return pool;
}

export async function executeFile(file: SQLFile): Promise<boolean> {
    const stmt = await file.read();

    if (stmt.trim() === "" || stmt === null || stmt === undefined) {
        log.error(`File ${file.path} is empty.`);
        return false;
    }

    const res = await execute_with_client(stmt);

    const success = res.exitCode === 0;
    if (!success) {
        log.error(`Error executing file ${file.path}`, res.stderr, res.stdout);
        return false;
    }

    return success;
}

export async function test_connection(pool: mariadb.Pool) {
    await pool.getConnection().then((conn) => {
        conn.ping();
        conn.release();
    });
}

export async function getDatabaseInfo(
    pool: mariadb.Pool,
    database: string,
    table: string,
) {
    // Check if database exists
    const sql = `SELECT COUNT(*) AS count
            FROM information_schema.tables
            WHERE table_schema = ?
            AND table_name = ?;`;
    const result = await pool.query(sql, [database, table]);

    if (result[0].count == 0n || result[0].count == 0) {
        log.info(
            `Table ${table} does not exist in database ${database}. Creating...`,
        );
        const sql = `
        USE ${database};
        CREATE TABLE IF NOT EXISTS ${database}.${table} (
            version_number INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            applied_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            applied_by VARCHAR(32) NOT NULL,
            description VARCHAR(256) NOT NULL
        ) ENGINE=InnoDB;
        `;

        await pool.query(`CREATE DATABASE IF NOT EXISTS ${database}`);
        await pool.query(sql);
    }

    const get_version_sql = `USE ${database}; SELECT version_number, applied_at FROM ${table} ORDER BY version_number DESC LIMIT 1;`;
    const version = await pool.query(get_version_sql).then((res) => res[1]);

    if (version.length === 0) {
        return { version_number: 0, applied_at: new Date() };
    }
    return version[0] as { version_number: number; applied_at: Date };
}
import { exec } from "child_process";
import fs from "fs/promises";
import { tmpdir } from "os";

interface CommandResult {
    stdout: string;
    stderr: string;
    exitCode: number | null;
}
async function execute_with_client(stmt: string): Promise<CommandResult> {
    if (CREDS === undefined) {
        throw new Error("Database credentials not set.");
    }

    // Create temp file
    await fs.writeFile(
        `${tmpdir()}/temp.sql`,
        "START TRANSACTION;" + stmt + ";COMMIT;",
    );
    // Execute file
    return new Promise((resolve, reject) => {
        const command = `mariadb --port="${CREDS!.port}" --user="${CREDS!.user}" --password="${CREDS!.password}" --host="${CREDS!.host}" --skip-ssl < ${tmpdir()}/temp.sql`;
        exec(command, (error, stdout, stderr) => {
            const exitCode = error ? error.code : 0;
            resolve({ stdout, stderr, exitCode: exitCode! });
        });
    });
}
