import fs from "fs/promises";
import { glob } from "glob";
import path from "path";

import { log } from "./logging";

export class SQLFile {
    path: string;
    version_number: number;
    description: string;

    constructor(path: string, version_number: number, description: string) {
        this.path = path;
        this.version_number = version_number;
        this.description = description;
    }

    static fromPath(filePath: string): SQLFile {
        // Path has to match the pattern: <version_number>_<description>.sql
        const fileName = path.basename(filePath);
        const regex = /(\d+)_(.*)\.sql/;
        const match = fileName.match(regex);

        if (!match) {
            throw new Error(
                `Invalid file name: ${fileName}, must match pattern: <version_number>_<description>.sql`,
            );
        }

        const version_number = parseInt(match[1]!);
        if (isNaN(version_number) || version_number <= 0) {
            throw new Error(
                `Invalid version number: ${match[1]}, must be a number and bigger than 0.`,
            );
        }
        const description = match[2]!;

        return new SQLFile(filePath, version_number, description);
    }

    async read(): Promise<string> {
        const content = await fs.readFile(this.path, "utf-8");
        const filteredContent = content
            .split("\n")
            .filter((line) => line.trim() !== "")
            .join("\n");
        return filteredContent;
    }
}

export async function getAllFileFromDir(
    dir: string,
    recursive = true,
    extension = ".sql",
): Promise<SQLFile[]> {
    // check if dir exists
    if (!(await exists(dir))) {
        throw new Error(`Directory does not exist: ${dir}`);
    }

    const ending = recursive ? "/**/*" + extension : "/*" + extension;
    const files = await glob(path.join(dir, ending));

    const sql_files = [];
    for (const file of files) {
        try {
            sql_files.push(SQLFile.fromPath(file));
        } catch (err) {
            let message = "";
            if (err instanceof Error) {
                message = err.message;
            }
            log.warn(`Skipping file ${file}.`, message);
        }
    }
    return sql_files;
}

async function exists(filePath: string): Promise<boolean> {
    return await fs
        .stat(filePath)
        .then(() => true)
        .catch(() => false);
}
