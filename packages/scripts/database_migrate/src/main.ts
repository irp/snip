import dotenv from "dotenv";
import path from "path";

import {
    createPool,
    executeFile,
    getDatabaseInfo,
    test_connection,
} from "./database";
import { getAllFileFromDir } from "./fileParsing";
import { log } from "./logging";

import { Argument, Command, Option } from "@commander-js/extra-typings";

// Load .env file from the current working directory
dotenv.config({
    path: "../../.env",
});

/** Create command parser
 * for arguments
 */
const program = new Command("Database Migrate")
    .description("Automatically migrate the database to the newest version.")
    .addOption(
        new Option("--host <host>", "mysql database host").default("localhost"),
    )
    .addOption(new Option("--port <port>", "mysql database port").default(3306))
    .addOption(
        new Option("--user <user>", "mysql database user").default("root"),
    )
    .addOption(
        new Option(
            "--password <password>",
            "mysql database user password",
        ).default(null, "Loaded from .env file"),
    )
    .addArgument(
        new Argument("<folder>", "Folder containing the migration files"),
    )
    .action(async (folder, options) => {
        await main(folder, options);
    });

program.showHelpAfterError();
program.parse();

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function parseOptions(options: any) {
    const { host, port, user } = options;
    const { password: pwraw } = options;
    let password: string | undefined = undefined;
    if (!pwraw) {
        password = process.env.MYSQL_ROOT_PASSWORD;
    } else {
        password = pwraw;
    }
    if (!password) {
        log.error("No password provided. Use --password or .env file");
        return;
    }
    if (!host || !port || !user) {
        log.error("Missing required options: --host, --port, --user");
        return;
    }
    return { host, port, user, password };
}

async function main(
    folder: string,
    options: {
        host: string;
        port: string | number;
        user: string;
        password: string | null;
    },
) {
    const opt = parseOptions(options);
    if (!opt) {
        return;
    }
    const { host, port, user, password } = opt;
    folder = path.resolve(folder);

    // Create database pool
    const pool = await createPool({ host, port, user, password });
    const version = await getDatabaseInfo(pool, "snip_dbinfo", "changelog");
    log.info(
        `Current database version: v${version.version_number} applied at ${version.applied_at.toISOString()}`,
    );

    // Get all files from folder
    const files = await getAllFileFromDir(folder);

    // Sort files by version number
    files.sort((a, b) => a.version_number - b.version_number);

    // Check if duplicate version numbers exist
    const duplicates = files
        .map((file) => file.version_number)
        .filter((version, index, self) => self.indexOf(version) !== index);

    if (duplicates.length > 0) {
        log.error("Duplicate version numbers found:", duplicates);
        return;
    }

    log.info(`Found ${files.length} migration files in folder ${folder}`);

    // Execute all files from version number
    for (const file of files) {
        if (version.version_number >= file.version_number) {
            log.info(
                `Skipping migration v${file.version_number}. Already applied.`,
            );
            continue;
        }
        const start = Date.now();
        const success = await executeFile(file);
        if (!success) {
            log.error("Migration failed. Stopping.");
            pool.end();
            process.exit(1);
        }
        // Write update to database
        await pool.query(
            `INSERT INTO snip_dbinfo.changelog (version_number, applied_at, applied_by, description) VALUES (?, NOW(), ?, ?)`,
            [file.version_number, "migration_script", file.path],
        );
        const time = Date.now() - start;
        log.info(
            `Executed migration v${file.version_number} (${file.path}) in ${time}ms`,
        );
    }

    log.info("Database migration successful.");
    pool.end();
}
