import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import terser from '@rollup/plugin-terser';
import { wasm } from '@rollup/plugin-wasm';


export default {
    input: ['build/main.js'], // The entry file
    output: {
        dir: 'bundle', // The output file
        format: 'esm', // Output format (e.g., 'cjs', 'es', 'umd', etc.)
    },
    plugins: [
        nodeResolve({
            preferBuiltins: true
        }), // Resolve third-party modules in node_modules
        wasm(), // Resolve WebAssembly modules
        commonjs(), // Convert CommonJS modules to ES6
        json(), // Convert JSON files to ES6
        terser() // Minify the bundle
    ],
    external: [
        "skia-canvas",
        "sharp",
        "mariadb"
    ], // Include most external packages in the bundle
};