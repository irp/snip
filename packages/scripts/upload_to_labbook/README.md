# Upload examples

This folder contains examples of how to upload data to a LabBook, you can take the examples and modify them to suit your needs.

## Setup

Before you run a script update or create an .env file in this folder with your credentials:

```bash
API_TOKEN=your_api_token
BOOK_ID=your_book_id
SNIP_URL=your_snip_deployment_url
```

The deployment url is your LabBook without the protocol, e.g. `https://snip.roentgen.physik.uni-goettingen.de/`.
The book_id and the api_token can be found in the settings of the LabBook you want to use.


## Usage

To run a script, open a terminal and run the script using bash or python.

```bash
cd ./python
pip3 install -r requirements.txt
python3 upload.py
```

The python script is a bit more generic and can be used to upload any file to a LabBook. The bash scripts are for more specific use cases.

```bash
cd ./bash
bash upload_image.sh
```

You can find a number of json files and images in the assets which are used by the scripts. Also have a look at the scripts to see how they are used.

## Images in arrays

As of now there is no nice way to upload images nested in array snips. To do it you have to encode the image binary to base64.

```bash
cat ../assets/TEST.png | base64 |  paste -sd' ' > base64.txt
```

You than have to copy this string into the json file, for an example see `assets/array_with_text_and_image.json`.
