import sys
import click
import json
import xml.etree.ElementTree as ET
import requests


@click.command()
@click.option("--input", "-i", type=click.Path(exists=True), help="Input file")
@click.option("--token", "-t", help="Api token")
@click.option("--book_id", "-b", help="Labbook id")
def main(input, token, book_id):
    """
    Convert a given restore.macro to a snip file
    and upload it to the labbook.

    Parameters
    ----------
    input : str
        Path to the restore.macro file
    token : str
        Api token
    book_id : str
        Labbook id (see in the url when editing a labbook)
    """

    # Check if all parameters are given
    if not input:
        print("Please specify an input file")
        sys.exit(1)
    if not token:
        print("Please specify an api token")
        sys.exit(1)
    if not book_id:
        print("Please specify a labbook id")
        sys.exit(1)

    # Parse xml file
    tree = ET.parse(input)
    point = tree.getroot().find("point")  # Get root element

    # convert to dict
    data = {
        object.get("name"): {prop.get("name"): prop.get("value") for prop in object}
        for object in point
    }
    # TODO: add data to the snip!

    """Create two rows in an array snip
    """
    h1 = [
        "sample",
        "file",
        "source",
    ]

    h2 = [
        "",
        "",
        "detector",
    ]

    b1 = [
        "energy/ current/ power",
        "exposure time",
        "num. avr. frames",
        "spot mode",
        "geom. magn.",
        "voxel size",
        "volume height",
        "volume diameter",
        "SOD",
        "SDD",
        "slices",
        "img/ turn",
    ]

    b2 = [
        "",
        "",
        "ImgZoom",
        "Zoom",
        "",
        "black cali",
        "gain cali",
        "before start",
        "after move",
        "anti ring",
        "beam alignment",
        "6 pass + ref",
    ]

    f1 = [
        "start @",
        "estimated time",
        "end @",
    ]

    text1 = ""
    for i in range(len(h1)):
        text1 += f"{h1[i]} \n"
    text1 += "______________________________\n"
    for i in range(len(b1)):
        text1 += f"{b1[i]} \n"
    text1 += "______________________________\n"
    for i in range(len(f1)):
        text1 += f"{f1[i]} \n"

    text2 = ""
    for i in range(len(h2)):
        text2 += f"{h2[i]} \n"
    text2 += "______________________________\n"
    for i in range(len(b2)):
        text2 += f"{b2[i]} \n"

    """ Create array snip
    """
    snip1 = {
        "type": "text",
        "data": {"text": text1},
        "view": {
            "x": 0,
            "y": 0,
            "size": 15,
            "lheight": 1.4 * 20,
            "wrap": 700,
        },
    }

    snip2 = {
        "type": "text",
        "data": {"text": text2},
        "view": {
            "x": 700,
            "y": 0,
            "size": 15,
            "lheight": 1.4 * 20,
            "wrap": 700,
        },
    }

    snip = {
        "type": "array",
        "data": {
            "snips": [snip1, snip2],
        },
    }
    snip = json.dumps(snip, indent=4)

    """ Upload snip to labbook
    """
    r = requests.post(
        f"http://localhost:4000/api/books/{book_id}/upload",
        files={"file": ("snip", snip, "application/json")},
        headers={"Authorization": f"Bearer {token}"},
    )

    # Print response
    print(r.text)


if __name__ == "__main__":
    main()
