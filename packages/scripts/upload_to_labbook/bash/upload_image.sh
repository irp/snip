#!/bin/bash
source ../.env

# Remove test if you want to upload to the real book
curl -k $SNIP_URL/api/books/$BOOK_ID/upload?test=true \
    -X POST \
    -H "Authorization: Bearer $API_TOKEN" \
    -F "file=@../assets/test.jpg"

