#!/bin/bash
source ../.env

# Remove test if you want to upload to the real book
curl -k $SNIP_URL/api/books/$BOOK_ID/upload?test=true \
    -X POST \
    -H "Authorization: Bearer $API_TOKEN" \
    -H "Content-Type: application/json" \
    -d "@../assets/array_with_text_and_image.json"
