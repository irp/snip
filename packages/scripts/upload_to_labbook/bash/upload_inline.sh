#!/bin/bash
source ../.env

# Remove test if you want to upload to the real book
curl -k $SNIP_URL/api/books/$BOOK_ID/upload?test=true \
    -X POST \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer $API_TOKEN" \
    -d "{\"type\": \"text\", \"data\": {\"text\": \"Hallo\"}}"


