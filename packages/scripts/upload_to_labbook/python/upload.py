#!/usr/bin/env python3

import click
import requests
from os import environ
from dotenv import load_dotenv
import sys
import logging


logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

# Load environment variables from .env file
# We use
#  - API_TOKEN
#  - BOOK_ID
#  - SNIP_URL
# If you haven't created a .env file yet please do so or pass the variables as arguments
load_dotenv("../.env")


@click.command()
@click.option(
    "--input",
    "-i",
    type=click.Path(exists=True),
    help="Input file",
    default="../assets/text.json",
)
@click.option(
    "--no_test",
    is_flag=True,
    help="Test mode i.e. no final commit to the database useful for testing",
    default=True,
)
@click.option("--token", "-t", help="Api token", default=environ.get("API_TOKEN"))
@click.option("--book_id", "-b", help="Book id", default=environ.get("BOOK_ID"))
@click.option("--snip_url", "-s", help="Snip url", default=environ.get("SNIP_URL"))
def main(input, no_test, token, book_id, snip_url):
    """
    Upload a given file to the labbook.

    Parameters
    ----------
    input : str
        Path to the file
    test : bool
        Test mode i.e. no final commit to the database useful for testing
    token : str
        Api token
    book_id : str
        Labbook id (see in the url when editing a labbook)
    """

    # Check if all parameters are given
    if not input:
        log.error("Please specify an input file")
        sys.exit(1)
    if not token:
        log.error("Please specify an api token")
        sys.exit(1)
    if not book_id:
        log.error("Please specify a labbook id")
        sys.exit(1)

    # We need to create a files obejct for the request
    files = {"file": (input, open(input, "rb"), "application/json")}

    url = f"{snip_url}/api/books/{book_id}/upload"
    if no_test:
        log.info("Running in test mode (no final commit to the database)")
        url += "?test=true"

    # Post request happens here!
    r = requests.post(
        url,
        files=files,
        headers={"Authorization": f"Bearer {token}"},
    )

    # Print response
    if r.status_code == 200:
        log.info("Received 200 response from server (OK)")
        log.info("Got the following response:")
        log.info(r.json())
    else:
        log.warning("Received an error from the server")
        log.warning(f"Status code: {r.status_code}")
        log.warning(f"Response: {r.json()}")


if __name__ == "__main__":
    main()
