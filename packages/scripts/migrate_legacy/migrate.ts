import { close_pools } from "./src/connection";
import { get_version, logger, save_mappings } from "./src/utils";
import { create_groups } from "./src/permissions";
import { migrate_books } from "./src/books";
import { migrate_pages } from "./src/pages";
import { migrate_doodles } from "./src/doodles";
import { migrate_blobs } from "./src/blobs";
import { migrate_snips } from "./src/snips";
import { adjust_array_snip_coordinates } from "./src/adjust_coords";


async function main() {

    const start_time = new Date();

    console.log("Starting migration of legacy database in");
    // Wait for 5 seconds and print each second
    for (let i = 5; i > 0; i--) {
        console.log(i);
        await new Promise(resolve => setTimeout(resolve, 1000));
    }

    // Get the database version
    let version = await get_version();
    if (version != 9) {
        console.warn("Database version is not 9, aborting!");
        return;
    }


    // Create/migrate groups
    console.log("Creating groups...");
    await create_groups();
    console.log("Groups created");

    // Migrate books
    console.log("Migrating books...");
    await migrate_books();
    console.log("Books migrated");

    //Migrate pages
    console.log("Migrating pages...");
    await migrate_pages();
    console.log("Pages migrated");

    //Migrate doodles to snips
    console.log("Migrating doodles...");
    await migrate_doodles();
    console.log("Doodles migrated");

    //Migrate blobs
    console.log("Migrating blobs...");
    await migrate_blobs();
    console.log("Blobs migrated");

    // Migrate snips & views
    console.log("Migrating snips...");
    await migrate_snips();
    console.log("Snips migrated");

    /*
    Adjust x and y coordinates of the snips inside the array snips
        
    This is done because in the legacy version, the x and y coordinates were
    modified in the plotting.
    */
    console.log("Adjusting array snip coordinates...");
    await adjust_array_snip_coordinates(start_time);
    console.log("Array snip coordinates adjusted");

    // Save all mappings
    console.log("Saving all mappings...");
    await save_mappings();
    console.log("Mappings saved");


    // Print all errors
    console.log("Migration finished");
    console.log("Printing all errors:");
    console.log("========================================");
    logger.print_all();

    // Close all pools
    await new Promise(resolve => setTimeout(resolve, 5000));
    await close_pools();
}




// Start the main function
(async () => {
    await main();
})();
