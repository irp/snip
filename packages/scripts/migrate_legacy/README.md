# Migrate script

This script allows to migrate a legacy database into an existing new database (version 9). To run you need to execute the docker compose `migrate` file. See below! Please read the how it works before continuing!


## How it works

We create two mariadb docker containers (legacy and current) than each column in the legacy
db is edited and copied to the new database. For more details feel free to check the
migration.ts script. This is executed on startup of the compose file!

There is most likely a more efficient way to do all of this but as this should only be run once performance was not really a concern.


## Run it

Make sure to backup the data before you start! E.g. using the `../database/backup_volume.sh` script.

In the project root execute:
```bash
./database/backup_volume.sh
docker-compose -f ./scripts/migrate_legacy/docker-compose.migrate.yml -f docker-compose.yml up
```

After migration if some errors occur and you might want to debug using the mappings from the old to the new ids! See `mappings` subfolder after running the script.
