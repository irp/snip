import { get_pool } from "./connection";
import cliProgress from "cli-progress";
import { progressOptions } from "./utils";

export async function adjust_array_snip_coordinates(start_time) {

    const pool = await get_pool("mariadb", "snip_data", 3306);

    const length = await pool.query("SELECT COUNT(*) as length FROM snips WHERE created > ? AND type = 'array'", [start_time]).then((res) => {
        return res[0].length;
    });


    const progressBar = new cliProgress.SingleBar(progressOptions, cliProgress.Presets.shades_classic);
    progressBar.start(length, 0);


    const sql = `
        SELECT * FROM snips WHERE created > ? AND type = "array";
    `;

    const array_snips = await pool.query(sql, [start_time]).then((res) => {
        return res;
    });

    for (const array_snip of array_snips) {
        const data = JSON.parse(array_snip.data_json);
        const ids = data.snip_ids

        // Get all array snips
        const sql_a = `
            SELECT * FROM snips WHERE id IN (?);
        `;
        const array_snips = await pool.query(sql_a, [ids]).then((res) => {
            return res;
        });

        // Iterate array snips
        await adjust_x_y(array_snips);

        progressBar.increment();
    }
    progressBar.stop();

}

async function adjust_x_y(snips) {
    const pool = await get_pool("mariadb", "snip_data", 3306);

    for (let i = 0; i < snips.length - 1; i++) {
        var next_view = snips[i + 1].view_json;
        var view = snips[i].view_json;

        if (!next_view || !view || next_view == "" || view == "") {
            continue;
        }

        // Parse
        next_view = JSON.parse(next_view);
        view = JSON.parse(view);

        const class_type = type_classify(snips[i].type);

        if (class_type == "text") {
            const text = JSON.parse(snips[i].data_json).text;
            if (!text) {
                continue;
            }
            const lheight = parseInt(view.lheight);
            const nLines = text.split("\n").length;
            const fontSize = parseInt(view.size);
            next_view.y = view.y + nLines * lheight + fontSize * 0.25;
        }
        if (class_type == "image") {
            view.rx = parseInt(view.rx);
            view.ry = parseInt(view.ry);
            view.rw = parseInt(view.rw);
            view.rh = parseInt(view.rh);
            view.width = parseInt(view.width);
            const height = view.rh * view.width / view.rw;
            next_view.y = view.y + height * 1.25;
        }

        // Update
        const sql = `
            UPDATE snips SET view_json = ? WHERE id = ?;
        `;
        await pool.query(
            sql,
            [JSON.stringify(view), snips[i].id]
        );
        await pool.query(
            sql,
            [JSON.stringify(next_view), snips[i + 1].id]
        );

    }
}


export function type_classify(type) {
    switch (type) {
        case "text":
        case "uprp/spec/timestamp":
        case "uprp/spec/attct":
        case "uprp/spec/motots":
        case "uprp/spec/logfile":
            return "text";
        // All images
        case "image":
        case "uprp/spec/legacy":
        case 'uprp/spec/cplot':
        case 'uprp/spec/livedada':
        case 'uprp/spec/matlab':
        case 'uprp/spec/scope':
        case 'uprp/spec/manta':
            return "image";
    }
}