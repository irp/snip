import { get_pool } from "./connection";


export const ag_to_id = new Map<string, number>();

/** Creates the groups from the legacy database.
 * 
 * This is the AG field in the books table (legacy db)
 * 
 * For now this is just done manually as it is not a lot of groups.
 * 
 */
export async function create_groups() {
    const groups = [
        {
            name: "Köster",
            name_old: "Köster",
            description: "Köster Group, Institut für Röntgenphysik, Göttingen",
        },
        {
            name: "Salditt",
            name_old: "Salditt",
            description: "Salditt Group, Institut für Röntgenphysik, Göttingen",
        },
        {
            name: "IRP-Göttingen",
            name_old: "IRP",
            description: "Institut für Röntgenphysik, Göttingen",
        }
    ];


    const pool = await get_pool("mariadb", "snip_perms", 3306);


    for (const group of groups) {
        const sql = `
            INSERT INTO groups (name, description) VALUES (?, ?)
        `;
        const id = await pool.query(sql, [group.name, group.description]).then((res) => {
            //Get id of inserted group res
            const id = res.insertId;
            return id;
        });

        // Add the id to the map
        ag_to_id.set(group.name_old, parseInt(id));
    }

}

