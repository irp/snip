import { get_pool } from "./connection";
import cliProgress from "cli-progress"
import { pages_oldId_to_newId } from "./pages";
import { progressOptions, logger } from "./utils";


export async function migrate_doodles() {
    const pool = await get_pool("mariadb_legacy", "snip", 3306);
    const doodles = await pool.query("SELECT * FROM doodles").then((res) => {
        return res;
    });

    const progressBar = new cliProgress.SingleBar(progressOptions, cliProgress.Presets.shades_classic);
    progressBar.start(doodles.length, 0);

    for (const doodle of doodles) {
        // 1. Insert doodle into new database
        await insert_doodle(doodle);
        // I guess the id is not used for anything?

        // 2. Increment progress bar
        progressBar.increment();
    }
    progressBar.stop();
}

/** Doodles in the old db are basically snips now!
 */
async function insert_doodle(doodle) {
    const pool = await get_pool("mariadb", "snip_data", 3306);

    // Check page id
    if (!pages_oldId_to_newId.has(parseInt(doodle.pageid))) {
        logger.log("Legacy page id '" + doodle.pageid + "' not found in pages table!");
        return undefined;
    }
    const page_id = pages_oldId_to_newId.get(doodle.pageid);

    // Get book id from current db
    const book_id = await get_bookid_via_pageid(page_id).catch((err) => {
        logger.log("Error getting book id from page id '" + page_id + " and doodle legacy id " + doodle.id + '!');
        return undefined;
    });

    if (book_id == undefined) {
        return undefined;
    }

    const type = "doodle";

    const old_data = JSON.parse(doodle.json);


    const data_json = JSON.stringify({
        type: "doodle",
        edges: old_data.edges,
    });

    const view_json = JSON.stringify({
        x: 0,
        y: 0,
        colour: old_data.colour || "#00c",
        width: old_data.width || 4,
    });

    const sql = `
        INSERT INTO snips(book_id, page_id, type, data_json, view_json) VALUES(?, ?, ?, ?, ?);
    `;

    const id = await pool
        .query(
            sql,
            [book_id, page_id, type, data_json, view_json],
        )
        .then((res) => {
            //Get id of inserted page res
            const id = res.insertId;
            return id;
        });
}


async function get_bookid_via_pageid(page_id) {
    const pool = await get_pool("mariadb", "snip_data", 3306);
    const sql = `
        SELECT book_id FROM pages WHERE id = ?;
    `;

    const book_id = await pool.query(sql, [page_id]).then((res) => {
        return res[0].book_id;
    });

    return book_id;
}