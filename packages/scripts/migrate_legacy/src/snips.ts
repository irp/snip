import { get_pool } from "./connection";
import cliProgress from "cli-progress"
import { progressOptions, logger } from "./utils";
import { books_oldId_to_newId } from "./books";
import { pages_oldId_to_newId } from "./pages";
import { blobs_oldId_to_newId } from "./blobs";

export const snips_oldId_to_newId = new Map<number, number | number[]>();

export async function migrate_snips() {
    // Get all snips from the legacy database
    const pool = await get_pool("mariadb_legacy", "snip", 3306);


    // Create view if it doesn't exist
    await pool.query(`
    CREATE OR REPLACE VIEW snips_resolved
    AS
    SELECT s.*, v.json as json_view, v.pageid as pageid
    FROM snips s LEFT JOIN views v ON s.id = v.snipid
    WHERE s.hide = 0;
    `
    );

    const length = await pool.query("SELECT COUNT(*) as length FROM snips_resolved").then((res) => {
        return res[0].length;
    });

    // Create progress bar
    const bar = new cliProgress.SingleBar(progressOptions, cliProgress.Presets.shades_classic);
    bar.start(length, 0);

    // Prepare stream
    const connection = await pool.getConnection();
    const stream = connection.queryStream("SELECT * FROM `snips_resolved`");

    // Iterate snips
    for await (const snip of stream) {
        // Insert snip into new database
        const id = await insert_snip(snip);

        // add to map
        snips_oldId_to_newId.set(parseInt(snip.id), parseInt(id));

        // Update progress bar
        bar.increment();
    }
    connection.release();

    // Check if all snips are in map
    if (snips_oldId_to_newId.size != length) {
        console.log("Not all snips are in map");
        console.log("Map size: " + snips_oldId_to_newId.size);
        console.log("Length: " + length);
    }


    bar.stop();

    return
}

/** Inserting a snip takes some work
 * ...
 * old:
 * id, bookid, hide, upd, type, json
 * ...
 */
async function insert_snip(snip) {

    // Check bookid
    if (!books_oldId_to_newId.has(parseInt(snip.bookid))) {
        logger.log("Legacy book id '" + snip.bookid + "' not found in books table! (" + snip.id + ")");
        return undefined;
    }
    // Check pageid
    if (!pages_oldId_to_newId.has(parseInt(snip.pageid))) {
        logger.log("Legacy page id '" + snip.pageid + "' not found in pages table! (" + snip.id + ")");
        return undefined;
    }

    const book_id = books_oldId_to_newId.get(parseInt(snip.bookid));
    const page_id = pages_oldId_to_newId.get(parseInt(snip.pageid));

    const data_json_old = JSON.parse(snip.json);
    const view_json_old = JSON.parse(snip.json_view);

    // Type decision
    var type_old;
    if (Array.isArray(data_json_old)) {
        type_old = "array";
    } else {
        type_old = snip.type || data_json_old.type || undefined;
    }
    var type = parse_type(type_old);
    if (type === undefined) {
        logger.log("Legacy snip id '" + snip.id + "' has no type!");
        return undefined;
    }
    // Insert snip into new database
    return await _insert_snip(snip.id, book_id, page_id, type, data_json_old, view_json_old, snip.upd);
}


async function _insert_snip(oldId, book_id, page_id, type, data_json_old, view_json_old, last_updated) {

    // Parsing the data is a bit harder thus we use a switch statement
    // and helper functions
    var data_json = data_json_old;
    var view_json = view_json_old;
    if (type === "array") {
        if (data_json.length == 1) {
            return await _insert_snip(
                oldId,
                book_id,
                page_id,
                data_json[0].type,
                data_json[0],
                view_json[0],
                last_updated
            );
        }
        if (data_json.length > 1) {
            const ids: number[] = [];
            for (let i = 0; i < data_json.length; i++) {

                const id = await _insert_snip(
                    undefined,
                    book_id,
                    page_id,
                    data_json[i].type,
                    data_json[i],
                    view_json[i],
                    last_updated
                );
                ids.push(id);
            }

            //insert array snip
            data_json = {
                type: "array",
                snip_ids: ids,
                legacy: true
            }
            view_json = {
                x: view_json[0].x,
                y: view_json[0].y,
            }
        }
        if (data_json.length < 1) {
            logger.log("Could not parse snip id '" + oldId + "' array data misformed!");
            return undefined;
        }
    }


    // Insert snip into new database
    const pool = await get_pool("mariadb", "snip_data", 3306);
    const sql = `
        INSERT INTO snips (book_id, page_id, type, data_json, view_json, last_updated, blob_id)
        VALUES (?, ?, ?, ?, ?, ?, ?);
    `;
    type = parse_type(type);
    // Check if blobid is in data_json
    var blob_id: number | null = null;
    if ("blobid" in data_json) {
        // Check if blobid is in map
        if (!blobs_oldId_to_newId.has(parseInt(data_json.blobid))) {
            logger.log("Legacy blob id '" + data_json.blobid + "' not found in blobs table! (" + oldId + ")");
            return undefined;
        }
        // Remove blobid from data_json
        blob_id = blobs_oldId_to_newId.get(parseInt(data_json.blobid)) || null;
        delete data_json.blobid;
    }

    view_json = await parse_view_json(type, view_json);

    const values = [book_id, page_id, type, JSON.stringify(data_json), JSON.stringify(view_json), last_updated, blob_id];
    const id = await pool
        .query(sql, values)
        .then((res) => {
            const id = res.insertId;
            return id;
        })
        .catch((err) => {
            console.log(err);
        });


    return id;
}


async function parse_data_json(type, data_json, oldId) {
    switch (type) {
        // Should be formatted already
        case "array":
            break
        // All texts
        case "text":
        case "uprp/spec/timestamp":
        case "uprp/spec/attct":
        case "uprp/spec/motots":
        case "uprp/spec/logfile":
            break
        // All images
        case "image":
        case "uprp/spec/legacy":
        case 'uprp/spec/cplot':
        case 'uprp/spec/livedada':
        case 'uprp/spec/matlab':
        case 'uprp/spec/scope':
        case 'uprp/spec/manta':
            if (!("blobid" in data_json)) {
                console.log(data_json);
                logger.log("Legacy snip id `" + oldId + "` has no blobid!");
            }
            break;
    }

    // Check if it has a blobid
    /*
    if ("blobid" in data_json || "blob_id" in data_json) {
        const bid = parseInt(data_json.blobid) || parseInt(data_json.blob_id);
        if (!blobs_oldId_to_newId.has(bid)) {
            logger.log("Legacy blob id '" + bid + "' not found!");
            return data_json;
        }
        data_json.blobid = blobs_oldId_to_newId.get(data_json.blobid);
    } else {
        console.log(data_json);
    }
    */

    return data_json;
}




function parse_view_json(type, view_json) {
    switch (type) {
        case "array":
            // Should be formatted already
            break
        // All texts
        case "text":
            view_json = { size: 12, lheight: 12 * 1.25, wrap: 78, font: "PlexMono", ...view_json };
        case "uprp/spec/timestamp":
        case "uprp/spec/attct":
            view_json = { size: 7, lheight: 7 * 1.25, wrap: 78, font: "PlexMono", ...view_json };
            break;
        case "uprp/spec/motots":
            view_json = { size: 7, lheight: 7 * 1.25, wrap: 200, font: "PlexMono", ...view_json };
            break;
        case "uprp/spec/logfile":
            view_json = { size: 7, lheight: 7 * 1.25, wrap: 78, font: "PlexMono", ...view_json };
        // All images
        case "image":
            view_json = { rot: "0", ...view_json };
            break;
        case "uprp/spec/legacy":
            view_json = { width: 1400, rot: "0", ...view_json };
            break;
        case 'uprp/spec/cplot':
            view_json = { width: 600, rot: "0", ...view_json };
            break;
        case 'uprp/spec/livedada':
            view_json = { width: 800, rot: "0", ...view_json };
            break;
        case 'uprp/spec/matlab':
            view_json = { width: 900, rot: "0", ...view_json };
            break;
        case 'uprp/spec/scope':
            view_json = { width: 900, rot: "0", ...view_json };
            break;
        case 'uprp/spec/manta':
            view_json = { width: 900, rot: "0", ...view_json };
            break;
    }
    return view_json;
}



function parse_type(str) {
    switch (str) {
        case "array":
        case "legacy":
        case "image":
        case "text":
            return str
        case "motors":
        case "macro.spec":
        case "logfile":
        case "timestamp":
        case "matlab":
        case "livedata":
        case "livedada":
        case "manta":
        case "attct":
        case "cplot":
        case "scope":
        case "dada":
            return "uprp/spec/" + str;
        case "uprp/spec/motors":
        case "uprp/spec/macro.spec":
        case "uprp/spec/logfile":
        case "uprp/spec/timestamp":
        case "uprp/spec/matlab":
        case "uprp/spec/livedata":
        case "uprp/spec/livedada":
        case "uprp/spec/manta":
        case "uprp/spec/attct":
        case "uprp/spec/cplot":
        case "uprp/spec/scope":
        case "uprp/spec/dada":
            return str;
        default:
            logger.log("Unknown snip type '" + str + "'!");
            return null;
    }
}