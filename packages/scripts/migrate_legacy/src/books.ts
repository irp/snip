import { get_pool } from "./connection";
import { ag_to_id } from "./permissions";



export const books_oldId_to_newId = new Map<number, number>();
/** Migrates the book column from legeacy into the new format
 * 
 */
export async function migrate_books() {

    // Get all books from the legacy database
    const pool = await get_pool("mariadb_legacy", "snip", 3306);
    const books = await pool.query("SELECT * FROM books").then((res) => {
        return res;
    });

    // Iterate books
    for (const book of books) {
        // 1. Insert book into new database
        const newId = await insert_book(book);
        // 2. Add old id to new id to map
        books_oldId_to_newId.set(parseInt(book.id), parseInt(newId));
    }
    return
}


function get_group_id(ag) {
    return ag_to_id.has(ag) ? ag_to_id.get(ag) : 1;
}

async function insert_book(book) {
    const pool = await get_pool("mariadb", "snip_data", 3306);
    const sql = `
        INSERT INTO books (title, comment, owner_group_id, created, finished) VALUES (?, ?, ?, ?, ?);
    `;
    const id = await pool
        .query(sql, [
            book.title,
            book.comment,
            get_group_id(book.AG),
            new Date(book.date1),
            new Date(book.date2),
        ])
        .then((res) => {
            //Get id of inserted book res
            const id = res.insertId;
            return id;
        });
    return id;
}