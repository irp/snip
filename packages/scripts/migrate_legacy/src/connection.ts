import mariadb from "mariadb";
import { config } from "../../../app/config";

const tinyToBoolean = (column, next) => {
    if (column.type == "TINY" && column.length === 1) {
        const val = column.int();
        return val === null ? null : val === 1;
    }
    return next();
};


const pools = new Map();

export async function get_pool(host, database, port) {

    if (pools.has(host + database)) {
        return pools.get(host + database);
    }
    else {
        return await register_pool(host, database, port);
    }
}

async function register_pool(host, database, port) {
    const pool = mariadb.createPool({
        host: host,
        user: config.MYSQL.USER,
        password: config.MYSQL.PASSWORD,
        database: database,
        dateStrings: false,
        connectionLimit: 20,
        trace: config.NODE_ENV === "development", // Enable tracing for development
        bigIntAsNumber: true,
        decimalAsNumber: true,
        insertIdAsNumber: true,
        typeCast: tinyToBoolean,
    });
    pools.set(host + database, pool);
    await test_connection(pool);
    return pool;
}

async function test_connection(pool) {
    await pool
        .getConnection()
        .then((conn) => {
            conn.ping();
            console.debug(
                "[Database] Connection established (PID:" +
                process.pid +
                ")"
            );
            conn.release();
        })
        .catch((err) => {
            console.error(err); // any of connection time or query time errors from above
        });
}


export function close_pool(host, database) {
    if (pools.has(host + database)) {
        pools.get(host + database).end();
    }
}

export async function close_pools() {
    for (const pool of pools.values()) {
        await pool.end();
    }
}