import { books_oldId_to_newId } from "./books";
import { get_pool } from "./connection";
import cliProgress from "cli-progress"
import { progressOptions, logger } from "./utils";

export const pages_oldId_to_newId = new Map<number, number>();

export async function migrate_pages() {
    const pool = await get_pool("mariadb_legacy", "snip", 3306);
    const pages = await pool.query("SELECT * FROM pages").then((res) => {
        return res;
    });


    const progressBar = new cliProgress.SingleBar(progressOptions, cliProgress.Presets.shades_classic,);
    progressBar.start(pages.length, 0);

    // Iterate pages
    for (const page of pages) {
        // 1. Insert page into new database
        const newId = await insert_page(page);
        // 2. Add old id to new id to map
        pages_oldId_to_newId.set(parseInt(page.id), parseInt(newId));
        // 3. Increment progress bar
        progressBar.increment();
    }

    progressBar.stop();
    return
}



async function insert_page(page) {
    const pool = await get_pool("mariadb", "snip_data", 3306);
    const sql = `
        INSERT INTO pages (book_id, page_number, last_updated) VALUES (?, ?, ?);
    `;

    if (!books_oldId_to_newId.has(parseInt(page.bookid))) {
        logger.log("Legacy book id '" + page.bookid + "' not found in books table!");
        return undefined;
    }

    const id = await pool
        .query(sql, [
            books_oldId_to_newId.get(parseInt(page.bookid)),
            page.pageno,
            new Date(page.upd),
        ])
        .then((res) => {
            //Get id of inserted page res
            const id = res.insertId;
            return id;
        });

    return id;
}
