import { get_pool } from "./connection";
import { books_oldId_to_newId } from "./books";
import { pages_oldId_to_newId } from "./pages";
import { blobs_oldId_to_newId } from "./blobs";
import { snips_oldId_to_newId } from "./snips";


export async function get_version() {
    const pool = await get_pool("mariadb", "snip_dbinfo", 3306);

    const version = pool.query("SELECT * FROM snip_dbinfo.changelog ORDER BY version_number DESC LIMIT 1")
        .then((res) => {
            return res[0].version_number;
        })
    return version;
}

export const progressOptions = {
    noTTYOutput: true,
    notTTYSchedule: 5000,
};




// Define logger as object
class Logger {
    messages: Map<string, number>;

    constructor() {
        this.messages = new Map<string, number>();
    }

    print_all() {
        this.messages.forEach((value, key) => {
            console.log(key, value);
        });
    }

    log(message) {
        if (!this.messages) {
            this.messages = new Map<string, number>();
        }
        const count = this.messages.get(message) || 0;
        this.messages.set(message, count + 1);
    }
}

export const logger = new Logger();


export async function save_mappings() {
    //Just save maps to file
    const fs = require("fs");
    const path = require("path");

    const dir = path.join(__dirname, "../mappings");
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    const books = path.join(dir, "books.json");
    const pages = path.join(dir, "pages.json");
    const blobs = path.join(dir, "blobs.json");
    const snips = path.join(dir, "snips.json");

    fs.writeFileSync(books, JSON.stringify(Array.from(books_oldId_to_newId.entries()), null, 4));
    fs.writeFileSync(pages, JSON.stringify(Array.from(pages_oldId_to_newId.entries()), null, 4));
    fs.writeFileSync(blobs, JSON.stringify(Array.from(blobs_oldId_to_newId.entries()), null, 4));
    fs.writeFileSync(snips, JSON.stringify(Array.from(snips_oldId_to_newId.entries()), null, 4));
}
