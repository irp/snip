import { get_pool } from "./connection";
import cliProgress from "cli-progress"
import { progressOptions } from "./utils";

export const blobs_oldId_to_newId = new Map<number, number>();

export async function migrate_blobs() {
    const pool = await get_pool("mariadb_legacy", "snip", 3306);

    const length = await pool.query("SELECT COUNT(*) AS length FROM blobs")
        .then((res) => {
            return res[0].length;
        });


    const progressBar = new cliProgress.SingleBar(progressOptions, cliProgress.Presets.shades_classic);
    progressBar.start(length, 0);


    // Prepare stream
    const connection = await pool.getConnection();
    const stream = connection.queryStream("SELECT * FROM blobs");

    // Iterate blobs
    for await (const blob of stream) {
        // 1. Insert blob into new database
        const newId = await insert_blob(blob);
        // 2. Add old id to new id to map
        await blobs_oldId_to_newId.set(parseInt(blob.id), parseInt(newId));
        // 3. Increment progress bar
        progressBar.increment();
    }
    connection.release();

    // Check if all blobs are in map
    if (blobs_oldId_to_newId.size != length) {
        console.log("Not all blobs are in map");
        console.log("Map size: " + blobs_oldId_to_newId.size);
        console.log("Length: " + length);
    }

    progressBar.stop();
}


async function insert_blob(blob) {
    const pool = await get_pool("mariadb", "snip_data", 3306);

    const sql = `
        INSERT INTO blobs(mime, size, data, hash, last_updated) VALUES (?, ?, ?, ?, ?);
    `;

    const id = await pool
        .query(
            sql, [
            blob.mime,
            blob.size,
            blob.data,
            blob.hash,
            new Date(blob.upd)
        ])
        .then((res) => {
            //Get id of inserted blob res
            const id = res.insertId;
            return id;
        })
        .catch((err) => {
            console.log(err);
        });

    return id;
}
