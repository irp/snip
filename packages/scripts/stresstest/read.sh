Hello y'all, I will give you a short overview on the architecture of snip as I have a better overview over this as Markus.

We have two big aspects:

The first is the client gui i.e. - all webinterfaces and user interactions
And the second is the server backend i.e. - the database - the apis - the socket server

Firstof the client html and js code is deployed via a nodejs server where we internally use typescript which is transpiled and than served. Specifically we use react for state management here. For the rendering the labbook pages we use the canvas api as it is highly performant inside a browser worker. Changes to these pages are than send to the server and other users via a websocket connection.

On the server we mainly do the same thing, we respond to the websocket request and resolve them. We also render the pages on the serverside to update images and pdfs. These are injested by the client for previews and for download. This is done to ensure that the client and the server render the same and that preview are always up to date. Here we created a scalable worker pool which is able to perform multiple of these tasks.

The database is a mariadb database which is basically just a relational database. Here we created some scripts to automatically migrate the database incase of changes to the structure and also to sync the database types with the typescript types. This allows to have a type safe database access. One could say dev ops.

There is also a small email service which is used to send emails for registration and password reset.

At the moment all of this is deployed with docker compose but in theory everything can also be just run without docker at all. And ofcourse we use version control with git and some automation using the gitlab continuous integration.
