# Stresstest

This folder houses a collection of scripts that can be used to stress test the
system.

## `test_socketio.ts`

This script can be used to test the socketio server. It will connect to the
server with a given number of clients and draw random lines on the canvas. The
script will also measure the time it takes for the server to send the lines to
the clients.

```
tsc test_socketio.ts && parallel -j 10 node test_socketio.js ::: {1..10}
```
