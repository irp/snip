"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var socket_io_client_1 = require("socket.io-client");
var URL = "https://snip.roentgen.physik.uni-goettingen.de";
var socket_io_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJib29rX2lkIjoiMTI3IiwidXNlcl9pZCI6OCwiaWF0IjoxNjk1NzMzNzY5LCJleHAiOjE2OTU4MjAxNjl9.yBwC2uzy3FnpdZ5jeMEMQdIaX2o077JLxIiYwByB8C4";
var BOOK_ID = 127;
var PAGE_ID = 20902;
var sleep = function (ms) {
    return new Promise(function (resolve) { return setTimeout(resolve, ms); });
};
function test_reading(token) {
    return __awaiter(this, void 0, void 0, function () {
        var socket;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    socket = (0, socket_io_client_1.io)(URL + "/book-" + BOOK_ID, {
                        auth: {
                            token: token
                        },
                        autoConnect: false
                    });
                    socket.on("error", function (err) {
                        console.log(err);
                    });
                    socket.on("connect_error", function (err) {
                        console.log(err);
                    });
                    socket.on("connect", function () {
                        console.log("Connected to server via websocket");
                    });
                    socket.on("snip:insert", function (newSnip) {
                        console.log("Got snip", newSnip.id);
                    });
                    socket.connect();
                    return [4 /*yield*/, sleep(2000)];
                case 1:
                    _a.sent();
                    // Do stuff with socket
                    socket.emit("page:join", PAGE_ID, function (success) {
                        if (success) {
                            console.log("Joined page");
                        }
                        else {
                            console.log("Failed to join page");
                        }
                    });
                    console.log("Waiting for snips");
                    return [2 /*return*/];
            }
        });
    });
}
function test_writing(token) {
    return __awaiter(this, void 0, void 0, function () {
        var socket, edges, i, rand_x, rand_y, i, SNIP_DATA;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    socket = (0, socket_io_client_1.io)(URL + "/book-" + BOOK_ID, {
                        auth: {
                            token: token
                        }
                    });
                    socket.on("error", function (err) {
                        console.log(err);
                    });
                    socket.on("connect_error", function (err) {
                        console.log(err);
                    });
                    socket.on("connect", function () {
                        console.log("Connected to server via websocket");
                    });
                    // Do stuff with socket
                    socket.emit("page:join", PAGE_ID, function (success) {
                        if (success) {
                            console.log("Joined page");
                        }
                        else {
                            console.log("Failed to join page");
                        }
                    });
                    edges = [];
                    for (i = 0; i < 100; i++) {
                        rand_x = Math.random() * 1000;
                        rand_y = Math.random() * 1000;
                        edges.push([rand_x, rand_y]);
                    }
                    return [4 /*yield*/, sleep(2000)];
                case 1:
                    _a.sent();
                    i = 0;
                    _a.label = 2;
                case 2:
                    if (!(i < 10)) return [3 /*break*/, 5];
                    edges.forEach(function (edge) {
                        edge[0] += 10;
                        edge[1] += 10;
                    });
                    SNIP_DATA = {
                        page_id: PAGE_ID,
                        book_id: BOOK_ID,
                        "type": "doodle",
                        "data": {
                            edges: edges
                        },
                        "view": {
                            x: 0,
                            y: 0,
                            colour: "#000000",
                            width: 5,
                            smoothing: 1
                        }
                    };
                    socket.emit("snip:insert", SNIP_DATA, function (id) {
                        console.log("Inserted snip with id " + id);
                    });
                    return [4 /*yield*/, sleep(500)];
                case 3:
                    _a.sent();
                    _a.label = 4;
                case 4:
                    i++;
                    return [3 /*break*/, 2];
                case 5:
                    socket.close();
                    return [2 /*return*/];
            }
        });
    });
}
;
// Parse arg to run test reading or writing
var args = process.argv.slice(2);
console.log(args);
if (args.length < 1) {
    console.error("Please provide exactly one argument, either 'read' or 'write'");
    process.exit(1);
}
var arg = args[0];
if (arg === "read") {
    console.log("Running read test");
    (function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, test_reading(socket_io_token)];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); })();
}
if (arg === "write") {
    console.log("Running write test");
    (function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, test_writing(socket_io_token)];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); })();
}
