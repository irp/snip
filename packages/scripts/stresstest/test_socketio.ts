import { io } from "socket.io-client";

const URL = "https://snip.roentgen.physik.uni-goettingen.de"
const socket_io_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJib29rX2lkIjoiMTI3IiwidXNlcl9pZCI6OCwiaWF0IjoxNjk1NzMzNzY5LCJleHAiOjE2OTU4MjAxNjl9.yBwC2uzy3FnpdZ5jeMEMQdIaX2o077JLxIiYwByB8C4"

const BOOK_ID = 127
const PAGE_ID = 20902

const sleep = (ms: number) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}


async function test_reading(token) {
    const socket = io(URL + "/book-" + BOOK_ID, {
        auth: {
            token,
        },
        autoConnect: false,
    });

    socket.on("error", (err) => {
        console.log(err);
    });
    socket.on("connect_error", (err) => {
        console.log(err);
    });
    socket.on("connect", () => {
        console.log("Connected to server via websocket");
    })
    socket.on("snip:insert", (newSnip: any) => {
        console.log("Got snip", newSnip.id);
    });


    socket.connect();

    await sleep(2000);

    // Do stuff with socket
    socket.emit("page:join", PAGE_ID, (success: boolean) => {
        if (success) {
            console.log("Joined page");
        } else {
            console.log("Failed to join page");
        }
    });

    console.log("Waiting for snips");
}


async function test_writing(token) {
    const socket = io(URL + "/book-" + BOOK_ID, {
        auth: {
            token,
        },
    });
    socket.on("error", (err) => {
        console.log(err);
    });
    socket.on("connect_error", (err) => {
        console.log(err);
    });
    socket.on("connect", () => {
        console.log("Connected to server via websocket");
    })


    // Do stuff with socket
    socket.emit("page:join", PAGE_ID, (success: boolean) => {
        if (success) {
            console.log("Joined page");
        } else {
            console.log("Failed to join page");
        }
    });


    // Generate 100 random edges between 0 and 1000
    const edges: number[][] = [];
    for (let i = 0; i < 100; i++) {
        const rand_x = Math.random() * 1000;
        const rand_y = Math.random() * 1000;
        edges.push([rand_x, rand_y]);
    }

    await sleep(2000);

    //draw this snip 100 times with slight offset
    for (let i = 0; i < 10; i++) {
        edges.forEach((edge) => {
            edge[0] += 10;
            edge[1] += 10;
        });

        const SNIP_DATA = {
            page_id: PAGE_ID,
            book_id: BOOK_ID,
            "type": "doodle",
            "data": {
                edges,
            },
            "view": {
                x: 0,
                y: 0,
                colour: "#000000",
                width: 5,
                smoothing: 1,
            }
        };


        socket.emit("snip:insert", SNIP_DATA, (id: number) => {
            console.log("Inserted snip with id " + id);
        });
        await sleep(500);
    }

    socket.close();
};

// Parse arg to run test reading or writing
const args = process.argv.slice(2);
console.log(args);
if (args.length < 1) {
    console.error("Please provide exactly one argument, either 'read' or 'write'");
    process.exit(1);
}
const arg = args[0];
if (arg === "read") {
    console.log("Running read test");
    (async () => {
        await test_reading(socket_io_token);
    })();
}
if (arg === "write") {
    console.log("Running write test");
    (async () => {
        await test_writing(socket_io_token);
    })();
}