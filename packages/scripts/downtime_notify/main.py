import logging
import smtplib
import time
from collections import deque
from email.mime.text import MIMEText
from logging.handlers import RotatingFileHandler

import requests
from ping3 import ping

# Configuration
WEBSITE_URL = "https://snip.roentgen.physik.uni-goettingen.de"
CHECK_INTERVAL = 20  # Seconds between checks

# SMTP email configuration
TRANSPORT = {
    "HOST": "email.gwdg.de",
    "PORT": 587,
    "USER": "xxx@xxx.de",
    "PASSWORD": "xxx",
}
TO = [
    {
        "EMAIL": "youremail",
        "CC": [],
        "DOWNTIME_THRESHOLD": 120,  # Seconds before sending the first email
    },
]

LOG_FILE = "./website_monitor.log"

# Logging setup
handler = RotatingFileHandler(
    LOG_FILE, maxBytes=5 * 1024 * 1024, backupCount=3
)  # 5 MB per file, keep 3 backups
logging.basicConfig(
    handlers=[handler],
    level=logging.INFO,
    format="%(asctime)s - %(levelname)s - %(message)s",
)


def send_email(subject: str, body: str, to_emails: list[str], cc_emails=None):
    """Send an email using the configured SMTP server."""
    msg = MIMEText(body)
    msg["Subject"] = subject
    msg["From"] = TRANSPORT["USER"]

    if to_emails and len(to_emails) > 0:
        msg["To"] = ", ".join(to_emails)
    else:
        logging.error("No recipient email addresses provided!")
        return

    if cc_emails and len(cc_emails) > 0:
        msg["Cc"] = ", ".join(cc_emails)

    try:
        with smtplib.SMTP(TRANSPORT["HOST"], TRANSPORT["PORT"]) as server:
            server.starttls()
            server.login(TRANSPORT["USER"], TRANSPORT["PASSWORD"])
            server.sendmail(
                TRANSPORT["USER"], to_emails + (cc_emails or []), msg.as_string()
            )
        logging.info(f"Email sent successfully to {to_emails}, CC: {cc_emails}")
    except Exception as e:
        logging.error(f"Failed to send email: {e}")


def check_website():
    """Check if the website is up and measure the ping time."""
    try:
        # Check if website is up
        response = requests.get(WEBSITE_URL, timeout=5)
        if response.status_code == 200:
            # Measure ping
            ping_time = ping(WEBSITE_URL.split("//")[1].split("/")[0], unit="ms")
            if ping_time is not None:
                logging.info(f"Website is up. Ping: {ping_time:.2f} ms")
            else:
                logging.warning("Website is up, but ping failed.")
            return True
        else:
            logging.warning(f"Website returned status code: {response.status_code}")
            return False
    except Exception as e:
        logging.error(f"Website is down: {e}")
        return False


def monitor_website():
    """Monitor the website and send an email if it goes down."""
    downtime_start: None | float = None
    send = []

    # Circular buffer to keep track of last n checks
    max_length = 1
    for recipient in TO:
        max_length = max(max_length, recipient["DOWNTIME_THRESHOLD"] // CHECK_INTERVAL)

    uptime_history = deque(maxlen=max_length)

    while True:
        if check_website():
            uptime_history.append((True, time.time()))
            downtime_start = None
            send = []
        else:
            uptime_history.append((False, time.time()))

            if downtime_start is None:
                downtime_start = time.time()
            else:
                downtime_duration = time.time() - downtime_start
                for recipient in TO:
                    if downtime_duration >= recipient["DOWNTIME_THRESHOLD"]:
                        if recipient["EMAIL"] not in send:
                            send_email(
                                "[Downtime Monitor] Snip is down!",
                                f"{WEBSITE_URL} has been down for {downtime_duration:.0f} seconds. Please check!",
                                [recipient["EMAIL"]],
                                recipient["CC"],
                            )
                            send.append(recipient["EMAIL"])
        time.sleep(CHECK_INTERVAL)


if __name__ == "__main__":
    monitor_website()
