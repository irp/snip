// Compute text bounds using a text snippet

import { writeFileSync } from "fs";
import path from "path";
import { Canvas, FontLibrary } from "skia-canvas";
import { fileURLToPath } from "url";

import { FONT_URLS } from "@snip/database/fonts/internal";
import { BaseSnip } from "@snip/snips/general/base";
import { TextSnip } from "@snip/snips/general/text";

const __filenameNew = fileURLToPath(import.meta.url);
const __dirnameNew = path.dirname(__filenameNew);

for (const [name, family] of FONT_URLS) {
    FontLibrary.use(family, "../../assets/fonts/" + name);
}

BaseSnip.prototype.createCanvas = function (width: number, height: number) {
    return new Canvas(width, height) as unknown as OffscreenCanvas;
};

const tesT_canvas = new Canvas(1000, 1000);
const tesT_ctx = tesT_canvas.getContext("2d");

tesT_ctx.font = "20px PlexMono";
tesT_ctx.fillText("Hello, World!", 10, 20);
writeFileSync("test.png", await tesT_canvas.toBuffer("png"));

const sizes = [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

const texts = [
    "Hello, World!",
    "This is a test",
    "This is a test\nwith a newline longer line",
];

const data = [];
const canvas = new Canvas(1000, 1000);
const ctx = canvas.getContext("2d");
for (const size of sizes) {
    for (const text of texts) {
        for (const [name, family] of FONT_URLS) {
            console.log(`Rendering ${family} ${size} ${text}`);
            const snip = new TextSnip({
                text,
                fontFamily: family.replace(".ttf", ""),
                fontSize: size,
            });
            const bounds = [snip.width, snip.height];
            data.push({
                text,
                fontFamily: family,
                fontSize: size,
                bounds,
            });

            // Render the snip
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            snip.render(ctx as any);
            ctx.translate(0, snip.height + 10);
        }
    }
}

const dir = __dirnameNew + "/../python/tests/test_snippets/";
writeFileSync(dir + "textbounds.json", JSON.stringify(data, null, 2));
writeFileSync(dir + "textbounds.png", await canvas.toBuffer("png"));
