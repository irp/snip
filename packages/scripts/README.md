# Scripts

This small package includes a number of script that are either run by the ci/cd pipeline or are used to interact with the labbook.

## Prepare Scripts


The prepare script is run before the labbook is started. It is primarily used to get sql typings as typescript types and to restrict the fonts used to the ones defined in the database.


```bash
pnpm run prepare
```

## test text bounds


The text test bounds script is used to generate a number of textbounds for the integration test with the python package. It is run manually and the output is automatically copied to the python package.


```bash
pnpm run test-text-bounds
```


## Other stuff in this folder


`upload_to_labbook` folder

This folder contains a number of scripts that can be used as an example to upload snippets to the labbook. 

`migrate_legacy` folder

Allows to migrate a legacy database to the new database format.

`compress_images` folder

This script is used to compress images in the database. It is used to reduce the size of the database.

